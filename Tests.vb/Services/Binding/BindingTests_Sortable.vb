Imports isr.Core.Services

Partial Public Class BindingTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(BindingTestsInfo.Get.Exists, $"{GetType(BindingTestsInfo)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " SORTABLE BINDINGLIST "

    ''' <summary> A sale. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Private Class Sale

        ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        Public Sub New()
            Me.SaleDate = DateTimeOffset.Now
        End Sub

        ''' <summary> Gets the sale details. </summary>
        ''' <value> The sale details. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Property SaleDetails As SortableBindingList(Of SaleDetail)

        ''' <summary> Gets the salesman. </summary>
        ''' <value> The salesman. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Property Salesman As String

        ''' <summary> Gets the client. </summary>
        ''' <value> The client. </value>
        Public Property Client As String

        ''' <summary> Gets the sale date. </summary>
        ''' <value> The sale date. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Property SaleDate As DateTimeOffset

        ''' <summary> Gets the total number of amount. </summary>
        ''' <value> The total number of amount. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public ReadOnly Property TotalAmount As Decimal
            Get
                Debug.Assert(Me.SaleDetails IsNot Nothing)
                Return Me.SaleDetails.Sum(Function(a) a.TotalAmount)
            End Get
        End Property
    End Class

    ''' <summary> A sale detail. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Private Class SaleDetail

        ''' <summary> Gets the product. </summary>
        ''' <value> The product. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Property Product As String

        ''' <summary> Gets the quantity. </summary>
        ''' <value> The quantity. </value>
        Public Property Quantity As Integer

        ''' <summary> Gets the unit price. </summary>
        ''' <value> The unit price. </value>
        Public Property UnitPrice As Decimal

        ''' <summary> Gets the total number of amount. </summary>
        ''' <value> The total number of amount. </value>
        Public ReadOnly Property TotalAmount As Decimal
            Get
                Return Me.UnitPrice * Me.Quantity
            End Get
        End Property
    End Class

    ''' <summary> Sortable list. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <returns> A list of. </returns>
    Private Shared Function SortableList() As SortableBindingList(Of Sale)
        Dim sales As Sale() = {New Sale() With {.Client = "Jane Doe", .SaleDate = New Date(2008, 1, 10), .Salesman = "John",
                                                .SaleDetails = New SortableBindingList(Of SaleDetail)() From {
                                                     New SaleDetail() With {.Product = "Sportsman", .Quantity = 1, .UnitPrice = 80},
                                                     New SaleDetail() With {.Product = "Tusker Malt", .Quantity = 2, .UnitPrice = 100},
                                                     New SaleDetail() With {.Product = "Alvaro", .Quantity = 1, .UnitPrice = 50}}},
                              New Sale() With {.Client = "Ben Jones", .SaleDate = New Date(2008, 1, 11), .Salesman = "Danny",
                                                .SaleDetails = New SortableBindingList(Of SaleDetail)() From {
                                                    New SaleDetail() With {.Product = "Embassy Kings", .Quantity = 1, .UnitPrice = 80},
                                                    New SaleDetail() With {.Product = "Tusker", .Quantity = 5, .UnitPrice = 100},
                                                    New SaleDetail() With {.Product = "Movie", .Quantity = 3, .UnitPrice = 50}}},
                              New Sale() With {.Client = "Tim Kim", .SaleDate = New Date(2008, 1, 12), .Salesman = "Kaplan",
                                               .SaleDetails = New SortableBindingList(Of SaleDetail)() From {
                                                    New SaleDetail() With {.Product = "Citizen Special", .Quantity = 10, .UnitPrice = 30},
                                                    New SaleDetail() With {.Product = "Burn", .Quantity = 2, .UnitPrice = 100}}}}
        Return New SortableBindingList(Of Sale)(sales.ToList)
    End Function

    ''' <summary> (Unit Test Method) tests sortable binding list. </summary>
    ''' <remarks> Tested 05/14/2019. </remarks>
    <TestMethod()>
    Public Sub SortableBindingListTest()
        Dim sales As SortableBindingList(Of Sale) = SortableList()
        Dim l As List(Of Sale) = sales.ToList
        Dim firstValue As String = l(0).Client
        Dim secondValue As String = l(1).Client
        Assert.IsTrue(String.Compare(firstValue, secondValue, StringComparison.CurrentCulture) = 1,
                      $"Before sorting {firstValue} must come before {secondValue }")

        l = sales.OrderBy(Of String)(Function(x) x.Client).ToList
        firstValue = l(0).Client
        secondValue = l(1).Client
        Assert.IsTrue(String.Compare(firstValue, secondValue, StringComparison.CurrentCulture) = -1,
                      $"After sorting ascending {firstValue} must come before {secondValue }")

        l = sales.OrderByDescending(Of String)(Function(x) x.Client).ToList
        firstValue = l(0).Client
        secondValue = l(1).Client
        Assert.IsTrue(String.Compare(firstValue, secondValue, StringComparison.CurrentCulture) = 1,
                      $"After sorting descending {firstValue} must come before {secondValue }")

        ' adds a last value, the list will remain ordered even without change.
        Dim expectedLastClient As String = "Yoda"
        sales.AllowNew = True
        sales.AllowEdit = True
        sales.AllowRemove = True
        Dim item As Sale = New Sale With {.Client = expectedLastClient, .SaleDate = New Date(2009, 1, 12), .Salesman = "Kaplan",
                                               .SaleDetails = New SortableBindingList(Of SaleDetail)() From {
                                                    New SaleDetail() With {.Product = "Cola", .Quantity = 4, .UnitPrice = 3.1D},
                                                    New SaleDetail() With {.Product = "Avian", .Quantity = 12, .UnitPrice = 1.5D}}}
        sales.Insert(0, item)
        ' sales.Add(item)
        l = sales.ToList
        firstValue = l(0).Client
        secondValue = l(1).Client
        Assert.IsTrue(String.Compare(expectedLastClient, secondValue, StringComparison.CurrentCulture) = 1,
                      $"After adding {expectedLastClient}={firstValue} must come before {secondValue }")

        l = sales.OrderBy(Of String)(Function(x) x.Client).ToList
        firstValue = l(0).Client
        secondValue = l(1).Client
        Assert.IsTrue(String.Compare(firstValue, secondValue, StringComparison.CurrentCulture) = -1,
                      $"After sorting ascending {firstValue} must come before {secondValue }")

        ' adds a first value, the list will remain ordered even without change.
        Dim expectedFirstClient As String = "Abe"
        sales.Add(New Sale With {.Client = expectedFirstClient, .SaleDate = New Date(2009, 1, 12), .Salesman = "Kaplan",
                                               .SaleDetails = New SortableBindingList(Of SaleDetail)() From {
                                                    New SaleDetail() With {.Product = "Pepsi", .Quantity = 4, .UnitPrice = 2.8D},
                                                    New SaleDetail() With {.Product = "Perrier", .Quantity = 12, .UnitPrice = 1.5D}}})
        l = sales.ToList
        firstValue = l(0).Client
        secondValue = l(1).Client
        Assert.IsTrue(String.Compare(expectedFirstClient, secondValue, StringComparison.CurrentCulture) = -1,
                      $"After adding {expectedFirstClient}={firstValue} must come before {secondValue }")
    End Sub

#End Region

#Region " B-Tree "


#End Region

End Class
