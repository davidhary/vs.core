Imports isr.Core.Services

<TestClass()>
Partial Public Class BindingTests

#Region " BINDING LIST VIEW "

    ''' <summary> Compare action. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="laftHand">  The laft hand. </param>
    ''' <param name="rightHand"> The right hand. </param>
    ''' <returns> An Integer. </returns>
    Private Function CompareAction(ByVal laftHand As Foo, ByVal rightHand As Foo) As Integer

        Return laftHand.Abscissa.CompareTo(rightHand.Abscissa)
    End Function

    ''' <summary> A foo. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Private Class Foo

        ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        Public Sub New()
            Dim r As New Random()
            Me._Abscissa = r.Next(0, 100)
            Me._Ordinate = r.Next(100, 1000).ToString()
        End Sub

        ''' <summary> Gets or sets the abscissa. </summary>
        ''' <value> The abscissa. </value>
        Public ReadOnly Property Abscissa() As Integer

        ''' <summary> Gets or sets the ordinate. </summary>
        ''' <value> The ordinate. </value>
        Public ReadOnly Property Ordinate() As String
    End Class

    ''' <summary> (Unit Test Method) tests binding list view. </summary>
    ''' <remarks> tested 05/15/2019. </remarks>
    <TestMethod()>
    Public Sub BindingListViewTest()
        Dim list As New List(Of Foo)
        For i As Integer = 1 To BindingTestsInfo.Get.ItemCount
            list.Add(New Foo())
        Next
        Assert.AreEqual(BindingTestsInfo.Get.ItemCount, list.Count, "Expected list size")

        Dim sw As New Stopwatch()
        Dim expectedTimeSpan As TimeSpan = TimeSpan.Zero
        Dim actualTimeSpan As TimeSpan = TimeSpan.Zero
        sw.Start()
        Using view As New BindingListView(Of Foo)(list)
            sw.Stop()
            expectedTimeSpan = BindingTestsInfo.Get.I7k2600CreateTimespan
            actualTimeSpan = sw.Elapsed
            Assert.IsTrue(expectedTimeSpan > actualTimeSpan,
                          $"BLV Create: expecting {BindingTestsInfo.Get.I7k2600} time span {expectedTimeSpan.TotalMilliseconds} > {actualTimeSpan.TotalMilliseconds}")

            sw.Reset()
            sw.Start()
            view.Sort = NameOf(Foo.Abscissa)
            sw.Stop()
            expectedTimeSpan = BindingTestsInfo.Get.I7k2600SortInternalTimespan
            actualTimeSpan = sw.Elapsed
            Assert.IsTrue(expectedTimeSpan > actualTimeSpan,
                          $"BLV.Sort: expecting {BindingTestsInfo.Get.I7k2600} time span {expectedTimeSpan.TotalMilliseconds} > {actualTimeSpan.TotalMilliseconds}")

            sw.Reset()
            sw.Start()
            view.ApplySort(AddressOf Me.CompareAction)
            sw.Stop()
            expectedTimeSpan = BindingTestsInfo.Get.I7k2600SortExtTimeSpan
            actualTimeSpan = sw.Elapsed
            Assert.IsTrue(expectedTimeSpan > actualTimeSpan,
                          $"BLV.ApplySort(delegate): expecting {BindingTestsInfo.Get.I7k2600} {expectedTimeSpan.TotalMilliseconds} > {actualTimeSpan.TotalMilliseconds}")
        End Using

        Using dataTable As New Data.DataTable
            dataTable.Locale = Globalization.CultureInfo.CurrentCulture
            dataTable.Columns.Add(NameOf(Foo.Abscissa), GetType(Integer))
            dataTable.Columns.Add(NameOf(Foo.Ordinate), GetType(String))
            For i As Integer = 1 To BindingTestsInfo.Get.ItemCount
                Dim row As Data.DataRow = dataTable.NewRow()
                row(0) = list(i - 1).Abscissa
                row(1) = list(i - 1).Ordinate
                dataTable.Rows.Add(row)
            Next

            sw.Start()
            dataTable.DefaultView.Sort = NameOf(Foo.Abscissa)
            sw.Stop()
            expectedTimeSpan = BindingTestsInfo.Get.I7k2600DataTableSortTimespan
            actualTimeSpan = sw.Elapsed
            Assert.IsTrue(expectedTimeSpan > actualTimeSpan,
                          $"{NameOf(Data.DataTable)}.{NameOf(Data.DataTable.DefaultView)}.Sort: expecting {BindingTestsInfo.Get.I7k2600} {expectedTimeSpan.TotalMilliseconds} > {actualTimeSpan.TotalMilliseconds}")
        End Using
    End Sub

#End Region

End Class
