''' <summary> Binding Tests. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/18/2019. </para>
''' </remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
     Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "15.9.0.0"),
     Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
Friend Class BindingTestsInfo
    Inherits ApplicationSettingsBase

#Region " SINGLETON "

    ''' <summary>
    ''' Initializes an instance of the <see cref="T:System.Configuration.ApplicationSettingsBase" />
    ''' class to its default state.
    ''' </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' Gets the locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    ''' <value> The sync locker. </value>
    Private Shared Property _SyncLocker As New Object

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Private Shared Property _Instance As BindingTestsInfo

    ''' <summary> Instantiates the class. </summary>
    ''' <remarks> Use this property to instantiate a single instance of this class. This class uses
    ''' lazy instantiation, meaning the instance isn't created until the first time it's retrieved. </remarks>
    ''' <returns> A new or existing instance of the class. </returns>
    Public Shared Function [Get]() As BindingTestsInfo
        If _Instance Is Nothing Then
            SyncLock _SyncLocker
                _Instance = CType(Global.System.Configuration.ApplicationSettingsBase.Synchronized(New BindingTestsInfo()), BindingTestsInfo)
            End SyncLock
        End If
        Return _Instance
    End Function

    ''' <summary> Returns true if an instance of the class was created and not disposed. </summary>
    ''' <value> <c>True</c> if instantiated; otherwise, <c>False</c>. </value>
    Public Shared ReadOnly Property Instantiated() As Boolean
        Get
            SyncLock _SyncLocker
                Return _Instance IsNot Nothing
            End SyncLock
        End Get
    End Property

#End Region

#Region " SETTINGS EDITORS EXCLUDED "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Public Shared Sub OpenSettingsEditor()
        isr.Core.Services.WindowsForms.EditConfiguration($"{GetType(BindingTestsInfo)} Editor", BindingTestsInfo.Get)
    End Sub

#End Region

#Region " CONFIGURATION INFORMATION "

    ''' <summary> Returns true if test settings exist. </summary>
    ''' <value> <c>True</c> if testing settings exit. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property Exists As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set
            Me.AppSettingSetter(Value)
        End Set
    End Property

    ''' <summary> Returns true to output test messages at the verbose level. </summary>
    ''' <value> The verbose messaging level. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("False")>
    Public Property Verbose As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set
            Me.AppSettingSetter(Value)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the sentinel indicating of all data are to be used for a test.
    ''' </summary>
    ''' <value> <c>true</c> if all data are to be used for a test; otherwise <c>false</c>. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property All As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set
            Me.AppSettingSetter(Value)
        End Set
    End Property

    ''' <summary> True if the test set is enabled. </summary>
    ''' <value> The enabled option. </value>
    <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Property Enabled As Boolean
        Get
            Return Me.AppSettingGetter(False)
        End Get
        Set
            Me.AppSettingSetter(Value)
        End Set
    End Property

#End Region

#Region " I7 TESTS SETTINGS "

    ''' <summary> Gets or sets the number of items. </summary>
    ''' <value> The number of items. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),
         Global.System.Configuration.DefaultSettingValueAttribute("200000")>
    Public Property ItemCount() As Integer
        Get
            Return Me.AppSettingGetter(0)
        End Get
        Set
            Me.AppSettingSetter(Value)
        End Set
    End Property

    ''' <summary> Gets or sets the 7k 2600 create timespan. </summary>
    ''' <value> The i 7k 2600 create timespan. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),
         Global.System.Configuration.DefaultSettingValueAttribute("00:00:02.8250000")>
    Public Property I7k2600CreateTimespan() As Global.System.TimeSpan
        Get
            Return Me.AppSettingGetter(TimeSpan.Zero)
        End Get
        Set
            Me.AppSettingSetter(Value)
        End Set
    End Property

    ''' <summary> Gets or sets the 7k 2600 sort internal timespan. </summary>
    ''' <value> The i 7k 2600 sort internal timespan. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),
         Global.System.Configuration.DefaultSettingValueAttribute("00:00:01.4790000")>
    Public Property I7k2600SortInternalTimespan() As Global.System.TimeSpan
        Get
            Return Me.AppSettingGetter(TimeSpan.Zero)
        End Get
        Set
            Me.AppSettingSetter(Value)
        End Set
    End Property

    ''' <summary> Gets or sets the 7k 2600 sort extent time span. </summary>
    ''' <value> The i 7k 2600 sort extent time span. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),
         Global.System.Configuration.DefaultSettingValueAttribute("00:00:00.6540000")>
    Public Property I7k2600SortExtTimeSpan() As Global.System.TimeSpan
        Get
            Return Me.AppSettingGetter(TimeSpan.Zero)
        End Get
        Set
            Me.AppSettingSetter(Value)
        End Set
    End Property

    ''' <summary> Gets or sets the 7k 2600 data table sort timespan. </summary>
    ''' <value> The i 7k 2600 data table sort timespan. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),
         Global.System.Configuration.DefaultSettingValueAttribute("00:00:01.2570000")>
    Public Property I7k2600DataTableSortTimespan() As Global.System.TimeSpan
        Get
            Return Me.AppSettingGetter(TimeSpan.Zero)
        End Get
        Set
            Me.AppSettingSetter(Value)
        End Set
    End Property

    ''' <summary> Gets or sets the 7k 2600. </summary>
    ''' <value> The i 7k 2600. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Diagnostics.DebuggerNonUserCodeAttribute(),
         Global.System.Configuration.DefaultSettingValueAttribute("I7k2600")>
    Public Property I7k2600() As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set
            Me.AppSettingSetter(Value)
        End Set
    End Property

#End Region

End Class
