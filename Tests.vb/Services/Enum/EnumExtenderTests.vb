Imports isr.Core

''' <summary>
''' This is a test class for EfficientEnumTest and is intended to contain all EfficientEnumTest
''' Unit Tests.
''' </summary>
''' <remarks> David, 2020-09-18. </remarks>
<TestClass()>
Public Class EnumExtenderTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " ENUM EXTENDER TESTS "
    ''' <summary> The color enum. </summary>
    Private Shared ReadOnly ColorEnum As EnumExtender(Of Color) = New EnumExtender(Of Color)()

    ''' <summary> Values that represent colors. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Private Enum Color
        ''' <summary> An enum constant representing the white option. </summary>
        <ComponentModel.Description("While Color")> White
        ''' <summary> An enum constant representing the black option. </summary>
        <ComponentModel.Description("Black Color")> Black
        ''' <summary> An enum constant representing the red option. </summary>
        <ComponentModel.Description("Red Color")> Red
        ''' <summary> An enum constant representing the yellow option. </summary>
        <ComponentModel.Description("Yellow Color")> Yellow
        ''' <summary> An enum constant representing the blue option. </summary>
        <ComponentModel.Description("Blue Color")> Blue
        ''' <summary> An enum constant representing the green option. </summary>
        <ComponentModel.Description("Green Color")> Green
        ''' <summary> An enum constant representing the cyan option. </summary>
        <ComponentModel.Description("Cyan Color")> Cyan
        ''' <summary> An enum constant representing the magenta option. </summary>
        <ComponentModel.Description("Magenta Color")> Magenta
        ''' <summary> An enum constant representing the pink option. </summary>
        <ComponentModel.Description("Pink Color")> Pink
        ''' <summary> An enum constant representing the purple option. </summary>
        <ComponentModel.Description("Purple Color")> Purple
        ''' <summary> An enum constant representing the orange option. </summary>
        <ComponentModel.Description("Orange Color")> Orange
        ''' <summary> An enum constant representing the brown option. </summary>
        <ComponentModel.Description("Brown Color")> Brown
    End Enum

    ''' <summary> Values that represent trings. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Private Shared ReadOnly EnumStrings As String() = New String() {"White", "Black", "Red", "Yellow", "Blue", "Green", "Cyan", "Magenta", "Pink", "Purple", "Orange", "Brown"}
    ''' <summary> The iterations. </summary>
    Private Const _Iterations As Integer = 100000

    ''' <summary> Main entry-point for this application. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Public Shared Sub Main()
        Dim randomNumber As Random = New Random()
        Using TempPerformanceMonitor As PerformanceMonitor = New PerformanceMonitor("{Built-in Enum class}")
            For i As Integer = 0 To _Iterations - 1
                Dim index As Integer = randomNumber.Next(0, 11)
                Dim c1 As Color = CType(System.Enum.ToObject(GetType(Color), index), Color)
                Dim c2 As Color = CType(System.Enum.Parse(GetType(Color), EnumStrings(index)), Color)
            Next i
        End Using

        ' Verify initialization of the data out of the comparative measurement.
        ' As you can see, this initialization is the gain for the later efficiency.
        Dim colorEnum As New EnumExtender(Of Color)
        Using TempPerformanceMonitor As PerformanceMonitor = New PerformanceMonitor("{StrongQuickEnum<Color> class}")
            For i As Integer = 0 To _Iterations - 1
                Dim index As Integer = randomNumber.Next(0, 11)
                Dim c1 As Color = colorEnum.ToObject(index)
                Dim c2 As Color = colorEnum.Parse(EnumExtenderTests.EnumStrings(index))
            Next i
        End Using
        Console.ReadLine()
    End Sub

    ''' <summary> A performance monitor. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Friend Class PerformanceMonitor
        Implements IDisposable
        ''' <summary> The timestarted. </summary>
        Private ReadOnly _Timestarted As Long
        ''' <summary> The name. </summary>
        Private ReadOnly _Name As String

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        ''' <param name="name"> The name. </param>
        Friend Sub New(ByVal name As String)
            Me._Name = name
            Me._Timestarted = DateTimeOffset.Now.Ticks
        End Sub

        ''' <summary>
        ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        ''' resources.
        ''' </summary>
        ''' <remarks> David, 2020-09-18. </remarks>
        Public Sub Dispose() Implements IDisposable.Dispose
            Console.WriteLine("Operation " & Me._Name & ":" & Constants.vbTab + Constants.vbTab + (DateTimeOffset.Now.Ticks - Me._Timestarted).ToString())
        End Sub
    End Class

    ''' <summary> (Unit Test Method) converts this object to an object test 1. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub ToObjectTest1()
        Dim value As Integer = CInt(Color.Cyan)
        Dim expected As Color = Color.Cyan
        Dim actual As Color = ColorEnum.ToObject(value)
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) converts this object to an object test 2. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub ToObjectTest2()
        Dim value As Integer = CInt(Color.Cyan)
        Dim expected As Color = Color.Cyan
        Dim actual As Color = ColorEnum.ToObject(value)
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) parse test 1. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub ParseTest1()
        Dim value As String = NameOf(Color.White)
        Dim ignoreCase As Boolean = False
        Dim expected As Color = Color.White
        Dim actual As Color = ColorEnum.Parse(value, ignoreCase)
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> A test for Parse. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Public Shared Sub ParseTestHelper(Of T)()
        Dim value As String = NameOf(Color.Cyan)
        Dim expected As Color = Color.Cyan
        Dim actual As Color = ColorEnum.Parse(value)
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) tests parse. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub ParseTest()
        EnumExtenderTests.ParseTestHelper(Of Color)()
    End Sub

    ''' <summary> A test for IsDefined. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Public Shared Sub IsDefinedTestHelper(Of T)()
        Dim value As Object = Color.Cyan
        Dim expected As Boolean = True
        Dim actual As Boolean = ColorEnum.IsDefined(value)
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) tests is defined. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub IsDefinedTest()
        EnumExtenderTests.IsDefinedTestHelper(Of Color)()
    End Sub

    ''' <summary> A test for GetValues. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Public Shared Sub GetValuesTestHelper(Of T)()
        Dim expected As Array = [Enum].GetValues(GetType(Color))
        Dim actual As Array = ColorEnum.Values
        Dim a As Integer() = New Integer(expected.Length - 1) {}
        actual.CopyTo(a, 0)
        Dim b As Integer() = New Integer(actual.Length - 1) {}
        expected.CopyTo(b, 0)
        Assert.IsTrue((a.Length = b.Length AndAlso a.Intersect(b).Count() = a.Length), "Values are equal")
    End Sub

    ''' <summary> (Unit Test Method) tests get values. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub GetValuesTest()
        EnumExtenderTests.GetValuesTestHelper(Of Color)()
    End Sub

    ''' <summary> A test for GetUnderlyingType. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Public Shared Sub GetUnderlyingTypeTestHelper(Of T)()
        Dim expected As Type = GetType(Integer)
        Dim actual As Type = ColorEnum.UnderlyingType
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) tests get underlying type. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub GetUnderlyingTypeTest()
        EnumExtenderTests.GetUnderlyingTypeTestHelper(Of Color)()
    End Sub

    ''' <summary> A test for GetNames. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Public Shared Sub GetNamesTestHelper(Of T)()
        Dim expected() As String = EnumStrings
        Dim actual() As String = ColorEnum.Names.ToArray
        Assert.IsTrue((expected.Length = actual.Length AndAlso expected.Intersect(actual).Count() = expected.Length), "Names are equal")
    End Sub

    ''' <summary> (Unit Test Method) tests get names. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub GetNamesTest()
        EnumExtenderTests.GetNamesTestHelper(Of Color)()
    End Sub

    ''' <summary> A test for GetName. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Public Shared Sub GetNameTestHelper(Of T)()
        Dim value As Color = Color.Black
        Dim expected As String = NameOf(Color.Black)
        Dim actual As String = ColorEnum.Name(value)
        Assert.AreEqual(expected, actual, $"Name of {value}")
    End Sub

    ''' <summary> (Unit Test Method) tests get name. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub GetNameTest()
        EnumExtenderTests.GetNameTestHelper(Of Color)()
    End Sub

    ''' <summary> A test for EfficientEnum`1 Constructor. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Public Shared Sub EfficientEnumConstructorTestHelper(Of T)()
        Dim target As EnumExtender(Of T) = New EnumExtender(Of T)
        Dim expectedDesction As String = "Black Color"
        Dim actualDescription As String = target.Description(Color.Black)
        Assert.AreEqual(expectedDesction, actualDescription, $"Description of {Color.Black}")
    End Sub

    ''' <summary> (Unit Test Method) tests efficient enum constructor. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub EfficientEnumConstructorTest()
        EnumExtenderTests.EfficientEnumConstructorTestHelper(Of Color)()
    End Sub
#End Region

End Class
