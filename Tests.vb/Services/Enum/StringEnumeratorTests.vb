Imports isr.Core.Services

''' <summary>
''' This is a test class for StringEnumeratorTest and is intended to contain all
''' StringEnumeratorTest Unit Tests.
''' </summary>
''' <remarks> David, 2020-09-18. </remarks>
<TestClass()>
Public Class StringEnumeratorTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        ' needs to add delay here to prevent an application domain exception.
        isr.Core.Services.My.MyLibrary.DoEvents()
        isr.Core.Services.My.MyLibrary.Delay(200)
        isr.Core.Services.My.MyLibrary.DoEvents()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " STRING ENUMERATOR TESTS "

    ''' <summary> A test for TryParse. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="value">             The value. </param>
    ''' <param name="enumValueExpected"> The enum value expected. </param>
    <CodeAnalysis.SuppressMessage("Performance", "CA1822:Mark members as static", Justification:="<Pending>")>
    Public Sub TryParseTestHelper(Of T As Structure)(ByVal value As String, ByVal enumValueExpected As T)
        Dim target As StringEnumerator(Of T) = New StringEnumerator(Of T)()
        Dim expected As Boolean = True
        Dim actual As Boolean
        Dim enumValue As T = Nothing
        actual = target.TryParse(value, enumValue)
        Assert.AreEqual(enumValueExpected, CType(enumValue, T))
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary>
    ''' (Unit Test Method) attempts to parse a test value test from the given data, returning a
    ''' default value rather than throwing an exception if it fails.
    ''' </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub TryParseTestValueTest()
        Me.TryParseTestHelper("2", TestValue.Error)
    End Sub

    ''' <summary>
    ''' (Unit Test Method) attempts to parse a color test from the given data, returning a default
    ''' value rather than throwing an exception if it fails.
    ''' </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub TryParseColorTest()
        Dim colorStrings() As String = {"0", "2", "8", "blue", "Blue", "Yellow", "Red, Green"}
        Dim colorValues() As Colors = {Colors.None, Colors.Green, Colors.Red Or Colors.Green Or Colors.Blue,
                                           Colors.Blue, Colors.Blue, Colors.None, Colors.Red Or Colors.Green}
        For Each colorString As String In colorStrings
            Dim colorValue As Colors
            Dim actual As Boolean = [Enum].TryParse(colorString, colorValue)
            If actual Then
                If [Enum].IsDefined(GetType(Colors), colorValue) Or colorValue.ToString().Contains(",") Then
                    Trace.Write(String.Format("Converted '{0}' to {1}.", colorString, colorValue.ToString()))
                Else
                    Trace.Write(String.Format("{0} is not an underlying value of the Colors enumeration.", colorString))
                End If
            Else
                Trace.Write(String.Format("{0} is not a member of the Colors enumeration.", colorString))
            End If
        Next
    End Sub

    ''' <summary> Values that represent test values. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    Private Enum TestValue
        ''' <summary> An enum constant representing the none option. </summary>
        None
        ''' <summary> An enum constant representing the Information option. </summary>
        Info
        ''' <summary> An enum constant representing the error] option. </summary>
        [Error]
    End Enum

    ''' <summary> A bit-field of flags for specifying colors. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <Flags()>
    Public Enum Colors As Integer
        ''' <summary> An enum constant representing the none option. </summary>
        None = 0
        ''' <summary> An enum constant representing the red option. </summary>
        Red = 1
        ''' <summary> An enum constant representing the green option. </summary>
        Green = 2
        ''' <summary> An enum constant representing the blue option. </summary>
        Blue = 4
    End Enum
#End Region

End Class
