
'''<summary>
'''This is a test class for KnownFoldersTest and is intended
'''to contain all KnownFoldersTest Unit Tests
'''</summary>
<TestClass()>
Public Class KnownFoldersTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Services.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

    ''' <summary> A test for GetPath. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="knownFolder"> A known folder enum value. </param>
    ''' <param name="expected">    The expected value. </param>
    Public Shared Sub TestGetPath(ByVal knownFolder As KnownFolder, ByVal expected As String)
        Dim actual As String
        actual = KnownFolders.GetPath(knownFolder)
        Assert.AreEqual(expected, actual, True, Globalization.CultureInfo.CurrentCulture)
    End Sub

    ''' <summary> A test for Get Path on multiple <see cref="KnownFolder"/> folders. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub GetPathTest()
        KnownFoldersTests.TestGetPath(KnownFolder.Documents, String.Format("C:\Users\{0}\Documents", Environment.UserName))
        KnownFoldersTests.TestGetPath(KnownFolder.UserProfiles, "C:\Users")
        KnownFoldersTests.TestGetPath(KnownFolder.ProgramData, "C:\ProgramData")
    End Sub

    ''' <summary> A test for Get Default Path. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    ''' <param name="knownFolder"> A known folder enum value. </param>
    ''' <param name="expected">    The expected value. </param>
    Public Shared Sub TestGetDefaultPath(ByVal knownFolder As KnownFolder, ByVal expected As String)
        Dim actual As String
        actual = KnownFolders.GetDefaultPath(knownFolder)
        Assert.AreEqual(expected, actual, True, Globalization.CultureInfo.CurrentCulture)
    End Sub

    ''' <summary> A test for GetDefaultPath. </summary>
    ''' <remarks> David, 2020-09-18. </remarks>
    <TestMethod()>
    Public Sub GetDefaultPathTest()
        KnownFoldersTests.TestGetDefaultPath(KnownFolder.Documents, String.Format("C:\Users\{0}\Documents", Environment.UserName))
        KnownFoldersTests.TestGetDefaultPath(KnownFolder.UserProfiles, "C:\Users")
        KnownFoldersTests.TestGetDefaultPath(KnownFolder.ProgramData, "C:\ProgramData")
    End Sub
End Class
