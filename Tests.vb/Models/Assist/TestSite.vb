''' <summary> A test site class. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/12/2018 </para></remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
    Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0"),
    Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>

''' <summary> A test site. </summary>
''' <remarks> David, 2020-09-22. </remarks>
Friend Class TestSite
    Inherits isr.Core.MSTest.TestSiteBase

End Class

