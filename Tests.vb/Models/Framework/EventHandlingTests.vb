Imports isr.Core.Models

''' <summary> An event handling tests. </summary>
''' <remarks> David, 2020-09-22. </remarks>
'''<summary>
'''This is a test class for event handling
'''</summary>
<TestClass()>
Public Class EventHandlingTests

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Models.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

    #End Region


    #Region " VIEW MODEL PROPERTY CHANGE EVENT HANDLING TESTS "

    ''' <summary> A property change. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Private Class PropertyChange
        Inherits ViewModelBase
        ''' <summary> The expected value. </summary>
        Private _ExpectedValue As String

        ''' <summary> Gets or sets the expected value. </summary>
        ''' <value> The expected value. </value>
        Public Property ExpectedValue As String
            Get
                Return Me._ExpectedValue
            End Get
            Set(value As String)
                If Not String.Equals(value, Me.ExpectedValue) Then
                    Me._ExpectedValue = value
                    Me.SyncNotifyPropertyChanged()
                End If
            End Set
        End Property

        ''' <summary> Gets or sets the actual value. </summary>
        ''' <value> The actual value. </value>
        Public Property ActualValue As String

    End Class

    ''' <summary> Handles the property changed. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Private Sub HandlePropertyChanged(ByVal sender As Object, ByVal e As ComponentModel.PropertyChangedEventArgs)
        Dim propertyChangeSender As PropertyChange = TryCast(sender, PropertyChange)
        If propertyChangeSender Is Nothing Then Return
        Select Case e?.PropertyName
            Case NameOf(PropertyChange.ExpectedValue)
                propertyChangeSender.ActualValue = propertyChangeSender.ExpectedValue
        End Select

    End Sub

    ''' <summary> (Unit Test Method) tests property change event handler. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub PropertyChangeHandlingTest()
        Dim sender As New PropertyChange
        AddHandler sender.PropertyChanged, AddressOf Me.HandlePropertyChanged
        sender.ExpectedValue = NameOf(PropertyChange.ExpectedValue)
        Assert.AreEqual(sender.ExpectedValue, sender.ActualValue, "should equal after handling the synchronized event")
    End Sub

    #End Region

End Class
