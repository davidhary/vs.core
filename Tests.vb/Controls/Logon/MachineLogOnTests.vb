Imports isr.Core.Controls

''' <summary>
''' This is a test class for MachineLogOnTest and is intended to contain all MachineLogOnTest
''' Unit Tests.
''' </summary>
''' <remarks> David, 2020-09-23. </remarks>
<TestClass()>
Public Class MachineLogOnTests

    #Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Controls.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

    #End Region

    ''' <summary> A test for Validate. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub ValidateRole()
        If Not TestInfo.MachineLogonTestsEnabled Then
            TestInfo.VerboseMessage("Machine log-on tests disabled")
            Return
        End If
        Dim userName As String = Core.WindowsForms.EnterInput("Enter Admin user name or empty to cancel this test")
        If String.IsNullOrEmpty(userName) Then Return
        Dim password As String = Core.WindowsForms.EnterInput("Enter Admin password or empty to cancel this test", True)
        If String.IsNullOrEmpty(password) Then Return
        Using target As MachineLogOn = New MachineLogOn()
            Dim roles As New List(Of String) From {"Administrators"}
            Dim allowedUserRoles As New ArrayList(roles)
            Dim expected As Boolean = True
            Dim actual As Boolean = target.Authenticate(userName, password, allowedUserRoles)
            Assert.AreEqual(expected, actual, target.ValidationMessage)
        End Using
    End Sub

    ''' <summary> A test for Validate. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub ValidateUser()
        If Not TestInfo.MachineLogonTestsEnabled Then
            TestInfo.VerboseMessage("Machine log-on tests disabled")
            Return
        End If
        Dim userName As String = Core.WindowsForms.EnterInput("Enter a User name or empty to cancel this test")
        If String.IsNullOrEmpty(userName) Then Return
        Dim password As String = Core.WindowsForms.EnterInput("Enter password or empty to cancel this test", True)
        If String.IsNullOrEmpty(password) Then Return
        Using target As MachineLogOn = New MachineLogOn()
            Dim expected As Boolean = True
            Dim actual As Boolean
            actual = target.Authenticate(userName, password)
            Assert.AreEqual(expected, actual, target.ValidationMessage)
        End Using
    End Sub
End Class
