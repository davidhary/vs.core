''' <summary> A trace event identities. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/17/2018 </para>
''' </remarks>
<TestClass()>
Public Class TraceEventIdentities

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region


#Region " SAVE TRACE EVENT IDENTITIES "

    ''' <summary> Save the trace event identities. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="fileName"> Filename of the file. </param>
    ''' <param name="openMode"> The open mode. </param>
    ''' <param name="values">   The values. </param>
    Private Shared Sub SaveTraceEventIdentities(ByVal fileName As String, ByVal openMode As OpenMode,
                                                ByVal values As IEnumerable(Of KeyValuePair(Of String, Integer)))
        If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
        Dim fileNo As Integer = Microsoft.VisualBasic.FreeFile()
        If openMode = OpenMode.Output AndAlso System.IO.File.Exists(fileName) Then
            System.IO.File.Delete(fileName)
        End If
        Microsoft.VisualBasic.FileOpen(fileNo, fileName, openMode)
        Microsoft.VisualBasic.WriteLine(fileNo, "hex", "decimal", "description")
        For Each value As KeyValuePair(Of String, Integer) In values
            Microsoft.VisualBasic.WriteLine(fileNo, CInt(value.Value).ToString("X"), value.Value.ToString, value.Key)
        Next
        Microsoft.VisualBasic.FileClose(fileNo)
    End Sub

    ''' <summary> (Unit Test Method) enumerates test event identities. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub SaveTestEventIdentities()
        Dim fileName As String = TestInfo.TraceEventProjectIdentitiesFileName
        Dim values As New List(Of KeyValuePair(Of String, Integer))
        For Each value As Global.isr.Core.ProjectTraceEventId In [Enum].GetValues(GetType(ProjectTraceEventId))
            values.Add(New KeyValuePair(Of String, Integer)(isr.Core.EnumExtensions.Methods.Description(value), value))
        Next
        TraceEventIdentities.SaveTraceEventIdentities(fileName, OpenMode.Output, values)
    End Sub

#End Region

End Class
