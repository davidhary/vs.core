Imports isr.Core.VisualBasicLoggingExtensions

''' <summary>
''' This is a test class for VisualBasicLoggingExtensionsTest and is intended to contain all
''' VisualBasicLoggingExtensionsTest Unit Tests.
''' </summary>
''' <remarks> David, 2020-09-23. </remarks>
<TestClass()>
Public Class LoggingExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

    ''' <summary> A test for ReplaceDefaultTraceListener with trace event. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="expected"> The expected. </param>
    <CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1804:RemoveUnusedLocals", MessageId:="value")>
    Public Shared Sub AssertTraceEventLog(ByVal expected As TraceEventType)

        Dim actual As TraceEventType = expected
        Assert.AreEqual(expected, actual)

        Dim value As Logging.FileLogTraceListener = Nothing
        value = My.Application.Log.ReplaceDefaultTraceListener(True)
        If Not My.Application.Log.DefaultFileLogWriterFileExists Then
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Critical, 1, "trace event into {0}", My.Application.Log.DefaultFileLogWriterFilePath)
        End If
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Verbose, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Information, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Warning, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Critical, 1, "setting trace level to {0}", expected)
        My.Application.Log.ApplyTraceLevel(TraceEventType.Information)
        actual = My.Application.Log.TraceLevel
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Verbose, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Information, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Warning, 1, "trace event level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.TraceSource.TraceEvent(TraceEventType.Error, 1, "trace event level {0}", My.Application.Log.TraceLevel)

    End Sub

    ''' <summary> A test for ReplaceDefaultTraceListener with trace event. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub TraceEventLogTestInformation()
        LoggingExtensionsTests.AssertTraceEventLog(TraceEventType.Information)
    End Sub

    ''' <summary> A test for ReplaceDefaultTraceListener with logging log. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="expected"> The expected. </param>
    Public Shared Sub AssertLoggingLog(ByVal expected As TraceEventType)
        Dim actual As TraceEventType = expected
        Assert.AreEqual(expected, actual)
        Dim value As Logging.FileLogTraceListener = Nothing
        value = My.Application.Log.ReplaceDefaultTraceListener(True)
        If Not My.Application.Log.DefaultFileLogWriterFileExists Then
            My.Application.Log.TraceSource.TraceEvent(TraceEventType.Critical, 1, "trace event into {0}", My.Application.Log.DefaultFileLogWriterFilePath)
        End If
        My.Application.Log.WriteLogEntry(TraceEventType.Critical, "Tracing into {0}", value.FullLogFileName)
        My.Application.Log.WriteLogEntry(TraceEventType.Verbose, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Information, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Warning, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Error, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Critical, "setting trace level to {0}", expected)
        actual = My.Application.Log.TraceLevel
        My.Application.Log.WriteLogEntry(TraceEventType.Verbose, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Information, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Warning, "trace level {0}", My.Application.Log.TraceLevel)
        My.Application.Log.WriteLogEntry(TraceEventType.Error, "trace level {0}", My.Application.Log.TraceLevel)
    End Sub

    ''' <summary> A test for ReplaceDefaultTraceListener with logging log. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub LoggingLogTestInformation()
        LoggingExtensionsTests.AssertLoggingLog(TraceEventType.Information)
    End Sub

End Class
