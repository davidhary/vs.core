
''' <summary> A machine information tests. </summary>
''' <remarks> David, 2020-09-23. </remarks>
<TestClass()>
Public Class MachineInfoTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

    ''' <summary> A test for Machine ID Using insecure MD5 algorithm,. </summary>
    ''' <remarks> Causes application domain exception with test agent. </remarks>
    <TestMethod()>
    Public Sub MachineUniqueIdMD5Test()
        Dim expected As String = "7ae637a47a35f672ed013462c7b92649"
        Select Case True
            Case String.Equals(My.Computer.Name, "LimeDev", StringComparison.OrdinalIgnoreCase)
                expected = "f77145622be6795484b53c570731a5fc"
            Case String.Equals(My.Computer.Name, "Fig10Dev", StringComparison.OrdinalIgnoreCase)
                expected = "7ae637a47a35f672ed013462c7b92649"
        End Select
        ' on FIG10Dev
        Dim actual As String = MachineInfo.BuildMachineUniqueIdMD5
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) machine unique identifier SHA 1. </summary>
    ''' <remarks> Causes application domain exception with test agent. </remarks>
    <TestMethod()>
    Public Sub MachineUniqueIdSha1()
        Dim expected As String = "7ae637a47a35f672ed013462c7b92649"
        Select Case True
            Case String.Equals(My.Computer.Name, "LimeDev", StringComparison.OrdinalIgnoreCase)
                expected = "e8744184d701a53a060f1502d40e2a8e09a61a31"
            Case String.Equals(My.Computer.Name, "Fig10Dev", StringComparison.OrdinalIgnoreCase)
                expected = "85f9c966b0a3f128c41186a0160aa0c9cce68e65"
        End Select
        Dim actual As String = MachineInfo.BuildMachineUniqueIdSha1
        Assert.AreEqual(expected, actual)
    End Sub

End Class
