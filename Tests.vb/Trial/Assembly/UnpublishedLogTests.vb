''' <summary> This is a test class for unpublished messages. </summary>
''' <remarks> David, 2020-09-23. </remarks>
<TestClass()>
Public Class UnpublishedLogTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

    ''' <summary> Tests logging unpublished messages. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="expected">      The expected. </param>
    ''' <param name="checkFileSize"> True to check file size. </param>
    Public Shared Sub TestLoggingUnpublishedMessages(ByVal expected As TraceEventType, ByVal checkFileSize As Boolean)

        Dim payload As String = "Message 1"
        Dim expectedMessage As New TraceMessage(expected, isr.Core.My.MyLibrary.TraceEventId, payload)

        Dim initialLogFileSize As Long = isr.Core.My.MyLibrary.Logger.FileSize

        Dim actualLogFileName As String = isr.Core.My.MyLibrary.Logger.FullLogFileName
        Assert.IsFalse(String.IsNullOrWhiteSpace(actualLogFileName), $"Tests if log file name '{actualLogFileName}' is empty")

        ' clear trace message log
        isr.Core.My.MyLibrary.UnpublishedTraceMessages.DequeueContent()

        ' check content
        Dim actualContents As String = isr.Core.My.MyLibrary.UnpublishedTraceMessages.DequeueContent()
        Assert.IsTrue(String.IsNullOrWhiteSpace(actualContents), "Unpublished messages should clear")

        Dim expectedUnpublishedMessageCount As Integer = 0
        Dim actualUnpublicMessageCount As Integer = 0
        Assert.AreEqual(expectedUnpublishedMessageCount, actualUnpublicMessageCount, $"Initial unpublished message count should match")

        isr.Core.My.MyLibrary.Logger.DefaultFileLogWriter.AutoFlush = False

        ' log an unpublished trace message to the library.
        isr.Core.My.MyLibrary.LogUnpublishedMessage(expectedMessage)

        expectedUnpublishedMessageCount += 1
        actualUnpublicMessageCount = isr.Core.My.MyLibrary.UnpublishedTraceMessages.Count
        Assert.AreEqual(expectedUnpublishedMessageCount, actualUnpublicMessageCount, $"Unpublished message count should match")

        Dim actualMessage As TraceMessage = isr.Core.My.MyLibrary.UnpublishedTraceMessages.TryPeek
        Assert.AreEqual(expectedMessage.Id, actualMessage.Id, " Message trace events identities match")

        isr.Core.My.MyLibrary.LogUnpublishedMessage(expectedMessage)

        If checkFileSize Then
            ' this no longer works. The log does not show the new messages until after the program closes.
            isr.Core.My.MyLibrary.Logger.FlushMessages()
            isr.Core.My.MyLibrary.DoEvents()
            isr.Core.My.MyLibrary.Delay(50)
            isr.Core.My.MyLibrary.DoEvents()
            isr.Core.My.MyLibrary.Logger.DefaultFileLogWriter.Flush()
            isr.Core.My.MyLibrary.Delay(50)
            isr.Core.My.MyLibrary.DoEvents()
            Dim newLogFileSize As Long = isr.Core.My.MyLibrary.Logger.FileSize
            Dim endTime As DateTime = Date.Now.Add(TimeSpan.FromMilliseconds(200))
            Do Until newLogFileSize > initialLogFileSize OrElse Date.Now > endTime
                isr.Core.My.MyLibrary.Delay(50)
                isr.Core.My.MyLibrary.DoEvents()
                isr.Core.My.MyLibrary.Logger.DefaultFileLogWriter.Flush()
                isr.Core.My.MyLibrary.Delay(50)
                isr.Core.My.MyLibrary.DoEvents()
                newLogFileSize = isr.Core.My.MyLibrary.Logger.FileSize
            Loop
            Assert.IsTrue(newLogFileSize > initialLogFileSize, $"New file Size {newLogFileSize} > initial file size {initialLogFileSize}")
        End If

    End Sub

    ''' <summary> (Unit Test Method) tests log unpublished messages. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub LogUnpublishedMessagesTest()
        UnpublishedLogTests.TestLoggingUnpublishedMessages(TraceEventType.Warning, My.Settings.CheckUnpublishedMessageLogFileSize)
    End Sub

End Class
