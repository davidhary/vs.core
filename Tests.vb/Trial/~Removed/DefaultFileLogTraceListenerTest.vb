﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports isr.Sre.Applications



'''<summary>
'''This is a test class for DefaultFileLogTraceListenerTest and is intended
'''to contain all DefaultFileLogTraceListenerTest Unit Tests
'''</summary>
<TestClass()> 
Public Class DefaultFileLogTraceListenerTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize(), CLSCompliant(False)>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

#If False Then
        Public Shared Sub TestProcessException()
        Try
            Throw New ArgumentException()
        Catch ex As Exception
            If My.MyApplication.ProcessException(Nothing, ex) = System.Windows.Forms.DialogResult.Abort Then
                ' exit with an error code
                Environment.Exit(-1)
                Application.Exit()
            End If
            Dim result As Windows.Forms.DialogResult = Windows.Forms.DialogResult.None
            result = My.MyApplication.ProcessException(Nothing, ex, Microsoft.SqlServer.MessageBox.ExceptionMessageBoxButtons.AbortRetryIgnore,
                                                       Microsoft.SqlServer.MessageBox.ExceptionMessageBoxSymbol.Error,
                                                       Microsoft.SqlServer.MessageBox.ExceptionMessageBoxDefaultButton.Button1,
                                                       "Exception occurred starting this application")
            If result = DialogResult.Abort Then
                ' exit with an error code
                Environment.Exit(-1)
                Windows.Forms.Application.Exit()
            End If

        End Try

    End Sub
#End If

    '''<summary>
    '''A test for TestProcessException
    '''</summary>
    <TestMethod()> 
    Public Sub TestProcessExceptionTest()
        '   DefaultFileLogTraceListener.TestProcessException()
    End Sub

End Class
