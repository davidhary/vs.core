﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.Forma.ParseExtensions
'''<summary>
'''This is a test class for ConvertToTest and is intended
'''to contain all ConvertToTest Unit Tests
'''</summary>
<TestClass()>
Public Class ConvertToTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(ByVal value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize(), CLSCompliant(False)>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary>
    '''A test for ConvertTo
    '''</summary>
    Public Shared Sub ConvertToTest3Helper(Of T)(ByVal value As String, ByVal expected As T, ByVal culture As String)
        Dim actual As T
        actual = ConvertTo(Of T)(value, culture)
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()>
    Public Sub ConvertToTest3()
        ConvertToTest.ConvertToTest3Helper(Of Boolean)("true", True, "EN-US")
    End Sub

    '''<summary>
    '''A test for ConvertTo
    '''</summary>
    Public Shared Sub ConvertToTest2Helper(Of T)(ByVal value As String, ByVal expected As T, ByVal defaultValue As T)
        Dim actual As T
        actual = ConvertTo(Of T)(value, defaultValue)
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()>
    Public Sub ConvertToTest2()
        ConvertToTest.ConvertToTest2Helper(Of Boolean)("true", True, False)
    End Sub

    '''<summary>
    '''A test for ConvertTo
    '''</summary>
    Public Shared Sub ConvertToTest1Helper(Of T)(ByVal value As String, ByVal expected As T, ByVal culture As String, ByVal defaultValue As T)
        Dim actual As T
        actual = ConvertTo(Of T)(value, culture, defaultValue)
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()>
    Public Sub ConvertToTest1()
        ConvertToTest.ConvertToTest1Helper(Of Boolean)("true", True, "EN-US", False)
    End Sub

    '''<summary>
    '''A test for ConvertTo
    '''</summary>
    Public Shared Sub ConvertToTestHelper(Of T)(ByVal value As String, ByVal expected As T)
        Dim actual As T
        actual = value.ConvertTo(Of T)()
        Assert.AreEqual(expected, actual)
    End Sub

    <TestMethod()>
    Public Sub ConvertToTestBoolean()
        ConvertToTest.ConvertToTestHelper(Of Boolean)("true", True)
    End Sub

    <TestMethod()>
    Public Sub ConvertToTestByte()
        ConvertToTest.ConvertToTestHelper(Of Byte)("&h12", 18)
    End Sub

    <TestMethod()>
    Public Sub ConvertToTestInteger()
        ConvertToTestHelper(Of Integer)("&h12", 18)
    End Sub

    <TestMethod()>
    Public Sub ConvertToTestShort()
        ConvertToTest.ConvertToTestHelper(Of Short)("&h12", 18)
    End Sub

    <TestMethod()>
    Public Sub ConvertToTestLong()
        ConvertToTest.ConvertToTestHelper(Of Long)("0x12", 18)
    End Sub

End Class

