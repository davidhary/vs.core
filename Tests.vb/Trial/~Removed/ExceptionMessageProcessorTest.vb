﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports isr.Sre.Applications



'''<summary>
'''This is a test class for DefaultFileLogTraceListenerTest and is intended
'''to contain all DefaultFileLogTraceListenerTest Unit Tests
'''</summary>
<TestClass()> 
Public Class ExceptionMessageProcessorTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize(), CLSCompliant(False)>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary>
    '''A test for TestProcessException
    '''</summary>
    <TestMethod()> 
    Public Sub TestProcessExceptionTest()
        ExceptionMessageProcessorTester.TestProcessException()
    End Sub

    '''<summary>
    '''A test for TestProcessExceptionThread
    '''</summary>
    <TestMethod()> 
    Public Sub TestProcessExceptionThreadTest()
        ExceptionMessageProcessorTester.TestProcessExceptionThread()
    End Sub

End Class
