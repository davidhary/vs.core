﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting

Imports isr.Sre



'''<summary>
'''This is a test class for ApplicationInfoTest and is intended
'''to contain all ApplicationInfoTest Unit Tests
'''</summary>
<TestClass()> 
Public Class ApplicationInfoTest


    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize(), CLSCompliant(False)>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary>
    '''A test for DropTrailingElements
    '''</summary>
    <TestMethod()> 
    Public Sub DropTrailingElementsTest()
        Dim value As String = "Sre.Gle.Indicator.Installer-6.1.4795.123"
        Dim delimiter As Char = "."c
        Dim count As Integer = 2
        Dim expected As String = "Sre.Gle.Indicator.Installer-6.1"
        Dim actual As String
        actual = ApplicationInfo.DropTrailingElements(value, delimiter, count)
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for BuildAllUsersConfigFilePath
    '''</summary>
    <TestMethod()>
    Public Sub BuildAllUsersConfigFilePathTest()
        Dim extension As String = ".INI"
        Dim significantVersionElements As Integer = 2
        Dim expected As String = "C:\ProgramData\Microsoft Corporation\Microsoft® Visual Studio® 2010\10.0.0.0\isr.SreGle.exe.INI"
        Dim actual As String
        actual = ApplicationInfo.BuildApplicationConfigFilePath(extension, significantVersionElements, True)
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for BuildApplicationConfigFileName
    '''</summary>
    <TestMethod()> 
    Public Sub BuildApplicationConfigFileNameTest()
        Dim extension As String = ".INI"
        Dim expected As String = "isr.SreGle.exe.INI"
        Dim actual As String
        actual = ApplicationInfo.BuildApplicationConfigFileName(extension)
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for BuildCurrentUserConfigFilePath
    '''</summary>
    <TestMethod()>
    Public Sub BuildCurrentUserConfigFilePathTest()
        Dim extension As String = ".INI"
        Dim significantVersionElements As Integer = 2
        Dim expected As String = "C:\Users\David\AppData\Roaming\Microsoft Corporation\Microsoft® Visual Studio® 2010\10.0.0.0\isr.SreGle.exe.INI"
        Dim actual As String
        actual = ApplicationInfo.BuildApplicationConfigFilePath(extension, significantVersionElements, False)
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for ReplaceTrailingElements
    '''</summary>
    <TestMethod()> 
    Public Sub ReplaceTrailingElementsTest()
        Dim value As String = "Sre.Gle.Indicator.Installer-6.1.4795.123"
        Dim delimiter As Char = "."c
        Dim count As Integer = 2
        Dim replace As String = "0"
        Dim expected As String = "Sre.Gle.Indicator.Installer-6.1.0.0"
        Dim actual As String
        actual = ApplicationInfo.ReplaceTrailingElements(value, delimiter, count, replace)
        Assert.AreEqual(expected, actual)
    End Sub

    '''<summary>
    '''A test for ReplaceTrailingElements
    '''</summary>
    <TestMethod()> 
    Public Sub ReplaceTrailingElementsTestNone()
        Dim value As String = "Sre.Gle.Indicator.Installer-6.1.4795.123"
        Dim delimiter As Char = "."c
        Dim count As Integer = 0
        Dim replace As String = "0"
        Dim expected As String = "Sre.Gle.Indicator.Installer-6.1.4795.123"
        Dim actual As String
        actual = ApplicationInfo.ReplaceTrailingElements(value, delimiter, count, replace)
        Assert.AreEqual(expected, actual)
    End Sub

End Class
