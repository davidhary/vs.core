﻿Imports Microsoft.VisualStudio.TestTools.UnitTesting
Imports isr.Core.AOP

Public Class ExtensionTestTarget

    Public Sub New()
        MyBase.New()
        newPropertyValue2 = String.Empty
    End Sub

    Private newPropertyValue As String
    <ComponentModel.DefaultValue("New Property")>
    Public Property NewProperty() As String
        Get
            Return newPropertyValue
        End Get
        Set(ByVal value As String)
            newPropertyValue = value
        End Set
    End Property

    Private newPropertyValue1 As String
    <ComponentModel.DefaultValue("New Property 1")>
    Public Property NewProperty1() As String
        Get
            Return newPropertyValue1
        End Get
        Set(ByVal value As String)
            newPropertyValue1 = value
        End Set
    End Property

    Private newPropertyValue2 As String
    Public Property NewProperty2() As String
        Get
            Return newPropertyValue2
        End Get
        Set(ByVal value As String)
            newPropertyValue2 = value
        End Set
    End Property

End Class

Public Class SelectInitTestTarget

    Public Sub New()
        MyBase.New()
        newPropertyValue2 = String.Empty
        Me.ApplyDefaultValues()
    End Sub

    Private newPropertyValue As String
    <ComponentModel.DefaultValue("New Property")> 
    Public Property NewProperty() As String
        Get
            Return newPropertyValue
        End Get
        Set(ByVal value As String)
            newPropertyValue = value
        End Set
    End Property

    Private newPropertyValue1 As String
    <ComponentModel.DefaultValue("New Property 1")> 
    Public Property NewProperty1() As String
        Get
            Return newPropertyValue1
        End Get
        Set(ByVal value As String)
            newPropertyValue1 = value
        End Set
    End Property

    Private newPropertyValue2 As String
    Public Property NewProperty2() As String
        Get
            Return newPropertyValue2
        End Get
        Set(ByVal value As String)
            newPropertyValue2 = value
        End Set
    End Property

End Class

'''<summary>
'''This is a test class for ExtensionsTest and is intended
'''to contain all ExtensionsTest Unit Tests
'''</summary>
<TestClass()> 
Public Class AopExtensionsTest

    Private testContextInstance As TestContext

    '''<summary>
    '''Gets or sets the test context which provides
    '''information about and functionality for the current test run.
    '''</summary>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
        Get
            Return testContextInstance
        End Get
        Set(value As TestContext)
            testContextInstance = Value
        End Set
    End Property

#Region "Additional test attributes"
    '
    'You can use the following additional attributes as you write your tests:
    '
    'Use ClassInitialize to run code before running the first test in the class
    '<ClassInitialize(), CLSCompliant(False)>  
    'Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
    'End Sub
    '
    'Use ClassCleanup to run code after all tests in a class have run
    '<ClassCleanup()>  
    'Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    'End Sub
    '
    'Use TestInitialize to run code before running each test
    '<TestInitialize()>  
    'Public Sub MyTestInitialize()
    'End Sub
    '
    'Use TestCleanup to run code after each test has run
    '<TestCleanup()>  
    'Public Sub MyTestCleanup()
    'End Sub
    '
#End Region

    '''<summary>
    '''A test for ApplyDefaultValues
    '''</summary>
    <TestMethod()> 
    Public Sub ApplyDefaultValuesTest()
        Dim target As ExtensionTestTarget = New ExtensionTestTarget
        Extensions.ApplyDefaultValues(target)
        Assert.AreEqual(target.NewProperty, "New Property")
        Assert.AreEqual(target.NewProperty1, "New Property 1")
        Assert.AreEqual(target.NewProperty2, "")
    End Sub

    '''<summary>
    '''A test for ResetDefaultValues
    '''</summary>
    <TestMethod()> 
    Public Sub ResetDefaultValuesTest()
        Dim target As ExtensionTestTarget = New ExtensionTestTarget
        Extensions.ApplyDefaultValues(target)
        Assert.AreEqual(target.NewProperty, "New Property")
        Assert.AreEqual(target.NewProperty1, "New Property 1")
        Assert.AreEqual(target.NewProperty2, "")
        target.NewProperty = "0"
        target.NewProperty1 = "1"
        target.NewProperty2 = "2"
        Extensions.ResetDefaultValues(target)
        Assert.AreEqual(target.NewProperty, "New Property")
        Assert.AreEqual(target.NewProperty1, "New Property 1")
        Assert.AreEqual(target.NewProperty2, "2")
    End Sub

    '''<summary>
    '''A test for ApplyDefaultValues with self initialization.
    '''</summary>
    <TestMethod()> 
    Public Sub ApplyDefaultValuesTestSelectInit()
        Dim target As SelectInitTestTarget = New SelectInitTestTarget
        Assert.AreEqual(target.NewProperty, "New Property")
        Assert.AreEqual(target.NewProperty1, "New Property 1")
        Assert.AreEqual(target.NewProperty2, "")
        target.NewProperty = "0"
        target.NewProperty1 = "1"
        target.NewProperty2 = "2"
        Extensions.ResetDefaultValues(target)
        Assert.AreEqual(target.NewProperty, "New Property")
        Assert.AreEqual(target.NewProperty1, "New Property 1")
        Assert.AreEqual(target.NewProperty2, "2")
    End Sub

End Class
