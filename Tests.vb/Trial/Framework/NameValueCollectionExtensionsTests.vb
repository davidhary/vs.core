Imports System.Collections.Specialized

Imports isr.Core.CollectionExtensions
Imports isr.Core.MSTest

''' <summary> A name value collection extensions tests. </summary>
''' <remarks> David, 2020-09-23. </remarks>
Public Class NameValueCollectionExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region
    ''' <summary> The collection. </summary>
    Private Shared ReadOnly Collection As NameValueCollection = New NameValueCollection From {{"ValidEntry", "ValidEntry"}}

    ''' <summary> (Unit Test Method) does not throw exception when key does not exist. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub DoesNotThrowExceptionWhenKeyDoesNotExist()
        Assert.Equals(Collection.GetValueAs("InvalidEntry", "DefaultValue"), "DefaultValue")
    End Sub

    ''' <summary>
    ''' (Unit Test Method) queries if a given does not throw exception when key exists.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub DoesNotThrowExceptionWhenKeyExists()
        Assert.Equals(Collection.GetValueAs(Of String)("ValidEntry"), "ValidEntry")
    End Sub

    ''' <summary> (Unit Test Method) throws exception when key is null or white space. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    <TestMethod()>
    Public Sub ThrowsExceptionWhenKeyIsNullOrWhiteSpace()
        Try
            Collection.GetValueAs(Of String)(Nothing)
        Catch ex As ArgumentException
        Catch
            Assert.Fail("expected exception was not thrown")
        End Try
        Try
            Collection.GetValueAs(Of String)(Nothing)

        Catch ex As ArgumentException
        Catch
            Assert.Fail("expected exception was not thrown")
        End Try
        Asserts.Instance.Throws(Of ArgumentException)(Function() Collection.GetValueAs(Of String)(" "))
        Asserts.Instance.Throws(Of ArgumentException)(Function() Collection.GetValueAs(Of String)(vbTab))
        Asserts.Instance.Throws(Of ArgumentException)(Function() Collection.GetValueAs(Nothing, String.Empty))
        Asserts.Instance.Throws(Of ArgumentException)(Function() Collection.GetValueAs(" ", String.Empty))
        Asserts.Instance.Throws(Of ArgumentException)(Function() Collection.GetValueAs(vbTab, String.Empty))
    End Sub

End Class
