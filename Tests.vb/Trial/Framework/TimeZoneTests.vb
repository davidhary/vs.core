''' <summary> A time zone tests. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/14/2018 </para>
''' </remarks>
<TestClass()>
Public Class TimeZoneTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " TIME ZONE TESTS "

    ''' <summary> Query if 'timeZone' is daylight saving time. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="timeZone">  The time zone. </param>
    ''' <param name="dateValue"> The date value. </param>
    ''' <returns> <c>true</c> if daylight saving time; otherwise <c>false</c> </returns>
    Public Overloads Shared Function IsDaylightSavingTime(ByVal timeZone As String, dateValue As String) As Boolean
        Return TimeZoneInfo.FindSystemTimeZoneById(timeZone).IsDaylightSavingTime(DateTimeOffset.Parse(dateValue))
    End Function

    ''' <summary> (Unit Test Method) tests arizona time zone. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub ArizonaTimeZoneTest()
        Dim dayString As String = "1/1/2017"
        Dim timeZone As String = "US Mountain Standard Time"
        Dim expected As Boolean = False
        Dim actual As Boolean = TimeZoneTests.IsDaylightSavingTime(timeZone, dayString)
        Assert.AreEqual(expected, actual)
        dayString = "6/1/2017"
        actual = TimeZoneTests.IsDaylightSavingTime(timeZone, dayString)
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) tests pacific time zone. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub PacificTimeZoneTest()
        Dim dayString As String = "1/1/2017"
        Dim timeZone As String = "Pacific Standard Time"
        Dim expected As Boolean = False
        Dim actual As Boolean = TimeZoneTests.IsDaylightSavingTime(timeZone, dayString)
        Assert.AreEqual(expected, actual)
        dayString = "6/1/2017"
        expected = True
        actual = TimeZoneTests.IsDaylightSavingTime(timeZone, dayString)
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) tests get arizona time zone. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub GetArizonaTimeZoneTest()
        Dim expected As String = "US Mountain Standard Time"
        Dim actual As String = TimeZoneInfo.FindSystemTimeZoneById(expected).Id
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) tests local pacific standard time. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub LocalPacificStandardTimeTest()
        Dim tz As TimeZoneInfo = TimeZoneInfo.Local
        Dim expected As String = TestInfo.TimeZone
        Dim actual As String = tz.Id
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> Query if 'timeZone' is daylight saving time. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="dateValue"> The date value. </param>
    ''' <returns> <c>true</c> if daylight saving time; otherwise <c>false</c> </returns>
    Public Overloads Shared Function IsDaylightSavingTime(ByVal dateValue As String) As Boolean
        Dim tz As TimeZoneInfo = TimeZoneInfo.Local
        Return tz.IsDaylightSavingTime(DateTimeOffset.Parse(dateValue))
    End Function

    ''' <summary> Query if 'timeZone' is daylight saving time. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> <c>true</c> if daylight saving time; otherwise <c>false</c> </returns>
    Public Overloads Shared Function IsDaylightSavingTime() As Boolean
        Dim tz As TimeZoneInfo = TimeZoneInfo.Local
        Return tz.IsDaylightSavingTime(DateTimeOffset.Now)
    End Function

    ''' <summary> (Unit Test Method) tests daylight savings time affirmative. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub DaylightSavingsTimeAffirmativeTest()
        Dim expectedBoolean As Boolean = False
        Dim actualBoolean As Boolean = TimeZoneTests.IsDaylightSavingTime("1/1/2017")
        Assert.AreEqual(expectedBoolean, actualBoolean)
    End Sub

    ''' <summary> (Unit Test Method) tests daylight savings time negative. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub DaylightSavingsTimeNegativeTest()
        Dim expectedBoolean As Boolean = True
        Dim actualBoolean As Boolean = TimeZoneTests.IsDaylightSavingTime("6/1/2017")
        Assert.AreEqual(expectedBoolean, actualBoolean)
    End Sub

    ''' <summary> (Unit Test Method) tests UTC time offset. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub UtcTimeOffsetTest()
        Dim expected As TimeSpan = TimeSpan.FromHours(If(TimeZoneTests.IsDaylightSavingTime(), TestInfo.TimeZoneOffset + 1, TestInfo.TimeZoneOffset))
        Dim actual As TimeSpan = DateTimeOffset.Now.Offset
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) tests UTC time conversion. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub UtcTimeConversionTest()
        Dim timeNow As DateTime = DateTime.Now
        Dim expected As DateTime = timeNow.ToUniversalTime
        Dim actual As DateTime = timeNow.Subtract(DateTimeOffset.Now.Offset)
        Assert.AreEqual(expected, actual)
    End Sub

    ''' <summary> (Unit Test Method) tests UTC time. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub UtcTimeTest()
        Dim timeNow As DateTime = DateTime.Now
        Dim expected As DateTime = timeNow.ToUniversalTime
        Dim actual As DateTime = timeNow.Subtract(DateTimeOffset.Parse(timeNow.Date.ToShortDateString).Offset)
        Assert.AreEqual(expected, actual)
    End Sub

#End Region

End Class
