Imports System.Collections.ObjectModel

''' <summary> Tests for using Enumerable tests. </summary>
''' <remarks>
''' (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 09/021/2020 </para>
''' </remarks>
<TestClass()>
Public Class EnumerableTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " ENUMERABLE TESTS "

    ''' <summary> Iterate array. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="objectName"> Name of the object. </param>
    ''' <param name="a">          An IEnumerable(OfDouble) to process. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function IterateArray(ByVal objectName As String, ByVal a As Double()) As TimeSpan
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim v As Double
        For i As Integer = 0 To a.Length - 1
            v = a(i)
        Next
        Dim elapsed As TimeSpan = sw.Elapsed
        _TestSite.TraceMessage($"Iterate {objectName}(Of Double)[{a.Length}] cast as Array in {elapsed.TotalMilliseconds}ms")
        a(0) = v
        Return elapsed
    End Function

    ''' <summary> For each array. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="objectName"> Name of the object. </param>
    ''' <param name="a">          An IEnumerable(OfDouble) to process. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function ForEachArray(ByVal objectName As String, ByVal a As Double()) As TimeSpan
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim v As Double
        For Each item As Double In a
            v = item
        Next
        Dim elapsed As TimeSpan = sw.Elapsed
        _TestSite.TraceMessage($"For Each {objectName}(Of Double)[{a.Length}] cast as Array in {elapsed.TotalMilliseconds}ms")
        a(0) = v
        Return elapsed
    End Function

    ''' <summary> Populates from array. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="objectName"> Name of the object. </param>
    ''' <param name="a">          An IEnumerable(OfDouble) to process. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function PopulateFromArray(ByVal objectName As String, ByVal a As Double()) As TimeSpan
        Dim col As New Collection(Of Double)
        Dim sw As Stopwatch = Stopwatch.StartNew
        For Each item As Double In a
            col.Add(item)
        Next
        Dim elapsed As TimeSpan = sw.Elapsed
        _TestSite.TraceMessage($"Populate Collection {objectName}(Of Double)[{a.Length}] cast as Array in {elapsed.TotalMilliseconds}ms")
        Return elapsed
    End Function

    ''' <summary> Iterate i list. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="objectName"> Name of the object. </param>
    ''' <param name="a">          An IEnumerable(OfDouble) to process. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function IterateIList(ByVal objectName As String, ByVal a As IList(Of Double)) As TimeSpan
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim v As Double
        For i As Integer = 0 To a.Count - 1
            v = a(i)
        Next
        Dim elapsed As TimeSpan = sw.Elapsed
        _TestSite.TraceMessage($"Iterate {objectName}(Of Double)[{a.Count}] cast as IList in {elapsed.TotalMilliseconds}ms")
        a(0) = 0
        ' collection is read only: a(0) = 0
        Return elapsed
    End Function

    ''' <summary> For each i list. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="objectName"> Name of the object. </param>
    ''' <param name="a">          An IEnumerable(OfDouble) to process. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function ForEachIList(ByVal objectName As String, ByVal a As IList(Of Double)) As TimeSpan
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim v As Double
        For Each item As Double In a
            v = item
        Next
        Dim elapsed As TimeSpan = sw.Elapsed
        _TestSite.TraceMessage($"For Each {objectName}(Of Double)[{a.Count}] cast as IList in {elapsed.TotalMilliseconds}ms")
        ' collection is read only: a(0) = 0
        Return elapsed
    End Function

    ''' <summary> Populates from i list. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="objectName"> Name of the object. </param>
    ''' <param name="a">          An IEnumerable(OfDouble) to process. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function PopulateFromIList(ByVal objectName As String, ByVal a As IList(Of Double)) As TimeSpan
        Dim col As New Collection(Of Double)
        Dim sw As Stopwatch = Stopwatch.StartNew
        For Each item As Double In a
            col.Add(item)
        Next
        Dim elapsed As TimeSpan = sw.Elapsed
        _TestSite.TraceMessage($"Populate Collection {objectName}(Of Double)[{a.Count}] cast as IList in {elapsed.TotalMilliseconds}ms")
        Return elapsed
    End Function

    ''' <summary> Iterate i enumerable. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="objectName"> Name of the object. </param>
    ''' <param name="a">          An IEnumerable(OfDouble) to process. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function IterateIEnumerable(ByVal objectName As String, ByVal a As IEnumerable(Of Double)) As TimeSpan
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim v As Double
        For i As Integer = 0 To a.Count - 1
            v = a(i)
        Next
        Dim elapsed As TimeSpan = sw.Elapsed
        _TestSite.TraceMessage($"Iterate {objectName}(Of Double)[{a.Count}] cast as IEnumerable in {elapsed.TotalMilliseconds}ms")
        Return elapsed
    End Function

    ''' <summary> For each i enumerable. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="objectName"> Name of the object. </param>
    ''' <param name="a">          An IEnumerable(OfDouble) to process. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function ForEachIEnumerable(ByVal objectName As String, ByVal a As IEnumerable(Of Double)) As TimeSpan
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim v As Double
        For Each item As Double In a
            v = item
        Next
        Dim elapsed As TimeSpan = sw.Elapsed
        _TestSite.TraceMessage($"For Each {objectName}(Of Double)[{a.Count}] cast as IEnumerable in {elapsed.TotalMilliseconds}ms")
        Return elapsed
    End Function

    ''' <summary> Populates from i enumerable. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="objectName"> Name of the object. </param>
    ''' <param name="a">          An IEnumerable(OfDouble) to process. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function PopulateFromIEnumerable(ByVal objectName As String, ByVal a As IEnumerable(Of Double)) As TimeSpan
        Dim col As New Collection(Of Double)
        Dim sw As Stopwatch = Stopwatch.StartNew
        For Each item As Double In a
            col.Add(item)
        Next
        Dim elapsed As TimeSpan = sw.Elapsed
        _TestSite.TraceMessage($"Populate Collection {objectName}(Of Double)[{a.Count}] cast as IEnumerable in {elapsed.TotalMilliseconds}ms")
        Return elapsed
    End Function

    ''' <summary> Iterate i collection. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="objectName"> Name of the object. </param>
    ''' <param name="a">          An IEnumerable(OfDouble) to process. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function IterateICollection(ByVal objectName As String, ByVal a As ICollection(Of Double)) As TimeSpan
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim v As Double
        For i As Integer = 0 To a.Count - 1
            v = a(i)
        Next
        Dim elapsed As TimeSpan = sw.Elapsed
        _TestSite.TraceMessage($"Iterate {objectName}(Of Double)[{a.Count}] cast as ICollection in {elapsed.TotalMilliseconds}ms")
        Return elapsed
    End Function

    ''' <summary> For each i collection. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="objectName"> Name of the object. </param>
    ''' <param name="a">          An IEnumerable(OfDouble) to process. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function ForEachICollection(ByVal objectName As String, ByVal a As ICollection(Of Double)) As TimeSpan
        Dim sw As Stopwatch = Stopwatch.StartNew
        Dim v As Double
        For Each item As Double In a
            v = item
        Next
        Dim elapsed As TimeSpan = sw.Elapsed
        _TestSite.TraceMessage($"ForEach {objectName}(Of Double)[{a.Count}] cast as ICollection in {elapsed.TotalMilliseconds}ms")
        Return elapsed
    End Function

    ''' <summary> Populates from i collection. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="objectName"> Name of the object. </param>
    ''' <param name="a">          An IEnumerable(OfDouble) to process. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Shared Function PopulateFromICollection(ByVal objectName As String, ByVal a As ICollection(Of Double)) As TimeSpan
        Dim col As New Collection(Of Double)
        Dim sw As Stopwatch = Stopwatch.StartNew
        For Each item As Double In a
            col.Add(item)
        Next
        Dim elapsed As TimeSpan = sw.Elapsed
        ' expression is a value and cannot be a target of assignment: a(0) = 0
        _TestSite.TraceMessage($"Populate Collection {objectName}(Of Double)[{a.Count}] cast as ICollection in {elapsed.TotalMilliseconds}ms")
        Return elapsed
    End Function

    ''' <summary> Enumerates array as i enumerable in this collection. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="a"> An IEnumerable(OfDouble) to process. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process array as i enumerable in this
    ''' collection.
    ''' </returns>
    Public Shared Function ArrayAsIEnumerable(ByVal a As IEnumerable(Of Double)) As IEnumerable(Of Double)
        Dim col As New Collection(Of Double)
        Dim sw As Stopwatch = Stopwatch.StartNew
        For Each item As Double In a
            col.Add(item)
        Next
        Return col.ToArray
    End Function

    ''' <summary> Array as i list. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="a"> An IEnumerable(OfDouble) to process. </param>
    ''' <returns> A list of. </returns>
    Public Shared Function ArrayAsIList(ByVal a As IEnumerable(Of Double)) As IList(Of Double)
        Dim col As New Collection(Of Double)
        Dim sw As Stopwatch = Stopwatch.StartNew
        For Each item As Double In a
            col.Add(item)
        Next
        Return col.ToArray
    End Function

    ''' <summary> List as i list. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="a"> An IEnumerable(OfDouble) to process. </param>
    ''' <returns> A list of. </returns>
    Public Shared Function ListAsIList(ByVal a As IEnumerable(Of Double)) As IList(Of Double)
        Dim l As New List(Of Double)
        Dim sw As Stopwatch = Stopwatch.StartNew
        For Each item As Double In a
            l.Add(item)
        Next
        Return l
    End Function

    ''' <summary> Lists as i enumerables in this collection. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="a"> An IEnumerable(OfDouble) to process. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process as i enumerables in this collection.
    ''' </returns>
    Public Shared Function ListAsIEnumerable(ByVal a As IEnumerable(Of Double)) As IEnumerable(Of Double)
        Dim l As New List(Of Double)
        Dim sw As Stopwatch = Stopwatch.StartNew
        For Each item As Double In a
            l.Add(item)
        Next
        Return l
    End Function

    ''' <summary> Executes the tests. </summary>
    ''' <remarks> David, 9/7/2020. </remarks>
    ''' <param name="testLevel"> The test level. </param>
    Private Shared Sub RunTests(ByVal testLevel As TestLevel)

        Dim l As New List(Of Double)
        Dim x As New Collection(Of Double)
        Dim elapsed As TimeSpan
        Dim count As Integer = 10000000
        Dim sw As Stopwatch = Stopwatch.StartNew
        For i As Integer = 1 To count
            l.Add(i)
            x.Add(i)
        Next
        elapsed = sw.Elapsed
        _TestSite.TraceMessage($"populate {count} in {elapsed.TotalMilliseconds}ms")
        Dim a As Double()
        sw.Restart()
        a = x.ToArray
        elapsed = sw.Elapsed
        _TestSite.TraceMessage($".ToArray Collection(of Double)[{count}] in {elapsed.TotalMilliseconds}ms")
        a(0) = 0
        sw.Restart()
        a = l.ToArray
        elapsed = sw.Elapsed
        _TestSite.TraceMessage($".ToArray List(of Double)[{count}] in {elapsed.TotalMilliseconds}ms")

        sw.Restart()
        Dim ro As IReadOnlyList(Of Double) = New List(Of Double)(a)
        elapsed = sw.Elapsed
        _TestSite.TraceMessage($".Read-Only List(of Double)[{count}] in {elapsed.TotalMilliseconds}ms")

        If testLevel = TestLevel.None Then Return

        If (testLevel And TestLevel.Iterate) <> 0 Then
            EnumerableTests.IterateArray("Array", a)
            EnumerableTests.IterateIList("Array", a)
            EnumerableTests.IterateIEnumerable("Array", a)
            EnumerableTests.IterateICollection("Array", a)

            EnumerableTests.IterateArray("List", l.ToArray)
            EnumerableTests.IterateIList("List", l)
            EnumerableTests.IterateIEnumerable("List", l)
            EnumerableTests.IterateICollection("List", l)

            EnumerableTests.IterateArray("Collection", x.ToArray)
            EnumerableTests.IterateIList("Collection", x)
            EnumerableTests.IterateIEnumerable("Collection", x)
            EnumerableTests.IterateICollection("Collection", x)

            EnumerableTests.IterateArray("Read-Only List", ro.ToArray)
            EnumerableTests.IterateIList("Read-Only List", ro.ToList)
            EnumerableTests.IterateIEnumerable("Read-Only List", ro)
            EnumerableTests.IterateICollection("Read-Only List", ro.ToList)
        End If

        If (testLevel And TestLevel.ForEach) <> 0 Then
            EnumerableTests.ForEachArray("Array", a)
            EnumerableTests.ForEachIList("Array", a)
            EnumerableTests.ForEachIEnumerable("Array", a)
            EnumerableTests.ForEachICollection("Array", a)

            EnumerableTests.ForEachArray("List", l.ToArray)
            EnumerableTests.ForEachIList("List", l)
            EnumerableTests.ForEachIEnumerable("List", l)
            EnumerableTests.ForEachICollection("List", l)

            EnumerableTests.ForEachArray("Collection", x.ToArray)
            EnumerableTests.ForEachIList("Collection", x)
            EnumerableTests.ForEachIEnumerable("Collection", x)
            EnumerableTests.ForEachICollection("Collection", x)

            EnumerableTests.ForEachArray("Read-Only List", ro.ToArray)
            EnumerableTests.ForEachIList("Read-Only List", ro.ToList)
            EnumerableTests.ForEachIEnumerable("Read-Only List", ro)
            EnumerableTests.ForEachICollection("Read-Only List", ro.ToList)
        End If

        If (testLevel And TestLevel.Populate) <> 0 Then
            EnumerableTests.PopulateFromArray("Array", a)
            EnumerableTests.PopulateFromIList("Array", a)
            EnumerableTests.PopulateFromIEnumerable("Array", a)
            EnumerableTests.PopulateFromICollection("Array", a)

            EnumerableTests.PopulateFromArray("List", l.ToArray)
            EnumerableTests.PopulateFromIList("List", l)
            EnumerableTests.PopulateFromIEnumerable("List", l)
            EnumerableTests.PopulateFromICollection("List", l)

            EnumerableTests.PopulateFromArray("Collection", x.ToArray)
            EnumerableTests.PopulateFromIList("Collection", x)
            EnumerableTests.PopulateFromIEnumerable("Collection", x)
            EnumerableTests.PopulateFromICollection("Collection", x)
        End If


        If (testLevel And TestLevel.ReturnValue) <> 0 Then
            Dim lal As IList(Of Double) = EnumerableTests.ListAsIList(a)
            Dim aae As IEnumerable(Of Double) = EnumerableTests.ArrayAsIEnumerable(a)
            Dim aal As IList(Of Double) = EnumerableTests.ArrayAsIList(a)
            Dim lae As IEnumerable(Of Double) = EnumerableTests.ListAsIEnumerable(a)
            If (testLevel And TestLevel.Iterate) <> 0 Then
                EnumerableTests.IterateArray("Array as IEnumerable", aae.ToArray)
                EnumerableTests.IterateIList("Array as IEnumerable", aae.ToList)
                EnumerableTests.IterateIEnumerable("Array as IEnumerable", aae)
                EnumerableTests.IterateICollection("Array as IEnumerable", aae.ToList)

                EnumerableTests.IterateArray("Array as IList", aal.ToArray)
                EnumerableTests.IterateIList("Array as IList", aal)
                EnumerableTests.IterateIEnumerable("Array as IList", aal)
                EnumerableTests.IterateICollection("Array as IList", aal)


                EnumerableTests.IterateArray("List as IEnumerable", lae.ToArray)
                EnumerableTests.IterateIList("List as IEnumerable", aae.ToList)
                EnumerableTests.IterateIEnumerable("List as IEnumerable", aae)
                EnumerableTests.IterateICollection("List as IEnumerable", aae.ToList)

                EnumerableTests.IterateArray("List as IList", lal.ToArray)
                EnumerableTests.IterateIList("List as IList", aal)
                EnumerableTests.IterateIEnumerable("List as IList", aal)
                EnumerableTests.IterateICollection("List as IList", aal)
            End If
            If (testLevel And TestLevel.ForEach) <> 0 Then
                EnumerableTests.ForEachArray("Array as IEnumerable", aae.ToArray)
                EnumerableTests.ForEachIList("Array as IEnumerable", aae.ToList)
                EnumerableTests.ForEachIEnumerable("Array as IEnumerable", aae)
                EnumerableTests.ForEachICollection("Array as IEnumerable", aae.ToList)

                EnumerableTests.ForEachArray("Array as IList", aal.ToArray)
                EnumerableTests.ForEachIList("Array as IList", aal)
                EnumerableTests.ForEachIEnumerable("Array as IList", aal)
                EnumerableTests.ForEachICollection("Array as IList", aal)

                EnumerableTests.ForEachArray("List as IEnumerable", lae.ToArray)
                EnumerableTests.ForEachIList("List as IEnumerable", aae.ToList)
                EnumerableTests.ForEachIEnumerable("List as IEnumerable", aae)
                EnumerableTests.ForEachICollection("List as IEnumerable", aae.ToList)

                EnumerableTests.ForEachArray("List as IList", lal.ToArray)
                EnumerableTests.ForEachIList("List as IList", aal)
                EnumerableTests.ForEachIEnumerable("List as IList", aal)
                EnumerableTests.ForEachICollection("List as IList", aal)
            End If
        End If

    End Sub

    ''' <summary> A bit-field of flags for specifying test levels. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <Flags>
    Private Enum TestLevel
        ''' <summary> An enum constant representing the none option. </summary>
        None
        ''' <summary> An enum constant representing the iterate option. </summary>
        Iterate
        ''' <summary> An enum constant representing for each option. </summary>
        ForEach
        ''' <summary> An enum constant representing the populate option. </summary>
        Populate
        ''' <summary> An enum constant representing the return value option. </summary>
        ReturnValue
    End Enum

    ''' <summary> (Unit Test Method) iteration performance. </summary>
    ''' <remarks>
    ''' <list type="bullet"> <item>
    ''' Times in ms                                             </item><item>
    ''' populate 10000000 in 304                                </item><item>
    '''                                 .ToArray    TO Read-Only</item><item>
    ''' Collection(of Double)[10000000]    35                   </item><item>
    ''' List(Of Double)[10000000]          23       23          </item><item>
    '''                                                                         Populate    Iterate As      Iterate   For Each as   For Each</item><item>
    ''' Item                                                Iterate   For Each  Collection  IEnumerable     As IList  IEnumerable   Return As
    ''' IList</item><item>
    ''' Array(Of Double)[10000000] cast as Array                 17      19     166          14              14         19          19
    ''' </item><item>
    ''' Array(Of Double)[10000000] cast as IList                 32      56     257          44              44
    ''' 79          79 </item><item>
    ''' Array(Of Double)[10000000] cast as IEnumerable          423      57     218         459
    ''' 465         78          78 </item><item>
    ''' Array(Of Double)[10000000] cast as ICollection          436      56     203         209
    ''' 453         79          79 </item><item>
    ''' 
    ''' List(Of Double)[10000000] cast as Array                  14      19     197          14              14         19          18
    ''' </item><item>
    ''' List(Of Double)[10000000] cast as IList                  44      82     277          44              41
    ''' 79          78    </item><item>
    ''' List(Of Double)[10000000] cast as IEnumerable           200      87     335         469
    ''' 402         82          78    </item><item>
    ''' List(Of Double)[10000000] cast as ICollection           220      85     295         206
    ''' 446         80          79    </item><item>
    ''' </item><item>
    ''' Collection(Of Double)[10000000] cast as Array            15      19     173</item><item>
    ''' Collection(Of Double)[10000000] cast as IList            69      80     317</item><item>
    ''' Collection(Of Double)[10000000] cast as IEnumerable     231      92     305</item><item>
    ''' Collection(Of Double)[10000000] cast as ICollection     222      93     326</item><item>
    ''' </item><item>
    ''' Read-Only List(Of Double)[10000000] cast as Array        14      22</item><item>
    ''' Read-Only List(Of Double)[10000000] cast as IList in     42      96</item><item>
    ''' Read-Only List(Of Double)[10000000] cast as IEnumerable 197      90</item><item>
    ''' Read-Only List(Of Double)[10000000] cast as ICollection 198     145</item><item>
    ''' </item><item>
    ''' Summary: Iterations using indexes has a significant penalty. </item><item>
    ''' </item><item>
    ''' </item><item>
    ''' </item><item>
    ''' </item><item>
    ''' </item><item>
    ''' </item><item>
    ''' </item></list>
    ''' </remarks>
    <TestMethod()>
    Public Sub IterationPerformance()
        EnumerableTests.RunTests(TestLevel.None)
    End Sub

#End Region

End Class
