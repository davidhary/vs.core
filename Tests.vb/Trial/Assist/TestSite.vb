''' <summary> A test site class. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/12/2018 </para></remarks>
<Global.System.Runtime.CompilerServices.CompilerGeneratedAttribute(),
    Global.System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Editors.SettingsDesigner.SettingsSingleFileGenerator", "16.7.0.0"),
    Global.System.ComponentModel.EditorBrowsableAttribute(Global.System.ComponentModel.EditorBrowsableState.Advanced)>
Friend Class TestSite
    Inherits isr.Core.MSTest.TestSiteBase

    ''' <summary> Gets or sets the filename of the trace event project identities file. </summary>
    ''' <value> The filename of the trace event project identities file. </value>
    <Global.System.Configuration.UserScopedSettingAttribute(), Global.System.Configuration.DefaultSettingValueAttribute("True")>
    Public Overridable Property TraceEventProjectIdentitiesFileName As String
        Get
            Return Me.AppSettingGetter(String.Empty)
        End Get
        Set(value As String)
            Me.AppSettingSetter(value)
        End Set
    End Property

End Class

