''' <summary> This is a test class for the blue splash screen. </summary>
''' <remarks> David, 2020-09-23. </remarks>
<TestClass()>
Public Class BlueSplashScreenTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

    ''' <summary> (Unit Test Method) displays a splash screen test. </summary>
    ''' <remarks>
    ''' This unit test causes a warning when ran from the test agent:  
    ''' This is an issue with the way the picture box displays a gif image.
    ''' System.AppDomainUnloadedException: Attempted to access an unloaded AppDomain. This can happen
    ''' if the test(s) started a thread but did not stop it. Make sure that all the threads started
    ''' by the test(s) are stopped before completion.
    ''' https://stackoverflow.com/questions/42979071/appdomainunloadedexception-when-unit-testing-a-winforms-form-with-an-animated-gi.
    ''' </remarks>
    <TestMethod()>
    Public Sub DisplaySplashScreenTest()
        Using splashScreen As isr.Core.Forma.BlueSplash = New isr.Core.Forma.BlueSplash()
            splashScreen.Show()
            splashScreen.TopmostSetter(True)
            BlueSplashScreenTests.Display(splashScreen)
        End Using
    End Sub

    ''' <summary> Displays the given splashScreen. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <param name="splashScreen"> The splash screen. </param>
    Private Shared Sub Display(ByVal splashScreen As isr.Core.Forma.BlueSplash)
        For i As Integer = 1 To 10
            splashScreen.DisplayMessage($"Splash message {DateTimeOffset.Now}")
            Threading.Tasks.Task.Delay(300).Wait()
        Next
    End Sub

    ''' <summary> Tests the process exception on a another thread. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod()>
    Public Sub DisplaySplashScreenThreadTest()
        Dim oThread As New Threading.Thread(New Threading.ThreadStart(AddressOf Me.DisplaySplashScreenTest))
        oThread.Start()
        oThread.Join()
    End Sub


End Class
