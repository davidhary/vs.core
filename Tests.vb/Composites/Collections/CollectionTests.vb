Imports System.Collections.Specialized

Imports isr.Core.Composites

''' <summary> This is a test class for event handling. </summary>
''' <remarks> David, 2020-09-22. </remarks>
<TestClass()>
Public Class CollectionTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext

    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region

#Region " SYNCHRONIZED COLLECTION TESTS "

    ''' <summary> The actual action. </summary>
    Private _ActualAction As NotifyCollectionChangedAction

    ''' <summary> Handles the collection changed. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Notify collection changed event information. </param>
    Private Sub HandleCollectionChanged(ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
        Dim collectionSender As SynchronizedObservableCollection(Of String) = TryCast(sender, SynchronizedObservableCollection(Of String))
        If collectionSender Is Nothing Then Return
        Me._ActualAction = e.Action
    End Sub

    ''' <summary> (Unit Test Method) tests property change event handler. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    <TestMethod()>
    Public Sub CollectionChangeHandlingTest()
        Using sender As New SynchronizedObservableCollection(Of String)
            AddHandler sender.CollectionChanged, AddressOf Me.HandleCollectionChanged
            sender.Add($"Item#{sender.Count}")
            Dim expectedAction As NotifyCollectionChangedAction = NotifyCollectionChangedAction.Add
            Assert.AreEqual(expectedAction, Me._ActualAction, "actions should equal after add")
        End Using
    End Sub

#End Region

End Class
