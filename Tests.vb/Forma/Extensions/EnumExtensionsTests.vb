Imports isr.Core.WinForms.ComboBoxEnumExtensions

''' <summary> Enum extensions tests. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/14/2018 </para>
''' </remarks>
<TestClass()>
Public Class EnumExtensionsTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Forma.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region


#Region " COMBO BOX TESTS "

    ''' <summary> (Unit Test Method) tests combo box trace event type enum. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub ComboBoxTraceEventTypeEnumTest()
        Dim includeMask As TraceEventType = TraceEventType.Critical Or TraceEventType.Error Or TraceEventType.Information Or TraceEventType.Verbose Or TraceEventType.Warning
        Dim excludeMask As TraceEventType = TraceEventType.Critical
        Using panel As New System.Windows.Forms.Form
            Using comboBox As New System.Windows.Forms.ComboBox
                'comboBox.CreateControl()
                panel.Controls.Add(comboBox)
                'panel.PerformLayout()
                'comboBox.PerformLayout()

                ' list enum names
                comboBox.ListEnumNames(includeMask, excludeMask)
                Dim expectedItemCount As Integer = 4
                Assert.AreEqual(expectedItemCount, comboBox.Items.Count, "Expected item count")

                ' selected item
                comboBox.SelectedIndex = 0
                Dim expectedValue As TraceEventType = TraceEventType.Error
                Dim actualItem As KeyValuePair(Of TraceEventType, String) = comboBox.SelectedEnumItem(Of TraceEventType)
                Assert.AreEqual(expectedValue, actualItem.Key, "Selected item at index zero")

                ' select value
                expectedValue = TraceEventType.Information
                actualItem = comboBox.SelectValue(expectedValue)
                Assert.AreEqual(expectedValue, actualItem.Key, "Selected item by value")

                actualItem = comboBox.SelectedEnumItem(Of TraceEventType)
                Assert.AreEqual(expectedValue, actualItem.Key, "Selected item by key value pair")

                Assert.AreEqual(expectedValue, comboBox.SelectedEnumValue(Of TraceEventType), "Generic selected value")
                Assert.AreEqual(expectedValue, comboBox.SelectedEnumValue(Of TraceEventType)(TraceEventType.Warning),
                                    "Generic selected value with default")
            End Using
        End Using
    End Sub

    ''' <summary> (Unit Test Method) tests combo box notify synchronize level enum. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub ComboBoxNotifySyncLevelEnumTest()
        Dim includeMask As NotifySyncLevel = NotifySyncLevel.Async Or NotifySyncLevel.Sync
        Dim excludeMask As NotifySyncLevel = NotifySyncLevel.None
        Using panel As New System.Windows.Forms.Form
            Using comboBox As New System.Windows.Forms.ComboBox
                'comboBox.CreateControl()
                panel.Controls.Add(comboBox)
                'panel.PerformLayout()
                'comboBox.PerformLayout()

                ' list enum names
                comboBox.ListEnumNames(includeMask, excludeMask)
                Dim expectedItemCount As Integer = 2
                Assert.AreEqual(expectedItemCount, comboBox.Items.Count, "Expected item count")

                ' selected item
                comboBox.SelectedIndex = 0
                Dim expectedValue As NotifySyncLevel = NotifySyncLevel.Sync
                Dim actualItem As KeyValuePair(Of NotifySyncLevel, String) = comboBox.SelectedEnumItem(Of NotifySyncLevel)
                Assert.AreEqual(expectedValue, actualItem.Key, "Selected item at index zero")

                ' select value
                expectedValue = NotifySyncLevel.Async
                actualItem = comboBox.SelectValue(expectedValue)
                Assert.AreEqual(expectedValue, actualItem.Key, "Selected item by value")

                actualItem = comboBox.SelectedEnumItem(Of NotifySyncLevel)
                Assert.AreEqual(expectedValue, actualItem.Key, "Selected item by key value pair")

                Assert.AreEqual(expectedValue, comboBox.SelectedEnumValue(Of NotifySyncLevel), "Generic selected value")
                Assert.AreEqual(expectedValue, comboBox.SelectedEnumValue(Of NotifySyncLevel)(NotifySyncLevel.None),
                                    "Generic selected value with default")

            End Using
        End Using
    End Sub

    ''' <summary>
    ''' (Unit Test Method) tests tool strip combo box notify synchronize level enum.
    ''' </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestMethod>
    Public Sub ToolStripComboBoxNotifySyncLevelEnumTest()
        Dim includeMask As NotifySyncLevel = NotifySyncLevel.Async Or NotifySyncLevel.Sync
        Dim excludeMask As NotifySyncLevel = NotifySyncLevel.None
        Using panel As New System.Windows.Forms.Form
            Using toolStrip As New System.Windows.Forms.ToolStrip
                panel.Controls.Add(toolStrip)
                Using toolStripComboBox As New System.Windows.Forms.ToolStripComboBox
                    toolStrip.Items.Add(toolStripComboBox)
                    toolStrip.ResumeLayout()
                    toolStrip.PerformLayout()
                    toolStrip.Invalidate()
                    ' list enum names
                    toolStripComboBox.ComboBox.ListEnumNames(includeMask, excludeMask)

                    Dim expectedItemCount As Integer = 2
                    Assert.AreEqual(expectedItemCount, toolStripComboBox.Items.Count, "Expected item count")

                    ' selected item
                    toolStripComboBox.SelectedIndex = 0
                    Dim expectedValue As NotifySyncLevel = NotifySyncLevel.Sync
                    Dim actualItem As KeyValuePair(Of NotifySyncLevel, String) = toolStripComboBox.SelectedEnumItem(Of NotifySyncLevel)
                    Assert.AreEqual(expectedValue, actualItem.Key, "Selected item at index zero")

                    ' select value
                    expectedValue = NotifySyncLevel.Async
                    actualItem = toolStripComboBox.SelectValue(expectedValue)
                    Assert.AreEqual(expectedValue, actualItem.Key, "Selected item by value")

                    actualItem = toolStripComboBox.SelectedEnumItem(Of NotifySyncLevel)
                    Assert.AreEqual(expectedValue, actualItem.Key, "Selected item by key value pair")

                    Assert.AreEqual(expectedValue, toolStripComboBox.SelectedEnumValue(Of NotifySyncLevel), "Generic selected value")
                    Assert.AreEqual(expectedValue, toolStripComboBox.SelectedEnumValue(Of NotifySyncLevel)(NotifySyncLevel.None),
                                    "Generic selected value with default")

                End Using
            End Using
        End Using
    End Sub

    ''' <summary> Values that represent notify Synchronization levels. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Public Enum NotifySyncLevel
        ''' <summary> . </summary>
        <ComponentModel.Description("No notification")> None = 0
        ''' <summary> An enum constant representing the sync] option. </summary>
        <ComponentModel.Description("Synchronize")> [Sync]
        ''' <summary> An enum constant representing the async] option. </summary>
        <ComponentModel.Description("A-Synchronize")> [Async]
    End Enum

#End Region

End Class
