Imports System.ComponentModel
Imports System.Xml

Imports isr.Core.WinFormsViews

''' <summary> Component Tests. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/18/2019. </para>
''' </remarks>
<TestClass()>
Public Class ComponentTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Forma.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        Assert.IsTrue(ComponentTestInfo.Get.Exists, $"{GetType(ComponentTestInfo)} settings should exist")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region


#Region " AGGREGATE BINDING LIST VIEW "

    ''' <summary> A feed. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Class Feed

        ''' <summary> Gets or sets URL of the document. </summary>
        ''' <value> The URL. </value>
        Private ReadOnly Property Url As String

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <param name="url"> URL of the resource. </param>
        Public Sub New(ByVal url As String)
            Me._Url = url
            Me._Items = New BindingList(Of FeedItem)
        End Sub

        ''' <summary> Updates this object. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        Public Sub Update()
            Dim doc As New XmlDocument()
            doc.Load(Me._Url)
            Me._Title = doc.SelectSingleNode("/rss/channel/title").InnerText
            For Each node As XmlNode In doc.SelectNodes("//item")
                Dim item As New FeedItem With {
                        .Title = node("title").InnerText,
                        .Description = node("description").InnerText,
                        .PubDate = DateTime.Parse(node("pubDate").InnerText)
                    }
                Me.Items.Add(item)
            Next
        End Sub

        ''' <summary> Gets or sets the items. </summary>
        ''' <value> The items. </value>
        Public ReadOnly Property Items() As BindingList(Of FeedItem)
        ''' <summary> The title. </summary>
        Private _Title As String

        ''' <summary> Returns a string that represents the current object. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        ''' <returns> A string that represents the current object. </returns>
        Public Overrides Function ToString() As String
            Return If(Me._Title, Me.Url)
        End Function
    End Class

    ''' <summary> A feed item. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Class FeedItem

        ''' <summary> Gets or sets the title. </summary>
        ''' <value> The title. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Property Title() As String

        ''' <summary> Gets or sets the description. </summary>
        ''' <value> The description. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Property Description() As String

        ''' <summary> Gets or sets the pub date. </summary>
        ''' <value> The pub date. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Property PubDate() As DateTime

    End Class

    ''' <summary> (Unit Test Method) tests aggregate binding list view. </summary>
    ''' <remarks> tested 05/15/2019. </remarks>
    <TestMethod()>
    Public Sub AggregateBindingListViewTest()
        Using itemsView As New AggregateBindingListView(Of FeedItem)()
            Using panel As New System.Windows.Forms.Form
                Using grid As New System.Windows.Forms.DataGridView
                    'grid.CreateControl()
                    panel.Controls.Add(grid)
                    grid.DataSource = itemsView
                    Dim expectedCount As Integer = itemsView.Count
                    Dim actualCount As Integer = grid.Rows.Count

                    Assert.AreEqual(expectedCount, actualCount, "empty items view")
                    Dim feed As Feed = New Feed(ComponentTestInfo.Get.NewsOnlineFeedUrl)
                    feed.Update()
                    itemsView.SourceLists.Add(feed.Items)
                    expectedCount = feed.Items.Count
                    actualCount = itemsView.Count
                    Assert.AreEqual(expectedCount, actualCount, "expected items view count")
                    Dim sw As Stopwatch = Stopwatch.StartNew
                    sw.Restart()
                    Do Until sw.Elapsed > TimeSpan.FromMilliseconds(100) OrElse grid.Rows.Count = expectedCount
                        isr.Core.My.MyLibrary.DoEvents()
                    Loop
                    actualCount = grid.Rows.Count
                    Assert.AreEqual(expectedCount, actualCount, "expected grid row count")
                    feed = New Feed(ComponentTestInfo.Get.NewsOnlineFeedUrl)
                    feed.Update()
                    itemsView.SourceLists.Add(feed.Items)
                    expectedCount += feed.Items.Count
                    actualCount = itemsView.Count
                    Assert.AreEqual(expectedCount, actualCount, $"expected items view count after adding {feed.Items.Count} items")
                    sw.Restart()
                    Do Until sw.Elapsed > TimeSpan.FromMilliseconds(100) OrElse grid.Rows.Count = expectedCount
                        isr.Core.My.MyLibrary.DoEvents()
                    Loop
                    actualCount = grid.Rows.Count
                    Assert.AreEqual(expectedCount, actualCount, "expected grid row count")
                    itemsView.SourceLists.Remove(feed.Items)
                    expectedCount -= feed.Items.Count
                    actualCount = itemsView.Count
                    Assert.AreEqual(expectedCount, actualCount, $"expected items view count after removing {feed.Items.Count} items")
                    sw.Restart()
                    Do Until sw.Elapsed > TimeSpan.FromMilliseconds(100) OrElse grid.Rows.Count = expectedCount
                        isr.Core.My.MyLibrary.DoEvents()
                    Loop
                    actualCount = grid.Rows.Count
                    Assert.AreEqual(expectedCount, actualCount, "expected grid row count")
                End Using
            End Using
        End Using
    End Sub

#End Region

End Class
