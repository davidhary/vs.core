Imports isr.Core.Forma
Imports isr.Core.Constructs

''' <summary> Collections Tests. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/18/2019. </para>
''' </remarks>
<TestClass()>
Public Class CollectionsTests

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> My class initialize. </summary>
    ''' <remarks>
    ''' Use ClassInitialize to run code before running the first test in the class.
    ''' </remarks>
    ''' <param name="testContext"> Gets or sets the test context which provides information about
    '''                            and functionality for the current test run. </param>
    <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
    <ClassInitialize(), CLSCompliant(False)>
    Public Shared Sub MyClassInitialize(ByVal testContext As TestContext)
        Try
            _TestSite = New TestSite
            _TestSite.AddTraceMessagesQueue(_TestSite.TraceMessagesQueueListener)
            _TestSite.AddTraceMessagesQueue(isr.Core.Forma.My.MyLibrary.UnpublishedTraceMessages)
            _TestSite.InitializeTraceListener()
        Catch
            ' cleanup to meet strong guarantees
            Try
                MyClassCleanup()
            Finally
            End Try
            Throw
        End Try
    End Sub

    ''' <summary> My class cleanup. </summary>
    ''' <remarks> Use ClassCleanup to run code after all tests in a class have run. </remarks>
    <ClassCleanup()>
    Public Shared Sub MyClassCleanup()
        _TestSite?.Dispose()
    End Sub

    ''' <summary> Initializes before each test runs. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestInitialize()> Public Sub MyTestInitialize()
        ' assert reading of test settings from the configuration file.
        Assert.IsTrue(TestInfo.Exists, $"{NameOf(TestInfo)} settings should exist")
        Dim expectedUpperLimit As Double = 12
        Assert.IsTrue(Math.Abs(TestInfo.TimeZoneOffset) < expectedUpperLimit, $"{NameOf(TestSite.TimeZoneOffset)} should be lower than {expectedUpperLimit}")
        TestInfo.ClearMessageQueue()
    End Sub

    ''' <summary> Cleans up after each test has run. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    <TestCleanup()> Public Sub MyTestCleanup()
        TestInfo.AssertMessageQueue()
    End Sub

    ''' <summary>
    ''' Gets the test context which provides information about and functionality for the current test
    ''' run.
    ''' </summary>
    ''' <value> The test context. </value>
    <CLSCompliant(False)>
    Public Property TestContext() As TestContext
    ''' <summary> The test site. </summary>
    Private Shared _TestSite As TestSite

    ''' <summary> Gets information describing the test. </summary>
    ''' <value> Information describing the test. </value>
    Private Shared ReadOnly Property TestInfo() As TestSite
        Get
            Return _TestSite
        End Get
    End Property

#End Region


#Region " SORTABLE BINDING LIST "

    ''' <summary> A sale. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Class Sale

        ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        Public Sub New()
            Me.SaleDate = DateTimeOffset.Now
        End Sub

        ''' <summary> Gets the sale details. </summary>
        ''' <value> The sale details. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Property SaleDetails As SortableBindingList(Of SaleDetail)

        ''' <summary> Gets the salesman. </summary>
        ''' <value> The salesman. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Property Salesman As String

        ''' <summary> Gets the client. </summary>
        ''' <value> The client. </value>
        Public Property Client As String

        ''' <summary> Gets the sale date. </summary>
        ''' <value> The sale date. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Property SaleDate As DateTimeOffset

        ''' <summary> Gets the total number of amount. </summary>
        ''' <value> The total number of amount. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public ReadOnly Property TotalAmount As Decimal
            Get
                Debug.Assert(Me.SaleDetails IsNot Nothing)
                Return Me.SaleDetails.Sum(Function(a) a.TotalAmount)
            End Get
        End Property
    End Class

    ''' <summary> A sale detail. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Private Class SaleDetail

        ''' <summary> Gets the product. </summary>
        ''' <value> The product. </value>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Property Product As String

        ''' <summary> Gets the quantity. </summary>
        ''' <value> The quantity. </value>
        Public Property Quantity As Integer

        ''' <summary> Gets the unit price. </summary>
        ''' <value> The unit price. </value>
        Public Property UnitPrice As Decimal

        ''' <summary> Gets the total number of amount. </summary>
        ''' <value> The total number of amount. </value>
        Public ReadOnly Property TotalAmount As Decimal
            Get
                Return Me.UnitPrice * Me.Quantity
            End Get
        End Property
    End Class

    ''' <summary> Sortable list. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    ''' <returns> A list of. </returns>
    Private Shared Function SortableList() As SortableBindingList(Of Sale)
        Dim sales As Sale() = {New Sale() With {.Client = "Jane Doe", .SaleDate = New Date(2008, 1, 10), .Salesman = "John",
                                                    .SaleDetails = New SortableBindingList(Of SaleDetail)() From {
                                                         New SaleDetail() With {.Product = "Sportsman", .Quantity = 1, .UnitPrice = 80},
                                                         New SaleDetail() With {.Product = "Tusker Malt", .Quantity = 2, .UnitPrice = 100},
                                                         New SaleDetail() With {.Product = "Alvaro", .Quantity = 1, .UnitPrice = 50}}},
                                  New Sale() With {.Client = "Ben Jones", .SaleDate = New Date(2008, 1, 11), .Salesman = "Danny",
                                                    .SaleDetails = New SortableBindingList(Of SaleDetail)() From {
                                                        New SaleDetail() With {.Product = "Embassy Kings", .Quantity = 1, .UnitPrice = 80},
                                                        New SaleDetail() With {.Product = "Tusker", .Quantity = 5, .UnitPrice = 100},
                                                        New SaleDetail() With {.Product = "Movie", .Quantity = 3, .UnitPrice = 50}}},
                                  New Sale() With {.Client = "Tim Kim", .SaleDate = New Date(2008, 1, 12), .Salesman = "Kaplan",
                                                   .SaleDetails = New SortableBindingList(Of SaleDetail)() From {
                                                        New SaleDetail() With {.Product = "Citizen Special", .Quantity = 10, .UnitPrice = 30},
                                                        New SaleDetail() With {.Product = "Burn", .Quantity = 2, .UnitPrice = 100}}}}
        Return New SortableBindingList(Of Sale)(sales.ToList)
    End Function

    ''' <summary> (Unit Test Method) tests sortable binding list. </summary>
    ''' <remarks> Tested 05/14/2019. </remarks>
    <TestMethod()>
    Public Sub SortableBindingListTest()
        Dim sales As SortableBindingList(Of Sale) = SortableList()
        Dim l As List(Of Sale) = sales.OrderBy(Of String)(Function(x) x.Client).ToList
        Assert.IsTrue(String.Compare(l(0).Client, l(1).Client, StringComparison.CurrentCulture) = -1, $"{l(0).Client} must come before {l(1).Client}")
        l = sales.OrderByDescending(Of String)(Function(x) x.Client).ToList
        Assert.IsTrue(String.Compare(l(0).Client, l(1).Client, StringComparison.CurrentCulture) = 1, $"{l(0).Client} must come after {l(1).Client}")
        'sales.OrderByDescending(Of String)(Function(x) x.Client)
        Dim expectedFirstClient As String = "Abe"
        sales.Add(New Sale With {.Client = expectedFirstClient, .SaleDate = New Date(2009, 1, 12), .Salesman = "Kaplan",
                                                   .SaleDetails = New SortableBindingList(Of SaleDetail)() From {
                                                        New SaleDetail() With {.Product = "Pepsi", .Quantity = 4, .UnitPrice = 2.8D},
                                                        New SaleDetail() With {.Product = "Perrier", .Quantity = 12, .UnitPrice = 1.5D}}})
        Dim expectedLastClient As String = "Yoda"
        sales.Add(New Sale With {.Client = expectedLastClient, .SaleDate = New Date(2009, 1, 12), .Salesman = "Kaplan",
                                                   .SaleDetails = New SortableBindingList(Of SaleDetail)() From {
                                                        New SaleDetail() With {.Product = "Cola", .Quantity = 4, .UnitPrice = 3.1D},
                                                        New SaleDetail() With {.Product = "Avian", .Quantity = 12, .UnitPrice = 1.5D}}})
    End Sub

    ''' <summary> (Unit Test Method) tests data grid view sortable binding list. </summary>
    ''' <remarks> Tested 05/14/2019. </remarks>
    <TestMethod>
    Public Sub DataGridViewSortableBindingListTest()
        Using panel As New System.Windows.Forms.Form
            Using grid As New System.Windows.Forms.DataGridView
                'grid.CreateControl()
                panel.Controls.Add(grid)
                'panel.PerformLayout()
                'grid.PerformLayout()
                Dim sales As SortableBindingList(Of Sale) = SortableList()
                grid.DataSource = sales
                ' grid has a header row.
                Dim expectedItemCount As Integer = sales.Count + 1
                Assert.AreEqual(expectedItemCount, grid.Rows.Count, "Expected row count")
                Dim firstValue As String = grid.Rows(0).Cells.Item(NameOf(Sale.Client)).Value.ToString
                Dim secondValue As String = grid.Rows(1).Cells.Item(NameOf(Sale.Client)).Value.ToString
                Assert.IsTrue(String.Compare(firstValue, secondValue, StringComparison.CurrentCulture) = 1, $"Before sorting {firstValue} must come before {secondValue }")

                ' test sort ascending
                Dim direction As ComponentModel.ListSortDirection = ComponentModel.ListSortDirection.Ascending
                grid.Sort(grid.Columns.Item(NameOf(Sale.Client)), direction)
                firstValue = grid.Rows(0).Cells.Item(NameOf(Sale.Client)).Value.ToString
                secondValue = grid.Rows(1).Cells.Item(NameOf(Sale.Client)).Value.ToString
                Assert.IsTrue(String.Compare(firstValue, secondValue, StringComparison.CurrentCulture) = -1, $"After sorting {direction} {firstValue} must come before {secondValue }")

                ' test sort descending
                direction = ComponentModel.ListSortDirection.Descending
                grid.Sort(grid.Columns.Item(NameOf(Sale.Client)), direction)
                firstValue = grid.Rows(0).Cells.Item(NameOf(Sale.Client)).Value.ToString
                secondValue = grid.Rows(1).Cells.Item(NameOf(Sale.Client)).Value.ToString
                Assert.IsTrue(String.Compare(firstValue, secondValue, StringComparison.CurrentCulture) = 1, $"After sorting {direction} {firstValue} must come before {secondValue }")
            End Using
        End Using
    End Sub

#End Region


End Class
