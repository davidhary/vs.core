﻿Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-09-23. </remarks>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-09-23. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub
        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Core Forma Tests"
        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Unit Tests for the Core Forma Library"
        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "isr.Core.FormaTests"

    End Class

End Namespace
