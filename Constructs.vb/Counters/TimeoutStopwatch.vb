''' <summary> A stop watch with timeout preset. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 05/06/2009, 1.1.3413.x. </para>
''' </remarks>
<DebuggerDisplay("Elapsed = {Elapsed}")>
Public Class TimeoutStopwatch
    Inherits Diagnostics.Stopwatch

    ''' <summary>
    ''' Constructs a new timeout stop watch with the specified
    ''' <paramref name="duration">duration</paramref>
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="duration"> Specifies the stop watch timeout duration. </param>
    Public Sub New(ByVal duration As TimeSpan)
        MyBase.New()
        Me._Duration = duration
    End Sub

    ''' <summary>
    ''' Constructs a new timeout stop watch with the specified
    ''' <paramref name="duration">duration</paramref>
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="duration"> Specifies the stop watch duration. </param>
    ''' <returns> Returns true if timeout is done. </returns>
    Public Overloads Shared Function StartNew(ByVal duration As TimeSpan) As TimeoutStopwatch
        Dim osw As New TimeoutStopwatch(duration)
        osw.Start()
        Return osw
    End Function

    ''' <summary> Gets or sets the duration of the timeout stop watch. </summary>
    ''' <value> <c>Duration</c>is a TimeSpan property. </value>
    Public Property Duration() As TimeSpan

    ''' <summary> Returns true if timeout. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> Returns true if timeout. </returns>
    Public Function IsTimeout() As Boolean
        Return Me.Elapsed > Me.Duration
    End Function

    ''' <summary> Waits time timeout. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub Wait()
        If Not Me.IsRunning Then Me.Start()
        Dim interval As TimeSpan = Me.Duration - Me.Elapsed
        If interval > TimeSpan.Zero Then Threading.Tasks.Task.Delay(Me.Duration - Me.Elapsed)
    End Sub

End Class
