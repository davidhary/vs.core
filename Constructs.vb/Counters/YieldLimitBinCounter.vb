''' <summary> A yield bin counter. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/7/2016 </para>
''' </remarks>
<DebuggerDisplay("Total = {TotalCount}")>
Public Class YieldLimitBinCounter
    Inherits YieldCounter

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub New()
        MyBase.New()
        Me.ResetKnownStateThis()
        Me.ClearKnownStateThis()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As YieldLimitBinCounter)
        MyBase.New(value)
        If value IsNot Nothing Then
            Me._GoodBinNumber = value.GoodBinNumber
            Me._HighBinNumber = value.HighBinNumber
            Me._LowBinNumber = value.LowBinNumber
            Me._LowerLimit = value.LowerLimit
            Me._UpperLimit = value.UpperLimit
            Me._BinNumber = value.BinNumber
            For Each kvp As KeyValuePair(Of Integer, Integer) In value.BinCountDictionary
                Me._BinCountDictionary.Item(kvp.Key) = kvp.Value
                Me._BinNumber = kvp.Key
            Next
        End If
    End Sub

#End Region

#Region " RESET AND CLEAR "

    ''' <summary>
    ''' Clears to known (clear) state; Clears select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Private Sub ClearKnownStateThis()
        Me._BinCountDictionary = New Dictionary(Of Integer, Integer)
        For i As Integer = 0 To 30
            Me._BinCountDictionary.Add(i, 0)
        Next
    End Sub

    ''' <summary>
    ''' Clears to known (clear) state; Clears select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Overrides Sub ClearKnownState()
        Me.ClearKnownStateThis()
        MyBase.ClearKnownState()
    End Sub

    ''' <summary> Publishes this object. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Overrides Sub Publish()
        Me.NotifyPropertyChanged(NameOf(YieldLimitBinCounter.BinNumber))
    End Sub

    ''' <summary> Resets the know state. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Private Sub ResetKnownStateThis()
        Me._GoodBinNumber = 0
        Me._HighBinNumber = 1
        Me._LowBinNumber = 2
        Me._LowerLimit = -1
        Me._UpperLimit = 1
    End Sub

    ''' <summary> Resets the know state. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Overrides Sub ResetKnownState()
        Me.ResetKnownStateThis()
        MyBase.ResetKnownState()
    End Sub

#End Region

#Region " BIN COUNTER "

    ''' <summary> Gets or sets a dictionary of bin counts. </summary>
    ''' <value> A Dictionary of bin counts. </value>
    Private ReadOnly Property BinCountDictionary As IDictionary(Of Integer, Integer)

    ''' <summary> Gets or sets the number of bins. </summary>
    ''' <value> The number of bins. </value>
    Public Property BinCount(ByVal binNumber As Integer) As Integer
        Get
            Return Me.BinCountDictionary.Item(binNumber)
        End Get
        Set(value As Integer)
            Me.BinCountDictionary.Item(binNumber) = value
            Me.BinNumber = binNumber
        End Set
    End Property
    ''' <summary> The bin number. </summary>
    Private _BinNumber As Integer

    ''' <summary> Gets or sets the bin number. </summary>
    ''' <value> The bin number. </value>
    Public Property BinNumber As Integer
        Get
            Return Me._BinNumber
        End Get
        Set(value As Integer)
            Me._BinNumber = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Gets or sets the good bin number. </summary>
    ''' <value> The good bin number. </value>
    Public Property GoodBinNumber As Integer

    ''' <summary> Adds a value. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub AddValue(ByVal value As Double?)
        If value.HasValue Then
            Me.AddValue(value)
        Else
            Me.InvalidCount += 1
            Me.TotalCount += 1
        End If
    End Sub

#End Region

#Region " LIMIT BINS "

    ''' <summary> Gets or sets the high bin number. </summary>
    ''' <value> The high bin number. </value>
    Public Property HighBinNumber As Integer

    ''' <summary> Gets or sets the low bin number. </summary>
    ''' <value> The low bin number. </value>
    Public Property LowBinNumber As Integer

    ''' <summary> Gets or sets the lower limit. </summary>
    ''' <value> The lower limit. </value>
    Public Property LowerLimit As Double

    ''' <summary> Gets or sets the upper limit. </summary>
    ''' <value> The upper limit. </value>
    Public Property UpperLimit As Double

    ''' <summary> Adds a value. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub AddValue(ByVal value As Double)
        Dim bin As Integer
        If value > Me.UpperLimit Then
            bin = Me.HighBinNumber
        ElseIf value < Me.LowerLimit Then
            bin = Me.LowBinNumber
        Else
            bin = Me.GoodBinNumber
        End If
        Me.BinCountDictionary.Item(bin) += 1
        Me.TotalCount += 1
    End Sub

#End Region

End Class
