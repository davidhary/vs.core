''' <summary> Permits counting down using long integer. </summary>
''' <remarks>
''' (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 06/13/2006, 1.0.2355.x. </para>
''' </remarks>
<DebuggerDisplay("Current Value = {CurrentValue}")>
Public Class DownCounter

#Region " COUNTER "

    ''' <summary> Decrement the count. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub Countdown()
        Me._CurrentValue -= Me._DecrementValue
    End Sub

    ''' <summary> The current value. </summary>
    Private _CurrentValue As Integer

    ''' <summary> Gets the current value of the counter. </summary>
    ''' <value> The current value. </value>
    Public ReadOnly Property CurrentValue() As Integer
        Get
            Return Me._CurrentValue
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets or set the amount by which the count is decremented by on each countdown.
    ''' </summary>
    ''' <value> The decrement value. </value>
    Public Property DecrementValue() As Integer

    ''' <summary> Gets or sets the initial count down value. </summary>
    ''' <value> The initial value. </value>
    Public Property InitialValue() As Integer

    ''' <summary> Returns true if countdown is completed. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> The is countdown done. </returns>
    Public Function IsCountdownDone() As Boolean
        Return Me._CurrentValue < 0
    End Function

    ''' <summary> Restart from the initial value. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub Restart()
        Me._CurrentValue = Me._InitialValue
    End Sub

#End Region

End Class

