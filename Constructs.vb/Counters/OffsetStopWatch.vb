''' <summary> A stop watch with Offset preset. </summary>
''' <remarks>
''' (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 05/06/2009, 1.1.3413.x. </para>
''' </remarks>
<DebuggerDisplay("Elapsed = {Elapsed}")>
Public Class OffsetStopwatch
    Inherits Diagnostics.Stopwatch

    ''' <summary>
    ''' Constructs a new Offset stop watch with the specified offset (initial elapsed time)
    ''' <paramref name="duration">duration</paramref>
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="duration"> Specifies the stop watch Offset duration. </param>
    Public Sub New(ByVal duration As TimeSpan)
        MyBase.New()
        Me._InitialElapsedTimespan = duration
    End Sub

    ''' <summary>
    ''' Constructs a new timeout stop watch using the specified stopwatch elapsed time as offset.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="stopwatch"> The stopwatch. </param>
    Public Sub New(ByVal stopwatch As Stopwatch)
        Me.New(If(stopwatch Is Nothing, TimeSpan.Zero, stopwatch.Elapsed))
    End Sub

    ''' <summary>
    ''' Constructs a new Offset stop watch with the specified
    ''' <paramref name="duration">duration</paramref>
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="duration"> Specifies the stop watch duration. </param>
    ''' <returns> Returns true if Offset is done. </returns>
    Public Overloads Shared Function StartNew(ByVal duration As TimeSpan) As OffsetStopwatch
        Dim osw As New OffsetStopwatch(duration)
        osw.Start()
        Return osw
    End Function

    ''' <summary> Gets or sets the initial elapsed time. </summary>
    ''' <value> <c>Duration</c>is a TimeSpan property. </value>
    Public Property InitialElapsedTimespan() As TimeSpan

    ''' <summary> Gets the elapsed timespan including the initial offset time. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> A TimeSpan. </returns>
    Public Shadows Function Elapsed() As TimeSpan
        Return Me.NetElapsed.Add(Me.InitialElapsedTimespan)
    End Function

    ''' <summary> Net elapsed time (without offset). </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> A TimeSpan. </returns>
    Public Function NetElapsed() As TimeSpan
        Return MyBase.Elapsed
    End Function

End Class
