Imports isr.Core.EnumExtensions

''' <summary> A yield bin counter. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/7/2016 </para>
''' </remarks>
<DebuggerDisplay("Total = {TotalCount}")>
Public Class YieldBinCounter
    Inherits YieldCounter

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="binNumbers">        The bin numbers. </param>
    ''' <param name="goodBinNumbers">    The Good bin number. </param>
    ''' <param name="failureBinNumbers"> The Failure bin number. </param>
    ''' <param name="invalidBinNumbers"> The invalid bin number. </param>
    Public Sub New(ByVal binNumbers As Integer(), ByVal goodBinNumbers As Integer(),
                   ByVal failureBinNumbers As Integer(), ByVal invalidBinNumbers As Integer())
        MyBase.New()
        Me.ResetKnownStatThis(binNumbers, goodBinNumbers, failureBinNumbers, invalidBinNumbers)
    End Sub

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="binNumbers">        The bin numbers. </param>
    ''' <param name="goodBinNumbers">    The Good bin number. </param>
    ''' <param name="failureBinNumbers"> The Failure bin number. </param>
    ''' <param name="invalidBinNumbers"> The invalid bin number. </param>
    Public Sub New(ByVal binNumbers As IList(Of Integer), ByVal goodBinNumbers As IList(Of Integer),
                   ByVal failureBinNumbers As IList(Of Integer), ByVal invalidBinNumbers As IList(Of Integer))
        MyBase.New()
        Me.ResetKnownStatThis(binNumbers, goodBinNumbers, failureBinNumbers, invalidBinNumbers)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="binNumbers">        The bin numbers. </param>
    ''' <param name="goodBinNumbers">    The Good bin number. </param>
    ''' <param name="failureBinNumbers"> The Failure bin number. </param>
    ''' <param name="invalidBinNumbers"> The invalid bin number. </param>
    Public Sub New(ByVal binNumbers As IEnumerable(Of Integer), ByVal goodBinNumbers As IEnumerable(Of Integer),
                   ByVal failureBinNumbers As IEnumerable(Of Integer), ByVal invalidBinNumbers As IEnumerable(Of Integer))
        Me.New(binNumbers.ToList, goodBinNumbers.ToList, failureBinNumbers.ToList, invalidBinNumbers.ToList)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As YieldBinCounter)
#Disable Warning CA1825 ' Avoid zero-length array allocations.
        Me.New(If(value Is Nothing, New Integer() {}, value.BinNumbers),
               If(value Is Nothing, New Integer() {}, value.BinNumbers),
               If(value Is Nothing, New Integer() {}, value.BinNumbers),
               If(value Is Nothing, New Integer() {}, value.BinNumbers))
#Enable Warning CA1825 ' Avoid zero-length array allocations.
        If value IsNot Nothing Then
            Me.IncrementRangeThis(value)
            Me._BinNumber = value.BinNumber
        End If
    End Sub

#End Region

#Region " RESET AND CLEAR "

    ''' <summary>
    ''' Clears to known (clear) state; Clears select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Overrides Sub ClearKnownState()
        Me._BinCountDictionary.ClearKnownState()
        MyBase.ClearKnownState()
    End Sub

    ''' <summary> Publishes this object. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Overrides Sub Publish()
        Me.NotifyPropertyChanged(NameOf(YieldBinCounter.BinNumber))
    End Sub

    ''' <summary> Resets the known state. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="binNumbers">        The bin numbers. </param>
    ''' <param name="goodBinNumbers">    The Good bin number. </param>
    ''' <param name="failureBinNumbers"> The Failure bin number. </param>
    ''' <param name="invalidBinNumbers"> The invalid bin number. </param>
    Private Sub ResetKnownStatThis(ByVal binNumbers As Integer(), ByVal goodBinNumbers As Integer(),
                                   ByVal failureBinNumbers As Integer(), ByVal invalidBinNumbers As Integer())
        Me._BinCountDictionary = New BinCountDictionary(binNumbers)
        Me.InitializeFailureBinNumbersThis(failureBinNumbers)
        Me.InitializeGoodBinNumbersThis(goodBinNumbers)
        Me.InitializeInvalidBinNumbersThis(invalidBinNumbers)
        Me.ValidateBinNumbers()
    End Sub

    ''' <summary> Resets the known state. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="binNumbers">        The bin numbers. </param>
    ''' <param name="goodBinNumbers">    The Good bin number. </param>
    ''' <param name="failureBinNumbers"> The Failure bin number. </param>
    ''' <param name="invalidBinNumbers"> The invalid bin number. </param>
    Private Sub ResetKnownStatThis(ByVal binNumbers As IList(Of Integer), ByVal goodBinNumbers As IList(Of Integer),
                                   ByVal failureBinNumbers As IList(Of Integer), ByVal invalidBinNumbers As IList(Of Integer))
        Me._BinCountDictionary = New BinCountDictionary(binNumbers)
        Me.InitializeFailureBinNumbersThis(failureBinNumbers)
        Me.InitializeGoodBinNumbersThis(goodBinNumbers)
        Me.InitializeInvalidBinNumbersThis(invalidBinNumbers)
        Me.ValidateBinNumbers()
    End Sub

    ''' <summary> Validates the bin numbers. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Private Sub ValidateBinNumbers()
        For Each binNumber As Integer In Me.BinNumbers
            If Me.GoodBinNumbers.Contains(binNumber) Then
            ElseIf Me.InvalidBinNumbers.Contains(binNumber) Then
            ElseIf Me.FailureBinNumbers.Contains(binNumber) Then
            Else
                Throw New InvalidOperationException($"Bin number validation failed because bin number {binNumber} not defined.")
            End If
        Next
    End Sub

#End Region

#Region " SPECIAL BINS "

    ''' <summary> Gets the Good bin number. </summary>
    ''' <value> The Good bin number. </value>
    Public ReadOnly Property GoodBinNumbers As IList(Of Integer)

    ''' <summary> Initializes the good bin numbers on this class. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="goodBinNumbers"> The Good bin number. </param>
    Private Sub InitializeGoodBinNumbersThis(ByVal goodBinNumbers As Integer())
        Me._GoodBinNumbers = New List(Of Integer)(goodBinNumbers)
    End Sub

    ''' <summary> Initializes the Good bin numbers described by GoodBinNumbers. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="goodBinNumbers"> The Good bin number. </param>
    Public Sub InitializeGoodBinNumbers(ByVal goodBinNumbers As Integer())
        Me.InitializeGoodBinNumbersThis(goodBinNumbers)
    End Sub

    ''' <summary> Initializes the good bin numbers on this class. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="goodBinNumbers"> The Good bin number. </param>
    Private Sub InitializeGoodBinNumbersThis(ByVal goodBinNumbers As IList(Of Integer))
        Me._GoodBinNumbers = New List(Of Integer)(goodBinNumbers)
    End Sub

    ''' <summary> Initializes the Good bin numbers described by GoodBinNumbers. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="goodBinNumbers"> The Good bin number. </param>
    Public Sub InitializeGoodBinNumbers(ByVal goodBinNumbers As IList(Of Integer))
        Me.InitializeGoodBinNumbersThis(goodBinNumbers)
    End Sub

    ''' <summary> Gets the Failure bin numbers. </summary>
    ''' <value> The Failure bin numbers. </value>
    Public ReadOnly Property FailureBinNumbers As IList(Of Integer)

    ''' <summary> Initializes the failure bin numbers this. </summary>
    ''' <remarks> David, 2020-09-04. </remarks>
    ''' <param name="failureBinNumbers"> The Failure bin number. </param>
    Private Sub InitializeFailureBinNumbersThis(ByVal failureBinNumbers As Integer())
        Me._FailureBinNumbers = New List(Of Integer)(failureBinNumbers)
    End Sub

    ''' <summary> Initializes the Failure bin numbers described by FailureBinNumbers. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="failureBinNumbers"> The Failure bin number. </param>
    Public Sub InitializeFailureBinNumbers(ByVal failureBinNumbers As Integer())
        Me.InitializeFailureBinNumbersThis(failureBinNumbers)
    End Sub

    ''' <summary> Initializes the failure bin numbers this. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="failureBinNumbers"> The Failure bin number. </param>
    Private Sub InitializeFailureBinNumbersThis(ByVal failureBinNumbers As IList(Of Integer))
        Me._FailureBinNumbers = New List(Of Integer)(failureBinNumbers)
    End Sub

    ''' <summary> Initializes the Failure bin numbers described by FailureBinNumbers. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="failureBinNumbers"> The Failure bin number. </param>
    Public Sub InitializeFailureBinNumbers(ByVal failureBinNumbers As IList(Of Integer))
        Me.InitializeFailureBinNumbersThis(failureBinNumbers)
    End Sub

    ''' <summary> Gets the invalid bin number. </summary>
    ''' <value> The invalid bin number. </value>
    Public ReadOnly Property InvalidBinNumbers As IList(Of Integer)

    ''' <summary> Initializes the invalid bin numbers this. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="invalidBinNumbers"> The invalid bin number. </param>
    Private Sub InitializeInvalidBinNumbersThis(ByVal invalidBinNumbers As Integer())
        Me._InvalidBinNumbers = New List(Of Integer)(invalidBinNumbers)
    End Sub

    ''' <summary> Initializes the invalid bin numbers described by invalidBinNumbers. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="invalidBinNumbers"> The invalid bin number. </param>
    Public Sub InitializeInvalidBinNumbers(ByVal invalidBinNumbers As Integer())
        Me.InitializeInvalidBinNumbersThis(invalidBinNumbers)
    End Sub

    ''' <summary> Initializes the invalid bin numbers this. </summary>
    ''' <remarks> David, 2020-09-04. </remarks>
    ''' <param name="invalidBinNumbers"> The invalid bin number. </param>
    Private Sub InitializeInvalidBinNumbersThis(ByVal invalidBinNumbers As IList(Of Integer))
        Me._InvalidBinNumbers = New List(Of Integer)(invalidBinNumbers)
    End Sub

    ''' <summary> Initializes the invalid bin numbers described by invalidBinNumbers. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="invalidBinNumbers"> The invalid bin number. </param>
    Public Sub InitializeInvalidBinNumbers(ByVal invalidBinNumbers As IList(Of Integer))
        Me.InitializeInvalidBinNumbersThis(invalidBinNumbers)
    End Sub

    ''' <summary> Initializes the invalid bin numbers this. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="invalidBinNumbers"> The invalid bin number. </param>
    Private Sub InitializeInvalidBinNumbersThis(ByVal invalidBinNumbers As IEnumerable(Of Integer))
        Me._InvalidBinNumbers = New List(Of Integer)(invalidBinNumbers)
    End Sub

    ''' <summary> Initializes the invalid bin numbers described by invalidBinNumbers. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="invalidBinNumbers"> The invalid bin number. </param>
    Public Sub InitializeInvalidBinNumbers(ByVal invalidBinNumbers As IEnumerable(Of Integer))
        Me.InitializeInvalidBinNumbersThis(invalidBinNumbers)
    End Sub

#End Region

#Region " BIN COUNTER "
    ''' <summary> Dictionary of bin counts. </summary>
    Private _BinCountDictionary As BinCountDictionary

    ''' <summary> Gets a dictionary of bin counts. </summary>
    ''' <value> A Dictionary of bin counts. </value>
    Private ReadOnly Property BinCountDictionary As BinCountDictionary
        Get
            Return Me._BinCountDictionary
        End Get
    End Property

    ''' <summary> Gets the bin word. </summary>
    ''' <value> The bin word. </value>
    Public ReadOnly Property BinWord As Integer
        Get
            Return Me.BinCountDictionary.BinWord
        End Get
    End Property

    ''' <summary> Gets the bin numbers. </summary>
    ''' <value> The bin numbers. </value>
    Public ReadOnly Property BinNumbers As IEnumerable(Of Integer)
        Get
            Return Me.BinCountDictionary.BinNumbers
        End Get
    End Property

    ''' <summary> Increment bin. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="binNumber"> The bin number. </param>
    Public Sub IncrementBin(ByVal binNumber As Integer)
        Me.IncrementBin(binNumber, 1)
    End Sub

    ''' <summary> Increment bin this. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="binNumber"> The bin number. </param>
    ''' <param name="count">     Number of. </param>
    Private Sub IncrementBinThis(ByVal binNumber As Integer, ByVal count As Integer)
        If Me.GoodBinNumbers.Contains(binNumber) Then
            ' good bin is incremented in the total count
        ElseIf Me.InvalidBinNumbers.Contains(binNumber) Then
            Me.InvalidCount += count
        ElseIf Me.FailureBinNumbers.Contains(binNumber) Then
            Me.FailedCount += count
        Else
            Throw New InvalidOperationException($"Bin number {binNumber} not defined.")
        End If
        Me.BinCountDictionary.Item(binNumber) += count
        Me.TotalCount += count
        Me._BinNumber = binNumber
    End Sub

    ''' <summary> Increment bin. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="binNumber"> The bin number. </param>
    ''' <param name="count">     Number of. </param>
    Public Sub IncrementBin(ByVal binNumber As Integer, ByVal count As Integer)
        If Me.GoodBinNumbers.Contains(binNumber) Then
            ' good bin is incremented in the total count
        ElseIf Me.InvalidBinNumbers.Contains(binNumber) Then
            Me.InvalidCount += count
        ElseIf Me.FailureBinNumbers.Contains(binNumber) Then
            Me.FailedCount += count
        Else
            Throw New InvalidOperationException($"Bin number {binNumber} not defined.")
        End If
        Me.BinCountDictionary.Item(binNumber) += count
        Me.TotalCount += count
        Me.BinNumber = binNumber
    End Sub

    ''' <summary> Increment range on this class. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="counter"> The counter. </param>
    Private Sub IncrementRangeThis(ByVal counter As YieldBinCounter)
        If counter Is Nothing Then Throw New ArgumentNullException(NameOf(counter))
        For Each binNumber As Integer In counter.BinNumbers
            Me.IncrementBinThis(binNumber, counter.BinCountDictionary(binNumber))
        Next
    End Sub

    ''' <summary> Increment range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="counter"> The counter. </param>
    Public Sub IncrementRange(ByVal counter As YieldBinCounter)
        If counter Is Nothing Then Throw New ArgumentNullException(NameOf(counter))
        For Each binNumber As Integer In counter.BinNumbers
            Me.IncrementBin(binNumber, counter.BinCountDictionary(binNumber))
        Next
    End Sub

    ''' <summary> Decrement bin. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="binNumber"> The bin number. </param>
    Public Sub DecrementBin(ByVal binNumber As Integer)
        Me.DecrementBin(binNumber, 1)
    End Sub

    ''' <summary> Decrement bin. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="binNumber"> The bin number. </param>
    ''' <param name="count">     Number of. </param>
    Public Sub DecrementBin(ByVal binNumber As Integer, ByVal count As Integer)
        If Me.GoodBinNumbers.Contains(binNumber) Then
            ' good bin is incremented in the total count
        ElseIf Me.InvalidBinNumbers.Contains(binNumber) Then
            If Me.InvalidCount >= count Then
                Me.InvalidCount -= count
            Else
                Throw New InvalidOperationException($"Invalid count {Me.InvalidCount } lower then the decrement {count} for bin number {binNumber}")
            End If
        ElseIf Me.FailureBinNumbers.Contains(binNumber) Then
            If Me.FailedCount >= count Then
                Me.FailedCount -= count
            Else
                Throw New InvalidOperationException($"Failed count {Me.FailedCount } lower then the decrement {count} for bin number {binNumber}")
            End If
        Else
            Throw New InvalidOperationException($"Bin number {binNumber} not defined.")
        End If
        If Me.BinCountDictionary.Item(binNumber) >= count Then
            Me.BinCountDictionary.Item(binNumber) -= count
        Else
            Throw New InvalidOperationException($"Bin count {Me.BinCountDictionary.Item(binNumber)} for bin number {binNumber} lower then the decrement {count}")
        End If
        If Me.TotalCount >= count Then
            Me.TotalCount -= count
        Else
            Throw New InvalidOperationException($"Total count {Me.TotalCount} is lower then the decrement {count} for bin number {binNumber} ")
        End If
        Me.BinNumber = binNumber
    End Sub

    ''' <summary> Decrement range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="counter"> The counter. </param>
    Public Sub DecrementRange(ByVal counter As YieldBinCounter)
        If counter Is Nothing Then Throw New ArgumentNullException(NameOf(counter))
        For Each binNumber As Integer In counter.BinNumbers
            Me.DecrementBin(binNumber, counter.BinCountDictionary(binNumber))
        Next
    End Sub

    ''' <summary> Gets or sets the number of bins. </summary>
    ''' <value> The number of bins. </value>
    Public Property BinCount(ByVal binNumber As Integer) As Integer
        Get
            Return Me.BinCountDictionary.Item(binNumber)
        End Get
        Set(value As Integer)
            Me.BinCountDictionary.Item(binNumber) = value
            Me.BinNumber = binNumber
        End Set
    End Property
    ''' <summary> The bin number. </summary>
    Private _BinNumber As Integer

    ''' <summary> Gets or sets the bin number. </summary>
    ''' <value> The bin number. </value>
    Public Property BinNumber As Integer
        Get
            Return Me._BinNumber
        End Get
        Set(value As Integer)
            Me._BinNumber = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

#End Region

End Class

''' <summary> Dictionary of bin counts. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/14/2017 </para>
''' </remarks>
<Serializable>
Public Class BinCountDictionary
    Inherits System.Collections.Concurrent.ConcurrentDictionary(Of Integer, Integer)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="enumeration"> An enum constant representing the enumeration option. </param>
    Public Sub New(ByVal enumeration As System.Enum)
        Me.New(enumeration.IntegerValues.ToList)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="binNumbers"> The bin numbers. </param>
    Public Sub New(ByVal binNumbers As IList(Of Integer))
        MyBase.New()
        Me.ResetKnownStateThis(binNumbers)
    End Sub

    ''' <summary> Gets the bin numbers. </summary>
    ''' <value> The bin numbers. </value>
    Public ReadOnly Property BinNumbers As ICollection(Of Integer)
        Get
            Return Me.Keys
        End Get
    End Property

    ''' <summary> Resets the known state described by binNumbers. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="binNumbers"> The bin numbers. </param>
    Private Sub ResetKnownStateThis(ByVal binNumbers As IList(Of Integer))
        For Each i As Integer In binNumbers
            Me.TryAdd(i, 0)
        Next
    End Sub

    ''' <summary> Clears the known state. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub ClearKnownState()
        For Each key As Integer In Me.Keys
            Me(key) = 0
        Next
    End Sub

    ''' <summary> Resets the known state described by binNumbers. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="binNumbers"> The bin numbers. </param>
    Public Sub ResetKnownState(ByVal binNumbers As IList(Of Integer))
        Me.Clear()
        Me.ResetKnownStateThis(binNumbers)
    End Sub

    ''' <summary> Gets the bin word. </summary>
    ''' <value> The bin word. </value>
    Public ReadOnly Property BinWord As Integer
        Get
            Dim result As Integer = 0
            For Each value As KeyValuePair(Of Integer, Integer) In Me
                If value.Value > 0 Then
                    result = result Or (2 >> value.Key)
                End If
            Next
            Return result
        End Get
    End Property

End Class

