Imports isr.Core.NumericExtensions

''' <summary>
''' Defines a <see cref="T:System.Single">Single</see> Line: y = slope * x + offset.
''' </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 09/19/2005, 1.0.2088.x1. </para>
''' </remarks>
Public Class LineF

#Region " SHARED "

    ''' <summary> Gets a new instance of the unit Line. </summary>
    ''' <value> A <see cref="LineF"/> value. </value>
    Public Shared ReadOnly Property Unity() As LineF
        Get
            Return New LineF(0, 0, 1, 1)
        End Get
    End Property

    ''' <summary> Returns the calculates offset, i.e., the pressure at zero volts. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="zero">  Zero span values. </param>
    ''' <param name="slope"> Full span values. </param>
    ''' <returns> null if it fails, else the calculated offset. </returns>
    Private Shared Function ComputeOffset(ByVal zero As PointF, ByVal slope As Double) As Double

        If zero Is Nothing Then
            Throw New ArgumentNullException(NameOf(zero))
        End If
        Return -slope * zero.X

    End Function

    ''' <summary> Returns the calculates slope as the ratio of pressure to voltage change. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="zero"> Zero span values. </param>
    ''' <param name="full"> Full span values. </param>
    ''' <returns> The calculated slope. </returns>
    Private Shared Function ComputeSlope(ByVal zero As PointF, ByVal full As PointF) As Double

        If zero Is Nothing Then
            Throw New ArgumentNullException(NameOf(zero))
        End If
        If full Is Nothing Then
            Throw New ArgumentNullException(NameOf(full))
        End If
        Return (full.Y - zero.Y) / (full.X - zero.X)

    End Function

    ''' <summary> Transposes the (x,y) line to a (y,x) line. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="line"> Specifies the <see cref="LineF">Line</see>
    '''                     to transpose. </param>
    ''' <returns> The transposed (y,x) line. </returns>
    Public Shared Function Transpose(ByVal line As LineF) As LineF

        If line Is Nothing Then
            Throw New ArgumentNullException(NameOf(line))
        End If
        Return New LineF(line.Origin.Y, line.Origin.X, line.Insertion.Y, line.Insertion.X)

    End Function

    ''' <summary> Computes the ordinate value for the given abscissa. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="abscissa"> The x coordinate. </param>
    ''' <param name="offset">   A <see cref="T:System.Double">Double</see> expression that specifies
    '''                         the offset of the line. </param>
    ''' <param name="slope">    Full span values. </param>
    ''' <returns> A Single. </returns>
    Public Shared Function Ordinate(ByVal abscissa As Single, ByVal offset As Double, ByVal slope As Double) As Single
        Return CSng(offset + abscissa * slope)
    End Function


#End Region

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a unity size instance of the <see cref="LineF" /> class. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub New()
        Me.New(PointF.Zero, PointF.Unity)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="LineF" /> class by its limits.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="x1"> A <see cref="T:System.Single">Single</see> expression that specifies the x
    '''                   element of the first (x,y) point defining the line. </param>
    ''' <param name="y1"> A <see cref="T:System.Single">Single</see> expression that specifies the y
    '''                   element of the first (x,y) point defining the line. </param>
    ''' <param name="x2"> A <see cref="T:System.Single">Single</see> expression that specifies the x
    '''                   element of the second (x,y) point defining the line. </param>
    ''' <param name="y2"> A <see cref="T:System.Single">Single</see> expression that specifies the y
    '''                   element of the second (x,y) point defining the line. </param>
    Public Sub New(ByVal x1 As Single, ByVal y1 As Single, ByVal x2 As Single, ByVal y2 As Single)
        Me.New()
        Me.SetLine(x1, y1, x2, y2)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="LineF" /> class by its slope and offset.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="slope">  A <see cref="T:System.Double">Double</see> expression that specifies the
    '''                       slope of the line. </param>
    ''' <param name="offset"> A <see cref="T:System.Double">Double</see> expression that specifies the
    '''                       offset of the line. </param>
    Public Sub New(ByVal slope As Double, ByVal offset As Double)
        Me.New()
        Me.SetLine(slope, offset)
    End Sub

    ''' <summary>
    ''' Constructs a <see cref="LineF"/> instance by its origin and insertion points.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="origin">    A <see cref="PointF">Point</see> expression that specifies the origin
    '''                          point of the line. </param>
    ''' <param name="insertion"> A <see cref="PointF">Point</see> expression that specifies the
    '''                          insertion (end) point of the line. </param>
    Public Sub New(ByVal origin As PointF, ByVal insertion As PointF)
        MyBase.New()
        Me.SetLine(origin, insertion)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="LineF" /> class. The Copy Constructor.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="model"> The LineF object from which to copy. </param>
    Public Sub New(ByVal model As LineF)
        Me.New()
        If model IsNot Nothing Then
            Me.SetLine(model._Origin, model._Insertion)
        End If
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, LineF))
    End Function

    ''' <summary>
    ''' Compares two lines. The lines are compared using the distance between their slope and offset.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="other"> Specifies the other <see cref="LineF">Line</see>
    '''                      to compare for equality with this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As LineF) As Boolean
        Return other IsNot Nothing AndAlso (Me.OffsetSlope.Equals(other.OffsetSlope) OrElse Me.OffsetSlope.Hypotenuse(other.OffsetSlope) < Single.Epsilon)
    End Function

    ''' <summary> Compares two lines. The lines are compared using their slopes and offsets. </summary>
    ''' <remarks>
    ''' The two lines are the same if the have the same minimum and maximum values.
    ''' </remarks>
    ''' <param name="other">     Specifies the <see cref="LineF">Line</see>
    '''                          to compare for equality with this instance. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two lines. The
    '''                          lines are compared based on their slope and offset.  The offset
    '''                          tolerance is based on it relative change from the Y range.  The
    '''                          tolerance if based on the reference line. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As LineF, ByVal tolerance As Single) As Boolean
        Return other IsNot Nothing AndAlso Me.OffsetSlope.Hypotenuse(other.OffsetSlope) < tolerance * (Me.OffsetSlope.Hypotenuse + other.OffsetSlope.Hypotenuse)
    End Function

    ''' <summary> Compares two lines. The lines are compared using their slopes and offsets. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="other">     Specifies the <see cref="LineF">Line</see>
    '''                                  to compare for equality with this instance. </param>
    ''' <param name="tolerance"> Specifies the relative tolerance for comparing the two lines. The
    '''                          lines are compared based on the distance between their slope and
    '''                          offset relative to the slope and offset size. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As LineF, ByVal tolerance As Double) As Boolean
        Return other IsNot Nothing AndAlso Me.OffsetSlope.Hypotenuse(other.OffsetSlope) < tolerance * (Me.OffsetSlope.Hypotenuse + other.OffsetSlope.Hypotenuse)
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As LineF, ByVal right As LineF) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As LineF, ByVal right As LineF) As Boolean
        Return ((CObj(left) IsNot CObj(right)) AndAlso (left Is Nothing OrElse Not left.Equals(right)))
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> An <see cref="T:System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.Origin.GetHashCode Xor Me.Insertion.GetHashCode
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Sets the Line based on the slope and offset values. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="slope">  A <see cref="T:System.Double">Double</see> expression that specifies the
    '''                       slope of the line. </param>
    ''' <param name="offset"> A <see cref="T:System.Double">Double</see> expression that specifies the
    '''                       offset of the line. </param>
    Public Overloads Sub SetLine(ByVal slope As Double, ByVal offset As Double)
        Me.SetLine(New PointF(0, Convert.ToSingle(offset)), New PointF(1, Convert.ToSingle(slope + offset)))
    End Sub

    ''' <summary> Sets the Line based its origin and insertion points. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="origin">    A <see cref="PointF">Point</see> expression that specifies the origin
    '''                          point of the line. </param>
    ''' <param name="insertion"> A <see cref="PointF">Point</see> expression that specifies the
    '''                          insertion (end) point of the line. </param>
    Public Overloads Sub SetLine(ByVal origin As PointF, ByVal insertion As PointF)
        If origin Is Nothing Then
            Throw New ArgumentNullException(NameOf(origin))
        End If
        If insertion Is Nothing Then
            Throw New ArgumentNullException(NameOf(insertion))
        End If
        Me._Origin = New PointF(origin)
        Me._Insertion = New PointF(insertion)
        Dim slope As Double = ComputeSlope(origin, insertion)
        Me._OffsetSlope = New System.Windows.Point(ComputeOffset(origin, slope), slope)
        Me._Size = New Windows.Point(insertion.X - origin.X, insertion.Y - origin.Y)
        Me._Length = CSng(Me.Size.Hypotenuse)
    End Sub

    ''' <summary> Sets the Line based on the slope and offset values. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="x1">     A <see cref="T:System.Single">Single</see> expression that specifies the
    '''                       x element of the first (x,y) point defining the line. </param>
    ''' <param name="x2">     A <see cref="T:System.Single">Single</see> expression that specifies the
    '''                       x element of the second (x,y) point defining the line. </param>
    ''' <param name="slope">  A <see cref="T:System.Double">Double</see> expression that specifies the
    '''                       slope of the line. </param>
    ''' <param name="offset"> A <see cref="T:System.Double">Double</see> expression that specifies the
    '''                       offset of the line. </param>
    Public Overloads Sub SetLine(ByVal x1 As Single, ByVal x2 As Single, ByVal slope As Double, ByVal offset As Double)

        Me.SetLine(New PointF(x1, Convert.ToSingle(x1 * slope + offset)), New PointF(x2, Convert.ToSingle(x2 * slope + offset)))

    End Sub

    ''' <summary> Sets the Line based on the slope and offset values. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="slope">  A <see cref="T:System.Double">Double</see> expression that specifies the
    '''                       slope of the line. </param>
    ''' <param name="offset"> A <see cref="T:System.Double">Double</see> expression that specifies the
    '''                       offset of the line. </param>
    ''' <param name="y1">     A <see cref="T:System.Single">Single</see> expression that specifies the
    '''                       x element of the first (x,y) point defining the line. </param>
    ''' <param name="y2">     A <see cref="T:System.Single">Single</see> expression that specifies the
    '''                       x element of the second (x,y) point defining the line. </param>
    Public Overloads Sub SetLine(ByVal slope As Double, ByVal offset As Double, ByVal y1 As Single, ByVal y2 As Single)

        If slope = 0 Then
            Me.SetLine(New PointF(0, 0), New PointF(1, Convert.ToSingle(offset)))
        Else
            Me.SetLine(New PointF(Convert.ToSingle((y1 - offset) / slope), y1), New PointF(Convert.ToSingle((y2 - offset) / slope), y2))
        End If

    End Sub

    ''' <summary> Sets the Line based on the values. </summary>
    ''' <remarks> Use this class to set the line. </remarks>
    ''' <param name="x1"> A <see cref="T:System.Single">Single</see> expression that specifies the x
    '''                   element of the first (x,y) point defining the line. </param>
    ''' <param name="y1"> A <see cref="T:System.Single">Single</see> expression that specifies the y
    '''                   element of the first (x,y) point defining the line. </param>
    ''' <param name="x2"> A <see cref="T:System.Single">Single</see> expression that specifies the x
    '''                   element of the second (x,y) point defining the line. </param>
    ''' <param name="y2"> A <see cref="T:System.Single">Single</see> expression that specifies the y
    '''                   element of the second (x,y) point defining the line. </param>
    Public Overloads Sub SetLine(ByVal x1 As Single, ByVal y1 As Single, ByVal x2 As Single, ByVal y2 As Single)
        Me.SetLine(New PointF(x1, y1), New PointF(x2, y2))
    End Sub

    ''' <summary> Returns the default string representation of the line. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> A representation of the line, e.g., '[(x1,y1)-(x2,y2)]' . </returns>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture,
                             "[({0},{1})-({2},{3})]",
                             Me.Origin.X.ToString(Globalization.CultureInfo.CurrentCulture),
                             Me.Origin.Y.ToString(Globalization.CultureInfo.CurrentCulture),
                             Me.Insertion.X.ToString(Globalization.CultureInfo.CurrentCulture),
                             Me.Insertion.Y.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the size. </summary>
    ''' <value> The size. </value>
    Public ReadOnly Property Size As System.Windows.Point

    ''' <summary> Gets or sets the length. </summary>
    ''' <value> The length. </value>
    Public ReadOnly Property Length As Double

    ''' <summary> The insertion. </summary>
    Private _Insertion As PointF

    ''' <summary> Gets or sets the origin point of the line. </summary>
    ''' <value> A <see cref="PointF">Point</see> property. </value>
    Public Property Insertion() As PointF
        Get
            Return Me._Insertion
        End Get
        Set(value As PointF)
            If value Is Nothing Then
                Me._Insertion = Nothing
            ElseIf Not value.Equals(Me.Insertion) Then
                Me.SetLine(Me.Origin, value)
            End If
        End Set
    End Property

    ''' <summary> The origin. </summary>
    Private _Origin As PointF

    ''' <summary> Gets or sets the origin point of the line. </summary>
    ''' <value> A <see cref="PointF">Point</see> property. </value>
    Public Property Origin() As PointF
        Get
            Return Me._Origin
        End Get
        Set(value As PointF)
            If value Is Nothing Then
                Me._Origin = Nothing
            ElseIf Not value.Equals(Me.Origin) Then
                Me.SetLine(value, Me.Insertion)
            End If
        End Set
    End Property
    ''' <summary> The offset slope. </summary>
    Private _OffsetSlope As System.Windows.Point

    ''' <summary> Gets the [offset,slope] as a <see cref="System.Windows.Point"/> . </summary>
    ''' <value> The slope offset. </value>
    Public ReadOnly Property OffsetSlope As System.Windows.Point
        Get
            Return Me._OffsetSlope
        End Get
    End Property

    ''' <summary>
    ''' Gets the Offset of the line y = slope * x + offset. This is the Y value at x
    ''' = 0.
    ''' </summary>
    ''' <value> A <see cref="T:System.Int32">Int32</see> property. </value>
    Public ReadOnly Property Offset() As Double
        Get
            Return Me._OffsetSlope.X
        End Get
    End Property

    ''' <summary> Gets the Slope of the line y = slope * x + offset. </summary>
    ''' <value> A <see cref="T:System.Int32">Int32</see> property. </value>
    Public ReadOnly Property Slope() As Double
        Get
            Return Me._OffsetSlope.Y
        End Get
    End Property

    ''' <summary> Returns the X line value for the give Y value. </summary>
    ''' <value> The x coordinate. </value>
    Public ReadOnly Property X(ByVal y As Single) As Single
        Get
            Return If(Me.Slope <> 0, Convert.ToSingle((y - Me.Offset) / Me.Slope), Convert.ToSingle(Me.Offset))
        End Get
    End Property

    ''' <summary> Returns the Y line value for the give X value. </summary>
    ''' <value> The y coordinate. </value>
    Public ReadOnly Property Y(ByVal x As Single) As Single
        Get
            Return LineF.Ordinate(x, Me.Offset, Me.Slope)
        End Get
    End Property

#End Region

End Class

