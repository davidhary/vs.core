#Disable Warning CA1036 ' Override methods on comparable types

''' <summary>
''' This structure encapsulates the Astronomical Julian Day and handles associated calculations
''' and conversions between various formats.
''' </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/2004, 1.0.1581.x. </para>
''' </remarks>
Public Structure JulianDate
#Enable Warning CA1036 ' Override methods on comparable types

    Implements ICloneable, IComparable

#Region " SHARED PROPERTIES "

    ''' <summary>Represents the zero JulianDate</summary>
    Public Const Zero As Double = 0

    ''' <summary>The Astronomical Julian Day number that corresponds to XL Date 0.0</summary>
    Public Const MSOfficeDayZero As Double = 2415018.5

    ''' <summary>The number of minutes in a second.</summary>
    Public Const MinutesPerSecond As Double = 1.0 / 60.0

    ''' <summary>The number of days in a second.</summary>
    Public Const DaysPerSecond As Double = 1.0 / 86400.0

    ''' <summary>The number of days in a Minute.</summary>
    Public Const DaysPerMinute As Double = 1.0 / 1440.0

    ''' <summary>The number of days in an hour.</summary>
    Public Const DaysPerHour As Double = 1.0 / 24.0

    ''' <summary>The number of days in a month.</summary>
    Public Const DaysPerMonth As Double = 365.0R / 12.0R

    ''' <summary>The number of days in a year.</summary>
    Public Const DaysPerYear As Double = 365.0R

    ''' <summary>The number of years in a month.</summary>
    Public Const YearsPerMonth As Double = 1 / 12.0

    ''' <summary>The number of months in a year</summary>
    Public Const MonthsPerYear As Double = 12

    ''' <summary>The number of hours in a day</summary>
    Public Const HoursPerDay As Double = 24

    ''' <summary>The number of minutes in an hour</summary>
    Public Const MinutesPerHour As Double = 60

    ''' <summary>The number of hours in a minute.</summary>
    Public Const HoursPerMinute As Double = 1 / 60

    ''' <summary>The number of seconds in a minute</summary>
    Public Const SecondsPerMinute As Double = 60

    ''' <summary>The number of minutes in a day</summary>
    Public Const MinutesPerDay As Double = 1440.0

    ''' <summary>The number of seconds in a day</summary>
    Public Const SecondsPerDay As Double = 86400.0

    ''' <summary>Gets or sets the default format string to be used in <see cref="ToString"/> when
    '''   no format is provided</summary>
    Public Shared ReadOnly DefaultFormatString As String = "&d-&mmm-&yy &hh:&nn"

#End Region

#Region " SHARED "

    ''' <summary> Add a specified time interval value. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="addTo">        The left-hand-side of the '+-' operator (A JulianDate class) </param>
    ''' <param name="valueToAdd">   . </param>
    ''' <param name="intervalType"> . </param>
    ''' <returns> The modified JulianDate. </returns>
    Public Overloads Shared Function Add(ByVal addTo As JulianDate, ByVal valueToAdd As Double, ByVal intervalType As TimeIntervalType) As JulianDate
        Select Case intervalType
            Case TimeIntervalType.Year
                addTo.AddYears(valueToAdd)
            Case TimeIntervalType.Month
                addTo.AddMonths(valueToAdd)
            Case TimeIntervalType.Day
                addTo.AddDays(valueToAdd)
            Case TimeIntervalType.Hour
                addTo.AddHours(valueToAdd)
            Case TimeIntervalType.Minute
                addTo.AddMinutes(valueToAdd)
            Case TimeIntervalType.Second
                addTo.AddSeconds(valueToAdd)
            Case Else
                Debug.Assert(Not Debugger.IsAttached, "unhandled interval type")
        End Select
        Return addTo

    End Function

    ''' <summary>
    ''' Returns the number of days per time interval.  An average year is 365 days of 12 months
    ''' giving about 30.41 days per month.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="timeInterval"> The <see cref="TimeIntervalType"/> </param>
    ''' <returns> Days per time interval. </returns>
    Public Shared Function GetDaysPerTimeInterval(ByVal timeInterval As TimeIntervalType) As Double
        Select Case timeInterval
            Case TimeIntervalType.Year
                Return DaysPerYear
            Case TimeIntervalType.Month
                Return DaysPerMonth
            Case TimeIntervalType.Day
                Return 1.0R
            Case TimeIntervalType.Hour
                Return JulianDate.DaysPerHour
            Case TimeIntervalType.Minute
                Return JulianDate.DaysPerMinute
            Case TimeIntervalType.Second
                Return JulianDate.DaysPerSecond
            Case Else
                Debug.Assert(Not Debugger.IsAttached, "unhandled interval type")
                Return 1.0R
        End Select
    End Function

    ''' <summary>
    ''' Calculate an Astronomical Julian Day Number from the specified Calendar date (year, month,
    ''' day, hour, minute, second).  Presumes that the calendar date is normalized, i.e., month is
    ''' between 1 and 12, minute is between 0 and 59, etc.
    ''' </summary>
    ''' <remarks> Taken from http://www.SRRB.noaa.gov/highlights/sunrise/program.txt. </remarks>
    ''' <param name="year">        Normalized <see cref="T:System.Integer">integer</see> year value
    '''                            (e.g., 1994). </param>
    ''' <param name="month">       Normalized <see cref="T:System.Integer">integer</see> month value
    '''                            (e.g., 7 for July). </param>
    ''' <param name="day">         Normalized <see cref="T:System.Integer">integer</see> day value
    '''                            (e.g., 19 for the 19th day of the month). </param>
    ''' <param name="hour">        Normalized <see cref="T:System.Integer">integer</see> hour value
    '''                            (e.g., 14 for 2:00 pm). </param>
    ''' <param name="minute">      Normalized <see cref="T:System.Integer">integer</see> minute
    '''                            value (e.g., 35 for 35 minutes past the hour). </param>
    ''' <param name="second">      Normalized <see cref="T:System.Integer">integer</see> second
    '''                            value (e.g., 42 for 42 seconds past the minute). </param>
    ''' <param name="millisecond"> Normalized <see cref="T:System.Integer">integer</see> millisecond
    '''                            value past the second. </param>
    ''' <returns> Astronomical Julian Day number. </returns>
    Public Shared Function FromNormalizedCalendarDate(ByVal year As Integer, ByVal month As Integer,
                                                      ByVal day As Integer, ByVal hour As Integer, ByVal minute As Integer,
                                                      ByVal second As Integer, ByVal millisecond As Integer) As Double
        If month <= 2 Then
            year -= 1
            month += 12
        End If
        Dim a As Double = Math.Floor(year / 100.0)
        Dim b As Double = 2 - a + Math.Floor(a / 4.0)
        Return Math.Floor(365.25 * (year + 4716.0)) + Math.Floor(30.6001 * (month + 1)) +
               day + b - 1524.5 + hour * DaysPerHour + minute * DaysPerMinute +
               (second + 0.001 * millisecond) * DaysPerSecond
    End Function

    ''' <summary>
    ''' Calculate an Astronomical Julian Day from the specified Calendar date (year, month, day, hour,
    ''' minute, second)
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="year">        The <see cref="T:System.Integer">integer</see> year value (e.g.,
    '''                            1994). </param>
    ''' <param name="month">       The <see cref="T:System.Integer">integer</see> month value (e.g.,
    '''                            7 for July). </param>
    ''' <param name="day">         The <see cref="T:System.Integer">integer</see> day value (e.g.,
    '''                            19 for the 19th day of the month). </param>
    ''' <param name="hour">        The <see cref="T:System.Integer">integer</see> hour value (e.g.,
    '''                            14 for 2:00 pm). </param>
    ''' <param name="minute">      The <see cref="T:System.Integer">integer</see> minute value (e.g.,
    '''                            35 for 35 minutes past the hour). </param>
    ''' <param name="second">      The <see cref="T:System.Integer">integer</see> second value (e.g.,
    '''                            42 for 42 seconds past the minute). </param>
    ''' <param name="millisecond"> The <see cref="T:System.Integer">integer</see> millisecond value
    '''                            past the second. </param>
    ''' <returns> Astronomical Julian Day as <see cref="T:System.Double"/>. </returns>
    Public Shared Function FromArbitraryCalendarDate(ByVal year As Integer,
                                                     ByVal month As Integer, ByVal day As Integer,
                                                     ByVal hour As Integer, ByVal minute As Integer,
                                                     ByVal second As Integer, ByVal millisecond As Integer) As Double

        ' Normalize the data to allow for negative and out of range values
        ' In this way, setting month to zero would be December of the previous year,
        ' setting hour to 24 would be the first hour of the next day, etc.
        JulianDate.NormalizeCalendarDate(year, month, day, hour, minute, second, millisecond)
        Return JulianDate.FromNormalizedCalendarDate(year, month, day, hour, minute, second, millisecond)

    End Function

    ''' <summary> Convert a .Net DateTime structure to Julian day. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> A <see cref="T:System.DateTime">DateTime</see> value. </param>
    ''' <returns> Julian Day as <see cref="T:System.Double"/>. </returns>
    Public Shared Function FromDateTime(ByVal value As DateTime) As Double
        Return JulianDate.FromNormalizedCalendarDate(value.Year, value.Month, value.Day, value.Hour,
                                                     value.Minute, value.Second, value.Millisecond)
    End Function

    ''' <summary>
    ''' Calculate a decimal year value (e.g., 1994.6523) corresponding to the specified XL date.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The decimal year value <see cref="T:System.Double">Double</see> type. </param>
    ''' <returns> Julian Day as <see cref="T:System.Double"/>. </returns>
    Public Shared Function FromDecimalYear(ByVal value As Double) As Double
        Dim year As Integer = Convert.ToInt32(Math.Floor(value)) ' was fix
        Dim julianDay1 As Double = JulianDate.FromNormalizedCalendarDate(year, 1, 1, 0, 0, 0, 0)
        Dim julianDay2 As Double = JulianDate.FromNormalizedCalendarDate(year + 1, 1, 1, 0, 0, 0, 0)
        Return (value - year) * (julianDay2 - julianDay1) + julianDay1
    End Function

    ''' <summary>
    ''' Calculate an XL Date corresponding to the specified Astronomical Julian Day number.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> Specifies an Excel type date. </param>
    ''' <returns> Julian Day as <see cref="T:System.Double"/>. </returns>
    Public Shared Function FromExcelDate(ByVal value As Double) As Double
        Return value + MSOfficeDayZero
    End Function

    ''' <summary>
    ''' Calculate a Calendar date (year, month, day, hour, minute, second) corresponding to the
    ''' Astronomical Julian Day number.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="dayValue">    The Astronomical Julian Day. </param>
    ''' <param name="year">        [in,out] The <see cref="T:System.Integer">integer</see> year
    '''                            value (e.g., 1994). </param>
    ''' <param name="month">       [in,out] The <see cref="T:System.Integer">integer</see> month
    '''                            value (e.g., 7 for July). </param>
    ''' <param name="day">         [in,out] The <see cref="T:System.Integer">integer</see> day value
    '''                            (e.g., 19 for the 19th day of the month). </param>
    ''' <param name="hour">        [in,out] The <see cref="T:System.Integer">integer</see> hour
    '''                            value (e.g., 14 for 2:00 pm). </param>
    ''' <param name="minute">      [in,out] The <see cref="T:System.Integer">integer</see> minute
    '''                            value (e.g., 35 for 35 minutes past the hour). </param>
    ''' <param name="second">      [in,out] The <see cref="T:System.Integer">integer</see> second
    '''                            value (e.g., 42 for 42 seconds past the minute). </param>
    ''' <param name="millisecond"> [in,out] <see cref="T:System.Integer">integer</see> millisecond
    '''                            value past the second. </param>
    Public Shared Sub ToCalendarDate(ByVal dayValue As Double, ByRef year As Integer,
                                     ByRef month As Integer, ByRef day As Integer, ByRef hour As Integer,
                                     ByRef minute As Integer, ByRef second As Integer, ByRef millisecond As Integer)

        Dim z As Double = Math.Floor(dayValue + 0.5)
        Dim f As Double = dayValue + 0.5 - z
        Dim alpha As Double
        Dim a As Double

        If (z < 2299161) Then
            a = z
        Else
            alpha = Math.Floor((z - 1867216.25) / 36524.25)
            a = z + 1.0 + alpha - Math.Floor(alpha / 4)
        End If

        Dim b As Double = a + 1524.0
        Dim c As Double = Math.Floor((b - 122.1) / 365.25)
        Dim d As Double = Math.Floor(365.25 * c)
        Dim e As Double = Math.Floor((b - d) / 30.6001)
        day = Convert.ToInt32(Math.Floor(b - d - Math.Floor(30.6001 * e) + f), Globalization.CultureInfo.CurrentCulture)

        month = If(e < 14.0,
            Convert.ToInt32(e - 1.0, Globalization.CultureInfo.CurrentCulture),
            Convert.ToInt32(e - 13.0, Globalization.CultureInfo.CurrentCulture))

        year = If(month > 2,
            Convert.ToInt32(c - 4716, Globalization.CultureInfo.CurrentCulture),
            Convert.ToInt32(c - 4715, Globalization.CultureInfo.CurrentCulture))

        ' get the day fraction offset by half a day because Julian day is offset by
        ' 12 hours
        Dim tempDay As Double = (dayValue - 0.5) - Math.Floor(dayValue - 0.5)
        'Dim days As integer = Convert.ToInt32(Math.Floor(tempDay))
        tempDay = (tempDay - Math.Floor(tempDay)) * HoursPerDay
        hour = Convert.ToInt32(Math.Floor(tempDay))
        tempDay = (tempDay - hour) * MinutesPerHour
        minute = Convert.ToInt32(Math.Floor(tempDay))
        tempDay = (tempDay - minute) * SecondsPerMinute
        second = Convert.ToInt32(Math.Floor(tempDay), Globalization.CultureInfo.CurrentCulture)
        tempDay = (tempDay - second) * 1000
        millisecond = Convert.ToInt32(Math.Floor(tempDay), Globalization.CultureInfo.CurrentCulture)

    End Sub

    ''' <summary>
    ''' Calculate a day-of-week value (e.g., Sun=0, Mon=1, Tue=2, etc.)
    ''' corresponding to the specified Julian Day.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="dayValue"> The Astronomical Julian Day. </param>
    ''' <returns> The corresponding day-of-week (DoW) value, expressed in integer format. </returns>
    Public Shared Function ToDayOfWeek(ByVal dayValue As Double) As Integer
        Return Convert.ToInt32(Math.Floor((dayValue + 1.5) Mod 7)) ' was fix
    End Function

    ''' <summary>
    ''' Calculate a day-of-year value (e.g., 241.543 corresponds to the 241st day of the year)
    ''' corresponding to the specified Julian Day.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="dayValue"> The Astronomical Julian Day. </param>
    ''' <returns> The corresponding day-of-year (DoY) value. </returns>
    Public Shared Function ToDayOfYear(ByVal dayValue As Double) As Double
        Dim year, month, day, hour, minute, second, millisecond As Integer
        JulianDate.ToCalendarDate(dayValue, year, month, day, hour, minute, second, millisecond)
        Return dayValue - JulianDate.FromNormalizedCalendarDate(year, 1, 1, 0, 0, 0, 0) + 1.0
    End Function

    ''' <summary> Convert a Julian Day to a .Net DateTime structure. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="dayValue"> The Astronomical Julian Day. </param>
    ''' <returns> <see cref="T:System.DateTime">DateTime</see> </returns>
    Public Shared Function ToDateTime(ByVal dayValue As Double) As DateTime
        Dim year, month, day, hour, minute, second, millisecond As Integer
        JulianDate.ToCalendarDate(dayValue, year, month, day, hour, minute, second, millisecond)
        Return New DateTime(year, month, day, hour, minute, second, millisecond)
    End Function

    ''' <summary>
    ''' Calculate a decimal year value (e.g., 1994.6523) Corresponding to the specified Julian Day.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="dayValue"> The Astronomical Julian Day. </param>
    ''' <returns> The decimal year value. </returns>
    Public Shared Function ToDecimalYear(ByVal dayValue As Double) As Double
        Dim year, month, day, hour, minute, second, millisecond As Integer
        JulianDate.ToCalendarDate(dayValue, year, month, day, hour, minute, second, millisecond)
        Dim jDay1 As Double = JulianDate.FromNormalizedCalendarDate(year, 1, 1, 0, 0, 0, 0)
        Dim jDay2 As Double = JulianDate.FromNormalizedCalendarDate(year + 1, 1, 1, 0, 0, 0, 0)
        Dim jDayMid As Double = JulianDate.FromNormalizedCalendarDate(year, month, day, hour, minute, second, millisecond)
        Return year + (jDayMid - jDay1) / (jDay2 - jDay1)
    End Function

    ''' <summary>
    ''' Calculate an Astronomical Julian Day number corresponding to the specified XL date.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="dayValue"> The Astronomical Julian Day. </param>
    ''' <returns> The Excel date <see cref="T:System.Double">Double</see> type. </returns>
    Public Shared Function ToExcelDate(ByVal dayValue As Double) As Double
        Return dayValue - MSOfficeDayZero
    End Function

    ''' <summary> Format the specified XL Date value using the specified format string. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="dayValue">   The Astronomical Julian Day. </param>
    ''' <param name="dateFormat"> The formatting string to be used for the date.  The following
    ''' formatting elements will be replaced with the corresponding date values:
    ''' <list type="table"> <listheader> <term>Variable</term>
    ''' <description>Description</description> </listheader><item><term>
    ''' &amp;mmmm</term><description>month name (e.g., January)</description></item><item><term>
    ''' &amp;mmm</term><description>month abbreviation (e.g., Apr)</description></item><item><term>
    ''' &amp;mm</term><description>padded month number (e.g. 04)</description></item><item><term>
    ''' &amp;m</term><description>non-padded month number (e.g., 4)</description></item><item><term>
    ''' &amp;dd</term><description>padded day number (e.g., 09)</description></item>  <item><term>
    ''' &amp;d</term><description>non-padded day number (e.g., 9)</description></item><item><term>
    ''' &amp;yyyy</term><description>4 digit year number (e.g., 1995)</description></item><item><term>
    ''' &amp;yy</term><description>two digit year number (e.g., 95)</description></item><item><term>
    ''' &amp;hh</term><description>padded 24 hour time value (e.g., 08)</description></item><item><term>
    ''' &amp;h</term><description>non-padded 12 hour time value (e.g., 8)</description></item><item><term>
    ''' &amp;nn</term><description>padded minute value (e.g, 05)</description></item><item><term>
    ''' &amp;n</term><description>non-padded minute value (e.g., 5)</description></item><item><term>
    ''' &amp;ss</term><description>padded second value (e.g., 03)</description></item><item><term>
    ''' &amp;s</term><description>non-padded second value (e.g., 3)</description></item><item><term>
    ''' &amp;a</term><description>"am" or "pm"</description></item><item><term>
    ''' &amp;wwww</term><description>day of week (e.g., Wednesday)</description></item><item><term>
    ''' &amp;www</term><description>day of week abbreviation (e.g., Wed)</description></item> </list> </param>
    ''' <returns> A <see cref="T:System.String">String</see> representation of the date. </returns>
    ''' <example> <para>
    ''' "&amp;wwww, &amp;mmmm &amp;dd, &amp;yyyy &amp;h:&amp;nn &amp;a" ==&gt; "Sunday, February 12, 1956 4:23 pm"</para> <para>
    ''' "&amp;dd-&amp;mmm-&amp;yy" ==&gt; 12-Feb-56</para></example>
    Public Overloads Shared Function ToString(ByVal dayValue As Double, ByVal dateFormat As String) As String

        If String.IsNullOrWhiteSpace(dateFormat) Then
            dateFormat = "MM/dd/yyyy"
        End If

        Dim longMonth As String() = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}
        Dim int16Month As String() = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}
        Dim longDoW As String() = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"}
        Dim int16DoW As String() = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"}

        Dim year, month, day, hour, minute, second, millisecond As Integer

        JulianDate.ToCalendarDate(dayValue, year, month, day, hour, minute, second, millisecond)

        Dim result As String = dateFormat.Replace("&mmmm", longMonth(month - 1))
        result = result.Replace("&mmm", int16Month(month - 1))
        result = result.Replace("&mm", month.ToString("d2", Globalization.CultureInfo.CurrentCulture))
        result = result.Replace("&m", month.ToString("d", Globalization.CultureInfo.CurrentCulture))
        result = result.Replace("&yyyy", year.ToString("d", Globalization.CultureInfo.CurrentCulture))
        result = result.Replace("&yy", (year Mod 100).ToString("d", Globalization.CultureInfo.CurrentCulture))
        result = result.Replace("&dd", day.ToString("d2", Globalization.CultureInfo.CurrentCulture))
        result = result.Replace("&d", day.ToString("d", Globalization.CultureInfo.CurrentCulture))
        result = result.Replace("&hh", hour.ToString("d2", Globalization.CultureInfo.CurrentCulture))
        result = result.Replace("&h", (((hour + 11) Mod 12) + 1).ToString("d", Globalization.CultureInfo.CurrentCulture))
        result = result.Replace("&nn", minute.ToString("d2", Globalization.CultureInfo.CurrentCulture))
        result = result.Replace("&n", minute.ToString("d", Globalization.CultureInfo.CurrentCulture))
        result = result.Replace("&ss", second.ToString("d2", Globalization.CultureInfo.CurrentCulture))
        result = result.Replace("&s", second.ToString("d", Globalization.CultureInfo.CurrentCulture))
        result = result.Replace("&mss", millisecond.ToString("d2", Globalization.CultureInfo.CurrentCulture))
        result = result.Replace("&ms", millisecond.ToString("d", Globalization.CultureInfo.CurrentCulture))
        result = If(hour >= 12, result.Replace("&a", "pm"), result.Replace("&a", "am"))
        result = result.Replace("&wwww", longDoW(JulianDate.ToDayOfWeek(dayValue)))
        result = result.Replace("&www", int16DoW(JulianDate.ToDayOfWeek(dayValue)))
        Return result

    End Function

    ''' <summary>
    ''' Format this JulianDate value using the default format string
    ''' (<see cref="DefaultFormatString"/>).
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="dayValue"> The Astronomical Julian Day. </param>
    ''' <returns> A <see cref="T:System.String">String</see> representation of the date. </returns>
    Public Overloads Shared Function ToString(ByVal dayValue As Double) As String
        Return JulianDate.ToString(dayValue, DefaultFormatString)
    End Function

    ''' <summary>
    ''' Normalize a set of Calendar date values (year, month, day, hour, minute, second) to make sure
    ''' that month is between 1 and 12, hour is between 0 and 23, etc.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="year">        [in,out] The <see cref="T:System.Integer">integer</see> year
    '''                            value (e.g., 1994). </param>
    ''' <param name="month">       [in,out] The <see cref="T:System.Integer">integer</see> month
    '''                            value (e.g., 7 for July). </param>
    ''' <param name="day">         [in,out] The <see cref="T:System.Integer">integer</see> day value
    '''                            (e.g., 19 for the 19th day of the month). </param>
    ''' <param name="hour">        [in,out] The <see cref="T:System.Integer">integer</see> hour
    '''                            value (e.g., 14 for 2:00 pm). </param>
    ''' <param name="minute">      [in,out] The <see cref="T:System.Integer">integer</see> minute
    '''                            value (e.g., 35 for 35 minutes past the hour). </param>
    ''' <param name="second">      [in,out] The <see cref="T:System.Integer">integer</see> second
    '''                            value (e.g., 42 for 42 seconds past the minute). </param>
    ''' <param name="millisecond"> [in,out] The <see cref="T:System.Integer">integer</see>
    '''                            millisecond value past the second. </param>
    Private Shared Sub NormalizeCalendarDate(ByRef year As Integer, ByRef month As Integer,
                                             ByRef day As Integer, ByRef hour As Integer, ByRef minute As Integer,
                                             ByRef second As Integer, ByRef millisecond As Integer)

        ' Normalize the data to allow for negative and out of range values
        ' In this way, setting month to zero would be December of the previous year,
        ' setting hour to 24 would be the first hour of the next day, etc.
        ' Normalize the seconds and carry over to minutes
        Dim carry As Double = Math.Floor(0.001 * millisecond)
        millisecond -= Convert.ToInt32(1000 * carry)
        second += Convert.ToInt32(carry)

        carry = Math.Floor(second * MinutesPerSecond)
        second -= Convert.ToInt32(carry * SecondsPerMinute)
        minute += Convert.ToInt32(carry)

        ' Normalize the minutes and carry over to hours
        carry = Math.Floor(minute * HoursPerMinute)
        minute -= Convert.ToInt32(carry * MinutesPerHour)
        hour += Convert.ToInt32(carry)

        ' Normalize the hours and carry over to days
        carry = Math.Floor(hour * DaysPerHour)
        hour -= Convert.ToInt32(carry * HoursPerDay)
        day += Convert.ToInt32(carry)

        ' Normalize the months and carry over to years
        carry = Math.Floor(month * YearsPerMonth)
        month -= Convert.ToInt32(carry * MonthsPerYear)
        year += Convert.ToInt32(carry)
    End Sub

#End Region

#Region " OPERATORS "

    ''' <summary>
    ''' '-' operator overload.  When two JulianDates are subtracted, the number of days between dates
    ''' is returned.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <param name="right"> The right-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <returns>
    ''' The days between dates, expressed as <see cref="T:System.Double">Double</see> type.
    ''' </returns>
    Public Overloads Shared Function Subtract(ByVal left As JulianDate, ByVal right As JulianDate) As Double
        Return left.JulianDay - right.JulianDay
    End Function

    ''' <summary>
    ''' '-' operator overload.  When two JulianDates are subtracted, the number of days between dates
    ''' is returned.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <param name="right"> The right-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <returns>
    ''' The days between dates, expressed as <see cref="T:System.Double">Double</see> type.
    ''' </returns>
    Public Overloads Shared Operator -(ByVal left As JulianDate, ByVal right As JulianDate) As Double
        Return left.JulianDay - right.JulianDay
    End Operator

    ''' <summary>
    ''' '-' operator overload.  When A <see cref="T:System.Double">Double</see> value is subtracted
    ''' from A JulianDate, the result is a new JulianDate with the number of days subtracted.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <param name="right"> The right-hand-side of the '-' operator (A
    '''                      <see cref="T:System.Double">Double</see> value) </param>
    ''' <returns> A JulianDate with the model number of days subtracted. </returns>
    Public Overloads Shared Operator -(ByVal left As JulianDate, ByVal right As Double) As JulianDate
        left.JulianDay -= right
        Return left
    End Operator

    ''' <summary>
    ''' '+' operator overload.  When A <see cref="T:System.Double">Double</see> value is added to A
    ''' JulianDate, the result is a new JulianDate with the number of days added.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <param name="right"> The right-hand-side of the '+' operator (A
    '''                      <see cref="T:System.Double">Double</see> value) </param>
    ''' <returns> A JulianDate with the model number of days added. </returns>
    Public Overloads Shared Operator +(ByVal left As JulianDate, ByVal right As Double) As JulianDate
        left.JulianDay += right
        Return left
    End Operator

    ''' <summary> '++' operator overload.  Increment the date by one day. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="julianDate"> The JulianDate structure on which to operate. </param>
    ''' <returns> A JulianDate one day later than the specified date. </returns>
    Public Overloads Shared Function PlusPlus(ByVal julianDate As JulianDate) As JulianDate
        julianDate.JulianDay += 1.0
        Return julianDate
    End Function

    ''' <summary> '--' operator overload.  Decrement the date by one day. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="julianDate"> The JulianDate structure on which to operate. </param>
    ''' <returns> A JulianDate one day prior to the specified date. </returns>
    Public Overloads Shared Function MinusMinus(ByVal julianDate As JulianDate) As JulianDate
        julianDate.JulianDay -= 1.0
        Return julianDate
    End Function

    ''' <summary> The less then operator overload.  The Julian days are compared. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <param name="right"> The right-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <returns>
    ''' <c>True</c> if the <para>left</para> Julian day is before the
    ''' <para>right</para> Julian day.
    ''' </returns>
    Public Overloads Shared Operator <(ByVal left As JulianDate, ByVal right As JulianDate) As Boolean
        Return left.JulianDay < right.JulianDay
    End Operator

    ''' <summary> The greater then operator overload.  The Julian days are compared. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <param name="right"> The right-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <returns>
    ''' <c>True</c> if the <para>left</para> Julian day is after the
    ''' <para>right</para> Julian day.
    ''' </returns>
    Public Overloads Shared Operator >(ByVal left As JulianDate, ByVal right As JulianDate) As Boolean
        Return left.JulianDay > right.JulianDay
    End Operator

#End Region

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs a date from the given date. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="julianDay"> Specifies the Julian day. </param>
    Public Sub New(ByVal julianDay As Double)
        Me._JulianDay = julianDay
    End Sub

    ''' <summary>
    ''' Constructs a date class from a calendar date (year, month, day).  Assumes the time of day is
    ''' 00:00 hrs.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="year">  A <see cref="T:System.Integer">integer</see> value for the year, e.g.,
    '''                      1995. </param>
    ''' <param name="month"> A <see cref="T:System.Integer">integer</see> value for the month of the
    '''                      year, e.g., 8 for August.  It is permissible to have months outside of
    '''                      the 1-12 range, which will rollover to the previous or next year. </param>
    ''' <param name="day">   A <see cref="T:System.Integer">integer</see> value for the day of the
    '''                      month,
    '''                      e.g., 23. It is permissible to have day numbers outside of the 1-31 range,
    '''                      which will rollover to the previous or next month and year. </param>
    Public Sub New(ByVal year As Integer, ByVal month As Integer, ByVal day As Integer)
        Me._JulianDay = JulianDate.FromArbitraryCalendarDate(year, month, day, 0, 0, 0, 0)
    End Sub

    ''' <summary>
    ''' Constructs a date class from a calendar date and time (year, month, day, hour, minute,
    ''' second).
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="year">        A <see cref="T:System.Integer">integer</see> value for the year,
    '''                            e.g., 1995. </param>
    ''' <param name="month">       A <see cref="T:System.Integer">integer</see> value for the month
    '''                            of the year, e.g., 8 for August.  It is permissible to have
    '''                            months outside of the 1-12 range, which will rollover to the
    '''                            previous or next year. </param>
    ''' <param name="day">         A <see cref="T:System.Integer">integer</see> value for the day of
    '''                            the month, e.g., 23. It is permissible to have day numbers
    '''                            outside of the 1-31 range, which will rollover to the previous or
    '''                            next month and year. </param>
    ''' <param name="hour">        A <see cref="T:System.Integer">integer</see> value for the hour
    '''                            of the day, e.g. 15. It is permissible to have hour values
    '''                            outside the 0-23 range, which will rollover to the previous or
    '''                            next day. </param>
    ''' <param name="minute">      A <see cref="T:System.Integer">integer</see> value for the minute,
    '''                            e.g. 45. It is permissible to have hour values outside the 0-59
    '''                            range, which will rollover to the previous or next hour. </param>
    ''' <param name="second">      A <see cref="T:System.Integer">integer</see> value for the second,
    '''                            e.g. 35. It is permissible to have second values outside the 0-59
    '''                            range, which will rollover to the previous or next minute. </param>
    ''' <param name="millisecond"> The <see cref="T:System.Integer">integer</see> millisecond value
    '''                            past the second. </param>
    Public Sub New(ByVal year As Integer, ByVal month As Integer, ByVal day As Integer,
                   ByVal hour As Integer, ByVal minute As Integer, ByVal second As Integer,
                   ByVal millisecond As Integer)
        Me._JulianDay = FromArbitraryCalendarDate(year, month, day, hour, minute, second, millisecond)
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="model"> The object from which to copy. </param>
    Public Sub New(ByVal model As JulianDate)
        Me._JulianDay = model._JulianDay
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return obj IsNot Nothing AndAlso (Object.ReferenceEquals(Me, obj) OrElse Me.Equals(CType(obj, JulianDate)))
    End Function

    ''' <summary> Indicates whether this instance and a specified object are equal. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <param name="right"> The right-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <returns>
    ''' <c>True</c> if <paramref name="left" /> and <paramref name="right" /> are the same type and
    ''' represent the same value; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Shared Function Equals(ByVal left As JulianDate, ByVal right As JulianDate) As Boolean
        Return left.JulianDay.Equals(right.JulianDay)
    End Function

    ''' <summary>
    ''' Returns True if the value of the <paramref name="other"/> equals to the instance value.
    ''' </summary>
    ''' <remarks>
    ''' Julian Dates are the same if the have the same
    ''' <see cref="JulianDay"/> values.
    ''' </remarks>
    ''' <param name="other"> The other <see cref="JulianDate">JulianDate</see> to compare for equality
    '''                      with this instance. </param>
    ''' <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
    Public Overloads Function Equals(ByVal other As JulianDate) As Boolean
        Return Me.JulianDay.Equals(other.JulianDay)
    End Function

    ''' <summary> = casting operator. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <param name="right"> The right-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As JulianDate, ByVal right As JulianDate) As Boolean
        Return JulianDate.Equals(left, right)
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <param name="right"> The right-hand-side of the '-' operator (A JulianDate class) </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As JulianDate, ByVal right As JulianDate) As Boolean
        Return Not JulianDate.Equals(left, right)
    End Operator

#End Region

#Region " METHODS "

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> A new, independent copy of the JulianDate. </returns>
    Public Function Clone() As Object Implements ICloneable.Clone
        Return Me.Copy()
    End Function

    ''' <summary> Deep-copy clone routine. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> A new, independent copy of the JulianDate. </returns>
    Public Function Copy() As JulianDate
        Return New JulianDate(Me)
    End Function

    ''' <summary>
    ''' Format this JulianDate value using the default format string
    ''' (<see cref="DefaultFormatString"/>).
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> A <see cref="T:System.String">String</see> representation of the date. </returns>
    Public Overloads Overrides Function ToString() As String
        Return JulianDate.ToString(Me.JulianDay, DefaultFormatString)
    End Function

    ''' <summary> Format this JulianDate value using the specified format string. </summary>
    ''' <param name="dateFormat"> The formatting string to be used for the date.  The following
    ''' formatting elements will be replaced with the corresponding date values:
    ''' <list type="table"> <listheader> <term>Variable</term>
    ''' <description>Description</description> </listheader><item><term>
    ''' &amp;mmmm</term><description>month name (e.g., January)</description></item><item><term>
    ''' &amp;mmm</term><description>month abbreviation (e.g., Apr)</description></item><item><term>
    ''' &amp;mm</term><description>padded month number (e.g. 04)</description></item><item><term>
    ''' &amp;m</term><description>non-padded month number (e.g., 4)</description></item><item><term>
    ''' &amp;dd</term><description>padded day number (e.g., 09)</description></item>  <item><term>
    ''' &amp;d</term><description>non-padded day number (e.g., 9)</description></item><item><term>
    ''' &amp;yyyy</term><description>4 digit year number (e.g., 1995)</description></item><item><term>
    ''' &amp;yy</term><description>two digit year number (e.g., 95)</description></item><item><term>
    ''' &amp;hh</term><description>padded 24 hour time value (e.g., 08)</description></item><item><term>
    ''' &amp;h</term><description>non-padded 12 hour time value (e.g., 8)</description></item><item><term>
    ''' &amp;nn</term><description>padded minute value (e.g, 05)</description></item><item><term>
    ''' &amp;n</term><description>non-padded minute value (e.g., 5)</description></item><item><term>
    ''' &amp;ss</term><description>padded second value (e.g., 03)</description></item><item><term>
    ''' &amp;s</term><description>non-padded second value (e.g., 3)</description></item><item><term>
    ''' &amp;a</term><description>"am" or "pm"</description></item><item><term>
    ''' &amp;wwww</term><description>day of week (e.g., Wednesday)</description></item><item><term>
    ''' &amp;www</term><description>day of week abbreviation (e.g., Wed)</description></item> </list> </param>
    ''' <returns> A <see cref="T:System.String">String</see> representation of the date. </returns>
    ''' <example> <para>
    ''' "&amp;wwww, &amp;mmmm &amp;dd, &amp;yyyy &amp;h:&amp;nn &amp;a" ==&gt; "Sunday, February 12, 1956 4:23 pm"</para> <para>
    ''' "&amp;dd-&amp;mmm-&amp;yy" ==&gt; 12-Feb-56</para></example>
    Public Overloads Function ToString(ByVal dateFormat As String) As String
        Return JulianDate.ToString(Me.JulianDay, dateFormat)
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets or sets the astronomical Julian day. </summary>
    ''' <remarks>
    ''' The Astronomical Julian Day number is defined as the number of days since noon on January 1st,
    ''' 4713 B.C. (also referred to as 12:00 on January 1, -4712).
    ''' </remarks>
    ''' <value> The Julian day. </value>
    Public Property JulianDay() As Double

    ''' <summary> Gets or sets the date value in Excel Date format. </summary>
    ''' <remarks>
    ''' This format is in days since a reference date
    ''' <see cref="MSOfficeDayZero"/>, on January 1, 1900 at 00:00 hrs.  Negative values
    ''' are permissible, and the range of valid dates is from noon on January 1st, 4713 B.C. forward.
    ''' </remarks>
    ''' <value> The excel date. </value>
    Public Property ExcelDate() As Double
        Get
            Return JulianDate.ToExcelDate(Me.JulianDay)
        End Get
        Set(ByVal value As Double)
            Me._JulianDay = JulianDate.FromExcelDate(value)
        End Set
    End Property

    ''' <summary> Gets or sets the date value in .Net DateTime format. </summary>
    ''' <value> The date time. </value>
    Public Property DateTime() As DateTime
        Get
            Return JulianDate.ToDateTime(Me.JulianDay)
        End Get
        Set(ByVal value As DateTime)
            Me._JulianDay = JulianDate.FromDateTime(value)
        End Set
    End Property

    ''' <summary> Gets or sets the decimal year number (i.e., 1997.345). </summary>
    ''' <value> The decimal year. </value>
    Public Property DecimalYear() As Double
        Get
            Return JulianDate.ToDecimalYear(Me.JulianDay)
        End Get
        Set(ByVal value As Double)
            Me._JulianDay = JulianDate.FromDecimalYear(value)
        End Set
    End Property

    ''' <summary> Gets the calendar date (year, month, day). </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="year">  [in,out] A <see cref="T:System.Integer">integer</see> value for the year,
    '''                      e.g., 1995. </param>
    ''' <param name="month"> [in,out] A <see cref="T:System.Integer">integer</see> value for the month
    '''                      of the year, e.g., 8 for August. </param>
    ''' <param name="day">   [in,out] A <see cref="T:System.Integer">integer</see> value for the day
    '''                      of the month, e.g., 23. </param>
    Public Overloads Sub GetDate(ByRef year As Integer, ByRef month As Integer, ByRef day As Integer)
        Dim hour, minute, second, millisecond As Integer
        JulianDate.ToCalendarDate(Me.JulianDay, year, month, day, hour, minute, second, millisecond)
    End Sub

    ''' <summary> Set the date from the calendar date (year, month, day). </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="year">  A <see cref="T:System.Integer">integer</see> value for the year, e.g.,
    '''                      1995. </param>
    ''' <param name="month"> A <see cref="T:System.Integer">integer</see> value for the month of the
    '''                      year, e.g., 8 for August. </param>
    ''' <param name="day">   A <see cref="T:System.Integer">integer</see> value for the day of the
    '''                      month,
    '''                      e.g., 23. </param>
    Public Overloads Sub SetDate(ByVal year As Integer, ByVal month As Integer, ByVal day As Integer)
        Me._JulianDay = JulianDate.FromArbitraryCalendarDate(year, month, day, 0, 0, 0, 0)
    End Sub

    ''' <summary> Get the calendar date (year, month, day, hour, minute, second). </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="year">        [in,out] A <see cref="T:System.Integer">integer</see> value for
    '''                            the year, e.g., 1995. </param>
    ''' <param name="month">       [in,out] A <see cref="T:System.Integer">integer</see> value for
    '''                            the month of the year, e.g., 8 for August. </param>
    ''' <param name="day">         [in,out] A <see cref="T:System.Integer">integer</see> value for
    '''                            the day of the month, e.g., 23. </param>
    ''' <param name="hour">        [in,out] A <see cref="T:System.Integer">integer</see> value for
    '''                            the hour of the day, e.g. 15. </param>
    ''' <param name="minute">      [in,out] A <see cref="T:System.Integer">integer</see> value for
    '''                            the minute, e.g. 45. </param>
    ''' <param name="second">      [in,out] A <see cref="T:System.Integer">integer</see> value for
    '''                            the second, e.g. 35. </param>
    ''' <param name="millisecond"> <see cref="T:System.Integer">integer</see> millisecond value past
    '''                            the second. </param>
    Public Overloads Sub GetDate(ByRef year As Integer, ByRef month As Integer, ByRef day As Integer, ByRef hour As Integer,
                                 ByRef minute As Integer, ByRef second As Integer, ByVal millisecond As Integer)
        JulianDate.ToCalendarDate(Me.JulianDay, year, month, day, hour, minute, second, millisecond)
    End Sub

    ''' <summary>
    ''' Set the date based on the calendar date (year, month, day, hour, minute, second).
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="year">        A <see cref="T:System.Integer">integer</see> value for the year,
    '''                            e.g., 1995. </param>
    ''' <param name="month">       A <see cref="T:System.Integer">integer</see> value for the month
    '''                            of the year, e.g., 8 for August. </param>
    ''' <param name="day">         A <see cref="T:System.Integer">integer</see> value for the day of
    '''                            the month, e.g., 23. </param>
    ''' <param name="hour">        A <see cref="T:System.Integer">integer</see> value for the hour
    '''                            of the day, e.g. 15. </param>
    ''' <param name="minute">      A <see cref="T:System.Integer">integer</see> value for the minute,
    '''                            e.g. 45. </param>
    ''' <param name="second">      A <see cref="T:System.Integer">integer</see> value for the second,
    '''                            e.g. 35. </param>
    ''' <param name="millisecond"> The <see cref="T:System.Integer">integer</see> millisecond value
    '''                            past the second. </param>
    Public Overloads Sub SetDate(ByVal year As Integer, ByVal month As Integer, ByVal day As Integer, ByVal hour As Integer,
                                 ByVal minute As Integer, ByVal second As Integer, ByVal millisecond As Integer)
        Me._JulianDay = JulianDate.FromArbitraryCalendarDate(year, month, day, hour, minute, second, millisecond)
    End Sub

    ''' <summary>
    ''' Get the day of year value (241.345 means the 241st day of the year)
    ''' corresponding to this instance.
    ''' </summary>
    ''' <value> The day of the year <see cref="T:System.Double">Double</see> type. </value>
    Public ReadOnly Property DayOfYear() As Double
        Get
            Return JulianDate.ToDayOfYear(Me.JulianDay)
        End Get
    End Property

#End Region

#Region " Add Routines "

    ''' <summary> Add the specified number of milliseconds (can be fractional). </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="milliseconds"> The incremental number of seconds (negative or positive)
    '''                             <see cref="T:System.Double">Double</see> type. </param>
    Public Sub AddMilliseconds(ByVal milliseconds As Double)
        Me._JulianDay += 0.001 * milliseconds * DaysPerSecond
    End Sub

    ''' <summary> Add the specified number of seconds (can be fractional). </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="seconds"> The incremental number of seconds (negative or positive)
    '''                        <see cref="T:System.Double">Double</see> type. </param>
    Public Sub AddSeconds(ByVal seconds As Double)
        Me._JulianDay += seconds * DaysPerSecond
    End Sub

    ''' <summary> Add the specified number of minutes (can be fractional). </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="minutes"> The incremental number of minutes (negative or positive)
    '''                        <see cref="T:System.Double">Double</see> type. </param>
    Public Sub AddMinutes(ByVal minutes As Double)
        Me._JulianDay += minutes * DaysPerMinute
    End Sub

    ''' <summary> Add the specified number of hours (can be fractional). </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="hours"> The incremental number of hours (negative or positive)
    '''                      <see cref="T:System.Double">Double</see> type. </param>
    Public Sub AddHours(ByVal hours As Double)
        Me._JulianDay += hours * DaysPerHour
    End Sub

    ''' <summary> Add the specified number of days (can be fractional). </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="days"> The incremental number of days (negative or positive)
    '''                     <see cref="T:System.Double">Double</see> type. </param>
    Public Sub AddDays(ByVal days As Double)
        Me._JulianDay += days
    End Sub

    ''' <summary> Add the specified number of Months (can be fractional). </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="months"> The incremental number of months (negative or positive)
    '''                       <see cref="T:System.Double">Double</see> type. </param>
    Public Sub AddMonths(ByVal months As Double)

        Dim iMon As Integer = Convert.ToInt32(Math.Floor(months)) ' was fix
        Dim monFrac As Double = Math.Abs(months - iMon)
        Dim sMon As Integer = Math.Sign(months)
        Dim year, month, day, hour, minute, second, millisecond As Integer
        JulianDate.ToCalendarDate(Me.JulianDay, year, month, day, hour, minute, second, millisecond)
        If iMon <> 0 Then
            month += iMon
            Me._JulianDay = JulianDate.FromArbitraryCalendarDate(year, month, day, hour, minute, second, millisecond)
        End If

        If sMon <> 0 Then
            Dim xlDate2 As Double = JulianDate.FromArbitraryCalendarDate(year, month + sMon, day, hour, minute, second, millisecond)
            Me._JulianDay += (xlDate2 - Me.JulianDay) * monFrac
        End If
    End Sub

    ''' <summary> Add the specified number of years (can be fractional). </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="years"> The incremental number of years (negative or positive)
    '''                      <see cref="T:System.Double">Double</see> type. </param>
    Public Sub AddYears(ByVal years As Double)

        Dim iYear As Integer = Convert.ToInt32(Math.Floor(years)) ' was Fix
        Dim yearFrac As Double = Math.Abs(years - iYear)
        Dim sYear As Integer = Math.Sign(years)
        Dim year, month, day, hour, minute, second, millisecond As Integer

        JulianDate.ToCalendarDate(Me.JulianDay, year, month, day, hour, minute, second, millisecond)

        If iYear <> 0 Then
            year += iYear
            Me._JulianDay = JulianDate.FromArbitraryCalendarDate(year, month, day, hour, minute, second, millisecond)
        End If

        If sYear <> 0 Then
            Dim xlDate2 As Double = JulianDate.FromArbitraryCalendarDate(year + sYear, month, day, hour, minute, second, millisecond)
            Me._JulianDay += (xlDate2 - Me.JulianDay) * yearFrac
        End If

    End Sub

#End Region

#Region " IComparable implementation "

    ''' <summary>
    ''' Compares the current instance with another object of the same type and returns an integer
    ''' that indicates whether the current instance precedes, follows, or occurs in the same position
    ''' in the sort order as the other object.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="obj"> The object to compare for equality with this instance. This object should
    '''                    be type <see cref="JulianDate"/>. </param>
    ''' <returns>
    ''' A value that indicates the relative order of the objects being compared. The return value has
    ''' these meanings: Value Meaning Less than zero This instance is less than
    ''' <paramref name="obj" />. Zero This instance is equal to <paramref name="obj" />. Greater than
    ''' zero This instance is greater than <paramref name="obj" />.
    ''' </returns>
    Public Overloads Function CompareTo(ByVal obj As Object) As Integer Implements IComparable.CompareTo
        If TypeOf obj Is JulianDate Then
            Return CType(obj, JulianDate).JulianDay.CompareTo(Me.JulianDay)
        ElseIf TypeOf obj Is Double Then
            Return Convert.ToDouble(obj, Globalization.CultureInfo.CurrentCulture).CompareTo(Me.JulianDay)
        Else
            Return 0
        End If

    End Function

    ''' <summary>
    ''' Returns the hash code for this <see cref="JulianDate"/> structure. In this case, the hash
    ''' code is simply the equivalent hash code for the ,<see cref="JulianDate.JulianDay"/>
    ''' value.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
    Public Overrides Function GetHashCode() As Integer
        Return Me.JulianDay.GetHashCode()
    End Function

#End Region

End Structure

#Region " TYPES "

''' <summary> Enumeration type for date and time interval types. </summary>
''' <remarks> David, 2020-09-22. </remarks>
Public Enum TimeIntervalType
    ''' <summary> . </summary>
    <System.ComponentModel.Description("Year")> Year
    ''' <summary> . </summary>
    <System.ComponentModel.Description("Month")> Month
    ''' <summary> . </summary>
    <System.ComponentModel.Description("Day")> Day
    ''' <summary> . </summary>
    <System.ComponentModel.Description("Hour")> Hour
    ''' <summary> . </summary>
    <System.ComponentModel.Description("Minute")> Minute
    ''' <summary> . </summary>
    <System.ComponentModel.Description("Second")> Second
End Enum

#End Region

