''' <summary> Defines a <see cref="T:System.TimeSpan">TimeSpan</see> range class. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 04/30/2004, 1.0.1581. Create. </para>
''' </remarks>
Public Class RangeTimeSpan

#Region " SHARED "

    ''' <summary> Gets the empty range. </summary>
    ''' <value> A <see cref="T:System.TimeSpan.MinValue"/> value with
    ''' <see cref="T:System.TimeSpan.MinValue"/>
    ''' minimum value and <see cref="T:System.TimeSpan.MinValue"/> for the maximum value. </value>
    Public Shared ReadOnly Property [Empty]() As RangeTimeSpan
        Get
            Return New RangeTimeSpan(TimeSpan.MinValue, TimeSpan.MinValue)
        End Get
    End Property

    ''' <summary> Gets the Unity range. </summary>
    ''' <value> A <see cref="RangeTimeSpan"/> [0,1] value. </value>
    Public Shared ReadOnly Property Unity() As RangeTimeSpan
        Get
            Return New RangeTimeSpan(TimeSpan.Zero, TimeSpan.FromSeconds(1))
        End Get
    End Property

    ''' <summary> Gets the zero range value. </summary>
    ''' <value> A <see cref="RangeTimeSpan"/> value. </value>
    Public Shared ReadOnly Property Zero() As RangeTimeSpan
        Get
            Return New RangeTimeSpan(TimeSpan.Zero, TimeSpan.Zero)
        End Get
    End Property

    ''' <summary> Return the range of the specified data array. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="values"> The data array. </param>
    ''' <returns> The calculated range. </returns>
    Public Shared Function GetRange(ByVal values As TimeSpan()) As RangeTimeSpan
        ' return the unit range if no data
        If values Is Nothing OrElse Not values.Any Then Return RangeTimeSpan.Unity
        Dim min As TimeSpan = TimeSpan.MaxValue
        Dim max As TimeSpan = TimeSpan.MinValue
        For Each temp As TimeSpan In values
            If temp < min Then
                min = temp
            ElseIf temp > max Then
                max = temp
            End If
        Next
        Return New RangeTimeSpan(min, max)
    End Function

    ''' <summary> Return the range of the specified data array. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="values"> The data array. </param>
    ''' <returns> The calculated range. </returns>
    Public Shared Function GetRange(ByVal values As IEnumerable(Of TimeSpan)) As RangeTimeSpan
        ' return the unit range if no data
        If values Is Nothing OrElse Not values.Any Then Return RangeTimeSpan.Unity
        Dim min As TimeSpan = TimeSpan.MaxValue
        Dim max As TimeSpan = TimeSpan.MinValue
        For Each temp As TimeSpan In values
            If temp < min Then
                min = temp
            ElseIf temp > max Then
                max = temp
            End If
        Next
        Return New RangeTimeSpan(min, max)
    End Function

    ''' <summary> Return the range of the specified data array. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="values"> The data array. </param>
    ''' <returns> The calculated range. </returns>
    Public Shared Function GetRange(ByVal values As IList(Of TimeSpan)) As RangeTimeSpan
        ' return the unit range if no data
        If values Is Nothing OrElse Not values.Any Then Return RangeTimeSpan.Unity
        Dim min As TimeSpan = TimeSpan.MaxValue
        Dim max As TimeSpan = TimeSpan.MinValue
        For Each temp As TimeSpan In values
            If temp < min Then
                min = temp
            ElseIf temp > max Then
                max = temp
            End If
        Next
        Return New RangeTimeSpan(min, max)
    End Function

    ''' <summary> Extended range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="range"> A <see cref="RangeTimeSpan"/> value. </param>
    ''' <returns> A RangeTimeSpan. </returns>
    Public Function ExtendedRange(ByVal range As RangeTimeSpan) As RangeTimeSpan
        Dim result As New RangeTimeSpan(Me)
        result.ExtendRange(range)
        Return result
    End Function

    ''' <summary> Shifted range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A RangeTimeSpan. </returns>
    Public Function ShiftedRange(ByVal value As TimeSpan) As RangeTimeSpan
        Dim result As New RangeTimeSpan(Me)
        result.ShiftRange(value)
        Return result
    End Function

    ''' <summary> Transposed range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="shift"> The shift. </param>
    ''' <param name="scale"> The scale. </param>
    ''' <returns> A RangeTimeSpan. </returns>
    Public Function TransposedRange(ByVal shift As TimeSpan, ByVal scale As Double) As RangeTimeSpan
        Dim result As New RangeTimeSpan(Me)
        result.TransposeRange(shift, scale)
        Return result
    End Function

#End Region

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs a <see cref="RangeTimeSpan"/> instance by its limits. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="minValue"> A <see cref="T:System.TimeSpan">TimeSpan</see> expression that
    '''                         specifics the minimum range. </param>
    ''' <param name="maxValue"> A <see cref="T:System.TimeSpan">TimeSpan</see> expression that
    '''                         specifics the maximum range. </param>
    Public Sub New(ByVal minValue As TimeSpan, ByVal maxValue As TimeSpan)
        MyBase.New()
        Me.SetRange(minValue, maxValue)
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="model"> The RangeTimeSpan object from which to copy. </param>
    Public Sub New(ByVal model As RangeTimeSpan)
        MyBase.New()
        If model IsNot Nothing Then
            Me.SetRange(model._Min, model._Max)
        End If
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As RangeTimeSpan, ByVal right As RangeTimeSpan) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return RangeTimeSpan.Equals(left, right)
        End If
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As RangeTimeSpan, ByVal right As RangeTimeSpan) As Boolean
        Return Not RangeTimeSpan.Equals(left, right)
    End Operator

    ''' <summary> Returns True if equal. </summary>
    ''' <remarks>
    ''' Range Time Spans are the same if the have the same
    ''' <see cref="Type">min</see> and <see cref="Type">max</see> values.
    ''' </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> <c>True</c> if equals. </returns>
    Public Overloads Shared Function Equals(ByVal left As RangeTimeSpan, ByVal right As RangeTimeSpan) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left.Max.Equals(right.Max) AndAlso left.Min.Equals(right.Min)
        End If
    End Function

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, RangeTimeSpan))
    End Function

    ''' <summary>
    ''' Returns True if the value of the <paramref name="other"/> equals to the instance value.
    ''' </summary>
    ''' <remarks>
    ''' Range Time Spans are the same if the have the same
    ''' <see cref="Type">min</see> and <see cref="Type">max</see> values.
    ''' </remarks>
    ''' <param name="other"> The other <see cref="RangeTimeSpan">Range Time Span</see> to compare for
    '''                      equality with this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As RangeTimeSpan) As Boolean
        Return other IsNot Nothing AndAlso RangeTimeSpan.Equals(Me, other)
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Clips the given value. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A Date. </returns>
    Public Function Clip(ByVal value As TimeSpan) As TimeSpan
        Return If(value < Me.Min, Me.Min, If(value > Me.Max, Me.Max, value))
    End Function

    ''' <summary> Returns true if the point value is within the range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="point"> A <see cref="T:System.Date">Date</see> point value&gt; </param>
    ''' <returns>
    ''' <c>True</c> if value above or equal to minimum or below or equal to maximum.
    ''' </returns>
    Public Function Contains(ByVal point As TimeSpan) As Boolean
        Return point.CompareTo(Me.Min) >= 0 AndAlso point.CompareTo(Me.Max) <= 0
    End Function

    ''' <summary> Query if 'point' is inside the range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="point"> A <see cref="T:System.TimeSpan">Time Span</see> point value&gt; </param>
    ''' <returns> <c>true</c> if inside; otherwise <c>false</c> </returns>
    Public Function Encloses(ByVal point As TimeSpan) As Boolean
        Return (point > Me.Min) AndAlso (point < Me.Max)
    End Function

    ''' <summary> Returns true if the point value is within the range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="point">     A <see cref="T:System.Date">Date</see> point value&gt; </param>
    ''' <param name="tolerance"> Tolerance for comparison. </param>
    ''' <returns>
    ''' <c>True</c> if value above or equal to minimum - tolerance or below or equal to maximum +
    ''' tolerance.
    ''' </returns>
    Public Function Contains(ByVal point As TimeSpan, ByVal tolerance As TimeSpan) As Boolean
        Return (point.Subtract(Me.Min.Subtract(tolerance)).Ticks >= 0) AndAlso
               Me.Max.Subtract(point.Subtract(tolerance)).Ticks >= 0
    End Function

    ''' <summary>
    ''' Extend this range to include both its present values and the specified range.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="range"> A <see cref="RangeTimeSpan"/> value. </param>
    ''' <returns> Extended range. </returns>
    Public Function ExtendRange(ByVal range As RangeTimeSpan) As RangeTimeSpan
        If range Is Nothing Then Throw New ArgumentNullException(NameOf(range))
        If Me.Min.CompareTo(range.Min) > 0 Then Me.Min = range.Min
        If Me.Max.CompareTo(range.Max) < 0 Then Me.Max = range.Max
        Return Me
    End Function

    ''' <summary> Shift range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value by which to shift the range. </param>
    Public Sub ShiftRange(ByVal value As TimeSpan)
        Me.SetRange(Me.Min.Add(value), Me.Max.Add(value))
    End Sub

    ''' <summary> Transpose range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="shift"> The shift. </param>
    ''' <param name="scale"> The scale. </param>
    Public Sub TransposeRange(ByVal shift As TimeSpan, ByVal scale As Double)
        Dim newSpan As TimeSpan = TimeSpan.FromTicks(CLng(scale * Me.Span.Ticks))
        Dim newMin As TimeSpan = Me.Min.Add(shift)
        Me.SetRange(newMin, newMin.Add(newSpan))
    End Sub

    ''' <summary> Sets the range based on the extrema. </summary>
    ''' <remarks> Use this class to set the range. </remarks>
    ''' <param name="minValue"> A <see cref="T:System.TimeSpan">TimeSpan</see> expression that
    '''                         specifies the minimum value of the range. </param>
    ''' <param name="maxValue"> A <see cref="T:System.TimeSpan">TimeSpan</see> expression that
    '''                         specifies the maximum value of the range. </param>
    Public Overloads Sub SetRange(ByVal minValue As TimeSpan, ByVal maxValue As TimeSpan)
        Me.Min = minValue
        Me.Max = maxValue
    End Sub

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me.Min.GetHashCode Xor Me.Max.GetHashCode
    End Function

    ''' <summary> Returns the default string representation of the range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> A representation of the range, e.g., '[x,y]' . </returns>
    Public Overrides Function ToString() As String
        Return $"[{Me.Min},{Me.Max}]"
    End Function

    ''' <summary> Returns the default string representation of the range. </summary>
    ''' <remarks> David, 2020-04-13. </remarks>
    ''' <param name="format"> Describes the format to use, e.g., '"d\.hh\:mm\:ss\.fff"'. </param>
    ''' <returns> A representation of the range, e.g., '[x,y]' . </returns>
    Public Overloads Function ToString(ByVal format As String) As String
        Return $"[{Me.Min.ToString(format)},{Me.Max.ToString(format)}]"
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets the maximum value of the range. </summary>
    ''' <value> A <see cref="T:System.TimeSpan">TimeSpan</see> property. </value>
    Public Property Max() As TimeSpan

    ''' <summary> Gets the minimum value of the range. </summary>
    ''' <value> A <see cref="T:System.TimeSpan">TimeSpan</see> property. </value>
    Public Property Min() As TimeSpan

    ''' <summary> Gets the Span value of the range. </summary>
    ''' <value> A <see cref="T:System.TimeSpan"/> property. </value>
    Public ReadOnly Property Span() As TimeSpan
        Get
            Return Me.Max.Subtract(Me.Min)
        End Get
    End Property

#End Region

#Region " ATTRIBUTES "

    ''' <summary> Returns true if the range is <see cref="RangeTimeSpan.Empty"/>. </summary>
    ''' <value> The is <see cref="RangeTimeSpan.Empty"/>. </value>
    Public ReadOnly Property IsEmpty As Boolean
        Get
            Return Me.Equals(RangeTimeSpan.Empty)
        End Get
    End Property

    ''' <summary> Returns true if the range is <see cref="RangeTimeSpan.Unity"/>. </summary>
    ''' <value> The is <see cref="RangeTimeSpan.Unity"/>. </value>
    Public ReadOnly Property IsUnity As Boolean
        Get
            Return Me.Equals(RangeTimeSpan.Unity)
        End Get
    End Property

#End Region


End Class
