﻿''' <summary> Read once class. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class ReadOnce(Of T As Structure)
    Implements IEquatable(Of T)

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub New()
        MyBase.New()
        Me._HasValue = False
    End Sub

    ''' <summary> Gets or sets a value indicating whether this object has value. </summary>
    ''' <value> <c>true</c> if this object has value; otherwise <c>false</c> </value>
    Public Property HasValue As Boolean
    ''' <summary> The value. </summary>
    Private _Value As T

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property Value As T
        Get
            Me._HasValue = False
            Return Me._Value
        End Get
        Set(value As T)
            Me._Value = value
            Me._HasValue = True
        End Set
    End Property

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return String.Format("{0}{1}", IIf(Me.HasValue, "", "?"), Me._Value)
    End Function

#Region " EQUALS "

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>.
    ''' </returns>
    Public Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, ReadOnce(Of T)))
    End Function

    ''' <summary>
    ''' Indicates whether the current object is equal to another object of the same type.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="other"> An object to compare with this object. </param>
    ''' <returns>
    ''' <see langword="true" /> if the current object is equal to the <paramref name="other" />
    ''' parameter; otherwise, <see langword="false" />.
    ''' </returns>
    Public Overloads Function Equals(other As T) As Boolean Implements System.IEquatable(Of T).Equals
        Return Me.HasValue AndAlso other.Equals(Me.Value)
    End Function

    ''' <summary> Compares two ranges. </summary>
    ''' <remarks>
    ''' The two ranges are the same if the have the same minimum and maximum values.
    ''' </remarks>
    ''' <param name="other"> The other point to compare to this object. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As ReadOnce(Of T)) As Boolean
        Return other IsNot Nothing AndAlso (other.Value.Equals(Me.Value) And other.HasValue.Equals(Me.HasValue))
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As ReadOnce(Of T), ByVal right As ReadOnce(Of T)) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operator. </returns>
    Public Shared Operator <>(ByVal left As ReadOnce(Of T), ByVal right As ReadOnce(Of T)) As Boolean
        Return ((CObj(left) IsNot CObj(right)) AndAlso (left Is Nothing OrElse Not left.Equals(right)))
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> An <see cref="T:System.Single">Single</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.HasValue.GetHashCode Xor Me.Value.GetHashCode
    End Function

#End Region

End Class
