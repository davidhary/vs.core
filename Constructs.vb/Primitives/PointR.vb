''' <summary> Defines a <see cref="T:System.Double">Double</see> point. </summary>
''' <remarks>
''' (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 09/07/2005, 1.0.2077.x. </para>
''' </remarks>
Public Class PointR

#Region " SHARED "

    ''' <summary> Gets a new instance of the zero point value. </summary>
    ''' <value> A <see cref="PointR"/> value. </value>
    Public Shared ReadOnly Property Zero() As PointR
        Get
            Return New PointR(0, 0)
        End Get
    End Property

    ''' <summary> Gets a new instance of the Unity [1,1] point value. </summary>
    ''' <value> A <see cref="PointR"/> value. </value>
    Public Shared ReadOnly Property Unity() As PointR
        Get
            Return New PointR(1, 1)
        End Get
    End Property

#End Region

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs a <see cref="PointR"/> instance by its limits. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="x"> A <see cref="T:System.Double">Double</see> expression that specifics the x
    '''                  element. </param>
    ''' <param name="y"> A <see cref="T:System.Double">Double</see> expression that specifics the y
    '''                  element. </param>
    Public Sub New(ByVal x As Double, ByVal y As Double)
        MyBase.New()
        Me.SetPointThis(x, y)
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="model"> The PointR object from which to copy. </param>
    Public Sub New(ByVal model As PointR)
        MyBase.New()
        If model IsNot Nothing Then
            Me.SetPointThis(model._X, model._Y)
        End If
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, PointR))
    End Function

    ''' <summary> Compares two ranges. </summary>
    ''' <remarks>
    ''' The two ranges are the same if the have the same minimum and maximum values.
    ''' </remarks>
    ''' <param name="other"> The other point to compare to this object. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As PointR) As Boolean
        Return other IsNot Nothing AndAlso (other.X.Equals(Me.X) And other.Y.Equals(Me.Y))
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As PointR, ByVal right As PointR) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As PointR, ByVal right As PointR) As Boolean
        Return ((CObj(left) IsNot CObj(right)) AndAlso (left Is Nothing OrElse Not left.Equals(right)))
    End Operator

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> An <see cref="T:System.Single">Single</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.X.GetHashCode Xor Me.Y.GetHashCode
    End Function

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Gets the exponent based on the point extremum values.  This is the
    ''' <see cref="T:System.Double">Double</see> value representing the exponent of
    ''' the most significant digit of point values.  For example, the 4 for 20,000 or -3 for 0.0012.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> The exponent. </returns>
    Public Function GetExponent() As Double

        Return Convert.ToInt32(Math.Max(NumericExtensions.Methods.Exponent(Me.X), NumericExtensions.Methods.Exponent(Me.Y)))

    End Function

    ''' <summary>
    ''' Gets the exponent based on the point values.  This is the
    ''' <see cref="T:System.Double">Double</see> value representing the exponent of
    ''' the most significant digit of point values limits.  For example, the 4 for 20,000 or -3 for
    ''' 0.0012.  With engineering scales, the exponents are multiples of three, e.g., 20,000 yields
    ''' +3 and 0.0001 -3.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="useEngineeringScale"> True to use scale exponent increments of 3. </param>
    ''' <returns> The exponent. </returns>
    Public Function GetExponent(ByVal useEngineeringScale As Boolean) As Double

        Return Convert.ToInt32(Math.Max(NumericExtensions.Methods.Exponent(Me.X, useEngineeringScale), NumericExtensions.Methods.Exponent(Me.Y, useEngineeringScale)))

    End Function

    ''' <summary> Sets the point based on the values. </summary>
    ''' <remarks> Use this class to set the point. </remarks>
    ''' <param name="x"> A <see cref="T:System.Double">Double</see> expression that specifies the x
    '''                  value of the point. </param>
    ''' <param name="y"> A <see cref="T:System.Double">Double</see> expression that specifies the y
    '''                  value of the point. </param>
    Private Sub SetPointThis(ByVal x As Double, ByVal y As Double)
        If Not Me._X.Equals(x) Then
            Me._X = x
        End If
        If Not Me._Y.Equals(y) Then
            Me._Y = y
        End If
    End Sub

    ''' <summary> Sets the point based on the values. </summary>
    ''' <remarks> Use this method to change the point. </remarks>
    ''' <param name="x"> A <see cref="T:System.Double">Double</see> expression that specifies the x
    '''                  value of the point. </param>
    ''' <param name="y"> A <see cref="T:System.Double">Double</see> expression that specifies the y
    '''                  value of the point. </param>
    Public Overloads Sub SetPoint(ByVal x As Double, ByVal y As Double)
        Me.X = x
        Me.Y = y
    End Sub

    ''' <summary> Returns the default string representation of the point. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> A representation of the point, e.g., '(x,y)' . </returns>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},{1})",
                             Me.X.ToString(Globalization.CultureInfo.CurrentCulture),
                             Me.Y.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets the x value of the point. </summary>
    ''' <value> A <see cref="T:System.Double">Double</see> property. </value>
    Public Property X() As Double

    ''' <summary> Gets the y value of the point. </summary>
    ''' <value> A <see cref="T:System.Double">Double</see> property. </value>
    Public Property Y() As Double

    ''' <summary> Gets the hypotenuse. </summary>
    ''' <value> The hypotenuse. </value>
    Public ReadOnly Property Hypotenuse As Double
        Get
            Return GeometricMethods.Hypotenuse(Me.X, Me.Y)
        End Get
    End Property

#End Region

#Region " ATTRIBUTES "

    ''' <summary> Returns true if the range is <see cref="PointR.Zero"/>. </summary>
    ''' <value> The is <see cref="PointR.Zero"/>. </value>
    Public ReadOnly Property IsZero As Boolean
        Get
            Return Me.Equals(PointR.Zero)
        End Get
    End Property

    ''' <summary> Returns true if the range is <see cref="PointR.Unity"/>. </summary>
    ''' <value> The is <see cref="PointR.Unity"/>. </value>
    Public ReadOnly Property IsUnity As Boolean
        Get
            Return Me.Equals(PointR.Unity)
        End Get
    End Property

#End Region

End Class

Friend Module GeometricMethods

    ''' <summary> Hypotenuses. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value">      The value. </param>
    ''' <param name="orthogonal"> The orthogonal. </param>
    ''' <returns> A Double. </returns>
    Public Function Hypotenuse(ByVal value As Double, ByVal orthogonal As Double) As Double
        If (value = 0.0) AndAlso (orthogonal = 0.0) Then
            Return (0.0)
        Else
            Dim ax As Double = Math.Abs(value)
            Dim ay As Double = Math.Abs(orthogonal)
            If ax > ay Then
                Dim r As Double = orthogonal / value
                Return (ax * Math.Sqrt(1.0 + r * r))
            Else
                Dim r As Double = value / orthogonal
                Return (ay * Math.Sqrt(1.0 + r * r))
            End If
        End If
    End Function
End Module
