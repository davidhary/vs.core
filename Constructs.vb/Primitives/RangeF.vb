''' <summary> Defines a <see cref="T:System.Single">Single</see> range class. </summary>
''' <remarks>
''' (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 04/30/2004, 1.0.1581.x. </para>
''' </remarks>
Public Class RangeF

#Region " SHARED "

    ''' <summary> Gets a new instance of the empty range. </summary>
    ''' <value> A <see cref="RangeF"/> value with [0,0] for the [Min,Max] values. </value>
    Public Shared ReadOnly Property [Empty]() As RangeF
        Get
            Return New RangeF(0, 0)
        End Get
    End Property

    ''' <summary> Gets a new instance of the full range. </summary>
    Public Shared ReadOnly Property [Full]() As RangeF
        Get
            Return New RangeF(Double.MinValue, Double.MaxValue)
        End Get
    End Property

    ''' <summary> Gets a new instance of the full non-negative range. </summary>
    Public Shared ReadOnly Property [FullNonnegative]() As RangeF
        Get
            Return New RangeF(0, Double.MaxValue)
        End Get
    End Property

    ''' <summary> Gets a new instance of the Unity range. </summary>
    ''' <value> A <see cref="RangeF"/> [0,1] value. </value>
    Public Shared ReadOnly Property Unity() As RangeF
        Get
            Return New RangeF(0, 1)
        End Get
    End Property

    ''' <summary> Gets a new instance of the zero range. </summary>
    ''' <value> The zero. </value>
    Public Shared ReadOnly Property Zero() As RangeF
        Get
            Return New RangeF(0, 0)
        End Get
    End Property

    ''' <summary> Return the range of the specified data array. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="values"> The data array. </param>
    ''' <returns> The calculated range. </returns>
    Public Shared Function GetRange(ByVal values As Generic.IEnumerable(Of Single)) As RangeF
        ' return the unit range if no data
        If values Is Nothing OrElse Not values.Any Then Return RangeF.Unity
        Dim min As Single = Single.MaxValue
        Dim max As Single = Single.MinValue
        For Each temp As Single In values
            If temp < min Then
                min = temp
            ElseIf temp > max Then
                max = temp
            End If
        Next
        Return New RangeF(min, max)
    End Function

    ''' <summary> Extended range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="range"> A <see cref="RangeF"/> value. </param>
    ''' <returns> A RangeF. </returns>
    Public Function ExtendedRange(ByVal range As RangeF) As RangeF
        Dim result As New RangeF(Me)
        result.ExtendRange(range)
        Return result
    End Function

    ''' <summary> Shifted range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A RangeF. </returns>
    Public Function ShiftedRange(ByVal value As Single) As RangeF
        Dim result As New RangeF(Me)
        result.ShiftRange(value)
        Return result
    End Function

    ''' <summary> Transposed range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="shift"> The shift. </param>
    ''' <param name="scale"> The scale. </param>
    ''' <returns> A RangeF. </returns>
    Public Function TransposedRange(ByVal shift As Single, ByVal scale As Single) As RangeF
        Dim result As New RangeF(Me)
        result.TransposeRange(shift, scale)
        Return result
    End Function

#End Region

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructs a <see cref="RangeR"/> instance by its span. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="span"> A <see cref="T:System.Single">Single</see> property. </param>
    Public Sub New(ByVal span As Single)
        Me.New(CSng(-0.5 * span), CSng(0.5 * span))
    End Sub

    ''' <summary> Constructs a <see cref="RangeF"/> instance by its limits. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="minValue"> A <see cref="T:System.Single">Single</see> expression that specifics
    '''                         the minimum range. </param>
    ''' <param name="maxValue"> A <see cref="T:System.Single">Single</see> expression that specifics
    '''                         the maximum range. </param>
    Public Sub New(ByVal minValue As Single, ByVal maxValue As Single)
        MyBase.New()
        Me.SetRange(minValue, maxValue)
    End Sub

    ''' <summary> The Copy Constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="model"> The RangeF object from which to copy. </param>
    Public Sub New(ByVal model As RangeF)
        MyBase.New()
        If model IsNot Nothing Then
            Me.SetRange(model._Min, model._Max)
        End If
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary> = casting operator. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As RangeF, ByVal right As RangeF) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return RangeF.Equals(left, right)
        End If
    End Operator

    ''' <summary> &lt;&gt; casting operator. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As RangeF, ByVal right As RangeF) As Boolean
        Return Not RangeF.Equals(left, right)
    End Operator

    ''' <summary> Returns True if equal. </summary>
    ''' <remarks>
    ''' Ranges are the same if the have the same
    ''' <see cref="Type">min</see> and <see cref="Type">max</see> values.
    ''' </remarks>
    ''' <param name="left">  The left hand side item to compare for equality. </param>
    ''' <param name="right"> The left hand side item to compare for equality. </param>
    ''' <returns> <c>True</c> if equals. </returns>
    Public Overloads Shared Function Equals(ByVal left As RangeF, ByVal right As RangeF) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left.Max.Equals(right.Max) AndAlso left.Min.Equals(right.Min)
        End If
    End Function

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, <c>False</c>.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, RangeF))
    End Function

    ''' <summary>
    ''' Returns True if the value of the <paramref name="other"/> equals to the instance value.
    ''' </summary>
    ''' <remarks>
    ''' Ranges are the same if the have the same
    ''' <see cref="Type">min</see> and <see cref="Type">max</see> values.
    ''' </remarks>
    ''' <param name="other"> The other <see cref="RangeF">Range</see> to compare for equality with
    '''                      this instance. </param>
    ''' <returns> A Boolean data type. </returns>
    Public Overloads Function Equals(ByVal other As RangeF) As Boolean
        Return other IsNot Nothing AndAlso RangeF.Equals(Me, other)
    End Function

#End Region

#Region " METHODS "

    ''' <summary> Clips the given value. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A Date. </returns>
    Public Function Clip(ByVal value As Single) As Single
        Return If(value < Me.Min, Me.Min, If(value > Me.Max, Me.Max, value))
    End Function

    ''' <summary> Query if 'point' is inside the range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="point"> A <see cref="T:System.Single">Single</see> point value&gt; </param>
    ''' <returns> <c>true</c> if inside; otherwise <c>false</c> </returns>
    Public Function Encloses(ByVal point As Single) As Boolean
        Return (point > Me.Min) AndAlso (point < Me.Max)
    End Function

    ''' <summary> Returns true if the point value is within the range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="point"> A <see cref="T:System.Single">Single</see> point value&gt; </param>
    ''' <returns>
    ''' <c>True</c> if value above or equal to minimum or below or equal to maximum.
    ''' </returns>
    Public Function Contains(ByVal point As Single) As Boolean
        Return (point >= Me.Min) AndAlso (point <= Me.Max)
    End Function

    ''' <summary> Returns true if the point value is within the range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="point">     A <see cref="T:System.Single">Single</see> point value&gt; </param>
    ''' <param name="tolerance"> Tolerance for comparison. </param>
    ''' <returns>
    ''' <c>True</c> if value above or equal to minimum - tolerance or below or equal to maximum +
    ''' tolerance.
    ''' </returns>
    Public Function Contains(ByVal point As Single, ByVal tolerance As Single) As Boolean
        Return (point >= Me.Min - tolerance) AndAlso (point <= Me.Max + tolerance)
    End Function

    ''' <summary>
    ''' Extends this range to include both its present values and the specified range.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="range"> A <see cref="RangeR"/> value. </param>
    Public Sub ExtendRange(ByVal range As RangeF)
        If range Is Nothing Then Throw New ArgumentNullException(NameOf(range))
        If Me.Min > range.Min Then Me.Min = range.Min
        If Me.Max < range.Max Then Me.Max = range.Max
    End Sub

    ''' <summary> Shift range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value by which to shift the range. </param>
    Public Sub ShiftRange(ByVal value As Single)
        Me.SetRange(value + Me.Min, value + Me.Max)
    End Sub

    ''' <summary> Transpose range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="shift"> The shift. </param>
    ''' <param name="scale"> The scale. </param>
    Public Sub TransposeRange(ByVal shift As Single, ByVal scale As Single)
        Me.SetRange(shift + scale * Me.Min, shift + scale * Me.Max)
    End Sub

    ''' <summary> Sets the range based on the extrema. </summary>
    ''' <remarks> Use this class to set the range. </remarks>
    ''' <param name="minValue"> A <see cref="T:System.Single">Single</see> expression that specifies
    '''                         the minimum value of the range. </param>
    ''' <param name="maxValue"> A <see cref="T:System.Single">Single</see> expression that specifies
    '''                         the maximum value of the range. </param>
    Public Overloads Sub SetRange(ByVal minValue As Single, ByVal maxValue As Single)
        Me.Min = minValue
        Me.Max = maxValue
    End Sub

    ''' <summary>
    ''' Gets the exponent based on the range extremum values.  This is the
    ''' <see cref="T:System.Integer">integer</see> value representing the exponent of
    ''' the most significant digit of range limits.  For example, the 4 for 20,000 or -3 for 0.0012.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> The exponent. </returns>
    Public Function GetExponent() As Integer
        Return Convert.ToInt32(Math.Max(NumericExtensions.Methods.Exponent(Me.Min), NumericExtensions.Methods.Exponent(Me.Max)))
    End Function

    ''' <summary>
    ''' Gets the exponent based on the range extremum values.  This is the
    ''' <see cref="T:System.Integer">integer</see> value representing the exponent of
    ''' the most significant digit of range limits.  For example, the 4 for 20,000 or -3 for 0.0012.
    ''' With engineering scales, the exponents are multiples of three, e.g., 20,000 yields +3 and
    ''' 0.0001 -3.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="useEngineeringScale"> True to use scale exponent increments of 3. </param>
    ''' <returns> The exponent. </returns>
    Public Function GetExponent(ByVal useEngineeringScale As Boolean) As Integer

        Return Convert.ToInt32(Math.Max(NumericExtensions.Methods.Exponent(Me.Min, useEngineeringScale),
                                        NumericExtensions.Methods.Exponent(Me.Max, useEngineeringScale)))

    End Function

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Integer
        Return Me.Min.GetHashCode Xor Me.Max.GetHashCode
    End Function

    ''' <summary> Returns the default string representation of the range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> A representation of the range, e.g., '(min,max)' . </returns>
    Public Overrides Function ToString() As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},{1})",
                             Me.Min.ToString(Globalization.CultureInfo.CurrentCulture),
                             Me.Max.ToString(Globalization.CultureInfo.CurrentCulture))
    End Function

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets the maximum value of the range. </summary>
    ''' <value> A <see cref="T:System.Single">Single</see> property. </value>
    Public Property Max() As Single

    ''' <summary> Returns the mid range point of the range. </summary>
    ''' <value> A read only <see cref="T:System.Single">Single</see> property. </value>
    Public ReadOnly Property Midrange() As Single
        Get
            Return 0.5F * (Me.Max + Me.Min)
        End Get
    End Property

    ''' <summary> Gets the minimum value of the range. </summary>
    ''' <value> A <see cref="T:System.Single">Single</see> property. </value>
    Public Property Min() As Single

    ''' <summary> Gets the range value of the range. </summary>
    ''' <value> A <see cref="T:System.Single">Single</see> </value>
    Public ReadOnly Property Span() As Single
        Get
            Return (Me.Max - Me.Min)
        End Get
    End Property

#End Region

#Region " ATTRIBUTES "

    ''' <summary> Returns true if the range is <see cref="RangeF.Empty"/>. </summary>
    ''' <value> The is <see cref="RangeF.Empty"/>. </value>
    Public ReadOnly Property IsEmpty As Boolean
        Get
            Return Me.Equals(RangeF.Empty)
        End Get
    End Property

    ''' <summary> Returns true if the range is <see cref="RangeF.Unity"/>. </summary>
    ''' <value> The is <see cref="RangeF.Unity"/>. </value>
    Public ReadOnly Property IsUnity As Boolean
        Get
            Return Me.Equals(RangeF.Unity)
        End Get
    End Property

#End Region

End Class
