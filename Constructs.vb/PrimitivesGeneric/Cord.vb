''' <summary> Implements a generic line class as a line between two generic. </summary>
''' <remarks>
''' (c) 2006 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 04/10/2006, 1.1.2291. </para>
''' </remarks>
Public Class Cord(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})
    Inherits Line(Of T)

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Constructs a <see cref="Line(Of T)"/> instance by its origin and insertion points.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="origin">    Specifies the <see cref="Point(Of T)">origin</see> or start point of
    '''                          the line. </param>
    ''' <param name="insertion"> Specifies the <see cref="Point(Of T)">insertion</see> or end point of
    '''                          the line. </param>
    Public Sub New(ByVal origin As Point(Of T), ByVal insertion As Point(Of T))
        MyBase.New()
        Me.SetLine(origin, insertion)
    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary> Gets or sets the insertion point of the line. </summary>
    ''' <value> A <see cref="Point(Of T)">Point</see> property. </value>
    Public Property Insertion() As Point(Of T)
        Get
            Return New Point(Of T)(Me.X2, Me.Y2)
        End Get
        Set(ByVal value As Point(Of T))
            If value IsNot Nothing Then
                Me.X2 = value.X
                Me.Y2 = value.Y
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the origin point of the line. </summary>
    ''' <value> A <see cref="Point(Of T)">Point</see> property. </value>
    Public Property Origin() As Point(Of T)
        Get
            Return New Point(Of T)(Me.X1, Me.Y1)
        End Get
        Set(ByVal value As Point(Of T))
            If value IsNot Nothing Then
                Me.X1 = value.X
                Me.Y1 = value.Y
            End If
        End Set
    End Property

    ''' <summary> Sets the line coordinates. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="origin">    Specifies the origin point of the line. </param>
    ''' <param name="insertion"> Specifies the insertion point of the line. </param>
    Public Overloads Sub SetLine(ByVal origin As Point(Of T), ByVal insertion As Point(Of T))

        If origin Is Nothing Then
            Throw New ArgumentNullException(NameOf(origin))
        End If
        If insertion Is Nothing Then
            Throw New ArgumentNullException(NameOf(insertion))
        End If
        Me.Origin = New Point(Of T)(origin)
        Me.Insertion = New Point(Of T)(insertion)

    End Sub

#End Region

End Class
