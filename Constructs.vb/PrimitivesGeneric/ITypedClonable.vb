
''' <summary> Interface for typed Cloneable. </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License.</para><para>
''' David, 2020-04-27, . </para>
''' </remarks>
Public Interface ITypedCloneable(Of T)
    Inherits ICloneable

    ''' <summary> Creates a new instance. </summary>
    ''' <returns> The new  instance. </returns>
    Function CreateNew() As T

    ''' <summary> Creates the copy. </summary>
    ''' <returns> The new copy. </returns>
    Function CreateCopy() As T

    ''' <summary> Copies from described by value. </summary>
    ''' <param name="value"> The value. </param>
    Sub CopyFrom(ByVal value As T)

End Interface
