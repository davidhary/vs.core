''' <summary> Implements a generic point class. </summary>
''' <remarks>
''' (c) 2006 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 04/10/2006, 1.1.2291. </para>
''' </remarks>
Public Class Point(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="Point" /> class. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Point" /> class. The copy constructor.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="model"> The  <see cref="Point">Point</see> object from which to copy. </param>
    Public Sub New(ByVal model As Point(Of T))
        Me.New()
        If model IsNot Nothing Then
            Me.SetPointThis(model.X, model.Y)
        End If
    End Sub

    ''' <summary> Initializes a new instance of the <see cref="Point" /> class. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="x"> Specifies the X coordinate of the point. </param>
    ''' <param name="y"> Specifies the Y coordinate of the point. </param>
    Public Sub New(ByVal x As T, ByVal y As T)
        Me.New()
        Me.SetPointThis(x, y)
    End Sub

#End Region

#Region " SHARED "

    ''' <summary> Compares two points. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  Specifies the point to compare. </param>
    ''' <param name="right"> Specifies the point to compare with. </param>
    ''' <returns> <c>True</c> if the points are equal. </returns>
    Public Overloads Shared Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
        Return Point(Of T).Equals(TryCast(left, Point(Of T)), TryCast(right, Point(Of T)))
    End Function

    ''' <summary> Compares two points. </summary>
    ''' <remarks> The two points are the same if they have the same X and Y coordinates. </remarks>
    ''' <param name="left">  Specifies the point to compare. </param>
    ''' <param name="right"> Specifies the point to compare with. </param>
    ''' <returns> <c>True</c> if the points are equal. </returns>
    Public Overloads Shared Function Equals(ByVal left As Point(Of T), ByVal right As Point(Of T)) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left.X.Equals(right.X) AndAlso left.Y.Equals(right.Y)
        End If
    End Function

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As Point(Of T), ByVal right As Point(Of T)) As Boolean
        Return Point(Of T).Equals(left, right)
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As Point(Of T), ByVal right As Point(Of T)) As Boolean
        Return ((CObj(left) IsNot CObj(right)) AndAlso (left Is Nothing OrElse Not left.Equals(right)))
    End Operator

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, Point(Of T)))
    End Function

    ''' <summary> Compares two points. </summary>
    ''' <remarks> The two points are the same if they have the same X and Y coordinates. </remarks>
    ''' <param name="other"> Specifies the other point to compare. </param>
    ''' <returns> <c>True</c> if the points are equal. </returns>
    Public Overloads Function Equals(ByVal other As Point(Of T)) As Boolean
        Return other IsNot Nothing AndAlso Point(Of T).Equals(Me, other)
    End Function

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> An <see cref="T:System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.X.GetHashCode Xor Me.Y.GetHashCode
    End Function

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary> Holds the Y coordinate of the point. </summary>
    ''' <value> The y coordinate. </value>
    Public Property Y() As T

    ''' <summary> Holds the X coordinate of the point. </summary>
    ''' <value> The x coordinate. </value>
    Public Property X() As T

    ''' <summary> Sets the point based on the coordinates. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="x"> The x. </param>
    ''' <param name="y"> The y. </param>
    Private Sub SetPointThis(ByVal x As T, ByVal y As T)
        If x.CompareTo(Me._X) <> 0 Then
            Me._X = x
        End If
        If y.CompareTo(Me._Y) <> 0 Then
            Me._Y = y
        End If
    End Sub

    ''' <summary> Sets the point based on the coordinates. </summary>
    ''' <remarks> Use this class to set the point. </remarks>
    ''' <param name="x"> Specifies the X coordinate of the point. </param>
    ''' <param name="y"> Specifies the Y coordinate of the point. </param>
    Public Sub SetPoint(ByVal x As T, ByVal y As T)
        Me.X = x
        Me.Y = y
    End Sub

    ''' <summary> Returns the default string representation of the point. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> The formatted string representation of the point, e.g., '(x,y)'. </returns>
    Public Overrides Function ToString() As String
        Return Point(Of T).ToString(Me.X, Me.Y)
    End Function

    ''' <summary> Returns the default string representation of the point. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="x"> Specifies the X coordinate of the point. </param>
    ''' <param name="y"> Specifies the Y coordinate of the point. </param>
    ''' <returns> The formatted string representation of the point, e.g., '(x,y)'. </returns>
    Public Overloads Shared Function ToString(ByVal x As T, ByVal y As T) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},{1})", x, y)
    End Function

#End Region

End Class
