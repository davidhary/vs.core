''' <summary> Implements a generic range class. </summary>
''' <remarks>
''' (c) 2006 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 04/10/2006, 1.1.2291. </para>
''' </remarks>
Public Class Range(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="Range" /> class. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Protected Sub New()
        MyBase.New()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="Range" /> class. The copy constructor.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="model"> The  <see cref="Range">Range</see> object from which to copy. </param>
    Public Sub New(ByVal model As Range(Of T))
        MyBase.New()
        If model IsNot Nothing Then
            Me.SetRangeThis(model._Min, model._Max)
        End If
    End Sub

    ''' <summary> Constructs this class. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="minValue"> Min of range. </param>
    ''' <param name="maxValue"> Max of range. </param>
    Public Sub New(ByVal minValue As T, ByVal maxValue As T)
        MyBase.New()
        Me.SetRangeThis(minValue, maxValue)
    End Sub

#End Region

#Region " EQUALS "

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false.
    ''' </returns>
    Public Overloads Shared Function Equals(ByVal left As Object, ByVal right As Object) As Boolean
        Return Range(Of T).Equals(TryCast(left, Range(Of T)), TryCast(right, Range(Of T)))
    End Function

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false.
    ''' </returns>
    Public Overloads Shared Function Equals(ByVal left As Range(Of T), ByVal right As Range(Of T)) As Boolean
        If left Is Nothing Then
            Return right Is Nothing
        ElseIf right Is Nothing Then
            Return False
        Else
            Return left.Min.Equals(right.Min) AndAlso left.Max.Equals(right.Max)
        End If
    End Function

    ''' <summary>
    ''' Determines whether the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
    '''                    <see cref="T:System.Object" />. </param>
    ''' <returns>
    ''' <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
    ''' <see cref="T:System.Object" />; otherwise, false.
    ''' </returns>
    Public Overloads Overrides Function Equals(ByVal obj As Object) As Boolean
        Return Me.Equals(TryCast(obj, Range(Of T)))
    End Function

    ''' <summary> Compares two ranges. </summary>
    ''' <remarks> The two ranges are the same if they have the min and end values. </remarks>
    ''' <param name="other"> Specifies the other range to compare. </param>
    ''' <returns> <c>True</c> if the ranges are equal. </returns>
    Public Overloads Function Equals(ByVal other As Range(Of T)) As Boolean
        Return other IsNot Nothing AndAlso Range(Of T).Equals(Me, other)
    End Function

    ''' <summary> Creates a unique hash code. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> An <see cref="T:System.Int32">Int32</see> value. </returns>
    Public Overloads Overrides Function GetHashCode() As Int32
        Return Me.Min.GetHashCode Xor Me.Max.GetHashCode
    End Function

#End Region

#Region " OPERATORS "

    ''' <summary> Implements the operator =. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator =(ByVal left As Range(Of T), ByVal right As Range(Of T)) As Boolean
        Return (CObj(left) Is CObj(right)) OrElse (left IsNot Nothing AndAlso left.Equals(right))
    End Operator

    ''' <summary> Implements the operator &lt;&gt;. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="left">  Specifies the left hand side argument of the binary operation. </param>
    ''' <param name="right"> Specifies the right hand side argument of the binary operation. </param>
    ''' <returns> The result of the operation. </returns>
    Public Shared Operator <>(ByVal left As Range(Of T), ByVal right As Range(Of T)) As Boolean
        Return ((CObj(left) IsNot CObj(right)) AndAlso (left Is Nothing OrElse Not left.Equals(right)))
    End Operator

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>
    ''' Determines if the <paramref name="value">specified value</paramref>
    ''' is within range.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> Specifies the value which to check as contained within the range. </param>
    ''' <returns>
    ''' <c>True</c> if the <paramref name="value">specified value</paramref> is within range.
    ''' </returns>
    Public Function Contains(ByVal value As T) As Boolean
        Return value.CompareTo(Me.Min) >= 0 AndAlso value.CompareTo(Me.Max) <= 0
    End Function

    ''' <summary>
    ''' Returns a new range from the min of the two minima to the max of the two maxima.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="rangeA"> Specifies <see cref="Range(Of T)"/> A. </param>
    ''' <param name="rangeB"> Specifies <see cref="Range(Of T)"/> B. </param>
    ''' <returns> A new Range from the min of the two minima to the max of the two maxima. </returns>
    Public Shared Function Extend(ByVal rangeA As Range(Of T), ByVal rangeB As Range(Of T)) As Range(Of T)

        If rangeA Is Nothing Then
            Throw New ArgumentNullException(NameOf(rangeA))
        End If

        If rangeB Is Nothing Then
            Throw New ArgumentNullException(NameOf(rangeB))
        End If

        If rangeA.Min.CompareTo(rangeB.Min) > 0 Then
            Return If(rangeA.Max.CompareTo(rangeB.Max) < 0, New Range(Of T)(rangeB.Min, rangeB.Max), New Range(Of T)(rangeB.Min, rangeA.Max))
        ElseIf rangeA.Max.CompareTo(rangeB.Max) < 0 Then
            Return New Range(Of T)(rangeA.Min, rangeB.Max)
        Else
            Return New Range(Of T)(rangeA.Min, rangeA.Max)
        End If

    End Function

    ''' <summary>
    ''' Extends this range to include both its present values and the specified range.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="range"> A <see cref="Range(Of T)"/> value. </param>
    ''' <returns> A new Limits from the min of the two minima to the max of the two maxima. </returns>
    Public Function ExtendBy(ByVal range As Range(Of T)) As Range(Of T)

        If range Is Nothing Then
            Throw New ArgumentNullException(NameOf(range))
        End If

        If Me.Min.CompareTo(range.Min) > 0 Then
            Me.SetRange(range.Min, Me.Min)
        End If

        If Me.Max.CompareTo(range.Max) < 0 Then
            Me.SetRange(Me.Min, range.Max)
        End If

        Return Me

    End Function

    ''' <summary> Return the range of the specified data array. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="values"> The data array. </param>
    ''' <returns> The calculated range. </returns>
    Public Shared Function GetRange(ByVal values() As T) As Range(Of T)

        ' return the unit range if no data
        If values Is Nothing Then
            Throw New ArgumentNullException(NameOf(values))
        End If

        ' initialize the range values to the first value
        Dim temp As T
        temp = values(0)
        Dim min As T = temp
        Dim max As T = temp

        ' Loop over each point in the arrays
        For i As Int32 = 0 To values.Length - 1
            temp = values(i)
            If temp.CompareTo(min) < 0 Then
                min = temp
            ElseIf temp.CompareTo(max) > 0 Then
                max = temp
            End If
        Next i
        Return New Range(Of T)(min, max)

    End Function

    ''' <summary> The maximum. </summary>
    Private _Max As T

    ''' <summary> Returns the end or maximum value of the range. </summary>
    ''' <value> The maximum value. </value>
    Public ReadOnly Property Max() As T
        Get
            Return Me._Max
        End Get
    End Property

    ''' <summary> The minimum. </summary>
    Private _Min As T

    ''' <summary> Returns the start or minimum value of the range. </summary>
    ''' <value> The minimum value. </value>
    Public ReadOnly Property Min() As T
        Get
            Return Me._Min
        End Get
    End Property

    ''' <summary> Sets the range based on the extrema. </summary>
    ''' <remarks> Use this class to set the range. </remarks>
    ''' <param name="minValue"> Specified the minimum value of the range. </param>
    ''' <param name="maxValue"> Specifies the maximum value of the range. </param>
    Private Sub SetRangeThis(ByVal minValue As T, ByVal maxValue As T)

        If minValue.CompareTo(maxValue) <= 0 Then
            Me._Min = minValue
            Me._Max = maxValue
        Else
            Me._Min = maxValue
            Me._Max = minValue
        End If

    End Sub

    ''' <summary> Sets the range based on the extrema. </summary>
    ''' <remarks> Use this class to set the range. </remarks>
    ''' <param name="minValue"> Specified the minimum value of the range. </param>
    ''' <param name="maxValue"> Specifies the maximum value of the range. </param>
    Public Sub SetRange(ByVal minValue As T, ByVal maxValue As T)
        Me.SetRangeThis(minValue, maxValue)
    End Sub

    ''' <summary> Returns the default string representation of the range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> The formatted string representation of the range, e.g., '(min,max)'. </returns>
    Public Overrides Function ToString() As String
        Return Range(Of T).ToString(Me.Min, Me.Max)
    End Function

    ''' <summary> Returns the default string representation of the range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="min"> Returns the start or minimum value of the range. </param>
    ''' <param name="max"> Returns the end or maximum value of the range. </param>
    ''' <returns> The formatted string representation of the range, e.g., '(min,max)'. </returns>
    Public Overloads Shared Function ToString(ByVal min As T, ByVal max As T) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, "({0},{1})", min, max)
    End Function

#End Region

End Class
