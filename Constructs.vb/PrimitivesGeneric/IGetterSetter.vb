''' <summary> A structure. </summary>
''' <remarks>
''' David, 2020-05-29. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IGetterSetter(Of TValue As Structure)

    ''' <summary> Gets the <typeparamref name="TValue"/> for the given property name. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="name"> (Optional) Name of the runtime caller
    '''                                                           member. </param>
    ''' <returns> A Nullable of <typeparamref name="TValue"/>. </returns>
    Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As Nullable(Of TValue)

    ''' <summary> Sets the <typeparamref name="TValue"/> for the given property name. </summary>
    ''' <remarks> David, 2020-05-05. </remarks>
    ''' <param name="value">                                      value. </param>
    ''' <param name="name"> (Optional) Name of the runtime caller
    '''                                                           member. </param>
    ''' <returns> A Nullable of <typeparamref name="TValue"/>. </returns>
    Function Setter(ByVal value As TValue, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As TValue

End Interface

''' <summary> Interface for getter setter. </summary>
''' <remarks> David, 2020-09-22. </remarks>
Public Interface IGetterSetter

    ''' <summary> Getters the given &lt; runtime. compiler services. caller member name. </summary>
    ''' <param name="name"> (Optional) Name of the runtime caller
    '''                                                           member. </param>
    ''' <returns> A String. </returns>
    Function Getter(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String

    ''' <summary> Setters. </summary>
    ''' <param name="value">                                      Name of the runtime caller member. </param>
    ''' <param name="name"> (Optional) Name of the runtime caller
    '''                                                           member. </param>
    ''' <returns> A String. </returns>
    Function Setter(ByVal value As String, <Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing) As String

End Interface
