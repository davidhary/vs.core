﻿''' <summary> A thread safe monitor. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/29/2017 </para>
''' </remarks>
Public NotInheritable Class ThreadSafeMonitor
    Implements IDisposable
    ''' <summary> Number of busies. </summary>
    Private _BusyCount As Integer

    ''' <summary> RuturnsTrue if busy. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> <c>true</c> if busy; otherwise <c>false</c> </returns>
    Public Function Busy() As Boolean
        Return Me._BusyCount > 0
    End Function

    ''' <summary> Increments the busy count. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub Enter()
        System.Threading.Interlocked.Increment(Me._BusyCount)
    End Sub

    ''' <summary>
    ''' Increments the busy count and return an instance of the <see cref="ThreadSafeMonitor"/>.
    ''' </summary>
    ''' <remarks>
    ''' Use the following code to block re-entry
    ''' <code>
    ''' Private Monitor as New ThreadSafeMonitor
    ''' Private Sub OnCollectionChanged(e As NotifyCollectionChangedEventArgs)
    '''    Dim evt As NotifyCollectionChangedEventHandler = CollectionChangedEvent
    '''    If evt IsNot Nothing Then
    '''        Using Me.Monitor.SyncMonitor()
    '''           Me.Context.Send(Sub(state) evt(Me, e), Nothing)
    '''        End Using
    '''    End If
    ''' End Sub
    ''' </code>
    ''' </remarks>
    ''' <returns> An IDisposable. </returns>
    Public Function SyncMonitor() As IDisposable
        System.Threading.Interlocked.Increment(Me._BusyCount)
        Return Me
    End Function

    ''' <summary> Gets or sets the sentinel to detect redundant calls. </summary>
    ''' <value> The sentinel to detect redundant calls. </value>
    Private ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Models.ThreadSafeToken(Of T) and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Private Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                    System.Threading.Interlocked.Decrement(Me._BusyCount)
                End If
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
    End Sub


End Class
