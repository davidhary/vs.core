Imports System.Threading
Imports System.ComponentModel

''' <summary> A thread safe binding list <see cref="BindingList(Of T)"/> </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 03/06/2019 </para>
''' </remarks>
<Serializable>
<DebuggerDisplay("Count = {Count}")>
Public Class ConcurrentBindingList(Of T)
    Inherits BindingList(Of T)
    Implements IDisposable

#Region " CONSTRUCTION AND CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.ComponentModel.BindingList`1" /> class
    ''' using default values.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub New()
        MyBase.New()
    End Sub

    ''' <summary> Gets the is disposed. </summary>
    ''' <value> The is disposed. </value>
    Protected ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the <see cref="T:Collection" />
    ''' and its child controls and optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="disposing"> true to release both managed and unmanaged resources; false to
    '''                                                   release only unmanaged resources. </param>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                End If
                If Me._ItemsLocker IsNot Nothing Then Me._ItemsLocker.Dispose() : Me._ItemsLocker = Nothing
            End If
        Catch
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        ' uncomment the following line if Finalize() is overridden above.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#Region " THREAD SYNC MANAGEMENT  "

#Disable Warning CA2235 ' Mark all non-serializable fields

    ''' <summary> Gets the items locker. </summary>
    ''' <value> The items locker. </value>
    Protected ReadOnly Property ItemsLocker As New ReaderWriterLockSlim()
#Enable Warning CA2235 ' Mark all non-serializable fields
    ''' <summary> The synchronize root. </summary>
    <NonSerialized>
    Private _SyncRoot As Object

    ''' <summary> Gets the synchronization root. </summary>
    ''' <remarks>
    ''' The Sync root helps super classes synchronously access the items. For details see reply by
    ''' Roman Zavalov in
    ''' https://stackoverflow.com/questions/728896/whats-the-use-of-the-syncroot-pattern.
    ''' </remarks>
    ''' <value> The synchronization root. </value>
    Public ReadOnly Property SyncRoot() As Object
        Get
            If Me._SyncRoot Is Nothing Then
                Me.ItemsLocker.EnterReadLock()
                Try
                    Dim c As ICollection = TryCast(Me.Items, ICollection)
                    If c IsNot Nothing Then
                        Me._SyncRoot = c.SyncRoot
                    Else
                        Interlocked.CompareExchange(Of [Object])(Me._SyncRoot, New [Object](), Nothing)
                    End If
                Finally
                    Me.ItemsLocker.ExitReadLock()
                End Try
            End If
            Return Me._SyncRoot
        End Get
    End Property

#End Region

#Region " CHECK METHODS  "

    ''' <summary>
    ''' Returns <c>True</c> if the collection size is fixed or the collection is read only.
    ''' </summary>
    ''' <value> The size of the is fixed. </value>
    Public ReadOnly Property IsFixedSize() As Boolean
        Get
            Dim list As IList = TryCast(MyBase.Items, IList)
            Return If(list IsNot Nothing, list.IsFixedSize, Me.Items.IsReadOnly)
        End Get
    End Property

    ''' <summary> Gets the is read only. </summary>
    ''' <value> The is read only. </value>
    Public ReadOnly Property IsReadOnly() As Boolean
        Get
            Return Me.Items.IsReadOnly
        End Get
    End Property

    ''' <summary> Throws an exception if index is out of range. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
    '''                                                the required range. </exception>
    ''' <param name="index"> Zero-based index of the. </param>
    Private Sub EnsureIndexRange(index As Integer)
        If index < 0 OrElse index >= Me.Items.Count Then
            Throw New ArgumentOutOfRangeException(NameOf(index), $"Must be between {0} and {Me.Items.Count - 1}")
        End If
    End Sub

    ''' <summary> Ensures that not empty. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    Private Sub EnsureNotEmpty()
        If Not MyBase.Any Then
            Throw New NotSupportedException("Collection is empty")
        End If
    End Sub

    ''' <summary> Throws an exception if read only. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    Private Sub EnsureNotReadOnly()
        If Me.Items.IsReadOnly Then Throw New NotSupportedException("Collection is read only")
    End Sub

    ''' <summary> Query if 'value' is compatible object. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> <c>true</c> if compatible object; otherwise <c>false</c> </returns>
    Public Shared Function IsCompatibleObject(value As Object) As Boolean
        ' Non-null values are fine.  Only accept nulls if T is a class or Nullable<U>.
        ' Note that default(T) is not equal to null for value types except when T is Nullable<U>. 
        Return (TypeOf value Is T) OrElse (value Is Nothing)
    End Function

#End Region

#Region " PUBLIC OVERLOADS "

    ''' <summary> Gets any. </summary>
    ''' <value> any. </value>
    Public Overloads ReadOnly Property Any() As Boolean
        Get
            Me.ItemsLocker.EnterReadLock()
            Try
                Return MyBase.Any
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End Get
    End Property

    ''' <summary> Gets the number of elements. </summary>
    ''' <value> The count. </value>
    Public Overloads ReadOnly Property Count() As Integer
        Get
            Me.ItemsLocker.EnterReadLock()
            Try
                Return Me.Items.Count
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End Get
    End Property

    ''' <summary>
    ''' Provides direct access to the base class item to permit synchronization for the super class.
    ''' </summary>
    ''' <value> The base item. </value>
    Protected Property BaseItem(ByVal index As Integer) As T
        Get
            Return MyBase.Item(index)
        End Get
        Set(value As T)
            MyBase.Item(index) = value
        End Set
    End Property

    ''' <summary> Gets or sets the element at the specified index. </summary>
    ''' <value> The element at the specified index. </value>
    Public Overloads Property Item(index As Integer) As T
        Get
            Me.ItemsLocker.EnterReadLock()
            Try
                Me.EnsureIndexRange(index)
                Return MyBase.Item(index)
            Finally
                Me.ItemsLocker.ExitReadLock()
            End Try
        End Get
        Set
            Me.ItemsLocker.EnterWriteLock()
            Try
                Me.EnsureNotReadOnly()
                Me.EnsureIndexRange(index)
                MyBase.Item(index) = Value
            Finally
                Me.ItemsLocker.ExitWriteLock()
            End Try
        End Set
    End Property

    ''' <summary>
    ''' Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />
    ''' .
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="item"> The object to add to the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />
    '''                     . </param>
    Public Overloads Sub Add(ByVal item As T)
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            MyBase.Add(item)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary>
    ''' Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" />
    '''  to an <see cref="T:System.Array" />
    ''' , starting at a particular <see cref="T:System.Array" />
    '''  index.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="array"> The one-dimensional <see cref="T:System.Array" />
    '''                       that is the destination of the elements copied from
    '''                       <see cref="T:System.Collections.Generic.ICollection`1" />
    '''                      . The <see cref="T:System.Array" />
    '''                       must have zero-based indexing. </param>
    ''' <param name="index"> The zero-based index in <paramref name="array" />
    '''                       at which copying begins. </param>
    Public Overloads Sub CopyTo(array As T(), index As Integer)
        Me.ItemsLocker.EnterReadLock()
        Try
            MyBase.CopyTo(array, index)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Sub

    ''' <summary>
    ''' Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" />
    '''  contains a specific value.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="item"> The object to locate in the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />
    '''                     . </param>
    ''' <returns>
    ''' true if <paramref name="item" />
    '''  is found in the <see cref="T:System.Collections.Generic.ICollection`1" />
    ''' ; otherwise, false.
    ''' </returns>
    Public Overloads Function Contains(item As T) As Boolean
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.Contains(item)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    ''' <summary> Returns an enumerator that iterates through the collection. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> An enumerator that can be used to iterate through the collection. </returns>
    Public Overloads Function GetEnumerator() As IEnumerator(Of T)
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.ToList().GetEnumerator()
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

    ''' <summary>
    ''' Determines the index of a specific item in the
    ''' <see cref="T:System.Collections.Generic.IList`1" />
    ''' .
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="item"> The object to locate in the
    '''                     <see cref="T:System.Collections.Generic.IList`1" />
    '''                     . </param>
    ''' <returns>
    ''' The index of <paramref name="item" />
    '''  if found in the list; otherwise, -1.
    ''' </returns>
    Public Overloads Function IndexOf(item As T) As Integer
        Me.ItemsLocker.EnterReadLock()
        Try
            Return Me.Items.IndexOf(item)
        Finally
            Me.ItemsLocker.ExitReadLock()
        End Try
    End Function

#End Region

#Region " PROTECTED OVERRIDES "

    ''' <summary> Removes all items from the collection. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Protected Overrides Sub ClearItems()
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            MyBase.ClearItems()
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Inserts an item into the collection at the specified index. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="index"> The zero-based index at which <paramref name="item" />
    '''                       should be inserted. </param>
    ''' <param name="item">  The object to insert. </param>
    Protected Overrides Sub InsertItem(index As Integer, item As T)
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            Me.EnsureIndexRange(index)
            MyBase.InsertItem(index, item)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Base remove item. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="index"> Zero-based index of the. </param>
    Protected Sub BaseRemoveItem(ByVal index As Integer)
        MyBase.RemoveItem(index)
    End Sub

    ''' <summary> Removes the item at the specified index of the collection. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="index"> The zero-based index of the element to remove. </param>
    Protected Overrides Sub RemoveItem(ByVal index As Integer)
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            Me.EnsureIndexRange(index)
            MyBase.RemoveItem(index)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Replaces the element at the specified index. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="index"> The zero-based index of the element to replace. </param>
    ''' <param name="item">  The new value for the element at the specified index. </param>
    Protected Overrides Sub SetItem(index As Integer, item As T)
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            Me.EnsureIndexRange(index)
            MyBase.SetItem(index, item)
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

#End Region

#Region " QUEUE AND STACK OPERATIONS "

    ''' <summary> Adds an object onto the end of this queue. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="item"> The item. </param>
    Public Sub Enqueue(ByVal item As T)
        Me.Add(item)
    End Sub

    ''' <summary> Removes the head object from this queue. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> The head object from this queue. </returns>
    Public Function Dequeue() As T
        Me.ItemsLocker.EnterUpgradeableReadLock()
        Try
            Me.EnsureNotEmpty()
            Dim item As T = Me.BaseItem(0)
            Try
                Me.ItemsLocker.EnterWriteLock()
                Me.BaseRemoveItem(0)
                Return item
            Finally
                Me.ItemsLocker.ExitWriteLock()
            End Try
        Finally
            Me.ItemsLocker.ExitUpgradeableReadLock()
        End Try
    End Function

    ''' <summary> Returns the top-of-stack object without removing it. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> The current top-of-stack object. </returns>
    Public Function Peek() As T
        Return Me.Item(0)
    End Function

    ''' <summary> Pushes an object onto this stack. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="item"> The object to add to the
    '''                     <see cref="T:System.Collections.Generic.ICollection`1" />
    '''                     . </param>
    Public Sub Push(ByVal item As T)
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.EnsureNotReadOnly()
            If MyBase.Any Then
                MyBase.InsertItem(0, item)
            Else
                MyBase.Add(item)
            End If
        Finally
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Removes and returns the top-of-stack object. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> The previous top-of-stack object. </returns>
    Public Function Pop() As T
        Return Me.Dequeue
    End Function

#End Region

#Region " BULK OPERATIONS "

    ''' <summary>
    ''' Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Overloads Sub Clear()
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.RaiseListChangedEvents = False
            Me.EnsureNotReadOnly()
            MyBase.ClearItems()
        Finally
            Me.RaiseListChangedEvents = True
            Me.ResetBindings()
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Adds an object onto the end of this queue. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="items"> The items. </param>
    Public Sub Enqueue(ByVal items As IList(Of T))
        If items Is Nothing Then Throw New ArgumentNullException(NameOf(items))
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.RaiseListChangedEvents = False
            Me.EnsureNotReadOnly()
            For Each item As T In items
                MyBase.Add(item)
            Next
        Finally
            Me.RaiseListChangedEvents = True
            Me.ResetBindings()
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Adds items onto the end of this queue. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="items"> The items. </param>
    Public Sub Enqueue(ByVal items As IEnumerable(Of T))
        If items Is Nothing Then Throw New ArgumentNullException(NameOf(items))
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.RaiseListChangedEvents = False
            Me.EnsureNotReadOnly()
            For Each item As T In items
                MyBase.Add(item)
            Next
        Finally
            Me.RaiseListChangedEvents = True
            Me.ResetBindings()
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Pushes an object onto this stack. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="items"> The items. </param>
    Public Sub Push(items As IList(Of T))
        If items Is Nothing Then Throw New ArgumentNullException(NameOf(items))
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.RaiseListChangedEvents = False
            Me.EnsureNotReadOnly()
            For Each item As T In items
                If MyBase.Any Then
                    MyBase.Add(item)
                Else
                    MyBase.InsertItem(0, item)
                End If
            Next
        Finally
            Me.RaiseListChangedEvents = True
            Me.ResetBindings()
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Pushes an object onto this stack. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="items"> The items. </param>
    Public Sub Push(items As IEnumerable(Of T))
        If items Is Nothing Then Throw New ArgumentNullException(NameOf(items))
        Me.ItemsLocker.EnterWriteLock()
        Try
            Me.RaiseListChangedEvents = False
            Me.EnsureNotReadOnly()
            For Each item As T In items
                If MyBase.Any Then
                    MyBase.Add(item)
                Else
                    MyBase.InsertItem(0, item)
                End If
            Next
        Finally
            Me.RaiseListChangedEvents = True
            Me.ResetBindings()
            Me.ItemsLocker.ExitWriteLock()
        End Try
    End Sub

    ''' <summary> Removes the head objects from this queue. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="count"> The count. </param>
    ''' <returns> The head object from this queue. </returns>
    Public Function Dequeue(ByVal count As Integer) As IList(Of T)
        Me.ItemsLocker.EnterUpgradeableReadLock()
        Dim l As New List(Of T)
        Try
            Me.EnsureNotEmpty()
            Try
                Me.ItemsLocker.EnterWriteLock()
                Me.RaiseListChangedEvents = False
                Do While MyBase.Any AndAlso l.Count < count
                    l.Add(Me.BaseItem(0))
                    Me.BaseRemoveItem(0)
                Loop
            Finally
                Me.RaiseListChangedEvents = True
                Me.ResetBindings()
                Me.ItemsLocker.ExitWriteLock()
            End Try
        Finally
            Me.ItemsLocker.ExitUpgradeableReadLock()
        End Try
        Return l
    End Function

    ''' <summary> Removes and returns the top-of-stack objects. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="count"> The count. </param>
    ''' <returns> The previous top-of-stack object. </returns>
    Public Function Pop(ByVal count As Integer) As IList(Of T)
        Return Me.Dequeue(count)
    End Function

    ''' <summary> Removes items from the head of the queue (top of the tack). </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="count"> The count. </param>
    ''' <returns> The number of removed items. </returns>
    Public Function TrimStart(ByVal count As Integer) As Integer
        Me.ItemsLocker.EnterUpgradeableReadLock()
        Dim result As Integer = 0
        Try
            If MyBase.Any Then
                Try
                    Me.ItemsLocker.EnterWriteLock()
                    Me.RaiseListChangedEvents = False
                    Do While MyBase.Any AndAlso result < count
                        Me.BaseRemoveItem(0)
                        result += 1
                    Loop
                Finally
                    Me.RaiseListChangedEvents = True
                    Me.ResetBindings()
                    Me.ItemsLocker.ExitWriteLock()
                End Try
            End If
        Finally
            Me.ItemsLocker.ExitUpgradeableReadLock()
        End Try
        Return result
    End Function

    ''' <summary> Removes items from the tail of the queue (bottom of the tack). </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="count"> The count. </param>
    ''' <returns> The number of removed items. </returns>
    Public Function TrimEnd(ByVal count As Integer) As Integer
        Me.ItemsLocker.EnterUpgradeableReadLock()
        Dim result As Integer = 0
        Try
            If MyBase.Any Then
                Try
                    Me.ItemsLocker.EnterWriteLock()
                    Me.RaiseListChangedEvents = False
                    Do While MyBase.Any AndAlso result < count
                        Me.BaseRemoveItem(Me.Items.Count - 1)
                        result += 1
                    Loop
                Finally
                    Me.RaiseListChangedEvents = True
                    Me.ResetBindings()
                    Me.ItemsLocker.ExitWriteLock()
                End Try
            End If
        Finally
            Me.ItemsLocker.ExitUpgradeableReadLock()
        End Try
        Return result
    End Function

#End Region

#Region " THREAD SAFE EVENT HANDLERS "

    ''' <summary> Gets the context. </summary>
    ''' <value> The context. </value>
    Private Shared ReadOnly Property Ctx As SynchronizationContext
        Get
            Return SynchronizationContext.Current
        End Get
    End Property

    ''' <summary>
    ''' Raises the <see cref="E:System.ComponentModel.BindingList`1.AddingNew" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="e"> An <see cref="T:System.ComponentModel.AddingNewEventArgs" /> that contains
    '''                  the event data. </param>
    Protected Overrides Sub OnAddingNew(ByVal e As AddingNewEventArgs)
        If Ctx Is Nothing Then
            Me.BaseAddingNew(e)
        Else
            Ctx.Send(Sub()
                         Me.BaseAddingNew(e)
                     End Sub, Nothing)
        End If
    End Sub

    ''' <summary> Base adding new. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="e"> Adding new event information. </param>
    Private Sub BaseAddingNew(ByVal e As AddingNewEventArgs)
        MyBase.OnAddingNew(e)
    End Sub

    ''' <summary>
    ''' Raises the <see cref="E:System.ComponentModel.BindingList`1.ListChanged" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.ListChangedEventArgs" /> that contains
    '''                  the event data. </param>
    Protected Overrides Sub OnListChanged(ByVal e As ListChangedEventArgs)
        If Ctx Is Nothing Then
            Me.BaseListChanged(e)
        Else
            Ctx.Send(Sub()
                         Me.BaseListChanged(e)
                     End Sub, Nothing)
        End If
    End Sub

    ''' <summary> Base list changed. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="e"> List changed event information. </param>
    Private Sub BaseListChanged(ByVal e As ListChangedEventArgs)
        MyBase.OnListChanged(e)
    End Sub

#End Region

End Class

