Imports System.Threading

''' <summary> A Thread safe token. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/29/2017 </para>
''' </remarks>
<DebuggerDisplay("Value = {Value}")>
Public Class ConcurrentToken(Of T)
    Implements IDisposable

#Region " CONSTRUCTOR and CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-08-20. </remarks>
    Public Sub New()
        MyBase.New()
        Me._SlimLock = New ReaderWriterLockSlim()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-08-20. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As T)
        Me.New()
        Me._Value = value
    End Sub

    ''' <summary> Gets or sets the sentinel to detect redundant calls. </summary>
    ''' <value> The sentinel to detect redundant calls. </value>
    Protected ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Models.ThreadSafeToken(Of T) and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                End If
                If Me._SlimLock IsNot Nothing Then Me._SlimLock.Dispose() : Me._SlimLock = Nothing
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        ' uncomment the following line if Finalize() is overridden above.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region
    ''' <summary> The slim lock. </summary>
    Private _SlimLock As ReaderWriterLockSlim
    ''' <summary> The value. </summary>
    Private _Value As T

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property Value As T
        Get
            Me._SlimLock.EnterReadLock()
            Try
                Return Me._Value
            Finally
                Me._SlimLock.ExitReadLock()
            End Try
        End Get
        Set(value As T)
            Me._SlimLock.EnterWriteLock()
            Try
                Me._Value = value
            Finally
                Me._SlimLock.ExitWriteLock()
            End Try
        End Set
    End Property

End Class
