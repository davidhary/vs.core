''' <summary> Class encapsulating a list of Action delegates taking one arguments. </summary>
''' <remarks>
''' David, 2/3/2019, https://github.com/polterguy/lizzie/. <para>
''' Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com  </para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class Actions(Of TArgument)
    Inherits Sequence(Of Action(Of TArgument))

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.delegates.Sequence"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="lambdas"> Initial functors. </param>
    Public Sub New(ParamArray ByVal lambdas() As Action(Of TArgument))
        MyBase.New(lambdas)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.delegates.Sequence"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="lambdas"> Initial functors. </param>
    Public Sub New(ByVal lambdas As IEnumerable(Of Action(Of TArgument)))
        MyBase.New(lambdas)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="lambdas"> Initial items. </param>
    Public Sub New(ByVal lambdas As IList(Of Action(Of TArgument)))
        MyBase.New(lambdas)
    End Sub

    ''' <summary> Sequentially executes each action. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="actionArgument"> The action argument. </param>
    Public Sub Execute(ByVal actionArgument As TArgument)
        Executor.Sequentially(Me.Select(Function(ix) New Action(Sub() ix(actionArgument))))
    End Sub

    ''' <summary> Sequentially executes each action without blocking the calling thread. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="actionArgument"> The action argument. </param>
    Public Sub ExecuteUnblocked(ByVal actionArgument As TArgument)
        Executor.SequentiallyUnblocked(Me.Select(Function(ix) New Action(Sub() ix(actionArgument))))
    End Sub

    ''' <summary>
    ''' Executes each action in parallel blocking the calling thread until all actions are finished
    ''' executing.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="actionArgument"> The action argument. </param>
    Public Sub ExecuteParallel(ByVal actionArgument As TArgument)
        Executor.Parallel(Me.Select(Function(ix) New Action(Sub() ix(actionArgument))))
    End Sub

    ''' <summary> Executes each action in parallel without blocking the calling thread. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="actionArgument"> The action argument. </param>
    Public Sub ExecuteParallelUnblocked(ByVal actionArgument As TArgument)
        Executor.ParallelUnblocked(Me.Select(Function(ix) New Action(Sub() ix(actionArgument))))
    End Sub
End Class
