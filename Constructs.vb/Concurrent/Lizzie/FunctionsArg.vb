''' <summary> Class encapsulating a list of Func delegates taking one argument. </summary>
''' <remarks>
''' Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com <para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class Functions(Of TArgument, TResult)
    Inherits Sequence(Of Func(Of TArgument, TResult))

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Functions`1"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.delegates.Functions`1"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="lambdas"> Initial functors. </param>
    Public Sub New(ParamArray ByVal lambdas() As Func(Of TArgument, TResult))
        MyBase.New(lambdas)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.delegates.Functions`1"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="lambdas"> Initial functors. </param>
    Public Sub New(ByVal lambdas As IEnumerable(Of Func(Of TArgument, TResult)))
        MyBase.New(lambdas)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="lambdas"> Initial items. </param>
    Public Sub New(ByVal lambdas As IList(Of Func(Of TArgument, TResult)))
        MyBase.New(lambdas)
    End Sub

    ''' <summary> Evaluate each function in a sequence on the calling thread. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="functionArgument"> T1. </param>
    ''' <returns> The sequence. </returns>
    Public Function Evaluate(ByVal functionArgument As TArgument) As IEnumerable(Of TResult)
        Return Evaluator(Of TResult).Sequentially(Me.Select(Function(ix) New Func(Of TResult)(Function() ix(functionArgument))))
    End Function

    ''' <summary> Evaluates each function in parallel and returns the result to caller. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="functionArgument"> T1. </param>
    ''' <returns> The result of each function invocation. </returns>
    Public Function EvaluateParallel(ByVal functionArgument As TArgument) As IEnumerable(Of TResult)
        Return Evaluator(Of TResult).Parallel(Me.Select(Function(ix) New Func(Of TResult)(Function() ix(functionArgument))))
    End Function
End Class
