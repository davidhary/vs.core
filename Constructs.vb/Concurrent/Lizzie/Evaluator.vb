Imports System.Threading

''' <summary> Class allowing you to evaluate a list of functions. </summary>
''' <remarks>
''' David, 2/3/2019, https://github.com/polterguy/lizzie/. <para>
''' Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com </para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public NotInheritable Class Evaluator(Of TResult)

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Private Sub New()
        MyBase.New
    End Sub

    ''' <summary> Evaluates each function in order and returns the result to caller. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="functions"> Functions. </param>
    ''' <returns> The sequence. </returns>
    Public Shared Iterator Function Sequentially(ByVal functions As IEnumerable(Of Func(Of TResult))) As IEnumerable(Of TResult)
        ' Sequentially execute each action on calling thread.
        For Each ix As Func(Of TResult) In functions
            Yield ix()
        Next ix
    End Function

    ''' <summary>
    ''' Evaluates each function in parallel blocking the calling thread until all functions are
    ''' evaluated, and returns the results of the evaluation of each function to caller.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="functions"> Functions to evaluate. </param>
    ''' <returns> The result of each function. </returns>
    Public Shared Iterator Function Parallel(ByVal functions As IEnumerable(Of Func(Of TResult))) As IEnumerable(Of TResult)
        ' Synchronizing access to return values.
        Dim result As New List(Of TResult)()
        Dim sync As New Synchronizer(Of List(Of TResult))(result)

        ' Creates and starts a new thread for each action.
        Dim threads As List(Of Thread) = functions.Select(Function(ix) New Thread(New ThreadStart(Sub()
                                                                                                      Dim res As TResult = ix()
                                                                                                      sync.Write(Sub(sh) sh.Add(res))
                                                                                                  End Sub))).ToList()
        threads.ForEach(Sub(ix) ix.Start())
        threads.ForEach(Sub(ix) ix.Join())
        For Each ix As TResult In result
            Yield ix
        Next ix
    End Function
End Class
