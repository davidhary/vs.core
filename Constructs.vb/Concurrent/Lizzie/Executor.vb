Imports System.Threading

''' <summary> Class allowing you to execute a list of actions. </summary>
''' <remarks>
''' Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com <para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public NotInheritable Class Executor

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Private Sub New()
    End Sub

    ''' <summary> Sequentially executes each action on calling thread. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="actions"> Actions to execute. </param>
    Public Shared Sub Sequentially(ByVal actions As Action())
        If actions Is Nothing Then Throw New ArgumentNullException(NameOf(actions))
        ' Sequentially execute each action on calling thread.
        For Each ix As Action In actions
            ix()
        Next ix
    End Sub

    ''' <summary> Sequentially executes each action on calling thread. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="actions"> Actions to execute. </param>
    Public Shared Sub Sequentially(ByVal actions As IEnumerable(Of Action))
        If actions Is Nothing Then Throw New ArgumentNullException(NameOf(actions))
        ' Sequentially execute each action on calling thread.
        For Each ix As Action In actions
            ix()
        Next ix
    End Sub

    ''' <summary> Sequentially executes each action on calling thread. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="actions"> Actions to execute. </param>
    Public Shared Sub Sequentially(ByVal actions As IList(Of Action))
        If actions Is Nothing Then Throw New ArgumentNullException(NameOf(actions))
        ' Sequentially execute each action on calling thread.
        For Each ix As Action In actions
            ix()
        Next ix
    End Sub

    ''' <summary>
    ''' Sequentially executes each action on a different thread without blocking the calling thread.
    ''' </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="actions"> Actions to execute. </param>
    Public Shared Sub SequentiallyUnblocked(ByVal actions As Action())
        If actions Is Nothing Then Throw New ArgumentNullException(NameOf(actions))
        ' Sanity checking argument.
        If Not actions.Any() Then
            Return ' No reasons to create our thread.
        End If

        ' Executes actions on another thread.
        Dim thread As Thread = New Thread(New ThreadStart(Sub() Sequentially(actions)))
        thread.Start()
    End Sub

    ''' <summary>
    ''' Sequentially executes each action on a different thread without blocking the calling thread.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="actions"> Actions to execute. </param>
    Public Shared Sub SequentiallyUnblocked(ByVal actions As IEnumerable(Of Action))
        If actions Is Nothing Then Throw New ArgumentNullException(NameOf(actions))
        ' Sanity checking argument.
        If Not actions.Any() Then
            Return ' No reasons to create our thread.
        End If

        ' Executes actions on another thread.
        Dim thread As Thread = New Thread(New ThreadStart(Sub() Sequentially(actions)))
        thread.Start()
    End Sub

    ''' <summary>
    ''' Sequentially executes each action on a different thread without blocking the calling thread.
    ''' </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="actions"> Actions to execute. </param>
    Public Shared Sub SequentiallyUnblocked(ByVal actions As IList(Of Action))
        If actions Is Nothing Then Throw New ArgumentNullException(NameOf(actions))
        ' Sanity checking argument.
        If Not actions.Any() Then
            Return ' No reasons to create our thread.
        End If

        ' Executes actions on another thread.
        Dim thread As Thread = New Thread(New ThreadStart(Sub() Sequentially(actions)))
        thread.Start()
    End Sub

    ''' <summary>
    ''' Executes each action in parallel blocking the calling thread until all actions are finished
    ''' executing.
    ''' </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="actions"> Actions to execute. </param>
    Public Shared Sub Parallel(ByVal actions As Action())
        If actions Is Nothing Then Throw New ArgumentNullException(NameOf(actions))
        Dim threads As List(Of Thread) = actions.[Select](Function(ix) New Thread(New ThreadStart(Sub() ix()))).ToList()
        threads.ForEach(Sub(ix) ix.Start())
        threads.ForEach(Sub(ix) ix.Join())
    End Sub

    ''' <summary>
    ''' Executes each action in parallel blocking the calling thread until all actions are finished
    ''' executing.
    ''' </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="actions"> Actions to execute. </param>
    Public Shared Sub Parallel(ByVal actions As IEnumerable(Of Action))
        If actions Is Nothing Then Throw New ArgumentNullException(NameOf(actions))
        Dim threads As List(Of Thread) = actions.[Select](Function(ix) New Thread(New ThreadStart(Sub() ix()))).ToList()
        threads.ForEach(Sub(ix) ix.Start())
        threads.ForEach(Sub(ix) ix.Join())
    End Sub

    ''' <summary>
    ''' Executes each action in parallel blocking the calling thread until all actions are finished
    ''' executing.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="actions"> Actions to execute. </param>
    Public Shared Sub Parallel(ByVal actions As IList(Of Action))
        If actions Is Nothing Then Throw New ArgumentNullException(NameOf(actions))
        Dim threads As List(Of Thread) = actions.[Select](Function(ix) New Thread(New ThreadStart(Sub() ix()))).ToList()
        threads.ForEach(Sub(ix) ix.Start())
        threads.ForEach(Sub(ix) ix.Join())
    End Sub

    ''' <summary> Executes each action in parallel without blocking the calling thread. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="actions"> Actions to execute. </param>
    Public Shared Sub ParallelUnblocked(ByVal actions As Action())
        If actions Is Nothing Then Throw New ArgumentNullException(NameOf(actions))
        For Each ix As Action In actions
            Dim thread As Thread = New Thread(New ThreadStart(Sub() ix()))
            thread.Start()
        Next ix
    End Sub

    ''' <summary> Executes each action in parallel without blocking the calling thread. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="actions"> Actions to execute. </param>
    Public Shared Sub ParallelUnblocked(ByVal actions As IEnumerable(Of Action))
        If actions Is Nothing Then Throw New ArgumentNullException(NameOf(actions))
        For Each ix As Action In actions
            Dim thread As Thread = New Thread(New ThreadStart(Sub() ix()))
            thread.Start()
        Next ix
    End Sub

    ''' <summary> Executes each action in parallel without blocking the calling thread. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="actions"> Actions to execute. </param>
    Public Shared Sub ParallelUnblocked(ByVal actions As IList(Of Action))
        If actions Is Nothing Then Throw New ArgumentNullException(NameOf(actions))
        For Each ix As Action In actions
            Dim thread As Thread = New Thread(New ThreadStart(Sub() ix()))
            thread.Start()
        Next ix
    End Sub


End Class

