''' <summary> Class encapsulating a list of Func delegates taking no arguments. </summary>
''' <remarks>
''' Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com <para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class Functions(Of TResult)
    Inherits Sequence(Of Func(Of TResult))

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Functions`1"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.delegates.Functions`1"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="functions"> Initial functions. </param>
    Public Sub New(ParamArray ByVal functions() As Func(Of TResult))
        MyBase.New(functions)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.delegates.Functions`1"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="functions"> Initial functions. </param>
    Public Sub New(ByVal functions As IEnumerable(Of Func(Of TResult)))
        MyBase.New(functions)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="functions"> Initial items. </param>
    Public Sub New(ByVal functions As IList(Of Func(Of TResult)))
        MyBase.New(functions)
    End Sub

    ''' <summary> Evaluate each function in a sequence on the calling thread. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> The result of each evaluation. </returns>
    Public Function Evaluate() As IEnumerable(Of TResult)
        Return Evaluator(Of TResult).Sequentially(Me)
    End Function

    ''' <summary> Evaluates each function in parallel and returns the result to caller. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> The result of each function invocation. </returns>
    Public Function EvaluateParallel() As IEnumerable(Of TResult)
        Return Evaluator(Of TResult).Parallel(Me)
    End Function


End Class

