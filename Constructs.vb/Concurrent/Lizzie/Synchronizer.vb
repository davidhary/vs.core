Imports System.Threading

''' <summary>
''' Allows you to encapsulate an instance of a type that needs to be shared between multiple
''' threads, such that access to the instance is easily synchronized.
''' </summary>
''' <remarks>
''' <para>
''' The <see cref="ReaderWriterLockSlim"/> class allows you to specify if your code is writing to
''' the object or simply reading from the object. This enables multiple readers entrance at the
''' same time, but denies any write code access until all other read and write threads are done
''' doing their stuff. </para>
''' <para>
''' Now the problem: The syntax when consuming the ReaderWriterLock class becomes tedious, with
''' lots of repetitive code that reduces readability and complicates maintenance over time, and
''' your code often becomes scattered with multiple try and finally blocks. A simple typo can
''' also produce disastrous effects that are sometimes extremely difficult to spot later.</para>
''' <para>
''' This class solves the problem without repetitive code, while reducing the risk that a minor
''' typo will spoil your day. The class is entirely based on lambda trickery. It’s arguably just
''' syntactic sugar around some delegates, assuming the existence of a couple interfaces. Most
''' important, it can help make your code much more DRY (as in, “Don’t Repeat Yourself”).</para>
''' <para>Implementation</para><para>
''' The class assumes you have a read interface and a write interface on your type. You can also
''' use it by repeating the template class itself three times, if for some reason you can’t
''' change the implementation of the underlying class to which you need to synchronize access.
''' </para><para>
''' David, 2/3/2019. https://msdn.microsoft.com/en-us/magazine/mt833270.aspx. </para><para>
''' Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com </para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class Synchronizer(Of TScribe As {TIWrite, TIRead}, TIRead, TIWrite)
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> The slim lock. </summary>
    ''' <remarks>  Our actual locker, that will synchronize access to our _shared instance. </remarks>
    Private _SlimLock As New ReaderWriterLockSlim()

    ''' <summary> The scribe. </summary>
    ''' <remarks> Our actual scribe (read and write) resource. </remarks>
    Private _Scribe As TScribe

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:threadsynchronization.Synchronizer`1"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="scribe"> Instance that needs synchronize access in multiple threads. </param>
    Public Sub New(ByVal scribe As TScribe)
        Me._Scribe = scribe
    End Sub

    ''' <summary> Gets or sets the sentinel to detect redundant calls. </summary>
    ''' <value> The sentinel to detect redundant calls. </value>
    Protected ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Models.ThreadSafeToken(Of T) and
    ''' optionally releases the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                End If
                If Me._SlimLock IsNot Nothing Then Me._SlimLock.Dispose() : Me._SlimLock = Nothing
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        ' uncomment the following line if Finalize() is overridden above.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

    ''' <summary>
    ''' Enters a read lock giving the caller access to the shared instance in "read only" mode.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="functor"> Functor. </param>
    Public Sub Read(ByVal functor As Action(Of TIRead))
        If functor Is Nothing Then Throw New ArgumentNullException(NameOf(functor))
        Me._SlimLock.EnterReadLock()
        Try
            functor(Me._Scribe)
        Finally
            Me._SlimLock.ExitReadLock()
        End Try
    End Sub

    ''' <summary>
    ''' Enters a write lock giving the caller access to the shared resource in "read and write" mode.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="functor"> Functor. </param>
    Public Sub Write(ByVal functor As Action(Of TIWrite))
        If functor Is Nothing Then Throw New ArgumentNullException(NameOf(functor))
        Me._SlimLock.EnterWriteLock()
        Try
            functor(Me._Scribe)
        Finally
            Me._SlimLock.ExitWriteLock()
        End Try
    End Sub

    ''' <summary>
    ''' Enters a write lock giving the caller access to the shared resource in "read and write" mode,
    ''' for then to reassign the shared object to the value returned from the Func.
    ''' 
    ''' Useful for changing immutable instances.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="functor"> Functor. </param>
    Public Sub Assign(ByVal functor As Func(Of TIWrite, TScribe))
        If functor Is Nothing Then Throw New ArgumentNullException(NameOf(functor))
        Me._SlimLock.EnterWriteLock()
        Try
            Me._Scribe = functor(Me._Scribe)
        Finally
            Me._SlimLock.ExitWriteLock()
        End Try
    End Sub

End Class

''' <summary>
''' Simplified syntax where you cannot modify the shared type and implement your own read and
''' write interfaces.
''' 
''' Notice, when using this class you are on your own in regards to making sure you never
''' actually modify the shared instance inside a "read only" delegate.
''' </summary>
''' <remarks> David, 2020-09-22. </remarks>
Public Class Synchronizer(Of TScribe)
    Inherits Synchronizer(Of TScribe, TScribe, TScribe)

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:threadsynchronization.Synchronizer`1" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="scribe"> Instance that needs synchronize access in multiple threads. </param>
    Public Sub New(ByVal scribe As TScribe)
        MyBase.New(scribe)
    End Sub
End Class
