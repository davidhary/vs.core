''' <summary> Contains a sequence of TLambda instances. </summary>
''' <remarks>
''' Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com <para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public MustInherit Class Sequence(Of T)
    Implements IEnumerable(Of T)

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Protected Sub New()
        Me.List = New List(Of T)()
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="items"> Initial items. </param>
    Protected Sub New(ParamArray ByVal items() As T)
        Me.List = New List(Of T)(items)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="items"> Initial items. </param>
    Protected Sub New(ByVal items As IEnumerable(Of T))
        Me.List = New List(Of T)(items)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="items"> Initial items. </param>
    Protected Sub New(ByVal items As IList(Of T))
        Me.List = New List(Of T)(items)
    End Sub

    ''' <summary> Gets or sets the list. </summary>
    ''' <value> The list. </value>
    Private ReadOnly Property List As List(Of T)

    ''' <summary> Appends the specified item to the sequence. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="item"> Item to append. </param>
    Public Sub Add(ByVal item As T)
        Me.List.Add(item)
    End Sub

    ''' <summary> Adds a range of items to the sequence. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="items"> Items to add. </param>
    Public Sub AddRange(ParamArray ByVal items() As T)
        Me.List.AddRange(items)
    End Sub

    ''' <summary> Adds a range of items to the sequence. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="items"> Items to add. </param>
    Public Sub AddRange(ByVal items As IEnumerable(Of T))
        Me.List.AddRange(items)
    End Sub

    ''' <summary> Adds a range of items to the sequence. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="items"> Items to add. </param>
    Public Sub AddRange(ByVal items As IList(Of T))
        Me.List.AddRange(items)
    End Sub

#Region "[ -- Interface implementations -- ]"

    ''' <summary> Gets the enumerator for the items. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> The enumerator. </returns>
    Public Function GetEnumerator() As IEnumerator(Of T) Implements IEnumerable(Of T).GetEnumerator
        Return Me.List.GetEnumerator()
    End Function

    Private Function IEnumerable_GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
        Return Me.GetEnumerator()
    End Function
#End Region

End Class
