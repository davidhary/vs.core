''' <summary>
''' Class encapsulating a list of Func delegates where the output from one is being passed in as
''' input to the next.
''' </summary>
''' <remarks>
''' Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com <para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Class Chain(Of T)
    Inherits Functions(Of T, T)

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Chain`1"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Chain`1"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="functions"> Initial functors. </param>
    Public Sub New(ParamArray ByVal functions() As Func(Of T, T))
        MyBase.New(functions)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Chain`1"/> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="functions"> Initial functors. </param>
    Public Sub New(ByVal functions As IEnumerable(Of Func(Of T, T)))
        MyBase.New(functions)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="functions"> Initial items. </param>
    Public Sub New(ByVal functions As IList(Of Func(Of T, T)))
        MyBase.New(functions)
    End Sub

    ''' <summary> Evaluates the chain, and returns the result to caller. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="actionArgument"> The action argument. </param>
    ''' <returns> A T. </returns>
    Public Function EvaluateChain(ByVal actionArgument As T) As T
        For Each ix As Func(Of T, T) In Me
            actionArgument = ix(actionArgument)
        Next ix
        Return actionArgument
    End Function
End Class
