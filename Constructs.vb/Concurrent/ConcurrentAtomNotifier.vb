''' <summary>
''' Implements a thread safe notifier based on the
''' <see cref="Synchronizer(Of TImpl, TIRead, TIWrite)"/> class.
''' </summary>
''' <remarks>
''' <para>
''' Regardless of how many threads are accessing the property, no Write method will be invoked as
''' long as another Read or Write method is being executed. However, multiple Read methods can be
''' invoked simultaneously, without having to scatter your code with multiple try/catch/finally
''' statements, or repeating the same code over and over.
''' </para><para>
''' For the record, consuming it with a simple string is meaningless, because System.String is
''' immutable.
''' </para><para>
''' The basic idea Is that all methods that can modify the state Of your instance must be added
''' To the IWriteToShared Interface. At the same time, all methods that only read from your
''' instance should be added To the IReadFromShared Interface. By separating your concerns Like
''' this into two distinct interfaces, And implementing both interfaces On your underlying type,
''' you can Then use the Synchronizer Class To synchronize access To your instance. Just Like
''' that, the art Of synchronizing access To your code becomes much simpler, And you can Do it
''' For the most part In a much more declarative manner.
''' </para><para>
''' When it comes to multi-threaded programming, syntactic sugar might be the difference between
''' success And failure. Debugging multi-threaded code Is often extremely difficult, And creating
''' unit tests for synchronization objects can be an exercise in futility.
''' </para><para>
''' If you want, you can create an overloaded type With only one generic argument, inheriting
''' from the original Synchronizer Class And passing On its Single generic argument As the type
''' argument three times To its base Class. Doing this, you won't need the read or write
''' interfaces, since you can simply use the concrete implementation of your type. However, this
''' approach requires that you manually take care of those parts that need to use either the
''' Write or Read method. It’s also slightly less safe, but does allow you to wrap classes you
''' cannot change into a Synchronizer instance.</para> <para>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para><para>
''' David, 2/3/2019 </para>
''' </remarks>
Public Class ConcurrentAtomNotifier(Of T As {IComparable(Of T), IEquatable(Of T)})
    Inherits isr.Core.Models.ViewModelBase
    Implements IDisposable


#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Gets or sets the is disposed. </summary>
    ''' <value> The is disposed. </value>
    Protected ReadOnly Property IsDisposed As Boolean

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(disposing As Boolean)
        Try
            If Not Me.IsDisposed Then
                If disposing Then
                End If
                If Me._Sync IsNot Nothing Then Me._Sync.Dispose() : Me._Sync = Nothing
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
        Me.Dispose(True)
        ' uncomment the following line if Finalize() is overridden above.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#Region " VALUE "
    ''' <summary> The value. </summary>
    Private _Value As T

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property [Value] As T
        Get
            Me._Sync.Read(Sub(x) Me._Value = x.GetValue())
            Return Me._Value
        End Get
        Set(ByVal value As T)
            If Not value.Equals(Me.Value) Then
                Me._Sync.Write(Sub(x) x.SetValue(value))
                Me.AsyncNotifyPropertyChanged()
                isr.Core.My.MyLibrary.DoEvents()
            End If
        End Set
    End Property

#End Region

#Region " SYNCHRONIZER "
    ''' <summary> The synchronize. </summary>
    Private _Sync As New Synchronizer(Of MySharedClass, IReadFromShared, IWriteToShared)(New MySharedClass)

    ''' <summary> Interface for read from shared. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Private Interface IReadFromShared

        ''' <summary> Gets the value. </summary>
        ''' <remarks> David, 2020-09-22. </remarks>
        ''' <returns> The value. </returns>
        Function GetValue() As T
    End Interface

    ''' <summary> Interface for write to shared. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Friend Interface IWriteToShared

        ''' <summary> Sets a value. </summary>
        ''' <remarks> David, 2020-09-22. </remarks>
        ''' <param name="value"> The value. </param>
        Sub SetValue(ByVal value As T)
    End Interface

    ''' <summary> my shared class. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Private Class MySharedClass
        Implements IReadFromShared, IWriteToShared
        ''' <summary> The foo. </summary>
        Private _Foo As T

        ''' <summary> Gets the value. </summary>
        ''' <remarks> David, 2020-09-22. </remarks>
        ''' <returns> The value. </returns>
        Public Function GetValue() As T Implements IReadFromShared.GetValue
            Return Me._Foo
        End Function

        ''' <summary> Sets a value. </summary>
        ''' <remarks> David, 2020-09-22. </remarks>
        ''' <param name="value"> The value. </param>
        Public Sub SetValue(ByVal value As T) Implements IWriteToShared.SetValue
            Me._Foo = value
        End Sub
    End Class

#End Region

End Class
