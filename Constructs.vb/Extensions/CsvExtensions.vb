Imports System.Runtime.CompilerServices

Namespace CommaSeparatedValuesExtensions

    ''' <summary> Extension methods for building dictionaries from comma-separated-values. </summary>
    ''' <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

        ''' <summary> Builds a Dictionary(Of String, RangeR). </summary>
        ''' <remarks> David, 2020-09-22. </remarks>
        ''' <param name="keyValuePairs"> The key value pairs comma-separated text. </param>
        ''' <param name="key">           A key argument for permitting overrides. </param>
        ''' <param name="value">         A value argument for permitting overrides. </param>
        ''' <returns> A Dictionary(Of Integer, String) </returns>
        <CodeAnalysis.SuppressMessage("Style", "IDE0060:Remove unused parameter", Justification:="<Pending>")>
        <Extension>
        Public Function BuildDictionary(ByVal keyValuePairs As String, ByVal key As String, ByVal value As Core.Constructs.RangeR) As Dictionary(Of String, isr.Core.Constructs.RangeR)
            Dim dix As New Dictionary(Of String, isr.Core.Constructs.RangeR)
            Dim values As Queue(Of String) = isr.Core.CommaSeparatedValuesExtensions.Methods.SplitQueue(keyValuePairs)
            Dim min As Double = 0
            Dim max As Double = 0
            Do While values.Any
                key = values.Dequeue
                If values.Any AndAlso Double.TryParse(values.Dequeue, min) Then
                    If values.Any AndAlso Double.TryParse(values.Dequeue, max) Then
                        dix.Add(key, New Core.Constructs.RangeR(min, max))
                    End If
                End If
            Loop
            Return dix
        End Function


    End Module

End Namespace
