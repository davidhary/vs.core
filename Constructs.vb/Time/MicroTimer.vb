''' <summary> A micro timer. </summary>
''' <remarks>
''' (c) 2013 Ken Loveday. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 11/25/2019, </para><para>
''' https://www.codeproject.com/Articles/98346/Microsecond-and-Millisecond-NET-Timer. </para>
''' </remarks>
Public Class MicroTimer

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub New()
        MyBase.New
        Me._TimerThread = Nothing
        Me._IgnoreEventIfLateBy = Long.MaxValue
        Me._Interval = 0
        Me._StopTimer = True
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="microsecondsInterval"> The microseconds interval. </param>
    Public Sub New(ByVal microsecondsInterval As Long)
        Me.New()
        Me.Interval = microsecondsInterval
    End Sub
    ''' <summary> The interval. </summary>
    Private _Interval As Long

    ''' <summary> Gets or sets the interval in micro seconds. </summary>
    ''' <value> The interval in micro seconds. </value>
    Public Property Interval() As Long
        Get
            Return System.Threading.Interlocked.Read(Me._Interval)
        End Get
        Set(ByVal value As Long)
            System.Threading.Interlocked.Exchange(Me._Interval, value)
        End Set
    End Property
    ''' <summary> Amount to ignore event if late by. </summary>
    Private _IgnoreEventIfLateBy As Long

    ''' <summary> Gets or sets the amount to ignore event if late by. </summary>
    ''' <value> Amount to ignore event if late by. </value>
    Public Property IgnoreEventIfLateBy() As Long
        Get
            Return System.Threading.Interlocked.Read(Me._IgnoreEventIfLateBy)
        End Get
        Set(ByVal value As Long)
            System.Threading.Interlocked.Exchange(Me._IgnoreEventIfLateBy, If(value <= 0, Long.MaxValue, value))
        End Set
    End Property
    ''' <summary> The timer thread. </summary>
    Private _TimerThread As System.Threading.Thread

    ''' <summary> Gets or sets the enabled. </summary>
    ''' <value> The enabled. </value>
    Public Property Enabled() As Boolean
        Get
            Return (Me._TimerThread IsNot Nothing AndAlso Me._TimerThread.IsAlive)
        End Get
        Set(ByVal value As Boolean)
            If value Then
                Me.Start()
            Else
                Me.Stop()
            End If
        End Set
    End Property

    ''' <summary> Starts this object. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub Start()
        If Me.Enabled OrElse Me.Interval <= 0 Then
            Return
        End If

        Me._StopTimer = False

        Dim threadStart As System.Threading.ThreadStart = Sub() Me.NotificationTimer(Me._Interval, Me._IgnoreEventIfLateBy, Me._StopTimer)

        Me._TimerThread = New System.Threading.Thread(threadStart) With {.Priority = System.Threading.ThreadPriority.Highest}
        Me._TimerThread.Start()
    End Sub
    ''' <summary> True to stop timer. </summary>
    Private _StopTimer As Boolean
    ''' <summary> Stops the timer. </summary>
    Public Sub [Stop]()
        Me._StopTimer = True
    End Sub

    ''' <summary> Stops the timer. Waits for the current event to finish. Aborts
    ''' the timer if the event does not finish the timeout, </summary>
    ''' <param name="timeout"> The timeout. </param>
    Public Sub [Stop](ByVal timeout As TimeSpan)
        If Not Me.StopAndWait(timeout) Then Me.Abort()
    End Sub

    ''' <summary> Stops and wait. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub StopAndWait()
        Me.StopAndWait(System.Threading.Timeout.InfiniteTimeSpan)
    End Sub

    ''' <summary> Stops and wait. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function StopAndWait(ByVal timeout As TimeSpan) As Boolean
        Me._StopTimer = True
        Return (Not Me.Enabled) OrElse Me._TimerThread.ManagedThreadId = System.Threading.Thread.CurrentThread.ManagedThreadId OrElse Me._TimerThread.Join(timeout)
    End Function

    ''' <summary> Aborts the timer (aborts the time thread). </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub Abort()
        Me._StopTimer = True

        If Me.Enabled Then
            Me._TimerThread.Abort()
        End If
    End Sub

    ''' <summary> Notification timer. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="interval">            [in,out] The interval in micro seconds. </param>
    ''' <param name="ignoreEventIfLateBy"> [in,out] Amount to ignore event if late by. </param>
    ''' <param name="stopTimer">           [in,out] True to stop timer. </param>
    Private Sub NotificationTimer(ByRef interval As Long, ByRef ignoreEventIfLateBy As Long, ByRef stopTimer As Boolean)

        Dim timerCount As Integer = 0
        Dim nextNotification As Long = 0

        Dim microStopwatch As New MicroStopwatch()
        microStopwatch.Start()

        Do While Not stopTimer
            Dim callbackFunctionExecutionTime As Long = microStopwatch.ElapsedMicroseconds - nextNotification

            Dim timerIntervalInMicroSecCurrent As Long = System.Threading.Interlocked.Read(interval)
            Dim ignoreEventIfLateByCurrent As Long = System.Threading.Interlocked.Read(ignoreEventIfLateBy)

            nextNotification += timerIntervalInMicroSecCurrent
            timerCount += 1

            Dim elapsedMicroseconds As Long = microStopwatch.ElapsedMicroseconds

            Do While elapsedMicroseconds < nextNotification
                System.Threading.Thread.SpinWait(10)
                elapsedMicroseconds = microStopwatch.ElapsedMicroseconds
            Loop

            Dim timerLateBy As Long = elapsedMicroseconds - nextNotification

            If timerLateBy >= ignoreEventIfLateByCurrent Then
                Continue Do
            End If

            Dim microTimerEventArgs As New MicroTimerEventArgs(timerCount, elapsedMicroseconds, timerLateBy, callbackFunctionExecutionTime)
            RaiseEvent MicroTimerElapsed(Me, microTimerEventArgs)
        Loop

        microStopwatch.Stop()
    End Sub

    ''' <summary> Event queue for all listeners interested in MicroTimerElapsed events. </summary>
    Public Event MicroTimerElapsed As EventHandler(Of MicroTimerEventArgs)

End Class

''' <summary> Additional information for micro timer events. </summary>
''' <remarks>
''' (c) 2013 Ken Loveday. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 11/25/2019, </para><para>
''' https://www.codeproject.com/Articles/98346/Microsecond-and-Millisecond-NET-Timer. </para>
''' </remarks>
Public Class MicroTimerEventArgs
    Inherits EventArgs

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="timerCount">                    The number of timers. </param>
    ''' <param name="elapsedMicroseconds">           The elapsed microseconds. </param>
    ''' <param name="timerLateBy">                   Amount to timer late by. </param>
    ''' <param name="callbackFunctionExecutionTime"> The callback function execution time. </param>
    Public Sub New(ByVal timerCount As Integer, ByVal elapsedMicroseconds As Long, ByVal timerLateBy As Long, ByVal callbackFunctionExecutionTime As Long)
        MyBase.New()
        Me.TimerCount = timerCount
        Me.ElapsedMicroseconds = elapsedMicroseconds
        Me.TimerLateBy = timerLateBy
        Me.CallbackFunctionExecutionTime = callbackFunctionExecutionTime
    End Sub

    ''' <summary>
    ''' Gets Simple counter, number times timed event (callback function) executed.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The number of timers. </value>
    Public ReadOnly Property TimerCount() As Integer

    ''' <summary> Gets the time when timed event was called since timer started. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> The elapsed microseconds. </value>
    Public ReadOnly Property ElapsedMicroseconds() As Long

    ''' <summary>
    ''' Gets the how late the timer was compared to when it should have been called.
    ''' </summary>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <value> Amount to timer late by. </value>
    Public ReadOnly Property TimerLateBy() As Long

    ''' <summary>
    ''' Gets the time it took to execute previous call to callback function (OnTimedEvent).
    ''' </summary>
    ''' <value> The callback function execution time. </value>
    Public ReadOnly Property CallbackFunctionExecutionTime() As Long

End Class

''' <summary> A micro stopwatch. </summary>
''' <remarks>
''' (c) 2013 Ken Loveday. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 11/25/2019, </para><para>
''' https://www.codeproject.com/Articles/98346/Microsecond-and-Millisecond-NET-Timer. </para>
''' </remarks>
Public Class MicroStopwatch
    Inherits System.Diagnostics.Stopwatch
    ''' <summary> The micro security per tick. </summary>
    Private ReadOnly _MicroSecPerTick As Double = 1000000.0R / System.Diagnostics.Stopwatch.Frequency

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Diagnostics.Stopwatch" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    Public Sub New()
        If Not System.Diagnostics.Stopwatch.IsHighResolution Then
            Throw New InvalidOperationException("On this system the high-resolution performance counter is not available")
        End If
    End Sub

    ''' <summary> Gets the elapsed microseconds. </summary>
    ''' <value> The elapsed microseconds. </value>
    Public ReadOnly Property ElapsedMicroseconds() As Long
        Get
            Return CLng(Fix(Me.ElapsedTicks * Me._MicroSecPerTick))
        End Get
    End Property
End Class

