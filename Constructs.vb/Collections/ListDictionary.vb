''' <summary>
''' An dictionary of items <typeparamref name="TKey"/> ordered by the order they are entered
''' <typeparamref name="TValue"/>.
''' </summary>
''' <remarks>
''' David, 2020-05-22,
''' https://www.codeproject.com/Tips/5268618/ListDictionary-An-Improved-OrderedDictionary <para>
''' Enumerating the data Dictionary as KeyValuePair did Not Return data In any particular order,
''' and using System.Collection.Specialized's OrderedDictionary was cumbersome. So, I created my
''' own orderly dictionary called ListDictionary which is discussed in this tip. </para><para>
''' (c) 2020 Michael Sydney Balloni. All rights reserved. </para><para>
''' Licensed under The MIT License.</para><para>
''' </para>
''' </remarks>
Public Class ListDictionary(Of TKey As IComparable(Of TKey), TValue)

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub New()
        MyBase.New
        Me.Entries = New List(Of KeyValuePair(Of TKey, TValue))()
        Me.EntriesDictionary = New Dictionary(Of TKey, TValue)()
    End Sub

    ''' <summary> Gets the list of entries. </summary>
    ''' <value> The entries. </value>
    Public ReadOnly Property Entries As IList(Of KeyValuePair(Of TKey, TValue))

    ''' <summary> Gets the keys. </summary>
    ''' <value> The keys. </value>
    Public ReadOnly Property Keys As IEnumerable(Of TKey)
        Get
            Return Me.Entries.[Select](Function(kvp) kvp.Key)
        End Get
    End Property

    ''' <summary> Gets the values. </summary>
    ''' <value> The values. </value>
    Public ReadOnly Property Values As IEnumerable(Of TValue)
        Get
            Return Me.Entries.[Select](Function(kvp) kvp.Value)
        End Get
    End Property

    ''' <summary> Gets or sets a dictionary of entries. </summary>
    ''' <value> A dictionary of entries. </value>
    Private ReadOnly Property EntriesDictionary As IDictionary(Of TKey, TValue)

    ''' <summary> Gets or sets the item. </summary>
    ''' <value> The item. </value>
    Default Public Property Item(ByVal key As TKey) As TValue
        Get
            Return Me.EntriesDictionary(key)
        End Get
        Set(ByVal value As TValue)
            Me.Setter(key, value)
        End Set
    End Property

    ''' <summary> Gets any. </summary>
    ''' <value> any. </value>
    Public ReadOnly Property Any As Boolean
        Get
            Return Me.Entries.Any
        End Get
    End Property

    ''' <summary> Gets the number of. </summary>
    ''' <value> The count. </value>
    Public ReadOnly Property Count As Integer
        Get
            Return Me.Entries.Count
        End Get
    End Property

    ''' <summary> Query if 'key' contains key. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="key"> The key. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    Public Function ContainsKey(ByVal key As TKey) As Boolean
        Return Me.EntriesDictionary.ContainsKey(key)
    End Function

    ''' <summary> Gets the first key. </summary>
    ''' <value> The first key. </value>
    Public ReadOnly Property FirstKey As TKey
        Get
            Return Me.Entries.First.Key
        End Get
    End Property

    ''' <summary> Gets the Last key. </summary>
    ''' <value> The Last key. </value>
    Public ReadOnly Property LastKey As TKey
        Get
            Return Me.Entries.Last.Key
        End Get
    End Property

    ''' <summary> Sets the key value pair and sorts the entities list. </summary>
    ''' <remarks> David, 2020-05-22. </remarks>
    ''' <param name="key"> The key. </param>
    ''' <param name="val"> The value. </param>
    Public Sub Setter(ByVal key As TKey, ByVal val As TValue)
        Dim added As Boolean = False
        If Me.EntriesDictionary.ContainsKey(key) Then
            For i As Integer = 0 To Me.Entries.Count - 1
                Dim curKey As TKey = Me.Entries(i).Key
                If curKey.CompareTo(key) = 0 Then
                    Me.Entries(i) = New KeyValuePair(Of TKey, TValue)(key, val)
                    added = True
                    Exit For
                End If
            Next
        End If
        If Not added Then Me.Entries.Add(New KeyValuePair(Of TKey, TValue)(key, val))
        Me.EntriesDictionary(key) = val
    End Sub

End Class

