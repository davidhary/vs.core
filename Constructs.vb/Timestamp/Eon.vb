''' <summary> Eon - a time span time keeper. </summary>
''' <remarks>
''' Time, which is kept in Date Time Offset, is saved to bases as Date Time Offset where
''' supported. Otherwise, time is stored as Coordinated Universal Time (UTC). <para>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para><para>
''' David, 9/11/2019, </para><para>
''' David, 11/19/2014, 2.1.5436. </para>
''' </remarks>
Public Class Eon

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="minimumTime"> The <see cref="StartTime"/> if started or the
    '''                            <see cref="MinimumTime"/> </param>
    Public Sub New(ByVal minimumTime As DateTimeOffset)
        MyBase.New()
        Me._MinimumTime = minimumTime
        Me.ResetKnownStateThis()
    End Sub

    ''' <summary> Validated eon. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="eon"> The eon. </param>
    ''' <returns> An Eon. </returns>
    Public Shared Function ValidatedEon(ByVal eon As Eon) As Eon
        If eon Is Nothing Then Throw New ArgumentNullException(NameOf(eon))
        Return eon
    End Function

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As Eon)
        Me.New(Eon.ValidatedEon(value).MinimumTime)
        If value IsNot Nothing Then
            Me.InitializeKnownStateThis(value.StartTime, value.EndTime)
        End If
    End Sub

#End Region

#Region " RESET AND CLEAR "

    ''' <summary>
    ''' Clears to known (clear) state; Clears select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Private Sub ClearKnownStateThis()
        Me.StartTime = Me.MinimumTime
        Me.EndTime = New DateTimeOffset?
    End Sub

    ''' <summary>
    ''' Clears to known (clear) state; Clears select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub ClearKnownState()
        Me.ClearKnownStateThis()
    End Sub

    ''' <summary>
    ''' Initializes to known (Initialize) state; Initializes select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="startTime"> The start time. </param>
    ''' <param name="endTime">   The end time. </param>
    Private Sub InitializeKnownStateThis(ByVal startTime As DateTimeOffset, ByVal endTime As DateTimeOffset?)
        Me.StartTime = startTime
        Me.EndTime = endTime
    End Sub

    ''' <summary>
    ''' Initializes to known (Initialize) state; Initializes select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="startTime"> The start time. </param>
    ''' <param name="endTime">   The end time. </param>
    Public Sub InitializeKnownState(ByVal startTime As DateTimeOffset, ByVal endTime As DateTimeOffset?)
        Me.InitializeKnownStateThis(startTime, endTime)
    End Sub

    ''' <summary>
    ''' Initializes to known (Initialize) state; Initializes select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="startTime"> The start time. </param>
    Public Sub InitializeKnownState(ByVal startTime As DateTimeOffset)
        Me.InitializeKnownState(startTime, New DateTimeOffset?)
    End Sub

    ''' <summary>
    ''' Initializes to known (Initialize) state; Initializes select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub InitializeKnownState()
        Me.InitializeKnownState(DateTimeOffset.Now)
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Private Sub ResetKnownStateThis()
        Me.ClearKnownStateThis()
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub ResetKnownState()
        Me.ResetKnownStateThis()
    End Sub

#End Region

#Region " TIME PROPERTIES "
    ''' <summary> The start time. </summary>
    Private _StartTime As DateTimeOffset

    ''' <summary> Gets or sets the start time. </summary>
    ''' <remarks>
    ''' The eon must have a start time. Start time can be set to <see cref="MinimumTime"/> to tag the
    ''' eon as not started. Start time can be <see cref="ResetStartTime()">reset</see>,
    ''' <see cref="PostponeStartTime(DateTimeOffset)">posponed</see> or
    ''' <see cref="AdvanceStartTime(DateTimeOffset)">advanced</see>.
    ''' </remarks>
    ''' <value> The start time. </value>
    Public Property StartTime As DateTimeOffset
        Get
            Return Me._StartTime
        End Get
        Set(value As DateTimeOffset)
            Me._StartTime = value
        End Set
    End Property
    ''' <summary> The end time. </summary>
    Private _EndTime As DateTimeOffset?

    ''' <summary> Gets or sets the End time. </summary>
    ''' <value> The End time. </value>
    Public Property EndTime As DateTimeOffset?
        Get
            Return Me._EndTime
        End Get
        Set(value As DateTimeOffset?)
            Me._EndTime = value
        End Set
    End Property

#End Region

#Region " TIME FUNCTIONS "

    ''' <summary> Determines if the <see cref="Eon"/> started. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> <c>true</c> if it started; otherwise <c>false</c> </returns>
    Public Function HasStarted() As Boolean
        Return Me.StartTime > Me.MinimumTime
    End Function

    ''' <summary> Query if time span has ended. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> <c>true</c> if time span has ended; otherwise <c>false</c> </returns>
    Public Function HasEnded() As Boolean
        Return Me.EndTime.HasValue
    End Function

    ''' <summary> Query start time is later than end time or ended but not started. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> <c>true</c> if conflict; otherwise <c>false</c> </returns>
    Public Function HasConflict() As Boolean
        Return (Me.HasEnded AndAlso Not Me.HasStarted) OrElse
                Me.HasStarted AndAlso Me.HasEnded AndAlso Me.StartTime > Me.EndTime.Value
    End Function

    ''' <summary> Query if this Eon is active. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> <c>true</c> if active; otherwise <c>false</c>. </returns>
    Public Function IsActive() As Boolean
        Return Me.HasStarted AndAlso Not Me.HasEnded
    End Function

    ''' <summary>
    ''' Gets the active start time. Defaults to <see cref="DateTimeOffset.MinValue"/>. Is set to SQL
    ''' minimum time.
    ''' </summary>
    ''' <value> The <see cref="StartTime"/> if started or the <see cref="MinimumTime"/> </value>
    Public Property MinimumTime As DateTimeOffset

    ''' <summary> Gets the eon start time. </summary>
    ''' <value> The eon start time if started or the <see cref="MinimumTime"/> </value>
    Public ReadOnly Property ActiveStartTime As DateTimeOffset
        Get
            Return If(Me.HasStarted, Me.StartTime, Me.MinimumTime)
        End Get
    End Property

    ''' <summary> Gets the active end time. </summary>
    ''' <value> The <see cref="EndTime"/> if started or the <see cref="MinimumTime"/> </value>
    Public ReadOnly Property ActiveEndTime As DateTimeOffset
        Get
            If Me.HasEnded Then
                Return Me.EndTime.Value
            ElseIf Me.HasStarted Then
                Return DateTimeOffset.Now
            Else
                Return Me.MinimumTime
            End If
        End Get
    End Property

    ''' <summary> Gets the last elapsed time. </summary>
    ''' <value> The last elapsed time. </value>
    Protected ReadOnly Property LastElapsedTime As TimeSpan

    ''' <summary> Gets the elapsed time span. </summary>
    ''' <value> The elapsed time span. </value>
    Public ReadOnly Property ElapsedTimespan As TimeSpan
        Get
            Me._LastElapsedTime = Me.ActiveEndTime.Subtract(Me.ActiveStartTime)
            Return Me.LastElapsedTime
        End Get
    End Property

#End Region

#Region " TIME ACTIONS "

    ''' <summary> Starts the <see cref="Eon"/>. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub [Start]()
        Me.InitializeKnownState()
    End Sub

    ''' <summary> Starts the <see cref="Eon"/>. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="dateTimeOffset"> The Date/Time. </param>
    Public Overridable Sub Start(ByVal dateTimeOffset As DateTimeOffset)
        Me.InitializeKnownState(dateTimeOffset)
    End Sub

    ''' <summary> Ends the <see cref="Eon"/>. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Overridable Sub Finish()
        Me.Finish(DateTimeOffset.Now)
    End Sub

    ''' <summary> Ends the <see cref="Eon"/>. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Overridable Sub Finish(ByVal value As DateTimeOffset)
        Me.EndTime = value
    End Sub

    ''' <summary> Opens the Eon after a <see cref="Finish()">stop</see>. </summary>
    Public Overridable Sub [Resume]()
        Me.EndTime = New DateTimeOffset?
    End Sub

    ''' <summary> Resets the Eon start time to current time. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Overridable Sub ResetStartTime()
        Me.ResetStartTime(DateTimeOffset.Now)
    End Sub

    ''' <summary> Resets the Eon start time. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The new start time. </param>
    Public Overridable Sub ResetStartTime(ByVal value As DateTimeOffset)
        Me.StartTime = value
    End Sub

#End Region

#Region " TIME SHIFTS "

    ''' <summary> Advance start time to an earlier time. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub AdvanceStartTime(ByVal value As DateTimeOffset)
        If Me.HasStarted Then
            If Me.StartTime > value Then
                Me.StartTime = value
            End If
        Else
            Me.Start(value)
        End If
    End Sub

    ''' <summary> Postpone start time. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub PostponeStartTime(ByVal value As DateTimeOffset)
        If Me.HasStarted Then
            If Me.StartTime < value Then
                Me.StartTime = value
            End If
        Else
            Me.Start(value)
        End If
    End Sub

    ''' <summary> Sets start time to a later start time. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="startTime"> The start time. </param>
    Public Sub StartLater(ByVal startTime As DateTimeOffset)
        If Not Me.HasStarted OrElse Me.StartTime < startTime Then Me.Start(startTime)
    End Sub

    ''' <summary> Sets start time to an earlier start time. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="startTime"> The start time. </param>
    Public Sub StartEarlier(ByVal startTime As DateTimeOffset)
        If Not Me.HasStarted OrElse Me.StartTime > startTime Then Me.Start(startTime)
    End Sub

    ''' <summary> Postpone end time. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub PostponeEndTime(ByVal value As DateTimeOffset?)
        If value.HasValue Then
            If Me.HasStarted Then
                If Me.HasEnded Then
                    If Me.EndTime.Value < value.Value Then
                        Me.Finish(value.Value)
                    End If
                Else
                    Me.EndTime = value
                End If
            Else
                Me.Finish(value.Value)
            End If
        End If
    End Sub

#End Region

#Region " FORMATTING "

    ''' <summary> Gets the default date time format. </summary>
    ''' <value> The default date time format. </value>
    Public Shared Property DefaultDateTimeFormat As String = "yyyy/MM/dd HH:mm:ss K"

    ''' <summary> Gets the default elapsed time format. </summary>
    ''' <value> The default elapsed time format. </value>
    Public Shared Property DefaultElapsedTimeFormat As String = "d\.hh\:mm\:ss\.fff"

#End Region

End Class

''' <summary> A list of eons sorted by the Eon start time. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/11/2017 </para>
''' </remarks>
Public Class EonSortedList
    Inherits SortedList(Of DateTimeOffset, Eon)

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="minimumTime"> The minimum time of the. </param>
    Public Sub New(ByVal minimumTime As DateTimeOffset)
        MyBase.New()
        Me._MinimumTimeEon = New Eon(minimumTime) With {.StartTime = minimumTime}
    End Sub

    ''' <summary> Gets the minimum time eon. </summary>
    ''' <value> The minimum time eon. </value>
    Public ReadOnly Property MinimumTimeEon As Eon

    ''' <summary> Gets the default start time. </summary>
    ''' <value> The default start time. </value>
    Public Shared ReadOnly Property DefaultStartTime As DateTimeOffset

    ''' <summary> Adds item. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="item"> The item to add. </param>
    Public Overloads Sub Add(ByVal item As Eon)
        If item IsNot Nothing Then
            MyBase.Add(item.StartTime, item)
        End If
    End Sub

    ''' <summary> Adds item. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="startTime"> The start time. </param>
    ''' <param name="endTime">   The end time. </param>
    Public Overloads Sub Add(ByVal startTime As DateTimeOffset, endTime As DateTimeOffset?)
        MyBase.Add(startTime, New Eon(Me.MinimumTimeEon.MinimumTime) With {.StartTime = (startTime), .EndTime = (endTime)})
    End Sub

    ''' <summary> Updates this object. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="startTime"> The start time. </param>
    ''' <param name="endTime">   The end time. </param>
    Public Sub Update(ByVal startTime As DateTimeOffset, endTime As DateTimeOffset?)
        If Me.ContainsKey(startTime) Then
            Me(startTime).StartTime = startTime
            Me(startTime).EndTime = endTime
        Else
            Me.Add(startTime, endTime)
        End If
    End Sub

    ''' <summary> Gets the first. </summary>
    ''' <value> The first. </value>
    Public ReadOnly Property First As Eon
        Get
            Return If(Me.Values.Any, Me.Values.First, Me.MinimumTimeEon)
        End Get
    End Property

    ''' <summary> Gets the last. </summary>
    ''' <value> The last. </value>
    Public ReadOnly Property Last As Eon
        Get
            Return If(Me.Values.Any, Me.Values.Last, Me.MinimumTimeEon)
        End Get
    End Property

    ''' <summary> Gets the start time. </summary>
    ''' <value> The start time. </value>
    Public ReadOnly Property StartTime As DateTimeOffset
        Get
            Return Me.First.ActiveStartTime
        End Get
    End Property

    ''' <summary> Gets the start time caption. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> A String. </returns>
    Public Function StartTimeCaption() As String
        Return Me.StartTime.ToString(Me.DateTimeFormat)
    End Function

    ''' <summary> Gets the end time. </summary>
    ''' <value> The end time. </value>
    Public ReadOnly Property EndTime As DateTimeOffset
        Get
            Return Me.Last.ActiveEndTime
        End Get
    End Property

    ''' <summary> Elapsed time span. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> A TimeSpan. </returns>
    Public Function ElapsedTimeSpan() As TimeSpan
        Dim value As TimeSpan = TimeSpan.Zero
        For Each eon As Eon In Me.Values
            value.Add(eon.ElapsedTimespan)
        Next
    End Function

    ''' <summary> Elapsed time caption. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> A String. </returns>
    Public Function ElapsedTimeCaption() As String
        Return Me.ElapsedTimeSpan.ToString(Me.ElapsedTimeFormat)
    End Function

    ''' <summary> Gets or sets the date time format. </summary>
    ''' <value> The date time format. </value>
    Public Property DateTimeFormat As String = "yyyy/MM/dd HH:mm:ss K"

    ''' <summary> Gets or sets the elapsed time format. </summary>
    ''' <value> The elapsed time format. </value>
    Public Property ElapsedTimeFormat As String = "d\.hh\:mm\:ss\.fff"

End Class

''' <summary> Dictionary of ordered list of Eons. </summary>
''' <remarks>
''' (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 8/31/2017 </para>
''' </remarks>
<CodeAnalysis.SuppressMessage("Usage", "CA2237:Mark ISerializable types with serializable", Justification:="<Pending>")>
Public Class OrderedEonsDictionary
    Inherits Dictionary(Of Integer, EonSortedList)

    ''' <summary> Adds a dictionary to 'minimumTime'. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="key">         The key. </param>
    ''' <param name="minimumTime"> The minimum time of the. </param>
    Public Sub AddDictionary(ByVal key As Integer, ByVal minimumTime As DateTimeOffset)
        If Not Me.ContainsKey(key) Then
            Me._MinimumTimeEon = New Eon(minimumTime)
            Me.Add(key, New EonSortedList(minimumTime))
        End If
    End Sub

    ''' <summary> Returns true if the reference dictionary has any elements. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="key"> The key. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public Function Any(ByVal key As Integer) As Boolean
        Return Me.ContainsKey(key) AndAlso Me(key).Any
    End Function

    ''' <summary> Gets or sets the minimum time eon. </summary>
    ''' <value> The minimum time eon. </value>
    Public ReadOnly Property MinimumTimeEon As Eon

    ''' <summary> Gets the start time. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="key"> The key. </param>
    ''' <returns> A DateTimeOffset. </returns>
    Public Function StartTime(ByVal key As Integer) As DateTimeOffset
        Return If(Me.Any(key), Me(key).StartTime, Me.MinimumTimeEon.StartTime)
    End Function

    ''' <summary> Gets the start time caption. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="key"> The key. </param>
    ''' <returns> A String. </returns>
    Public Function StartTimeCaption(ByVal key As Integer) As String
        Return If(Me.Any(key), Me(key).StartTimeCaption(), Me.MinimumTimeEon.StartTime.ToString(Me.DateTimeFormat))
    End Function

    ''' <summary> Gets the end time. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="key"> The key. </param>
    ''' <returns> A DateTimeOffset. </returns>
    Public Function EndTime(ByVal key As Integer) As DateTimeOffset
        Return If(Me.Any(key), Me(key).Last.ActiveEndTime, Me.MinimumTimeEon.ActiveEndTime)
    End Function

    ''' <summary> Elapsed time span. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="key"> The key. </param>
    ''' <returns> A TimeSpan. </returns>
    Public Function ElapsedTimeSpan(ByVal key As Integer) As TimeSpan
        Dim value As TimeSpan = TimeSpan.Zero
        If Me.Any(key) Then
            value = Me(key).ElapsedTimeSpan
        End If
        Return value
    End Function

    ''' <summary> Elapsed time caption. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="key"> The key. </param>
    ''' <returns> A String. </returns>
    Public Function ElapsedTimeCaption(ByVal key As Integer) As String
        Return Me(key).ElapsedTimeCaption
    End Function

    ''' <summary> Gets or sets the date time format. </summary>
    ''' <value> The date time format. </value>
    Public Property DateTimeFormat As String = "yyyy/MM/dd HH:mm:ss K"

End Class

