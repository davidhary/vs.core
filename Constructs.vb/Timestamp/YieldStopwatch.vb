Imports System.Timers

''' <summary> A yield counter using a stopwatch and optional timer. </summary>
''' <remarks>
''' Time, which is kept in Date Time Offset, is saved to bases as Date Time Offset where
''' supported. Otherwise, time is stored as Coordinated Universal Time (UTC). <para>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para><para>
''' David, 10/8/2016 </para>
''' </remarks>
<DebuggerDisplay("Total = {TotalCount}")>
Public Class YieldStopwatch
    Inherits YieldCounter

#Region " CONSTRUCTOR "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub New()
        MyBase.New()
        Me.ResetKnownStateThis()
        Me.ClearKnownStateThis()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As YieldStopwatch)
        MyBase.New(value)
        If value IsNot Nothing Then
            Me.InitializeKnownStateThis(value.StartTime)
            Me.Interval = value.Interval
            Me._Stopwatch = New OffsetStopwatch(value.Stopwatch)
            If value.Stopwatch.IsRunning Then
            Else
                Me.Stopwatch.Reset()
            End If
        End If
    End Sub

    ''' <summary> Creates a new Yield Stopwatch. </summary>
    ''' <remarks> Helps implement CA2000. </remarks>
    ''' <returns> A  <see cref="YieldStopwatch"/>. </returns>
    Public Overloads Shared Function Create() As YieldStopwatch
        Dim result As YieldStopwatch

        Try
            result = New YieldStopwatch
        Catch
            Throw
        End Try
        Return result
    End Function

#End Region

#Region " PUBLISH "

    ''' <summary> Publishes this object. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Overrides Sub Publish()
        MyBase.Publish()
        Me.NotifyPropertyChanged(NameOf(YieldStopwatch.StartTime))
        Me.NotifyPropertyChanged(NameOf(YieldStopwatch.EndTime))
    End Sub

#End Region

#Region " RESET AND CLEAR "

    ''' <summary>
    ''' Clears to known (clear) state; Clears select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Private Sub ClearKnownStateThis()
        Me._StartTime = DateTimeOffset.MinValue
        Me._Stopwatch = New OffsetStopwatch(TimeSpan.Zero)
    End Sub

    ''' <summary>
    ''' Clears to known (clear) state; Clears select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Overrides Sub ClearKnownState()
        MyBase.ClearKnownState()
        ' if clearing and value cleared, we need to publish to make sure displays get updated.
        Me.ClearKnownStateThis()
        Me.Publish()
    End Sub

    ''' <summary> Resets to known (default/instantiated) state. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Private Sub ResetKnownStateThis()
        Me.ClearKnownStateThis()
        Me.ElapsedTimer = New Timers.Timer()
        Me.ElapsedTimer.Stop()
    End Sub

    ''' <summary> Resets the know state. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Overrides Sub ResetKnownState()
        MyBase.ResetKnownState()
        Me.ResetKnownStateThis()
        Me.Publish()
    End Sub

    ''' <summary>
    ''' Initializes to known (Initialize) state; Initializes select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="startTime"> The start time. </param>
    Private Sub InitializeKnownStateThis(ByVal startTime As DateTimeOffset)
        Me.ClearKnownStateThis()
        Me._StartTime = startTime
    End Sub

    ''' <summary>
    ''' Initializes to known (Initialize) state; Initializes select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <param name="startTime"> The start time. </param>
    Public Sub InitializeKnownState(ByVal startTime As DateTimeOffset)
        Me.InitializeKnownStateThis(startTime)
    End Sub

    ''' <summary>
    ''' Initializes to known (Initialize) state; Initializes select values to their initial state.
    ''' </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub InitializeKnownState()
        Me.InitializeKnownState(DateTimeOffset.Now)
    End Sub

#End Region

#Region " TIME ELEMENTS "

    ''' <summary> Gets the stopwatch. </summary>
    ''' <value> The stopwatch. </value>
    Public Property Stopwatch As OffsetStopwatch

    ''' <summary> Gets the is started. </summary>
    ''' <value> The is started. </value>
    Public ReadOnly Property IsStarted As Boolean
        Get
            Return Me.StartTime > DateTimeOffset.MinValue
        End Get
    End Property

    ''' <summary> Gets the start time. </summary>
    ''' <value> The start time. </value>
    Public ReadOnly Property StartTime As DateTimeOffset

    ''' <summary> Gets the end time. </summary>
    ''' <value> The end time. </value>
    Public ReadOnly Property EndTime As DateTimeOffset
        Get
            Return Me.StartTime.Add(Me.Stopwatch.Elapsed)
        End Get
    End Property

#End Region

#Region " TIME ACTIONS "

    ''' <summary> Resets and starts. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub Start()
        Me.InitializeKnownState()
        Me.Stopwatch.Start()
        If Me.Interval > TimeSpan.Zero Then Me.ElapsedTimer.Start()
        Me.NotifyPropertyChanged(NameOf(YieldStopwatch.StartTime))
    End Sub

    ''' <summary> Finishes measuring elapsed time. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    Public Sub Finish()
        Me.ElapsedTimer.Stop()
        Me.Stopwatch.Stop()
        Me.NotifyPropertyChanged(NameOf(YieldStopwatch.EndTime))
    End Sub

    ''' <summary> Resumes counting after a <see cref="Finish()">stop</see>. </summary>
    Public Sub [Resume]()
        If Me.Interval > TimeSpan.Zero Then Me.ElapsedTimer.Start()
        If Not Me.Stopwatch.IsRunning Then Me.Stopwatch.Start()
    End Sub

#End Region

#Region " RATE "

    ''' <summary> Hourly rate. </summary>
    ''' <remarks> David, 2020-09-22. </remarks>
    ''' <returns> A Double. </returns>
    Public Overloads Function HourlyRate() As Double
        Return Me.HourlyRate(Me.Stopwatch.Elapsed)
    End Function

#End Region

#Region " TIMER "
        Private WithEvents ElapsedTimer As Timers.Timer

    ''' <summary> Gets or sets the interval. </summary>
    ''' <value> The interval. </value>
    Public Property Interval As TimeSpan
        Get
            Return TimeSpan.FromTicks(CLng(TimeSpan.TicksPerMillisecond * Me.ElapsedTimer.Interval))
        End Get
        Set(value As TimeSpan)
            Me.ElapsedTimer.Interval = value.TotalMilliseconds
        End Set
    End Property

    ''' <summary> Timer elapsed. </summary>
    ''' <remarks>
    ''' The synchronization context is captured as part of the property change and other event
    ''' handlers and is no longer needed here.
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Elapsed event information. </param>
    Private Sub Timer_Elapsed(sender As Object, e As ElapsedEventArgs) Handles ElapsedTimer.Elapsed
        Me.NotifyPropertyChanged(NameOf(YieldStopwatch.ElapsedTimeCaption))
    End Sub

#End Region

#Region " FORMAT "

    ''' <summary> Gets the ElapsedTime format. </summary>
    ''' <value> The ElapsedTime format. </value>
    Public Property ElapsedTimeFormat As String = "d\.hh\:mm\:ss\.fff"

    ''' <summary> Gets the start time caption. </summary>
    ''' <value> The start time caption. </value>
    Public ReadOnly Property ElapsedTimeCaption As String
        Get
            Return Me.Stopwatch.Elapsed.ToString(Me.ElapsedTimeFormat)
        End Get
    End Property

#End Region

End Class

