# Changelog
All notable changes to these libraries will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [6.0.8070] - 2022-02-04
* Targeting Visual Studio 2022, C# 10 and .NET 6.0.
* Update NuGet packages.
* Remove unused references. 
* Update build version.
* Display version file when updating build version.
* Remove connection UI Projects.

## [6.0.7780] - 2021-04-19
Add net core statistics library.

## [6.0.7629] - 2020-11-20
Appliance Base: breaking. Returns Tuple on elapsed-until methods.

## [6.0.7616] - 2020-11-06
Adds Spin and Do Events wait functions.

## [6.0.7615] - 2020-11-05
Deprecates using thread sleep for time delays as being unreliable with resolution no better than the system clock.

## [6.0.7563] - 2020-09-15
Converted to C#.

## [5.0.7560] - 2020-09-12
Replaces Agnostic with Services.

## [4.0.6961] - 2019-01-22
Created from Core 3.

## [4.0.6956] - 2019-01-17
### Linq Statistics
Removes reference to the core library.

## [3.3.6655] - 2018-03-13
### Linq Statistics
Split off from engineering library.

## [3.4.6956] - 2019-01-17
Adds splash, popup and metro forms. Removes property invokes.

## [3.4.6944] - 2019-01-05
Adds binding management to base controls and forms.

## [3.4.6898] - 2018-11-20
Fixes compact string extensions (Measure Text no longer alters the string argument).

## [3.4.6667] - 2018-04-03
2018 release.

## [3.3.6439] - 2017-08-18
Revamps message listeners and talkers. Defaults listeners to Verbose level and get the talkers to filter trace level thus allowing to control how messages are published as well as listened to.

## [3.3.6438] - 2017-08-17
Adds functionality to the trace message talkers to ensure trace levels are correctly updated.

## [3.2.6436] - 2017-08-15
Changes trace message tool strip drop down to a drop down list.

## [3.2.6419] - 2017-06-24
Adds functionality to Enum extensions.

## [3.1.6384] - 2017-06-24
Defaults to UTC time.

## [3.1.6370] - 2017-06-10
Adds class for running tasks with dependencies.

## [3.1.6354] - 2017-05-25
Uses thread save queue for messages box.

## [3.1.6340] - 2017-05-11
Remove trim start from the default log file writer. Fixes the default Full range.

## [3.1.6332] - 2017-05-03
Uses thread safe queue for logging.

## [3.1.6298] - 2017-03-30
Adds exception extension. Adds thread safe token.

## [3.0.7130] - 2019-07-10
### Shell
Add code for finding a node. Fixes expanding a node.

## [3.0.7127] - 2019-07-07
### Shell
Forked from Code Project (see link below).

## [3.0.6093] - 2016-09-04
Adds post and send event handler extensions.

## [3.0.6089] - 2016-09-02
Adds base and coding exceptions.

## [3.0.5934] - 2016-03-31
Adds configuration editor form.

## [3.0.5866] - 2016-01-23
Updates to .NET 4.6.1

## [2.1.5828] - 2015-12-16
Moves trace and log from the Diagnosis class. 

## [2.1.5822] - 2015-12-10
Created from Core Publishers. 

## [2.1.5163] - 2014-02-19
Uses local sync and asynchronous safe event handlers. 

## [2.0.5126] - 2014-01-13
Tagged as 2014.

## [2.0.5024] - 2013-09-04
Simplified Publisher interface and base class.

## [2.0.4955] - 2013-09-04
Created based on the legacy core structures.

## [1.2.4654] - 2012-09-28
Adds Property Changed Publisher base. Validates name before invoking property changed notification. Validates values of action results. Uses base values when adding action results.

(C) 2012 Integrated Scientific Resources, Inc. All rights reserved.

```
## Release template - [version] - [date]
## Unreleased
### Added
### Changed
### Deprecated
### Removed
### Fixed
*<project name>*
```
[6.0.8070]: https://bitbucket.org/davidhary/vs.core/src/main/
