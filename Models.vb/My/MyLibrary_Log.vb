Imports isr.Core.Models.ExceptionExtensions
Namespace My

    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Logs unpublished exception. </summary>
        ''' <remarks> David, 2020-09-21. </remarks>
        ''' <param name="activity">  The activity. </param>
        ''' <param name="exception"> The exception. </param>
        Public Shared Sub LogUnpublishedException(ByVal activity As String, ByVal exception As Exception)
            MyLibrary.LogUnpublishedMessage(New TraceMessage(TraceEventType.Error, MyLibrary.TraceEventId, $"Exception {activity};. {exception.ToFullBlownString}"))
        End Sub

        ''' <summary> Applies the Logger. </summary>
        ''' <remarks> David, 2020-09-21. </remarks>
        ''' <param name="value"> The value. </param>
        Public Shared Sub Apply(ByVal value As Logger)
            MyLibrary._Logger = value
        End Sub

        ''' <summary> Applies the trace level described by value. </summary>
        ''' <remarks> David, 2020-09-21. </remarks>
        ''' <param name="value"> The value. </param>
        Public Shared Sub ApplyTraceLogLevel(ByVal value As TraceEventType)
            MyLibrary.TraceLevel = value
            MyLibrary.Logger.ApplyTraceLevel(value)
        End Sub

        ''' <summary> Applies the trace level described by value. </summary>
        ''' <remarks> David, 2020-09-21. </remarks>
        Public Shared Sub ApplyTraceLogLevel()
            MyLibrary.ApplyTraceLogLevel(MyLibrary.TraceLevel)
        End Sub

    End Class

End Namespace
