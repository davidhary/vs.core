Imports System.Windows.Threading
Namespace My

    Partial Public NotInheritable Class MyLibrary

        ''' <summary> Lets Windows process all the messages currently in the message queue. </summary>
        ''' <remarks> David, 2020-09-21. </remarks>
        Public Shared Sub DoEvents()
            isr.Core.DispatcherExtensions.Methods.DoEvents(Dispatcher.CurrentDispatcher)
        End Sub

        ''' <summary>
        ''' Delays operations by the given delay time selecting the delay clock which resolution exceeds
        ''' 0.2 times the delay time. T.
        ''' </summary>
        ''' <remarks> David, 2020-09-21. </remarks>
        ''' <param name="delayMilliseconds"> The delay in milliseconds. </param>
        Public Shared Sub Delay(ByVal delayMilliseconds As Double)
            Delay(isr.Core.TimeSpanExtensions.Methods.FromMilliseconds(delayMilliseconds))
        End Sub

        ''' <summary>
        ''' Delays operations by the given delay time selecting the delay clock which resolution exceeds
        ''' 0.2 times the delay time. sions.DoEvents(Dispatcher)"/> to release messages currently in the
        ''' message queue.
        ''' </summary>
        ''' <remarks> David, 2020-09-21. </remarks>
        ''' <param name="delayTime"> The delay time. </param>
        Public Shared Sub Delay(ByVal delayTime As TimeSpan)
            isr.Core.TimeSpanExtensions.Methods.StartDelayTask(delayTime).Wait()
        End Sub

        ''' <summary>
        ''' Delays operations by the given delay time selecting the delay clock which resolution exceeds
        ''' <paramref name="resolution"/> times the delay time.
        ''' </summary>
        ''' <remarks> David, 2020-09-21. </remarks>
        ''' <param name="delayTime">  The delay time. </param>
        ''' <param name="resolution"> The resolution. </param>
        Public Shared Sub Delay(ByVal delayTime As TimeSpan, ByVal resolution As Double)
            isr.Core.TimeSpanExtensions.Methods.StartDelayTask(delayTime, resolution).Wait()
        End Sub

        ''' <summary>
        ''' Executes the specified delegate on the <see cref="DispatcherPriority.Render"/> priority.
        ''' </summary>
        ''' <remarks> David, 2020-09-21. </remarks>
        ''' <param name="act"> The act. </param>
        Public Shared Sub Render(ByVal act As Action)
            isr.Core.DispatcherExtensions.Methods.Render(Dispatcher.CurrentDispatcher, act)
        End Sub

    End Class

End Namespace

