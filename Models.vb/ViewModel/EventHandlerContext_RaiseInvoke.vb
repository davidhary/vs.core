﻿Imports System.ComponentModel
Imports System.Threading

Partial Public Class EventHandlerContext(Of T As Class)

    ''' <summary>
    ''' Asynchronously raises (Posts) the <see cref="EventHandler(Of TEventArgs)"/>. It does all the checking to see
    ''' if the SynchronizationContext is nothing or not, and invokes the delegate accordingly.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      event information. </param>
    Public Sub Post(ByVal sender As Object, ByVal e As T)
        Dim evt As EventHandler(Of T) = Me.Handler
        If evt IsNot Nothing Then
            If Me.Context Is Nothing Then
                ' if not UI or active context, invoke the event
                evt.Invoke(sender, e)
            Else
                Me.Context.Post(Sub() evt.Invoke(sender, e), Nothing)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Synchronously raises (sends) the <see cref="EventHandler(Of TEventArgs)"/>. It does all the checking to see
    ''' if the SynchronizationContext is nothing or not, and invokes the delegate accordingly.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub Send(ByVal sender As Object, ByVal e As T)
        Dim evt As EventHandler(Of T) = Me.Handler
        If evt IsNot Nothing Then
            If Me.Context Is Nothing Then
                ' if not UI context, invoke the event
                evt.Invoke(sender, e)
            Else
                ' Dim context As SynchronizationContext = If(Me.Context, EventHandlerContext(Of T).CurrentSyncContext)
                ' if UI context, send
                Me.Context.Send(Sub() evt.Invoke(sender, e), Nothing)
            End If
        End If
    End Sub

End Class

