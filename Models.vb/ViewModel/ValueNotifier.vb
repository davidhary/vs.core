''' <summary> Comparable, equatable and formattable value Notifier. </summary>
''' <remarks>
''' This single-value class can be used to raise property change events. This could be used, for
''' example, to report a formattable progress. <para>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para><para>
''' David, 10/10/2014 </para>
''' </remarks>
Public Class ValueNotifier(Of T As {IComparable(Of T), IEquatable(Of T), IFormattable})
    Inherits ViewModelBase

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

#End Region

#Region " VALUE "

    ''' <summary> The value. </summary>
    Private _Value As T

    ''' <summary> Gets or sets the value. </summary>
    ''' <value> The value. </value>
    Public Property [Value] As T
        Get
            Return Me._Value
        End Get
        Set(ByVal value As T)
            If Not value.Equals(Me.Value) Then
                Me._Value = value
                Me.AsyncNotifyPropertyChanged()
                My.MyLibrary.DoEvents()
            End If
        End Set
    End Property

#End Region

End Class

