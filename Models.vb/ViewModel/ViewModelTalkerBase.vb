''' <summary> Defines the contract that must be implemented by View Models. </summary>
''' <remarks>
''' <list type="bullet">View Model Notify implementation: <item>
''' Notify Function calls Synchronization Context Post;</item><item>
''' Async Notify Function calls Synchronization Context Post;</item><item>
''' Sync Notify function calls Synchronization Context Send;</item><item>
''' Raise event (custom implementation) calls Synchronization Context Post;</item><item>
''' Notify is used as the default in property set functions.</item></list> <para>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved. </para><para>
''' Licensed under The MIT License.</para><para>
''' David, 12/10/2018. Created from property notifiers
''' </para>
''' </remarks>
Public MustInherit Class ViewModelTalkerBase
    Inherits ViewModelBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="ViewModelTalkerBase" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Protected Sub New()
        MyBase.New()
        Me._Talker = New TraceMessageTalker
        Me._IsAssignedTalker = False
    End Sub

    ''' <summary> Specialized constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="talker"> The talker. </param>
    Protected Sub New(ByVal talker As ITraceMessageTalker)
        Me.New()
        Me._Talker = talker
        Me._IsAssignedTalker = True
    End Sub

#End Region

End Class

