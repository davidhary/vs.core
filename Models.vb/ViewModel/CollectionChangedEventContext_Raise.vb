﻿Imports System.Threading
Imports System.Collections.Specialized

Partial Public Class NotifyCollectionChangedEventContext

#Region " ACTIVE CONTEXT "

    ''' <summary> Returns the current synchronization context. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is null. </exception>
    ''' <returns> A Threading.SynchronizationContext. </returns>
    Private Shared Function CurrentSyncContext() As Threading.SynchronizationContext
        If Threading.SynchronizationContext.Current Is Nothing Then
            Threading.SynchronizationContext.SetSynchronizationContext(New Threading.SynchronizationContext)
        End If
        If Threading.SynchronizationContext.Current Is Nothing Then
            Throw New InvalidOperationException("Current Synchronization Context not set;. Must be set before starting the thread.")
        End If
        Return Threading.SynchronizationContext.Current
    End Function

    ''' <summary> Gets a context for the active. </summary>
    ''' <value> The active context. </value>
    Private ReadOnly Property ActiveContext As SynchronizationContext
        Get
            Return If(Me.Context, NotifyCollectionChangedEventContext.CurrentSyncContext)
        End Get
    End Property

#End Region

#Region " SEND POST "

    ''' <summary>
    ''' Asynchronously raises (Posts) the <see cref="NotifyCollectionChangedEventHandler"/>. It does all the checking to see
    ''' if the SynchronizationContext is nothing or not, and invokes the delegate accordingly.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Collection Changed event information. </param>
    Public Sub Post(ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
        Dim evt As NotifyCollectionChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then Me.ActiveContext.Post(Sub() evt(sender, e), Nothing)
    End Sub

    ''' <summary>
    ''' Synchronously raises (sends) the <see cref="NotifyCollectionChangedEventHandler"/>. It does all the checking to see
    ''' if the SynchronizationContext is nothing or not, and invokes the delegate accordingly.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Collection Changed event information. </param>
    Public Sub Send(ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
        Dim evt As NotifyCollectionChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then Me.ActiveContext.Send(Sub() evt(sender, e), Nothing)
    End Sub

#End Region

End Class

