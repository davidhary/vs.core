﻿Imports System.ComponentModel
Partial Public Class ViewModelTalkerBase
    Implements INotifyPropertyChanged

    ''' <summary> Removes the property changed event handlers. </summary>
    Protected Sub RemovePropertyChangedEventHandlers()
        For i As Integer = Me.PropertyChangedHandlers.Count - 1 To 0 Step -1
            Me.PropertyChangedHandlers.RemoveAt(i)
        Next
    End Sub

    Private PropertyChangedHandlers As New PropertyChangeEventContextCollection()

    ''' <summary> Event queue for all listeners interested in Custom events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event PropertyChanged As PropertyChangedEventHandler Implements INotifyPropertyChanged.PropertyChanged

        AddHandler(ByVal value As PropertyChangedEventHandler)
            Me.PropertyChangedHandlers.Add(New PropertyChangedEventContext(value))
        End AddHandler

        RemoveHandler(ByVal value As PropertyChangedEventHandler)
            Me.PropertyChangedHandlers.RemoveValue(value)
        End RemoveHandler

        RaiseEvent(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            ' Defaults to 'Send' to prevent cross thread exceptions.
            Me.PropertyChangedHandlers.Send(sender, e)
        End RaiseEvent

    End Event

End Class
