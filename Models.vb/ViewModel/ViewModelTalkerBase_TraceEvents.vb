Imports System.ComponentModel

Partial Public Class ViewModelTalkerBase
    Implements ITalker

    #Region " TRACE EVENTS: LOG AND SHOW "

    ''' <summary> List of trace event bidnings. </summary>
    Private Shared _TraceEventBidningList As BindingList(Of TraceEventType)

    ''' <summary> Gets a list of trace event bindings. </summary>
    ''' <value> A List of trace event bindings. </value>
    Public Shared ReadOnly Property TraceEventBindingList As BindingList(Of TraceEventType)
        Get

            If ViewModelTalkerBase._TraceEventBidningList Is Nothing OrElse Not ViewModelTalkerBase._TraceEventBidningList.Any Then
                ViewModelTalkerBase._TraceEventBidningList = New BindingList(Of TraceEventType) From {TraceEventType.Critical, TraceEventType.Error,
                                                                                                TraceEventType.Warning, TraceEventType.Information,
                                                                                                TraceEventType.Verbose}
            End If
            Return ViewModelTalkerBase._TraceEventBidningList
        End Get
    End Property

    ''' <summary> The trace events. </summary>
    Private Shared _TraceEvents As List(Of KeyValuePair(Of TraceEventType, String))

    ''' <summary> Gets the trace events. </summary>
    ''' <value> The trace events. </value>
    Public Shared ReadOnly Property TraceEvents As IEnumerable(Of KeyValuePair(Of TraceEventType, String))
        Get

            If ViewModelTalkerBase._TraceEvents Is Nothing OrElse Not ViewModelTalkerBase._TraceEvents.Any Then
                ViewModelTalkerBase._TraceEvents = New List(Of KeyValuePair(Of TraceEventType, String)) From {
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Critical),
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Error),
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Warning),
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Information),
                                ViewModelTalkerBase.ToTraceEvent(TraceEventType.Verbose)}
            End If
            Return ViewModelTalkerBase._TraceEvents
        End Get
    End Property

    ''' <summary> Converts a value to a trace event. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> Value as a KeyValuePair(Of TraceEventType, String) </returns>
    Public Shared Function ToTraceEvent(ByVal value As TraceEventType) As KeyValuePair(Of TraceEventType, String)
        Return New KeyValuePair(Of TraceEventType, String)(value, value.ToString)
    End Function

    ''' <summary> The trace log event. </summary>
    Private _TraceLogEvent As KeyValuePair(Of TraceEventType, String)

    ''' <summary> Gets or sets the trace log level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceLogEvent As KeyValuePair(Of TraceEventType, String)
        Get

            Return Me._TraceLogEvent
        End Get
        Set(value As KeyValuePair(Of TraceEventType, String))
            If value.Key <> Me.TraceLogEvent.Key Then
                Me._TraceLogEvent = value
                Me.TraceLogLevel = value.Key
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the trace log level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceLogLevel As TraceEventType
        Get
            Return Me.Talker.TraceLogLevel
        End Get
        Set(value As TraceEventType)
            If value <> Me.TraceLogLevel Then
                Me.ApplyTalkerTraceLevel(ListenerType.Logger, value)
                Me.SyncNotifyPropertyChanged()
                Me.TraceLogEvent = ViewModelTalkerBase.ToTraceEvent(value)
            End If
        End Set
    End Property

    ''' <summary> The trace show event. </summary>
    Private _TraceShowEvent As KeyValuePair(Of TraceEventType, String)

    ''' <summary> Gets or sets the trace Show level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceShowEvent As KeyValuePair(Of TraceEventType, String)
        Get

            Return Me._TraceShowEvent
        End Get
        Set(value As KeyValuePair(Of TraceEventType, String))
            If value.Key <> Me.TraceShowEvent.Key Then
                Me._TraceShowEvent = value
                Me.TraceShowLevel = value.Key
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the trace Show level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceShowLevel As TraceEventType
        Get
            Return Me.Talker.TraceShowLevel
        End Get
        Set(value As TraceEventType)
            If value <> Me.TraceShowLevel Then
                Me.ApplyTalkerTraceLevel(ListenerType.Display, value)
                Me.SyncNotifyPropertyChanged()
                Me.TraceShowEvent = ViewModelTalkerBase.ToTraceEvent(value)
            End If
        End Set
    End Property

    #End Region

End Class

