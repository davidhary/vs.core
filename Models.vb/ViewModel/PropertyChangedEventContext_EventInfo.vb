﻿Imports System.ComponentModel
Imports System.Threading

Partial Public Class PropertyChangedEventContext

#Region " USING EVENT INFO "

    ''' <summary>
    ''' Asynchronously raises (Posts) or invokes (if no context) the <see cref="PropertyChangedEventHandler"/>. It does all the checking to see
    ''' if the SynchronizationContext is nothing or not, and invokes the delegate accordingly.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub PostOrInvoke(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Static sendPostCallbackDelegate As SendOrPostCallback
        Dim evt As PropertyChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then
            If Me.Context Is Nothing Then
                ' If the object is living in a non-UI thread, use invoke
                evt.Invoke(sender, e)
            Else
                If sendPostCallbackDelegate Is Nothing Then
                    sendPostCallbackDelegate = New SendOrPostCallback(AddressOf Me.OnEvent)
                End If
                Me.Context.Post(sendPostCallbackDelegate, New EventInfo(sender, e))
            End If
        End If
    End Sub

    ''' <summary>
    ''' Synchronously raises (sends) or invokes (if no context) the <see cref="PropertyChangedEventHandler"/>. It does all the checking to see
    ''' if the SynchronizationContext is nothing or not, and invokes the delegate accordingly.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub SendOrInvoke(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Static sendPostCallbackDelegate As SendOrPostCallback
        Dim evt As PropertyChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then
            If Me.Context Is Nothing Then
                ' If the object is living in a non-UI thread, use invoke
                evt.Invoke(sender, e)
            Else
                If sendPostCallbackDelegate Is Nothing Then
                    sendPostCallbackDelegate = New SendOrPostCallback(AddressOf Me.OnEvent)
                End If
                Me.Context.Send(sendPostCallbackDelegate, New EventInfo(sender, e))
            End If
        End If
    End Sub

    ''' <summary> Executes (invokes) the event handler post action. </summary>
    ''' <param name="eventInfo"> Information describing the event. </param>
    Private Sub OnEvent(ByVal eventInfo As Object)
        Dim evt As PropertyChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then
            Dim info As EventInfo = CType(eventInfo, EventInfo)
            evt.Invoke(info.Sender, info.Arguments)
        End If
    End Sub

    ''' <summary> Information about the event. </summary>
    Private Structure EventInfo
        Public ReadOnly Property Sender As Object
        Public ReadOnly Property Arguments As PropertyChangedEventArgs
        Public Sub New(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
            Me.Sender = sender
            Me.Arguments = e
        End Sub
    End Structure

#End Region

End Class

Partial Public Class PropertyChangeEventContextCollection

    ''' <summary> Executes (posts) the event handler action using the event info structure. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub PostOrInvoke(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        For i As Integer = 0 To Me.Count - 1 : Item(i).PostOrInvoke(sender, e) : Next
    End Sub

    ''' <summary> Executes (sends the event handler action using the event info structure. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub SendOrInvoke(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        For i As Integer = 0 To Me.Count - 1 : Item(i).SendOrInvoke(sender, e) : Next
    End Sub

End Class
