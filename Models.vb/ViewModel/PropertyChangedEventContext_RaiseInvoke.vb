﻿Imports System.ComponentModel
Imports System.Threading

Partial Public Class PropertyChangedEventContext

#Region " SEND POST "

    ''' <summary>
    ''' Asynchronously raises (Posts) the <see cref="PropertyChangedEventHandler"/>. It does all the checking to see
    ''' if the SynchronizationContext is nothing or not, and invokes the delegate accordingly.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub Post(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then
            If Me.Context Is Nothing Then
                ' invoke if non-UI context, 
                evt.Invoke(sender, e)
            Else
                Me.Context.Post(Sub() evt(sender, e), Nothing)
            End If
        End If
    End Sub

    ''' <summary>
    ''' Synchronously raises (sends) the <see cref="PropertyChangedEventHandler"/>. It does all the checking to see
    ''' if the SynchronizationContext is nothing or not, and invokes the delegate accordingly.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub Send(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then
            If Me.Context Is Nothing Then
                evt.Invoke(sender, e)
            Else
                Me.Context.Send(Sub() evt.Invoke(sender, e), Nothing)
            End If
        End If
    End Sub

#End Region

#Region " DYNAMIC INVOKE "

    ''' <summary> Posts a dynamic invoke of the event delegate. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub PostDynamicInvoke(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then
            If Context Is Nothing Then
                For Each d As [Delegate] In evt.GetInvocationList
                    d.DynamicInvoke(New Object() {sender, e})
                Next
            Else
                For Each d As [Delegate] In evt.GetInvocationList
                    Context.Post(Sub(ee) d.DynamicInvoke(New Object() {sender, e}), e)
                Next
            End If
        End If
    End Sub

#End Region

#Region " POST DYNAMIC INVOKE "

    ''' <summary> Posts a dynamic invoke of the event delegate. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub PostDynamicInvoke(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Dim context As SynchronizationContext = If(Me.Context, PropertyChangedEventContext.CurrentSyncContext)
        Me.PostDynamicInvoke(context, sender, e)
    End Sub

    ''' <summary> Posts a dynamic invoke of the event delegate. </summary>
    ''' <param name="context"> The context. </param>
    ''' <param name="sender">  Source of the event. </param>
    ''' <param name="e">       Property changed event information. </param>
    Private Sub PostDynamicInvoke(ByVal context As SynchronizationContext, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then
            For Each d As [Delegate] In evt.GetInvocationList
                context.Post(Sub(ee) d.DynamicInvoke(New Object() {sender, e}), e)
            Next
        End If
    End Sub

#End Region

#Region " TARGET INVOKE "

    ''' <summary> Invokes the event on that delegate target. </summary>
    ''' <param name="syncInvokeTarget"> The synchronization invoke target. </param>
    ''' <param name="[delegate]">       A [Delegate] to process. </param>
    ''' <param name="sender">           Source of the event. </param>
    ''' <param name="e">                A T to process. </param>
    Private Shared Sub TargetInvoke(ByVal syncInvokeTarget As ISynchronizeInvoke, [delegate] As [Delegate], ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        If syncInvokeTarget Is Nothing Then
            ' if the target does not implement the I Synchronize Invoke interface, e.g., it is not a control, 
            ' then do a dynamic invoke. this is a bit slow and does not support asynchronous call.
            [delegate].DynamicInvoke(New Object() {sender, e})
        Else
            ' execute the delegate on the target thread.
            syncInvokeTarget.Invoke([delegate], New Object() {sender, e})
        End If
    End Sub

    ''' <summary> Invokes the event on that delegate target. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      A T to process. </param>
    Public Sub TargetInvoke(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then
            For Each d As [Delegate] In evt.GetInvocationList
                If d.Target IsNot Nothing Then
                    ' asynchronously executes the delegate on the target thread.
                    ' https://blogs.msdn.microsoft.com/jaredpar/2008/01/07/isynchronizeinvoke-now/
                    Dim syncInvokeTarget As ISynchronizeInvoke = TryCast(d.Target, ISynchronizeInvoke)
                    PropertyChangedEventContext.TargetInvoke(syncInvokeTarget, d, sender, e)
                End If
            Next
        End If
    End Sub

#End Region

End Class

