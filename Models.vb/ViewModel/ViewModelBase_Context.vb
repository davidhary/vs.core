﻿Imports System.ComponentModel
Imports System.Threading
Partial Public Class ViewModelBase

#Region " SYNC CONTEXT: MOST LIKELY MNO LONGER REQUIRED "

    ''' <summary> Caches the synchronization context for threading functions. </summary>
    ''' <value> The captured synchronization context. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public ReadOnly Property CapturedSyncContext As Threading.SynchronizationContext

    ''' <summary> Applies the synchronization context. </summary>
    Private Sub ApplyCapturedSyncContextThis()
        If Me._CapturedSyncContext Is Nothing Then
            If SynchronizationContext.Current Is Nothing Then
                Me._CapturedSyncContext = New SynchronizationContext
            Else
                Me._CapturedSyncContext = SynchronizationContext.Current
            End If
        End If
        If SynchronizationContext.Current Is Nothing Then
            If Me._CapturedSyncContext Is Nothing Then Me._CapturedSyncContext = New SynchronizationContext
            Threading.SynchronizationContext.SetSynchronizationContext(Me._CapturedSyncContext)

        End If
    End Sub

    ''' <summary> Applies the captured or a new synchronization context. </summary>
    Public Sub ApplyCapturedSyncContext()
        Me._ApplyCapturedSyncContext()
    End Sub

    ''' <summary>
    ''' Captures and applies the given synchronization context, the current sync context or a new
    ''' sync contexts if the first two are null.
    ''' </summary>
    ''' <param name="syncContext"> Context for the synchronization. </param>
    Public Overridable Sub CaptureSyncContext(ByVal syncContext As Threading.SynchronizationContext)
        If syncContext IsNot Nothing Then Me._CapturedSyncContext = syncContext
        Me.ApplyCapturedSyncContext()
    End Sub

#End Region

End Class

