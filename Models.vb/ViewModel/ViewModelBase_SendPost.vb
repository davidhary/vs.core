﻿Imports System.ComponentModel
Partial Public Class ViewModelBase

#Region " POST "

    ''' <summary>
    ''' Asynchronously notifies (post) property change on a different thread. Unsafe for cross threading.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 7 to 10 times larger than naked raise event. This has no
    ''' advantage even with slow handler functions.
    ''' </remarks>
    ''' <param name="e"> Property Changed event information. </param>
    <Obsolete("Use Async Notify")>
    Protected Overridable Sub PostPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.PropertyChangedHandlers.Post(Me, e)
    End Sub

    ''' <summary>
    ''' Asynchronously notifies (post) property change on a different thread. Unsafe for cross threading.
    ''' </summary>
    ''' <param name="name"> (Optional) caller member. </param>
        <Obsolete("Use Async Notify")>
    Protected Sub PostPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.PostPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#Region " SEND "

    ''' <summary>
    ''' Synchronously notifies property change on the synchronization thread using 'Send' to prevent
    ''' cross thread exceptions.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 3 to 4 times larger than naked raise event. This is the best approach.
    ''' </remarks>
    ''' <param name="e"> Property Changed event information. </param>
    <Obsolete("Use Sync Notify")>
    Protected Overridable Sub SendPropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.PropertyChangedHandlers.Send(Me, e)
    End Sub

    ''' <summary>
    ''' Synchronously notifies property change on the synchronization thread using 'Send' to prevent
    ''' cross thread exceptions.
    ''' </summary>
    ''' <param name="name"> (Optional) caller member. </param>
    <Obsolete("Use Sync Notify")>
        Protected Sub SendPropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.SendPropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

#End Region

#Region " POST DYNAMIC INVOKE "

    ''' <summary> Asynchronously posts and dynamically invokes the property changed event. </summary>
    ''' <remarks>
    ''' Overhead of this method is 3 to 4 times larger than naked raise event. This is equivalent
    ''' the<see cref="SyncNotifyPropertyChanged(PropertyChangedEventArgs)"/> method,.
    ''' </remarks>
    ''' <param name="name"> (Optional) caller member. </param>
        Protected Sub PostDynamicInvokePropertyChanged(<Runtime.CompilerServices.CallerMemberName()> Optional ByVal name As String = Nothing)
        If Not String.IsNullOrWhiteSpace(name) Then
            Me.PostDynamicInvokePropertyChanged(New PropertyChangedEventArgs(name))
        End If
    End Sub

    ''' <summary> Asynchronously posts and dynamically invokes the property changed event. </summary>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub PostDynamicInvokePropertyChanged(ByVal e As PropertyChangedEventArgs)
        Me.PropertyChangedHandlers.PostDynamicInvoke(Me, e)
    End Sub

#End Region

End Class

