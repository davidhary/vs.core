Imports isr.Core.Models.ExceptionExtensions

''' <summary> A resource selector view model base class. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/11/2018 </para>
''' </remarks>
Public MustInherit Class SelectorViewModel
    Inherits ViewModelTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Protected Sub New()
        MyBase.New
        Me.NewThis()
    End Sub

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="talker"> The talker. </param>
    Protected Sub New(ByVal talker As ITraceMessageTalker)
        MyBase.New(talker)
        Me.NewThis()
    End Sub

    ''' <summary> Creates a new this. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Private Sub NewThis()
        Me._ResourceNames = New System.ComponentModel.BindingList(Of String)
        Me._SearchImage = My.Resources.Find
        Me._IsOpen = False
        Me._Searchable = True
    End Sub

#End Region

#Region " SEARCHABLE "

    ''' <summary> True if searchable. </summary>
    Private _Searchable As Boolean

    ''' <summary>
    ''' Gets or sets the condition determining if the control can be searchable. The elements can be
    ''' searched only if not open.
    ''' </summary>
    ''' <value> The searchable. </value>
    Public Property Searchable() As Boolean
        Get
            Return Me._Searchable
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Searchable.Equals(value) Then
                Me._Searchable = value
                Me.SyncNotifyPropertyChanged()
            End If
            Me.SyncNotifyPropertyChanged(NameOf(SelectorViewModel.SearchEnabled))
        End Set
    End Property

    ''' <summary> Gets search enabled state. </summary>
    ''' <value> The search enabled state. </value>
    Public ReadOnly Property SearchEnabled() As Boolean
        Get
            Return Me.Searchable AndAlso Not Me.IsOpen
        End Get
    End Property

    ''' <summary> True if is open, false if not. </summary>
    Private _IsOpen As Boolean

    ''' <summary> Gets or sets the open status. </summary>
    ''' <remarks> the search should be disabled when the resource is open. </remarks>
    ''' <value> The open status. </value>
    Public Property IsOpen() As Boolean
        Get
            Return Me._IsOpen
        End Get
        Set(value As Boolean)
            If Me.IsOpen <> value Then
                Me._IsOpen = value
                Me.SyncNotifyPropertyChanged()
            End If
            Me.SyncNotifyPropertyChanged(NameOf(SelectorViewModel.SearchEnabled))
        End Set
    End Property

    ''' <summary> The search tool tip. </summary>
    Private _SearchToolTip As String

    ''' <summary> Gets or sets the Search tool tip. </summary>
    ''' <value> The Search tool tip. </value>
    Public Property SearchToolTip() As String
        Get
            Return Me._SearchToolTip
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.SearchToolTip) Then
                Me._SearchToolTip = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The search image. </summary>
    Private _SearchImage As System.Drawing.Image

    ''' <summary> Gets or sets the Search Image. </summary>
    ''' <value> The Search tool tip. </value>
    Public Property SearchImage() As System.Drawing.Image
        Get
            Return Me._SearchImage
        End Get
        Set(ByVal value As System.Drawing.Image)
            If Not System.Drawing.Image.Equals(value, Me.SearchImage) Then
                Me._SearchImage = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " RESOURCE NAME "

    ''' <summary> Name of the candidate resource. </summary>
    Private _CandidateResourceName As String

    ''' <summary> Gets or sets the name of the candidate resource. </summary>
    ''' <value> The name of the candidate resource. </value>
    Public Property CandidateResourceName() As String
        Get
            Return Me._CandidateResourceName
        End Get
        Set(ByVal value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            value = value.Trim
            If Not String.Equals(value, Me.CandidateResourceName, StringComparison.OrdinalIgnoreCase) Then
                Me._CandidateResourceName = value
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets the resource found tool tip. </summary>
    ''' <value> The resource found tool tip. </value>
    Public Property ResourceFoundToolTip As String = "Resource found"

    ''' <summary> Gets the resource not found tool tip. </summary>
    ''' <value> The resource not found tool tip. </value>
    Public Property ResourceNotFoundToolTip As String = "Resource not found"

    ''' <summary> Gets existence status of the resource. </summary>
    ''' <value> The resource name exists. </value>
    Public ReadOnly Property SelectedResourceExists As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.ValidatedResourceName)
        End Get
    End Property

    ''' <summary> Name of the validated resource. </summary>
    Private _ValidatedResourceName As String

    ''' <summary> Returns the validated resource name. </summary>
    ''' <value> The name of the validated resource. </value>
    Public Property ValidatedResourceName() As String
        Get
            Return Me._ValidatedResourceName
        End Get
        Set(value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            value = value.Trim
            If Not String.Equals(value, Me.ValidatedResourceName, StringComparison.OrdinalIgnoreCase) Then
                Me._ValidatedResourceName = value
                Me.SyncNotifyPropertyChanged()
                Me.SearchToolTip = If(String.IsNullOrWhiteSpace(Me.ValidatedResourceName), Me.ResourceFoundToolTip, Me.ResourceNotFoundToolTip)
            End If
        End Set
    End Property

    ''' <summary> Queries resource exists. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public MustOverride Function QueryResourceExists(ByVal resourceName As String) As Boolean

#End Region

#Region " VALIDATE RESOURCE  "

    ''' <summary> True to enable, false to disable the validation. </summary>
    Private _ValidationEnabled As Boolean

    ''' <summary> Gets or sets Validation Enabled state. </summary>
    ''' <remarks>
    ''' Validation is disabled by default to facilitate resource selection in case of resource
    ''' manager mismatch between VISA implementations.
    ''' </remarks>
    ''' <value> The Validation enabled. </value>
    Public Property ValidationEnabled() As Boolean
        Get
            Return Me._ValidationEnabled
        End Get
        Set(value As Boolean)
            Me._ValidationEnabled = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Attempts to validate (e.g., check existence) the resource by name. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="resourceName"> Name of the resource. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Public MustOverride Function TryValidateResource(ByVal resourceName As String) As (Success As Boolean, Details As String)

    ''' <summary> Used to notify of start of the validation of resource name. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="resourceName"> The name of the resource. </param>
    ''' <param name="e">            Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnValidatingResourceName(ByVal resourceName As String, ByVal e As ComponentModel.CancelEventArgs)
        Me.PublishInfo($"Validating {resourceName};. ")
    End Sub

    ''' <summary> Used to notify of validation of resource name. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="resourceName"> The name of the resource. </param>
    ''' <param name="e">            Action event information. </param>
    Protected Overridable Sub OnResourceNameValidated(ByVal resourceName As String, ByVal e As EventArgs)
        Me.PublishInfo($"{resourceName} validated;. ")
    End Sub

    ''' <summary> Attempts to validate resource name from the given data. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="resourceName">        The name of the resource. </param>
    ''' <param name="applyEnabledFilters"> True to apply filters if enabled. </param>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function TryValidateResourceName(ByVal resourceName As String, ByVal applyEnabledFilters As Boolean) As (Success As Boolean, Details As String)
        Dim result As (Success As Boolean, Details As String) = (True, String.Empty)
        Dim activity As String = String.Empty
        Try
            activity = Me.PublishVerbose($"Selecting resource name; checking conditions")
            If String.IsNullOrWhiteSpace(resourceName) Then
                result = (False, $"{activity} canceled because name is empty")
            Else
                Dim args As New System.ComponentModel.CancelEventArgs
                activity = Me.PublishVerbose("Notifying 'Selecting' resource name")
                Me.OnValidatingResourceName(resourceName, args)
                If args.Cancel Then
                    activity = Me.PublishVerbose("'Selecting' canceled")
                Else
                    activity = Me.PublishVerbose("enumerating resource names")
                    Me.EnumerateResources(applyEnabledFilters)
                    activity = Me.PublishVerbose($"trying to select resource '{resourceName}'")
                    result = Me.TryValidateResource(resourceName)
                    If result.Success Then
                        Me.OnResourceNameValidated(resourceName, EventArgs.Empty)
                    End If
                End If
            End If
        Catch ex As Exception
            result = (False, $"Exception {activity};. {ex.ToFullBlownString}")
        Finally
        End Try
        If Not result.Success Then Me.PublishWarning(result.Details)
        Return result
    End Function

#End Region

#Region " ENUMERATE RESOURCES "

    ''' <summary> True if has resources, false if not. </summary>
    Private _HasResources As Boolean

    ''' <summary> Gets or sets the sentinel indication if resources were enumerated. </summary>
    ''' <value> The has resources. </value>
    Public Property HasResources As Boolean
        Get
            Return Me._HasResources
        End Get
        Set(ByVal value As Boolean)
            If Not Me.HasResources.Equals(value) Then
                Me._HasResources = value
                ' using sync notification is required when using unit tests; otherwise,
                ' actions invoked by the event may not occur.
                Me.SyncNotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Gets or sets the resource names. </summary>
    ''' <value> The resource names. </value>
    Public ReadOnly Property ResourceNames() As System.ComponentModel.BindingList(Of String)

    ''' <summary> Enumerate resource names. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="names"> The names. </param>
    ''' <returns>
    ''' A =<see cref="System.ComponentModel.BindingList(Of System.String)">binding list</see>
    ''' </returns>
    Public Function EnumerateResources(ByVal names As String()) As System.ComponentModel.BindingList(Of String)
        ' suspend binding while building the resource names
        Me._ResourceNames.RaiseListChangedEvents = False
        Me._ResourceNames.Clear()
        If names?.Any Then
            For Each name As String In names
                Me._ResourceNames.Add(name)
            Next
        End If
        ' resume and reset binding 
        Me._ResourceNames.RaiseListChangedEvents = True
        Me._ResourceNames.ResetBindings()
        Me.SyncNotifyPropertyChanged(NameOf(SelectorViewModel.ResourceNames))
        Me.HasResources = Me.ResourceNames.Any
        Return Me.ResourceNames
    End Function

    ''' <summary> Enumerate resource names. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="names"> The names. </param>
    ''' <returns>
    ''' A =<see cref="System.ComponentModel.BindingList(Of System.String)">binding list</see>
    ''' </returns>
    Public Function EnumerateResources(ByVal names As IEnumerable(Of String)) As System.ComponentModel.BindingList(Of String)
        ' suspend binding while building the resource names
        Me._ResourceNames.RaiseListChangedEvents = False
        Me._ResourceNames.Clear()
        If names?.Any Then
            For Each name As String In names
                Me._ResourceNames.Add(name)
            Next
        End If
        ' resume and reset binding 
        Me._ResourceNames.RaiseListChangedEvents = True
        Me._ResourceNames.ResetBindings()
        Me.SyncNotifyPropertyChanged(NameOf(SelectorViewModel.ResourceNames))
        Me.HasResources = Me.ResourceNames.Any
        Return Me.ResourceNames
    End Function

    ''' <summary> Enumerate resource names. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="names"> The names. </param>
    ''' <returns>
    ''' A =<see cref="System.ComponentModel.BindingList(Of System.String)">binding list</see>
    ''' </returns>
    Public Function EnumerateResources(ByVal names As IList(Of String)) As System.ComponentModel.BindingList(Of String)
        ' suspend binding while building the resource names
        Me._ResourceNames.RaiseListChangedEvents = False
        Me._ResourceNames.Clear()
        If names?.Any Then
            For Each name As String In names
                Me._ResourceNames.Add(name)
            Next
        End If
        ' resume and reset binding 
        Me._ResourceNames.RaiseListChangedEvents = True
        Me._ResourceNames.ResetBindings()
        Me.SyncNotifyPropertyChanged(NameOf(SelectorViewModel.ResourceNames))
        Me.HasResources = Me.ResourceNames.Any
        Return Me.ResourceNames
    End Function

    ''' <summary> Enumerate resource names. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="applyEnabledFilters"> True to apply filters if enabled. </param>
    ''' <returns> A list of <see cref="System.String"/>. </returns>
    Public MustOverride Function EnumerateResources(ByVal applyEnabledFilters As Boolean) As System.ComponentModel.BindingList(Of String)

#End Region

#Region " VALIDATE RESOURCE NAME ASYNC TASK "

    ''' <summary> Gets or sets the default resource name validation task timeout. </summary>
    ''' <value> The default resource name validation timeout. </value>
    Public Shared Property DefaultResourceNameValidationTaskTimeout As TimeSpan = TimeSpan.FromSeconds(5)

    ''' <summary> Gets or sets the resource name validation tasker. </summary>
    ''' <value> The resource name validation tasker. </value>
    Public ReadOnly Property ResourceNameValidationTasker As Tasker(Of (Success As Boolean, Details As String))

    ''' <summary> Starts task validating resource name. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <param name="resourceName">        The name of the resource. </param>
    ''' <param name="applyEnabledFilters"> True to apply filters if enabled. </param>
    Public Sub StartTaskValidatingResourceName(ByVal resourceName As String, ByVal applyEnabledFilters As Boolean)
        Me._ResourceNameValidationTasker = New isr.Core.Tasker(Of (Success As Boolean, Details As String))
        Me._ResourceNameValidationTasker.StartAction(Function()
                                                         Return Me.TryValidateResourceName(resourceName, applyEnabledFilters)
                                                     End Function)
    End Sub

    ''' <summary> Await resource name selection. </summary>
    ''' <remarks> David, 8/5/2020. </remarks>
    ''' <returns> The (Success As Boolean, Details As String) </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="<Pending>")>
    Public Function AwaitResourceNameSelection() As (Success As Boolean, Details As String)
        Try
            Dim result As (Status As Threading.Tasks.TaskStatus, Result As (Success As Boolean, Details As String)) =
                           Me.ResourceNameValidationTasker.AwaitCompletion(SelectorViewModel.DefaultResourceNameValidationTaskTimeout)
            Return If(Threading.Tasks.TaskStatus.RanToCompletion = result.Status,
                    (True, "buffer streaming completed"),
                    (False, $"resource name selection task existed with a { result.Status} status"))
        Catch ex As Exception
            Return (False, ex.ToFullBlownString)
        End Try
    End Function

#End Region

End Class

