﻿Imports System.ComponentModel
Imports System.Threading

Partial Public Class EventHandlerContext(Of T As Class)

#Region " USING EVENT INFO "

    ''' <summary>
    ''' Asynchronously raises (Posts) or invokes the <see cref="EventHandler(Of T)"/> event. It does
    ''' all the checking to see if the SynchronizationContext is nothing or not, and invokes the
    ''' delegate accordingly.
    ''' </summary>
    ''' <remarks>
    ''' This original code was replaced by code that requires no casting from the <see cref="EventInfo"/>
    ''' <see cref="EventInfo"/>
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      The event information. </param>
    Public Sub PostOrInvoke(ByVal sender As Object, ByVal e As T)
        Static sendPostCallbackDelegate As SendOrPostCallback
        If Me.Context Is Nothing Then
            Me.Handler.Invoke(sender, e)
        Else
            If sendPostCallbackDelegate Is Nothing Then
                sendPostCallbackDelegate = New SendOrPostCallback(AddressOf Me.OnEvent)
            End If
            Me.Context.Post(sendPostCallbackDelegate, New EventInfo(sender, e))
        End If
    End Sub

    ''' <summary>
    ''' Synchronously raises (sends) or invokes the <see cref="EventHandler(Of T)"/> event. It does all the checking to see
    ''' if the SynchronizationContext is nothing or not, and invokes the delegate accordingly.
    ''' </summary>
    ''' <remarks>
    ''' This original code was replaced by code that requires no casting from the <see cref="EventInfo"/>
    ''' </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      The event information. </param>
    Public Sub SendOrInvoke(ByVal sender As Object, ByVal e As T)
        Static sendPostCallbackDelegate As SendOrPostCallback
        If Me.Context Is Nothing Then
            Me.Handler.Invoke(sender, e)
        Else
            If sendPostCallbackDelegate Is Nothing Then
                sendPostCallbackDelegate = New SendOrPostCallback(AddressOf Me.OnEvent)
            End If
            Me.Context.Send(sendPostCallbackDelegate, New EventInfo(sender, e))
        End If
    End Sub

    ''' <summary> Executes the event handler post action. </summary>
    ''' <param name="eventInfo"> Information describing the event. </param>
    Private Sub OnEvent(ByVal eventInfo As Object)
        Dim info As EventInfo = CType(eventInfo, EventInfo)
        Me.Handler.Invoke(info.Sender, info.Arguments)
    End Sub

    Private Structure EventInfo
        Public ReadOnly Property Sender As Object
        Public ReadOnly Property Arguments As T
        Public Sub New(ByVal sender As Object, ByVal e As T)
            Me.Sender = sender
            Me.Arguments = e
        End Sub
    End Structure

#End Region

End Class

Partial Public Class EventHandlerContextCollection(Of T As Class)

    ''' <summary> Post this message. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      A T to process. </param>
    Public Sub PostOrInvoke(sender As Object, ByVal e As T)
        For i As Integer = 0 To Me.Count - 1 : Item(i).PostOrInvoke(sender, e) : Next
    End Sub

    ''' <summary> Send this message. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      A T to process. </param>
    Public Sub SendOrInvoke(sender As Object, ByVal e As T)
        For i As Integer = 0 To Me.Count - 1 : Item(i).SendOrInvoke(sender, e) : Next
    End Sub

End Class
