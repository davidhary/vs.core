Imports isr.Core.Models.ExceptionExtensions

''' <summary> A resource Opener view model base class. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/11/2018 </para>
''' </remarks>
Public MustInherit Class OpenerViewModel
    Inherits ViewModelTalkerBase

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Protected Sub New()
        MyBase.New
        Me.NewThis()
    End Sub

    ''' <summary> Specialized default constructor for use only by derived class. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="talker"> The talker. </param>
    Protected Sub New(ByVal talker As ITraceMessageTalker)
        MyBase.New(talker)
        Me.NewThis()
    End Sub

    ''' <summary> News this object. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Private Sub NewThis()
        Me._OpenedImage = My.Resources.Opened
        Me._ClosedImage = My.Resources.Closed
        Me._ClearImage = My.Resources.Clear
        Me._Openable = True
        Me._Clearable = True
    End Sub

#End Region

#Region " CLEARABLE "

    ''' <summary> True if clearable. </summary>
    Private _Clearable As Boolean

    ''' <summary>
    ''' Gets or sets the value indicating if the clear button is visible and can be enabled. An item
    ''' can be cleared only if it is Opened.
    ''' </summary>
    ''' <value> The clearable. </value>
    Public Property Clearable() As Boolean
        Get
            Return Me._Clearable
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Clearable.Equals(value) Then
                Me._Clearable = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The clear tool tip. </summary>
    Private _ClearToolTip As String

    ''' <summary> Gets or sets the Clear tool tip. </summary>
    ''' <value> The Clear tool tip. </value>
    Public Property ClearToolTip() As String
        Get
            Return Me._ClearToolTip
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.ClearToolTip) Then
                Me._ClearToolTip = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The clear image. </summary>
    Private _ClearImage As System.Drawing.Image

    ''' <summary> Gets or sets the Clear Image. </summary>
    ''' <value> The Clear tool tip. </value>
    Public Property ClearImage() As System.Drawing.Image
        Get
            Return Me._ClearImage
        End Get
        Set(ByVal value As System.Drawing.Image)
            If Not System.Drawing.Image.Equals(value, Me.ClearImage) Then
                Me._ClearImage = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " OPENABLE "

    ''' <summary> True if openable. </summary>
    Private _Openable As Boolean

    ''' <summary>
    ''' Gets or sets the value indicating if the Open button is visible and can be enabled. An item
    ''' can be Opened only if it is selected.
    ''' </summary>
    ''' <value> The Openable. </value>
    Public Property Openable() As Boolean
        Get
            Return Me._Openable
        End Get
        Set(ByVal value As Boolean)
            If Not Me.Openable.Equals(value) Then
                Me._Openable = value
                Me.NotifyPropertyChanged()
            End If
            Me.OpenEnabled = Me.Openable AndAlso Not String.IsNullOrWhiteSpace(Me.ValidatedResourceName)
        End Set
    End Property

    ''' <summary> True to enable, false to disable the open. </summary>
    Private _OpenEnabled As Boolean

    ''' <summary> Gets or sets Open Enabled state. </summary>
    ''' <value> The Open enabled. </value>
    Public Property OpenEnabled() As Boolean
        Get
            Return Me._OpenEnabled
        End Get
        Set(value As Boolean)
            Me._OpenEnabled = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> True to enable, false to disable the validation. </summary>
    Private _ValidationEnabled As Boolean

    ''' <summary> Gets or sets Validation Enabled state. </summary>
    ''' <remarks>
    ''' Validation is disabled by default to facilitate resource selection in case of resource
    ''' manager mismatch between VISA implementations.
    ''' </remarks>
    ''' <value> The Validation enabled. </value>
    Public Property ValidationEnabled() As Boolean
        Get
            Return Me._ValidationEnabled
        End Get
        Set(value As Boolean)
            Me._ValidationEnabled = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> Name of the validated resource. </summary>
    Private _ValidatedResourceName As String

    ''' <summary> Returns the validated resource name. </summary>
    ''' <value> The name of the validated resource. </value>
    Public Property ValidatedResourceName() As String
        Get
            Return Me._ValidatedResourceName
        End Get
        Set(value As String)
            If String.IsNullOrWhiteSpace(value) Then value = String.Empty
            value = value.Trim
            If Not String.Equals(value, Me.ValidatedResourceName, StringComparison.OrdinalIgnoreCase) Then
                Me._ValidatedResourceName = value
                Me.NotifyPropertyChanged()
            End If
            Me.CandidateResourceNameValidated = Not String.IsNullOrWhiteSpace(Me.ValidatedResourceName)
            Me.OpenEnabled = Me.Openable AndAlso Me.CandidateResourceNameValidated
        End Set
    End Property

    ''' <summary> True if candidated resource name validated. </summary>
    Private _CandidatedResourceNameValidated As Boolean

    ''' <summary> Gets or sets the candidate resource name validated. </summary>
    ''' <value> The candidate resource name validated. </value>
    Public Overridable Property CandidateResourceNameValidated As Boolean
        Get
            Return Me._CandidatedResourceNameValidated
        End Get
        Set(value As Boolean)
            Me._CandidatedResourceNameValidated = value
            Me.NotifyPropertyChanged()
        End Set
    End Property

    ''' <summary> The open tool tip. </summary>
    Private _OpenToolTip As String

    ''' <summary> Gets or sets the Open tool tip. </summary>
    ''' <value> The Open tool tip. </value>
    Public Property OpenToolTip() As String
        Get
            Return Me._OpenToolTip
        End Get
        Set(ByVal value As String)
            If Not String.Equals(value, Me.OpenToolTip) Then
                Me._OpenToolTip = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " OPEN FIELDS "

    ''' <summary> Gets the Opened image. </summary>
    ''' <value> The Opened image. </value>
    Public Property OpenedImage As System.Drawing.Image

    ''' <summary> Gets the Closed image. </summary>
    ''' <value> The Closed image. </value>
    Public Property ClosedImage As System.Drawing.Image

    ''' <summary> Gets the Open image. </summary>
    ''' <value> The Open image. </value>
    Public ReadOnly Property OpenImage As System.Drawing.Image
        Get
            Return If(Me.IsOpen, Me.OpenedImage, Me.ClosedImage)
        End Get
    End Property

    ''' <summary> Gets the Open status. </summary>
    ''' <value> The Open status. </value>
    Public MustOverride ReadOnly Property IsOpen() As Boolean

    ''' <summary> Gets the close status. </summary>
    ''' <value> The Close status. </value>
    Public ReadOnly Property IsClose() As Boolean
        Get
            Return Not Me.IsOpen
        End Get
    End Property

#End Region

#Region " NAME  "

    ''' <summary> Gets the name of the designated resource. </summary>
    ''' <value> The name of the designated resource. </value>
    Public ReadOnly Property DesignatedResourceName As String
        Get
            Return If(Me.CandidateResourceNameValidated, Me.ValidatedResourceName, Me.CandidateResourceName)
        End Get
    End Property

    ''' <summary> Name of the candidate resource. </summary>
    Private _CandidateResourceName As String

    ''' <summary> Gets or sets the name of the candidate resource. </summary>
    ''' <value> The name of the candidate resource. </value>
    Public Overridable Property CandidateResourceName As String
        Get
            Return Me._CandidateResourceName
        End Get
        Set(ByVal value As String)
            If value Is Nothing Then value = String.Empty
            If Not String.Equals(Me.CandidateResourceName, value) Then
                Me._CandidateResourceName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> Name of the open resource. </summary>
    Private _OpenResourceName As String

    ''' <summary> Gets or sets the name of the open resource. </summary>
    ''' <value> The name of the open resource. </value>
    Public Overridable Property OpenResourceName As String
        Get
            Return Me._OpenResourceName
        End Get
        Set(ByVal value As String)
            If value Is Nothing Then value = String.Empty
            If Not String.Equals(Me.OpenResourceName, value) Then
                Me._OpenResourceName = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " TITLE "

    ''' <summary> The candidate resource title. </summary>
    Private _CandidateResourceTitle As String

    ''' <summary> Gets or sets the candidate resource title. </summary>
    ''' <value> The candidate resource title. </value>
    Public Overridable Property CandidateResourceTitle As String
        Get
            Return Me._CandidateResourceTitle
        End Get
        Set(value As String)
            If value Is Nothing Then value = String.Empty
            If Not String.Equals(value, Me.CandidateResourceTitle) Then
                Me._CandidateResourceTitle = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The open resource title. </summary>
    Private _OpenResourceTitle As String

    ''' <summary> Gets or sets a short title for the device. </summary>
    ''' <value> The short title of the device. </value>
    Public Overridable Property OpenResourceTitle As String
        Get
            Return Me._OpenResourceTitle
        End Get
        Set(value As String)
            If value Is Nothing Then value = String.Empty
            If Not String.Equals(Me.OpenResourceTitle, value) Then
                Me._OpenResourceTitle = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The resource title caption. </summary>
    Private _ResourceTitleCaption As String

    ''' <summary> Gets or sets the Title caption. </summary>
    ''' <value> The Title caption. </value>
    Public Overridable Property ResourceTitleCaption As String
        Get
            Return Me._ResourceTitleCaption
        End Get
        Set(value As String)
            If Not String.Equals(Me.ResourceTitleCaption, value) Then
                Me._ResourceTitleCaption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " CAPTION "

    ''' <summary> The resource closed caption. </summary>
    Private _ResourceClosedCaption As String

    ''' <summary> Gets or sets the default resource name closed caption. </summary>
    ''' <value> The resource closed caption. </value>
    Public Overridable Property ResourceClosedCaption As String
        Get
            If String.IsNullOrEmpty(Me._ResourceClosedCaption) Then
                Me._ResourceClosedCaption = "<closed>"
            End If
            Return Me._ResourceClosedCaption
        End Get
        Set(ByVal value As String)
            If Not String.Equals(Me.ResourceClosedCaption, value) Then
                Me._ResourceClosedCaption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

    ''' <summary> The resource name caption. </summary>
    Private _ResourceNameCaption As String

    ''' <summary> Gets or sets the resource name caption. </summary>
    ''' <value>
    ''' The <see cref="OpenResourceName"/> or <see cref="CandidateResourceName"/> resource names.
    ''' </value>
    Public Overridable Property ResourceNameCaption As String
        Get
            Return Me._ResourceNameCaption
        End Get
        Set(value As String)
            If value Is Nothing Then value = String.Empty
            If Not String.Equals(Me.ResourceNameCaption, value) Then
                Me._ResourceNameCaption = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " OPENING "

    ''' <summary> Allows taking actions before opening. </summary>
    ''' <remarks>
    ''' This override should occur as the first call of the overriding method. After this call, the
    ''' parent class adds the subsystems.
    ''' </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnOpening(ByVal e As System.ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If Not e.Cancel Then
            Me.IsInitialized = False
            Me.SyncNotifyOpening(e)
        End If
    End Sub

    ''' <summary> Removes the Opening event handlers. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Protected Sub RemoveOpeningEventHandlers()
        Me._OpeningEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The Opening event handlers. </summary>
    Private ReadOnly _OpeningEventHandlers As New EventHandlerContextCollection(Of System.ComponentModel.CancelEventArgs)

    ''' <summary> Event queue for all listeners interested in Opening events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Opening As EventHandler(Of System.ComponentModel.CancelEventArgs)
        AddHandler(value As EventHandler(Of System.ComponentModel.CancelEventArgs))
            Me._OpeningEventHandlers.Add(New EventHandlerContext(Of System.ComponentModel.CancelEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.ComponentModel.CancelEventArgs))
            Me._OpeningEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.ComponentModel.CancelEventArgs)
            Me._OpeningEventHandlers.Send(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="Opening">Opening Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyOpening(ByVal e As ComponentModel.CancelEventArgs)
        Me._OpeningEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " OPENED "

    ''' <summary> Notifies an open changed. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Public Overridable Sub NotifyOpenChanged()
        Me.OpenToolTip = $"Click to {If(Me.IsOpen, "Close", "Open")}"
        Me.NotifyPropertyChanged(NameOf(OpenerViewModel.IsOpen))
        Me.NotifyPropertyChanged(NameOf(OpenerViewModel.IsClose))
        Me.NotifyPropertyChanged(NameOf(OpenerViewModel.OpenImage))
        If Me.IsOpen Then
            Me.ResourceTitleCaption = Me.OpenResourceTitle
            Me.CandidateResourceTitle = Me.OpenResourceTitle
            Me.ResourceNameCaption = $"{Me.OpenResourceTitle}.{Me.OpenResourceName}"
        Else
            Me.ResourceTitleCaption = $"{Me.OpenResourceTitle}.{Me.ResourceClosedCaption}"
            Me.ResourceNameCaption = $"{Me.OpenResourceTitle}.{Me.OpenResourceName}.{Me.ResourceClosedCaption}"
        End If
    End Sub

    ''' <summary> Notifies of the opened event. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnOpened(ByVal e As System.EventArgs)
        Me.NotifyOpenChanged()
        Me.SyncNotifyOpened(e)
    End Sub

    ''' <summary> Removes the Opened event handlers. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Protected Sub RemoveOpenedEventHandlers()
        Me._OpenedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The Opened event handlers. </summary>
    Private ReadOnly _OpenedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in Opened events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Opened As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._OpenedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._OpenedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._OpenedEventHandlers.Send(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="Opened">Opened Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyOpened(ByVal e As System.EventArgs)
        Me._OpenedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " CLOSING "

    ''' <summary> Allows taking actions before closing. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If Not e.Cancel Then
            Me.IsInitialized = False
            Me.SyncNotifyClosing(e)
        End If
    End Sub

    ''' <summary> Removes the Closing event handlers. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Protected Sub RemoveClosingEventHandlers()
        Me._ClosingEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The Closing event handlers. </summary>
    Private ReadOnly _ClosingEventHandlers As New EventHandlerContextCollection(Of System.ComponentModel.CancelEventArgs)

    ''' <summary> Event queue for all listeners interested in Closing events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Closing As EventHandler(Of System.ComponentModel.CancelEventArgs)
        AddHandler(value As EventHandler(Of System.ComponentModel.CancelEventArgs))
            Me._ClosingEventHandlers.Add(New EventHandlerContext(Of System.ComponentModel.CancelEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.ComponentModel.CancelEventArgs))
            Me._ClosingEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.ComponentModel.CancelEventArgs)
            Me._ClosingEventHandlers.Send(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="Closing">Closing Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyClosing(ByVal e As System.ComponentModel.CancelEventArgs)
        Me._ClosingEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " CLOSED "

    ''' <summary> Notifies of the closed event. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnClosed(ByVal e As System.EventArgs)
        Me.NotifyOpenChanged()
        Me.SyncNotifyClosed(e)
    End Sub

    ''' <summary> Removes the Closed event handlers. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Protected Sub RemoveClosedEventHandlers()
        Me._ClosedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The Closed event handlers. </summary>
    Private ReadOnly _ClosedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in Closed events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Closed As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._ClosedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._ClosedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._ClosedEventHandlers.Send(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="Closed">Closed Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyClosed(ByVal e As System.EventArgs)
        Me._ClosedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " INITIALIZING "

    ''' <summary> Allows taking actions before Initializing. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnInitializing(ByVal e As System.ComponentModel.CancelEventArgs)
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        If e IsNot Nothing AndAlso Not e.Cancel Then Me.SyncNotifyInitializing(e)
    End Sub

    ''' <summary> Removes the Initializing event handlers. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Protected Sub RemoveInitializingEventHandlers()
        Me._InitializingEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The Initializing event handlers. </summary>
    Private ReadOnly _InitializingEventHandlers As New EventHandlerContextCollection(Of System.ComponentModel.CancelEventArgs)

    ''' <summary> Event queue for all listeners interested in Initializing events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Initializing As EventHandler(Of System.ComponentModel.CancelEventArgs)
        AddHandler(value As EventHandler(Of System.ComponentModel.CancelEventArgs))
            Me._InitializingEventHandlers.Add(New EventHandlerContext(Of System.ComponentModel.CancelEventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.ComponentModel.CancelEventArgs))
            Me._InitializingEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.ComponentModel.CancelEventArgs)
            Me._InitializingEventHandlers.Send(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="Initializing">Initializing Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyInitializing(ByVal e As ComponentModel.CancelEventArgs)
        Me._InitializingEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " INITIALIZED "

    ''' <summary> True if is initialized, false if not. </summary>
    Private _IsInitialized As Boolean

    ''' <summary>
    ''' Gets or sets the Initialized sentinel of the device. The device is ready after it is
    ''' initialized.
    ''' </summary>
    ''' <value> <c>True</c> if hardware device is Initialized; <c>False</c> otherwise. </value>
    Public Overridable Property IsInitialized As Boolean
        Get
            Return Me._IsInitialized
        End Get
        Set(ByVal value As Boolean)
            If Not Me.IsInitialized.Equals(value) Then
                Me._IsInitialized = value
                Me.NotifyPropertyChanged()
                Me.PublishInfo($"{Me.ResourceNameCaption} {IIf(Me.IsInitialized, "initialized", "not ready")}")
            End If
        End Set
    End Property

    ''' <summary> Notifies of the Initialized event. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="e"> Event information to send to registered event handlers. </param>
    Protected Overridable Sub OnInitialized(ByVal e As System.EventArgs)
        Me.IsInitialized = True
        Me.SyncNotifyInitialized(e)
    End Sub

    ''' <summary> Removes the Initialized event handlers. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Protected Sub RemoveInitializedEventHandlers()
        Me._InitializedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The Initialized event handlers. </summary>
    Private ReadOnly _InitializedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in Initialized events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event Initialized As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._InitializedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._InitializedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._InitializedEventHandlers.Send(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="Initialized">Initialized Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyInitialized(ByVal e As System.EventArgs)
        Me._InitializedEventHandlers.Send(Me, e)
    End Sub

#End Region

#Region " IDENTITY "

    ''' <summary> The identity. </summary>
    Private _Identity As String = String.Empty

    ''' <summary> Gets or sets the Identity. </summary>
    ''' <value> The Identity. </value>
    Public Property Identity As String
        Get
            Return Me._Identity
        End Get
        Set(value As String)
            If Not String.Equals(Me.Identity, value) Then
                Me._Identity = value
                Me.NotifyPropertyChanged()
            End If
        End Set
    End Property

#End Region

#Region " COMMANDS "

    ''' <summary> Opens a resource. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="resourceName">  The name of the resource. </param>
    ''' <param name="resourceTitle"> The resource title. </param>
    Public Overridable Sub OpenResource(ByVal resourceName As String, ByVal resourceTitle As String)
        If Me.IsOpen Then
            Me.ValidatedResourceName = resourceName
            Me.CandidateResourceNameValidated = String.Equals(Me.ValidatedResourceName, Me.CandidateResourceName, StringComparison.OrdinalIgnoreCase)
            Me.OpenResourceName = resourceName
            Me.OpenResourceTitle = resourceTitle
            Me.CandidateResourceTitle = resourceTitle
        End If
    End Sub

    ''' <summary> Attempts to open resource from the given data. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="resourceName">  The name of the resource. </param>
    ''' <param name="resourceTitle"> The resource title. </param>
    ''' <param name="e">             Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Function TryOpen(ByVal resourceName As String, ByVal resourceTitle As String, ByVal e As ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        Try
            activity = $"opening {resourceTitle}:{resourceName}" : Me.PublishInfo($"{activity};. ")
            Me.OpenResource(resourceName, resourceTitle)
        Catch ex As Exception
            e.RegisterError($"Exception {activity};. {ex.ToFullBlownString}")
            Me.Publish(e)
        Finally
            If Not e.Failed AndAlso Not Me.IsOpen Then
                e.RegisterError($"{activity} failed; resource not signaled as open")
            End If
        End Try
        Return Not e.Failed
    End Function

    ''' <summary> Attempts to open resource from the given data. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="e"> Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Function TryOpen(ByVal e As ActionEventArgs) As Boolean
        Return Me.TryOpen(Me.ValidatedResourceName, Me.CandidateResourceTitle, e)
    End Function

    ''' <summary> Closes the resource. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Public Overridable Sub CloseResource()
    End Sub

    ''' <summary> Attempts to close resource from the given data. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Action event information. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Function TryClose(ByVal e As ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        Try
            activity = $"Closing {Me.ResourceNameCaption}" : Me.PublishInfo($"{activity};. ")
            Me.CloseResource()
        Catch ex As Exception
            e.RegisterError($"Exception {activity};. {ex.ToFullBlownString}")
            Me.Publish(e)
        Finally
            If Not e.Failed AndAlso Me.IsOpen Then
                e.RegisterError($"{activity} failed; resource not signaled as Close")
            End If
        End Try
        Return Not e.Failed
    End Function

    ''' <summary> Applies default settings and clears the resource active state. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Public MustOverride Sub ClearActiveState()

    ''' <summary> Attempts to clear active state. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="e"> Action event information. </param>
    ''' <returns> True if it succeeds, false if it fails. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Overridable Function TryClearActiveState(ByVal e As ActionEventArgs) As Boolean
        If e Is Nothing Then Throw New ArgumentNullException(NameOf(e))
        Dim activity As String = String.Empty
        Try
            activity = $"Clearing {Me.ResourceNameCaption} active state" : Me.PublishInfo($"{activity};. ")
            Me.ClearActiveState()
        Catch ex As Exception
            e.RegisterError($"Exception {activity};. {ex.ToFullBlownString}")
            Me.Publish(e)
        End Try
        Return Not e.Failed
    End Function

#End Region

End Class

