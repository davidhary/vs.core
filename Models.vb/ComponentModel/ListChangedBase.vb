﻿Imports System.ComponentModel
Partial Public Class ListChangedBase

#Region " List changed event handler "

    ''' <summary> Removes the List Changed event handlers. </summary>
    Protected Sub RemoveListChangedEventHandler()
        Me.ListChangedHandlers.RemoveAll()
    End Sub

    Private ListChangedHandlers As New NotifyListChangedEventContextCollection()

    ''' <summary> Event queue for all listeners interested in List Changed events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event ListChanged As ListChangedEventHandler

        AddHandler(ByVal value As ListChangedEventHandler)
            Me.ListChangedHandlers.Add(New NotifyListChangedEventContext(value))
        End AddHandler

        RemoveHandler(ByVal value As ListChangedEventHandler)
            Me.ListChangedHandlers.RemoveValue(value)
        End RemoveHandler

        RaiseEvent(ByVal sender As Object, ByVal e As ListChangedEventArgs)
            Me.ListChangedHandlers.Raise(sender, e)
        End RaiseEvent

    End Event

#End Region

#Region " Notify "

    ''' <summary>
    ''' Synchronously notifies (send) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="e"> List Changed event information. </param>
    Protected Overridable Sub NotifyListChanged(ByVal e As ListChangedEventArgs)
        Me.ListChangedHandlers.Send(Me, e)
    End Sub

    ''' <summary>
    ''' Asynchronously notifies (posts) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 7 to 10 times larger than naked raise event. This has no
    ''' advantage even with slow handler functions.
    ''' </remarks>
    ''' <param name="e"> List Changed event information. </param>
    Protected Overridable Sub AsyncNotifyListChanged(ByVal e As ListChangedEventArgs)
        Me.ListChangedHandlers.Post(Me, e)
    End Sub

    ''' <summary>
    ''' Synchronously notifies (send) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 3 to 4 times larger than naked raise event. This is the best approach.
    ''' </remarks>
    ''' <param name="e"> List Changed event information. </param>
    Protected Overridable Sub SyncNotifyListChanged(ByVal e As ListChangedEventArgs)
        Me.ListChangedHandlers.Send(Me, e)
    End Sub

#End Region

End Class
