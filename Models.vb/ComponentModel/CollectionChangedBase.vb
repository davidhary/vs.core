Imports System.ComponentModel
Imports System.Collections.Specialized
Imports isr.Core.Services

''' <summary> A collection changed base. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/22/2019 </para>
''' </remarks>
Public Class CollectionChangedBase
    Inherits ViewModelBase
    Implements INotifyCollectionChanged, INotifyPropertyChanged

    ''' <summary> Removes the Collection Changed event handlers. </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    Protected Sub RemoveCollectionChangedEventHandler()
        Me.CollectionChangedHandlers.RemoveAll()
    End Sub

    ''' <summary> The collection changed handlers. </summary>
    Private ReadOnly CollectionChangedHandlers As New NotifyCollectionChangedEventContextCollection()

    ''' <summary> Event queue for all listeners interested in Collection Changed events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event CollectionChanged As NotifyCollectionChangedEventHandler Implements INotifyCollectionChanged.CollectionChanged

        AddHandler(ByVal value As NotifyCollectionChangedEventHandler)
            Me.CollectionChangedHandlers.Add(New NotifyCollectionChangedEventContext(value))
        End AddHandler

        RemoveHandler(ByVal value As NotifyCollectionChangedEventHandler)
            Me.CollectionChangedHandlers.RemoveValue(value)
        End RemoveHandler

        RaiseEvent(ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
            Me.CollectionChangedHandlers.Raise(sender, e)
        End RaiseEvent

    End Event

#Region " NOTIFY "

    ''' <summary>
    ''' Asynchronously notifies (posts) collection change on a different thread. Unsafe for cross
    ''' threading; fast return of control to the invoking function.
    ''' </summary>
    ''' <remarks> David, 2020-09-21. </remarks>
    ''' <param name="e"> Collection Changed event information. </param>
    Protected Overridable Sub NotifyCollectionChanged(ByVal e As NotifyCollectionChangedEventArgs)
        Me.CollectionChangedHandlers.Post(Me, e)
    End Sub

    ''' <summary>
    ''' Asynchronously notifies (posts) collection change on a different thread. Unsafe for cross
    ''' threading; fast return of control to the invoking function.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
    ''' even with slow handler functions.
    ''' </remarks>
    ''' <param name="e"> Collection Changed event information. </param>
    Protected Overridable Sub AsyncNotifyCollectionChanged(ByVal e As NotifyCollectionChangedEventArgs)
        Me.CollectionChangedHandlers.Post(Me, e)
    End Sub

    ''' <summary>
    ''' Synchronously notifies (send) collection change on a different thread. Safe for cross
    ''' threading.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 3 to 4 times larger than naked raise event. This is the best
    ''' approach.
    ''' </remarks>
    ''' <param name="e"> Collection Changed event information. </param>
    Protected Overridable Sub SyncNotifyCollectionChanged(ByVal e As NotifyCollectionChangedEventArgs)
        Me.CollectionChangedHandlers.Send(Me, e)
    End Sub

#End Region

End Class
