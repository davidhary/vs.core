﻿Imports System.ComponentModel
Partial Public Class AddingNewBase

#Region " Adding New event handler "

    ''' <summary> Removes the Adding New event handlers. </summary>
    Protected Sub RemoveAddingNewEventHandler()
        Me.AddingNewHandlers.RemoveAll()
    End Sub

    Private AddingNewHandlers As New NotifyAddingNewEventContextCollection()

    ''' <summary> Event queue for all listeners interested in Adding New events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event AddingNew As AddingNewEventHandler

        AddHandler(ByVal value As AddingNewEventHandler)
            Me.AddingNewHandlers.Add(New NotifyAddingNewEventContext(value))
        End AddHandler

        RemoveHandler(ByVal value As AddingNewEventHandler)
            Me.AddingNewHandlers.RemoveValue(value)
        End RemoveHandler

        RaiseEvent(ByVal sender As Object, ByVal e As AddingNewEventArgs)
            Me.AddingNewHandlers.Raise(sender, e)
        End RaiseEvent

    End Event

#End Region

#Region " Notify "

    ''' <summary>
    ''' Synchronously notifies (send) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <param name="e"> Adding New event information. </param>
    Protected Overridable Sub NotifyAddingNew(ByVal e As AddingNewEventArgs)
        Me.AddingNewHandlers.Send(Me, e)
    End Sub

    ''' <summary>
    ''' Asynchronously notifies (posts) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 7 to 10 times larger than naked raise event. This has no
    ''' advantage even with slow handler functions.
    ''' </remarks>
    ''' <param name="e"> Adding New event information. </param>
    Protected Overridable Sub AsyncNotifyAddingNew(ByVal e As AddingNewEventArgs)
        Me.AddingNewHandlers.Post(Me, e)
    End Sub

    ''' <summary>
    ''' Synchronously notifies (send) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 3 to 4 times larger than naked raise event. This is the best approach.
    ''' </remarks>
    ''' <param name="e"> Adding New event information. </param>
    Protected Overridable Sub SyncNotifyAddingNew(ByVal e As AddingNewEventArgs)
        Me.AddingNewHandlers.Send(Me, e)
    End Sub

#End Region

End Class
