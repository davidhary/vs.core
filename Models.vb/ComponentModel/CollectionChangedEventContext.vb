﻿Imports System.Threading
Imports System.Collections.Specialized

''' <summary> A notify collection changed event context. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/17/2019 </para></remarks>
Public Class NotifyCollectionChangedEventContext

    ''' <summary> Constructor. </summary>
    ''' <param name="handler"> The handler. </param>
    Public Sub New(ByVal handler As NotifyCollectionChangedEventHandler)
        Me.Handler = handler
        Me.Context = SynchronizationContext.Current
    End Sub

    ''' <summary> Gets or sets the synchronization context. </summary>
    ''' <value> The context. </value>
    Private ReadOnly Property Context As SynchronizationContext

    ''' <summary> Gets or sets the handler. </summary>
    ''' <value> The handler. </value>
    Public ReadOnly Property Handler As NotifyCollectionChangedEventHandler

#Region " INVOKE "

    ''' <summary>
    ''' Executes the given operation directly irrespective of the <see cref="Context"/>.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Collection Changed event information. </param>
    Public Sub UnsafeInvoke(ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
        Dim evt As NotifyCollectionChangedEventHandler = Me.Handler
        evt?.Invoke(sender, e)
    End Sub

#End Region

#Region " ACTIVE CONTEXT "

    ''' <summary> Returns the current synchronization context. </summary>
    ''' <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is null. </exception>
    ''' <returns> A Threading.SynchronizationContext. </returns>
    Private Shared Function CurrentSyncContext() As Threading.SynchronizationContext
        If Threading.SynchronizationContext.Current Is Nothing Then
            Threading.SynchronizationContext.SetSynchronizationContext(New Threading.SynchronizationContext)
        End If
        If Threading.SynchronizationContext.Current Is Nothing Then
            Throw New InvalidOperationException("Current Synchronization Context not set;. Must be set before starting the thread.")
        End If
        Return Threading.SynchronizationContext.Current
    End Function

    ''' <summary> Gets a context for the active. </summary>
    ''' <value> The active context. </value>
    Private ReadOnly Property ActiveContext As SynchronizationContext
        Get
            Return If(Me.Context, NotifyCollectionChangedEventContext.CurrentSyncContext)
        End Get
    End Property

#End Region

#Region " SEND POST "

    ''' <summary>
    ''' Asynchronously raises (Posts) the <see cref="NotifyCollectionChangedEventHandler"/>. It does all the checking to see
    ''' if the SynchronizationContext is nothing or not, and invokes the delegate accordingly.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Collection Changed event information. </param>
    Public Sub Post(ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
        Dim evt As NotifyCollectionChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then Me.ActiveContext.Post(Sub() evt(sender, e), Nothing)
    End Sub

    ''' <summary>
    ''' Synchronously raises (sends) the <see cref="NotifyCollectionChangedEventHandler"/>. It does all the checking to see
    ''' if the SynchronizationContext is nothing or not, and invokes the delegate accordingly.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Collection Changed event information. </param>
    Public Sub Send(ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
        Dim evt As NotifyCollectionChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then Me.ActiveContext.Send(Sub() evt(sender, e), Nothing)
    End Sub

#End Region

End Class

''' <summary> Collection of Notify Collection Changed event contexts. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/11/2018 </para></remarks>
Public Class NotifyCollectionChangedEventContextCollection
    Inherits List(Of NotifyCollectionChangedEventContext)

    ''' <summary>
    ''' Executes the given operation directly irrespective of the <see cref="SynchronizationContext"/>.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Collection Changed event information. </param>
    Public Sub UnsafeInvoke(ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
        For Each evt As NotifyCollectionChangedEventContext In Me : evt?.UnsafeInvoke(sender, e) : Next
    End Sub

    ''' <summary> Executes (sends) the event handler action synchronously (thread safe). </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Collection Changed event information. </param>
    Public Sub Raise(ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
        For Each evt As NotifyCollectionChangedEventContext In Me : evt?.Send(sender, e) : Next
    End Sub

    ''' <summary> Executes (posts) the event handler action asynchronously (thread unsafe). </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Collection Changed event information. </param>
    Public Sub Post(ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
        For Each evt As NotifyCollectionChangedEventContext In Me : evt?.Post(sender, e) : Next
    End Sub

    ''' <summary> Executes (sends) the event handler action synchronously (thread safe). </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Collection Changed event information. </param>
    Public Sub Send(ByVal sender As Object, ByVal e As NotifyCollectionChangedEventArgs)
        For Each evt As NotifyCollectionChangedEventContext In Me : evt?.Send(sender, e) : Next
    End Sub

    ''' <summary> Looks up a given key to find its associated last index. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> An Integer. </returns>
    Private Function LookupLastIndex(ByVal value As NotifyCollectionChangedEventHandler) As Integer
        Return Me.ToList.FindLastIndex(Function(x) x.Handler.Equals(value))
    End Function

    ''' <summary> Removes the value described by value. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub RemoveValue(ByVal value As NotifyCollectionChangedEventHandler)
        If Me.Any Then
            Dim lastIndex As Integer = Me.LookupLastIndex(value)
            If lastIndex >= 0 AndAlso lastIndex < Me.Count AndAlso Me.ElementAt(lastIndex).Handler.Equals(value) Then Me.RemoveAt(lastIndex)
        End If
    End Sub

    ''' <summary> Removes all. </summary>
    Public Overloads Sub RemoveAll()
        Do While Me.Count > 0
            Me.RemoveAt(0)
        Loop
    End Sub

End Class

