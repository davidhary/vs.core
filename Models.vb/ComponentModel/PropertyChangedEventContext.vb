﻿Imports System.ComponentModel
Imports System.Threading

''' <summary> A property changed event context. </summary>
''' <remarks>
''' The <see cref="PropertyChangedEventContext"/> and the custom
''' <see cref="PropertyChangedEventHandler"/> make the PropertyChanged event fire on the
''' SynchronizationContext of the listening code. <para>
''' The PropertyChanged event declaration is modified to a custom event handler. </para><para>
''' As each handler may have a different context, the AddHandler block stores the handler being
''' passed in as well as the SynchronizationContext to be used later when raising the event.
''' .</para> <para>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para><para>
''' David, 10/01/2009" by="Bill McCarthy"> https://visualstudiomagazine.com/Articles/2009/10/01/Threading-and-the-UI.aspx?Page=2. </para></remarks>
Public Class PropertyChangedEventContext

    ''' <summary> Constructor. </summary>
    ''' <param name="handler"> The handler. </param>

    Public Sub New(ByVal handler As PropertyChangedEventHandler)
        Me.Handler = handler
        Me.Context = SynchronizationContext.Current
    End Sub

    ''' <summary> Gets or sets the synchronization context. </summary>
    ''' <value> The context. </value>
    Private ReadOnly Property Context As SynchronizationContext

    ''' <summary> Gets or sets the handler. </summary>
    ''' <value> The handler. </value>
    Public ReadOnly Property Handler As PropertyChangedEventHandler

#Region " INVOKE "

    ''' <summary>
    ''' Executes the given operation directly irrespective of the <see cref="Context"/>.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub UnsafeInvoke(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.Handler
        evt?.Invoke(sender, e)
    End Sub

#End Region

End Class

''' <summary> Collection of property change event contexts. </summary>
''' <remarks> (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/11/2018 </para></remarks>
Public Class PropertyChangeEventContextCollection
    Inherits List(Of PropertyChangedEventContext)

    ''' <summary>
    ''' Executes the given operation directly irrespective of the <see cref="SynchronizationContext"/>.
    ''' </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub UnsafeInvoke(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        For Each evt As PropertyChangedEventContext In Me : evt?.UnsafeInvoke(sender, e) : Next
    End Sub

    ''' <summary> Executes (sends) the event handler action synchronously (thread safe). </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub Raise(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        For Each evt As PropertyChangedEventContext In Me : evt?.Send(sender, e) : Next
    End Sub

    ''' <summary> Executes (posts) the event handler action. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub Post(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        For Each evt As PropertyChangedEventContext In Me : evt?.Post(sender, e) : Next
    End Sub

    ''' <summary> Executes (sends) the event handler action synchronously (thread safe). </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub Send(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        For Each evt As PropertyChangedEventContext In Me : evt?.Send(sender, e) : Next
    End Sub

    ''' <summary> Posts a dynamic invoke of the event delegate. </summary>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub PostDynamicInvoke(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        For Each evt As PropertyChangedEventContext In Me : evt?.PostDynamicInvoke(sender, e) : Next
    End Sub

    ''' <summary> Looks up a given key to find its associated last index. </summary>
    ''' <param name="value"> The value. </param>
    ''' <returns> An Integer. </returns>
    Private Function LookupLastIndex(ByVal value As PropertyChangedEventHandler) As Integer
        Return Me.FindLastIndex(Function(x) x.Handler.Equals(value))
    End Function

    ''' <summary> Removes the value described by value. </summary>
    ''' <param name="value"> The value. </param>
    Public Sub RemoveValue(ByVal value As PropertyChangedEventHandler)
        If Me.Any Then
            Dim lastIndex As Integer = Me.LookupLastIndex(value)
            If lastIndex >= 0 AndAlso lastIndex < Me.Count AndAlso Me.ElementAt(lastIndex).Handler.Equals(value) Then Me.RemoveAt(lastIndex)
        End If
    End Sub

    ''' <summary> Removes all. </summary>
    Public Overloads Sub RemoveAll()
        Do While Me.Count > 0
            Me.RemoveAt(0)
        Loop
    End Sub

End Class

