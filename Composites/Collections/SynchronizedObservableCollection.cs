using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading;

using isr.Core.Concurrent;
using isr.Core.Models;

namespace isr.Core.Composites
{

    /// <summary> A synchronized observable collection. </summary>
    /// <remarks> David, 2017-05-02. TO_DO: Modify based on the Observable Keyed Collection. </remarks>
    [DebuggerDisplay( "Count = {Count}" )]
    public class SynchronizedObservableCollection<T> : CollectionChangedBase, IDisposable, IList<T>, IList, IReadOnlyList<T>, INotifyCollectionChanged, INotifyPropertyChanged
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public SynchronizedObservableCollection() : base()
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="items"> Items to add to the collection. </param>
        public SynchronizedObservableCollection( IList<T> items ) : this()
        {
            if ( items is null )
            {
                throw new ArgumentNullException( nameof( items ), "'collection' cannot be null" );
            }

            foreach ( T item in items )
            {
                this.Items.Add( item );
            }
        }

        /// <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
        /// <remarks>
        /// Do not make this method Overridable (virtual) because a derived class should not be able to
        /// override this method.
        /// </remarks>
        public void Dispose()
        {

            // Do not change this code.  Put cleanup code in Dispose(Boolean) below.

            // this disposes all child classes.
            this.Dispose( true );

            // Take this object off the finalization(Queue) and prevent finalization code 
            // from executing a second time.
            GC.SuppressFinalize( this );
        }

        /// <summary>
        /// Gets the dispose status sentinel of the base class.  This applies to the derived class
        /// provided proper implementation.
        /// </summary>
        /// <value> <c>True</c> if disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Cleans up unmanaged or managed and unmanaged resources. </summary>
        /// <remarks>
        /// Executes in two distinct scenarios as determined by its disposing parameter:<para>
        /// If True, the method has been called directly or indirectly by a user's code--managed and
        /// unmanaged resources can be disposed.</para><para>
        /// If False, the method has been called by the runtime from inside the finalizer and you should
        /// not reference other objects--only unmanaged resources can be disposed.</para>
        /// </remarks>
        /// <param name="disposing"> <c>True</c> if this method releases both managed and unmanaged
        /// resources;
        /// False if this
        /// method releases
        /// only unmanaged
        /// resources. </param>
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( this.ItemsLocker is object )
                    {
                        this.ItemsLocker.Dispose();
                        this.ItemsLocker = null;
                    }
                }
            }
            finally
            {
                // set the sentinel indicating that the class was disposed.
                this.IsDisposed = true;
            }
        }

        #endregion

        #region " NOTIFICATIONS "

        #region " COLLECTION CHANGE NOTIFICATIONS "

        /// <summary> Raises the notify collection changed event. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="action"> The action. </param>
        /// <param name="item">   The object to add to the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        /// <param name="index">  Zero-based index of the. </param>
        private void OnCollectionChanged( NotifyCollectionChangedAction action, object item, int index )
        {
            this.OnCollectionChanged( new NotifyCollectionChangedEventArgs( action, item, index ) );
        }


        /// <summary> Raises the notify collection changed event. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="action">   The action. </param>
        /// <param name="item">     The object to add to the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        /// <param name="index">    Zero-based index of the. </param>
        /// <param name="oldIndex"> The old index. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private void OnCollectionChanged( NotifyCollectionChangedAction action, object item, int index, int oldIndex )
        {
            this.OnCollectionChanged( new NotifyCollectionChangedEventArgs( action, item, index, oldIndex ) );
        }

        /// <summary> Raises the notify collection changed event. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="action">  The action. </param>
        /// <param name="oldItem"> The old item. </param>
        /// <param name="newItem"> The new item. </param>
        /// <param name="index">   Zero-based index of the. </param>
        private void OnCollectionChanged( NotifyCollectionChangedAction action, object oldItem, object newItem, int index )
        {
            this.OnCollectionChanged( new NotifyCollectionChangedEventArgs( action, newItem, oldItem, index ) );
        }

        /// <summary> Raises the notify collection changed event. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnCollectionChanged( NotifyCollectionChangedEventArgs e )
        {
            using ( this.BlockReentrancy() )
            {
                this.NotifyCollectionChanged( e );
            }
        }

        /// <summary> Executes the 'collection reset' action. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        private void OnCollectionReset()
        {
            this.OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Reset ) );
        }

        #endregion

        #endregion

        #region " THREAD SYNC MANAGEMENT  "

        /// <summary> Gets the items locker. </summary>
        /// <value> The items locker. </value>
        private ReaderWriterLockSlim ItemsLocker { get; set; } = new ReaderWriterLockSlim();

        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" />
        /// is synchronized (thread safe).
        /// </summary>
        /// <value>
        /// <see langword="true" /> if access to the <see cref="T:System.Collections.ICollection" /> is
        /// synchronized (thread safe); otherwise, <see langword="false" />.
        /// </value>
        bool ICollection.IsSynchronized => true;

        [NonSerialized]
        private object _SyncRoot;

        /// <summary>
        /// Gets an object that can be used to synchronize access to the
        /// <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        /// <value>
        /// An object that can be used to synchronize access to the
        /// <see cref="T:System.Collections.ICollection" />.
        /// </value>
        object ICollection.SyncRoot
        {
            get {
                if ( this._SyncRoot is null )
                {
                    this.ItemsLocker.EnterReadLock();
                    try
                    {
                        if ( this.Items is ICollection c )
                        {
                            this._SyncRoot = c.SyncRoot;
                        }
                        else
                        {
                            _ = Interlocked.CompareExchange<object>( ref this._SyncRoot, new object(), null );
                        }
                    }
                    finally
                    {
                        this.ItemsLocker.ExitReadLock();
                    }
                }

                return this._SyncRoot;
            }
        }

        #region " CHECK RE-RENTY and VALIDATION METHODS "

        /// <summary> Gets the monitor. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The monitor. </value>
        private ThreadSafeMonitor Monitor { get; set; } = new ThreadSafeMonitor();

        /// <summary> Blocks reentrancy. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> An IDisposable. </returns>
        private IDisposable BlockReentrancy()
        {
            this.Monitor.Enter();
            return this.Monitor;
        }

        /// <summary> Check reentrancy. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        private void CheckReentrancy()
        {
            if ( this.Monitor.Busy() )
            {
                throw new InvalidOperationException( "Reentrancy not allowed" );
            }
        }

        #endregion

        #endregion

        #region " ITEMS AS T "

        /// <summary> Gets the items. </summary>
        /// <value> The items. </value>
        private IList<T> Items { get; set; } = new List<T>();

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> has a fixed
        /// size.
        /// </summary>
        /// <value>
        /// <see langword="true" /> if the <see cref="T:System.Collections.IList" /> has a fixed size;
        /// otherwise, <see langword="false" />.
        /// </value>
        bool IList.IsFixedSize => this.Items is IList list ? list.IsFixedSize : this.Items.IsReadOnly;

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> is read-only.
        /// </summary>
        /// <value>
        /// <see langword="true" /> if the <see cref="T:System.Collections.IList" /> is read-only;
        /// otherwise, <see langword="false" />.
        /// </value>
        public bool IsReadOnly => this.Items.IsReadOnly;

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.IList" /> is read-only.
        /// </summary>
        /// <value>
        /// <see langword="true" /> if the <see cref="T:System.Collections.IList" /> is read-only;
        /// otherwise, <see langword="false" />.
        /// </value>
        bool IList.IsReadOnly => this.IsReadOnly;

        #endregion

        #region " LIST, LIST OF T and READ ONLY LIST OF T IMPLEMENTATION  "

        /// <summary> Check index. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="index"> Zero-based index of the. </param>
        private void CheckIndex( int index )
        {
            if ( index < 0 || index >= this.Items.Count )
            {
                throw new ArgumentOutOfRangeException( nameof( index ), FormattableString.Invariant( $"Must be between 0 and {this.Items.Count}" ) );
            }
        }

        /// <summary> Gets the number of elements. </summary>
        /// <value> The count. </value>
        public int Count
        {
            get {
                this.ItemsLocker.EnterReadLock();
                try
                {
                    return this.Items.Count;
                }
                finally
                {
                    this.ItemsLocker.ExitReadLock();
                }
            }
        }

        /// <summary>   Gets or sets the element at the specified index. </summary>
        /// <param name="index">    The zero-based index of the element to get or set. </param>
        /// <returns>   The element at the specified index. </returns>
        public T this[int index]
        {
            get => this.GetItemThis( index );
            set => this.SetItemThis( index, value );
        }

        /// <summary> Gets or sets the element at the specified index. </summary>
        /// <value> The element at the specified index. </value>
        private T GetItemThis( int index )
        {
            this.ItemsLocker.EnterReadLock();
            try
            {
                this.CheckIndex( index );
                return this.Items[index];
            }
            finally
            {
                this.ItemsLocker.ExitReadLock();
            }
        }

        private void SetItemThis( int index, T value )
        {
            T oldValue;
            this.ItemsLocker.EnterWriteLock();
            try
            {
                this.CheckIsReadOnly();
                this.CheckIndex( index );
                this.CheckReentrancy();
                oldValue = this.ElementAtOrDefault( index );
                this.Items[index] = value;
            }
            finally
            {
                this.ItemsLocker.ExitWriteLock();
            }
            this.NotifyPropertyChanged( "Item" );
            this.OnCollectionChanged( NotifyCollectionChangedAction.Replace, ( object ) oldValue, ( object ) value, index );
        }

        /// <summary>   Gets or sets the element at the specified index. </summary>
        /// <param name="index">    The zero-based index of the element to get or set. </param>
        /// <returns>   The element at the specified index. </returns>
        object IList.this[int index] { get => this.GetItemThis( index ); set => this.SetItemThis( index, ( T ) value ); }

        /// <summary> Check is read only. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
        private void CheckIsReadOnly()
        {
            if ( this.Items.IsReadOnly )
            {
                throw new NotSupportedException( "Collection is read only" );
            }
        }

        /// <summary> Query if 'value' is compatible object. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> <c>true</c> if compatible object; otherwise <c>false</c> </returns>
        private static bool IsCompatibleObject( object value )
        {
            // Non-null values are fine.  Only accept nulls if T is a class or Nullable<U>.
            // Note that default(T) is not equal to null for value types except when T is Nullable<U>. 
            return value is T || value is null;
        }

        #endregion

        #region " PUBLIC METHODS "

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="item"> The object to add to the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        public void Add( T item )
        {
            this.ItemsLocker.EnterWriteLock();
            int index = -1;
            try
            {
                this.CheckIsReadOnly();
                this.CheckReentrancy();
                index = this.Items.Count;
                this.Items.Insert( index, item );
            }
            finally
            {
                this.ItemsLocker.ExitWriteLock();
            }

            this.NotifyPropertyChanged( nameof( SynchronizedObservableCollection<T>.Count ) );
            this.NotifyPropertyChanged( "Item" );
            this.OnCollectionChanged( NotifyCollectionChangedAction.Add, ( object ) item, index );
        }

        /// <summary> Adds an item to the <see cref="T:System.Collections.IList" />. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="value"> The object to add to the <see cref="T:System.Collections.IList" />. </param>
        /// <returns>
        /// The position into which the new element was inserted, or -1 to indicate that the item was not
        /// inserted into the collection.
        /// </returns>
        int IList.Add( object value )
        {
            this.ItemsLocker.EnterWriteLock();
            int index = -1;
            T item;
            try
            {
                this.CheckIsReadOnly();
                this.CheckReentrancy();
                index = this.Items.Count;
                item = ( T ) value;
                this.Items.Insert( index, item );
            }
            catch ( InvalidCastException ex )
            {
                throw new ArgumentException( FormattableString.Invariant( $"'{nameof( value )}' is the wrong type" ), ex );
            }
            finally
            {
                this.ItemsLocker.ExitWriteLock();
            }

            this.NotifyPropertyChanged( nameof( SynchronizedObservableCollection<T>.Count ) );
            this.NotifyPropertyChanged( "Item" );
            this.OnCollectionChanged( NotifyCollectionChangedAction.Add, ( object ) item, index );
            return index;
        }

        /// <summary> Removes all items from the <see cref="T:System.Collections.IList" />. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public void Clear()
        {
            this.ItemsLocker.EnterWriteLock();
            try
            {
                this.CheckIsReadOnly();
                this.CheckReentrancy();
                this.Items.Clear();
            }
            finally
            {
                this.ItemsLocker.ExitWriteLock();
            }

            this.NotifyPropertyChanged( nameof( SynchronizedObservableCollection<T>.Count ) );
            this.NotifyPropertyChanged( "Item" );
            this.OnCollectionReset();
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an
        /// <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="array">      The one-dimensional <see cref="T:System.Array" /> that is the
        /// destination of the elements copied from
        /// <see cref="T:System.Collections.Generic.ICollection`1" />. The
        /// <see cref="T:System.Array" /> must have zero-based indexing. </param>
        /// <param name="arrayIndex"> The zero-based index in <paramref name="array" /> at which copying
        /// begins. </param>
        public void CopyTo( T[] array, int arrayIndex )
        {
            this.ItemsLocker.EnterReadLock();
            try
            {
                this.Items.CopyTo( array, arrayIndex );
            }
            finally
            {
                this.ItemsLocker.ExitReadLock();
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an
        /// <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentException">           Thrown when one or more arguments have
        /// unsupported or illegal values. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <exception cref="ArrayTypeMismatchException">  Thrown when an attempt is made to store an
        /// object of the wrong type in the array.
        /// </exception>
        /// <param name="array"> The one-dimensional <see cref="T:System.Array" /> that is the destination
        /// of the elements copied from
        /// <see cref="T:System.Collections.ICollection" />. The
        /// <see cref="T:System.Array" /> must have zero-based indexing. </param>
        /// <param name="index"> The zero-based index in <paramref name="array" /> at which copying
        /// begins. </param>
        void ICollection.CopyTo( Array array, int index )
        {
            this.ItemsLocker.EnterReadLock();
            try
            {
                if ( array is null )
                {
                    throw new ArgumentNullException( nameof( array ), FormattableString.Invariant( $"'{nameof( array )}' cannot be null" ) );
                }

                if ( array.Rank != 1 )
                {
                    throw new ArgumentException( "Multidimensional arrays are not supported", nameof( array ) );
                }

                if ( array.GetLowerBound( 0 ) != 0 )
                {
                    throw new ArgumentException( "Non-zero lower bound arrays are not supported", nameof( array ) );
                }

                if ( index < 0 )
                {
                    throw new ArgumentOutOfRangeException( nameof( index ), FormattableString.Invariant( $"'{nameof( index )}' is out of range" ) );
                }

                if ( array.Length - index < this.Items.Count )
                {
                    throw new ArgumentException( "Array is too small" );
                }

                if ( array is T[] tArray )
                {
                    this.Items.CopyTo( tArray, index );
                }
                else
                {
                    // 
                    // Catch the obvious case assignment will fail.
                    // We can found all possible problems by doing the check though.
                    // For example, if the element type of the Array is derived from T,
                    // we can't figure out if we can successfully copy the element beforehand.
                    // 
                    var targetType = array.GetType().GetElementType();
                    var sourceType = typeof( T );
                    if ( !(targetType.IsAssignableFrom( sourceType ) || sourceType.IsAssignableFrom( targetType )) )
                    {
                        throw new ArrayTypeMismatchException( "Invalid array type" );
                    }

                    // 
                    // We can't cast array of value type to object[], so we don't support 
                    // widening of primitive types here.
                    // 
                    if ( array is not object[] objects )
                    {
                        throw new ArrayTypeMismatchException( "Invalid array type" );
                    }

                    int count = this.Items.Count;
                    try
                    {
                        int i = 0;
                        while ( i < count )
                        {
                            index += 1;
                            objects[index] = this.Items[i];
                            i += 1;
                        }
                    }
                    catch ( ArrayTypeMismatchException ex )
                    {
                        throw new ArrayTypeMismatchException( "Invalid array type", ex );
                    }
                }
            }
            finally
            {
                this.ItemsLocker.ExitReadLock();
            }
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a
        /// specific value.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="item"> The object to locate in the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="item" /> is found in the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public bool Contains( T item )
        {
            this.ItemsLocker.EnterReadLock();
            try
            {
                return this.Items.Contains( item );
            }
            finally
            {
                this.ItemsLocker.ExitReadLock();
            }
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.IList" /> contains a specific value.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="value"> The object to locate in the <see cref="T:System.Collections.IList" />. </param>
        /// <returns>
        /// <see langword="true" /> if the <see cref="T:System.Object" /> is found in the
        /// <see cref="T:System.Collections.IList" />; otherwise, <see langword="false" />.
        /// </returns>
        bool IList.Contains( object value )
        {
            if ( IsCompatibleObject( value ) )
            {
                this.ItemsLocker.EnterReadLock();
                try
                {
                    return this.Items.Contains( ( T ) value );
                }
                finally
                {
                    this.ItemsLocker.ExitReadLock();
                }
            }

            return false;
        }

        /// <summary> Returns an enumerator that iterates through the collection. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> An enumerator that can be used to iterate through the collection. </returns>
        public IEnumerator<T> GetEnumerator()
        {
            this.ItemsLocker.EnterReadLock();
            try
            {
                return this.Items.ToList().GetEnumerator();
            }
            finally
            {
                this.ItemsLocker.ExitReadLock();
            }
        }

        /// <summary> Gets the enumerator. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> The enumerator. </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            this.ItemsLocker.EnterReadLock();
            try
            {
                return (( IEnumerable ) this.Items.ToList()).GetEnumerator();
            }
            finally
            {
                this.ItemsLocker.ExitReadLock();
            }
        }

        /// <summary>
        /// Determines the index of a specific item in the
        /// <see cref="T:System.Collections.Generic.IList`1" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="item"> The object to locate in the
        /// <see cref="T:System.Collections.Generic.IList`1" />. </param>
        /// <returns> The index of <paramref name="item" /> if found in the list; otherwise, -1. </returns>
        public int IndexOf( T item )
        {
            this.ItemsLocker.EnterReadLock();
            try
            {
                return this.Items.IndexOf( item );
            }
            finally
            {
                this.ItemsLocker.ExitReadLock();
            }
        }

        /// <summary>
        /// Determines the index of a specific item in the <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="value"> The object to locate in the <see cref="T:System.Collections.IList" />. </param>
        /// <returns>
        /// The index of <paramref name="value" /> if found in the list; otherwise, -1.
        /// </returns>
        int IList.IndexOf( object value )
        {
            if ( IsCompatibleObject( value ) )
            {
                this.ItemsLocker.EnterReadLock();
                try
                {
                    return this.Items.IndexOf( ( T ) value );
                }
                finally
                {
                    this.ItemsLocker.ExitReadLock();
                }
            }

            return -1;
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.Generic.IList`1" /> at the specified
        /// index.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="index"> The zero-based index at which <paramref name="item" /> should be
        /// inserted. </param>
        /// <param name="item">  The object to insert into the
        /// <see cref="T:System.Collections.Generic.IList`1" />. </param>
        public void Insert( int index, T item )
        {
            this.ItemsLocker.EnterWriteLock();
            try
            {
                this.CheckIsReadOnly();
                if ( index < 0 || index >= this.Items.Count )
                {
                    throw new ArgumentOutOfRangeException( nameof( index ), FormattableString.Invariant( $"Must be between {0} and {this.Items.Count}" ) );
                }

                this.CheckReentrancy();
                this.Items.Insert( index, item );
            }
            finally
            {
                this.ItemsLocker.ExitWriteLock();
            }

            this.NotifyPropertyChanged( nameof( SynchronizedObservableCollection<T>.Count ) );
            this.NotifyPropertyChanged( "Item" );
            this.OnCollectionChanged( NotifyCollectionChangedAction.Add, ( object ) item, index );
        }

        /// <summary>
        /// Inserts an item to the <see cref="T:System.Collections.IList" /> at the specified index.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="index"> The zero-based index at which <paramref name="value" /> should be
        /// inserted. </param>
        /// <param name="value"> The object to insert into the <see cref="T:System.Collections.IList" />. </param>
        void IList.Insert( int index, object value )
        {
            try
            {
                this.Insert( index, ( T ) value );
            }
            catch ( InvalidCastException ex )
            {
                throw new ArgumentException( FormattableString.Invariant( $"'{nameof( value )}' is the wrong type" ), ex );
            }
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="item"> The object to remove from the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="item" /> was successfully removed from the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
        /// <see langword="false" />. This method also returns <see langword="false" /> if
        /// <paramref name="item" /> is not found in the original
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove( T item )
        {
            int index;
            T value;
            this.ItemsLocker.EnterWriteLock();
            try
            {
                this.CheckIsReadOnly();
                this.CheckReentrancy();
                index = this.Items.IndexOf( item );
                if ( index < 0 )
                {
                    return false;
                }

                value = this.Items[index];
                this.Items.RemoveAt( index );
            }
            finally
            {
                this.ItemsLocker.ExitWriteLock();
            }
            this.NotifyPropertyChanged( nameof( SynchronizedObservableCollection<T>.Count ) );
            this.NotifyPropertyChanged( "Item" );
            this.OnCollectionChanged( NotifyCollectionChangedAction.Remove, ( object ) value, index );
            return true;
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the
        /// <see cref="T:System.Collections.IList" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="value"> The object to remove from the <see cref="T:System.Collections.IList" />. </param>
        void IList.Remove( object value )
        {
            if ( IsCompatibleObject( value ) )
            {
                _ = this.Remove( ( T ) value );
            }
        }

        /// <summary>
        /// Removes the <see cref="T:System.Collections.Generic.IList`1" /> item at the specified index.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="index"> The zero-based index of the item to remove. </param>
        public void RemoveAt( int index )
        {
            T value;
            this.ItemsLocker.EnterWriteLock();
            try
            {
                this.CheckIsReadOnly();
                this.CheckIndex( index );
                this.CheckReentrancy();
                value = this.Items[index];
                this.Items.RemoveAt( index );
            }
            finally
            {
                this.ItemsLocker.ExitWriteLock();
            }
            this.NotifyPropertyChanged( nameof( SynchronizedObservableCollection<T>.Count ) );
            this.NotifyPropertyChanged( "Item" );
            this.OnCollectionChanged( NotifyCollectionChangedAction.Remove, ( object ) value, index );
        }
        #endregion

    }
}
