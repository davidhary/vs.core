using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;

namespace isr.Core.Composites
{

    /// <summary> Implements an observable keyed collection. </summary>
    /// <remarks>
    /// David, 2017-05-02  <para>
    /// (c) 2011 MULJADI BUDIMAN. All rights reserved. </para><para>
    /// http://geekswithblogs.net/NewThingsILearned/archive/2010/01/12/make-keyedcollectionlttkey-titemgt-to-work-properly-with-wpf-data-binding.aspx.
    /// </para>
    /// </remarks>
    public abstract class ObservableKeyedCollection<TKey, TItem> : KeyedCollection<TKey, TItem>, INotifyCollectionChanged
    {

        #region " COLLECTION IMPLEMENTATION "

        /// <summary> Replaces the item at the specified index with the specified item. </summary>
        /// <remarks> Overrides and implements collection change notification. </remarks>
        /// <param name="index"> The zero-based index of the item to be replaced. </param>
        /// <param name="item">  The new item. </param>
        protected override void SetItem( int index, TItem item )
        {
            var oldItem = this.Items[index];
            base.SetItem( index, item );
            this.OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Replace, item, oldItem, index ) );
        }

        /// <summary>
        /// Inserts an element into the <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" />
        /// at the specified index.
        /// </summary>
        /// <remarks> Overrides and implements collection change notification. </remarks>
        /// <param name="index"> The zero-based index at which <paramref name="item" /> should be
        /// inserted. </param>
        /// <param name="item">  The object to insert. </param>
        protected override void InsertItem( int index, TItem item )
        {
            base.InsertItem( index, item );
            this.OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Add, item, index ) );
        }

        /// <summary>
        /// Removes all elements from the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" />.
        /// </summary>
        /// <remarks> Overrides and implements collection change notification. </remarks>
        protected override void ClearItems()
        {
            base.ClearItems();
            this.OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Reset ) );
        }

        /// <summary>
        /// Removes the element at the specified index of the
        /// <see cref="T:System.Collections.ObjectModel.KeyedCollection`2" />.
        /// </summary>
        /// <remarks> Overrides and implements collection change notification. </remarks>
        /// <param name="index"> The index of the element to remove. </param>
        protected override void RemoveItem( int index )
        {
            var item = this.Items[index];
            base.RemoveItem( index );
            this.OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Remove, item ) );
        }

        /// <summary> Adds a range. </summary>
        /// <remarks> Overrides and implements a deferred collection change notification. </remarks>
        /// <param name="items"> An IEnumerable(OfTItem) of items to append to this. </param>
        public void AddRange( IList<TItem> items )
        {
            if ( items?.Any() == true )
            {
                try
                {
                    this.DeferNotifyCollectionChanged = true;
                    foreach ( TItem Item in items )
                    {
                        this.Add( Item );
                    }
                }
                catch
                {
                    throw;
                }
                finally
                {
                    this.DeferNotifyCollectionChanged = false;
                }

                this.OnCollectionChanged( new NotifyCollectionChangedEventArgs( NotifyCollectionChangedAction.Reset ) );
            }
        }

        #endregion

        #region " I NOTIFY COLLECTION CHANGE IMPLEMENTATION "

        /// <summary> Event queue for all listeners interested in CollectionChanged events. </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary> Gets or sets the defer notify collection changed. </summary>
        /// <value> The defer notify collection changed. </value>
        protected bool DeferNotifyCollectionChanged { get; set; }

        /// <summary> Raises the notify collection changed event. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void OnCollectionChanged( NotifyCollectionChangedEventArgs e )
        {
            if ( !this.DeferNotifyCollectionChanged )
            {
                var evt = CollectionChanged;
                evt?.Invoke( this, e );
            }
        }

        #endregion

    }
}
