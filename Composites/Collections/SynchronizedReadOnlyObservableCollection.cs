﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;

namespace isr.Core.Composites
{

    /// <summary> Implements a synchronized read only observable keyed collection. </summary>
    /// <remarks>
    /// David, 2017-05-02.  <para>
    /// (c) 2011 MULJADI BUDIMAN. All rights reserved. </para><para>
    /// http://geekswithblogs.net/NewThingsILearned/archive/2010/01/12/make-keyedcollectionlttkey-titemgt-to-work-properly-with-wpf-data-binding.aspx
    /// </para>
    /// </remarks>
    [DebuggerDisplay( "Count = {Count}" )]
    public class SynchronizedReadOnlyObservableCollection<T> : ReadOnlyCollection<T>, INotifyCollectionChanged, INotifyPropertyChanged
    {

        #region " CONSTRCTION and CLEANUP "

        /// <summary> Constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="list"> The list. </param>
        public SynchronizedReadOnlyObservableCollection( SynchronizedObservableCollection<T> list ) : base( list )
        {
            this.Context = SynchronizationContext.Current;
            (( INotifyCollectionChanged ) this.Items).CollectionChanged += this.HandleCollectionChanged;
            (( INotifyPropertyChanged ) this.Items).PropertyChanged += this.HandlePropertyChanged;
        }
        #endregion

        #region " Public Events "

        /// <summary> Event queue for all listeners interested in CollectionChanged events. </summary>
        public event NotifyCollectionChangedEventHandler CollectionChanged;

        /// <summary> Handles the collection changed. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Notify collection changed event information. </param>
        private void HandleCollectionChanged( object sender, NotifyCollectionChangedEventArgs e )
        {
            this.OnCollectionChanged( e );
        }

        /// <summary> Raises the notify collection changed event. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnCollectionChanged( NotifyCollectionChangedEventArgs e )
        {
            var evt = CollectionChanged;
            if ( evt is object )
            {
                this.Context.Send( state => evt( this, e ), null );
            }
        }

        /// <summary> Event queue for all listeners interested in PropertyChanged events. </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary> Handles the property changed. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="sender"> Source of the event. </param>
        /// <param name="e">      Property changed event information. </param>
        private void HandlePropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            this.OnPropertyChanged( e );
        }

        /// <summary> Gets or sets the context. </summary>
        /// <value> The context. </value>
        private SynchronizationContext Context { get; set; }

        /// <summary> Raises the property changed event. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        private void OnPropertyChanged( PropertyChangedEventArgs e )
        {
            var evt = PropertyChanged;
            if ( evt is object )
            {
                this.Context.Send( state => evt( this, e ), null );
            }
        }
        #endregion

    }
}