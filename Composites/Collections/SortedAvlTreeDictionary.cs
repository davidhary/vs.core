using System;
using System.Collections;
using System.Collections.Generic;

namespace isr.Core.Composites
{

    /// <summary>
    /// An AVL tree is a self-balancing Binary Search Tree (BST) where the difference between heights
    /// of left and right subtrees cannot be more than one for all nodes.
    /// </summary>
    /// <remarks> David, 2019-09-16. </remarks>
    public class SortedAvlTreeDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {

        /// <summary> An Avl tree node. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        private sealed class AvlTreeNode
        {

            /// <summary> Gets the key. </summary>
            /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
            /// <value> The key. </value>
            public TKey Key { get; set; }

            /// <summary> Gets the value. </summary>
            /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
            /// <value> The value. </value>
            public TValue Value { get; set; }

            /// <summary> Gets the left. </summary>
            /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
            /// <value> The left. </value>
            public AvlTreeNode Left { get; set; }

            /// <summary> Gets the right. </summary>
            /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
            /// <value> The right. </value>
            public AvlTreeNode Right { get; set; }

            /// <summary> Gets the height. </summary>
            /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
            /// <value> The height. </value>
            public int Height { get; set; }
        }

        /// <summary> The root. </summary>
        private AvlTreeNode _Root;

        /// <summary> The comparer. </summary>
        private readonly IComparer<TKey> _Comparer;

        /// <summary> Searches for the first match. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="x"> A TKey to process. </param>
        /// <param name="t"> An AvlTreeNode to process. </param>
        /// <returns> An AvlTreeNode. </returns>
        private AvlTreeNode SearchThis( TKey x, AvlTreeNode t )
        {
            if ( t is null )
            {
                return null;
            }

            int c = this._Comparer.Compare( x, t.Key );
            return 0 > c ? 0 == c ? t : this.SearchThis( x, t.Left ) : 0 == c ? t : this.SearchThis( x, t.Right );
        }

        /// <summary> Adds x. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="x"> A TKey to process. </param>
        /// <param name="v"> A TValue to process. </param>
        /// <param name="t"> An AvlTreeNode to process. </param>
        /// <returns> An AvlTreeNode. </returns>
        private AvlTreeNode AddThis( TKey x, TValue v, AvlTreeNode t )
        {
            if ( t is null )
            {
                t = new AvlTreeNode() {
                    Key = x,
                    Value = v,
                    Height = 0,
                    Right = null,
                    Left = null
                };
            }
            else
            {
                int c = this._Comparer.Compare( x, t.Key );
                if ( 0 > c )
                {
                    t.Left = this.AddThis( x, v, t.Left );
                    if ( GetHeightThis( t.Left ) - GetHeightThis( t.Right ) == 2 )
                    {
                        t = 0 > this._Comparer.Compare( x, t.Left.Key ) ? RorThis( t ) : RorrThis( t );
                    }
                }
                else if ( 0 < c )
                {
                    t.Right = this.AddThis( x, v, t.Right );
                    if ( GetHeightThis( t.Right ) - GetHeightThis( t.Left ) == 2 )
                    {
                        t = 0 < this._Comparer.Compare( x, t.Right.Key ) ? RolThis( t ) : RollThis( t );
                    }
                }
                else
                {
                    throw new InvalidOperationException( "An item with the specified key already exists in the dictionary." );
                }
            }

            t.Height = Math.Max( GetHeightThis( t.Left ), GetHeightThis( t.Right ) ) + 1;
            t.Value = v;
            return t;
        }

        /// <summary> RORs the given t. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="t"> An AvlTreeNode to process. </param>
        /// <returns> An AvlTreeNode. </returns>
        private static AvlTreeNode RorThis( AvlTreeNode t )
        {
            if ( t.Left is object )
            {
                var u = t.Left;
                t.Left = u.Right;
                u.Right = t;
                t.Height = Math.Max( GetHeightThis( t.Left ), GetHeightThis( t.Right ) ) + 1;
                u.Height = Math.Max( GetHeightThis( u.Left ), t.Height ) + 1;
                return u;
            }

            return t;
        }

        /// <summary> ROLs the given t. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="t"> An AvlTreeNode to process. </param>
        /// <returns> An AvlTreeNode. </returns>
        private static AvlTreeNode RolThis( AvlTreeNode t )
        {
            if ( t.Right is object )
            {
                var u = t.Right;
                t.Right = u.Left;
                u.Left = t;
                t.Height = Math.Max( GetHeightThis( t.Left ), GetHeightThis( t.Right ) ) + 1;
                u.Height = Math.Max( GetHeightThis( t.Right ), t.Height ) + 1;
                return u;
            }

            return t;
        }

        /// <summary> Rolls the given t. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="t"> An AvlTreeNode to process. </param>
        /// <returns> An AvlTreeNode. </returns>
        private static AvlTreeNode RollThis( AvlTreeNode t )
        {
            t.Right = RorThis( t.Right );
            return RolThis( t );
        }

        /// <summary> Rorrs the given t. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="t"> An AvlTreeNode to process. </param>
        /// <returns> An AvlTreeNode. </returns>
        private static AvlTreeNode RorrThis( AvlTreeNode t )
        {
            t.Left = RolThis( t.Left );
            return RorThis( t );
        }

        /// <summary> Gets left most. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="t"> An AvlTreeNode to process. </param>
        /// <returns> The left most. </returns>
        private AvlTreeNode GetLeftMostThis( AvlTreeNode t )
        {
            return t is null ? null : t.Left is null ? t : this.GetLeftMostThis( t.Left );
        }

        /// <summary> Removes this object. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="x"> A TKey to process. </param>
        /// <param name="t"> An AvlTreeNode to process. </param>
        /// <returns> An AvlTreeNode. </returns>
        private AvlTreeNode RemoveThis( TKey x, AvlTreeNode t )
        {
            AvlTreeNode temp;
            if ( t is null )
            {
                return null;
            }
            else if ( 0 > this._Comparer.Compare( x, t.Key ) )
            {
                t.Left = this.RemoveThis( x, t.Left );
            }
            else if ( 0 < this._Comparer.Compare( x, t.Key ) )
            {
                t.Right = this.RemoveThis( x, t.Right );
            }
            else if ( t.Left is object && t.Right is object )
            {
                temp = this.GetLeftMostThis( t.Right );
                t.Key = temp.Key;
                t.Value = temp.Value;
                t.Right = this.RemoveThis( t.Key, t.Right );
            }
            else if ( t.Left is null )
            {
                t = t.Right;
            }
            else if ( t.Right is null )
            {
                t = t.Left;
            }

            if ( t is null )
            {
                return t;
            }

            t.Height = Math.Max( GetHeightThis( t.Left ), GetHeightThis( t.Right ) ) + 1;
            if ( GetHeightThis( t.Left ) - GetHeightThis( t.Right ) == -2 )
            {
                return GetHeightThis( t.Right.Right ) - GetHeightThis( t.Right.Left ) == 1 ? RolThis( t ) : RollThis( t );
            }
            else if ( GetHeightThis( t.Right ) - GetHeightThis( t.Left ) == 2 )
            {
                return GetHeightThis( t.Left.Left ) - GetHeightThis( t.Left.Right ) == 1 ? RorThis( t ) : RorrThis( t );
            }

            return t;
        }

        /// <summary> Try remove. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="x"> A TKey to process. </param>
        /// <param name="t"> An AvlTreeNode to process. </param>
        /// <param name="s"> The output by reference s. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        private bool TryRemoveThis( TKey x, AvlTreeNode t, out AvlTreeNode s )
        {
            s = null;
            AvlTreeNode temp;
            bool res;
            if ( t is null )
            {
                return false;
            }
            else if ( 0 > this._Comparer.Compare( x, t.Key ) )
            {
                res = this.TryRemoveThis( x, t.Left, out s );
                if ( res )
                {
                    t.Left = s;
                }
            }
            else if ( 0 < this._Comparer.Compare( x, t.Key ) )
            {
                res = this.TryRemoveThis( x, t.Right, out s );
                if ( res )
                {
                    t.Right = s;
                }
            }
            else if ( t.Left is object && t.Right is object )
            {
                temp = this.GetLeftMostThis( t.Right );
                t.Key = temp.Key;
                t.Value = temp.Value;
                res = this.TryRemoveThis( t.Key, t.Right, out s );
                if ( res )
                {
                    t.Right = s;
                }
            }
            else
            {
                if ( t.Left is null )
                {
                    t = t.Right;
                }
                else if ( t.Right is null )
                {
                    t = t.Left;
                }

                res = true;
            }

            if ( t is null )
            {
                s = null;
                return res;
            }

            t.Height = Math.Max( GetHeightThis( t.Left ), GetHeightThis( t.Right ) ) + 1;
            if ( GetHeightThis( t.Left ) - GetHeightThis( t.Right ) == -2 )
            {
                if ( GetHeightThis( t.Right.Right ) - GetHeightThis( t.Right.Left ) == 1 )
                {
                    s = RolThis( t );
                    return true;
                }
                else
                {
                    s = RollThis( t );
                    return true;
                }
            }
            else if ( GetHeightThis( t.Right ) - GetHeightThis( t.Left ) == 2 )
            {
                if ( GetHeightThis( t.Left.Left ) - GetHeightThis( t.Left.Right ) == 1 )
                {
                    s = RorThis( t );
                    return true;
                }
                else
                {
                    s = RorrThis( t );
                    return true;
                }
            }

            s = t;
            return res;
        }

        /// <summary> Gets a height. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="t"> An AvlTreeNode to process. </param>
        /// <returns> The height. </returns>
        private static int GetHeightThis( AvlTreeNode t )
        {
            return t is null ? -1 : t.Height;
        }


        /// <summary> Gets a balance. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="t"> An AvlTreeNode to process. </param>
        /// <returns> The balance. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "CodeQuality", "IDE0051:Remove unused private members", Justification = "<Pending>" )]
        private static int GetBalanceThis( AvlTreeNode t )
        {
            return t is null ? 0 : GetHeightThis( t.Left ) - GetHeightThis( t.Right );
        }

        /// <summary> Enumerates enum nodes in this collection. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="t"> An AvlTreeNode to process. </param>
        /// <returns>
        /// An enumerator that allows for each to be used to process enum nodes in this collection.
        /// </returns>
        private IEnumerable<KeyValuePair<TKey, TValue>> EnumNodesThis( AvlTreeNode t )
        {
            if ( t is object )
            {
                foreach ( KeyValuePair<TKey, TValue> Item in this.EnumNodesThis( t.Left ) )
                {
                    yield return Item;
                }

                yield return new KeyValuePair<TKey, TValue>( t.Key, t.Value );
                foreach ( KeyValuePair<TKey, TValue> Item in this.EnumNodesThis( t.Right ) )
                {
                    yield return Item;
                }
            }
        }

        /// <summary> Gets a count. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="t"> An AvlTreeNode to process. </param>
        /// <returns> The count. </returns>
        private int GetCountThis( AvlTreeNode t )
        {
            if ( t is null )
            {
                return 0;
            }

            int result = 1;
            result += this.GetCountThis( t.Left );
            result += this.GetCountThis( t.Right );
            return result;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="comparer"> The comparer. </param>
        public SortedAvlTreeDictionary( IComparer<TKey> comparer )
        {
            this._Comparer = comparer ?? Comparer<TKey>.Default;
            this._Root = null;
        }

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public SortedAvlTreeDictionary() : this( null )
        {
        }

        /// <summary> Gets the number of. </summary>
        /// <value> The count. </value>
        public int Count => this.GetCountThis( this._Root );

        /// <summary> Gets the is read only. </summary>
        /// <value> The is read only. </value>
        public bool IsReadOnly => false;

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an
        /// element with the specified key.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="key"> The key to locate in the
        /// <see cref="T:System.Collections.Generic.IDictionary`2" />. </param>
        /// <returns>
        /// <see langword="true" /> if the <see cref="T:System.Collections.Generic.IDictionary`2" />
        /// contains an element with the key; otherwise, <see langword="false" />.
        /// </returns>
        public bool ContainsKey( TKey key )
        {
            var n = this.SearchThis( key, this._Root );
            return n is object;
        }

        /// <summary> Gets or sets the element with the specified key. </summary>
        /// <exception cref="KeyNotFoundException"> Thrown when a Key Not Found error condition occurs. </exception>
        /// <value> The element with the specified key. </value>
        public TValue this[TKey key]
        {
            get => this.TryGetValue( key, out TValue value ) ? value : throw new KeyNotFoundException();

            set {
                var n = this.SearchThis( key, this._Root );
                if ( n is object )
                {
                    n.Value = value;
                }
                else
                {
                    this.Add( key, value );
                }
            }
        }

        /// <summary> Gets the value associated with the specified key. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="key">   The key whose value to get. </param>
        /// <param name="value"> When this method returns, the value associated with the
        /// specified key, if the key is found; otherwise, the default value for the
        /// type of the <paramref name="value" /> parameter. This parameter is passed
        /// uninitialized. </param>
        /// <returns>
        /// <see langword="true" /> if the object that implements
        /// <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an element with the
        /// specified key; otherwise, <see langword="false" />.
        /// </returns>
        public bool TryGetValue( TKey key, out TValue value )
        {
            var n = this.SearchThis( key, this._Root );
            if ( n is object )
            {
                value = n.Value;
                return true;
            }

            value = default;
            return false;
        }

        /// <summary> Query if this object contains the given item. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="item"> The item to remove. </param>
        /// <returns> <c>true</c> if the object is in this collection; otherwise <c>false</c> </returns>
        public bool Contains( KeyValuePair<TKey, TValue> item )
        {
            return this.TryGetValue( item.Key, out TValue value ) && Equals( value, item.Value );
        }

        /// <summary> Copies to. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="array">      The array. </param>
        /// <param name="arrayIndex"> Zero-based index of the array. </param>
        public void CopyTo( KeyValuePair<TKey, TValue>[] array, int arrayIndex )
        {
            CollectionExtensions.CollectionExtensionMethods.CopyTo( this, array, arrayIndex );
            // DictionaryUtility.CopyTo(Me, array, index)
        }

        /// <summary>
        /// Adds an element with the provided key and value to the
        /// <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="key">   The object to use as the key of the element to add. </param>
        /// <param name="value"> The object to use as the value of the element to add. </param>
        public void Add( TKey key, TValue value )
        {
            this._Root = this.AddThis( key, value, this._Root );
        }

        /// <summary> Adds item. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="item"> The item to remove. </param>
        public void Add( KeyValuePair<TKey, TValue> item )
        {
            this.Add( item.Key, item.Value );
        }

        /// <summary>
        /// Removes the element with the specified key from the
        /// <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="key"> The key of the element to remove. </param>
        /// <returns>
        /// <see langword="true" /> if the element is successfully removed; otherwise,
        /// <see langword="false" />.  This method also returns <see langword="false" /> if
        /// <paramref name="key" /> was not found in the original
        /// <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </returns>
        public bool Remove( TKey key )
        {
            bool res = this.TryRemoveThis( key, this._Root, out AvlTreeNode s );
            if ( res )
            {
                this._Root = s;
                return true;
            }

            return false;
        }

        /// <summary> Removes the given item. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="item"> The item to remove. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool Remove( KeyValuePair<TKey, TValue> item )
        {
            return this.TryGetValue( item.Key, out TValue value ) && Equals( value, item.Value ) && this.Remove( item.Key );
        }

        /// <summary> Clears this object to its blank/initial state. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public void Clear()
        {
            this._Root = null;
        }

        /// <summary>
        /// Gets an <see cref="T:System.Collections.Generic.ICollection`1" /> containing the keys of the
        /// <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </summary>
        /// <value>
        /// An <see cref="T:System.Collections.Generic.ICollection`1" /> containing the keys of the
        /// object that implements <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </value>
        public ICollection<TKey> Keys => CollectionExtensions.CollectionExtensionMethods.CreateKeys( this );// Return DictionaryUtility.CreateKeys(Me)

        /// <summary>
        /// Gets an <see cref="T:System.Collections.Generic.ICollection`1" /> containing the values in
        /// the <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </summary>
        /// <value>
        /// An <see cref="T:System.Collections.Generic.ICollection`1" /> containing the values in the
        /// object that implements <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </value>
        public ICollection<TValue> Values => CollectionExtensions.CollectionExtensionMethods.CreateValues( this );// Return DictionaryUtility.CreateValues(Me)

        /// <summary> Gets the enumerator. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> The enumerator. </returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            return this.EnumNodesThis( this._Root ).GetEnumerator();
        }

        /// <summary> Gets the enumerator. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> The enumerator. </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
