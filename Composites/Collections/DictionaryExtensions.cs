using System;
using System.Collections.Generic;

namespace isr.Core.Composites.CollectionExtensions
{
    /// <summary>   Collection extension methods. </summary>
    /// <remarks>   David, 2020-09-22. </remarks>
    public static partial class CollectionExtensionMethods
    {

        /// <summary> Copies from a collection to an array. </summary>
        /// <remarks> David, 2019-09-15. </remarks>
        /// <exception cref="ArgumentNullException">       Thrown when one or more required arguments
        /// are null. </exception>
        /// <exception cref="ArgumentException">           Thrown when one or more arguments have
        /// unsupported or illegal values. </exception>
        /// <exception cref="ArgumentOutOfRangeException"> Thrown when one or more arguments are outside
        /// the required range. </exception>
        /// <param name="source">     Another instance to copy. </param>
        /// <param name="array">      The array. </param>
        /// <param name="arrayIndex"> The zero-based index in <paramref name="array" /> at which copying
        /// begins. </param>
        public static void CopyTo<TKey, TValue>( this ICollection<KeyValuePair<TKey, TValue>> source, KeyValuePair<TKey, TValue>[] array, int arrayIndex )
        {
            if ( source is null )
            {
                throw new ArgumentNullException( nameof( source ) );
            }

            if ( array is null )
            {
                throw new ArgumentNullException( nameof( array ) );
            }

            int i = source.Count;
            if ( i > array.Length )
            {
                throw new ArgumentException( "The array is not big enough to hold the dictionary entries.", nameof( array ) );
            }

            if ( 0 > arrayIndex || i > array.Length + arrayIndex )
            {
                throw new ArgumentOutOfRangeException( nameof( arrayIndex ) );
            }

            i = 0;
            foreach ( KeyValuePair<TKey, TValue> item in source )
            {
                array[i + arrayIndex] = item;
                i += 1;
            }
        }

        /// <summary> Creates the keys. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="parent"> The parent. </param>
        /// <returns> The new keys. </returns>
        public static ICollection<TKey> CreateKeys<TKey, TValue>( this IDictionary<TKey, TValue> parent )
        {
            return new KeysCollection<TKey, TValue>( parent );
        }

        /// <summary> Creates the values. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="parent"> The parent. </param>
        /// <returns> The new values. </returns>
        public static ICollection<TValue> CreateValues<TKey, TValue>( this IDictionary<TKey, TValue> parent )
        {
            return new ValuesCollection<TKey, TValue>( parent );
        }
    }
}
