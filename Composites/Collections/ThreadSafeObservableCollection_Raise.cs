﻿using System.ComponentModel;

namespace isr.Core.Composites
{
    public partial class ThreadSafeObservableCollection<T>
    {

        #region " NOTIFY "

        /// <summary>
        /// Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
        /// fast return of control to the invoking function.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
        /// even with slow handler functions.
        /// </remarks>
        /// <param name="e"> Property Changed event information. </param>
        protected virtual void NotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            this._PropertyChangedHandlers.Post( this, e );
        }

        /// <summary>
        /// Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
        /// fast return of control to the invoking function.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="name"> (Optional) Name of the caller member. </param>
        protected void NotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.NotifyPropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        /// <summary>
        /// Asynchronously notifies (posts) property change on a different thread. Safe for cross
        /// threading.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
        /// even with slow handler functions.
        /// </remarks>
        /// <param name="e"> Property Changed event information. </param>
        protected virtual void AsyncNotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            this._PropertyChangedHandlers.Post( this, e );
        }

        /// <summary>
        /// Asynchronously notifies (posts) property change on a different thread. Safe for cross
        /// threading.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="name"> (Optional) Name of the caller member. </param>
        protected void AsyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.AsyncNotifyPropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        /// <summary>
        /// Synchronously notifies (send) property change on a different thread. Safe for cross threading.
        /// </summary>
        /// <remarks>
        /// Overhead of this method is 3 to 4 times larger than naked raise event. This is the best
        /// approach.
        /// </remarks>
        /// <param name="e"> Property Changed event information. </param>
        protected virtual void SyncNotifyPropertyChanged( PropertyChangedEventArgs e )
        {
            this._PropertyChangedHandlers.Send( this, e );
        }

        /// <summary>
        /// Synchronously notifies (send) property change on a different thread. Safe for cross threading.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="name"> (Optional) Name of the caller member. </param>
        protected void SyncNotifyPropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.SyncNotifyPropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        #endregion

        #region " RAISE (SEND) "

        /// <summary> Raises (sends) the property changed event. </summary>
        /// <remarks>
        /// Overhead of this method is 3 to 4 times larger than naked raise event. This is equivalent
        /// the<see cref="SyncNotifyPropertyChanged(PropertyChangedEventArgs)"/> method,.
        /// </remarks>
        /// <param name="name"> (Optional) Name of the caller member. </param>
        protected void RaisePropertyChanged( [System.Runtime.CompilerServices.CallerMemberName()] string name = null )
        {
            if ( !string.IsNullOrWhiteSpace( name ) )
            {
                this.RaisePropertyChanged( new PropertyChangedEventArgs( name ) );
            }
        }

        /// <summary> Raises (sends) the property changed event. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="e"> Event information to send to registered event handlers. </param>
        protected virtual void RaisePropertyChanged( PropertyChangedEventArgs e )
        {
            this.OnPropertyChanged( this, e );
        }

        #endregion

    }
}