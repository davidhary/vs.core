using System;
using System.ComponentModel;

namespace isr.Core.Composites
{
    public partial class ThreadSafeObservableCollection<T> : INotifyPropertyChanged
    {

        #region " CUSTOM PROPERTY CHANGED EVENT IMPLEMENTATION "

        /// <summary> Removes the property changed event handlers. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        protected void RemovePropertyChangedEventHandlers()
        {
            this._PropertyChangedHandlers.RemoveAll();
        }

        /// <summary> The property changed handlers. </summary>
        [NonSerialized]
        private readonly PropertyChangeEventContextCollection _PropertyChangedHandlers = new ();

        /// <summary> Event queue for all listeners interested in property changed events. </summary>
        /// <remarks> A custom Event is used here to allow us to synchronize with the event listeners.
        /// Using a custom Raise method lets you iterate through the delegate list.
        /// </remarks>
        public new event PropertyChangedEventHandler PropertyChanged
        {
            add {
                this._PropertyChangedHandlers.Add( new PropertyChangedEventContext( value ) );
            }

            remove {
                this._PropertyChangedHandlers.RemoveValue( value );
            }
        }

        private void OnPropertyChanged( object sender, PropertyChangedEventArgs e )
        {
            this._PropertyChangedHandlers.Post( sender, e );
        }


        #endregion

    }
}
