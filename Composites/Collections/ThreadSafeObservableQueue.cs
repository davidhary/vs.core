using System.Diagnostics;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.Composites
#pragma warning restore IDE1006 // Naming Styles
{

    /// <summary> A Thread safe Observable Queue. </summary>
    /// <remarks> David, 2016-12-12. </remarks>
    [DebuggerDisplay( "Count = {Count}" )]
    public class ThreadSafeObservableQueue<T> : ThreadSafeObservableCollection<T>
    {

        /// <summary> Adds an object onto the end of this queue. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="item"> The item. </param>
        public void Enqueue( T item )
        {
            this.Add( item );
        }

        /// <summary> Removes the head object from this queue. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> The head object from this queue. </returns>
        public T Dequeue()
        {
            this.ItemsLocker.EnterUpgradeableReadLock();
            try
            {
                var item = this.GetBaseItem( 0 );
                this.ItemsLocker.EnterWriteLock();
                this.BaseRemoveItem( 0 );
                return item;
            }
            finally
            {
                this.ItemsLocker.ExitUpgradeableReadLock();
            }
        }

        /// <summary> Returns the top-of-stack object without removing it. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> The current top-of-stack object. </returns>
        public T Peek()
        {
            return base.GetItemThis( 0 );
        }
    }
}
