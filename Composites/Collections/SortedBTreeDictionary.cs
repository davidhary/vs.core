using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace isr.Core.Composites
{

    /// <summary>
    /// A B-tree. A self-balancing tree data structure that maintains sorted data and allows searches,
    /// sequential access, insertions, and deletions in logarithmic time. The B-tree is a
    /// generalization of a binary search tree in that a node can have more than two children.
    /// </summary>
    /// <remarks> David, 2019-09-16. </remarks>
    public class SortedBTreeDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {

        #region " CONTRUCTION "

        /// <summary> The comparer. </summary>
        private readonly IComparer<TKey> _Comparer;

        /// <summary> The default mininum degree. </summary>
        private const int _DefaultMininumDegree = 3;

        /// <summary> The root. </summary>
        private BTreeNode _Root;

        /// <summary> The minimum degree. </summary>
        private readonly int _MinimumDegree;

        /// <summary> Constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="minimumDegree"> The minimum degree. </param>
        /// <param name="comparer">      The comparer. </param>
        public SortedBTreeDictionary( int minimumDegree, IComparer<TKey> comparer ) : base()
        {
            this._Comparer = comparer ?? Comparer<TKey>.Default;
            this._Root = null;
            this._MinimumDegree = minimumDegree;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="minimumDegree"> The minimum degree. </param>
        public SortedBTreeDictionary( int minimumDegree ) : this( minimumDegree, null )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="comparer"> The comparer. </param>
        public SortedBTreeDictionary( IComparer<TKey> comparer ) : this( _DefaultMininumDegree, comparer )
        {
        }

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public SortedBTreeDictionary() : this( _DefaultMininumDegree )
        {
        }

        #endregion

        /// <summary> Returns an enumerator that iterates through the collection. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> An enumerator that can be used to iterate through the collection. </returns>
        public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
        {
            if ( this._Root is object )
            {
                foreach ( KeyValuePair<TKey, TValue> Item in this._Root )
                {
                    yield return Item;
                }
            }
        }

        /// <summary> Gets the enumerator. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <returns> The enumerator. </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an
        /// <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="array">      The one-dimensional <see cref="T:System.Array" /> that is the
        /// destination of the elements copied from
        /// <see cref="T:System.Collections.Generic.ICollection`1" />. The
        /// <see cref="T:System.Array" /> must have zero-based indexing. </param>
        /// <param name="arrayIndex"> The zero-based index in <paramref name="array" /> at which copying
        /// begins. </param>
        public void CopyTo( KeyValuePair<TKey, TValue>[] array, int arrayIndex )
        {
            // DictionaryUtility.CopyTo(Me, array, index)
            CollectionExtensions.CollectionExtensionMethods.CopyTo( this, array, arrayIndex );
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an
        /// element with the specified key.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="key"> The key to locate in the
        /// <see cref="T:System.Collections.Generic.IDictionary`2" />. </param>
        /// <returns>
        /// <see langword="true" /> if the <see cref="T:System.Collections.Generic.IDictionary`2" />
        /// contains an element with the key; otherwise, <see langword="false" />.
        /// </returns>
        public bool ContainsKey( TKey key )
        {
            return this._Root is object && this._Root.ContainsKey( key );
        }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a
        /// specific value.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="item"> The object to locate in the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="item" /> is found in the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public bool Contains( KeyValuePair<TKey, TValue> item )
        {
            return this.TryGetValue( item.Key, out TValue value ) && Equals( value, item.Value );
        }

        /// <summary> Gets the value associated with the specified key. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="key">   The key whose value to get. </param>
        /// <param name="value"> When this method returns, the value associated with the
        /// specified key, if the key is found; otherwise, the default value for the
        /// type of the <paramref name="value" /> parameter. This parameter is passed
        /// uninitialized.
        /// </param>
        /// <returns>
        /// <see langword="true" /> if the object that implements
        /// <see cref="T:System.Collections.Generic.IDictionary`2" /> contains an element with the
        /// specified key; otherwise, <see langword="false" />.
        /// </returns>
        public bool TryGetValue( TKey key, out TValue value )
        {
            if ( this._Root is object )
            {
                var node = this._Root.Search( key );
                if ( node is object )
                {
                    return node.TryGet( key, out value );
                }
            }

            value = default;
            return false;
        }

        /// <summary> Gets or sets the element with the specified key. </summary>
        /// <exception cref="KeyNotFoundException"> Thrown when a Key Not Found error condition occurs. </exception>
        /// <value> The element with the specified key. </value>
        public TValue this[TKey key]
        {
            get {
                if ( this._Root is object )
                {
                    var node = this._Root.Search( key );
                    if ( node is object )
                    {
                        if ( node.TryGet( key, out TValue result ) )
                        {
                            return result;
                        }
                    }
                }

                throw new KeyNotFoundException();
            }

            set {
                if ( this._Root is object )
                {
                    var node = this._Root.Search( key );
                    if ( node is object && node.TrySet( key, value ) )
                    {
                        return;
                    }
                }

                this.AddThis( key, value );
            }
        }

        /// <summary>
        /// Gets the number of elements contained in the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <value>
        /// The number of elements contained in the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </value>
        public int Count => this._Root is null ? 0 : this._Root.GetItemCount();

        /// <summary>
        /// Adds an element with the provided key and value to the
        /// <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="key">   The object to use as the key of the element to add. </param>
        /// <param name="value"> The object to use as the value of the element to add. </param>
        public void Add( TKey key, TValue value )
        {
            if ( this.ContainsKey( key ) )
            {
                throw new ArgumentException( "The specified key already exists in the dictionary.", nameof( key ) );
            }

            this.AddThis( key, value );
        }

        /// <summary> Adds this to 'value'. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="key">   The key of the element to remove. </param>
        /// <param name="value"> The value. </param>
        private void AddThis( TKey key, TValue value )
        {
            if ( this._Root is null )
            {
                this._Root = new BTreeNode( this._Comparer, this._MinimumDegree, true );
                this._Root.Items[0] = new KeyValuePair<TKey, TValue>( key, value );
                this._Root.KeyCount = 1;
            }
            else if ( this._Root.KeyCount == 2 * this._MinimumDegree - 1 )
            {
                var newRoot = new BTreeNode( this._Comparer, this._MinimumDegree, false );
                newRoot.Children[0] = this._Root;
                newRoot.Split( 0, this._Root );
                int i = 0;
                if ( 0 > this._Comparer.Compare( newRoot.Items[0].Key, key ) )
                {
                    i += 1;
                }

                newRoot.Children[i].Insert( key, value );
                this._Root = newRoot;
            }
            else
            {
                this._Root.Insert( key, value );
            }
        }

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="item"> The object to add to the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        public void Add( KeyValuePair<TKey, TValue> item )
        {
            this.Add( item.Key, item.Value );
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="item"> The object to remove from the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="item" /> was successfully removed from the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
        /// <see langword="false" />. This method also returns <see langword="false" /> if
        /// <paramref name="item" /> is not found in the original
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        public bool Remove( KeyValuePair<TKey, TValue> item )
        {
            return this.TryGetValue( item.Key, out TValue value ) && Equals( item.Value, value ) && this.Remove( item.Key );
        }

        /// <summary>
        /// Removes the element with the specified key from the
        /// <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        /// <param name="key"> The key of the element to remove. </param>
        /// <returns>
        /// <see langword="true" /> if the element is successfully removed; otherwise,
        /// <see langword="false" />.  This method also returns <see langword="false" /> if
        /// <paramref name="key" /> was not found in the original
        /// <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </returns>
        public bool Remove( TKey key )
        {
            if ( this._Root is object )
            {
                if ( !this._Root.Remove( key ) )
                {
                    if ( 0 == this._Root.KeyCount )
                    {
                        this._Root = this._Root.IsLeaf ? null : this._Root.Children[0];
                    }
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        public void Clear()
        {
            this._Root = null;
        }

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" />
        /// is read-only.
        /// </summary>
        /// <value>
        /// <see langword="true" /> if the <see cref="T:System.Collections.Generic.ICollection`1" /> is
        /// read-only; otherwise, <see langword="false" />.
        /// </value>
        public bool IsReadOnly => false;

        /// <summary>
        /// Gets an <see cref="T:System.Collections.Generic.ICollection`1" /> containing the keys of the
        /// <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </summary>
        /// <value>
        /// An <see cref="T:System.Collections.Generic.ICollection`1" /> containing the keys of the
        /// object that implements <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </value>
        public ICollection<TKey> Keys => CollectionExtensions.CollectionExtensionMethods.CreateKeys( this );// Return DictionaryUtility.CreateKeys(Me)

        /// <summary>
        /// Gets an <see cref="T:System.Collections.Generic.ICollection`1" /> containing the values in
        /// the <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </summary>
        /// <value>
        /// An <see cref="T:System.Collections.Generic.ICollection`1" /> containing the values in the
        /// object that implements <see cref="T:System.Collections.Generic.IDictionary`2" />.
        /// </value>
        public ICollection<TValue> Values => CollectionExtensions.CollectionExtensionMethods.CreateValues( this );// Return DictionaryUtility.CreateValues(Me)

        /// <summary> A tree node. </summary>
        /// <remarks> David, 202-09-12. </remarks>
        private sealed class BTreeNode : IEnumerable<KeyValuePair<TKey, TValue>>
        {
            /// <summary> The comparer. </summary>
            private readonly IComparer<TKey> _Comparer;
            /// <summary> The minimum degree. </summary>
            private readonly int _MinimumDegree;

            /// <summary> Gets the items. </summary>
            /// <value> The items. </value>
            internal KeyValuePair<TKey, TValue>[] Items { get; set; }

            /// <summary> Gets the children. </summary>
            /// <value> The children. </value>
            internal BTreeNode[] Children { get; set; }

            /// <summary> Gets the number of keys. </summary>
            /// <value> The number of keys. </value>
            internal int KeyCount { get; set; }

            /// <summary> Gets the is leaf. </summary>
            /// <value> The is leaf. </value>
            internal bool IsLeaf { get; set; }

            /// <summary> Constructor. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="comparer">      The comparer. </param>
            /// <param name="minimumDegree"> The minimum degree. </param>
            /// <param name="isLeaf">        True if is leaf, false if not. </param>
            public BTreeNode( IComparer<TKey> comparer, int minimumDegree, bool isLeaf ) : base()
            {
                this._Comparer = comparer;
                this._MinimumDegree = minimumDegree;
                this.IsLeaf = isLeaf;
                this.Items = new KeyValuePair<TKey, TValue>[(2 * this._MinimumDegree - 1)];
                this.Children = new BTreeNode[(2 * this._MinimumDegree)];
                this.KeyCount = 0;
            }

            /// <summary> Inserts. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="key">   The key. </param>
            /// <param name="value"> The value. </param>
            internal void Insert( TKey key, TValue value )
            {
                int i = this.KeyCount - 1;
                if ( i + 1 >= this.Children.Count() )
                {
                    Debugger.Break();
                }

                if ( this.IsLeaf )
                {
                    while ( i >= 0 && 0 < this._Comparer.Compare( this.Items[i].Key, key ) )
                    {
                        this.Items[i + 1] = this.Items[i];
                        i -= 1;
                    }

                    this.Items[i + 1] = new KeyValuePair<TKey, TValue>( key, value );
                    this.KeyCount += 1;
                }
                else
                {
                    while ( i >= 0 && 0 < this._Comparer.Compare( this.Items[i].Key, key ) )
                    {
                        i -= 1;
                    }

                    if ( this.Children[i + 1].KeyCount == 2 * this._MinimumDegree - 1 )
                    {
                        this.Split( i + 1, this.Children[i + 1] );
                        if ( 0 > this._Comparer.Compare( this.Items[i + 1].Key, key ) )
                        {
                            i += 1;
                        }
                    }

                    this.Children[i + 1].Insert( key, value );
                }
            }

            /// <summary> Splits. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="i">      Zero-based index of the. </param>
            /// <param name="target"> Target for the. </param>
            internal void Split( int i, BTreeNode target )
            {
                var newNode = new BTreeNode( this._Comparer, target._MinimumDegree, target.IsLeaf ) { KeyCount = this._MinimumDegree - 1 };
                for ( int j = 0, loopTo = this._MinimumDegree - 1 - 1; j <= loopTo; j++ )
                {
                    newNode.Items[j] = target.Items[j + this._MinimumDegree];
                }

                if ( !target.IsLeaf )
                {
                    for ( int j = 0, loopTo1 = this._MinimumDegree - 1; j <= loopTo1; j++ )
                    {
                        newNode.Children[j] = target.Children[j + this._MinimumDegree];
                    }
                }

                target.KeyCount = this._MinimumDegree - 1;
                for ( int j = this.KeyCount, loopTo2 = i + 1; j >= loopTo2; j -= 1 )
                {
                    this.Children[j + 1] = this.Children[j];
                }

                this.Children[i + 1] = newNode;
                for ( int j = this.KeyCount - 1, loopTo3 = i; j >= loopTo3; j -= 1 )
                {
                    this.Items[j + 1] = this.Items[j];
                }

                this.Items[i] = target.Items[this._MinimumDegree - 1];
                this.KeyCount += 1;
            }

            /// <summary> Returns an enumerator that iterates through the collection. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <returns> An enumerator that can be used to iterate through the collection. </returns>
            public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
            {
                int i;
                var loopTo = this.KeyCount - 1;
                for ( i = 0; i <= loopTo; i++ )
                {
                    if ( !this.IsLeaf )
                    {
                        foreach ( KeyValuePair<TKey, TValue> Item in this.Children[i] )
                        {
                            yield return Item;
                        }
                    }

                    yield return this.Items[i];
                }

                if ( !this.IsLeaf )
                {
                    foreach ( KeyValuePair<TKey, TValue> Item in this.Children[i] )
                    {
                        yield return Item;
                    }
                }
            }

            /// <summary> Gets the enumerator. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <returns> The enumerator. </returns>
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }

            /// <summary> Searches for the first match for the given t key. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="k"> The k to remove. </param>
            /// <returns> A BTreeNode. </returns>
            public BTreeNode Search( TKey k )
            {
                int i = 0;
                while ( i < this.KeyCount && 0 < this._Comparer.Compare( k, this.Items[i].Key ) )
                {
                    i += 1;
                }

                return i >= this.KeyCount
                    ? null
                    : 0 == this._Comparer.Compare( this.Items[i].Key, k ) ? this : this.IsLeaf ? null : this.Children[i].Search( k );
            }

            /// <summary> Attempts to get a TValue from the given TKey. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="k"> The k to remove. </param>
            /// <param name="v"> A TValue to process. </param>
            /// <returns> True if it succeeds, false if it fails. </returns>
            internal bool TryGet( TKey k, out TValue v )
            {
                int i = 0;
                while ( i < this.KeyCount && 0 < this._Comparer.Compare( k, this.Items[i].Key ) )
                {
                    i += 1;
                }

                var item = this.Items[i];
                if ( 0 == this._Comparer.Compare( item.Key, k ) )
                {
                    v = item.Value;
                    return true;
                }

                if ( this.IsLeaf )
                {
                    v = default;
                    return false;
                }

                return this.Children[i].TryGet( k, out v );
            }

            /// <summary> Attempts to set a TValue from the given TKey. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="k"> The k to remove. </param>
            /// <param name="v"> A TValue to process. </param>
            /// <returns> True if it succeeds, false if it fails. </returns>
            internal bool TrySet( TKey k, TValue v )
            {
                int i = 0;
                while ( i < this.KeyCount && 0 < this._Comparer.Compare( k, this.Items[i].Key ) )
                {
                    i += 1;
                }

                var item = this.Items[i];
                if ( 0 == this._Comparer.Compare( item.Key, k ) )
                {
                    this.Items[i] = new KeyValuePair<TKey, TValue>( k, v );
                    return true;
                }

                return !this.IsLeaf && this.Children[i].TrySet( k, v );
            }

            /// <summary> Query if 'k' contains key. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="k"> The k to remove. </param>
            /// <returns> True if it succeeds, false if it fails. </returns>
            public bool ContainsKey( TKey k )
            {
                int i = 0;
                while ( i < this.KeyCount && 0 < this._Comparer.Compare( k, this.Items[i].Key ) )
                {
                    i += 1;
                }

                if ( i >= this.KeyCount )
                {
                    return false;
                }

                var item = this.Items[i];
                return 0 == this._Comparer.Compare( item.Key, k ) || (!this.IsLeaf && this.Children[i].ContainsKey( k ));
            }

            /// <summary> Gets index of key this. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="k"> The k to remove. </param>
            /// <returns> The index of key this. </returns>
            private int GetIndexOfKeyThis( TKey k )
            {
                int idx = 0;
                while ( idx < this.KeyCount && 0 > this._Comparer.Compare( this.Items[idx].Key, k ) )
                {
                    idx += 1;
                }

                return idx;
            }

            /// <summary> Removes the given k. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="k"> The k to remove. </param>
            /// <returns> True if it succeeds, false if it fails. </returns>
            internal bool Remove( TKey k )
            {
                int idx = this.GetIndexOfKeyThis( k );
                if ( idx < this.KeyCount && 0 == this._Comparer.Compare( this.Items[idx].Key, k ) )
                {
                    if ( this.IsLeaf )
                    {
                        this.RemoveFromLeafThis( idx );
                    }
                    else
                    {
                        this.RemoveFromNonLeafThis( idx );
                    }
                }
                else
                {
                    if ( this.IsLeaf )
                    {
                        return false;
                    }

                    bool flag = idx == this.KeyCount;
                    if ( this.Children[idx].KeyCount < this._MinimumDegree )
                    {
                        this.FillThis( idx );
                    }

                    _ = flag && idx > this.KeyCount ? this.Children[idx - 1].Remove( k ) : this.Children[idx].Remove( k );
                }

                return true;
            }

            /// <summary> Removes from leaf this described by idx. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="idx"> Zero-based index of the. </param>
            private void RemoveFromLeafThis( int idx )
            {
                for ( int i = idx + 1, loopTo = this.KeyCount - 1; i <= loopTo; i++ )
                {
                    this.Items[i - 1] = this.Items[i];
                }

                this.KeyCount -= 1;
                return;
            }

            /// <summary> Removes from non leaf this described by idx. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="idx"> Zero-based index of the. </param>
            private void RemoveFromNonLeafThis( int idx )
            {
                var k = this.Items[idx].Key;
                if ( this.Children[idx].KeyCount >= this._MinimumDegree )
                {
                    var pred = this.GetPreviousItemThis( idx );
                    this.Items[idx] = pred;
                    _ = this.Children[idx].Remove( pred.Key );
                }
                else if ( this.Children[idx + 1].KeyCount >= this._MinimumDegree )
                {
                    var succ = this.GetNextItemThis( idx );
                    this.Items[idx] = succ;
                    _ = this.Children[idx + 1].Remove( succ.Key );
                }
                else
                {
                    this.MergeThis( idx );
                    _ = this.Children[idx].Remove( k );
                }

                return;
            }

            /// <summary> Gets item count. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <returns> The item count. </returns>
            internal int GetItemCount()
            {
                int i;
                int result = 0;
                var loopTo = this.KeyCount - 1;
                for ( i = 0; i <= loopTo; i++ )
                {
                    if ( !this.IsLeaf )
                    {
                        result += this.Children[i].GetItemCount();
                    }

                    result += 1;
                }

                if ( !this.IsLeaf )
                {
                    result += this.Children[i].GetItemCount();
                }

                return result;
            }

            /// <summary> Gets the previous item this. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="idx"> Zero-based index of the. </param>
            /// <returns> The previous item this. </returns>
            private KeyValuePair<TKey, TValue> GetPreviousItemThis( int idx )
            {
                var cur = this.Children[idx];
                while ( !cur.IsLeaf )
                {
                    cur = cur.Children[cur.KeyCount];
                }

                return cur.Items[cur.KeyCount - 1];
            }

            /// <summary> Gets the next item this. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="idx"> Zero-based index of the. </param>
            /// <returns> The next item this. </returns>
            private KeyValuePair<TKey, TValue> GetNextItemThis( int idx )
            {
                var cur = this.Children[idx + 1];
                while ( !cur.IsLeaf )
                {
                    cur = cur.Children[0];
                }

                return cur.Items[0];
            }

            /// <summary> Fill this. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="idx"> Zero-based index of the. </param>
            private void FillThis( int idx )
            {
                if ( idx != 0 && this.Children[idx - 1].KeyCount >= this._MinimumDegree )
                {
                    this.BorrowFromPreviousThis( idx );
                }
                else if ( idx != this.KeyCount && this.Children[idx + 1].KeyCount >= this._MinimumDegree )
                {
                    this.BorrowFromNextThis( idx );
                }
                else if ( idx != this.KeyCount )
                {
                    this.MergeThis( idx );
                }
                else if ( 0 != idx )
                {
                    this.MergeThis( idx - 1 );
                }

                return;
            }

            /// <summary> Borrow from previous this. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="idx"> Zero-based index of the. </param>
            private void BorrowFromPreviousThis( int idx )
            {
                var child = this.Children[idx];
                var sibling = this.Children[idx - 1];
                for ( int i = child.KeyCount - 1; i >= 0; i -= 1 )
                {
                    child.Items[i + 1] = child.Items[i];
                }

                if ( !child.IsLeaf )
                {
                    for ( int i = child.KeyCount; i >= 0; i -= 1 )
                    {
                        child.Children[i + 1] = child.Children[i];
                    }
                }

                child.Items[0] = this.Items[idx - 1];
                if ( !child.IsLeaf )
                {
                    child.Children[0] = sibling.Children[sibling.KeyCount];
                }

                this.Items[idx - 1] = sibling.Items[sibling.KeyCount - 1];
                child.KeyCount += 1;
                sibling.KeyCount -= 1;
                return;
            }

            /// <summary> Borrow from next this. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="idx"> Zero-based index of the. </param>
            private void BorrowFromNextThis( int idx )
            {
                var child = this.Children[idx];
                var sibling = this.Children[idx + 1];
                child.Items[child.KeyCount] = this.Items[idx];
                if ( !child.IsLeaf )
                {
                    child.Children[child.KeyCount + 1] = sibling.Children[0];
                }

                this.Items[idx] = sibling.Items[0];
                for ( int i = 1, loopTo = sibling.KeyCount - 1; i <= loopTo; i++ )
                {
                    sibling.Items[i - 1] = sibling.Items[i];
                }

                if ( !sibling.IsLeaf )
                {
                    for ( int i = 1, loopTo1 = sibling.KeyCount; i <= loopTo1; i++ )
                    {
                        sibling.Children[i - 1] = sibling.Children[i];
                    }
                }

                child.KeyCount += 1;
                sibling.KeyCount -= 1;
                return;
            }

            /// <summary> Merge this. </summary>
            /// <remarks> David, 202-09-12. </remarks>
            /// <param name="idx"> Zero-based index of the. </param>
            private void MergeThis( int idx )
            {
                var child = this.Children[idx];
                var sibling = this.Children[idx + 1];
                child.Items[this._MinimumDegree - 1] = this.Items[idx];
                for ( int i = 0, loopTo = sibling.KeyCount - 1; i <= loopTo; i++ )
                {
                    child.Items[i + this._MinimumDegree] = sibling.Items[i];
                }

                if ( !child.IsLeaf )
                {
                    for ( int i = 0, loopTo1 = sibling.KeyCount; i <= loopTo1; i++ )
                    {
                        child.Children[i + this._MinimumDegree] = sibling.Children[i];
                    }
                }

                for ( int i = idx + 1, loopTo2 = this.KeyCount - 1; i <= loopTo2; i++ )
                {
                    this.Items[i - 1] = this.Items[i];
                }

                for ( int i = idx + 2, loopTo3 = this.KeyCount; i <= loopTo3; i++ )
                {
                    this.Children[i - 1] = this.Children[i];
                }

                child.KeyCount += sibling.KeyCount + 1;
                this.KeyCount -= 1;
                return;
            }
        }
    }
}
