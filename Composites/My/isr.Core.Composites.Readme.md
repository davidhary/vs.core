## ISR Core Composites<sub>&trade;</sub>: Core Composites Class Library
* [History](#Revision-History)
* [License](#The-MIT-License)
* [Open Source](#Open-Source)
* [Closed software](#Closed-software)

### Revision History [](#){name=Revision-History}

*5.0.7280 2019-12-07*  
Uses now Services, Model and Constructs created from
agnostic.

*3.2.6431 2017-08-10*  
Renames hourly yield to hourly rate. Uses common code
for formatting time elements in Eon and Yield stopwatch.

*3.2.6428 2017-08-07*  
Adds bins and increment function to the yield counter.

*3.2.6422 2017-08-01*  
Fixes validation of bin numbers.

*3.2.6419 2017-07-29*  
Adds Synchronized Keyed Observable collection. Uses
Pith Thread Safe Monitor.

*3.1.6382 2017-06-22*  
Uses UTC for time keepers and yield counters.

*3.1.6352 2017-05-23*  
Uses clear know state to set start time.

*3.1.6222 2016-01-13*  
Adds wait function for timeout stopwatch.

*3.1.6190 2016-12-12*  
Adds synchronized observable collections.

*3.1.6117 2016-09-30*  
Renames count down stop watch to timeout stopwatch.
Adds Yield Counter. Adds Offset stopwatch.

*3.0.5866 2016-01-23*  
Updates to .NET 4.6.1

*2.0.5126 2014-01-13*  
Tagged as 2014.

*2.0.5019 2013-09-28*  
Created based on the legacy core structures.

\(C\) 2009 Integrated Scientific Resources, Inc. All rights reserved.

### The MIT License [](#){name=The-MIT-License}
THE SOFTWARE IS PROVIDED \"AS IS\", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

This software was developed and tested using Microsoft<sup>&reg;</sup> [Visual Studio](https://www.visualstudIO.com/) 2019.  

Source code for this project is hosted on [Bit Bucket](https://bitbucket.org/davidhary).

### Open source  [](#){name=Open-Source}
Open source used by this software is described and licensed at the
following sites:  
[Collection Extensions](https://github.com/CoryCharlton/CCSWE.Core)
