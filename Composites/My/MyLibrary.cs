﻿
namespace isr.Core.Composites.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 202-09-12. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 202-09-12. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Core Composites Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Core Composites Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Core.Composites";
    }
}