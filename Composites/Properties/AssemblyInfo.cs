﻿using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.Composites.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.Composites.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.Composites.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]
