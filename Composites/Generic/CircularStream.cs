using System;
using System.IO;

namespace isr.Core.Composites
{
    /// <summary>   A circular stream. </summary>
    /// <remarks>   David, 2020-09-09.
    /// From: https://archive.codeplex.com/?p=circularbuffer </remarks>
    /// <license> (c) 2012 Alex Regueiro.<para>
    /// Licensed under The MIT License. </para><para>
    /// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    /// </para> </license>
    public class CircularStream : Stream
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="bufferCapacity">   The buffer capacity. </param>
        public CircularStream( int bufferCapacity )
            : base()
        {
            this._Buffer = new CircularBuffer<byte>( bufferCapacity );
        }

        /// <summary>   The buffer. </summary>
        private readonly CircularBuffer<byte> _Buffer;

        /// <summary>
        /// When overridden in a derived class, gets or sets the position within the current stream.
        /// </summary>
        /// <value> The current position within the stream. </value>
        public override long Position
        {
            get => throw new NotImplementedException();
            set => throw new NotImplementedException();
        }

        /// <summary>   Gets or sets the capacity. </summary>
        /// <value> The capacity. </value>
        public int Capacity
        {
            get => this._Buffer.Capacity;
            set => this._Buffer.Capacity = value;
        }

        /// <summary>
        /// When overridden in a derived class, gets the length in bytes of the stream.
        /// </summary>
        /// <value> A long value representing the length of the stream in bytes. </value>
        public override long Length => this._Buffer.Size;

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether the current stream
        /// supports seeking.
        /// </summary>
        /// <value>
        /// <see langword="true" /> if the stream supports seeking; otherwise, <see langword="false" />.
        /// </value>
        public override bool CanSeek => true;

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether the current stream
        /// supports reading.
        /// </summary>
        /// <value>
        /// <see langword="true" /> if the stream supports reading; otherwise, <see langword="false" />.
        /// </value>
        public override bool CanRead => true;

        /// <summary>
        /// When overridden in a derived class, gets a value indicating whether the current stream
        /// supports writing.
        /// </summary>
        /// <value>
        /// <see langword="true" /> if the stream supports writing; otherwise, <see langword="false" />.
        /// </value>
        public override bool CanWrite => true;

        /// <summary>   Gets the buffer. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>   An array of byte. </returns>
        public byte[] GetBuffer()
        {
            return this._Buffer.GetBuffer();
        }

        /// <summary>   Convert this object into an array representation. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>   An array that represents the data in this object. </returns>
        public byte[] ToArray()
        {
            return this._Buffer.ToArray();
        }

        /// <summary>
        /// When overridden in a derived class, clears all buffers for this stream and causes any
        /// buffered data to be written to the underlying device.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        public override void Flush()
        {
        }

        /// <summary>
        /// When overridden in a derived class, writes a sequence of bytes to the current stream and
        /// advances the current position within this stream by the number of bytes written.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="buffer">   An array of bytes. This method copies <paramref name="count" /> bytes
        ///                         from <paramref name="buffer" /> to the current stream. </param>
        /// <param name="offset">   The zero-based byte offset in <paramref name="buffer" /> at which to
        ///                         begin copying bytes to the current stream. </param>
        /// <param name="count">    The number of bytes to be written to the current stream. </param>
        public override void Write( byte[] buffer, int offset, int count )
        {
            _ = this._Buffer.Put( buffer, offset, count );
        }

        /// <summary>
        /// Writes a byte to the current position in the stream and advances the position within the
        /// stream by one byte.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="value">    The byte to write to the stream. </param>
        public override void WriteByte( byte value )
        {
            this._Buffer.Put( value );
        }

        /// <summary>
        /// When overridden in a derived class, reads a sequence of bytes from the current stream and
        /// advances the position within the stream by the number of bytes read.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="buffer">   An array of bytes. When this method returns, the buffer contains the
        ///                         specified byte array with the values between
        ///                         <paramref name="offset" /> and (<paramref name="offset" /> +
        ///                         <paramref name="count" /> - 1) replaced by the bytes read from the
        ///                         current source. </param>
        /// <param name="offset">   The zero-based byte offset in <paramref name="buffer" /> at which to
        ///                         begin storing the data read from the current stream. </param>
        /// <param name="count">    The maximum number of bytes to be read from the current stream. </param>
        /// <returns>
        /// The total number of bytes read into the buffer. This can be less than the number of bytes
        /// requested if that many bytes are not currently available, or zero (0) if the end of the
        /// stream has been reached.
        /// </returns>
        public override int Read( byte[] buffer, int offset, int count )
        {
            return this._Buffer.Get( buffer, offset, count );
        }

        /// <summary>
        /// Reads a byte from the stream and advances the position within the stream by one byte, or
        /// returns -1 if at the end of the stream.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>
        /// The unsigned byte cast to an <see langword="Int32" />, or -1 if at the end of the stream.
        /// </returns>
        public override int ReadByte()
        {
            return this._Buffer.Get();
        }

        /// <summary>
        /// When overridden in a derived class, sets the position within the current stream.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="offset">   A byte offset relative to the <paramref name="origin" /> parameter. </param>
        /// <param name="origin">   A value of type <see cref="T:System.IO.SeekOrigin" /> indicating the
        ///                         reference point used to obtain the new position. </param>
        /// <returns>   The new position within the current stream. </returns>
        public override long Seek( long offset, SeekOrigin origin )
        {
            throw new NotImplementedException();
        }

        /// <summary>   When overridden in a derived class, sets the length of the current stream. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="value">    The desired length of the current stream in bytes. </param>
        public override void SetLength( long value )
        {
            throw new NotImplementedException();
        }
    }
}
