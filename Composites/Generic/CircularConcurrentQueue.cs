using System;
using System.Collections.Concurrent;

namespace isr.Core.Composites
{

    /// <summary>   A Concurrent Queue of fixed size. </summary>
    /// <remarks>   David, 2020-09-10. </remarks>
    /// <license>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.</para>
    /// </license>
    /// <typeparamref name="T">    Generic type parameter. </typeparamref>
    public class CircularConcurrentQueue<T> : ConcurrentQueue<T>
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        private CircularConcurrentQueue()
        {
        }

        /// <summary>
        /// Creates a queue with room for capacity of items. Extra items are dequeued upon queuing items
        /// beyond the capacity.
        /// </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="capacity"> The capacity. </param>
        public CircularConcurrentQueue( int capacity )
        {
            this.Capacity = capacity;
        }

        /// <summary>   The synchronize root. </summary>
        [NonSerialized]
        private Object _SyncRoot;

        /// <summary>
        /// Gets an object that can be used to synchronize access to the
        /// <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        /// <value>
        /// An object that can be used to synchronize access to the
        /// <see cref="T:System.Collections.ICollection" />.
        /// </value>
        public virtual Object SyncRoot
        {
            get {
                if ( this._SyncRoot == null )
                {
                    _ = System.Threading.Interlocked.CompareExchange( ref this._SyncRoot, new Object(), null );
                }
                return this._SyncRoot;
            }
        }

        /// <summary>   Gets or sets the capacity. </summary>
        /// <value> The capacity. </value>
        public int Capacity { get; private set; }

        /// <summary>
        /// Adds an item to the end of the
        /// <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" />. Extra items are dequeued
        /// upon queuing items beyond the fixed <see cref="Capacity"/>.
        /// </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="item">  The item to add to the end of the
        ///                     <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" />. The
        ///                     value can be a null reference (Nothing in Visual Basic) for reference
        ///                     types. </param>
        public new void Enqueue( T item )
        {
            base.Enqueue( item );
            lock ( this.SyncRoot )
            {
                while ( base.Count > this.Capacity && base.TryDequeue( out _ ) )
                {; }
            }
        }

        /// <summary>
        /// Adds items to the end of the
        /// <see cref="T:System.Collections.Concurrent.ConcurrentQueue`1" />. Extra items are dequeued
        /// upon queuing items beyond the fixed <see cref="Capacity"/>.
        /// </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="items">    The items. </param>
        public void Enqueue( T[] items )
        {
            foreach ( T item in items )
            {
                base.Enqueue( item );
            }
            lock ( this.SyncRoot )
            {
                while ( base.Count > this.Capacity && base.TryDequeue( out _ ) )
                {; }
            }
        }

        /// <summary>   Removes items from the beginning of the queue. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="dst">  Destination for the items. </param>
        /// <returns>   The number of actual items removed from this queue. </returns>
        public int Dequeue( T[] dst )
        {
            return this.Dequeue( dst, 0, this.Count );
        }

        /// <summary>   Removes items from the beginning of the queue. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="dst">      Destination for the items. </param>
        /// <param name="count">    Number of item to remove. </param>
        /// <returns>   The number of actual items removed from this queue. </returns>
        public int Dequeue( T[] dst, int count )
        {
            return this.Dequeue( dst, 0, count );
        }

        /// <summary>   Removes items from the beginning of the queue. </summary>
        /// <remarks>   David, 2020-09-10. </remarks>
        /// <param name="dst">      Destination for the items. </param>
        /// <param name="offset">   The offset. </param>
        /// <param name="count">    Number of item to remove. </param>
        /// <returns>   The number of actual items removed from this queue. </returns>
        public int Dequeue( T[] dst, int offset, int count )
        {
            int realCount = Math.Min( count, this.Count );
            int actualCount = 0;
            int dstIndex = offset;
            for ( int i = 0; i < realCount; i++, dstIndex++ )
            {
                if ( this.TryDequeue( out dst[dstIndex] ) )
                {
                    actualCount++;
                }
                else
                {
                    break;
                }
            }
            return actualCount;
        }


    }

}
