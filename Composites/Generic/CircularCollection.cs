using System;
using System.Collections;
using System.Diagnostics;
using System.Reflection;
using System.Threading;

namespace isr.Core.Composites
{

    /// <summary>
        /// <para>Implements a <b>Circular Buffer</b>. </para>
        /// The Circular Buffer is a memory queue where memory locations are reused when the data
        /// producer (writer) overwrites (modulo the buffer size) previously used locations. The Circular
        /// Buffer operates in two modes of operation: the synchronous mode and the asynchronous mode.
        /// The synchronous mode is useful where one process, either the producer (writer) or the
        /// consumer (reader), operates much faster than the other. The faster process would otherwise
        /// waste time waiting for the slower process. Instead the faster process can "burst" off the
        /// data into the Circular Buffer and continue. The slower process will read the data at its own
        /// rate. However, in the synchronous mode, the producer and consumer of the Circular Buffer must
        /// access the queue at the same average rate over time or an overflow or underflow of data will
        /// occur. In the asynchronous mode, we are only interested in the last data items written by the
        /// producer--earlier data will be lost. This is useful in debug tracing where debug information
        /// is continuously written into the Circular Buffer over a period of time until the error
        /// condition is detected. The Circular Buffer is then examined for the sequence of states,
        /// commands, etc. written just before the error was detected to help isolate the bug. The actual
        /// capacity of the Circular Buffer is one less (N-1) than the total length of the Buffer(N) so
        /// that a full buffer and an empty buffer can be differentiated. The default length is N=256 for
        /// a default capacity of 255. A client process can be notified that the Circular Buffer has
        /// reached a specified count (WaterMark) using the CBEventHandler delegate
        /// <para>
        /// Example
        /// </para><para><code>
        /// ' notify every time circular buffer is at count 8 theCB.SetWaterMark(8);
        /// </code></para><para><code>
        /// theCB.WaterMarkNotify += new QueueCB.CBEventHandler(OnWaterMarkEvent);
        /// </code></para>
        /// </summary>
        /// <remarks>
        /// Copyright (C) 2002 Robert HinRichs. All rights reserved.<para>
        /// https://www.codeproject.com/articles/2880/circular-buffer-2</para><para>
        /// BOBH@INAV.NET</para><para>
        /// Licensed under The MIT License. </para>
        /// </remarks>
    public class CircularCollection<T> : IEnumerable, IDisposable
    {

        #region " CONSTRUCTION "

        /// <summary> Default Constructor creates N=256 element Circular Buffer. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public CircularCollection() : this( 256 )
        {
        }

        /// <summary> Constructor creates a Circular Buffer with the specified length. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="length"> Circular Buffer Length (# of elements) </param>
        public CircularCollection( int length ) : base()
        {
            this._NumberToAdd = 0;
            this._Buffer = new T[length];
            this.Capacity = length;
            this._Add = 1 - this.Capacity;
            this._Len = this.Capacity;
            this.ClearThis();
            this._SynchronousMode = true;
            this._WatermarkCheckingEnabled = true;
            this._CountEvent = new CountEvent();
        }

        #region " IDisposable Support "

        /// <summary> Gets a value indicating whether this instance is disposed. </summary>
        /// <value> <c>True</c> if this instance is disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Releases unmanaged and - optionally - managed resources. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed && disposing )
                {
                    if ( WatermarkNotify is object )
                    {
                        foreach ( Delegate d in WatermarkNotify.GetInvocationList() )
                            WatermarkNotify -= ( EventHandler<EventArgs> ) d;
                    }

                    if ( this._CountEvent is object )
                    {
                        this._CountEvent.Dispose();
                        this._CountEvent = null;
                    }
                }
            }
            catch
            {
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }

        #endregion

        #endregion

        /// <summary>
        /// Occurs when the read pointer hits the watermark count.
        /// </summary>
        public event EventHandler<EventArgs> WatermarkNotify;

        /// <summary>
        /// Returns current number of items in the Circular Buffer. In the asynchronous mode, count will
        /// saturate at N-1 if an overflow conditions exists. The read index will then follow the write
        /// index so that the latest items are always available.
        /// </summary>
        /// <value> The count. </value>
        public int Count { get; private set; }

        /// <summary> Returns next Read Index. </summary>
        /// <value> The read index. </value>
        public int ReadIndex { get; private set; }

        /// <summary> Returns next Write Index. </summary>
        /// <value> The write index. </value>
        public int WriteIndex { get; private set; }

        /// <summary> True to enable synchronous mode, false to disable it. </summary>
        private bool _SynchronousMode;

        /// <summary>
        /// Used to set synchronous or asynchronous modes of operation Modes differ in how they handle
        /// the buffer overflow condition.
        /// </summary>
        /// <remarks>
        /// 1> Synchronous Mode -- In the synchronous mode the emptying of the
        /// Circular Buffer must occur at the same average rate as the filling
        /// of the Circular Buffer so that the overrun condition never occurs.
        /// That is, as the tail index chases the head index around the circle
        /// of the buffer, the head index always stays ahead (in a modulo N sense)
        /// of the tail index. No data is lost. An exception will be thrown when
        /// a buffer overrun condition occurs.
        /// <para>
        /// 2> Asynchronous Mode -- In the asynchronous mode, data will be lost
        /// as the Circular Buffer is overwritten. Usually in this mode, only
        /// the last item written are of interest. When the Write index
        /// (head index) catches the Read Index (tail index) the Read Index is
        /// automatically incremented and the Circular Buffer count will indicate
        /// N-1 items in the Buffer.
        /// The synchronous mode is pretty straight forward as most of the
        /// responsibility for avoiding buffer overflow is a system issue for the
        /// producer and consumer of the data. The circular buffer just checks
        /// for error conditions.
        /// The asynchronous mode is more difficult as buffer overruns are allowed.
        /// So things operate as normal until the maximum occupancy is reached in
        /// the asynchronous mode. From then on, while the buffer is still at
        /// maximum occupancy, the read index follows the write index. This
        /// simulates synchronous operation. I call this the saturation
        /// mode. While saturation exists, an additional complication must be
        /// handled when the data is read. The read index is stored ahead of valid
        /// data (write index so an adjustment must be made before it is used to
        /// output data from the circular buffer.
        /// Capacity is N-1
        /// </para>
        /// </remarks>
        /// <value> Property <c>SynchronousMode</c>set to true of false. </value>
        public bool SynchronousMode
        {
            get => this._SynchronousMode;

            set {
                lock ( this )
                    this._SynchronousMode = value;
            }
        }

        /// <summary> Number of watermarks. </summary>
        private int _WatermarkCount;

        /// <summary>
        /// Gets or sets the level (queue count) at which the WaterMarkNotify event will fire.
        /// </summary>
        /// <value> Property <c>Watermark</c>Pos. integer at which event fires. </value>
        public int WatermarkCount
        {
            get => this._WatermarkCount;

            set {
                lock ( this )
                    this._WatermarkCount = value;
            }
        }

        /// <summary> True to enable, false to disable the watermark checking. </summary>
        private bool _WatermarkCheckingEnabled;

        /// <summary> Gets or sets a value indicating whether watermark checking is enabled. </summary>
        /// <value> <c>True</c> if watermark checking is enabled; otherwise, <c>False</c>. </value>
        public bool WatermarkCheckingEnabled
        {
            get => this._WatermarkCheckingEnabled;

            set {
                lock ( this )
                    this._WatermarkCheckingEnabled = value;
            }
        }

        /// <summary> Returns assembly version string. </summary>
        /// <value> The get version. </value>
        public string GetVersion
        {
            get {
                var asm = Assembly.GetExecutingAssembly();
                var asmn = asm.GetName();
                var ver = asmn.Version;
                return ver.ToString();
            }
        }

        /// <summary> The add. </summary>
        private int _Add;

        /// <summary> The length. </summary>
        private int _Len;

        /// <summary> Number of to adds. </summary>
        private int _NumberToAdd;

        /// <summary> The capacity. </summary>
        private int _Capacity;

        /// <summary> Gets or sets the capacity. Client must set through method. </summary>
        /// <value> The capacity. </value>
        public int Capacity
        {
            get => this._Capacity;

            set {
                this._Capacity = value;
                this._Add = 1 - this.Capacity;
                this._Len = this.Capacity;
                this.Clear();
            }
        }

        /// <summary> The buffer. </summary>
        private readonly T[] _Buffer;

        /// <summary>
        /// Resynchronizes the buffer pointers to beginning of buffer, i.e., it sets the Read pointer to
        /// the write pointer. This is equivalent to dumping all the data in the buffer.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void Resync()
        {
            this.ClearThis();
        }

        /// <summary> Clears the Circular Buffer and initializes with null. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void ClearThis()
        {
            this.ReadIndex = 0;
            this.WriteIndex = 0;
            this.Count = 0;
            Array.Clear( this._Buffer, 0, this.Capacity );
        }

        /// <summary> Clears the Circular Buffer and initializes with null. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void Clear()
        {
            this.ClearThis();
        }

        /// <summary> Enable Water Mark (queue count) checking. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void WatermarkEnable()
        {
            this._WatermarkCheckingEnabled = true;
        }

        /// <summary> Disable Water Mark (queue count) checking. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void WatermarkDisable()
        {
            this._WatermarkCheckingEnabled = false;
        }

        /// <summary> Sets Asynchronous Mode of Circular Buffer operation-- see class summary. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void SetAsynchronousMode()
        {
            this.SynchronousMode = false;
        }

        /// <summary> Sets Synchronous Mode of Circular Buffer operation-- see class summary. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void SetSynchronousMode()
        {
            this.SynchronousMode = true;
        }

        /// <summary> Sets WaterMark (queue count) value for WaterMark event operations. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="System.IndexOutOfRangeException"> Thrown when an Index Range error condition occurs. </exception>
        /// <param name="mark"> Positive Integer WaterMark event value. </param>
        public void SetWatermark( int mark )
        {
            this.WatermarkCount = mark <= this.Capacity - 1 | mark < 0 ? mark : throw new System.IndexOutOfRangeException( "Watermark out of range" );
        }

        /// <summary> Returns item at read index without modifying the queue. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="System.IndexOutOfRangeException"> Thrown when an Index Range error condition occurs. </exception>
        /// <returns>
        /// Returns the object at the read index
        /// <c>without</c> removing it (modifying the index)
        /// </returns>
        public T Peek()
        {
            lock ( this )
            {
                T temp;
                if ( this.Count >= 1 )
                {
                    temp = this._Buffer[this.ReadIndex];
                    return temp;
                }
                else
                {
                    throw new System.IndexOutOfRangeException( "Too few items in circular buffer" );
                }
            } 
        }

        /// <summary> Add single item to the Circular Buffer. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="System.IndexOutOfRangeException"> Thrown when an Index Range error condition occurs. </exception>
        /// <param name="item"> Generic item. </param>
        public void Enqueue( T item )
        {
            if ( this.Count + 1 > this.Capacity - 1 && this.SynchronousMode == true )
            {
                throw new System.IndexOutOfRangeException( "Circular Buffer capacity exceeded--Synchronous Mode" );
            }
            else // Async mode
            {
                this._NumberToAdd = 1;
                this.WriteThis( item );
            }
        }

        /// <summary> Add entire Array[] to the Circular Buffer. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="System.IndexOutOfRangeException"> Thrown when an Index Range error condition occurs. </exception>
        /// <param name="items"> Array of items. </param>
        public void Enqueue( Array items )
        {
            if ( items is null )
                return;
            if ( this.Count + items.Length > this.Capacity - 1 && this.SynchronousMode == true )
            {
                throw new System.IndexOutOfRangeException( "Circular Buffer capacity exceeded--Synchronous Mode" );
            }
            else // Async mode
            {
                this._NumberToAdd = items.Length;
                this.WriteThis( items );
            }
        }

        /// <summary> The count event. </summary>
        private CountEvent _CountEvent;

        /// <summary>
        /// Add a single item to the Circular Buffer but block if no room. Used with synchronous mode
        /// only.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="item"> Generic item. </param>
        public void EnqueueBlocking( T item )
        {
            if ( this.SynchronousMode == true )
            {
                if ( this.Count <= this.Capacity - 1 ) // if room is available
                {
                    this.WriteThis( item ); // write the item
                }
                else
                {
                    this._CountEvent.WaitLoad( 1 );
                    this.WriteThis( item );
                } // write the item

                this._CountEvent.SetCopy( 1 ); // in case waiting for items
            }
            else
            {
                throw new ArgumentException( "must be in synchronous mode" );
            }
        }

        /// <summary>
        /// Add entire Array[] to the Circular Buffer but block if no room. Used with synchronous mode
        /// only.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="items"> Array of items. </param>
        public void EnqueueBlocking( Array items )
        {
            if ( items is null )
                return;
            if ( this.SynchronousMode == true )
            {
                if ( this.Count + items.Length <= this.Capacity - 1 ) // if room is available
                {
                    this.WriteThis( items ); // write the item
                }
                else
                {
                    this._CountEvent.WaitLoad( items.Length );
                    this.WriteThis( items );
                } // write the item

                this._CountEvent.SetCopy( items.Length ); // in case waiting for items
            }
            else
            {
                throw new ArgumentException( "must be in synchronous mode" );
            }
        }

        /// <summary>
        /// Return/Remove single item from the Circular Buffer Will throw exception if no items are in
        /// the Circular Buffer regardless of mode. This and CopyTo(Array, index, number) are the only
        /// queue item removal methods that will check for underflow and throw an exception. Others will
        /// either block or return the number of items they were able to successfully remove.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="System.IndexOutOfRangeException"> Thrown when an Index Range error condition occurs. </exception>
        /// <returns> returns/removes a single object from the Circular Buffer. </returns>
        public T Dequeue()
        {
            return this.Count > 0 ? this.ReadThis() : throw new System.IndexOutOfRangeException( "No items in circular buffer" );
        }

        /// <summary>
        /// Return/Remove single item from the Circular Buffer but block if empty. Must be in synchronous
        /// mode.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <returns> returns/removes a single object from the Circular Buffer. </returns>
        public T DequeueBlocking()
        {
            T tempo;
            if ( this.SynchronousMode == true )
            {
                if ( this.Count <= 0 )
                {
                    this._CountEvent.WaitCopy( 1 );
                }

                tempo = this.ReadThis();
                this._CountEvent.SetLoad( 1 ); // this is for a blocked enqueue
                return tempo;
            }
            else
            {
                throw new ArgumentException( "must be in synchronous mode" );
            }
        }

        /// <summary>
        /// Copy/Remove items from entire Circular Buffer to an Array[] with Array offset of index.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="System.IndexOutOfRangeException"> Thrown when an Index Range error condition occurs. </exception>
        /// <param name="items"> Target Array[]. </param>
        /// <param name="index"> Target Array[] offset. </param>
        /// <returns> Number of items copied/removed from Circular Buffer. </returns>
        public int CopyTo( Array items, int index )
        {
            if ( items is null )
                return this.Count;
            int cacheCount;
            lock ( this )
            {
                cacheCount = this.Count;
                if ( items.Length < cacheCount )
                {
                    throw new System.IndexOutOfRangeException( "Too many items for destination array" );
                }
                else
                {
                    this.ReadThis( items, index, items.Length );
                    return cacheCount;
                }
            }
        }

        /// <summary>
        /// Copy/Remove items from Circular Buffer to an Array[] with Array offset of index. This and
        /// <see cref="Dequeue">dequeue</see> are the only queue item removal methods that will check for
        /// underflow and throw an exception. Others will either block or return the number of items they
        /// were able to successfully remove.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="System.IndexOutOfRangeException"> Thrown when an Index Range error condition occurs. </exception>
        /// <param name="items">     Target Array[]. </param>
        /// <param name="index">     Target Array[] offset. </param>
        /// <param name="itemCount"> Number of items to copy/remove. </param>
        /// <returns> Number of items copied/removed from Circular Buffer. </returns>
        public int CopyTo( Array items, int index, int itemCount )
        {
            if ( items is null )
                return this.Count;
            int cacheCount;
            lock ( this )
            {
                cacheCount = this.Count;
                if ( cacheCount >= itemCount )
                {
                    this.ReadThis( items, index, itemCount );
                }
                else
                {
                    throw new System.IndexOutOfRangeException( "Not enough items in circular buffer" );
                }

                return cacheCount;
            }
        }

        /// <summary> Copy/Remove items from entire Circular Buffer to an Array[]. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="System.IndexOutOfRangeException"> Thrown when an Index Range error condition occurs. </exception>
        /// <param name="items"> Target Array[]. </param>
        /// <returns> Number if items copied/removed from Circular Buffer. </returns>
        public int CopyTo( Array items )
        {
            if ( items is null )
                return this.Count;
            int cacheCount;
            lock ( this )
            {
                cacheCount = this.Count;
                if ( items.Length < cacheCount )
                {
                    throw new System.IndexOutOfRangeException( "Too many items for destination array" );
                }
                else
                {
                    this.ReadThis( items, 0, cacheCount );
                    return cacheCount;
                }
            }
        }

        /// <summary>
        /// Copy/Remove items from entire Circular Buffer to an Array[] with Array offset of index but
        /// block if empty. Must be in synchronous mode.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="System.IndexOutOfRangeException"> Thrown when an Index Range error condition occurs. </exception>
        /// <exception cref="ArgumentException">   Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="items"> Target Array[]. </param>
        /// <param name="index"> Target Array[] offset. </param>
        /// <returns> Number if items copied/removed from Circular Buffer. </returns>
        public int CopyToBlocking( Array items, int index )
        {
            if ( items is null )
                return this.Count;
            int cacheCount;
            if ( this.SynchronousMode == true )
            {
                // returns all available items from the circular buffer 
                // to caller's array
                lock ( this )
                    cacheCount = this.Count;
                if ( items.Length < cacheCount )
                {
                    throw new System.IndexOutOfRangeException( "Too many items for destination array" );
                }
                else if ( cacheCount > 0 )
                {
                    this.ReadThis( items, index, cacheCount );
                    // this is the only reason method is called blocking
                    // this if for a blocked enqueue
                    this._CountEvent.SetLoad( cacheCount );
                }

                return cacheCount;
            }
            else
            {
                throw new ArgumentException( "must be in synchronous mode" );
            }
        }

        /// <summary>
        /// Copy/Remove items from entire Circular Buffer to an Array[] with Array offset of index but
        /// block if empty. Must be in synchronous mode.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="items">     Target Array[]. </param>
        /// <param name="index">     Target Array[] offset. </param>
        /// <param name="itemCount"> number to copy. </param>
        /// <returns> Number if items copied/removed from Circular Buffer. </returns>
        public int CopyToBlocking( Array items, int index, int itemCount )
        {
            if ( items is null )
                return this.Count;
            int cacheCount;
            if ( this.SynchronousMode == true )
            {
                if ( this.Count < itemCount )
                {
                    // since a specific number was asked for.
                    this._CountEvent.WaitCopy( itemCount );
                    cacheCount = itemCount;
                    this.ReadThis( items, index, itemCount );
                }
                else
                {
                    cacheCount = itemCount;
                    this.ReadThis( items, index, itemCount );
                }

                // this if for a blocked enqueue
                this._CountEvent.SetLoad( itemCount );
                return cacheCount;
            }
            else
            {
                throw new ArgumentException( "Must be in synchronous mode" );
            }
        }

        /// <summary>
        /// Copy/Remove items from entire Circular Buffer to an Array[] with Array offset of zero but
        /// block if empty. Must be in synchronous mode.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="System.IndexOutOfRangeException"> Thrown when an Index Range error condition occurs. </exception>
        /// <exception cref="ArgumentException">   Thrown when one or more arguments have unsupported or
        /// illegal values. </exception>
        /// <param name="items"> Target Array[]. </param>
        /// <returns> Number if items copied/removed from Circular Buffer. </returns>
        public int CopyToBlocking( Array items )
        {
            if ( items is null )
                return this.Count;
            int cacheCount;
            if ( this.SynchronousMode == true )
            {
                // returns all available items from the circular buffer 
                // to caller's array
                cacheCount = this.Count;
                if ( items.Length < cacheCount )
                {
                    throw new System.IndexOutOfRangeException( "Too many items for destination array" );
                }
                else if ( cacheCount > 0 )
                {
                    this.ReadThis( items, 0, items.Length );
                    // this is the only reason method is 
                    // called "blocking"
                    // this if for a blocked enqueue
                    this._CountEvent.SetLoad( cacheCount );
                }

                return cacheCount;
            }
            else
            {
                throw new ArgumentException( "Must be in synchronous mode" );
            }
        }

        /// <summary> Displays Circular Buffer contents and or state information. Overridable. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public virtual void DumpCircularBuffer()
        {
        }

        /// <summary> Raise the watermark event by invoking the delegates. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void OnWatermarkNotify()
        {
            try
            {
                // fire the generic event
                var evt = WatermarkNotify;
                evt?.Invoke( this, EventArgs.Empty );
            }
            catch ( Exception )
            {
                Debug.Assert( !Debugger.IsAttached );
            }
        }

        #region " PRIVATE SUPPORT FUNCTIONS "

        /// <summary> Writes the item to the circular collection. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="item"> The item. </param>
        private void WriteThis( T item )
        {
            lock ( this )
            {
                int i = this.WriteIndex;
                int tempCount;
                this._Buffer[i] = item;
                // for speed, we only update the count after the operation
                // but if watermark check is on, we must do it here also
                // but don't bother if nobody has registered for the event
                if ( this.WatermarkCheckingEnabled == true && WatermarkNotify is object )
                {
                    tempCount = i - this.ReadIndex;
                    if ( tempCount < 0 )
                    {
                        tempCount += this._Capacity; // modulo buffer size
                    }

                    if ( tempCount == this.WatermarkCount )
                    {
                        this.OnWatermarkNotify();
                    }
                }

                i += this._Add;
                if ( i < 0 )
                {
                    i += this._Len;
                }

                this.WriteIndex = i;
                this.UpdateCount();
            } // unlock
        }

        /// <summary> Writes the items from the array into the buffer. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="items"> The items. </param>
        private void WriteThis( Array items )
        {
            if ( items is null )
                return;
            lock ( this )
            {
                int i = this.WriteIndex;
                int n = items.Length;
                int offset = 0;
                int tempCount;
                bool tempVar = n > 0;
                n -= 1;
                while ( tempVar )
                {
                    this._Buffer[i] = ( T ) items.GetValue( offset );
                    offset += 1;

                    // for speed, we only update the count after the operation
                    // but if watermark check is on, we must do it here also
                    // but don't bother if nobody has registered for the event
                    if ( this.WatermarkCheckingEnabled == true && WatermarkNotify is object )
                    {
                        tempCount = i - this.ReadIndex;
                        if ( tempCount < 0 )
                        {
                            tempCount += this._Capacity; // modulo buffer size
                        }

                        if ( tempCount == this.WatermarkCount )
                        {
                            this.OnWatermarkNotify();
                        }
                    }

                    i += this._Add;
                    if ( i < 0 )
                    {
                        i += this._Len;
                    }

                    tempVar = n > 0;
                    n -= 1;
                }

                this.WriteIndex = i;
                this.UpdateCount();
            } // unlock
        }

        /// <summary> Reads an item from the circular collection. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> this. </returns>
        private T ReadThis()
        {
            lock ( this )
            {
                int i = this.ReadIndex;
                T temp;
                int tempCount;

                // A modification to the read index may be required
                // if we have been operating in asynchronous saturated mode
                if ( this.SynchronousMode == false )
                {
                    if ( this.Count >= this.Capacity - 1 )
                    {
                        i = (this.ReadIndex - 1) % this.Capacity;
                    }
                }

                temp = this._Buffer[i];

                // for speed, we only update the count after the operation
                // but if watermark check is on, we must do it here also
                // but don't bother if nobody has registered for the event
                if ( this.WatermarkCheckingEnabled == true && WatermarkNotify is object )
                {
                    tempCount = this.ReadIndex - i; // change
                    if ( tempCount < 0 )
                    {
                        tempCount += this._Capacity; // modulo buffer size
                    }

                    if ( tempCount == this.WatermarkCount )
                    {
                        this.OnWatermarkNotify();
                    }
                }

                i += this._Add;
                if ( i < 0 )
                {
                    i += this._Len;
                }

                this.ReadIndex = i;
                this.UpdateCount();
                return temp;
            } // unlock
        }

        /// <summary> Reads the specified items from the circular buffer into the array. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <exception cref="System.IndexOutOfRangeException"> Thrown when an Index Range error condition occurs. </exception>
        /// <param name="items">      The items. </param>
        /// <param name="index">      The index. </param>
        /// <param name="itemsCount"> The items count. </param>
        private void ReadThis( Array items, int index, int itemsCount )
        {
            if ( items is null )
                return;
            lock ( this )
            {
                int i = this.ReadIndex;
                int n = itemsCount - index;
                int offset = index;
                int tempCount;

                // A modification to the read index may be required
                // if we have been operating in asynchronous saturated mode
                // if(SynchronousMode==false)
                // if(Count >= (capacity-1))
                // I = (n.r.p-1)%capacity;
                if ( this.Count >= n )
                {
                    bool tempVar = n > 0;
                    n -= 1;
                    while ( tempVar )
                    {
                        items.SetValue( this._Buffer[i], offset );
                        offset += 1;
                        // for speed, we only update the count after the 
                        // operation but if watermark check is on, we must 
                        // do it here also but don't bother if nobody has
                        // registered for the event
                        if ( this.WatermarkCheckingEnabled == true && WatermarkNotify is object )
                        {
                            tempCount = this.ReadIndex - i; // change
                            if ( tempCount < 0 )
                            {
                                tempCount += this._Capacity; // modulo buffer size
                            }

                            if ( tempCount == this.WatermarkCount )
                            {
                                this.OnWatermarkNotify();
                            }
                        }

                        i += this._Add;
                        if ( i < 0 )
                        {
                            i += this._Len;
                        }

                        tempVar = n > 0;
                        n -= 1;
                    }
                }
                else
                {
                    throw new System.IndexOutOfRangeException( "Too few items in circular buffer" );
                }

                this.ReadIndex = i;
                this.UpdateCount();
            } // unlock
        }

        /// <summary> Updates the count. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        private void UpdateCount()
        {
            if ( this.SynchronousMode == true )
            {
                this.Count = this.WriteIndex - this.ReadIndex;
                if ( this.Count < 0 )
                {
                    this.Count += this._Capacity; // modulo buffer size
                }
            }
            // if asynchronousMode, adjust the read index to follow the 
            // write index if buffer is full (saturation)
            // check to see if we are in saturation
            else if ( this.Count + this._NumberToAdd >= this.Capacity - 1 )
            {
                this.ReadIndex = (this.WriteIndex + this._NumberToAdd) % this.Capacity;
                // and Count remains at max capacity
                this.Count = this.Capacity - 1;
            }
            else
            {
                this.Count = this.WriteIndex - this.ReadIndex;
                if ( this.Count < 0 )
                {
                    this.Count += this._Capacity; // modulo buffer size
                }
            }
        }
        #endregion

        #region " IEnumerator Support "

        /// <summary> IEnumerator support. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> The enumerator. </returns>
        public CircularCollectionEnumerator GetEnumerator()
        {
            return new CircularCollectionEnumerator( this );
        }

        /// <summary> Enumerable get enumerator. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <returns> An IEnumerator. </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        /// <summary> Enumerator class for the circular Collection. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public class CircularCollectionEnumerator : IEnumerator
        {

            /// <summary> The buffer. </summary>
            private readonly CircularCollection<T> _Buffer;

            /// <summary> Constructor. </summary>
            /// <remarks> David, 2020-10-22. </remarks>
            /// <param name="buffer"> The buffer. </param>
            internal CircularCollectionEnumerator( CircularCollection<T> buffer )
            {
                this._Buffer = buffer;
            }

            // IEnumerator support

            /// <summary> IEnumerator support. </summary>
            /// <remarks> David, 2020-10-22. </remarks>
            /// <returns>
            /// <see langword="true" /> if the enumerator was successfully advanced to the next element;
            /// <see langword="false" /> if the enumerator has passed the end of the collection.
            /// </returns>
            public bool MoveNext()
            {
                lock ( this )
                    return this._Buffer.Count != 0;
            }

            /// <summary> IEnumerator support. </summary>
            /// <remarks> David, 2020-10-22. </remarks>
            public void Reset()
            {
            }

            /// <summary> Gets the current element in the collection. </summary>
            /// <remarks> Client must set through method. </remarks>
            /// <value> The current element in the collection. </value>
            object IEnumerator.Current
            {
                get {
                    lock ( this )
                    {
                        int i = this._Buffer.ReadIndex;
                        T temp;

                        // A modification to the read index may be required
                        // if we have been operating in asynchronous 
                        // saturated mode
                        if ( this._Buffer.SynchronousMode == false )
                        {
                            if ( this._Buffer.Count >= this._Buffer.Capacity - 1 )
                            {
                                i = (this._Buffer.ReadIndex - 1) % this._Buffer.Capacity;
                            }
                        }

                        temp = this._Buffer._Buffer[i];
                        i += this._Buffer._Add;
                        if ( i < 0 )
                        {
                            i += this._Buffer._Len;
                        }

                        this._Buffer.ReadIndex = i;
                        this._Buffer.UpdateCount();
                        return temp;
                    } // unlock
                }
            }

            /// <summary> Gets the current element in the collection. </summary>
            /// <value> The current element in the collection. </value>
            public T Current => this.Current;

        } // end class CBEnumerator

          #endregion


    }

        /// <summary> Blocking Support Class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
    internal class CountEvent : IDisposable
    {

        /// <summary> Initializes a new instance of the <see cref="CountEvent" /> class. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        internal CountEvent() : base()
        {
            this._LoadAutoEvent = new AutoResetEvent( false );
            this._CopyAutoEvent = new AutoResetEvent( false );
            this._CopyEventCount = 0;
            this._LoadEventCount = 0;
        }

        /// <summary> The load automatic event. </summary>
        private AutoResetEvent _LoadAutoEvent;

        /// <summary> The copy automatic event. </summary>
        private AutoResetEvent _CopyAutoEvent;

        /// <summary> Number of load events. </summary>
        private int _LoadEventCount = 0;

        /// <summary> Number of copy events. </summary>
        private int _CopyEventCount = 0;

        /// <summary> Requests to continue when the number specified can be written. </summary>
        /// <remarks> WaitLoad() and SetLoad() are part of a blocked enqueue operation. </remarks>
        /// <param name="requestedCount"> The requested count. </param>
        internal void WaitLoad( int requestedCount )
        {
            lock ( this )
                this._LoadEventCount = requestedCount; // wait for count to reach this value
            _ = this._LoadAutoEvent.WaitOne();
        }

        /// <summary> Updates the count as items are removed from the circular buffer. </summary>
        /// <remarks> WaitLoad() and SetLoad() are part of a blocked enqueue operation. </remarks>
        /// <param name="requestedCount"> The requested count. </param>
        internal void SetLoad( int requestedCount )
        {
            lock ( this )
            {
                if ( this._LoadEventCount > 0 )
                {
                    this._LoadEventCount -= requestedCount; // queue has been emptied by this amount
                    if ( this._LoadEventCount <= 0 )
                    {
                        _ = this._LoadAutoEvent.Set();
                    }
                }
            }
        }

        /// <summary> Requests to continue when the number specified can be read. </summary>
        /// <remarks> WaitCopy() and SetCopy() are part of a blocked dequeue operation. </remarks>
        /// <param name="requestedCount"> The requested count. </param>
        internal void WaitCopy( int requestedCount )
        {
            lock ( this )
                this._CopyEventCount = requestedCount; // countdown this value
            _ = this._CopyAutoEvent.WaitOne();
        }

        /// <summary> Updates the count as items are written into the circular buffer. </summary>
        /// <remarks> WaitCopy() and SetCopy() are part of a blocked dequeue operation. </remarks>
        /// <param name="requestedCount"> The requested count. </param>
        internal void SetCopy( int requestedCount )
        {
            lock ( this )
            {
                if ( this._CopyEventCount > 0 )
                {
                    this._CopyEventCount -= requestedCount; // queue has been emptied by this amount
                    if ( this._CopyEventCount <= 0 )
                    {
                        _ = this._CopyAutoEvent.Set();
                    }
                }
            }
        }

        #region "IDisposable Support"

        /// <summary> Gets or sets a value indicating whether this instance is disposed. </summary>
        /// <value> <c>True</c> if this instance is disposed; otherwise, <c>False</c>. </value>
        protected bool IsDisposed { get; set; }

        /// <summary> Releases unmanaged and - optionally - managed resources. </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        /// <param name="disposing"> <c>True</c> to release both managed and unmanaged resources;
        /// <c>False</c> to release only unmanaged resources. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        protected virtual void Dispose( bool disposing )
        {
            try
            {
                if ( !this.IsDisposed )
                {
                    if ( disposing )
                    {
                        if ( this._LoadAutoEvent is object )
                        {
                            this._LoadAutoEvent.Dispose();
                            this._LoadAutoEvent = null;
                        }

                        if ( this._CopyAutoEvent is object )
                        {
                            this._CopyAutoEvent.Dispose();
                            this._CopyAutoEvent = null;
                        }
                    }
                    // TO_DO: free unmanaged resources (unmanaged objects) and override Finalize() below.
                    // TO_DO: set large fields to null.
                }
            }
            catch
            {
            }
            finally
            {
                this.IsDisposed = true;
            }
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
        /// resources.
        /// </summary>
        /// <remarks> David, 2020-10-22. </remarks>
        public void Dispose()
        {
            // Do not change this code.  Put cleanup code in Dispose(disposing As Boolean) above.
            this.Dispose( true );
            GC.SuppressFinalize( this );
        }
        #endregion

    }
}
