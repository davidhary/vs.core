using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Threading;

namespace isr.Core.Composites
{
    /// <summary>   Circular Buffer. </summary>
    /// <remarks>   David, 2020-09-09.
    /// From: https://archive.codeplex.com/?p=circularbuffer </remarks>
    /// <license> (c) 2012 Alex Regueiro.<para>
    /// Licensed under The MIT License. </para><para>
    /// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
    /// BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    /// NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
    /// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    /// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    /// </para> </license>
    /// <typeparamref name="T">    Generic type parameter. </typeparamref>
    public class CircularBuffer<T> : ICollection<T>, IEnumerable<T>, ICollection, IEnumerable
    {

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="capacity"> The capacity. </param>
        public CircularBuffer( int capacity )
            : this( capacity, false )
        {
        }

        /// <summary>   Constructor. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <exception cref="ArgumentException">    Thrown when one or more arguments have unsupported or
        ///                                         illegal values. </exception>
        /// <param name="capacity">         The capacity. </param>
        /// <param name="allowOverflow">    True if allow overflow, false if not. </param>
        public CircularBuffer( int capacity, bool allowOverflow )
        {
            if ( capacity < 0 )
            {
                throw new ArgumentException( Properties.Resources.CircularBufferZeroCapacity, $"{nameof( capacity )}" );
            }

            Contract.EndContractBlock();

            this._Capacity = capacity;
            this.Size = 0;
            this.Head = 0;
            this.Tail = 0;
            this._Buffer = new T[capacity];
            this.AllowOverflow = allowOverflow;
        }


        /// <summary>   The buffer. </summary>
        private T[] _Buffer;

        /// <summary>   Gets or sets the head: First valid element in the buffer </summary>
        /// <value> The head. </value>
        public int Head { get; private set; }

        /// <summary>   Gets or sets the tail. </summary>
        /// <value> The tail. </value>
        public int Tail { get; private set; }

        /// <summary>   Gets or sets a value indicating whether the buffer overflew. </summary>
        /// <value> True if buffer overflow, false if not. </value>
        public bool BufferOverflow { get; private set; }

        /// <summary>   The synchronize root. </summary>
        [NonSerialized()]
        private object _SyncRoot;

        /// <summary>   Gets or sets a value indicating whether buffer overflow is allowed. </summary>
        /// <remarks>
        /// the buffer overflows it the buffer <see cref="Size">number of items not yet read</see>
        /// exceeds the buffer <see cref="Capacity"/>
        /// </remarks>
        /// <value> True if allow overflow, false if not. </value>
        public bool AllowOverflow
        {
            get;
            set;
        }

        /// <summary>   The capacity. </summary>
        private int _Capacity;

        /// <summary>   Gets or sets the capacity. </summary>
        /// <exception cref="ArgumentOutOfRangeException">  Thrown when one or more arguments are outside
        ///                                                 the required range. </exception>
        /// <value> The capacity. </value>
        public int Capacity
        {
            get => this._Capacity;
            set {
                if ( value == this._Capacity )
                {
                    return;
                }

                if ( value < this.Size )
                {
                    throw new ArgumentOutOfRangeException( $"{nameof( value )}", Properties.Resources.CircularBufferCapacityTooSmall );
                }

                var dst = new T[value];
                if ( this.Size > 0 )
                {
                    this.CopyTo( dst );
                }

                this._Buffer = dst;

                this._Capacity = value;
            }
        }

        /// <summary>   Gets or sets the number of unread items. </summary>
        /// <value> The number of unread items. </value>
        public int Size { get; private set; }

        /// <summary>
        /// Determines whether the <see cref="T:System.Collections.Generic.ICollection`1" /> contains a
        /// specific value.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="item"> The object to locate in the
        ///                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="item" /> is found in the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
        /// <see langword="false" />.
        /// </returns>
        public bool Contains( T item )
        {
            int bufferIndex = this.Head;
            var comparer = EqualityComparer<T>.Default;
            for ( int i = 0; i < this.Size; i++, bufferIndex++ )
            {
                if ( bufferIndex == this._Capacity )
                {
                    bufferIndex = 0;
                }

                if ( item == null && this._Buffer[bufferIndex] == null )
                {
                    return true;
                }
                else if ( (this._Buffer[bufferIndex] != null) &&
                    comparer.Equals( this._Buffer[bufferIndex], item ) )
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Removes all items from the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        public void Clear()
        {
            this.Size = 0;
            this.Head = 0;
            this.Tail = 0;
            this.BufferOverflow = false;
        }

        /// <summary>   Puts the given items into the buffer <see cref="Tail"/>. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="source">  Source for the. </param>
        /// <returns>   The number of items. </returns>
        public int Put( T[] source )
        {
            return this.Put( source, 0, source.Length );
        }

        /// <summary>   Puts the given items into the buffer <see cref="Tail"/>. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="source">      Source for the items. </param>
        /// <param name="offset">   The offset. </param>
        /// <param name="count">    Number of items to put from the source. </param>
        /// <returns>   The number of items. </returns>
        public int Put( T[] source, int offset, int count )
        {
            if ( !this.AllowOverflow && count > this._Capacity - this.Size )
            {
                throw new InvalidOperationException( Properties.Resources.CircularBufferOverflow );
            }
            this.BufferOverflow = count > this._Capacity - this.Size;

            int srcIndex = offset;
            for ( int i = 0; i < count; i++, this.Tail++, srcIndex++ )
            {
                if ( this.Tail == this._Capacity )
                {
                    this.Tail = 0;
                }

                this._Buffer[this.Tail] = source[srcIndex];
            }
            this.Size = Math.Min( this.Size + count, this._Capacity );
            return count;
        }

        /// <summary>   Puts the given item into the buffer <see cref="Tail"/>. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <param name="item"> The object to add to the
        ///                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        public void Put( T item )
        {
            this.BufferOverflow = this.Size >= this.Capacity;
            if ( !this.AllowOverflow && this.BufferOverflow )
            {
                throw new InvalidOperationException( Properties.Resources.CircularBufferOverflow );
            }

            this._Buffer[this.Tail] = item;
            if ( ++this.Tail == this._Capacity )
            {
                this.Tail = 0;
            }

            this.Size++;
        }

        /// <summary>   Skips. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="count">    Number of items to skip. </param>
        public void Skip( int count )
        {
            this.Head += count;
            if ( this.Head >= this.Capacity )
            {
                this.Head -= this.Capacity;
            }
        }

        /// <summary>   Gets the get. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="count">    Number of. </param>
        /// <returns>   A T. </returns>
        public T[] Get( int count )
        {
            var dst = new T[count];
            _ = this.Get( dst );
            return dst;
        }

        /// <summary>   Gets items from the buffer. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="dst">  Destination for the. </param>
        /// <returns>   A T. </returns>
        public int Get( T[] dst )
        {
            return this.Get( dst, 0, dst.Length );
        }

        /// <summary>   Gets items from the buffer. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="dst">      Destination for the. </param>
        /// <param name="offset">   The offset. </param>
        /// <param name="count">    Number of. </param>
        /// <returns>   A T. </returns>
        public int Get( T[] dst, int offset, int count )
        {
            int realCount = Math.Min( count, this.Size );
            int dstIndex = offset;
            for ( int i = 0; i < realCount; i++, this.Head++, dstIndex++ )
            {
                if ( this.Head == this._Capacity )
                {
                    this.Head = 0;
                }

                dst[dstIndex] = this._Buffer[this.Head];
            }
            this.Size -= realCount;
            return realCount;
        }

        /// <summary>   Gets an item from the buffer. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <returns>   A T. </returns>
        public T Get()
        {
            if ( this.Size == 0 )
            {
                throw new InvalidOperationException( Properties.Resources.CircularBufferEmpty );
            }

            var item = this._Buffer[this.Head];
            if ( ++this.Head == this._Capacity )
            {
                this.Head = 0;
            }

            this.Size--;
            return item;
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an
        /// <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="array">    The one-dimensional <see cref="T:System.Array" /> that is the
        ///                         destination of the elements copied from
        ///                         <see cref="T:System.Collections.Generic.ICollection`1" />. The
        ///                         <see cref="T:System.Array" /> must have zero-based indexing. </param>
        public void CopyTo( T[] array )
        {
            this.CopyTo( array, 0 );
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an
        /// <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="array">        The one-dimensional <see cref="T:System.Array" /> that is the
        ///                             destination of the elements copied from
        ///                             <see cref="T:System.Collections.Generic.ICollection`1" />. The
        ///                             <see cref="T:System.Array" /> must have zero-based indexing. </param>
        /// <param name="arrayIndex">   The zero-based index in <paramref name="array" /> at which
        ///                             copying begins. </param>
        public void CopyTo( T[] array, int arrayIndex )
        {
            this.CopyTo( 0, array, arrayIndex, this.Size );
        }

        /// <summary>   Copies to. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <exception cref="ArgumentOutOfRangeException">  Thrown when one or more arguments are outside
        ///                                                 the required range. </exception>
        /// <param name="index">        Zero-based index of the. </param>
        /// <param name="array">        The one-dimensional <see cref="T:System.Array" /> that is the
        ///                             destination of the elements copied from
        ///                             <see cref="T:System.Collections.ICollection" />. The
        ///                             <see cref="T:System.Array" /> must have zero-based indexing. </param>
        /// <param name="arrayIndex">   The zero-based index in <paramref name="array" /> at which
        ///                             copying begins. </param>
        /// <param name="count">        Number of. </param>
        public void CopyTo( int index, T[] array, int arrayIndex, int count )
        {
            if ( count > this.Size )
            {
                throw new ArgumentOutOfRangeException( $"{nameof( count )}", Properties.Resources.CircularBufferReadCountTooLarge );
            }

            int bufferIndex = this.Head + index % this.Capacity;
            for ( int i = 0; i < count; i++, bufferIndex++, arrayIndex++ )
            {
                if ( bufferIndex == this.Capacity )
                {
                    bufferIndex = 0;
                }

                array[arrayIndex] = this._Buffer[bufferIndex];
            }
        }

        /// <summary>   Returns an enumerator that iterates through the collection. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>   An enumerator that can be used to iterate through the collection. </returns>
        public IEnumerator<T> GetEnumerator()
        {
            int bufferIndex = this.Head;
            for ( int i = 0; i < this.Size; i++, bufferIndex++ )
            {
                if ( bufferIndex == this._Capacity )
                {
                    bufferIndex = 0;
                }

                yield return this._Buffer[bufferIndex];
            }
        }

        /// <summary>   Gets the buffer. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>   An array of t. </returns>
        public T[] GetBuffer()
        {
            return this._Buffer;
        }

        /// <summary>   Convert this object into an array representation. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>   An array that represents the data in this object. </returns>
        public T[] ToArray()
        {
            var dst = new T[this.Size];
            this.CopyTo( dst );
            return dst;
        }

        #region ICollection<T> Members

        /// <summary>
        /// Gets the number of elements contained in the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <typeparamref name="T">    Generic type parameter. </typeparamref>
        /// <value>
        /// The number of elements contained in the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </value>
        int ICollection<T>.Count => this.Size;

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" />
        /// is read-only.
        /// </summary>
        /// <typeparamref name="T">    Generic type parameter. </typeparamref>
        /// <value>
        /// <see langword="true" /> if the <see cref="T:System.Collections.Generic.ICollection`1" /> is
        /// read-only; otherwise, <see langword="false" />.
        /// </value>
        bool ICollection<T>.IsReadOnly => false;

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <typeparamref name="T">    Generic type parameter. </typeparamref>
        /// <param name="item"> The object to add to the
        ///                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        void ICollection<T>.Add( T item )
        {
            this.Put( item );
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <typeparamref name="T">    Generic type parameter. </typeparamref>
        /// <param name="item"> The object to remove from the
        ///                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="item" /> was successfully removed from the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
        /// <see langword="false" />. This method also returns <see langword="false" /> if
        /// <paramref name="item" /> is not found in the original
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        bool ICollection<T>.Remove( T item )
        {
            if ( this.Size == 0 )
            {
                return false;
            }

            _ = this.Get();
            return true;
        }

        #endregion

        #region IEnumerable<T> Members

        /// <summary>   Returns an enumerator that iterates through the collection. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <typeparamref name="T">    Generic type parameter. </typeparamref>
        /// <returns>   An enumerator that can be used to iterate through the collection. </returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return this.GetEnumerator();
        }

        #endregion

        #region ICollection Members

        /// <summary>
        /// Gets the number of elements contained in the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <value>
        /// The number of elements contained in the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </value>
        int ICollection.Count => this.Size;

        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" />
        /// is synchronized (thread safe).
        /// </summary>
        /// <value>
        /// <see langword="true" /> if access to the <see cref="T:System.Collections.ICollection" /> is
        /// synchronized (thread safe); otherwise, <see langword="false" />.
        /// </value>
        bool ICollection.IsSynchronized => false;

        /// <summary>
        /// Gets an object that can be used to synchronize access to the
        /// <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        /// <value>
        /// An object that can be used to synchronize access to the
        /// <see cref="T:System.Collections.ICollection" />.
        /// </value>
        object ICollection.SyncRoot
        {
            get {
                if ( this._SyncRoot == null )
                {
                    _ = Interlocked.CompareExchange( ref this._SyncRoot, new object(), null );
                }

                return this._SyncRoot;
            }
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an
        /// <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="array">        The one-dimensional <see cref="T:System.Array" /> that is the
        ///                             destination of the elements copied from
        ///                             <see cref="T:System.Collections.ICollection" />. The
        ///                             <see cref="T:System.Array" /> must have zero-based indexing. </param>
        /// <param name="arrayIndex">   The zero-based index in <paramref name="array" /> at which
        ///                             copying begins. </param>
        void ICollection.CopyTo( Array array, int arrayIndex )
        {
            this.CopyTo( ( T[] ) array, arrayIndex );
        }

        #endregion

        #region IEnumerable Members

        /// <summary>   Returns an enumerator that iterates through a collection. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> object that can be used to iterate through
        /// the collection.
        /// </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return ( IEnumerator ) this.GetEnumerator();
        }

        #endregion
    }
}
