using System;
using System.Security.Permissions;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Diagnostics.Contracts;
using System.Collections.Generic;
using System.Collections;

namespace isr.Core.Composites
{

    /// <summary>
    /// Queue of items of <typeparamref name="T" />. Internally it is implemented as
    /// a circular buffer, so Enqueue can be O(n). Dequeue is O(1).
    /// </summary>
    /// <remarks> David, 2020-09-09. <para>
    /// (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    [DebuggerTypeProxy( typeof( GenericQueue<>.GenericQueueDebugView ) )]
    [DebuggerDisplay( "Count = {Count}" )]
    public class GenericQueue<T> : ICollection<T>, IEnumerable<T>, ICollection, IEnumerable
    {

        /// <summary>
        /// Creates a queue of items of <typeparamref name="T"/>. The default initial capacity and grow factor
        /// are used.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        public GenericQueue()
            : this( GenericQueue<T>._DefaultCapacity, GenericQueue<T>._DefaultGrowFactor )
        {
        }

        /// <summary>
        /// Creates a queue of items of <typeparamref name="T"/>. The default grow factor is used.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="capacity"> The capacity. </param>
        public GenericQueue( int capacity )
            : this( capacity, GenericQueue<T>._DefaultGrowFactor )
        {
        }

        /// <summary>
        /// Creates a queue of items of <typeparamref name="T"/>. When full, the new capacity is set to the old
        /// capacity * growFactor.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <exception cref="ArgumentOutOfRangeException">  Thrown when one or more arguments are outside
        ///                                                 the required range. </exception>
        /// <param name="capacity">     The capacity. </param>
        /// <param name="growFactor">   100 == 1.0, 130 == 1.3, 200 == 2.0. </param>
        public GenericQueue( int capacity, float growFactor )
        {
            if ( capacity < 0 )
            {
                throw new ArgumentOutOfRangeException( nameof( capacity ), $"Capacity {capacity} must not be negative" );
            }

            if ( !(growFactor >= GenericQueue<T>._GrowFactorRange.Min && growFactor <= GenericQueue<T>._GrowFactorRange.Max) )
            {
                throw new ArgumentOutOfRangeException( nameof( growFactor ), $"Queue Grow Factor {growFactor} out of range of [{GenericQueue<T>._GrowFactorRange.Min},{GenericQueue<T>._GrowFactorRange.Max}]" );
            }

            Contract.EndContractBlock();

            this._Array = new T[capacity];
            this.Head = 0;
            this.Tail = 0;
            this.Size = 0;
            this.GrowFactor = ( int ) (growFactor * 100);
        }

        /// <summary>
        /// Fills a Queue with the items of an ICollection.  Uses the enumerator to get each of the
        /// items.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="col">  The col. </param>
        public GenericQueue( ICollection<T> col ) : this( (col == null ? GenericQueue<T>._DefaultCapacity : col.Count) )
        {
            if ( col == null )
            {
                throw new ArgumentNullException( nameof( col ) );
            }

            Contract.EndContractBlock();

            IEnumerator<T> en = col.GetEnumerator();
            while ( en.MoveNext() )
            {
                this.Enqueue( en.Current );
            }
        }

        /// <summary>   Creates a new <see cref="GenericQueue{T}"/> that is a copy of the current instance. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>   A new <see cref="GenericQueue{T}"/> that is a copy of this instance. </returns>
        public virtual GenericQueue<T> CreateCopy()
        {
            GenericQueue<T> q = new ( this.Size ) { Size = this.Size };

            int numToCopy = this.Size;
            int firstPart = (this._Array.Length - this.Head < numToCopy) ? this._Array.Length - this.Head : numToCopy;
            Array.Copy( this._Array, this.Head, q._Array, 0, firstPart );
            numToCopy -= firstPart;
            if ( numToCopy > 0 )
            {
                Array.Copy( this._Array, 0, q._Array, this._Array.Length - this.Head, numToCopy );
            }

            q.Version = this.Version;
            return q;
        }

        /// <summary>   Makes a deep copy of this <see cref="GenericQueue{T}"/> . </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>   A copy of this <see cref="GenericQueue{T}"/> . </returns>
        public virtual Object Clone()
        {
            {
                return this.CreateCopy();
            }
        }


        /// <summary>   The array. </summary>
        private T[] _Array;

        /// <summary>   Gets or sets the head: First valid element in the queue. </summary>
        /// <value> The head. </value>
        public int Head { get; private set; }

        /// <summary>   Gets or sets the tail: Last valid element in the queue. </summary>
        /// <value> The tail. </value>
        public int Tail { get; private set; }

        /// <summary>   100 == 1.0, 130 == 1.3, 200 == 2.0. </summary>
        private int GrowFactor { get; set; }

        /// <summary>   The version. </summary>
        private long Version { get; set; }

        /// <summary>   Increments version. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="value">    The value. </param>
        private void IncrementVersion( int value )
        {
            this.Version = (this.Version + value) % int.MaxValue;
        }

        /// <summary>   The synchronize root. </summary>
        [NonSerialized]
        private Object _SyncRoot;

        /// <summary>   The minimum growth. </summary>
        private const int _MinimumGrow = 4;

        /// <summary>   The default grow factor. </summary>
        private const float _DefaultGrowFactor = 2f;

        /// <summary>   The default capacity. </summary>
        private const int _DefaultCapacity = 32;

        /// <summary>   The grow factor range. </summary>
        private static (float Min, float Max) _GrowFactorRange = (1.0f, 10.0f);

        /// <summary>   Gets or sets the size: Number of items in the queue. </summary>
        /// <value> The size. </value>
        private int Size { get; set; }

        /// <summary>
        /// Gets the number of items contained in the <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        /// <value>
        /// The number of items contained in the <see cref="T:System.Collections.ICollection" />.
        /// </value>
        public virtual int Count => this.Size;

        /// <summary>
        /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" />
        /// is synchronized (thread safe).
        /// </summary>
        /// <value>
        /// <see langword="true" /> if access to the <see cref="T:System.Collections.ICollection" /> is
        /// synchronized (thread safe); otherwise, <see langword="false" />.
        /// </value>
        public virtual bool IsSynchronized => false;

        /// <summary>
        /// Gets an object that can be used to synchronize access to the
        /// <see cref="T:System.Collections.ICollection" />.
        /// </summary>
        /// <value>
        /// An object that can be used to synchronize access to the
        /// <see cref="T:System.Collections.ICollection" />.
        /// </value>
        public virtual Object SyncRoot
        {
            get {
                if ( this._SyncRoot == null )
                {
                    _ = System.Threading.Interlocked.CompareExchange( ref this._SyncRoot, new Object(), null );
                }
                return this._SyncRoot;
            }
        }

        /// <summary>   Removes all items from the queue. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        public virtual void Clear()
        {
            if ( this.Head < this.Tail )
            {
                Array.Clear( this._Array, this.Head, this.Size );
            }
            else
            {
                Array.Clear( this._Array, this.Head, this._Array.Length - this.Head );
                Array.Clear( this._Array, 0, this.Tail );
            }

            this.Head = 0;
            this.Tail = 0;
            this.Size = 0;
            this.IncrementVersion( 1 );
        }

        /// <summary>   Gets the capacity. </summary>
        /// <value> The capacity. </value>
        public int Capacity => this._Array.Length;

        /// <summary>
        /// CopyTo copies a collection into an Array, starting at a particular index into the array.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <exception cref="ArgumentNullException">        Thrown when one or more required arguments
        ///                                                 are null. </exception>
        /// <exception cref="ArgumentException">            Thrown when one or more arguments have
        ///                                                 unsupported or illegal values. </exception>
        /// <exception cref="ArgumentOutOfRangeException">  Thrown when one or more arguments are outside
        ///                                                 the required range. </exception>
        /// <param name="array">    The one-dimensional <see cref="T:System.Array" /> that is the
        ///                         destination of the items copied from
        ///                         <see cref="T:System.Collections.ICollection" />. The
        ///                         <see cref="T:System.Array" /> must have zero-based indexing. </param>
        /// <param name="index">    The zero-based index in <paramref name="array" /> at which copying
        ///                         begins. </param>
        public virtual void CopyTo( T[] array, int index )
        {
            if ( array == null )
            {
                throw new ArgumentNullException( $"{nameof( array )}" );
            }

            if ( array.Rank != 1 )
            {
                throw new ArgumentException( $"Multi-dimensional array is not Not Supported" );
            }

            if ( index < 0 )
            {
                throw new ArgumentOutOfRangeException( $"{nameof( index )}", $"index {index} argument is out of range" );
            }

            Contract.EndContractBlock();

            int arrayLen = array.Length;
            if ( arrayLen - index < this.Size )
            {
                throw new ArgumentException( $"Invalid offset length; array length {arrayLen} - {index} < {this.Size}" );
            }

            int numToCopy = this.Size;
            if ( numToCopy == 0 )
            {
                return;
            }

            int firstPart = (this._Array.Length - this.Head < numToCopy) ? this._Array.Length - this.Head : numToCopy;
            Array.Copy( this._Array, this.Head, array, index, firstPart );
            numToCopy -= firstPart;
            if ( numToCopy > 0 )
            {
                Array.Copy( this._Array, 0, array, index + this._Array.Length - this.Head, numToCopy );
            }
        }

        /// <summary>   Adds an item to the tail of the queue. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="item">  The item to add. </param>
        public virtual void Enqueue( T item )
        {
            if ( this.Size == this._Array.Length )
            {
                int newcapacity = ( int ) (( long ) this._Array.Length * ( long ) this.GrowFactor / 100);
                if ( newcapacity < this._Array.Length + _MinimumGrow )
                {
                    newcapacity = this._Array.Length + _MinimumGrow;
                }
                this.SetCapacity( newcapacity );
            }

            this._Array[this.Tail] = item;
            this.Tail = (this.Tail + 1) % this._Array.Length;
            this.Size++;
            this.IncrementVersion( 1 );
        }

        /// <summary>   Adds obj to the tail of the queue. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="source">   Source for the. </param>
        /// <returns>   An int. </returns>
        public int Enqueue( T[] source )
        {
            return this.Enqueue( source, 0, source.Length );
        }

        /// <summary>   Adds obj to the tail of the queue. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="source">   Source for the. </param>
        /// <param name="offset">   The offset. </param>
        /// <param name="count">    The number of items contained in the
        ///                         <see cref="T:System.Collections.ICollection" />. </param>
        /// <returns>   An int. </returns>
        public int Enqueue( T[] source, int offset, int count )
        {
            if ( count >= this._Array.Length )
            {
                int newcapacity = count + _MinimumGrow;
                this.SetCapacity( newcapacity );
            }

            int srcIndex = offset;
            for ( int i = 0; i < count; i++, this.Tail++, srcIndex++ )
            {
                if ( this.Tail == this.Capacity )
                {
                    this.Tail = 0;
                }
                this._Array[this.Tail] = source[srcIndex];
            }
            this.Size = Math.Min( this.Size + count, this.Capacity );
            this.IncrementVersion( 1 );
            return count;
        }

        /// <summary>   Skips; Moves the head forward by the specified count. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="count">    Number of items to skip. </param>
        public void Skip( int count )
        {
            this.Head += count;
            if ( this.Head >= this.Capacity )
            {
                this.Head -= this.Capacity;
            }
        }

        /// <summary>
        /// GetEnumerator returns an IEnumerator over this Queue.  This Enumerator will support removing.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>
        /// An <see cref="T:System.Collections.IEnumerator" /> that can be used to iterate through
        /// the collection.
        /// </returns>
        public virtual IEnumerator GetEnumerator()
        {
            return new QueueEnumerator( this );
        }

        /// <summary>
        /// Removes the item at the <see cref="Head"/> of the queue and returns it. If the queue is empty, this
        /// method simply returns null.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <returns>   The head item from this queue. </returns>
        public virtual T Dequeue()
        {
            if ( this.Count == 0 )
            {
                throw new InvalidOperationException( "Queue is empty" );
            }

            Contract.EndContractBlock();

            T removed = this._Array[this.Head];
            // this._Array[this.Head] = default;
            this.Head = (this.Head + 1) % this._Array.Length;
            this.Size--;
            this.IncrementVersion( 1 );
            return removed;
        }

        /// <summary>   Removes the <see cref="Head"/> items from this queue. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="dst">  Destination for the. </param>
        /// <returns>   The <see cref="Head"/> items from this queue. </returns>
        public int Dequeue( T[] dst )
        {
            return this.Dequeue( dst, 0, dst.Length );
        }

        /// <summary>   Removes the <see cref="Head"/> items from this queue. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="dst">      Destination for the. </param>
        /// <param name="offset">   The offset. </param>
        /// <param name="count">    The number of items contained in the
        ///                         <see cref="T:System.Collections.ICollection" />. </param>
        /// <returns>   The head items from this queue. </returns>
        public int Dequeue( T[] dst, int offset, int count )
        {
            int realCount = Math.Min( count, this.Size );
            int dstIndex = offset;
            for ( int i = 0; i < realCount; i++, this.Head++, dstIndex++ )
            {
                if ( this.Head == this.Capacity )
                {
                    this.Head = 0;
                }

                dst[dstIndex] = this._Array[this.Head];
            }
            this.Size -= realCount;
            this.IncrementVersion( 1 );
            return realCount;
        }


        /// <summary>
        /// Returns the item at the <see cref="Head"/> of the queue. The item remains in the queue. If
        /// the queue is empty, this method throws an InvalidOperationException.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
        ///                                                 invalid. </exception>
        /// <returns>   The current item at the <see cref="Head"/> of the queue. </returns>
        public virtual T Peek()
        {
            if ( this.Count == 0 )
            {
                throw new InvalidOperationException( "Queue is empty" );
            }

            Contract.EndContractBlock();

            return this._Array[this.Head];
        }

        /// <summary>
        /// Returns a synchronized Queue.  Returns a synchronized wrapper class around the queue - the
        /// caller must not use references to the original queue.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
        ///                                             null. </exception>
        /// <param name="queue">    The queue. </param>
        /// <returns>   An GenericQueue. </returns>
        [HostProtection( Synchronization = true )]
        public static GenericQueue<T> Synchronized( GenericQueue<T> queue )
        {
            if ( queue == null )
            {
                throw new ArgumentNullException( $"{nameof( queue )}" );
            }

            Contract.EndContractBlock();
            return new SynchronizedQueue( queue );
        }

        /// <summary>
        /// Returns true if the queue contains at least one item equal to <paramref name="item"/>.
        /// Equality is determined using obj.Equals().
        /// 
        /// Exceptions: ArgumentNullException if obj == null.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="item"> The item to test for containment. </param>
        /// <returns>   True if the item is in this collection, false if not. </returns>
        public virtual bool Contains( T item )
        {
            int index = this.Head;
            int count = this.Size;

            while ( count-- > 0 )
            {
                if ( item == null )
                {
                    if ( this._Array[index] == null )
                    {
                        return true;
                    }
                }
                else if ( this._Array[index] != null && this._Array[index].Equals( item ) )
                {
                    return true;
                }
                index = (index + 1) % this._Array.Length;
            }

            return false;
        }

        /// <summary>   Gets an element. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="i">    Zero-based index of the. </param>
        /// <returns>   The element. </returns>
        internal T GetElement( int i )
        {
            return this._Array[(this.Head + i) % this._Array.Length];
        }

        /// <summary>
        /// Iterates over the items in the queue, returning an array of the items in the Queue, or an
        /// empty array if the queue is empty. The order of items in the array is first in to last in,
        /// the same order produced by successive calls to Dequeue.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <returns>   An array that represents the data in this Queue. </returns>
        public virtual T[] ToArray()
        {
            T[] arr = new T[this.Size];
            if ( this.Size == 0 )
            {
                return arr;
            }

            if ( this.Head < this.Tail )
            {
                Array.Copy( this._Array, this.Head, arr, 0, this.Size );
            }
            else
            {
                Array.Copy( this._Array, this.Head, arr, 0, this._Array.Length - this.Head );
                Array.Copy( this._Array, 0, arr, this._Array.Length - this.Head, this.Tail );
            }

            return arr;
        }

        /// <summary>
        /// Iterates over the items in the queue, returning an array of the items in the Queue, or an
        /// empty array if the queue is empty. The order of items in the array is first in to last in,
        /// the same order produced by successive calls to Dequeue.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="count">    The number of items contained in the
        ///                         <see cref="T:System.Collections.ICollection" />. </param>
        /// <returns>   An array that represents the data in this Queue. </returns>
        public virtual T[] ToArray( int count )
        {
            T[] arr = new T[count];
            if ( this.Size == 0 )
            {
                return arr;
            }

            if ( this.Head < this.Tail )
            {
                Array.Copy( this._Array, this.Head, arr, 0, count );
            }
            else
            {
                int num = this._Array.Length - this.Head;
                if ( count <= num )
                {
                    Array.Copy( this._Array, this.Head, arr, 0, count );
                }
                else
                {
                    Array.Copy( this._Array, this.Head, arr, 0, num );
                    Array.Copy( this._Array, 0, arr, num, this.Tail );
                }
            }

            return arr;
        }


        /// <summary>
        /// Grows or shrinks the buffer to hold the capacity if items. Capacity must be greater than <see cref="Size"/>.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="capacity"> The capacity. </param>
        private void SetCapacity( int capacity )
        {
            T[] newarray = new T[capacity];
            if ( this.Size > 0 )
            {
                if ( this.Head < this.Tail )
                {
                    Array.Copy( this._Array, this.Head, newarray, 0, this.Size );
                }
                else
                {
                    Array.Copy( this._Array, this.Head, newarray, 0, this._Array.Length - this.Head );
                    Array.Copy( this._Array, 0, newarray, this._Array.Length - this.Head, this.Tail );
                }
            }

            this._Array = newarray;
            this.Head = 0;
            this.Tail = (this.Size == capacity) ? 0 : this.Size;
            this.IncrementVersion( 1 );
        }

        /// <summary>   Trim to size. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        public virtual void TrimToSize()
        {
            this.SetCapacity( this.Size );
        }

        #region ICollection Members

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.ICollection" /> to an
        /// <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <remarks>   David, 2020-09-23. </remarks>
        /// <param name="array">        The one-dimensional <see cref="T:System.Array" /> that is the
        ///                             destination of the elements copied from
        ///                             <see cref="T:System.Collections.ICollection" />. The
        ///                             <see cref="T:System.Array" /> must have zero-based indexing. </param>
        /// <param name="arrayIndex">   The zero-based index in <paramref name="array" /> at which
        ///                             copying begins. </param>
        public void CopyTo( Array array, int arrayIndex )
        {
            this.CopyTo( ( T[] ) array, arrayIndex );
        }

        #endregion

        #region ICollection<T> Members

        /// <summary>
        /// Gets the number of elements contained in the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <typeparamref name="T">    Generic type parameter. </typeparamref>
        /// <value>
        /// The number of elements contained in the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </value>
        int ICollection<T>.Count => this.Size;

        /// <summary>
        /// Gets a value indicating whether the <see cref="T:System.Collections.Generic.ICollection`1" />
        /// is read-only.
        /// </summary>
        /// <typeparamref name="T">    Generic type parameter. </typeparamref>
        /// <value>
        /// <see langword="true" /> if the <see cref="T:System.Collections.Generic.ICollection`1" /> is
        /// read-only; otherwise, <see langword="false" />.
        /// </value>
        bool ICollection<T>.IsReadOnly => false;

        /// <summary>
        /// Adds an item to the <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <typeparamref name="T">    Generic type parameter. </typeparamref>
        /// <param name="item"> The object to add to the
        ///                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        void ICollection<T>.Add( T item )
        {
            this.Enqueue( item );
        }

        /// <summary>
        /// Removes the first occurrence of a specific object from the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <typeparamref name="T">    Generic type parameter. </typeparamref>
        /// <param name="item"> The object to remove from the
        ///                     <see cref="T:System.Collections.Generic.ICollection`1" />. </param>
        /// <returns>
        /// <see langword="true" /> if <paramref name="item" /> was successfully removed from the
        /// <see cref="T:System.Collections.Generic.ICollection`1" />; otherwise,
        /// <see langword="false" />. This method also returns <see langword="false" /> if
        /// <paramref name="item" /> is not found in the original
        /// <see cref="T:System.Collections.Generic.ICollection`1" />.
        /// </returns>
        bool ICollection<T>.Remove( T item )
        {
            if ( this.Size == 0 )
            {
                return false;
            }

            _ = this.Dequeue();
            return true;
        }

        /// <summary>
        /// Copies the elements of the <see cref="T:System.Collections.Generic.ICollection`1" /> to an
        /// <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <param name="array">    The one-dimensional <see cref="T:System.Array" /> that is the
        ///                         destination of the elements copied from
        ///                         <see cref="T:System.Collections.Generic.ICollection`1" />. The
        ///                         <see cref="T:System.Array" /> must have zero-based indexing. </param>
        public void CopyTo( T[] array )
        {
            this.CopyTo( array, 0 );
        }

        #endregion

        #region IEnumerable<T> Members

        /// <summary>   Returns an enumerator that iterates through the collection. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        /// <typeparamref name="T">    Generic type parameter. </typeparamref>
        /// <returns>   An enumerator that can be used to iterate through the collection. </returns>
        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            int bufferIndex = this.Head;
            for ( int i = 0; i < this.Size; i++, bufferIndex++ )
            {
                if ( bufferIndex == this.Capacity )
                {
                    bufferIndex = 0;
                }
                yield return this._Array[bufferIndex];
            }
        }

        #endregion

        /// <summary>   Implements a synchronization wrapper around a queue. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        private class SynchronizedQueue : GenericQueue<T>
        {
            /// <summary>   The q. </summary>
            private readonly GenericQueue<T> _Q;

            /// <summary>   The synchronizing root. </summary>
            private readonly Object _Root;

            /// <summary>   Constructor. </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <param name="q">    An encapsulated <see cref="GenericQueue{T}"/>. </param>
            internal SynchronizedQueue( GenericQueue<T> q )
            {
                this._Q = q;
                this._Root = this._Q.SyncRoot;
            }

            /// <summary>
            /// Gets a value indicating whether access to the <see cref="T:System.Collections.ICollection" />
            /// is synchronized (thread safe).
            /// </summary>
            /// <value>
            /// <see langword="true" /> if access to the <see cref="T:System.Collections.ICollection" /> is
            /// synchronized (thread safe); otherwise, <see langword="false" />.
            /// </value>
            public override bool IsSynchronized => true;

            /// <summary>   Gets the synchronize root. </summary>
            /// <value> The synchronize root. </value>
            public override Object SyncRoot => this._Root;

            /// <summary>
            /// Gets the number of items contained in the <see cref="T:System.Collections.ICollection" />.
            /// </summary>
            /// <value>
            /// The number of items contained in the <see cref="T:System.Collections.ICollection" />.
            /// </value>
            public override int Count
            {
                get {
                    lock ( this._Root )
                    {
                        return this._Q.Count;
                    }
                }
            }

            /// <summary>   Clears this <see cref="GenericQueue{T}"/> to its blank/initial state. </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            public override void Clear()
            {
                lock ( this._Root )
                {
                    this._Q.Clear();
                }
            }

            /// <summary>   Creates a new <see cref="GenericQueue{T}"/> that is a copy of the current instance. </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <returns>   A new <see cref="GenericQueue{T}"/> that is a copy of this instance. </returns>
            public SynchronizedQueue CreateCopyNew()
            {
                lock ( this._Root )
                {
                    return new SynchronizedQueue( this._Q.CreateCopy() );
                }
            }

            /// <summary>   Creates a new <see cref="GenericQueue{T}"/> that is a copy of the current instance. </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <returns>   A new <see cref="GenericQueue{T}"/> that is a copy of this instance. </returns>
            public override Object Clone()
            {
                lock ( this._Root )
                {
                    return new SynchronizedQueue( this._Q.CreateCopy() );
                }
            }

            /// <summary>
            /// Returns true if the queue contains at least one item equal to <paramref name="item"/>.
            /// Equality is determined using obj.Equals().
            /// 
            /// Exceptions: ArgumentNullException if obj == null.
            /// </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <param name="item"> The item to test for containment. </param>
            /// <returns>   True if the item is in this collection, false if not. </returns>
            public override bool Contains( T item )
            {
                lock ( this._Root )
                {
                    return this._Q.Contains( item );
                }
            }

            /// <summary>
            /// Copies the items of the <see cref="T:System.Collections.ICollection" /> to an
            /// <see cref="T:System.Array" />, starting at a particular <see cref="T:System.Array" /> index.
            /// </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <param name="array">        The one-dimensional <see cref="T:System.Array" /> that is the
            ///                             destination of the items copied from
            ///                             <see cref="T:System.Collections.ICollection" />. The
            ///                             <see cref="T:System.Array" /> must have zero-based indexing. </param>
            /// <param name="arrayIndex">   The zero-based index in <paramref name="array" /> at which
            ///                             copying begins. </param>
            public override void CopyTo( T[] array, int arrayIndex )
            {
                lock ( this._Root )
                {
                    this._Q.CopyTo( array, arrayIndex );
                }
            }

            /// <summary>   Adds an item to the tail of the queue. </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <param name="value">    The item to add. </param>
            public override void Enqueue( T value )
            {
                lock ( this._Root )
                {
                    this._Q.Enqueue( value );
                }
            }

            /// <summary>
            /// Removes the item at the <see cref="P:isr.Core.CompositesQueue`1.Head" /> of the queue
            /// and returns it. If the queue is empty, this method simply returns null.
            /// </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <returns>   The head item from this queue. </returns>
            public override T Dequeue()
            {
                lock ( this._Root )
                {
                    return this._Q.Dequeue();
                }
            }

            /// <summary>   Returns an enumerator that iterates through a collection. </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <returns>
            /// An <see cref="T:System.Collections.IEnumerator" /> that can be used to iterate through
            /// the collection.
            /// </returns>
            public override IEnumerator GetEnumerator()
            {
                lock ( this._Root )
                {
                    return this._Q.GetEnumerator();
                }
            }

            /// <summary>
            /// Returns the item at the <see cref="Head"/> of the queue. The item remains in the queue. If
            /// the queue is empty, this method throws an InvalidOperationException.
            /// </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <returns>
            /// The current item at the <see cref="P:isr.Core.CompositesQueue`1.Head" /> of the queue.
            /// </returns>
            public override T Peek()
            {
                lock ( this._Root )
                {
                    return this._Q.Peek();
                }
            }

            /// <summary>
            /// Iterates over the items in the queue, returning an array of the items in the Queue, or an
            /// empty array if the queue is empty. The order of items in the array is first in to last in,
            /// the same order produced by successive calls to Dequeue.
            /// </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <returns>   An array that represents the data in this <see cref="GenericQueue{T}"/>. </returns>
            public override T[] ToArray()
            {
                lock ( this._Root )
                {
                    return this._Q.ToArray();
                }
            }

            /// <summary>
            /// Iterates over the items in the queue, returning an array of the items in the Queue, or an
            /// empty array if the queue is empty. The order of items in the array is first in to last in,
            /// the same order produced by successive calls to Dequeue.
            /// </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <param name="count">    The number of items contained in the
            ///                         <see cref="T:System.Collections.ICollection" />. </param>
            /// <returns>   An array that represents the data in this Queue. </returns>
            public override T[] ToArray( int count )
            {
                lock ( this._Root )
                {
                    return this._Q.ToArray( count );
                }
            }

            /// <summary>   Trim to size. </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            public override void TrimToSize()
            {
                lock ( this._Root )
                {
                    this._Q.TrimToSize();
                }
            }
        }


        /// <summary>
        /// A queue enumerator. Implements an enumerator for a Queue.  The enumerator uses
        /// the internal version number of the list to ensure that no modifications are made to the list
        /// while an enumeration is in progress.
        /// </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        private class QueueEnumerator : IEnumerator, ICloneable
        {
            /// <summary>   The q. </summary>
            private readonly GenericQueue<T> _Q;

            /// <summary>   Zero-based index of the. </summary>
            private int _Index;

            /// <summary>   The version. </summary>
            private readonly long _Version;

            /// <summary>   The current element. </summary>
            private T _CurrentElement;

            /// <summary>   State of the enumerator. </summary>
            private EnumeratorState _State;

            /// <summary>   Constructor. </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <param name="q">    An ObjectQueue to process. </param>
            internal QueueEnumerator( GenericQueue<T> q )
            {
                this._Q = q;
                this._Version = this._Q.Version;
                this.Reset();
            }

            /// <summary>   Creates a new object that is a copy of the current instance. </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <returns>   A new object that is a copy of this instance. </returns>
            public Object Clone()
            {
                return this.MemberwiseClone();
            }

            /// <summary>   Advances the enumerator to the next element of the collection. </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
            ///                                                 invalid. </exception>
            /// <returns>
            /// <see langword="true" /> if the enumerator was successfully advanced to the next element;
            /// <see langword="false" /> if the enumerator has passed the end of the collection.
            /// </returns>
            public virtual bool MoveNext()
            {
                if ( this._Version != this._Q.Version )
                {
                    throw new InvalidOperationException( $"Queue enumerator version {this._Version} must equal queue version {this._Q.Version}" );
                }

                this._Index++;
                if ( this._Index == this._Q.Size )
                {
                    this._State = EnumeratorState.Ended;
                    return false;
                }
                else
                {
                    this._CurrentElement = this._Q.GetElement( this._Index );
                    return true;
                }
            }

            private enum EnumeratorState
            {
                None,
                Started,
                Ended
            }


            /// <summary>
            /// Gets the element in the collection at the current position of the enumerator.
            /// </summary>
            /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
            ///                                                 invalid. </exception>
            /// <value> The element in the collection at the current position of the enumerator. </value>
            public virtual Object Current
            {
                get {
                    if ( this._State == EnumeratorState.Started )
                    {
                        return this._CurrentElement;
                    }
                    else
                    {
                        if ( this._State == EnumeratorState.None )
                        {
                            throw new InvalidOperationException( "Enumerator not started" );
                        }
                        else
                        {
                            throw new InvalidOperationException( "Enumerator ended" );
                        }
                    }
                }
            }

            /// <summary>
            /// Sets the enumerator to its initial position, which is before the first element in the
            /// collection.
            /// </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <exception cref="InvalidOperationException">    Thrown when the requested operation is
            ///                                                 invalid. </exception>
            public virtual void Reset()
            {
                if ( this._Version != this._Q.Version )
                {
                    throw new InvalidOperationException( $"Queue enumerator version {this._Version} must equal queue version {this._Q.Version}" );
                }

                this._Index = -1;
                // this._CurrentElement = default;
                this._State = this._Q.Size == 0 ? EnumeratorState.None : EnumeratorState.Started;
            }
        }

        /// <summary>   A queue debug view. </summary>
        /// <remarks>   David, 2020-09-09. </remarks>
        internal class GenericQueueDebugView
        {
            /// <summary>   The queue. </summary>
            private readonly GenericQueue<T> _Queue;

            /// <summary>   Constructor. </summary>
            /// <remarks>   David, 2020-09-09. </remarks>
            /// <exception cref="ArgumentNullException">    Thrown when one or more required arguments are
            ///                                             null. </exception>
            /// <param name="queue">    The queue. </param>
            public GenericQueueDebugView( GenericQueue<T> queue )
            {
                Contract.EndContractBlock();

                this._Queue = queue ?? throw new ArgumentNullException( $"{nameof( queue )}" );
            }

            /// <summary>   Gets the items. </summary>
            /// <value> The items. </value>
            [DebuggerBrowsable( DebuggerBrowsableState.RootHidden )]
            public T[] Items => this._Queue.ToArray();
        }
    }
}



