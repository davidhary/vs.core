using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core.Constructs
{
    /// <summary> Class encapsulating a list of Action delegates taking one arguments. </summary>
    /// <remarks>
    /// David, 2019-02-03, https://github.com/polterguy/lizzie/. <para>
    /// Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com  </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class Actions<TArgument> : Sequence<Action<TArgument>>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public Actions() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.delegates.Sequence"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="lambdas"> Initial functors. </param>
        public Actions( params Action<TArgument>[] lambdas ) : base( lambdas )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.delegates.Sequence"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="lambdas"> Initial functors. </param>
        public Actions( IEnumerable<Action<TArgument>> lambdas ) : base( lambdas )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="lambdas"> Initial items. </param>
        public Actions( IList<Action<TArgument>> lambdas ) : base( lambdas )
        {
        }

        /// <summary> Sequentially executes each action. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="actionArgument"> The action argument. </param>
        public void Execute( TArgument actionArgument )
        {
            Executor.Sequentially( this.Select( ix => new Action( () => ix( actionArgument ) ) ) );
        }

        /// <summary> Sequentially executes each action without blocking the calling thread. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="actionArgument"> The action argument. </param>
        public void ExecuteUnblocked( TArgument actionArgument )
        {
            Executor.SequentiallyUnblocked( this.Select( ix => new Action( () => ix( actionArgument ) ) ) );
        }

        /// <summary>
        /// Executes each action in parallel blocking the calling thread until all actions are finished
        /// executing.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="actionArgument"> The action argument. </param>
        public void ExecuteParallel( TArgument actionArgument )
        {
            Executor.Parallel( this.Select( ix => new Action( () => ix( actionArgument ) ) ) );
        }

        /// <summary> Executes each action in parallel without blocking the calling thread. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="actionArgument"> The action argument. </param>
        public void ExecuteParallelUnblocked( TArgument actionArgument )
        {
            Executor.ParallelUnblocked( this.Select( ix => new Action( () => ix( actionArgument ) ) ) );
        }
    }
}
