using System;
using System.Collections.Generic;

namespace isr.Core.Constructs
{
    /// <summary> Class encapsulating a list of Action delegates taking no arguments. </summary>
    /// <remarks>
    /// <para>
    /// Once you’ve taken the first steps into the magic of lambdas (or delegates, as they’re called
    /// in C#), it’s not difficult to imagine that you can do more with them. For instance, a common
    /// recurring theme in multi-threading is to have multiple threads reach out to other servers to
    /// fetch data and return the data back to the caller.
    /// </para><para>
    /// The most basic example would be an application that reads data from 20 Web pages, And When
    /// complete returns the HTML back To a Single thread that creates some sort Of aggregated result
    /// based On the content Of all the pages. Unless you create one thread For Each Of your
    /// retrieval methods, this code will be much slower than desired—99 percent Of all execution
    /// time would likely be spent waiting For the HTTP request To Return.
    /// </para><para>
    /// Running this code On a Single thread Is inefficient, And the syntax For creating a thread Is
    /// difficult To Get right. The challenge compounds As you support multiple threads And their
    /// attendant objects, forcing developers To repeat themselves As they write the code. Once you
    /// realize that you can create a collection Of delegates, And a Class To wrap them, you can Then
    /// create all your threads With a Single method invocation. Just Like that, creating threads
    /// becomes much less painful.</para><para>
    /// David, 2019-02-03, https://github.com/polterguy/lizzie/. </para><para>
    /// Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com  </para><para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class Actions : Sequence<Action>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.collections.Actions"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public Actions() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.collections.Actions"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="actions"> Actions. </param>
        public Actions( params Action[] actions ) : base( actions )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.collections.Actions"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="actions"> Actions. </param>
        public Actions( IEnumerable<Action> actions ) : base( actions )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="actions"> Initial items. </param>
        public Actions( IList<Action> actions ) : base( actions )
        {
        }

        /// <summary> Sequentially executes each action. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public void Execute()
        {
            Executor.Sequentially( this );
        }

        /// <summary> Sequentially executes each action without blocking the calling thread. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public void ExecuteUnblocked()
        {
            Executor.SequentiallyUnblocked( this );
        }

        /// <summary>
        /// Executes each action in parallel blocking the calling thread until all actions are finished
        /// executing.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public void ExecuteParallel()
        {
            Executor.Parallel( this );
        }

        /// <summary> Executes each action in parallel without blocking the calling thread. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public void ExecuteParallelUnblocked()
        {
            Executor.ParallelUnblocked( this );
        }
    }
}
