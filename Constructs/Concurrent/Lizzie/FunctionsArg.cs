using System;
using System.Collections.Generic;
using System.Linq;

#pragma warning disable IDE1006 // Naming Styles
namespace isr.Core.Constructs
#pragma warning restore IDE1006 // Naming Styles
{
    /// <summary> Class encapsulating a list of Func delegates taking one argument. </summary>
    /// <remarks>
    /// Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com <para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class Functions<TArgument, TResult> : Sequence<Func<TArgument, TResult>>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Functions`1"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public Functions() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.delegates.Functions`1"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="lambdas"> Initial functors. </param>
        public Functions( params Func<TArgument, TResult>[] lambdas ) : base( lambdas )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.delegates.Functions`1"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="lambdas"> Initial functors. </param>
        public Functions( IEnumerable<Func<TArgument, TResult>> lambdas ) : base( lambdas )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="lambdas"> Initial items. </param>
        public Functions( IList<Func<TArgument, TResult>> lambdas ) : base( lambdas )
        {
        }

        /// <summary> Evaluate each function in a sequence on the calling thread. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="functionArgument"> T1. </param>
        /// <returns> The sequence. </returns>
        public IEnumerable<TResult> Evaluate( TArgument functionArgument )
        {
            return Evaluator<TResult>.Sequentially( this.Select( ix => new Func<TResult>( () => ix( functionArgument ) ) ) );
        }

        /// <summary> Evaluates each function in parallel and returns the result to caller. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="functionArgument"> T1. </param>
        /// <returns> The result of each function invocation. </returns>
        public IEnumerable<TResult> EvaluateParallel( TArgument functionArgument )
        {
            return Evaluator<TResult>.Parallel( this.Select( ix => new Func<TResult>( () => ix( functionArgument ) ) ) );
        }
    }
}
