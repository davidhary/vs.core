using System;
using System.Collections.Generic;

namespace isr.Core.Constructs
{
    /// <summary> Class encapsulating a list of Func delegates taking no arguments. </summary>
    /// <remarks>
    /// Copyright (c) 2018 Thomas Hansen - thomas@gaiasoul.com <para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public class Functions<TResult> : Sequence<Func<TResult>>
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Functions`1"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public Functions() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.delegates.Functions`1"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="functions"> Initial functions. </param>
        public Functions( params Func<TResult>[] functions ) : base( functions )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.delegates.Functions`1"/> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="functions"> Initial functions. </param>
        public Functions( IEnumerable<Func<TResult>> functions ) : base( functions )
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="T:poetic.lambda.lambdas.Sequence`1" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="functions"> Initial items. </param>
        public Functions( IList<Func<TResult>> functions ) : base( functions )
        {
        }

        /// <summary> Evaluate each function in a sequence on the calling thread. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The result of each evaluation. </returns>
        public IEnumerable<TResult> Evaluate()
        {
            return Evaluator<TResult>.Sequentially( this );
        }

        /// <summary> Evaluates each function in parallel and returns the result to caller. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The result of each function invocation. </returns>
        public IEnumerable<TResult> EvaluateParallel()
        {
            return Evaluator<TResult>.Parallel( this );
        }
    }
}
