using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core.Constructs
{
    /// <summary>
    /// An dictionary of items <typeparamref name="TKey"/> ordered by the order they are entered
    /// <typeparamref name="TValue"/>.
    /// </summary>
    /// <remarks>
    /// David, 2020-05-22,
    /// https://www.codeproject.com/Tips/5268618/ListDictionary-An-Improved-OrderedDictionary <para>
    /// Enumerating the data Dictionary as KeyValuePair did Not Return data In any particular order,
    /// and using System.Collection.Specialized's OrderedDictionary was cumbersome. So, I created my
    /// own orderly dictionary called ListDictionary which is discussed in this tip. </para><para>
    /// (c) 2020 Michael Sydney Balloni. All rights reserved. </para><para>
    /// Licensed under The MIT License.</para><para>
    /// </para>
    /// </remarks>
    public class ListDictionary<TKey, TValue> where TKey : IComparable<TKey>
    {

        /// <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public ListDictionary() : base()
        {
            this.Entries = new List<KeyValuePair<TKey, TValue>>();
            this.EntriesDictionary = new Dictionary<TKey, TValue>();
        }

        /// <summary> Gets the list of entries. </summary>
        /// <value> The entries. </value>
        public IList<KeyValuePair<TKey, TValue>> Entries { get; private set; }

        /// <summary> Gets the keys. </summary>
        /// <value> The keys. </value>
        public IEnumerable<TKey> Keys => this.Entries.Select( kvp => kvp.Key );

        /// <summary> Gets the values. </summary>
        /// <value> The values. </value>
        public IEnumerable<TValue> Values => this.Entries.Select( kvp => kvp.Value );

        /// <summary> Gets or sets a dictionary of entries. </summary>
        /// <value> A dictionary of entries. </value>
        private IDictionary<TKey, TValue> EntriesDictionary { get; set; }

        /// <summary> Gets or sets the item. </summary>
        /// <value> The item. </value>
        public TValue this[TKey key]
        {
            get => this.EntriesDictionary[key];

            set => this.Setter( key, value );
        }

        /// <summary> Gets any. </summary>
        /// <value> any. </value>
        public bool Any => this.Entries.Any();

        /// <summary> Gets the number of. </summary>
        /// <value> The count. </value>
        public int Count => this.Entries.Count;

        /// <summary> Query if 'key' contains key. </summary>
        /// <remarks> David, 2020-05-22. </remarks>
        /// <param name="key"> The key. </param>
        /// <returns> True if it succeeds, false if it fails. </returns>
        public bool ContainsKey( TKey key )
        {
            return this.EntriesDictionary.ContainsKey( key );
        }

        /// <summary> Gets the first key. </summary>
        /// <value> The first key. </value>
        public TKey FirstKey => this.Entries.First().Key;

        /// <summary> Gets the Last key. </summary>
        /// <value> The Last key. </value>
        public TKey LastKey => this.Entries.Last().Key;

        /// <summary> Sets the key value pair and sorts the entities list. </summary>
        /// <remarks> David, 2020-05-22. </remarks>
        /// <param name="key"> The key. </param>
        /// <param name="val"> The value. </param>
        public void Setter( TKey key, TValue val )
        {
            bool added = false;
            if ( this.EntriesDictionary.ContainsKey( key ) )
            {
                for ( int i = 0, loopTo = this.Entries.Count - 1; i <= loopTo; i++ )
                {
                    var curKey = this.Entries[i].Key;
                    if ( curKey.CompareTo( key ) == 0 )
                    {
                        this.Entries[i] = new KeyValuePair<TKey, TValue>( key, val );
                        added = true;
                        break;
                    }
                }
            }

            if ( !added )
            {
                this.Entries.Add( new KeyValuePair<TKey, TValue>( key, val ) );
            }

            this.EntriesDictionary[key] = val;
        }
    }
}
