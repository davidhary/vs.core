using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using isr.Core.EnumExtensions;

namespace isr.Core.Constructs
{

    /// <summary> A yield bin counter. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-10-07 </para>
    /// </remarks>
    [DebuggerDisplay( "Total = {TotalCount}" )]
    public class YieldBinCounter : YieldCounter
    {

        #region " CONSTRUCTION "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="binNumbers">        The bin numbers. </param>
        /// <param name="goodBinNumbers">    The Good bin number. </param>
        /// <param name="failureBinNumbers"> The Failure bin number. </param>
        /// <param name="invalidBinNumbers"> The invalid bin number. </param>
        public YieldBinCounter( int[] binNumbers, int[] goodBinNumbers, int[] failureBinNumbers, int[] invalidBinNumbers ) : base()
        {
            this.ResetKnownStatThis( binNumbers, goodBinNumbers, failureBinNumbers, invalidBinNumbers );
        }

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="binNumbers">        The bin numbers. </param>
        /// <param name="goodBinNumbers">    The Good bin number. </param>
        /// <param name="failureBinNumbers"> The Failure bin number. </param>
        /// <param name="invalidBinNumbers"> The invalid bin number. </param>
        public YieldBinCounter( IList<int> binNumbers, IList<int> goodBinNumbers, IList<int> failureBinNumbers, IList<int> invalidBinNumbers ) : base()
        {
            this.ResetKnownStatThis( binNumbers, goodBinNumbers, failureBinNumbers, invalidBinNumbers );
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="binNumbers">        The bin numbers. </param>
        /// <param name="goodBinNumbers">    The Good bin number. </param>
        /// <param name="failureBinNumbers"> The Failure bin number. </param>
        /// <param name="invalidBinNumbers"> The invalid bin number. </param>
        public YieldBinCounter( IEnumerable<int> binNumbers, IEnumerable<int> goodBinNumbers, IEnumerable<int> failureBinNumbers, IEnumerable<int> invalidBinNumbers ) : this( binNumbers.ToList(), goodBinNumbers.ToList(), failureBinNumbers.ToList(), invalidBinNumbers.ToList() )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value. </param>
        public YieldBinCounter( YieldBinCounter value ) : this( value is null ? (Array.Empty<int>()) : value.BinNumbers, value is null ? (Array.Empty<int>()) : value.BinNumbers, value is null ? (Array.Empty<int>()) : value.BinNumbers, value is null ? (Array.Empty<int>()) : value.BinNumbers )
        {
            if ( value is object )
            {
                this.IncrementRangeThis( value );
                this._BinNumber = value.BinNumber;
            }
        }

        #endregion

        #region " RESET AND CLEAR "

        /// <summary>
        /// Clears to known (clear) state; Clears select values to their initial state.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public override void ClearKnownState()
        {
            this.BinCountDictionary.ClearKnownState();
            base.ClearKnownState();
        }

        /// <summary> Publishes this object. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public override void Publish()
        {
            this.NotifyPropertyChanged( nameof( YieldBinCounter.BinNumber ) );
        }

        /// <summary> Resets the known state. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="binNumbers">        The bin numbers. </param>
        /// <param name="goodBinNumbers">    The Good bin number. </param>
        /// <param name="failureBinNumbers"> The Failure bin number. </param>
        /// <param name="invalidBinNumbers"> The invalid bin number. </param>
        private void ResetKnownStatThis( int[] binNumbers, int[] goodBinNumbers, int[] failureBinNumbers, int[] invalidBinNumbers )
        {
            this.BinCountDictionary = new BinCountDictionary( binNumbers );
            this.InitializeFailureBinNumbersThis( failureBinNumbers );
            this.InitializeGoodBinNumbersThis( goodBinNumbers );
            this.InitializeInvalidBinNumbersThis( invalidBinNumbers );
            this.ValidateBinNumbers();
        }

        /// <summary> Resets the known state. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="binNumbers">        The bin numbers. </param>
        /// <param name="goodBinNumbers">    The Good bin number. </param>
        /// <param name="failureBinNumbers"> The Failure bin number. </param>
        /// <param name="invalidBinNumbers"> The invalid bin number. </param>
        private void ResetKnownStatThis( IList<int> binNumbers, IList<int> goodBinNumbers, IList<int> failureBinNumbers, IList<int> invalidBinNumbers )
        {
            this.BinCountDictionary = new BinCountDictionary( binNumbers );
            this.InitializeFailureBinNumbersThis( failureBinNumbers );
            this.InitializeGoodBinNumbersThis( goodBinNumbers );
            this.InitializeInvalidBinNumbersThis( invalidBinNumbers );
            this.ValidateBinNumbers();
        }

        /// <summary> Validates the bin numbers. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        private void ValidateBinNumbers()
        {
            foreach ( int binNumber in this.BinNumbers )
            {
                if ( this.GoodBinNumbers.Contains( binNumber ) )
                {
                }
                else if ( this.InvalidBinNumbers.Contains( binNumber ) )
                {
                }
                else if ( this.FailureBinNumbers.Contains( binNumber ) )
                {
                }
                else
                {
                    throw new InvalidOperationException( $"Bin number validation failed because bin number {binNumber} not defined." );
                }
            }
        }

        #endregion

        #region " SPECIAL BINS "

        /// <summary> Gets the Good bin number. </summary>
        /// <value> The Good bin number. </value>
        public IList<int> GoodBinNumbers { get; private set; }

        /// <summary> Initializes the good bin numbers on this class. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="goodBinNumbers"> The Good bin number. </param>
        private void InitializeGoodBinNumbersThis( int[] goodBinNumbers )
        {
            this.GoodBinNumbers = new List<int>( goodBinNumbers );
        }

        /// <summary> Initializes the Good bin numbers described by GoodBinNumbers. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="goodBinNumbers"> The Good bin number. </param>
        public void InitializeGoodBinNumbers( int[] goodBinNumbers )
        {
            this.InitializeGoodBinNumbersThis( goodBinNumbers );
        }

        /// <summary> Initializes the good bin numbers on this class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="goodBinNumbers"> The Good bin number. </param>
        private void InitializeGoodBinNumbersThis( IList<int> goodBinNumbers )
        {
            this.GoodBinNumbers = new List<int>( goodBinNumbers );
        }

        /// <summary> Initializes the Good bin numbers described by GoodBinNumbers. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="goodBinNumbers"> The Good bin number. </param>
        public void InitializeGoodBinNumbers( IList<int> goodBinNumbers )
        {
            this.InitializeGoodBinNumbersThis( goodBinNumbers );
        }

        /// <summary> Gets the Failure bin numbers. </summary>
        /// <value> The Failure bin numbers. </value>
        public IList<int> FailureBinNumbers { get; private set; }

        /// <summary> Initializes the failure bin numbers this. </summary>
        /// <remarks> David, 2020-09-04. </remarks>
        /// <param name="failureBinNumbers"> The Failure bin number. </param>
        private void InitializeFailureBinNumbersThis( int[] failureBinNumbers )
        {
            this.FailureBinNumbers = new List<int>( failureBinNumbers );
        }

        /// <summary> Initializes the Failure bin numbers described by FailureBinNumbers. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="failureBinNumbers"> The Failure bin number. </param>
        public void InitializeFailureBinNumbers( int[] failureBinNumbers )
        {
            this.InitializeFailureBinNumbersThis( failureBinNumbers );
        }

        /// <summary> Initializes the failure bin numbers this. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="failureBinNumbers"> The Failure bin number. </param>
        private void InitializeFailureBinNumbersThis( IList<int> failureBinNumbers )
        {
            this.FailureBinNumbers = new List<int>( failureBinNumbers );
        }

        /// <summary> Initializes the Failure bin numbers described by FailureBinNumbers. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="failureBinNumbers"> The Failure bin number. </param>
        public void InitializeFailureBinNumbers( IList<int> failureBinNumbers )
        {
            this.InitializeFailureBinNumbersThis( failureBinNumbers );
        }

        /// <summary> Gets the invalid bin number. </summary>
        /// <value> The invalid bin number. </value>
        public IList<int> InvalidBinNumbers { get; private set; }

        /// <summary> Initializes the invalid bin numbers this. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="invalidBinNumbers"> The invalid bin number. </param>
        private void InitializeInvalidBinNumbersThis( int[] invalidBinNumbers )
        {
            this.InvalidBinNumbers = new List<int>( invalidBinNumbers );
        }

        /// <summary> Initializes the invalid bin numbers described by invalidBinNumbers. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="invalidBinNumbers"> The invalid bin number. </param>
        public void InitializeInvalidBinNumbers( int[] invalidBinNumbers )
        {
            this.InitializeInvalidBinNumbersThis( invalidBinNumbers );
        }

        /// <summary> Initializes the invalid bin numbers this. </summary>
        /// <remarks> David, 2020-09-04. </remarks>
        /// <param name="invalidBinNumbers"> The invalid bin number. </param>
        private void InitializeInvalidBinNumbersThis( IList<int> invalidBinNumbers )
        {
            this.InvalidBinNumbers = new List<int>( invalidBinNumbers );
        }

        /// <summary> Initializes the invalid bin numbers described by invalidBinNumbers. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="invalidBinNumbers"> The invalid bin number. </param>
        public void InitializeInvalidBinNumbers( IList<int> invalidBinNumbers )
        {
            this.InitializeInvalidBinNumbersThis( invalidBinNumbers );
        }

        /// <summary> Initializes the invalid bin numbers this. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="invalidBinNumbers"> The invalid bin number. </param>
        private void InitializeInvalidBinNumbersThis( IEnumerable<int> invalidBinNumbers )
        {
            this.InvalidBinNumbers = new List<int>( invalidBinNumbers );
        }

        /// <summary> Initializes the invalid bin numbers described by invalidBinNumbers. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="invalidBinNumbers"> The invalid bin number. </param>
        public void InitializeInvalidBinNumbers( IEnumerable<int> invalidBinNumbers )
        {
            this.InitializeInvalidBinNumbersThis( invalidBinNumbers );
        }

        #endregion

        #region " BIN COUNTER "

        /// <summary> Dictionary of bin counts. </summary>

        /// <summary> Gets a dictionary of bin counts. </summary>
        /// <value> A Dictionary of bin counts. </value>
        private BinCountDictionary BinCountDictionary { get; set; }

        /// <summary> Gets the bin word. </summary>
        /// <value> The bin word. </value>
        public int BinWord => this.BinCountDictionary.BinWord;

        /// <summary> Gets the bin numbers. </summary>
        /// <value> The bin numbers. </value>
        public IEnumerable<int> BinNumbers => this.BinCountDictionary.BinNumbers;

        /// <summary> Increment bin. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="binNumber"> The bin number. </param>
        public void IncrementBin( int binNumber )
        {
            this.IncrementBin( binNumber, 1 );
        }

        /// <summary> Increment bin this. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="binNumber"> The bin number. </param>
        /// <param name="count">     Number of. </param>
        private void IncrementBinThis( int binNumber, int count )
        {
            if ( this.GoodBinNumbers.Contains( binNumber ) )
            {
            }
            // good bin is incremented in the total count
            else if ( this.InvalidBinNumbers.Contains( binNumber ) )
            {
                this.InvalidCount += count;
            }
            else if ( this.FailureBinNumbers.Contains( binNumber ) )
            {
                this.FailedCount += count;
            }
            else
            {
                throw new InvalidOperationException( $"Bin number {binNumber} not defined." );
            }

            this.BinCountDictionary[binNumber] += count;
            this.TotalCount += count;
            this._BinNumber = binNumber;
        }

        /// <summary> Increment bin. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="binNumber"> The bin number. </param>
        /// <param name="count">     Number of. </param>
        public void IncrementBin( int binNumber, int count )
        {
            if ( this.GoodBinNumbers.Contains( binNumber ) )
            {
            }
            // good bin is incremented in the total count
            else if ( this.InvalidBinNumbers.Contains( binNumber ) )
            {
                this.InvalidCount += count;
            }
            else if ( this.FailureBinNumbers.Contains( binNumber ) )
            {
                this.FailedCount += count;
            }
            else
            {
                throw new InvalidOperationException( $"Bin number {binNumber} not defined." );
            }

            this.BinCountDictionary[binNumber] += count;
            this.TotalCount += count;
            this.BinNumber = binNumber;
        }

        /// <summary> Increment range on this class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="counter"> The counter. </param>
        private void IncrementRangeThis( YieldBinCounter counter )
        {
            if ( counter is null )
            {
                throw new ArgumentNullException( nameof( counter ) );
            }

            foreach ( int binNumber in counter.BinNumbers )
            {
                this.IncrementBinThis( binNumber, counter.BinCountDictionary[binNumber] );
            }
        }

        /// <summary> Increment range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="counter"> The counter. </param>
        public void IncrementRange( YieldBinCounter counter )
        {
            if ( counter is null )
            {
                throw new ArgumentNullException( nameof( counter ) );
            }

            foreach ( int binNumber in counter.BinNumbers )
            {
                this.IncrementBin( binNumber, counter.BinCountDictionary[binNumber] );
            }
        }

        /// <summary> Decrement bin. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="binNumber"> The bin number. </param>
        public void DecrementBin( int binNumber )
        {
            this.DecrementBin( binNumber, 1 );
        }

        /// <summary> Decrement bin. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <param name="binNumber"> The bin number. </param>
        /// <param name="count">     Number of. </param>
        public void DecrementBin( int binNumber, int count )
        {
            if ( this.GoodBinNumbers.Contains( binNumber ) )
            {
            }
            // good bin is incremented in the total count
            else if ( this.InvalidBinNumbers.Contains( binNumber ) )
            {
                if ( this.InvalidCount >= count )
                {
                    this.InvalidCount -= count;
                }
                else
                {
                    throw new InvalidOperationException( $"Invalid count {this.InvalidCount} lower then the decrement {count} for bin number {binNumber}" );
                }
            }
            else if ( this.FailureBinNumbers.Contains( binNumber ) )
            {
                if ( this.FailedCount >= count )
                {
                    this.FailedCount -= count;
                }
                else
                {
                    throw new InvalidOperationException( $"Failed count {this.FailedCount} lower then the decrement {count} for bin number {binNumber}" );
                }
            }
            else
            {
                throw new InvalidOperationException( $"Bin number {binNumber} not defined." );
            }

            if ( this.BinCountDictionary[binNumber] >= count )
            {
                this.BinCountDictionary[binNumber] -= count;
            }
            else
            {
                throw new InvalidOperationException( $"Bin count {this.BinCountDictionary[binNumber]} for bin number {binNumber} lower then the decrement {count}" );
            }

            if ( this.TotalCount >= count )
            {
                this.TotalCount -= count;
            }
            else
            {
                throw new InvalidOperationException( $"Total count {this.TotalCount} is lower then the decrement {count} for bin number {binNumber} " );
            }

            this.BinNumber = binNumber;
        }

        /// <summary> Decrement range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="counter"> The counter. </param>
        public void DecrementRange( YieldBinCounter counter )
        {
            if ( counter is null )
            {
                throw new ArgumentNullException( nameof( counter ) );
            }

            foreach ( int binNumber in counter.BinNumbers )
            {
                this.DecrementBin( binNumber, counter.BinCountDictionary[binNumber] );
            }
        }

        /// <summary> Gets or sets the number of bins. </summary>
        /// <value> The number of bins. </value>
        public int GetBinCount( int binNumber )
        {
            return this.BinCountDictionary[binNumber];
        }

        /// <summary>   Sets bin count. </summary>
        /// <remarks>   David, 2020-09-22. </remarks>
        /// <param name="binNumber">    The bin number. </param>
        /// <param name="value">        The value. </param>
        public void SetBinCount( int binNumber, int value )
        {
            this.BinCountDictionary[binNumber] = value;
            this.BinNumber = binNumber;
        }

        /// <summary> The bin number. </summary>
        private int _BinNumber;

        /// <summary> Gets or sets the bin number. </summary>
        /// <value> The bin number. </value>
        public int BinNumber
        {
            get => this._BinNumber;

            set {
                this._BinNumber = value;
                this.NotifyPropertyChanged();
            }
        }

        #endregion

    }

    /// <summary> Dictionary of bin counts. </summary>
    /// <remarks>
    /// (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2017-07-14 </para>
    /// </remarks>
    public class BinCountDictionary : System.Collections.Concurrent.ConcurrentDictionary<int, int>
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="enumeration"> An enum constant representing the enumeration option. </param>
        public BinCountDictionary( Enum enumeration ) : this( enumeration.IntegerValues().ToList() )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="binNumbers"> The bin numbers. </param>
        public BinCountDictionary( IList<int> binNumbers ) : base()
        {
            this.ResetKnownStateThis( binNumbers );
        }

        /// <summary> Gets the bin numbers. </summary>
        /// <value> The bin numbers. </value>
        public ICollection<int> BinNumbers => this.Keys;

        /// <summary> Resets the known state described by binNumbers. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="binNumbers"> The bin numbers. </param>
        private void ResetKnownStateThis( IList<int> binNumbers )
        {
            foreach ( int i in binNumbers )
            {
                _ = this.TryAdd( i, 0 );
            }
        }

        /// <summary> Clears the known state. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public void ClearKnownState()
        {
            foreach ( int key in this.Keys )
            {
                this[key] = 0;
            }
        }

        /// <summary> Resets the known state described by binNumbers. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="binNumbers"> The bin numbers. </param>
        public void ResetKnownState( IList<int> binNumbers )
        {
            this.Clear();
            this.ResetKnownStateThis( binNumbers );
        }

        /// <summary> Gets the bin word. </summary>
        /// <value> The bin word. </value>
        public int BinWord
        {
            get {
                int result = 0;
                foreach ( KeyValuePair<int, int> value in this )
                {
                    if ( value.Value > 0 )
                    {
                        result |= 2 >> value.Key;
                    }
                }

                return result;
            }
        }
    }
}
