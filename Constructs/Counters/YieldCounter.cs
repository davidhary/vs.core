using System;
using System.ComponentModel;
using System.Diagnostics;

namespace isr.Core.Constructs
{

    /// <summary> A yield counter. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2016-10-08 </para>
    /// </remarks>
    [DebuggerDisplay( "Total = {TotalCount}" )]
    public class YieldCounter : Models.ViewModelBase
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public YieldCounter() : base()
        {
            this.ResetKnownStateThis();
            this.ClearKnownStateThis();
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value. </param>
        public YieldCounter( YieldCounter value ) : this()
        {
            if ( value is object )
            {
                this._TotalCount = value.TotalCount;
                this.PercentYieldFormat = value.PercentYieldFormat;
            }
        }

        /// <summary> Creates a new Yield Counter. </summary>
        /// <remarks> Helps implement CA2000. </remarks>
        /// <returns> A  <see cref="YieldCounter"/>. </returns>
        public static YieldCounter Create()
        {
            YieldCounter result;
            try
            {
                result = new YieldCounter();
            }
            catch
            {
                throw;
            }

            return result;
        }

        #endregion

        #region " RESET AND CLEAR "

        /// <summary>
        /// Clears to known (clear) state; Clears select values to their initial state.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        private void ClearKnownStateThis()
        {
            this.ClearCounters();
        }

        /// <summary> Clears the known state. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public virtual void ClearKnownState()
        {
            // if clearing and value cleared, we need to publish to make sure displays get updated.
            this.ClearKnownStateThis();
            this.Publish();
        }

        /// <summary> Publishes this object. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public virtual void Publish()
        {
            this.NotifyPropertyChanged( nameof( YieldCounter.TotalCount ) );
        }

        /// <summary> Resets the know state. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        private void ResetKnownStateThis()
        {
            this.PercentYieldFormat = DefaultPercentYieldFormat;
        }

        /// <summary> Resets the know state. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public virtual void ResetKnownState()
        {
            this.ResetKnownStateThis();
            this.Publish();
        }

        #endregion

        #region " COUNTERS "

        /// <summary> Gets or sets the number of Failures. </summary>
        /// <value> The Failures number of count. </value>
        public int FailedCount { get; protected set; }

        /// <summary> Gets or sets the number of Invalid values. </summary>
        /// <value> The Invalid number of count. </value>
        public int InvalidCount { get; protected set; }

        /// <summary> Gets the number of valid values. </summary>
        /// <value> The number of valid values. </value>
        public int ValidCount => this.TotalCount - this.InvalidCount;

        /// <summary> Gets the number of goods. </summary>
        /// <value> The number of goods. </value>
        public int GoodCount => this.ValidCount - this.FailedCount;

        /// <summary> Number of totals. </summary>
        private int _TotalCount;

        /// <summary> Gets or sets the total count. </summary>
        /// <value> The total number of count. </value>
        public int TotalCount
        {
            get => this._TotalCount;

            protected set {
                if ( value != this.TotalCount )
                {
                    this._TotalCount = value;
                    this.NotifyPropertyChanged();
                }
            }
        }

        /// <summary> Clears the counters. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        private void ClearCounters()
        {
            this._TotalCount = 0;
            this.InvalidCount = 0;
            this.FailedCount = 0;
        }

        /// <summary> Increments the total yield count. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="count"> Number of. </param>
        public void Increment( int count )
        {
            this.Increment( YieldBinNumber.Good, count );
        }

        /// <summary> Increments the yield count. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="binNumber"> The bin number. </param>
        /// <param name="count">     Number of. </param>
        public void Increment( YieldBinNumber binNumber, int count )
        {
            switch ( binNumber )
            {
                case YieldBinNumber.Good:
                    {
                        this.TotalCount += count;
                        break;
                    }

                case YieldBinNumber.Fail:
                    {
                        this.FailedCount += count;
                        this.TotalCount += count;
                        break;
                    }

                case YieldBinNumber.Invalid:
                    {
                        this.InvalidCount += count;
                        this.TotalCount += count;
                        break;
                    }
            }
        }

        #endregion

        #region " YIELD "

        /// <summary> Gets the total used for calculating yield. </summary>
        /// <value> The total used for calculating yield. </value>
        public virtual int TotalYieldCount => this.TotalCount;

        /// <summary> Gets the count used for calculating yield. </summary>
        /// <value> The count used for calculating yield. </value>
        public virtual int YieldCount => this.GoodCount;

        /// <summary> Gets the percent yield. </summary>
        /// <value> The percent yield. </value>
        public double PercentYield => this.TotalCount > 0 ? 100 * this.YieldCount / ( double ) this.TotalYieldCount : 0d;

        /// <summary> The default percent yield format. </summary>
        public const string DefaultPercentYieldFormat = "{0:0} %";

        /// <summary> Gets the percent yield format. </summary>
        /// <value> The percent yield format. </value>
        public virtual string PercentYieldFormat { get; set; }

        /// <summary> Gets the percent yield caption. </summary>
        /// <value> The percent yield count caption. </value>
        public string PercentYieldCaption => string.Format( System.Globalization.CultureInfo.CurrentCulture, this.PercentYieldFormat, this.PercentYield );

        /// <summary> Gets the hourly yield. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="elapsed"> The elapsed. </param>
        /// <returns> An Integer. </returns>
        public double HourlyRate( TimeSpan elapsed )
        {
            return elapsed.TotalSeconds > 0d ? 3600 * this.YieldCount / elapsed.TotalSeconds : 0d;
        }

        #endregion

    }

    /// <summary> Values that represent yield bin numbers. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public enum YieldBinNumber
    {

        /// <summary> An enum constant representing the good option. </summary>
        [Description( "Good" )]
        Good,

        /// <summary> An enum constant representing the fail option. </summary>
        [Description( "Fail" )]
        Fail,

        /// <summary> An enum constant representing the invalid option. </summary>
        [Description( "Invalid" )]
        Invalid
    }
}
