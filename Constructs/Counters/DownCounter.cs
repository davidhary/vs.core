using System.Diagnostics;

namespace isr.Core.Constructs
{

    /// <summary> Permits counting down using long integer. </summary>
    /// <remarks>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2006-06-13, 1.0.2355 </para>
    /// </remarks>
    [DebuggerDisplay( "Current Value = {CurrentValue}" )]
    public class DownCounter
    {

        #region " COUNTER "

        /// <summary> Decrement the count. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public void Countdown()
        {
            this.CurrentValue -= this.DecrementValue;
        }

        /// <summary> Gets the current value of the counter. </summary>
        /// <value> The current value. </value>
        public int CurrentValue { get; private set; }

        /// <summary>
        /// Gets or sets or set the amount by which the count is decremented by on each countdown.
        /// </summary>
        /// <value> The decrement value. </value>
        public int DecrementValue { get; set; }

        /// <summary> Gets or sets the initial count down value. </summary>
        /// <value> The initial value. </value>
        public int InitialValue { get; set; }

        /// <summary> Returns true if countdown is completed. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The is countdown done. </returns>
        public bool IsCountdownDone()
        {
            return this.CurrentValue < 0;
        }

        /// <summary> Restart from the initial value. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public void Restart()
        {
            this.CurrentValue = this.InitialValue;
        }

        #endregion

    }
}
