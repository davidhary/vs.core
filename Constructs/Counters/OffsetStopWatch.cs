using System;
using System.Diagnostics;

namespace isr.Core.Constructs
{

    /// <summary> A stop watch with Offset preset. </summary>
    /// <remarks>
    /// (c) 2009 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2009-05-06, 1.1.3413 </para>
    /// </remarks>
    [DebuggerDisplay( "Elapsed = {Elapsed}" )]
    public class OffsetStopwatch : Stopwatch
    {

        /// <summary>
        /// Constructs a new Offset stop watch with the specified offset (initial elapsed time)
        /// <paramref name="duration">duration</paramref>
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="duration"> Specifies the stop watch Offset duration. </param>
        public OffsetStopwatch( TimeSpan duration ) : base()
        {
            this.InitialElapsedTimespan = duration;
        }

        /// <summary>
        /// Constructs a new timeout stop watch using the specified stopwatch elapsed time as offset.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="stopwatch"> The stopwatch. </param>
        public OffsetStopwatch( Stopwatch stopwatch ) : this( stopwatch is null ? TimeSpan.Zero : stopwatch.Elapsed )
        {
        }

        /// <summary>
        /// Constructs a new Offset stop watch with the specified
        /// <paramref name="duration">duration</paramref>
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="duration"> Specifies the stop watch duration. </param>
        /// <returns> Returns true if Offset is done. </returns>
        public static OffsetStopwatch StartNew( TimeSpan duration )
        {
            var osw = new OffsetStopwatch( duration );
            osw.Start();
            return osw;
        }

        /// <summary> Gets or sets the initial elapsed time. </summary>
        /// <value> <c>Duration</c>is a TimeSpan property. </value>
        public TimeSpan InitialElapsedTimespan { get; set; }

        /// <summary> Gets the elapsed timespan including the initial offset time. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A TimeSpan. </returns>
        public new TimeSpan Elapsed()
        {
            return this.NetElapsed().Add( this.InitialElapsedTimespan );
        }

        /// <summary> Net elapsed time (without offset). </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A TimeSpan. </returns>
        public TimeSpan NetElapsed()
        {
            return base.Elapsed;
        }
    }
}
