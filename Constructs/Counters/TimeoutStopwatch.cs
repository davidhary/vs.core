using System;
using System.Diagnostics;

namespace isr.Core.Constructs
{

    /// <summary> A stop watch with timeout preset. </summary>
    /// <remarks>
    /// (c) 2016 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2009-05-06, 1.1.3413 </para>
    /// </remarks>
    [DebuggerDisplay( "Elapsed = {Elapsed}" )]
    public class TimeoutStopwatch : Stopwatch
    {

        /// <summary>
        /// Constructs a new timeout stop watch with the specified
        /// <paramref name="duration">duration</paramref>
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="duration"> Specifies the stop watch timeout duration. </param>
        public TimeoutStopwatch( TimeSpan duration ) : base()
        {
            this.Duration = duration;
        }

        /// <summary>
        /// Constructs a new timeout stop watch with the specified
        /// <paramref name="duration">duration</paramref>
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="duration"> Specifies the stop watch duration. </param>
        /// <returns> Returns true if timeout is done. </returns>
        public static TimeoutStopwatch StartNew( TimeSpan duration )
        {
            var osw = new TimeoutStopwatch( duration );
            osw.Start();
            return osw;
        }

        /// <summary> Gets or sets the duration of the timeout stop watch. </summary>
        /// <value> <c>Duration</c>is a TimeSpan property. </value>
        public TimeSpan Duration { get; set; }

        /// <summary> Returns true if timeout. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> Returns true if timeout. </returns>
        public bool IsTimeout()
        {
            return this.Elapsed > this.Duration;
        }

        /// <summary> Waits time timeout. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public void Wait()
        {
            if ( !this.IsRunning )
            {
                this.Start();
            }

            var interval = this.Duration - this.Elapsed;
            if ( interval > TimeSpan.Zero )
            {
                _ = System.Threading.Tasks.Task.Delay( this.Duration - this.Elapsed );
            }
        }
    }
}
