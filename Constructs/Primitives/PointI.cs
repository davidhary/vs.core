using System;

namespace isr.Core.Constructs
{

    /// <summary> Defines a <see cref="T:System.Int32">Int32</see> point. </summary>
    /// <remarks>
    /// (c) 2005 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2005-09-07, 1.0.2077 </para>
    /// </remarks>
    public class PointI
    {

        #region " SHARED "

        /// <summary> Gets a new instance of the zero [0,0] point value. </summary>
        /// <value> A <see cref="PointI"/> value. </value>
        public static PointI Zero => new( 0, 0 );

        /// <summary> Gets a new instance of the Unity [1,1] point value. </summary>
        /// <value> A <see cref="PointI"/> value. </value>
        public static PointI Unity => new( 1, 1 );

        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs a <see cref="PointI"/> instance by its limits. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="x"> A <see cref="T:System.Int32">Int32</see> expression that specifics the x
        /// element. </param>
        /// <param name="y"> A <see cref="T:System.Int32">Int32</see> expression that specifics the y
        /// element. </param>
        public PointI( int x, int y ) : base()
        {
            this.SetPointThis( x, y );
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The PointI object from which to copy. </param>
        public PointI( PointI model ) : base()
        {
            if ( model is object )
            {
                this.SetPointThis( model.X, model.Y );
            }
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as PointI );
        }

        /// <summary> Compares two ranges. </summary>
        /// <remarks>
        /// The two ranges are the same if the have the same minimum and maximum values.
        /// </remarks>
        /// <param name="other"> The other point to compare to this object. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( PointI other )
        {
            return other is object && other.X.Equals( this.X ) & other.Y.Equals( this.Y );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( PointI left, PointI right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operator. </returns>
        public static bool operator !=( PointI left, PointI right )
        {
            return !ReferenceEquals( left, right ) && (left is null || !left.Equals( right ));
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Single">Single</see> value. </returns>
        public override int GetHashCode()
        {
            return this.X.GetHashCode() ^ this.Y.GetHashCode();
        }

        #endregion

        #region " METHODS "

        /// <summary>
        /// Gets the exponent based on the point extremum values.  This is the
        /// <see cref="T:System.Int32">Int32</see> value representing the exponent of
        /// the most significant digit of point values.  For example, the 4 for 20,000 or -3 for 0.0012.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The exponent. </returns>
        public int GetExponent()
        {
            return Convert.ToInt32( Math.Max( NumericExtensions.NumericExtensionMethods.Exponent( this.X ), NumericExtensions.NumericExtensionMethods.Exponent( this.Y ) ) );
        }

        /// <summary>
        /// Gets the exponent based on the point values.  This is the
        /// <see cref="T:System.Int32">Int32</see> value representing the exponent of
        /// the most significant digit of point values limits.  For example, the 4 for 20,000 or -3 for
        /// 0.0012.  With engineering scales, the exponents are multiples of three, e.g., 20,000 yields
        /// +3 and 0.0001 -3.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="useEngineeringScale"> True to use scale exponent increments of 3. </param>
        /// <returns> The exponent. </returns>
        public int GetExponent( bool useEngineeringScale )
        {
            return Convert.ToInt32( Math.Max( NumericExtensions.NumericExtensionMethods.Exponent( this.X, useEngineeringScale ), NumericExtensions.NumericExtensionMethods.Exponent( this.Y, useEngineeringScale ) ) );
        }

        /// <summary> Sets the point based on the values. </summary>
        /// <remarks> Use this class to set the point. </remarks>
        /// <param name="x"> A <see cref="T:System.Int32">Int32</see> expression that specifies the x
        /// value of the point. </param>
        /// <param name="y"> A <see cref="T:System.Int32">Int32</see> expression that specifies the y
        /// value of the point. </param>
        private void SetPointThis( int x, int y )
        {
            if ( !this.X.Equals( x ) )
            {
                this.X = x;
            }

            if ( !this.Y.Equals( y ) )
            {
                this.Y = y;
            }
        }

        /// <summary> Sets the point based on the values. </summary>
        /// <remarks> Use this class to set the point. </remarks>
        /// <param name="x"> A <see cref="T:System.Int32">Int32</see> expression that specifies the x
        /// value of the point. </param>
        /// <param name="y"> A <see cref="T:System.Int32">Int32</see> expression that specifies the y
        /// value of the point. </param>
        public void SetPoint( int x, int y )
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary> Returns the default string representation of the point. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A representation of the point, e.g., '(x,y)' . </returns>
        public override string ToString()
        {
            return $"({this.X},{this.Y})";
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the x value of the point. </summary>
        /// <value> A <see cref="T:System.Int32">Int32</see> property. </value>
        public int X { get; set; }

        /// <summary> Gets the y value of the point. </summary>
        /// <value> A <see cref="T:System.Int32">Int32</see> property. </value>
        public int Y { get; set; }

        /// <summary> Gets the hypotenuse. </summary>
        /// <value> The hypotenuse. </value>
        public double Hypotenuse => GeometricMethods.Hypotenuse( this.X, this.Y );

        #endregion

        #region " ATTRIBUTES "

        /// <summary> Returns true if the range is <see cref="PointI.Zero"/>. </summary>
        /// <value> The is <see cref="PointI.Zero"/>. </value>
        public bool IsZero => this.Equals( Zero );

        /// <summary> Returns true if the range is <see cref="PointI.Unity"/>. </summary>
        /// <value> The is <see cref="PointI.Unity"/>. </value>
        public bool IsUnity => this.Equals( Unity );

        #endregion


    }
}
