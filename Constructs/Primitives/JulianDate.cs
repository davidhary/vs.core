
using System;
using System.Diagnostics;

namespace isr.Core.Constructs
{

    /// <summary>
    /// This structure encapsulates the Astronomical Julian Day and handles associated calculations
    /// and conversions between various formats.
    /// </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581 </para>
    /// </remarks>
    public struct JulianDate : ICloneable, IComparable
    {

        #region " SHARED PROPERTIES "

        /// <summary>Represents the zero JulianDate</summary>
        public const double Zero = 0d;

        /// <summary>The Astronomical Julian Day number that corresponds to XL Date 0.0</summary>
        public const double MSOfficeDayZero = 2415018.5d;

        /// <summary>The number of minutes in a second.</summary>
        public const double MinutesPerSecond = 1.0d / 60.0d;

        /// <summary>The number of days in a second.</summary>
        public const double DaysPerSecond = 1.0d / 86400.0d;

        /// <summary>The number of days in a Minute.</summary>
        public const double DaysPerMinute = 1.0d / 1440.0d;

        /// <summary>The number of days in an hour.</summary>
        public const double DaysPerHour = 1.0d / 24.0d;

        /// <summary>The number of days in a month.</summary>
        public const double DaysPerMonth = 365.0d / 12.0d;

        /// <summary>The number of days in a year.</summary>
        public const double DaysPerYear = 365.0d;

        /// <summary>The number of years in a month.</summary>
        public const double YearsPerMonth = 1d / 12.0d;

        /// <summary>The number of months in a year</summary>
        public const double MonthsPerYear = 12d;

        /// <summary>The number of hours in a day</summary>
        public const double HoursPerDay = 24d;

        /// <summary>The number of minutes in an hour</summary>
        public const double MinutesPerHour = 60d;

        /// <summary>The number of hours in a minute.</summary>
        public const double HoursPerMinute = 1d / 60d;

        /// <summary>The number of seconds in a minute</summary>
        public const double SecondsPerMinute = 60d;

        /// <summary>The number of minutes in a day</summary>
        public const double MinutesPerDay = 1440.0d;

        /// <summary>The number of seconds in a day</summary>
        public const double SecondsPerDay = 86400.0d;

        /// <summary>Gets or sets the default format string to be used in <see cref="ToString()"/> when
        /// no format is provided</summary>
        public static readonly string DefaultFormatString = "&d-&mmm-&yy &hh:&nn";

        #endregion

        #region " SHARED "

        /// <summary> Add a specified time interval value. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="addTo">        The left-hand-side of the '+-' operator (A JulianDate class) </param>
        /// <param name="valueToAdd">   . </param>
        /// <param name="intervalType"> . </param>
        /// <returns> The modified JulianDate. </returns>
        public static JulianDate Add( JulianDate addTo, double valueToAdd, TimeIntervalType intervalType )
        {
            switch ( intervalType )
            {
                case TimeIntervalType.Year:
                    {
                        addTo.AddYears( valueToAdd );
                        break;
                    }

                case TimeIntervalType.Month:
                    {
                        addTo.AddMonths( valueToAdd );
                        break;
                    }

                case TimeIntervalType.Day:
                    {
                        addTo.AddDays( valueToAdd );
                        break;
                    }

                case TimeIntervalType.Hour:
                    {
                        addTo.AddHours( valueToAdd );
                        break;
                    }

                case TimeIntervalType.Minute:
                    {
                        addTo.AddMinutes( valueToAdd );
                        break;
                    }

                case TimeIntervalType.Second:
                    {
                        addTo.AddSeconds( valueToAdd );
                        break;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "unhandled interval type" );
                        break;
                    }
            }

            return addTo;
        }

        /// <summary>
        /// Returns the number of days per time interval.  An average year is 365 days of 12 months
        /// giving about 30.41 days per month.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="timeInterval"> The <see cref="TimeIntervalType"/> </param>
        /// <returns> Days per time interval. </returns>
        public static double GetDaysPerTimeInterval( TimeIntervalType timeInterval )
        {
            switch ( timeInterval )
            {
                case TimeIntervalType.Year:
                    {
                        return DaysPerYear;
                    }

                case TimeIntervalType.Month:
                    {
                        return DaysPerMonth;
                    }

                case TimeIntervalType.Day:
                    {
                        return 1.0d;
                    }

                case TimeIntervalType.Hour:
                    {
                        return DaysPerHour;
                    }

                case TimeIntervalType.Minute:
                    {
                        return DaysPerMinute;
                    }

                case TimeIntervalType.Second:
                    {
                        return DaysPerSecond;
                    }

                default:
                    {
                        Debug.Assert( !Debugger.IsAttached, "unhandled interval type" );
                        return 1.0d;
                    }
            }
        }

        /// <summary>
        /// Calculate an Astronomical Julian Day Number from the specified Calendar date (year, month,
        /// day, hour, minute, second).  Presumes that the calendar date is normalized, i.e., month is
        /// between 1 and 12, minute is between 0 and 59, etc.
        /// </summary>
        /// <remarks> Taken from http://www.SRRB.noaa.gov/highlights/sunrise/program.txt. </remarks>
        /// <param name="year">        Normalized <see cref="T:System.Integer">integer</see> year value
        /// (e.g., 1994). </param>
        /// <param name="month">       Normalized <see cref="T:System.Integer">integer</see> month value
        /// (e.g., 7 for July). </param>
        /// <param name="day">         Normalized <see cref="T:System.Integer">integer</see> day value
        /// (e.g., 19 for the 19th day of the month). </param>
        /// <param name="hour">        Normalized <see cref="T:System.Integer">integer</see> hour value
        /// (e.g., 14 for 2:00 pm). </param>
        /// <param name="minute">      Normalized <see cref="T:System.Integer">integer</see> minute
        /// value (e.g., 35 for 35 minutes past the hour). </param>
        /// <param name="second">      Normalized <see cref="T:System.Integer">integer</see> second
        /// value (e.g., 42 for 42 seconds past the minute). </param>
        /// <param name="millisecond"> Normalized <see cref="T:System.Integer">integer</see> millisecond
        /// value past the second. </param>
        /// <returns> Astronomical Julian Day number. </returns>
        public static double FromNormalizedCalendarDate( int year, int month, int day, int hour, int minute, int second, int millisecond )
        {
            if ( month <= 2 )
            {
                year -= 1;
                month += 12;
            }

            double a = Math.Floor( year / 100.0d );
            double b = 2d - a + Math.Floor( a / 4.0d );
            return Math.Floor( 365.25d * (year + 4716.0d) ) + Math.Floor( 30.6001d * (month + 1) ) + day + b - 1524.5d + hour * DaysPerHour + minute * DaysPerMinute + (second + 0.001d * millisecond) * DaysPerSecond;
        }

        /// <summary>
        /// Calculate an Astronomical Julian Day from the specified Calendar date (year, month, day, hour,
        /// minute, second)
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="year">        The <see cref="T:System.Integer">integer</see> year value (e.g.,
        /// 1994). </param>
        /// <param name="month">       The <see cref="T:System.Integer">integer</see> month value (e.g.,
        /// 7 for July). </param>
        /// <param name="day">         The <see cref="T:System.Integer">integer</see> day value (e.g.,
        /// 19 for the 19th day of the month). </param>
        /// <param name="hour">        The <see cref="T:System.Integer">integer</see> hour value (e.g.,
        /// 14 for 2:00 pm). </param>
        /// <param name="minute">      The <see cref="T:System.Integer">integer</see> minute value (e.g.,
        /// 35 for 35 minutes past the hour). </param>
        /// <param name="second">      The <see cref="T:System.Integer">integer</see> second value (e.g.,
        /// 42 for 42 seconds past the minute). </param>
        /// <param name="millisecond"> The <see cref="T:System.Integer">integer</see> millisecond value
        /// past the second. </param>
        /// <returns> Astronomical Julian Day as <see cref="T:System.Double"/>. </returns>
        public static double FromArbitraryCalendarDate( int year, int month, int day, int hour, int minute, int second, int millisecond )
        {

            // Normalize the data to allow for negative and out of range values
            // In this way, setting month to zero would be December of the previous year,
            // setting hour to 24 would be the first hour of the next day, etc.
            NormalizeCalendarDate( ref year, ref month, ref day, ref hour, ref minute, ref second, ref millisecond );
            return FromNormalizedCalendarDate( year, month, day, hour, minute, second, millisecond );
        }

        /// <summary> Convert a .Net DateTime structure to Julian day. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> A <see cref="T:System.DateTime">DateTime</see> value. </param>
        /// <returns> Julian Day as <see cref="T:System.Double"/>. </returns>
        public static double FromDateTime( DateTime value )
        {
            return FromNormalizedCalendarDate( value.Year, value.Month, value.Day, value.Hour, value.Minute, value.Second, value.Millisecond );
        }

        /// <summary>
        /// Calculate a decimal year value (e.g., 1994.6523) corresponding to the specified XL date.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The decimal year value <see cref="T:System.Double">Double</see> type. </param>
        /// <returns> Julian Day as <see cref="T:System.Double"/>. </returns>
        public static double FromDecimalYear( double value )
        {
            int year = Convert.ToInt32( Math.Floor( value ) ); // was fix
            double julianDay1 = FromNormalizedCalendarDate( year, 1, 1, 0, 0, 0, 0 );
            double julianDay2 = FromNormalizedCalendarDate( year + 1, 1, 1, 0, 0, 0, 0 );
            return (value - year) * (julianDay2 - julianDay1) + julianDay1;
        }

        /// <summary>
        /// Calculate an XL Date corresponding to the specified Astronomical Julian Day number.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> Specifies an Excel type date. </param>
        /// <returns> Julian Day as <see cref="T:System.Double"/>. </returns>
        public static double FromExcelDate( double value )
        {
            return value + MSOfficeDayZero;
        }

        /// <summary>
        /// Calculate a Calendar date (year, month, day, hour, minute, second) corresponding to the
        /// Astronomical Julian Day number.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="dayValue">    The Astronomical Julian Day. </param>
        /// <param name="year">        [in,out] The <see cref="T:System.Integer">integer</see> year
        /// value (e.g., 1994). </param>
        /// <param name="month">       [in,out] The <see cref="T:System.Integer">integer</see> month
        /// value (e.g., 7 for July). </param>
        /// <param name="day">         [in,out] The <see cref="T:System.Integer">integer</see> day value
        /// (e.g., 19 for the 19th day of the month). </param>
        /// <param name="hour">        [in,out] The <see cref="T:System.Integer">integer</see> hour
        /// value (e.g., 14 for 2:00 pm). </param>
        /// <param name="minute">      [in,out] The <see cref="T:System.Integer">integer</see> minute
        /// value (e.g., 35 for 35 minutes past the hour). </param>
        /// <param name="second">      [in,out] The <see cref="T:System.Integer">integer</see> second
        /// value (e.g., 42 for 42 seconds past the minute). </param>
        /// <param name="millisecond"> [in,out] <see cref="T:System.Integer">integer</see> millisecond
        /// value past the second. </param>
        public static void ToCalendarDate( double dayValue, ref int year, ref int month, ref int day, ref int hour, ref int minute, ref int second, ref int millisecond )
        {
            double z = Math.Floor( dayValue + 0.5d );
            double f = dayValue + 0.5d - z;
            double alpha;
            double a;
            if ( z < 2299161d )
            {
                a = z;
            }
            else
            {
                alpha = Math.Floor( (z - 1867216.25d) / 36524.25d );
                a = z + 1.0d + alpha - Math.Floor( alpha / 4d );
            }

            double b = a + 1524.0d;
            double c = Math.Floor( (b - 122.1d) / 365.25d );
            double d = Math.Floor( 365.25d * c );
            double e = Math.Floor( (b - d) / 30.6001d );
            day = Convert.ToInt32( Math.Floor( b - d - Math.Floor( 30.6001d * e ) + f ), System.Globalization.CultureInfo.CurrentCulture );
            month = e < 14.0d ? Convert.ToInt32( e - 1.0d, System.Globalization.CultureInfo.CurrentCulture ) : Convert.ToInt32( e - 13.0d, System.Globalization.CultureInfo.CurrentCulture );
            year = month > 2 ? Convert.ToInt32( c - 4716d, System.Globalization.CultureInfo.CurrentCulture ) : Convert.ToInt32( c - 4715d, System.Globalization.CultureInfo.CurrentCulture );

            // get the day fraction offset by half a day because Julian day is offset by
            // 12 hours
            double tempDay = dayValue - 0.5d - Math.Floor( dayValue - 0.5d );
            // Dim days As integer = Convert.ToInt32(Math.Floor(tempDay))
            tempDay = (tempDay - Math.Floor( tempDay )) * HoursPerDay;
            hour = Convert.ToInt32( Math.Floor( tempDay ) );
            tempDay = (tempDay - hour) * MinutesPerHour;
            minute = Convert.ToInt32( Math.Floor( tempDay ) );
            tempDay = (tempDay - minute) * SecondsPerMinute;
            second = Convert.ToInt32( Math.Floor( tempDay ), System.Globalization.CultureInfo.CurrentCulture );
            tempDay = (tempDay - second) * 1000d;
            millisecond = Convert.ToInt32( Math.Floor( tempDay ), System.Globalization.CultureInfo.CurrentCulture );
        }

        /// <summary>
        /// Calculate a day-of-week value (e.g., Sun=0, Mon=1, Tue=2, etc.)
        /// corresponding to the specified Julian Day.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="dayValue"> The Astronomical Julian Day. </param>
        /// <returns> The corresponding day-of-week (DoW) value, expressed in integer format. </returns>
        public static int ToDayOfWeek( double dayValue )
        {
            return Convert.ToInt32( Math.Floor( (dayValue + 1.5d) % 7d ) ); // was fix
        }

        /// <summary>
        /// Calculate a day-of-year value (e.g., 241.543 corresponds to the 241st day of the year)
        /// corresponding to the specified Julian Day.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="dayValue"> The Astronomical Julian Day. </param>
        /// <returns> The corresponding day-of-year (DoY) value. </returns>
        public static double ToDayOfYear( double dayValue )
        {
            int year = default, month = default, day = default, hour = default, minute = default, second = default, millisecond = default;
            ToCalendarDate( dayValue, ref year, ref month, ref day, ref hour, ref minute, ref second, ref millisecond );
            return dayValue - FromNormalizedCalendarDate( year, 1, 1, 0, 0, 0, 0 ) + 1.0d;
        }

        /// <summary> Convert a Julian Day to a .Net DateTime structure. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="dayValue"> The Astronomical Julian Day. </param>
        /// <returns> <see cref="T:System.DateTime">DateTime</see> </returns>
        public static DateTime ToDateTime( double dayValue )
        {
            int year = default, month = default, day = default, hour = default, minute = default, second = default, millisecond = default;
            ToCalendarDate( dayValue, ref year, ref month, ref day, ref hour, ref minute, ref second, ref millisecond );
            return new DateTime( year, month, day, hour, minute, second, millisecond );
        }

        /// <summary>
        /// Calculate a decimal year value (e.g., 1994.6523) Corresponding to the specified Julian Day.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="dayValue"> The Astronomical Julian Day. </param>
        /// <returns> The decimal year value. </returns>
        public static double ToDecimalYear( double dayValue )
        {
            int year = default, month = default, day = default, hour = default, minute = default, second = default, millisecond = default;
            ToCalendarDate( dayValue, ref year, ref month, ref day, ref hour, ref minute, ref second, ref millisecond );
            double jDay1 = FromNormalizedCalendarDate( year, 1, 1, 0, 0, 0, 0 );
            double jDay2 = FromNormalizedCalendarDate( year + 1, 1, 1, 0, 0, 0, 0 );
            double jDayMid = FromNormalizedCalendarDate( year, month, day, hour, minute, second, millisecond );
            return year + (jDayMid - jDay1) / (jDay2 - jDay1);
        }

        /// <summary>
        /// Calculate an Astronomical Julian Day number corresponding to the specified XL date.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="dayValue"> The Astronomical Julian Day. </param>
        /// <returns> The Excel date <see cref="T:System.Double">Double</see> type. </returns>
        public static double ToExcelDate( double dayValue )
        {
            return dayValue - MSOfficeDayZero;
        }

        /// <summary> Format the specified XL Date value using the specified format string. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="dayValue">   The Astronomical Julian Day. </param>
        /// <param name="dateFormat"> The formatting string to be used for the date.  The following
        /// formatting elements will be replaced with the corresponding date values:
        /// <list type="table"> <listheader> <term>Variable</term>
        /// <description>Description</description> </listheader><item><term>
        /// &amp;mmmm</term><description>month name (e.g., January)</description></item><item><term>
        /// &amp;mmm</term><description>month abbreviation (e.g., Apr)</description></item><item><term>
        /// &amp;mm</term><description>padded month number (e.g. 04)</description></item><item><term>
        /// &amp;m</term><description>non-padded month number (e.g., 4)</description></item><item><term>
        /// &amp;dd</term><description>padded day number (e.g., 09)</description></item>  <item><term>
        /// &amp;d</term><description>non-padded day number (e.g., 9)</description></item><item><term>
        /// &amp;yyyy</term><description>4 digit year number (e.g., 1995)</description></item><item><term>
        /// &amp;yy</term><description>two digit year number (e.g., 95)</description></item><item><term>
        /// &amp;hh</term><description>padded 24 hour time value (e.g., 08)</description></item><item><term>
        /// &amp;h</term><description>non-padded 12 hour time value (e.g., 8)</description></item><item><term>
        /// &amp;nn</term><description>padded minute value (e.g, 05)</description></item><item><term>
        /// &amp;n</term><description>non-padded minute value (e.g., 5)</description></item><item><term>
        /// &amp;ss</term><description>padded second value (e.g., 03)</description></item><item><term>
        /// &amp;s</term><description>non-padded second value (e.g., 3)</description></item><item><term>
        /// &amp;a</term><description>"am" or "pm"</description></item><item><term>
        /// &amp;wwww</term><description>day of week (e.g., Wednesday)</description></item><item><term>
        /// &amp;www</term><description>day of week abbreviation (e.g., Wed)</description></item> </list> </param>
        /// <returns> A <see cref="T:System.String">String</see> representation of the date. </returns>
        /// <example> <para>
        /// "&amp;wwww, &amp;mmmm &amp;dd, &amp;yyyy &amp;h:&amp;nn &amp;a" ==&gt; "Sunday, February 12, 1956 4:23 pm"</para> <para>
        /// "&amp;dd-&amp;mmm-&amp;yy" ==&gt; 12-Feb-56</para></example>
        public static string ToString( double dayValue, string dateFormat )
        {
            if ( string.IsNullOrWhiteSpace( dateFormat ) )
            {
                dateFormat = "MM/dd/yyyy";
            }

            var longMonth = new[] { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            var int16Month = new[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
            var longDoW = new[] { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
            var int16DoW = new[] { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };
            int year = default, month = default, day = default, hour = default, minute = default, second = default, millisecond = default;
            ToCalendarDate( dayValue, ref year, ref month, ref day, ref hour, ref minute, ref second, ref millisecond );
            string result = dateFormat.Replace( "&mmmm", longMonth[month - 1] );
            result = result.Replace( "&mmm", int16Month[month - 1] );
            result = result.Replace( "&mm", month.ToString( "d2", System.Globalization.CultureInfo.CurrentCulture ) );
            result = result.Replace( "&m", month.ToString( "d", System.Globalization.CultureInfo.CurrentCulture ) );
            result = result.Replace( "&yyyy", year.ToString( "d", System.Globalization.CultureInfo.CurrentCulture ) );
            result = result.Replace( "&yy", (year % 100).ToString( "d", System.Globalization.CultureInfo.CurrentCulture ) );
            result = result.Replace( "&dd", day.ToString( "d2", System.Globalization.CultureInfo.CurrentCulture ) );
            result = result.Replace( "&d", day.ToString( "d", System.Globalization.CultureInfo.CurrentCulture ) );
            result = result.Replace( "&hh", hour.ToString( "d2", System.Globalization.CultureInfo.CurrentCulture ) );
            result = result.Replace( "&h", ((hour + 11) % 12 + 1).ToString( "d", System.Globalization.CultureInfo.CurrentCulture ) );
            result = result.Replace( "&nn", minute.ToString( "d2", System.Globalization.CultureInfo.CurrentCulture ) );
            result = result.Replace( "&n", minute.ToString( "d", System.Globalization.CultureInfo.CurrentCulture ) );
            result = result.Replace( "&ss", second.ToString( "d2", System.Globalization.CultureInfo.CurrentCulture ) );
            result = result.Replace( "&s", second.ToString( "d", System.Globalization.CultureInfo.CurrentCulture ) );
            result = result.Replace( "&mss", millisecond.ToString( "d2", System.Globalization.CultureInfo.CurrentCulture ) );
            result = result.Replace( "&ms", millisecond.ToString( "d", System.Globalization.CultureInfo.CurrentCulture ) );
            result = hour >= 12 ? result.Replace( "&a", "pm" ) : result.Replace( "&a", "am" );
            result = result.Replace( "&wwww", longDoW[ToDayOfWeek( dayValue )] );
            result = result.Replace( "&www", int16DoW[ToDayOfWeek( dayValue )] );
            return result;
        }

        /// <summary>
        /// Format this JulianDate value using the default format string
        /// (<see cref="DefaultFormatString"/>).
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="dayValue"> The Astronomical Julian Day. </param>
        /// <returns> A <see cref="T:System.String">String</see> representation of the date. </returns>
        public static string ToString( double dayValue )
        {
            return ToString( dayValue, DefaultFormatString );
        }

        /// <summary>
        /// Normalize a set of Calendar date values (year, month, day, hour, minute, second) to make sure
        /// that month is between 1 and 12, hour is between 0 and 23, etc.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="year">        [in,out] The <see cref="T:System.Integer">integer</see> year
        /// value (e.g., 1994). </param>
        /// <param name="month">       [in,out] The <see cref="T:System.Integer">integer</see> month
        /// value (e.g., 7 for July). </param>
        /// <param name="day">         [in,out] The <see cref="T:System.Integer">integer</see> day value
        /// (e.g., 19 for the 19th day of the month). </param>
        /// <param name="hour">        [in,out] The <see cref="T:System.Integer">integer</see> hour
        /// value (e.g., 14 for 2:00 pm). </param>
        /// <param name="minute">      [in,out] The <see cref="T:System.Integer">integer</see> minute
        /// value (e.g., 35 for 35 minutes past the hour). </param>
        /// <param name="second">      [in,out] The <see cref="T:System.Integer">integer</see> second
        /// value (e.g., 42 for 42 seconds past the minute). </param>
        /// <param name="millisecond"> [in,out] The <see cref="T:System.Integer">integer</see>
        /// millisecond value past the second. </param>
        private static void NormalizeCalendarDate( ref int year, ref int month, ref int day, ref int hour, ref int minute, ref int second, ref int millisecond )
        {

            // Normalize the data to allow for negative and out of range values
            // In this way, setting month to zero would be December of the previous year,
            // setting hour to 24 would be the first hour of the next day, etc.
            // Normalize the seconds and carry over to minutes
            double carry = Math.Floor( 0.001d * millisecond );
            millisecond -= Convert.ToInt32( 1000d * carry );
            second += Convert.ToInt32( carry );
            carry = Math.Floor( second * MinutesPerSecond );
            second -= Convert.ToInt32( carry * SecondsPerMinute );
            minute += Convert.ToInt32( carry );

            // Normalize the minutes and carry over to hours
            carry = Math.Floor( minute * HoursPerMinute );
            minute -= Convert.ToInt32( carry * MinutesPerHour );
            hour += Convert.ToInt32( carry );

            // Normalize the hours and carry over to days
            carry = Math.Floor( hour * DaysPerHour );
            hour -= Convert.ToInt32( carry * HoursPerDay );
            day += Convert.ToInt32( carry );

            // Normalize the months and carry over to years
            carry = Math.Floor( month * YearsPerMonth );
            month -= Convert.ToInt32( carry * MonthsPerYear );
            year += Convert.ToInt32( carry );
        }

        #endregion

        #region " OPERATORS "

        /// <summary>
        /// '-' operator overload.  When two JulianDates are subtracted, the number of days between dates
        /// is returned.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
        /// <param name="right"> The right-hand-side of the '-' operator (A JulianDate class) </param>
        /// <returns>
        /// The days between dates, expressed as <see cref="T:System.Double">Double</see> type.
        /// </returns>
        public static double Subtract( JulianDate left, JulianDate right )
        {
            return left.JulianDay - right.JulianDay;
        }

        /// <summary>
        /// '-' operator overload.  When two JulianDates are subtracted, the number of days between dates
        /// is returned.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
        /// <param name="right"> The right-hand-side of the '-' operator (A JulianDate class) </param>
        /// <returns>
        /// The days between dates, expressed as <see cref="T:System.Double">Double</see> type.
        /// </returns>
        public static double operator -( JulianDate left, JulianDate right )
        {
            return left.JulianDay - right.JulianDay;
        }

        /// <summary>
        /// '-' operator overload.  When A <see cref="T:System.Double">Double</see> value is subtracted
        /// from A JulianDate, the result is a new JulianDate with the number of days subtracted.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
        /// <param name="right"> The right-hand-side of the '-' operator (A
        /// <see cref="T:System.Double">Double</see> value) </param>
        /// <returns> A JulianDate with the model number of days subtracted. </returns>
        public static JulianDate operator -( JulianDate left, double right )
        {
            left.JulianDay -= right;
            return left;
        }

        /// <summary>
        /// '+' operator overload.  When A <see cref="T:System.Double">Double</see> value is added to A
        /// JulianDate, the result is a new JulianDate with the number of days added.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
        /// <param name="right"> The right-hand-side of the '+' operator (A
        /// <see cref="T:System.Double">Double</see> value) </param>
        /// <returns> A JulianDate with the model number of days added. </returns>
        public static JulianDate operator +( JulianDate left, double right )
        {
            left.JulianDay += right;
            return left;
        }

        /// <summary> '++' operator overload.  Increment the date by one day. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="julianDate"> The JulianDate structure on which to operate. </param>
        /// <returns> A JulianDate one day later than the specified date. </returns>
        public static JulianDate PlusPlus( JulianDate julianDate )
        {
            julianDate.JulianDay += 1.0d;
            return julianDate;
        }

        /// <summary> '--' operator overload.  Decrement the date by one day. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="julianDate"> The JulianDate structure on which to operate. </param>
        /// <returns> A JulianDate one day prior to the specified date. </returns>
        public static JulianDate MinusMinus( JulianDate julianDate )
        {
            julianDate.JulianDay -= 1.0d;
            return julianDate;
        }

        /// <summary> The less then operator overload.  The Julian days are compared. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
        /// <param name="right"> The right-hand-side of the '-' operator (A JulianDate class) </param>
        /// <returns>
        /// <c>True</c> if the <para>left</para> Julian day is before the
        /// <para>right</para> Julian day.
        /// </returns>
        public static bool operator <( JulianDate left, JulianDate right )
        {
            return left.JulianDay < right.JulianDay;
        }

        /// <summary> The greater then operator overload.  The Julian days are compared. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
        /// <param name="right"> The right-hand-side of the '-' operator (A JulianDate class) </param>
        /// <returns>
        /// <c>True</c> if the <para>left</para> Julian day is after the
        /// <para>right</para> Julian day.
        /// </returns>
        public static bool operator >( JulianDate left, JulianDate right )
        {
            return left.JulianDay > right.JulianDay;
        }

        #endregion

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Constructs a date from the given date. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="julianDay"> Specifies the Julian day. </param>
        public JulianDate( double julianDay )
        {
            this.JulianDay = julianDay;
        }

        /// <summary>
        /// Constructs a date class from a calendar date (year, month, day).  Assumes the time of day is
        /// 00:00 hrs.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="year">  A <see cref="T:System.Integer">integer</see> value for the year, e.g.,
        /// 1995. </param>
        /// <param name="month"> A <see cref="T:System.Integer">integer</see> value for the month of the
        /// year, e.g., 8 for August.  It is permissible to have months outside of
        /// the 1-12 range, which will rollover to the previous or next year. </param>
        /// <param name="day">   A <see cref="T:System.Integer">integer</see> value for the day of the
        /// month,
        /// e.g., 23. It is permissible to have day numbers outside of the 1-31 range,
        /// which will rollover to the previous or next month and year. </param>
        public JulianDate( int year, int month, int day )
        {
            this.JulianDay = FromArbitraryCalendarDate( year, month, day, 0, 0, 0, 0 );
        }

        /// <summary>
        /// Constructs a date class from a calendar date and time (year, month, day, hour, minute,
        /// second).
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="year">        A <see cref="T:System.Integer">integer</see> value for the year,
        /// e.g., 1995. </param>
        /// <param name="month">       A <see cref="T:System.Integer">integer</see> value for the month
        /// of the year, e.g., 8 for August.  It is permissible to have
        /// months outside of the 1-12 range, which will rollover to the
        /// previous or next year. </param>
        /// <param name="day">         A <see cref="T:System.Integer">integer</see> value for the day of
        /// the month, e.g., 23. It is permissible to have day numbers
        /// outside of the 1-31 range, which will rollover to the previous or
        /// next month and year. </param>
        /// <param name="hour">        A <see cref="T:System.Integer">integer</see> value for the hour
        /// of the day, e.g. 15. It is permissible to have hour values
        /// outside the 0-23 range, which will rollover to the previous or
        /// next day. </param>
        /// <param name="minute">      A <see cref="T:System.Integer">integer</see> value for the minute,
        /// e.g. 45. It is permissible to have hour values outside the 0-59
        /// range, which will rollover to the previous or next hour. </param>
        /// <param name="second">      A <see cref="T:System.Integer">integer</see> value for the second,
        /// e.g. 35. It is permissible to have second values outside the 0-59
        /// range, which will rollover to the previous or next minute. </param>
        /// <param name="millisecond"> The <see cref="T:System.Integer">integer</see> millisecond value
        /// past the second. </param>
        public JulianDate( int year, int month, int day, int hour, int minute, int second, int millisecond )
        {
            this.JulianDay = FromArbitraryCalendarDate( year, month, day, hour, minute, second, millisecond );
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The object from which to copy. </param>
        public JulianDate( JulianDate model )
        {
            this.JulianDay = model.JulianDay;
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return obj is object && (ReferenceEquals( this, obj ) || this.Equals( ( JulianDate ) obj ));
        }

        /// <summary> Indicates whether this instance and a specified object are equal. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
        /// <param name="right"> The right-hand-side of the '-' operator (A JulianDate class) </param>
        /// <returns>
        /// <c>True</c> if <paramref name="left" /> and <paramref name="right" /> are the same type and
        /// represent the same value; otherwise, <c>False</c>.
        /// </returns>
        public static bool Equals( JulianDate left, JulianDate right )
        {
            return left.JulianDay.Equals( right.JulianDay );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="other"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Julian Dates are the same if the have the same
        /// <see cref="JulianDay"/> values.
        /// </remarks>
        /// <param name="other"> The other <see cref="JulianDate">JulianDate</see> to compare for equality
        /// with this instance. </param>
        /// <returns> <c>True</c> if equals; otherwise, <c>False</c>. </returns>
        public bool Equals( JulianDate other )
        {
            return this.JulianDay.Equals( other.JulianDay );
        }

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
        /// <param name="right"> The right-hand-side of the '-' operator (A JulianDate class) </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( JulianDate left, JulianDate right )
        {
            return Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left-hand-side of the '-' operator (A JulianDate class) </param>
        /// <param name="right"> The right-hand-side of the '-' operator (A JulianDate class) </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( JulianDate left, JulianDate right )
        {
            return !Equals( left, right );
        }

        #endregion

        #region " METHODS "

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A new, independent copy of the JulianDate. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A new, independent copy of the JulianDate. </returns>
        public JulianDate Copy()
        {
            return new JulianDate( this );
        }

        /// <summary>
        /// Format this JulianDate value using the default format string
        /// (<see cref="DefaultFormatString"/>).
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A <see cref="T:System.String">String</see> representation of the date. </returns>
        public override string ToString()
        {
            return ToString( this.JulianDay, DefaultFormatString );
        }

        /// <summary> Format this JulianDate value using the specified format string. </summary>
        /// <param name="dateFormat"> The formatting string to be used for the date.  The following
        /// formatting elements will be replaced with the corresponding date values:
        /// <list type="table"> <listheader> <term>Variable</term>
        /// <description>Description</description> </listheader><item><term>
        /// &amp;mmmm</term><description>month name (e.g., January)</description></item><item><term>
        /// &amp;mmm</term><description>month abbreviation (e.g., Apr)</description></item><item><term>
        /// &amp;mm</term><description>padded month number (e.g. 04)</description></item><item><term>
        /// &amp;m</term><description>non-padded month number (e.g., 4)</description></item><item><term>
        /// &amp;dd</term><description>padded day number (e.g., 09)</description></item>  <item><term>
        /// &amp;d</term><description>non-padded day number (e.g., 9)</description></item><item><term>
        /// &amp;yyyy</term><description>4 digit year number (e.g., 1995)</description></item><item><term>
        /// &amp;yy</term><description>two digit year number (e.g., 95)</description></item><item><term>
        /// &amp;hh</term><description>padded 24 hour time value (e.g., 08)</description></item><item><term>
        /// &amp;h</term><description>non-padded 12 hour time value (e.g., 8)</description></item><item><term>
        /// &amp;nn</term><description>padded minute value (e.g, 05)</description></item><item><term>
        /// &amp;n</term><description>non-padded minute value (e.g., 5)</description></item><item><term>
        /// &amp;ss</term><description>padded second value (e.g., 03)</description></item><item><term>
        /// &amp;s</term><description>non-padded second value (e.g., 3)</description></item><item><term>
        /// &amp;a</term><description>"am" or "pm"</description></item><item><term>
        /// &amp;wwww</term><description>day of week (e.g., Wednesday)</description></item><item><term>
        /// &amp;www</term><description>day of week abbreviation (e.g., Wed)</description></item> </list> </param>
        /// <returns> A <see cref="T:System.String">String</see> representation of the date. </returns>
        /// <example> <para>
        /// "&amp;wwww, &amp;mmmm &amp;dd, &amp;yyyy &amp;h:&amp;nn &amp;a" ==&gt; "Sunday, February 12, 1956 4:23 pm"</para> <para>
        /// "&amp;dd-&amp;mmm-&amp;yy" ==&gt; 12-Feb-56</para></example>
        public string ToString( string dateFormat )
        {
            return ToString( this.JulianDay, dateFormat );
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets or sets the astronomical Julian day. </summary>
        /// <remarks>
        /// The Astronomical Julian Day number is defined as the number of days since noon on January 1st,
        /// 4713 B.C. (also referred to as 12:00 on January 1, -4712).
        /// </remarks>
        /// <value> The Julian day. </value>
        public double JulianDay { get; set; }

        /// <summary> Gets or sets the date value in Excel Date format. </summary>
        /// <remarks>
        /// This format is in days since a reference date
        /// <see cref="MSOfficeDayZero"/>, on January 1, 1900 at 00:00 hrs.  Negative values
        /// are permissible, and the range of valid dates is from noon on January 1st, 4713 B.C. forward.
        /// </remarks>
        /// <value> The excel date. </value>
        public double ExcelDate
        {
            get => ToExcelDate( this.JulianDay );

            set => this.JulianDay = FromExcelDate( value );
        }

        /// <summary> Gets or sets the date value in .Net DateTime format. </summary>
        /// <value> The date time. </value>
        public DateTime DateTime
        {
            get => ToDateTime( this.JulianDay );

            set => this.JulianDay = FromDateTime( value );
        }

        /// <summary> Gets or sets the decimal year number (i.e., 1997.345). </summary>
        /// <value> The decimal year. </value>
        public double DecimalYear
        {
            get => ToDecimalYear( this.JulianDay );

            set => this.JulianDay = FromDecimalYear( value );
        }

        /// <summary> Gets the calendar date (year, month, day). </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="year">  [in,out] A <see cref="T:System.Integer">integer</see> value for the year,
        /// e.g., 1995. </param>
        /// <param name="month"> [in,out] A <see cref="T:System.Integer">integer</see> value for the month
        /// of the year, e.g., 8 for August. </param>
        /// <param name="day">   [in,out] A <see cref="T:System.Integer">integer</see> value for the day
        /// of the month, e.g., 23. </param>
        public void GetDate( ref int year, ref int month, ref int day )
        {
            int hour = default, minute = default, second = default, millisecond = default;
            ToCalendarDate( this.JulianDay, ref year, ref month, ref day, ref hour, ref minute, ref second, ref millisecond );
        }

        /// <summary> Set the date from the calendar date (year, month, day). </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="year">  A <see cref="T:System.Integer">integer</see> value for the year, e.g.,
        /// 1995. </param>
        /// <param name="month"> A <see cref="T:System.Integer">integer</see> value for the month of the
        /// year, e.g., 8 for August. </param>
        /// <param name="day">   A <see cref="T:System.Integer">integer</see> value for the day of the
        /// month,
        /// e.g., 23. </param>
        public void SetDate( int year, int month, int day )
        {
            this.JulianDay = FromArbitraryCalendarDate( year, month, day, 0, 0, 0, 0 );
        }

        /// <summary> Get the calendar date (year, month, day, hour, minute, second). </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="year">        [in,out] A <see cref="T:System.Integer">integer</see> value for
        /// the year, e.g., 1995. </param>
        /// <param name="month">       [in,out] A <see cref="T:System.Integer">integer</see> value for
        /// the month of the year, e.g., 8 for August. </param>
        /// <param name="day">         [in,out] A <see cref="T:System.Integer">integer</see> value for
        /// the day of the month, e.g., 23. </param>
        /// <param name="hour">        [in,out] A <see cref="T:System.Integer">integer</see> value for
        /// the hour of the day, e.g. 15. </param>
        /// <param name="minute">      [in,out] A <see cref="T:System.Integer">integer</see> value for
        /// the minute, e.g. 45. </param>
        /// <param name="second">      [in,out] A <see cref="T:System.Integer">integer</see> value for
        /// the second, e.g. 35. </param>
        /// <param name="millisecond"> <see cref="T:System.Integer">integer</see> millisecond value past
        /// the second. </param>
        public void GetDate( ref int year, ref int month, ref int day, ref int hour, ref int minute, ref int second, int millisecond )
        {
            ToCalendarDate( this.JulianDay, ref year, ref month, ref day, ref hour, ref minute, ref second, ref millisecond );
        }

        /// <summary>
        /// Set the date based on the calendar date (year, month, day, hour, minute, second).
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="year">        A <see cref="T:System.Integer">integer</see> value for the year,
        /// e.g., 1995. </param>
        /// <param name="month">       A <see cref="T:System.Integer">integer</see> value for the month
        /// of the year, e.g., 8 for August. </param>
        /// <param name="day">         A <see cref="T:System.Integer">integer</see> value for the day of
        /// the month, e.g., 23. </param>
        /// <param name="hour">        A <see cref="T:System.Integer">integer</see> value for the hour
        /// of the day, e.g. 15. </param>
        /// <param name="minute">      A <see cref="T:System.Integer">integer</see> value for the minute,
        /// e.g. 45. </param>
        /// <param name="second">      A <see cref="T:System.Integer">integer</see> value for the second,
        /// e.g. 35. </param>
        /// <param name="millisecond"> The <see cref="T:System.Integer">integer</see> millisecond value
        /// past the second. </param>
        public void SetDate( int year, int month, int day, int hour, int minute, int second, int millisecond )
        {
            this.JulianDay = FromArbitraryCalendarDate( year, month, day, hour, minute, second, millisecond );
        }

        /// <summary>
        /// Get the day of year value (241.345 means the 241st day of the year)
        /// corresponding to this instance.
        /// </summary>
        /// <value> The day of the year <see cref="T:System.Double">Double</see> type. </value>
        public double DayOfYear => ToDayOfYear( this.JulianDay );

        #endregion

        #region " Add Routines "

        /// <summary> Add the specified number of milliseconds (can be fractional). </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="milliseconds"> The incremental number of seconds (negative or positive)
        /// <see cref="T:System.Double">Double</see> type. </param>
        public void AddMilliseconds( double milliseconds )
        {
            this.JulianDay += 0.001d * milliseconds * DaysPerSecond;
        }

        /// <summary> Add the specified number of seconds (can be fractional). </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="seconds"> The incremental number of seconds (negative or positive)
        /// <see cref="T:System.Double">Double</see> type. </param>
        public void AddSeconds( double seconds )
        {
            this.JulianDay += seconds * DaysPerSecond;
        }

        /// <summary> Add the specified number of minutes (can be fractional). </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="minutes"> The incremental number of minutes (negative or positive)
        /// <see cref="T:System.Double">Double</see> type. </param>
        public void AddMinutes( double minutes )
        {
            this.JulianDay += minutes * DaysPerMinute;
        }

        /// <summary> Add the specified number of hours (can be fractional). </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="hours"> The incremental number of hours (negative or positive)
        /// <see cref="T:System.Double">Double</see> type. </param>
        public void AddHours( double hours )
        {
            this.JulianDay += hours * DaysPerHour;
        }

        /// <summary> Add the specified number of days (can be fractional). </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="days"> The incremental number of days (negative or positive)
        /// <see cref="T:System.Double">Double</see> type. </param>
        public void AddDays( double days )
        {
            this.JulianDay += days;
        }

        /// <summary> Add the specified number of Months (can be fractional). </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="months"> The incremental number of months (negative or positive)
        /// <see cref="T:System.Double">Double</see> type. </param>
        public void AddMonths( double months )
        {
            int iMon = Convert.ToInt32( Math.Floor( months ) ); // was fix
            double monFrac = Math.Abs( months - iMon );
            int sMon = Math.Sign( months );
            int year = default, month = default, day = default, hour = default, minute = default, second = default, millisecond = default;
            ToCalendarDate( this.JulianDay, ref year, ref month, ref day, ref hour, ref minute, ref second, ref millisecond );
            if ( iMon != 0 )
            {
                month += iMon;
                this.JulianDay = FromArbitraryCalendarDate( year, month, day, hour, minute, second, millisecond );
            }

            if ( sMon != 0 )
            {
                double xlDate2 = FromArbitraryCalendarDate( year, month + sMon, day, hour, minute, second, millisecond );
                this.JulianDay += (xlDate2 - this.JulianDay) * monFrac;
            }
        }

        /// <summary> Add the specified number of years (can be fractional). </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="years"> The incremental number of years (negative or positive)
        /// <see cref="T:System.Double">Double</see> type. </param>
        public void AddYears( double years )
        {
            int iYear = Convert.ToInt32( Math.Floor( years ) ); // was Fix
            double yearFrac = Math.Abs( years - iYear );
            int sYear = Math.Sign( years );
            int year = default, month = default, day = default, hour = default, minute = default, second = default, millisecond = default;
            ToCalendarDate( this.JulianDay, ref year, ref month, ref day, ref hour, ref minute, ref second, ref millisecond );
            if ( iYear != 0 )
            {
                year += iYear;
                this.JulianDay = FromArbitraryCalendarDate( year, month, day, hour, minute, second, millisecond );
            }

            if ( sYear != 0 )
            {
                double xlDate2 = FromArbitraryCalendarDate( year + sYear, month, day, hour, minute, second, millisecond );
                this.JulianDay += (xlDate2 - this.JulianDay) * yearFrac;
            }
        }

        #endregion

        #region " IComparable implementation "

        /// <summary>
        /// Compares the current instance with another object of the same type and returns an integer
        /// that indicates whether the current instance precedes, follows, or occurs in the same position
        /// in the sort order as the other object.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The object to compare for equality with this instance. This object should
        /// be type <see cref="JulianDate"/>. </param>
        /// <returns>
        /// A value that indicates the relative order of the objects being compared. The return value has
        /// these meanings: Value Meaning Less than zero This instance is less than
        /// <paramref name="obj" />. Zero This instance is equal to <paramref name="obj" />. Greater than
        /// zero This instance is greater than <paramref name="obj" />.
        /// </returns>
        public int CompareTo( object obj )
        {
            return obj is JulianDate
                ? (( JulianDate ) obj).JulianDay.CompareTo( this.JulianDay )
                : obj is double ? Convert.ToDouble( obj, System.Globalization.CultureInfo.CurrentCulture ).CompareTo( this.JulianDay ) : 0;
        }

        /// <summary>
        /// Returns the hash code for this <see cref="JulianDate"/> structure. In this case, the hash
        /// code is simply the equivalent hash code for the ,<see cref="JulianDate.JulianDay"/>
        /// value.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return this.JulianDay.GetHashCode();
        }

        #endregion

    }

    #region " TYPES "

    /// <summary> Enumeration type for date and time interval types. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public enum TimeIntervalType
    {

        /// <summary> . </summary>
        [System.ComponentModel.Description( "Year" )]
        Year,

        /// <summary> . </summary>
        [System.ComponentModel.Description( "Month" )]
        Month,

        /// <summary> . </summary>
        [System.ComponentModel.Description( "Day" )]
        Day,

        /// <summary> . </summary>
        [System.ComponentModel.Description( "Hour" )]
        Hour,

        /// <summary> . </summary>
        [System.ComponentModel.Description( "Minute" )]
        Minute,

        /// <summary> . </summary>
        [System.ComponentModel.Description( "Second" )]
        Second
    }
}

#endregion

