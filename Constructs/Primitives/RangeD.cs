using System;
using System.Collections.Generic;
using System.Linq;

namespace isr.Core.Constructs
{

    /// <summary> Defines a <see cref="T:System.Decimal">Decimal</see> range class. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License. </para><para>
    /// David, 2004-04-30, 1.0.1581 </para>
    /// </remarks>
    public class RangeD : ICloneable
    {

        #region " SHARED "

        /// <summary> Gets a new instance of the empty range. </summary>
        /// <value> A <see cref="RangeD"/> value with [0,0] for the [Min,Max] values. </value>
        public static RangeD Empty => new( 0m, 0m );

        /// <summary> Gets a new instance of the full range. </summary>
        public static RangeD Full => new( decimal.MinValue, decimal.MaxValue );

        /// <summary> Gets a new instance of the full non-negative range. </summary>
        /// <value> The full nonnegative. </value>
        public static RangeD FullNonnegative => new( 0m, decimal.MaxValue );

        /// <summary> Gets a new instance of the Unity range. </summary>
        /// <value> A <see cref="RangeD"/> [0,1] value. </value>
        public static RangeD Unity => new( 0m, 1m );

        /// <summary> Gets a new instance of the zero range. </summary>
        /// <value> The zero. </value>
        public static RangeD Zero => new( 0m, 0m );

        /// <summary> Return the range of the specified data array. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="values"> The data array. </param>
        /// <returns> The calculated range. </returns>
        public static RangeD GetRange( IEnumerable<decimal> values )
        {
            // return the unit range if no data
            if ( values is null || !values.Any() )
            {
                return Unity;
            }

            decimal min = decimal.MaxValue;
            decimal max = decimal.MinValue;
            foreach ( decimal temp in values )
            {
                if ( temp < min )
                {
                    min = temp;
                }
                else if ( temp > max )
                {
                    max = temp;
                }
            }

            return new RangeD( min, max );
        }

        /// <summary> Extended range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="range"> A <see cref="RangeD"/> value. </param>
        /// <returns> A RangeD. </returns>
        public RangeD ExtendedRange( RangeD range )
        {
            var result = new RangeD( this );
            result.ExtendRange( range );
            return result;
        }

        /// <summary> Shifted range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A RangeD. </returns>
        public RangeD ShiftedRange( decimal value )
        {
            var result = new RangeD( this );
            result.ShiftRange( value );
            return result;
        }

        /// <summary> Transposed range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="shift"> The shift. </param>
        /// <param name="scale"> The scale. </param>
        /// <returns> A RangeD. </returns>
        public RangeD TransposedRange( decimal shift, decimal scale )
        {
            var result = new RangeD( this );
            result.TransposeRange( shift, scale );
            return result;
        }

        #endregion

        #region " CONSTRUCTION "

        /// <summary> Constructs a <see cref="RangeD"/> instance by its span. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="span"> A <see cref="T:System.Decimal">Decimal</see> property. </param>
        public RangeD( decimal span ) : this( -0.5m * span, 0.5m * span, 0m )
        {
        }

        /// <summary> Constructs a <see cref="RangeD"/> instance by its limits. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="minValue"> A <see cref="T:System.Decimal">Decimal</see> expression that
        /// specifics the minimum range. </param>
        /// <param name="maxValue"> A <see cref="T:System.Decimal">Decimal</see> expression that
        /// specifics the maximum range. </param>
        public RangeD( decimal minValue, decimal maxValue ) : this( minValue, maxValue, 0m )
        {
        }

        /// <summary>
        /// Constructs a <see cref="RangeD"/>
        /// instance by its limits.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="minValue">   A <see cref="T:System.Decimal">
        /// Decimal</see>
        /// expression that specifics the minimum range. </param>
        /// <param name="maxValue">   A <see cref="T:System.Decimal">
        /// Decimal</see>
        /// expression that specifics the maximum range. </param>
        /// <param name="resolution"> The resolution. </param>
        public RangeD( decimal minValue, decimal maxValue, decimal resolution ) : base()
        {
            this.SetRange( minValue, maxValue );
            this.Resolution = resolution;
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The RangeD object from which to copy. </param>
        public RangeD( RangeD model ) : base()
        {
            if ( model is object )
            {
                this.SetRange( model.Min, model.Max );
                this.Resolution = model.Resolution;
            }
        }

        #endregion

        #region " EQUALS "

        /// <summary> = casting operator. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( RangeD left, RangeD right )
        {
            return left is null ? right is null : !(right is null) && Equals( left, right );
        }

        /// <summary> &lt;&gt; casting operator. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( RangeD left, RangeD right )
        {
            return !Equals( left, right );
        }

        /// <summary> Returns True if equal. </summary>
        /// <remarks>
        /// Ranges are the same if the have the same
        /// <see cref="Type">min</see> and <see cref="Type">max</see> values.
        /// </remarks>
        /// <param name="left">  The left hand side item to compare for equality. </param>
        /// <param name="right"> The left hand side item to compare for equality. </param>
        /// <returns> <c>True</c> if equals. </returns>
        public static bool Equals( RangeD left, RangeD right )
        {
            return left is null ? right is null : !(right is null) && left.Max.Equals( right.Max ) && left.Min.Equals( right.Min );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, <c>False</c>.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as RangeD );
        }

        /// <summary>
        /// Returns True if the value of the <paramref name="other"/> equals to the instance value.
        /// </summary>
        /// <remarks>
        /// Ranges are the same if the have the same
        /// <see cref="Type">min</see> and <see cref="Type">max</see> values.
        /// </remarks>
        /// <param name="other"> The other <see cref="RangeD">Range</see> to compare for equality with
        /// this instance. </param>
        /// <returns> A Boolean data type. </returns>
        public bool Equals( RangeD other )
        {
            return other is object && Equals( this, other );
        }

        #endregion

        #region " METHODS "

        /// <summary> Clips the given value. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value. </param>
        /// <returns> A Date. </returns>
        public decimal Clip( decimal value )
        {
            return value < this.Min ? this.Min : value > this.Max ? this.Max : value;
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A new, independent copy of the structure1. </returns>
        public object Clone()
        {
            return this.Copy();
        }

        /// <summary> Deep-copy clone routine. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A new, independent copy of  RangeD. </returns>
        public RangeD Copy()
        {
            return new RangeD( this );
        }

        /// <summary> Query if 'point' is inside the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point"> A <see cref="T:System.Decimal">Decimal</see> point value&gt; </param>
        /// <returns> <c>true</c> if inside; otherwise <c>false</c> </returns>
        public bool Encloses( decimal point )
        {
            return point > this.Min && point < this.Max;
        }

        /// <summary> Returns true if the point value is within the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point"> A <see cref="T:System.Decimal">Decimal</see> point value&gt; </param>
        /// <returns>
        /// <c>True</c> if value above or equal to minimum or below or equal to maximum.
        /// </returns>
        public bool Contains( decimal point )
        {
            return point >= this.Min && point <= this.Max;
        }

        /// <summary> Returns true if the point value is within the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="point">     A <see cref="T:System.Decimal">Decimal</see> point value&gt; </param>
        /// <param name="tolerance"> Tolerance for comparison. </param>
        /// <returns>
        /// <c>True</c> if value above or equal to minimum - tolerance or below or equal to maximum +
        /// tolerance.
        /// </returns>
        public bool Contains( decimal point, decimal tolerance )
        {
            return point >= this.Min - tolerance && point <= this.Max + tolerance;
        }

        /// <summary>
        /// Extends this range to include both its present values and the specified range.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="range"> A <see cref="RangeD"/> value. </param>
        public void ExtendRange( RangeD range )
        {
            if ( range is null )
            {
                throw new ArgumentNullException( nameof( range ) );
            }

            if ( this.Min > range.Min )
            {
                this.Min = range.Min;
            }

            if ( this.Max < range.Max )
            {
                this.Max = range.Max;
            }
        }

        /// <summary> Shift range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> The value by which to shift the range. </param>
        public void ShiftRange( decimal value )
        {
            this.SetRange( value + this.Min, value + this.Max );
        }

        /// <summary> Transpose range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="shift"> The shift. </param>
        /// <param name="scale"> The scale. </param>
        public void TransposeRange( decimal shift, decimal scale )
        {
            this.SetRange( shift + scale * this.Min, shift + scale * this.Max );
        }

        /// <summary> Sets the range based on the extrema. </summary>
        /// <remarks> Use this class to set the range. </remarks>
        /// <param name="minValue"> A <see cref="T:System.Decimal">Decimal</see> expression that
        /// specifies the minimum value of the range. </param>
        /// <param name="maxValue"> A <see cref="T:System.Decimal">Decimal</see> expression that
        /// specifies the maximum value of the range. </param>
        public void SetRange( decimal minValue, decimal maxValue )
        {
            this.Min = minValue;
            this.Max = maxValue;
        }

        /// <summary> Sets the range based on the span. </summary>
        /// <remarks> Use this class to set the range. </remarks>
        /// <param name="span"> A <see cref="T:System.Decimal">Decimal</see> property. </param>
        public void SetRange( decimal span )
        {
            this.Min = -0.5m * span;
            this.Max = +0.5m * span;
        }

        /// <summary> Sets the range based on the extrema. </summary>
        /// <remarks> Use this class to set the range. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="range"> A <see cref="RangeD"/> value. </param>
        public void SetRange( RangeD range )
        {
            if ( range is null )
            {
                throw new ArgumentNullException( nameof( range ) );
            }

            this.SetRange( range.Min, range.Max );
        }

        /// <summary>
        /// Gets the exponent based on the range extremum values.  This is the
        /// <see cref="T:System.Integer">integer</see> value representing the exponent of
        /// the most significant digit of range limits.  For example, the 4 for 20,000 or -3 for 0.0012.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The exponent. </returns>
        public int GetExponent()
        {
            return Convert.ToInt32( Math.Max( NumericExtensions.NumericExtensionMethods.Exponent( ( float ) this.Min ), NumericExtensions.NumericExtensionMethods.Exponent( ( float ) this.Max ) ) );
        }

        /// <summary>
        /// Gets the exponent based on the range extremum values.  This is the
        /// <see cref="T:System.Integer">integer</see> value representing the exponent of
        /// the most significant digit of range limits.  For example, the 4 for 20,000 or -3 for 0.0012.
        /// With engineering scales, the exponents are multiples of three, e.g., 20,000 yields +3 and
        /// 0.0001 -3.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="useEngineeringScale"> True to use scale exponent increments of 3. </param>
        /// <returns> The exponent. </returns>
        public int GetExponent( bool useEngineeringScale )
        {
            return Convert.ToInt32( Math.Max( NumericExtensions.NumericExtensionMethods.Exponent( ( float ) this.Min, useEngineeringScale ), NumericExtensions.NumericExtensionMethods.Exponent( ( float ) this.Max, useEngineeringScale ) ) );
        }

        /// <summary> Returns a new random number uniformly distributed between [min,max). </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="generator"> The random number generator. </param>
        /// <returns> A Decimal. </returns>
        public decimal NextRandom( Random generator )
        {
            return generator is null
                ? throw new ArgumentNullException( nameof( generator ) )
                : this.Min + this.Span * ( decimal ) generator.NextDouble();
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Integer">integer</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Min.GetHashCode() ^ this.Max.GetHashCode();
        }

        /// <summary> Returns the default string representation of the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> A representation of the range, e.g., '(min,max)' . </returns>
        public override string ToString()
        {
            return $"({this.Min},{this.Max})";
        }

        #endregion

        #region " PROPERTIES "

        /// <summary> Gets the maximum value of the range. </summary>
        /// <value> A <see cref="T:System.Decimal">Decimal</see> property. </value>
        public decimal Max { get; set; }

        /// <summary> Returns the mid range point of the range. </summary>
        /// <value> A read only <see cref="T:System.Decimal">Decimal</see> property. </value>
        public decimal Midrange => 0.5m * (this.Max + this.Min);

        /// <summary> Gets the minimum value of the range. </summary>
        /// <value> A <see cref="T:System.Decimal">Decimal</see> property. </value>
        public decimal Min { get; set; }

        /// <summary> Gets the span of the range. </summary>
        /// <value> A <see cref="T:System.Decimal">Decimal</see> property. </value>
        public decimal Span => this.Max - this.Min;

        /// <summary> Gets the resolution. </summary>
        /// <value> The resolution. </value>
        public decimal Resolution { get; set; }

        /// <summary> Gets the decimal places. </summary>
        /// <value> The decimal places. </value>
        public int DecimalPlaces => this.Resolution <= 0m || this.Resolution >= 1m ? 0 : -( int ) Math.Floor( Math.Log10( ( double ) this.Resolution ) );

        #endregion

        #region " ATTRIBUTES "

        /// <summary> Returns true if the range is <see cref="RangeD.Empty"/>. </summary>
        /// <value> The is <see cref="RangeD.Empty"/>. </value>
        public bool IsEmpty => this.Equals( Empty );

        /// <summary> Returns true if the range is <see cref="RangeD.Unity"/>. </summary>
        /// <value> The is <see cref="RangeD.Unity"/>. </value>
        public bool IsUnity => this.Equals( Unity );

        #endregion


    }
}
