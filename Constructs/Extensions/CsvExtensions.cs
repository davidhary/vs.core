using System.Collections.Generic;
using System.Linq;

namespace isr.Core.Constructs.CommaSeparatedValuesExtensions
{

    /// <summary> Extension methods for building dictionaries from comma-separated-values. </summary>
    /// <remarks> (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para> </remarks>
    public static class CommaSeparatedValuesExtensionMethods
    {

        /// <summary> Builds a Dictionary(Of String, RangeR). </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="keyValuePairs"> The key value pairs comma-separated text. </param>
        /// <param name="key">           A key argument for permitting overrides. </param>
        /// <param name="value">         A value argument for permitting overrides. </param>
        /// <returns> A Dictionary(Of Integer, String) </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0060:Remove unused parameter", Justification = "<Pending>" )]
        public static Dictionary<string, RangeR> BuildDictionary( this string keyValuePairs, string key, RangeR value )
        {
            var dix = new Dictionary<string, RangeR>();
            var values = Core.CommaSeparatedValuesExtensions.CommaSeparatedValuesExtensionMethods.SplitQueue( keyValuePairs );
            while ( values.Any() )
            {
                key = values.Dequeue();
                if ( values.Any() && double.TryParse( values.Dequeue(), out double min ) )
                {
                    if ( values.Any() && double.TryParse( values.Dequeue(), out double max ) )
                    {
                        dix.Add( key, new RangeR( min, max ) );
                    }
                }
            }

            return dix;
        }
    }
}
