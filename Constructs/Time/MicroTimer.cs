using System;
using System.Diagnostics;

using Microsoft.VisualBasic;

namespace isr.Core.Constructs
{

    /// <summary> A micro timer. </summary>
    /// <remarks>
    /// (c) 2013 Ken Loveday. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-11-25 </para><para>
    /// https://www.codeproject.com/Articles/98346/Microsecond-and-Millisecond-NET-Timer. </para>
    /// </remarks>
    public class MicroTimer
    {

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public MicroTimer() : base()
        {
            this._TimerThread = null;
            this._IgnoreEventIfLateBy = long.MaxValue;
            this._Interval = 0L;
            this._StopTimer = true;
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="microsecondsInterval"> The microseconds interval. </param>
        public MicroTimer( long microsecondsInterval ) : this()
        {
            this.Interval = microsecondsInterval;
        }

        /// <summary> The interval. </summary>
        private long _Interval;

        /// <summary> Gets or sets the interval in micro seconds. </summary>
        /// <value> The interval in micro seconds. </value>
        public long Interval
        {
            get => System.Threading.Interlocked.Read( ref this._Interval );

            set => System.Threading.Interlocked.Exchange( ref this._Interval, value );
        }

        /// <summary> Amount to ignore event if late by. </summary>
        private long _IgnoreEventIfLateBy;

        /// <summary> Gets or sets the amount to ignore event if late by. </summary>
        /// <value> Amount to ignore event if late by. </value>
        public long IgnoreEventIfLateBy
        {
            get => System.Threading.Interlocked.Read( ref this._IgnoreEventIfLateBy );

            set => System.Threading.Interlocked.Exchange( ref this._IgnoreEventIfLateBy, value <= 0L ? long.MaxValue : value );
        }

        /// <summary> The timer thread. </summary>
        private System.Threading.Thread _TimerThread;

        /// <summary> Gets or sets the enabled. </summary>
        /// <value> The enabled. </value>
        public bool Enabled
        {
            get => this._TimerThread is object && this._TimerThread.IsAlive;

            set {
                if ( value )
                {
                    this.Start();
                }
                else
                {
                    this.Stop();
                }
            }
        }

        /// <summary> Starts this object. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public void Start()
        {
            if ( this.Enabled || this.Interval <= 0L )
            {
                return;
            }

            this._StopTimer = false;
            System.Threading.ThreadStart threadStart = () => this.NotificationTimer( ref this._Interval, ref this._IgnoreEventIfLateBy, ref this._StopTimer );
            this._TimerThread = new System.Threading.Thread( threadStart ) { Priority = System.Threading.ThreadPriority.Highest };
            this._TimerThread.Start();
        }

        /// <summary> True to stop timer. </summary>
        private bool _StopTimer;

        /// <summary> Stops the timer. </summary>
        public void Stop()
        {
            this._StopTimer = true;
        }

        /// <summary> Stops the timer. Waits for the current event to finish. Aborts
        /// the timer if the event does not finish the timeout, </summary>
        /// <param name="timeout"> The timeout. </param>
        public void Stop( TimeSpan timeout )
        {
            if ( !this.StopAndWait( timeout ) )
            {
                this.Abort();
            }
        }

        /// <summary> Stops and wait. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public void StopAndWait()
        {
            _ = this.StopAndWait( System.Threading.Timeout.InfiniteTimeSpan );
        }

        /// <summary> Stops and wait. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="timeout"> The timeout. </param>
        /// <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        public bool StopAndWait( TimeSpan timeout )
        {
            this._StopTimer = true;
            return !this.Enabled || this._TimerThread.ManagedThreadId == System.Threading.Thread.CurrentThread.ManagedThreadId || this._TimerThread.Join( timeout );
        }

        /// <summary> Aborts the timer (aborts the time thread). </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        public void Abort()
        {
            this._StopTimer = true;
            if ( this.Enabled )
            {
                this._TimerThread.Abort();
            }
        }

        /// <summary> Notification timer. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="interval">            [in,out] The interval in micro seconds. </param>
        /// <param name="ignoreEventIfLateBy"> [in,out] Amount to ignore event if late by. </param>
        /// <param name="stopTimer">           [in,out] True to stop timer. </param>
        private void NotificationTimer( ref long interval, ref long ignoreEventIfLateBy, ref bool stopTimer )
        {
            int timerCount = 0;
            long nextNotification = 0L;
            var microStopwatch = new MicroStopwatch();
            microStopwatch.Start();
            while ( !stopTimer )
            {
                long callbackFunctionExecutionTime = microStopwatch.ElapsedMicroseconds - nextNotification;
                long timerIntervalInMicroSecCurrent = System.Threading.Interlocked.Read( ref interval );
                long ignoreEventIfLateByCurrent = System.Threading.Interlocked.Read( ref ignoreEventIfLateBy );
                nextNotification += timerIntervalInMicroSecCurrent;
                timerCount += 1;
                long elapsedMicroseconds = microStopwatch.ElapsedMicroseconds;
                while ( elapsedMicroseconds < nextNotification )
                {
                    System.Threading.Thread.SpinWait( 10 );
                    elapsedMicroseconds = microStopwatch.ElapsedMicroseconds;
                }

                long timerLateBy = elapsedMicroseconds - nextNotification;
                if ( timerLateBy >= ignoreEventIfLateByCurrent )
                {
                    continue;
                }

                var microTimerEventArgs = new MicroTimerEventArgs( timerCount, elapsedMicroseconds, timerLateBy, callbackFunctionExecutionTime );
                MicroTimerElapsed?.Invoke( this, microTimerEventArgs );
            }

            microStopwatch.Stop();
        }

        /// <summary> Event queue for all listeners interested in MicroTimerElapsed events. </summary>
        public event EventHandler<MicroTimerEventArgs> MicroTimerElapsed;
    }

    /// <summary> Additional information for micro timer events. </summary>
    /// <remarks>
    /// (c) 2013 Ken Loveday. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-11-25 </para><para>
    /// https://www.codeproject.com/Articles/98346/Microsecond-and-Millisecond-NET-Timer. </para>
    /// </remarks>
    public class MicroTimerEventArgs : EventArgs
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="timerCount">                    The number of timers. </param>
        /// <param name="elapsedMicroseconds">           The elapsed microseconds. </param>
        /// <param name="timerLateBy">                   Amount to timer late by. </param>
        /// <param name="callbackFunctionExecutionTime"> The callback function execution time. </param>
        public MicroTimerEventArgs( int timerCount, long elapsedMicroseconds, long timerLateBy, long callbackFunctionExecutionTime ) : base()
        {
            this.TimerCount = timerCount;
            this.ElapsedMicroseconds = elapsedMicroseconds;
            this.TimerLateBy = timerLateBy;
            this.CallbackFunctionExecutionTime = callbackFunctionExecutionTime;
        }

        /// <summary>
        /// Gets Simple counter, number times timed event (callback function) executed.
        /// </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The number of timers. </value>
        public int TimerCount { get; private set; }

        /// <summary> Gets the time when timed event was called since timer started. </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> The elapsed microseconds. </value>
        public long ElapsedMicroseconds { get; private set; }

        /// <summary>
        /// Gets the how late the timer was compared to when it should have been called.
        /// </summary>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        /// <value> Amount to timer late by. </value>
        public long TimerLateBy { get; private set; }

        /// <summary>
        /// Gets the time it took to execute previous call to callback function (OnTimedEvent).
        /// </summary>
        /// <value> The callback function execution time. </value>
        public long CallbackFunctionExecutionTime { get; private set; }
    }

    /// <summary> A micro stopwatch. </summary>
    /// <remarks>
    /// (c) 2013 Ken Loveday. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-11-25 </para><para>
    /// https://www.codeproject.com/Articles/98346/Microsecond-and-Millisecond-NET-Timer. </para>
    /// </remarks>
    public class MicroStopwatch : Stopwatch
    {

        /// <summary> The micro security per tick. </summary>
        private readonly double _MicroSecPerTick = 1000000.0d / Frequency;

        /// <summary>
        /// Initializes a new instance of the <see cref="T:System.Diagnostics.Stopwatch" /> class.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
        public MicroStopwatch()
        {
            if ( !IsHighResolution )
            {
                throw new InvalidOperationException( "On this system the high-resolution performance counter is not available" );
            }
        }

        /// <summary> Gets the elapsed microseconds. </summary>
        /// <value> The elapsed microseconds. </value>
        public long ElapsedMicroseconds => ( long ) Conversion.Fix( this.ElapsedTicks * this._MicroSecPerTick );
    }
}
