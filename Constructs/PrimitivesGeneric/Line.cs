using System;

namespace isr.Core.Constructs
{

    /// <summary> Implements a generic line class. </summary>
    /// <remarks>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2006-04-10, 1.1.2291. </para>
    /// </remarks>
    public class Line<T> where T : IComparable<T>, IEquatable<T>, IFormattable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="Line{T}" /> class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        protected Line() : base()
        {
        }

        /// <summary> Initializes a new instance of the <see cref="Line{T}" /> class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="x1"> Specifies the X1 coordinate of the line. </param>
        /// <param name="y1"> Specifies the Y1 coordinate of the line. </param>
        /// <param name="x2"> Specifies the X2 coordinate of the line. </param>
        /// <param name="y2"> Specifies the Y2 coordinate of the line. </param>
        public Line( T x1, T y1, T x2, T y2 ) : this()
        {
            this.SetLineThis( x1, y1, x2, y2 );
        }

        /// <summary> The Copy Constructor. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The  <see cref="Line{T}">Line</see> object from which to Copy. </param>
        public Line( Line<T> model ) : this()
        {
            if ( model is object )
            {
                this.SetLineThis( model.X1, model.Y1, model.X2, model.Y2 );
            }
        }


        #endregion

        #region " EQUALS "

        /// <summary> Compares two lines. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the line to compare to. </param>
        /// <param name="right"> Specifies the line to compare. </param>
        /// <returns> <c>True</c> if the lines are equal. </returns>
        public new static bool Equals( object left, object right )
        {
            return Equals( left as Line<T>, right as Line<T> );
        }

        /// <summary> Compares two lines. </summary>
        /// <remarks> The two lines are the same if they have the same X and Y coordinates. </remarks>
        /// <param name="left">  Specifies the line to compare to. </param>
        /// <param name="right"> Specifies the line to compare. </param>
        /// <returns> <c>True</c> if the lines are equal. </returns>
        public static bool Equals( Line<T> left, Line<T> right )
        {
            return left is null
                ? right is null
                : !(right is null)
&& left.X1.Equals( right.X1 ) && left.X2.Equals( right.X2 ) && left.Y1.Equals( right.Y1 ) && left.Y2.Equals( right.Y2 );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as Line<T> );
        }

        /// <summary> Compares two lines. </summary>
        /// <remarks> The two lines are the same if they have the same X1 and Y1 coordinates. </remarks>
        /// <param name="other"> Specifies the other line. </param>
        /// <returns> <c>True</c> if the lines are equal. </returns>
        public bool Equals( Line<T> other )
        {
            return other is object && other.X1.Equals( this.X1 ) && other.X2.Equals( this.X2 ) && other.Y1.Equals( this.Y1 ) && other.Y2.Equals( this.Y2 );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( Line<T> left, Line<T> right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( Line<T> left, Line<T> right )
        {
            return !ReferenceEquals( left, right ) && (left is null || !left.Equals( right ));
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return this.X1.GetHashCode() ^ this.Y1.GetHashCode() ^ this.X2.GetHashCode() ^ this.Y2.GetHashCode();
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary> Sets the line based on the coordinates. </summary>
        /// <remarks> Use this class to set the line. </remarks>
        /// <param name="x1"> Specifies the X1 coordinate of the line. </param>
        /// <param name="y1"> Specifies the Y1 coordinate of the line. </param>
        /// <param name="x2"> Specifies the X2 coordinate of the line. </param>
        /// <param name="y2"> Specifies the Y2 coordinate of the line. </param>
        private void SetLineThis( T x1, T y1, T x2, T y2 )
        {
            this.X1 = x1;
            this.Y1 = y1;
            this.X2 = x2;
            this.Y2 = y2;
        }

        /// <summary> Sets the line based on the coordinates. </summary>
        /// <remarks> Use this class to set the line. </remarks>
        /// <param name="x1"> Specifies the X1 coordinate of the line. </param>
        /// <param name="y1"> Specifies the Y1 coordinate of the line. </param>
        /// <param name="x2"> Specifies the X2 coordinate of the line. </param>
        /// <param name="y2"> Specifies the Y2 coordinate of the line. </param>
        public void SetLine( T x1, T y1, T x2, T y2 )
        {
            this.SetLineThis( x1, y1, x2, y2 );
        }

        /// <summary> Returns the default string representation of the line. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns>
        /// The formatted string representation of the line, e.g., '[(x1,y1),(x2,y2)]'.
        /// </returns>
        public override string ToString()
        {
            return ToString( this.X1, this.Y1, this.X2, this.Y2 );
        }

        /// <summary> Returns the default string representation of the line. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="x1"> Specifies the X1 coordinate of the line. </param>
        /// <param name="y1"> Specifies the Y1 coordinate of the line. </param>
        /// <param name="x2"> Specifies the X2 coordinate of the line. </param>
        /// <param name="y2"> Specifies the Y2 coordinate of the line. </param>
        /// <returns>
        /// The formatted string representation of the line, e.g., '[(x1,y1),(x2,y2)]'.
        /// </returns>
        private static string ToString( T x1, T y1, T x2, T y2 )
        {
            return $"[({x1},{y1})-({x2},{y2})]";
        }

        /// <summary> Transposes the (x,y) line to a (y,x) line. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="line"> Specifies the <see cref="Line{T}">Line</see> to transpose. </param>
        /// <returns> The transposed (y,x) line. </returns>
        public static Line<T> Transpose( Line<T> line )
        {
            return line is null ? throw new ArgumentNullException( nameof( line ) ) : new Line<T>( line.Y1, line.X1, line.Y2, line.X2 );
        }

        /// <summary> Holds the X1 coordinate of the line. </summary>
        /// <value> The x coordinate 1. </value>
        public T X1 { get; set; }

        /// <summary> Holds the X2 coordinate of the line. </summary>
        /// <value> The x coordinate 2. </value>
        public T X2 { get; set; }

        /// <summary> Holds the Y1 coordinate of the line. </summary>
        /// <value> The y coordinate 1. </value>
        public T Y1 { get; set; }

        /// <summary> Holds the Y2 coordinate of the line. </summary>
        /// <value> The y coordinate 2. </value>
        public T Y2 { get; set; }

        /// <summary> Suspends update of changes. </summary>
        /// <value> The suspend update. </value>
        protected bool SuspendUpdate { get; set; }

        #endregion

    }
}
