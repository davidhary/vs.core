using System;

namespace isr.Core.Constructs
{

    /// <summary> Interface for typed Cloneable. </summary>
    /// <remarks>
    /// (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2020-04-27, . </para>
    /// </remarks>
    public interface ITypedCloneable<T> : ICloneable
    {

        /// <summary> Creates a new instance. </summary>
        /// <returns> The new  instance. </returns>
        T CreateNew();

        /// <summary> Creates the copy. </summary>
        /// <returns> The new copy. </returns>
        T CreateCopy();

        /// <summary> Copies from described by value. </summary>
        /// <param name="value"> The value. </param>
        void CopyFrom( T value );
    }
}
