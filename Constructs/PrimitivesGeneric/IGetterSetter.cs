namespace isr.Core.Constructs
{

    /// <summary> A structure. </summary>
    /// <remarks>
    /// David, 2020-05-29. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para>
    /// </remarks>
    public interface IGetterSetter<TValue> where TValue : struct
    {

        /// <summary> Gets the <typeparamref name="TValue"/> for the given property name. </summary>
        /// <remarks> David, 2020-05-05. </remarks>
        /// <param name="name"> (Optional) Name of the runtime caller
        /// member. </param>
        /// <returns> A Nullable of <typeparamref name="TValue"/>. </returns>
        TValue? Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null );

        /// <summary> Sets the <typeparamref name="TValue"/> for the given property name. </summary>
        /// <remarks> David, 2020-05-05. </remarks>
        /// <param name="value">                                      value. </param>
        /// <param name="name"> (Optional) Name of the runtime caller
        /// member. </param>
        /// <returns> A Nullable of <typeparamref name="TValue"/>. </returns>
        TValue Setter( TValue value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null );
    }

    /// <summary> Interface for getter setter. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public interface IGetterSetter
    {

        /// <summary> Getters the given &lt; runtime. compiler services. caller member name. </summary>
        /// <param name="name"> (Optional) Name of the runtime caller
        /// member. </param>
        /// <returns> A String. </returns>
        string Getter( [System.Runtime.CompilerServices.CallerMemberName()] string name = null );

        /// <summary> Setters. </summary>
        /// <param name="value">                                      Name of the runtime caller member. </param>
        /// <param name="name"> (Optional) Name of the runtime caller
        /// member. </param>
        /// <returns> A String. </returns>
        string Setter( string value, [System.Runtime.CompilerServices.CallerMemberName()] string name = null );
    }
}
