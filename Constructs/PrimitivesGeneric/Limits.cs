using System;

namespace isr.Core.Constructs
{

    /// <summary> Implements a generic limits class. </summary>
    /// <remarks>
    /// (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2014-01-24, 2.0.5137. based on the generic Range. </para>
    /// </remarks>
    public class Limits<T> where T : IComparable<T>, IEquatable<T>, IFormattable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="Limits{T}" /> class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        protected Limits() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Limits{T}" /> class. The copy constructor.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The  <see cref="Limits{T}">Limits</see> object from which to copy. </param>
        public Limits( Limits<T> model ) : base()
        {
            if ( model is object )
            {
                this.SetLimitsThis( model.Min, model.Max );
            }
        }

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="minValue"> Min of Limits. </param>
        /// <param name="maxValue"> Max of Limits. </param>
        public Limits( T minValue, T maxValue ) : base()
        {
            this.SetLimitsThis( minValue, maxValue );
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public static new bool Equals( object left, object right )
        {
            return Equals( left as Limits<T>, right as Limits<T> );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public static bool Equals( Limits<T> left, Limits<T> right )
        {
            return left is null ? right is null : !(right is null) && left.Min.Equals( right.Min ) && left.Max.Equals( right.Max );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as Limits<T> );
        }

        /// <summary> Compares two Limits. </summary>
        /// <remarks> The two Limits are the same if they have the min and end values. </remarks>
        /// <param name="other"> Specifies the other Limits. </param>
        /// <returns> <c>True</c> if the Limits are equal. </returns>
        public bool Equals( Limits<T> other )
        {
            return other is object && Equals( this, other );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Min.GetHashCode() ^ this.Max.GetHashCode();
        }

        #endregion

        #region " OPERATORS "

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( Limits<T> left, Limits<T> right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( Limits<T> left, Limits<T> right )
        {
            return !ReferenceEquals( left, right ) && (left is null || !left.Equals( right ));
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary>
        /// Determines if the <paramref name="value">specified value</paramref> is within Limits.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> Specifies the value which to check as contained within the Limits. </param>
        /// <returns>
        /// <c>True</c> if the <paramref name="value">specified value</paramref>
        /// is within Limits.
        /// </returns>
        public bool Contains( T value )
        {
            return value.CompareTo( this.Min ) >= 0 && value.CompareTo( this.Max ) <= 0;
        }

        /// <summary>
        /// Returns a new Limits from the min of the two minima to the max of the two maxima.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="limitsA"> Specifies <see cref="Limits{T}"/> A. </param>
        /// <param name="limitsB"> Specifies <see cref="Limits{T}"/> B. </param>
        /// <returns> A new Limits from the min of the two minima to the max of the two maxima. </returns>
        public static Limits<T> Extend( Limits<T> limitsA, Limits<T> limitsB )
        {
            return limitsA is null
                ? throw new ArgumentNullException( nameof( limitsA ) )
                : limitsB is null
                ? throw new ArgumentNullException( nameof( limitsB ) )
                : limitsA.Min.CompareTo( limitsB.Min ) > 0
                ? limitsA.Max.CompareTo( limitsB.Max ) < 0 ? new Limits<T>( limitsB.Min, limitsB.Max ) : new Limits<T>( limitsB.Min, limitsA.Max )
                : limitsA.Max.CompareTo( limitsB.Max ) < 0
                    ? new Limits<T>( limitsA.Min, limitsB.Max )
                    : new Limits<T>( limitsA.Min, limitsA.Max );
        }

        /// <summary>
        /// Extends this Limits to include both its present values and the specified Limits.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="limits"> A <see cref="Limits{T}"/> value. </param>
        /// <returns> A new Limits from the min of the two minima to the max of the two maxima. </returns>
        public Limits<T> ExtendBy( Limits<T> limits )
        {
            if ( limits is null )
            {
                throw new ArgumentNullException( nameof( limits ) );
            }

            if ( this.Min.CompareTo( limits.Min ) > 0 )
            {
                this.SetLimits( limits.Min, this.Min );
            }

            if ( this.Max.CompareTo( limits.Max ) < 0 )
            {
                this.SetLimits( this.Min, limits.Max );
            }

            return this;
        }

        /// <summary> Return the Limits of the specified data array. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The data array. </param>
        /// <returns> The limits. </returns>
        public static Limits<T> GetLimits( T[] values )
        {

            // return the unit Limits if no data
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            // initialize the Limits values to the first value
            T temp;
            temp = values[0];
            var min = temp;
            var max = temp;

            // Loop over each point in the arrays
            for ( int i = 0, loopTo = values.Length - 1; i <= loopTo; i++ )
            {
                temp = values[i];
                if ( temp.CompareTo( min ) < 0 )
                {
                    min = temp;
                }
                else if ( temp.CompareTo( max ) > 0 )
                {
                    max = temp;
                }
            }

            return new Limits<T>( min, max );
        }

        /// <summary> Returns the end or maximum value of the Limits. </summary>
        /// <value> The maximum value. </value>
        public T Max { get; private set; }

        /// <summary> Returns the start or minimum value of the Limits. </summary>
        /// <value> The minimum value. </value>
        public T Min { get; private set; }

        /// <summary> Sets the Limits based on the extrema. </summary>
        /// <remarks> Use this class to set the Limits. </remarks>
        /// <param name="minValue"> Specified the minimum value of the Limits. </param>
        /// <param name="maxValue"> Specifies the maximum value of the Limits. </param>
        private void SetLimitsThis( T minValue, T maxValue )
        {
            if ( minValue.CompareTo( maxValue ) <= 0 )
            {
                this.Min = minValue;
                this.Max = maxValue;
            }
            else
            {
                this.Min = maxValue;
                this.Max = minValue;
            }
        }

        /// <summary> Sets the Limits based on the extrema. </summary>
        /// <remarks> Use this class to set the Limits. </remarks>
        /// <param name="minValue"> Specified the minimum value of the Limits. </param>
        /// <param name="maxValue"> Specifies the maximum value of the Limits. </param>
        public void SetLimits( T minValue, T maxValue )
        {
            this.SetLimitsThis( minValue, maxValue );
        }

        /// <summary> Returns the default string representation of the Limits. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The formatted string representation of the Limits, e.g., '(min,max)'. </returns>
        public override string ToString()
        {
            return ToString( this.Min, this.Max );
        }

        /// <summary> Returns the default string representation of the Limits. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="min"> Returns the start or minimum value of the Limits. </param>
        /// <param name="max"> Returns the end or maximum value of the Limits. </param>
        /// <returns> The formatted string representation of the Limits, e.g., '(min,max)'. </returns>
        public static string ToString( T min, T max )
        {
            return $"({min},{max})";
        }

        #endregion

    }
}
