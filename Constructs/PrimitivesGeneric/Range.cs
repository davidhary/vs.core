using System;

namespace isr.Core.Constructs
{

    /// <summary> Implements a generic range class. </summary>
    /// <remarks>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2006-04-10, 1.1.2291. </para>
    /// </remarks>
    public class Range<T> where T : IComparable<T>, IEquatable<T>, IFormattable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="Range{T}" /> class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        protected Range() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Range{T}" /> class. The copy constructor.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The  <see cref="Range{T}">Range</see> object from which to copy. </param>
        public Range( Range<T> model ) : base()
        {
            if ( model is object )
            {
                this.SetRangeThis( model.Min, model.Max );
            }
        }

        /// <summary> Constructs this class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="minValue"> Min of range. </param>
        /// <param name="maxValue"> Max of range. </param>
        public Range( T minValue, T maxValue ) : base()
        {
            this.SetRangeThis( minValue, maxValue );
        }

        #endregion

        #region " EQUALS "

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public new static bool Equals( object left, object right )
        {
            return Equals( left as Range<T>, right as Range<T> );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public static bool Equals( Range<T> left, Range<T> right )
        {
            return left is null ? right is null : !(right is null) && left.Min.Equals( right.Min ) && left.Max.Equals( right.Max );
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as Range<T> );
        }

        /// <summary> Compares two ranges. </summary>
        /// <remarks> The two ranges are the same if they have the min and end values. </remarks>
        /// <param name="other"> Specifies the other range to compare. </param>
        /// <returns> <c>True</c> if the ranges are equal. </returns>
        public bool Equals( Range<T> other )
        {
            return other is object && Equals( this, other );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return this.Min.GetHashCode() ^ this.Max.GetHashCode();
        }

        #endregion

        #region " OPERATORS "

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( Range<T> left, Range<T> right )
        {
            return ReferenceEquals( left, right ) || left is object && left.Equals( right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( Range<T> left, Range<T> right )
        {
            return !ReferenceEquals( left, right ) && (left is null || !left.Equals( right ));
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary>
        /// Determines if the <paramref name="value">specified value</paramref>
        /// is within range.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> Specifies the value which to check as contained within the range. </param>
        /// <returns>
        /// <c>True</c> if the <paramref name="value">specified value</paramref> is within range.
        /// </returns>
        public bool Contains( T value )
        {
            return value.CompareTo( this.Min ) >= 0 && value.CompareTo( this.Max ) <= 0;
        }

        /// <summary>
        /// Returns a new range from the min of the two minima to the max of the two maxima.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="rangeA"> Specifies <see cref="Range{T}"/> A. </param>
        /// <param name="rangeB"> Specifies <see cref="Range{T}"/> B. </param>
        /// <returns> A new Range from the min of the two minima to the max of the two maxima. </returns>
        public static Range<T> Extend( Range<T> rangeA, Range<T> rangeB )
        {
            if ( rangeA is null )
            {
                throw new ArgumentNullException( nameof( rangeA ) );
            }

            return rangeB is null
                ? throw new ArgumentNullException( nameof( rangeB ) )
                : rangeA.Min.CompareTo( rangeB.Min ) > 0
                ? rangeA.Max.CompareTo( rangeB.Max ) < 0 ? new Range<T>( rangeB.Min, rangeB.Max ) : new Range<T>( rangeB.Min, rangeA.Max )
                : rangeA.Max.CompareTo( rangeB.Max ) < 0 ? new Range<T>( rangeA.Min, rangeB.Max ) : new Range<T>( rangeA.Min, rangeA.Max );
        }

        /// <summary>
        /// Extends this range to include both its present values and the specified range.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="range"> A <see cref="Range{T}"/> value. </param>
        /// <returns> A new Limits from the min of the two minima to the max of the two maxima. </returns>
        public Range<T> ExtendBy( Range<T> range )
        {
            if ( range is null )
            {
                throw new ArgumentNullException( nameof( range ) );
            }

            if ( this.Min.CompareTo( range.Min ) > 0 )
            {
                this.SetRange( range.Min, this.Min );
            }

            if ( this.Max.CompareTo( range.Max ) < 0 )
            {
                this.SetRange( this.Min, range.Max );
            }

            return this;
        }

        /// <summary> Return the range of the specified data array. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        /// <param name="values"> The data array. </param>
        /// <returns> The calculated range. </returns>
        public static Range<T> GetRange( T[] values )
        {

            // return the unit range if no data
            if ( values is null )
            {
                throw new ArgumentNullException( nameof( values ) );
            }

            // initialize the range values to the first value
            T temp;
            temp = values[0];
            var min = temp;
            var max = temp;

            // Loop over each point in the arrays
            for ( int i = 0, loopTo = values.Length - 1; i <= loopTo; i++ )
            {
                temp = values[i];
                if ( temp.CompareTo( min ) < 0 )
                {
                    min = temp;
                }
                else if ( temp.CompareTo( max ) > 0 )
                {
                    max = temp;
                }
            }

            return new Range<T>( min, max );
        }

        /// <summary> Returns the end or maximum value of the range. </summary>
        /// <value> The maximum value. </value>
        public T Max { get; private set; }

        /// <summary> Returns the start or minimum value of the range. </summary>
        /// <value> The minimum value. </value>
        public T Min { get; private set; }

        /// <summary> Sets the range based on the extrema. </summary>
        /// <remarks> Use this class to set the range. </remarks>
        /// <param name="minValue"> Specified the minimum value of the range. </param>
        /// <param name="maxValue"> Specifies the maximum value of the range. </param>
        private void SetRangeThis( T minValue, T maxValue )
        {
            if ( minValue.CompareTo( maxValue ) <= 0 )
            {
                this.Min = minValue;
                this.Max = maxValue;
            }
            else
            {
                this.Min = maxValue;
                this.Max = minValue;
            }
        }

        /// <summary> Sets the range based on the extrema. </summary>
        /// <remarks> Use this class to set the range. </remarks>
        /// <param name="minValue"> Specified the minimum value of the range. </param>
        /// <param name="maxValue"> Specifies the maximum value of the range. </param>
        public void SetRange( T minValue, T maxValue )
        {
            this.SetRangeThis( minValue, maxValue );
        }

        /// <summary> Returns the default string representation of the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The formatted string representation of the range, e.g., '(min,max)'. </returns>
        public override string ToString()
        {
            return ToString( this.Min, this.Max );
        }

        /// <summary> Returns the default string representation of the range. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="min"> Returns the start or minimum value of the range. </param>
        /// <param name="max"> Returns the end or maximum value of the range. </param>
        /// <returns> The formatted string representation of the range, e.g., '(min,max)'. </returns>
        public static string ToString( T min, T max )
        {
            return $"({min},{max})";
        }

        #endregion

    }
}
