using System;

namespace isr.Core.Constructs
{

    /// <summary> Implements a generic point class. </summary>
    /// <remarks>
    /// (c) 2006 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2006-04-10, 1.1.2291. </para>
    /// </remarks>
    public class Point<T> where T : IComparable<T>, IEquatable<T>, IFormattable
    {

        #region " CONSTRUCTION and CLEANUP "

        /// <summary> Initializes a new instance of the <see cref="Point{T}" /> class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        protected Point() : base()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Point{T}" /> class. The copy constructor.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="model"> The  <see cref="Point{T}">Point</see> object from which to copy. </param>
        public Point( Point<T> model ) : this()
        {
            if ( model is object )
            {
                this.SetPointThis( model.X, model.Y );
            }
        }

        /// <summary> Initializes a new instance of the <see cref="Point{T}" /> class. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="x"> Specifies the X coordinate of the point. </param>
        /// <param name="y"> Specifies the Y coordinate of the point. </param>
        public Point( T x, T y ) : this()
        {
            this.SetPointThis( x, y );
        }

        #endregion

        #region " SHARED "

        /// <summary> Compares two points. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the point to compare. </param>
        /// <param name="right"> Specifies the point to compare with. </param>
        /// <returns> <c>True</c> if the points are equal. </returns>
        public new static bool Equals( object left, object right )
        {
            return Equals( left as Point<T>, right as Point<T> );
        }

        /// <summary> Compares two points. </summary>
        /// <remarks> The two points are the same if they have the same X and Y coordinates. </remarks>
        /// <param name="left">  Specifies the point to compare. </param>
        /// <param name="right"> Specifies the point to compare with. </param>
        /// <returns> <c>True</c> if the points are equal. </returns>
        public static bool Equals( Point<T> left, Point<T> right )
        {
            return left is null ? right is null : !(right is null) && left.X.Equals( right.X ) && left.Y.Equals( right.Y );
        }

        /// <summary> Implements the operator =. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator ==( Point<T> left, Point<T> right )
        {
            return Equals( left, right );
        }

        /// <summary> Implements the operator &lt;&gt;. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="left">  Specifies the left hand side argument of the binary operation. </param>
        /// <param name="right"> Specifies the right hand side argument of the binary operation. </param>
        /// <returns> The result of the operation. </returns>
        public static bool operator !=( Point<T> left, Point<T> right )
        {
            return !ReferenceEquals( left, right ) && (left is null || !left.Equals( right ));
        }

        /// <summary>
        /// Determines whether the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="obj"> The <see cref="T:System.Object" /> to compare with the current
        /// <see cref="T:System.Object" />. </param>
        /// <returns>
        /// <c>True</c> if the specified <see cref="T:System.Object" /> is equal to the current
        /// <see cref="T:System.Object" />; otherwise, false.
        /// </returns>
        public override bool Equals( object obj )
        {
            return this.Equals( obj as Point<T> );
        }

        /// <summary> Compares two points. </summary>
        /// <remarks> The two points are the same if they have the same X and Y coordinates. </remarks>
        /// <param name="other"> Specifies the other point to compare. </param>
        /// <returns> <c>True</c> if the points are equal. </returns>
        public bool Equals( Point<T> other )
        {
            return other is object && Equals( this, other );
        }

        /// <summary> Creates a unique hash code. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> An <see cref="T:System.Int32">Int32</see> value. </returns>
        public override int GetHashCode()
        {
            return this.X.GetHashCode() ^ this.Y.GetHashCode();
        }

        #endregion

        #region " METHODS AND PROPERTIES "

        /// <summary> Holds the Y coordinate of the point. </summary>
        /// <value> The y coordinate. </value>
        public T Y { get; set; }

        /// <summary> Holds the X coordinate of the point. </summary>
        /// <value> The x coordinate. </value>
        public T X { get; set; }

        /// <summary> Sets the point based on the coordinates. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="x"> The x. </param>
        /// <param name="y"> The y. </param>
        private void SetPointThis( T x, T y )
        {
            if ( x.CompareTo( this.X ) != 0 )
            {
                this.X = x;
            }

            if ( y.CompareTo( this.Y ) != 0 )
            {
                this.Y = y;
            }
        }

        /// <summary> Sets the point based on the coordinates. </summary>
        /// <remarks> Use this class to set the point. </remarks>
        /// <param name="x"> Specifies the X coordinate of the point. </param>
        /// <param name="y"> Specifies the Y coordinate of the point. </param>
        public void SetPoint( T x, T y )
        {
            this.X = x;
            this.Y = y;
        }

        /// <summary> Returns the default string representation of the point. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <returns> The formatted string representation of the point, e.g., '(x,y)'. </returns>
        public override string ToString()
        {
            return ToString( this.X, this.Y );
        }

        /// <summary> Returns the default string representation of the point. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="x"> Specifies the X coordinate of the point. </param>
        /// <param name="y"> Specifies the Y coordinate of the point. </param>
        /// <returns> The formatted string representation of the point, e.g., '(x,y)'. </returns>
        public static string ToString( T x, T y )
        {
            return $"({x},{y})";
        }

        #endregion

    }
}
