using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;

namespace isr.Core.Constructs
{

    /// <summary> A Sortable Binding List. </summary>
    /// <remarks>
    /// Sample code for putting data into a data grid view, which is not sortable by columns when
    /// bound with Linq:
    /// <code>
    /// Dim d = From device In Program.ue.devices Order By dev.device_id Select device
    /// Dim list As New SortableBindingList(Of device)(device.ToList())
    /// dataGridViewDeviceList.DataSource = list
    /// </code> <para>
    /// (c) 2008 Muigai Mwaura and 2013 Physlcu$ All rights reserved.</para><para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-05-13. Source:
    /// https://www.codeproject.com/Articles/31418/Implementing-a-Sortable-BindingList-Very-Very-Quic.
    /// </para>
    /// </remarks>
    public class SortableBindingList<T> : BindingList<T>
    {

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="items"> Items with which to populate the list. </param>
        public SortableBindingList( IList<T> items ) : base( items )
        {
        }

        /// <summary> Constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="items"> Items with which to populate the list. </param>
        public SortableBindingList( IEnumerable<T> items ) : this( items.ToList() )
        {
        }

        /// <summary> Default constructor. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        public SortableBindingList()
        {
        }

        /// <summary> Loads the items into the binding list and resets its binding. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="items"> Items with which to populate the list. </param>
        /// <returns> A sortable binding list. </returns>
        public SortableBindingList<T> Load( IList<T> items )
        {
            if ( items is object && items.Any() )
            {
                this.ResetItems( items );
            }

            return this;
        }

        /// <summary> Loads the items into the binding list and resets its binding. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="items"> Items with which to populate the list. </param>
        /// <returns> A sortable binding list. </returns>
        public SortableBindingList<T> Load( IEnumerable<T> items )
        {
            if ( items is object && items.Any() )
            {
                this.ResetItems( items );
            }

            return this;
        }

        /// <summary> Loads the items into the binding list and resets its binding. </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="items"> The items with which to populate the binding list. </param>
        private void ResetItems( IList<T> items )
        {
            if ( items is object && items.Any() )
            {
                this.RaiseListChangedEvents = false;
                var tempList = items.ToList();
                this.ClearItems();
                foreach ( T item in tempList )
                {
                    this.Add( item );
                }

                this.RaiseListChangedEvents = true;
                this.ResetBindings();
            }
        }

        /// <summary> Loads the items into the binding list and resets its binding. </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="items"> Items with which to populate the list. </param>
        private void ResetItems( IEnumerable<T> items )
        {
            this.ResetItems( items.ToList() );
        }

        /// <summary>
        /// Adds a range of items to the list while suspending the list changed events while the items
        /// are added.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="items"> Items to add to the binding list. </param>
        public void AddRange( IList<T> items )
        {
            if ( items is object && items.Any() )
            {
                this.RaiseListChangedEvents = false;
                foreach ( T item in items )
                {
                    this.Add( item );
                }

                this.RaiseListChangedEvents = true;
                this.ResetBindings();
            }
        }

        /// <summary>
        /// Adds a range of items to the list while suspending the list changed events while the items
        /// are added.
        /// </summary>
        /// <remarks> David, 2020-09-05. </remarks>
        /// <param name="items"> Items with which to populate the list. </param>
        public void AddRange( IEnumerable<T> items )
        {
            this.AddRange( items.ToList() );
        }

        /// <summary> Gets a value indicating whether the list supports sorting. </summary>
        /// <value>
        /// <see langword="true" /> if the list supports sorting; otherwise, <see langword="false" />.
        /// The default is <see langword="false" />.
        /// </value>
        protected override bool SupportsSortingCore => true;

        /// <summary> True if is sorted, false if not. </summary>
        private bool _IsSorted;

        /// <summary> Gets a value indicating whether the list is sorted. </summary>
        /// <value>
        /// <see langword="true" /> if the list is sorted; otherwise, <see langword="false" />. The
        /// default is <see langword="false" />.
        /// </value>
        protected override bool IsSortedCore => this._IsSorted;

        /// <summary> The sort direction. </summary>
        private ListSortDirection _SortDirection;

        /// <summary> Gets the direction the list is sorted. </summary>
        /// <value>
        /// One of the <see cref="T:System.ComponentModel.ListSortDirection" /> values. The default is
        /// <see cref="F:System.ComponentModel.ListSortDirection.Ascending" />.
        /// </value>
        protected override ListSortDirection SortDirectionCore => this._SortDirection;

        /// <summary> The sort property. </summary>
        private PropertyDescriptor _SortProperty;

        /// <summary>
        /// Gets the property descriptor that is used for sorting the list if sorting is implemented in a
        /// derived class; otherwise, returns <see langword="null" />.
        /// </summary>
        /// <value>
        /// The <see cref="T:System.ComponentModel.PropertyDescriptor" /> used for sorting the list.
        /// </value>
        protected override PropertyDescriptor SortPropertyCore => this._SortProperty;

        // <summary> The property descriptors. </summary>
        // private read only PropertyDescriptorCollection _PropertyDescriptors = TypeDescriptor.GetProperties(typeof(T));

        /// <summary>
        /// Sorts the items if overridden in a derived class; otherwise, throws a
        /// <see cref="T:System.NotSupportedException" />.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="prop">      A <see cref="T:System.ComponentModel.PropertyDescriptor" /> that
        /// specifies the property to sort on. </param>
        /// <param name="direction"> One of the <see cref="T:System.ComponentModel.ListSortDirection" />
        /// values. </param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Style", "IDE0039:Use local function", Justification = "<Pending>" )]
        protected override void ApplySortCore( PropertyDescriptor prop, ListSortDirection direction )
        {
            if ( prop is null )
            {
                base.ApplySortCore( prop, direction );
            }
            else
            {
                this._IsSorted = true;
                this._SortDirection = direction;
                this._SortProperty = prop;
                Func<T, object> predicate = n => n.GetType().GetProperty( prop.Name ).GetValue( n, null );
                if ( this.Items.Count > 10000 )
                {
                    this.ResetItems( this._SortDirection == ListSortDirection.Ascending ? this.Items.AsParallel().OrderBy( predicate ) : this.Items.AsParallel().OrderByDescending( predicate ) );
                }
                else
                {
                    this.ResetItems( this._SortDirection == ListSortDirection.Ascending ? this.Items.OrderBy( predicate ) : this.Items.OrderByDescending( predicate ) );
                }
            }
        }

        /// <summary>
        /// Removes any sort applied with
        /// <see cref="M:System.ComponentModel.BindingList`1.ApplySortCore(System.ComponentModel.PropertyDescriptor,System.ComponentModel.ListSortDirection)" />
        /// if sorting is implemented in a derived class; otherwise, raises
        /// <see cref="T:System.NotSupportedException" />.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        protected override void RemoveSortCore()
        {
            this._IsSorted = false;
            this._SortDirection = base.SortDirectionCore;
            this._SortProperty = base.SortPropertyCore;
            this.ResetBindings();
        }
    }

    /// <summary> A Simple Sortable Binding List. </summary>
    /// <remarks>
    /// (c) 2011 Kill Me Please. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2019-05-13 Source:  </para><para>
    /// https://www.codeproject.com/Articles/31418/Implementing-a-Sortable-BindingList-Very-Very-Quic.
    /// Https://www.codeproject.com/script/Membership/View.aspx?mid=4832730. </para>
    /// </remarks>
    public class SimpleSortableBindingList<T> : BindingList<T>
    {

        /// <summary> Gets a value indicating whether the list supports sorting. </summary>
        /// <value>
        /// <see langword="true" /> if the list supports sorting; otherwise, <see langword="false" />.
        /// The default is <see langword="false" />.
        /// </value>
        protected override bool SupportsSortingCore => true;

        /// <summary>
        /// Sorts the items if overridden in a derived class; otherwise, throws a
        /// <see cref="T:System.NotSupportedException" />.
        /// </summary>
        /// <remarks> David, 2020-09-17. </remarks>
        /// <param name="prop">      A <see cref="T:System.ComponentModel.PropertyDescriptor" /> that
        /// specifies the property to sort on. </param>
        /// <param name="direction"> One of the <see cref="T:System.ComponentModel.ListSortDirection" />
        /// values. </param>
        protected override void ApplySortCore( PropertyDescriptor prop, ListSortDirection direction )
        {
            if ( prop is null )
            {
                base.ApplySortCore( prop, direction );
            }
            else
            {
                int modifier = direction == ListSortDirection.Ascending ? 1 : -1;
                if ( prop.PropertyType.GetInterface( "IComparable" ) is object )
                {
                    var items = this.Items.ToList();
                    items.Sort( new Comparison<T>( ( a, b ) => {
                        IComparable aVal = prop.GetValue( a ) as IComparable;
                        IComparable bVal = prop.GetValue( b ) as IComparable;
                        return aVal.CompareTo( bVal ) * modifier;
                    } ) );
                    this.Items.Clear();
                    foreach ( T i in items )
                    {
                        this.Items.Add( i );
                    }
                }
            }
        }
    }
}
