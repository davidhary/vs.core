using System;
using System.Reflection;

[assembly: AssemblyTitle( isr.Core.Constructs.My.MyLibrary.AssemblyTitle )]
[assembly: AssemblyDescription( isr.Core.Constructs.My.MyLibrary.AssemblyDescription )]
[assembly: AssemblyProduct( isr.Core.Constructs.My.MyLibrary.AssemblyProduct )]
[assembly: CLSCompliant( true )]
[assembly: System.Runtime.InteropServices.ComVisible( false )]

[assembly: System.Runtime.CompilerServices.InternalsVisibleTo( isr.Core.Constructs.My.MyLibrary.TestAssemblyStrongName )]
