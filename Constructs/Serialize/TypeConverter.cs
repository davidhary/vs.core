using System;
using System.Runtime.Serialization;

using Microsoft.VisualBasic.CompilerServices;

namespace isr.Core.Constructs
{

    /// <summary> Provides type conversion methods. </summary>
    /// <remarks>
    /// (c) 2004 Integrated Scientific Resources, Inc. All rights reserved.<para>
    /// Licensed under The MIT License.</para><para>
    /// David, 2004-02-28, 1.0.1519 </para>
    /// </remarks>
    public sealed class TypeConverter
    {

        /// <summary> Prevents instantiation of this type, which defines only static members. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        private TypeConverter()
        {
        }

        #region " SHARED "

        /// <summary>Code page to use for serializing</summary>
        private static readonly System.Text.Encoding EncodingCodePage = System.Text.Encoding.UTF8;

        /// <summary> Returns a String value from the value object type. </summary>
        /// <remarks> Use This method to get a string value from the input data. </remarks>
        /// <param name="dataValue">  is an object expression that specifies the value which is to be
        /// converted. </param>
        /// <param name="dataFormat"> A <see cref="T:System.String">String</see> expression that specifies
        /// the format of the converted value. </param>
        /// <param name="dataType">   A <see cref="T:System.String">String</see> expression that specifies
        /// the format of the converted value. </param>
        /// <returns>
        /// The method returns a string value representing the data value to be converted.
        /// </returns>
        /// <seealso cref="Serialize"/>
        public static string ConvertToString( object dataValue, string dataFormat, Type dataType )
        {
            if ( dataType is null )
            {
                return string.Empty;
            }

            if ( dataValue is null )
            {
                return string.Empty;
            }

            switch ( Type.GetTypeCode( dataType ) )
            {
                case TypeCode.Empty:
                case TypeCode.DBNull:
                    {
                        return string.Empty;
                    }

                case TypeCode.DateTime:
                    {
                        DateTimeOffset value = ( DateTimeOffset ) dataValue;
                        return string.IsNullOrWhiteSpace( dataFormat ) ? value.ToString( System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat ) : value.ToString( dataFormat, System.Globalization.CultureInfo.CurrentCulture.DateTimeFormat );
                    }

                case TypeCode.Decimal:
                    {
                        decimal value = Conversions.ToDecimal( dataValue );
                        return string.IsNullOrWhiteSpace( dataFormat ) ? value.ToString( System.Globalization.CultureInfo.CurrentCulture.NumberFormat ) : value.ToString( dataFormat, System.Globalization.CultureInfo.CurrentCulture.NumberFormat );
                    }

                case TypeCode.Double:
                case TypeCode.Single:
                    {
                        double value = Conversions.ToDouble( dataValue );
                        return string.IsNullOrWhiteSpace( dataFormat ) ? value.ToString( System.Globalization.CultureInfo.CurrentCulture.NumberFormat ) : value.ToString( dataFormat, System.Globalization.CultureInfo.CurrentCulture.NumberFormat );
                    }

                default:
                    {

                        // use simple conversion otherwise
                        return dataValue.ToString();
                    }
            }
        }

        /// <summary>   Returns a value object from the object and object type. </summary>
        /// <remarks>
        /// Use This method to get a numeric value from a string expression. This routine is not really
        /// an integral part of the Settings Class but rather necessary to allow us to show how different
        /// variable types can be used. <seealso cref="TypeConverter.Deserialize(string, object)"/> is a
        /// preferred method.
        /// </remarks>
        /// <param name="value">    is an Object expression that specifies the value which is to be
        ///                         converted. </param>
        /// <param name="dataType"> A Type expression that specifies the system data type to create from
        ///                         the input string. </param>
        /// <returns>
        /// Returns an Object value which value is defined by the string and its type is determined by
        /// the data type argument.
        /// </returns>
        public static object ConvertToType( object value, Type dataType )
        {

            // Return a value per the given data type
            switch ( Type.GetTypeCode( dataType ) )
            {
                case TypeCode.Empty:
                case TypeCode.DBNull:
                    {
                        return value;
                    }

                case TypeCode.Boolean:
                    {
                        return Conversions.ToBoolean( value );
                    }

                case TypeCode.Byte:
                    {
                        return Conversions.ToByte( value );
                    }

                case TypeCode.Char:
                    {
                        return Conversions.ToChar( value );
                    }

                case TypeCode.DateTime:
                    {
                        return ( DateTimeOffset ) value;
                    }

                case TypeCode.Double:
                    {
                        return Conversions.ToDouble( value );
                    }

                case TypeCode.Int16:
                    {
                        return Conversions.ToShort( value );
                    }

                case TypeCode.Int32:
                    {
                        return Conversions.ToInteger( value );
                    }

                case TypeCode.Int64:
                    {
                        return Conversions.ToLong( value );
                    }

                case TypeCode.Object:
                    {
                        return value;
                    }

                case TypeCode.Single:
                    {
                        return Conversions.ToSingle( value );
                    }

                case TypeCode.String:
                    {
                        return value;
                    }

                default:
                    {
                        return value;
                    }
            }
        }

        /// <summary> Returns a value object from the string and object type. </summary>
        /// <remarks>
        /// Use This method to get a numeric value from a string expression. This routine is not really
        /// an integral part of the Settings Class but rather necessary to allow us to show how different
        /// variable types can be used. <seealso cref="TypeConverter.Deserialize(string, object)"/>is a preferred method.
        /// </remarks>
        /// <param name="value">    A <see cref="T:System.String">String</see> expression that specifies
        /// the value which is to be converted. </param>
        /// <param name="dataType"> A Type expression that specifies the system data type to create from
        /// the input string. </param>
        /// <returns>
        /// Returns an Object value which value is defined by the string and its type is determined by
        /// the data type argument.
        /// </returns>
        public static object ConvertToType( string value, Type dataType )
        {

            // Return a value per the given data type
            switch ( Type.GetTypeCode( dataType ) )
            {
                case TypeCode.Empty:
                case TypeCode.DBNull:
                    {
                        return value;
                    }

                case TypeCode.Boolean:
                    {
                        return Conversions.ToBoolean( value );
                    }

                case TypeCode.Byte:
                    {
                        return Conversions.ToByte( value );
                    }

                case TypeCode.Char:
                    {
                        return Conversions.ToChar( value );
                    }

                case TypeCode.DateTime:
                    {
                        return DateTime.Parse( value );
                    }

                case TypeCode.Double:
                    {
                        return Conversions.ToDouble( value );
                    }

                case TypeCode.Int16:
                    {
                        return Conversions.ToShort( value );
                    }

                case TypeCode.Int32:
                    {
                        return Conversions.ToInteger( value );
                    }

                case TypeCode.Int64:
                    {
                        return Conversions.ToLong( value );
                    }

                case TypeCode.Object:
                    {
                        return value;
                    }

                case TypeCode.Single:
                    {
                        return Conversions.ToSingle( value );
                    }

                case TypeCode.String:
                    {
                        return value;
                    }

                default:
                    {
                        return value;
                    }
            }
        }

        /// <summary> Converts the string to an object. </summary>
        /// <remarks>
        /// This method uses the type of the <paramref name="defaultValue">default value</paramref> to
        /// set the returned value type.
        /// </remarks>
        /// <exception cref="SerializationException"> Thrown when a Serialization error condition occurs;
        /// i.e., unable to serialize the value. </exception>
        /// <param name="value">        A <see cref="T:System.String">String</see> serialized value. </param>
        /// <param name="defaultValue"> Is an Object expression that specifies the default value. </param>
        /// <returns> Returns an Object value. </returns>
        /// <example>
        /// This example shows how to handle data conversions to and from strings.
        /// <code>
        /// Sub Form_Click
        /// Try
        /// ' Convert a string to a given type
        /// Dim defaultValue as Int32 = 0
        /// Dim value As String = "10"
        /// Dim outcome as Int32 = Conversions.Deserialize(value, defaultValue)
        /// Dim outcome2 as Int32 = Conversions.Deserialize(value,System.Type.GetType("System.Int32"))
        /// Catch e As System.Exception
        /// ' respond to any file name errors.
        /// system.Windows.Forms.MessageBox e.toString
        /// End Try
        /// End Sub  </code>
        /// To run This example, paste the code fragment into a WinForm class.
        /// Run the program by pressing F5, and then click on the form
        /// </example>
        public static object Deserialize( string value, object defaultValue )
        {
            if ( defaultValue is null )
            {
                return value;
            }
            else if ( defaultValue is DateTime || defaultValue is DateTimeOffset )
            {
                return DeserializeDate( value );
            }
            else if ( defaultValue is IConvertible )
            {
                if ( defaultValue.GetType().IsEnum )
                {
                    // handle enumeration separately
                    return Enum.Parse( defaultValue.GetType(), value );
                }
                else
                {
                    // all other convertible (primitive) types
                    return Convert.ChangeType( value, defaultValue.GetType(), System.Globalization.CultureInfo.CurrentCulture );
                }
            }
            else if ( defaultValue is ISerializable || defaultValue is ValueType || defaultValue is Array || defaultValue is null )
            {

                // Serializable object, structures, and arrays handled here.
                // Also assume Nothing resolves to a serialized "thing".
                return DeserializeOther( value, defaultValue );
            }
            else
            {
                // Here the developer is requesting data that CAN'T
                // be serialized or deserialized.  That's a coding problem!
                throw new SerializationException( $"Unable to deserialize value of type '{defaultValue.GetType()}'." );
            }
        }

        /// <summary> Converts the string to an object. </summary>
        /// <remarks>
        /// This method uses the type of the <paramref name="type">type</paramref> to set the returned
        /// value type.
        /// </remarks>
        /// <exception cref="SerializationException"> Thrown when a Serialization error condition occurs. </exception>
        /// <param name="value"> A <see cref="T:System.String">String</see> serialized value. </param>
        /// <param name="type">  Is an Object expression that specifies the default value. </param>
        /// <returns> Returns an Object value. </returns>
        public static object Deserialize( string value, Type type )
        {
            if ( type is null )
            {
                return null;
            }
            else if ( ReferenceEquals( type, typeof( DateTime ) ) || ReferenceEquals( type, typeof( DateTimeOffset ) ) )
            {
                return DeserializeDate( value );
            }
            else if ( type.IsEnum ) // GetType(I Convertible) Then
            {

                // handle enumeration separately
                return Enum.Parse( type, value );
            }
            else if ( type.IsPrimitive || type.Equals( typeof( decimal ) ) )
            {

                // all other convertible (primitive) types
                return Convert.ChangeType( value, type, System.Globalization.CultureInfo.CurrentCulture );
            }
            else if ( type.Equals( typeof( string ) ) )
            {
                return value;
            }
            else if ( type.IsSerializable || type.IsValueType || type.IsArray )
            {

                // Serializable object, structures, and arrays handled here.
                // Also assume Nothing resolves to a serialized "thing".
                return DeserializeOther( value, null );
            }
            else
            {
                // Here the developer is requesting data that CAN'T
                // be serialized or deserialized.  That's a coding problem!
                throw new SerializationException( $"Unable to deserialize value of type '{type.Name}'." );
            }
        }


        /// <summary> Deserialize structures and arrays handled here. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value">        A <see cref="T:System.String">String</see> serialized value. </param>
        /// <param name="defaultValue"> Is an Object expression that specifies the default value. </param>
        /// <returns> Returns an Object value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        private static object DeserializeOther( string value, object defaultValue )
        {
            if ( string.IsNullOrWhiteSpace( value ) )
            {
                return defaultValue;
            }

            try
            {
                // Serializable object, structures, and arrays handled here.
                // Also assume Nothing resolves to a serialized "thing".
                // Restore value from stream:
                var formatter = new System.Runtime.Serialization.Formatters.Soap.SoapFormatter();
                // Dim formatter As New System.Runtime.Serialization.Formatters.Binary.BinaryFormatter
                using var stream = new System.IO.MemoryStream( EncodingCodePage.GetBytes( value ) );
                return formatter.Deserialize( stream );
            }
            catch ( SerializationException )
            {
                // Can't deserialize to target type.
                // Probably because type def has been changed.
                // I could throw an error here, but I'd rather just
                // ignore the saved data and continue.
                return defaultValue;
            }
        }

        /// <summary> Deserializes a date value. </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        /// <param name="value"> A <see cref="T:System.String">String</see> expression that specifies the
        /// value which is to be converted. </param>
        /// <returns> Returns the date value. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "Exception is published" )]
        private static DateTimeOffset DeserializeDate( string value )
        {

            // use this to convert from tick count.
            if ( double.TryParse( value, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.CurrentCulture, out _ ) )
            {
                return new DateTime( Convert.ToInt64( value, System.Globalization.CultureInfo.CurrentCulture ) );
            }
            else
            {
                try
                {
                    // With This we can convert normal dates
                    return DateTimeOffset.Parse( value, System.Globalization.CultureInfo.CurrentCulture );
                }
                catch
                {
                    // With This we can convert normal dates
                    return DateTimeOffset.Parse( value );
                }
            }
        }

        /// <summary> Checks if an object is Value Type object. </summary>
        /// <remarks> Use This method to check if an object is Value type. </remarks>
        /// <param name="thisItem"> specifies an instance of an object. </param>
        /// <returns> <c>True</c> if value type; Otherwise, <c>False</c>. </returns>
        public static bool IsValueType( object thisItem )
        {
            return thisItem is ValueType;
        }


        /// <summary> Converts an object value to string. </summary>
        /// <remarks>
        /// Use This method to convert the object value to string for storing as an application settings.
        /// Note that date time types are saved as ticks to preserve partial second accuracy. Objects
        /// that are not Convertible are serialized.
        /// </remarks>
        /// <exception cref="SerializationException"> Thrown when a Serialization error condition occurs. </exception>
        /// <param name="value"> Is an Object expression that specifies the value to convert. </param>
        /// <returns> System.String. </returns>
        [System.Diagnostics.CodeAnalysis.SuppressMessage( "Design", "CA1031:Do not catch general exception types", Justification = "<Pending>" )]
        public static string Serialize( object value )
        {
            if ( value is null )
            {
                return string.Empty;
            }
            else if ( value is DateTimeOffset offset )
            {

                // serialize into ISO 8601 format
                return offset.ToString( "o" );
            }
            else if ( value is DateTime )
            {
                try
                {

                    // use ticks; convert only returns seconds.
                    return Conversions.ToDate( value ).Ticks.ToString( System.Globalization.CultureInfo.CurrentCulture );
                }
                catch ( InvalidCastException )
                {

                    // convert primitive to string representation.
                    return Conversions.ToString( Convert.ChangeType( value, TypeCode.String, System.Globalization.CultureInfo.CurrentCulture ) );
                }
            }
            else if ( value is IConvertible )
            {

                // convert primitive or enumeration to string representation.
                return Conversions.ToString( Convert.ChangeType( value, TypeCode.String, System.Globalization.CultureInfo.CurrentCulture ) );
            }
            else if ( value is ISerializable || value is ValueType || value is Array )
            {

                // Serializable object, structure, or array serialize object's data into string
                var formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                try
                {
                    using var stream = new System.IO.MemoryStream();
                    formatter.Serialize( stream, value );
                    return EncodingCodePage.GetString( stream.GetBuffer() );
                }
                catch ( SerializationException ex )
                {
                    // probably an array of unsupported (not serializable) objects.
                    throw new SerializationException( $"Unable to serialize value of type '{value.GetType()}'.", ex );
                }
            }
            else
            {

                // Can't serialize: programmer error!
                throw new SerializationException( $"Value of type '{value.GetType()}' was unhandled." );
            }
        }

        #endregion

    }
}
