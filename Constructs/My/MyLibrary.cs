
namespace isr.Core.Constructs.My
{

    /// <summary> Provides assembly information for the class library. </summary>
    /// <remarks> David, 2020-09-22. </remarks>
    public sealed partial class MyLibrary
    {

        /// <summary>
        /// Constructor that prevents a default instance of this class from being created.
        /// </summary>
        /// <remarks> David, 2020-09-22. </remarks>
        private MyLibrary() : base()
        {
        }

        /// <summary> Identifier for the trace event. </summary>
        public const int TraceEventId = ( int ) ProjectTraceEventId.Constructs;

        /// <summary> The assembly title. </summary>
        public const string AssemblyTitle = "Core Constructs Library";

        /// <summary> Information describing the assembly. </summary>
        public const string AssemblyDescription = "Core Constructs Library";

        /// <summary> The assembly product. </summary>
        public const string AssemblyProduct = "Core.Constructs";

        /// <summary> The Strong Name of the test assembly. </summary>
        public const string TestAssemblyStrongName = "isr.Core.ConstructsTests,PublicKey=" + isr.Core.My.SolutionInfo.PublicKey;

    }
}

