Imports System.ComponentModel
Imports System.Threading.Tasks

''' <summary> Encapsulates action and awaiting tasks. </summary>
''' <remarks>
''' David, 2020-07-17. (c) 2020 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class Tasker(Of TResult)
    Implements IDisposable

#Region " CONSTRUCTION ADN CLEANUP "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    Public Sub New()
        MyBase.New
    End Sub
    ''' <summary> True to disposed value. </summary>
    Private _DisposedValue As Boolean

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Services.Tasker and optionally releases
    ''' the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(disposing As Boolean)
        If Not Me._DisposedValue Then
            If disposing Then
                Me.ActionTask?.Dispose()
                Me.AsyncTask?.Dispose()
            End If
            Me._DisposedValue = True
        End If
    End Sub

    ''' <summary>
    ''' Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged
    ''' resources.
    ''' </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(disposing:=True)
        GC.SuppressFinalize(Me)
    End Sub

#End Region

    ''' <summary> Event queue for all listeners interested in AsyncCompleted events. </summary>
    Public Event AsyncCompleted As AsyncCompletedEventHandler

    ''' <summary> Gets or sets the asynchronous task. </summary>
    ''' <remarks> This property receives the return value from the task onsetting function. </remarks>
    ''' <value> The asynchronous task. </value>
    Private ReadOnly Property AsyncTask As Task(Of TResult)

    ''' <summary> Gets or sets the action task. </summary>
    ''' <remarks> This task can be monitored for status and can be awaited. </remarks>
    ''' <value> The action task. </value>
    Public ReadOnly Property ActionTask As Task(Of TResult)

    ''' <summary> Query if 'status' is task ended. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="status"> The status. </param>
    ''' <returns> <c>true</c> if task ended; otherwise <c>false</c> </returns>
    Private Shared Function IsTaskEnded(ByVal status As TaskStatus) As Boolean
        Return (status = TaskStatus.RanToCompletion OrElse status = TaskStatus.Canceled OrElse status = TaskStatus.Faulted)
    End Function

    ''' <summary> Query if this object is busy. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> <c>true</c> if busy; otherwise <c>false</c> </returns>
    Public Function IsBusy() As Boolean
        Return Not Me._DisposedValue AndAlso Me.ActionTask IsNot Nothing AndAlso Not Tasker(Of Task(Of TResult)).IsTaskEnded(Me.ActionTask.Status)
    End Function

    ''' <summary> Starts action asynchronous. </summary>
    ''' <remarks>
    ''' David, 2020-07-17. A continuation task is used to invoke the <see cref="AsyncCompleted"/> even
    ''' in case of task fault. A continuation task, which was described on stack overflow, is
    ''' augmented to invoke the completion event:
    ''' https://stackoverflow.com/questions/32067034/how-to-handle-task-run-exception.
    ''' </remarks>
    ''' <param name="action"> The action. </param>
    ''' <returns> A Threading.Tasks.Task(Of TResult) </returns>
    Public Async Function StartActionAsync(ByVal action As Func(Of TResult)) As Threading.Tasks.Task(Of TResult)
        Me._ActionTask = Threading.Tasks.Task.Factory.StartNew(Of TResult)(action).ContinueWith(Function(t) As TResult
                                                                                                    If t.IsFaulted Then
                                                                                                        Dim evt As AsyncCompletedEventHandler = Me.AsyncCompletedEvent
                                                                                                        evt?.Invoke(Me, New AsyncCompletedEventArgs(t.Exception, False, Nothing))
                                                                                                    End If
                                                                                                    Return t.Result
                                                                                                End Function)
        Return Await Me._ActionTask
    End Function

    ''' <summary> Starts an action. </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    ''' <param name="action"> The action. </param>
    Public Sub StartAction(ByVal action As Func(Of TResult))
        Me._AsyncTask = Me.StartActionAsync(action)
    End Sub

    ''' <summary> Awaits the given timeout for the task to idle. </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> True if the task idled within the given timespan, false if timeout. </returns>
    Public Function AwaitTaskIdle(ByVal timeout As TimeSpan) As Boolean
        Return Me.ActionTask.Wait(timeout)
    End Function

    ''' <summary> Await completion. </summary>
    ''' <remarks> David, 2020-07-17. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown if task has not been instantiated. </exception>
    ''' <exception cref="OperationFailedException">  Thrown when operation timeout. </exception>
    ''' <param name="timeout"> The timeout. </param>
    ''' <returns> The (Status As Threading.Tasks.TaskStatus, Result As TResult) </returns>
    Public Function AwaitCompletion(ByVal timeout As TimeSpan) As (Status As Threading.Tasks.TaskStatus, Result As TResult)
        If Me.ActionTask Is Nothing Then
            Throw New InvalidOperationException("Asynchronous task has not been initiated")
        ElseIf Me.AwaitTaskIdle(timeout) Then
            Return (Me.ActionTask.Status, Me.ActionTask.Result)
        Else
            Throw New OperationFailedException($"timeout awaiting completion of the asynchronous task")
        End If
    End Function

End Class
