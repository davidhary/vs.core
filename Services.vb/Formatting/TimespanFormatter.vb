﻿Imports System.Text.RegularExpressions

''' <summary> Time span formatter. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 7/15/2014, Created. </para><para>
''' http://stackoverflow.com/questions/3627922/format-time-span-in-datagridview-column </para>
''' </remarks>
Public Class TimeSpanFormatter
    Implements IFormatProvider, ICustomFormatter
    ''' <summary> The format parser. </summary>

    Private ReadOnly _FormatParser As Regex

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub New()
        MyBase.New()
        Me._FormatParser = New Regex("d{1,2}|h{1,2}|m{1,2}|s{1,2}|f{1,7}", RegexOptions.Compiled)
    End Sub

#Region "I Format Provider Members"

    ''' <summary>
    ''' Returns an object that provides formatting services for the specified type.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="formatType"> An object that specifies the type of format object to return. </param>
    ''' <returns>
    ''' An instance of the object specified by <paramref name="formatType" />, if the
    ''' <see cref="T:System.IFormatProvider" /> implementation can supply that type of object;
    ''' otherwise, null.
    ''' </returns>
    Public Function GetFormat(ByVal formatType As Type) As Object Implements IFormatProvider.GetFormat
        Return If(GetType(ICustomFormatter).Equals(formatType), Me, Nothing)
    End Function

#End Region

#Region "I Custom Formatter Members"

    ''' <summary>
    ''' Converts the value of a specified object to an equivalent string representation using
    ''' specified format and culture-specific formatting information.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="format1">        A format string containing formatting specifications. </param>
    ''' <param name="arg">            An object to format. </param>
    ''' <param name="formatProvider"> An object that supplies format information about the current
    '''                               instance. </param>
    ''' <returns>
    ''' The string representation of the value of <paramref name="arg" />, formatted as specified by
    ''' <paramref name="format1" /> and <paramref name="formatProvider" />.
    ''' </returns>
    Public Function Format(ByVal format1 As String, ByVal arg As Object, ByVal formatProvider As IFormatProvider) As String Implements ICustomFormatter.Format
        If TypeOf arg Is TimeSpan Then
            Dim timeSpan As TimeSpan = CType(arg, TimeSpan)
            Return Me._FormatParser.Replace(format1, TimeSpanFormatter.GetMatchEvaluator(timeSpan))
        Else
            Dim formattable As IFormattable = TryCast(arg, IFormattable)
            Return If(formattable IsNot Nothing,
                formattable.ToString(format1, formatProvider),
                If(arg IsNot Nothing, arg.ToString(), String.Empty))
        End If
    End Function

#End Region

    ''' <summary> Gets match evaluator. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="timeSpan"> The time span. </param>
    ''' <returns> The match evaluator. </returns>
    Private Shared Function GetMatchEvaluator(ByVal timeSpan As TimeSpan) As MatchEvaluator
        Return Function(m) EvaluateMatch(m, timeSpan)
    End Function

    ''' <summary> Evaluate match. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="match">    Specifies the match. </param>
    ''' <param name="timeSpan"> The time span. </param>
    ''' <returns> A String. </returns>
    Private Shared Function EvaluateMatch(ByVal match As Match, ByVal timeSpan As TimeSpan) As String
        Select Case match.Value
            Case "dd"
                Return timeSpan.Days.ToString("00")
            Case "d"
                Return timeSpan.Days.ToString("0")
            Case "hh"
                Return timeSpan.Hours.ToString("00")
            Case "h"
                Return timeSpan.Hours.ToString("0")
            Case "mm"
                Return timeSpan.Minutes.ToString("00")
            Case "m"
                Return timeSpan.Minutes.ToString("0")
            Case "ss"
                Return timeSpan.Seconds.ToString("00")
            Case "s"
                Return timeSpan.Seconds.ToString("0")
            Case "fffffff"
                Return (timeSpan.Milliseconds * 10000).ToString("0000000")
            Case "ffffff"
                Return (timeSpan.Milliseconds * 1000).ToString("000000")
            Case "fffff"
                Return (timeSpan.Milliseconds * 100).ToString("00000")
            Case "ffff"
                Return (timeSpan.Milliseconds * 10).ToString("0000")
            Case "fff"
                Return (timeSpan.Milliseconds).ToString("000")
            Case "ff"
                Return (timeSpan.Milliseconds \ 10).ToString("00")
            Case "f"
                Return (timeSpan.Milliseconds \ 100).ToString("0")
            Case Else
                Return match.Value
        End Select
    End Function
End Class
