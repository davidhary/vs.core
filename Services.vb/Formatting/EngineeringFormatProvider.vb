﻿''' <summary>
''' Custom numeric string formatting, including engineering notation. I needed to display numbers
''' in an engineering application. The customer wanted the numbers formatted nicely, with about
''' four significant digits and engineering notation. .NET has very flexible number formatting.
''' However, it lacks standard formatting into engineering notation. The extension point for
''' formatting numeric strings is through the FormatProvider and CustomFormatter interfaces. I
''' implemented a format provider for the engineering notation. However, exclusively using
''' engineering notation was awkward in our application. As an alternative to exclusively using
''' engineering notation, I decided on the following format, which mixes four significant digits,
''' engineering notation, and thousand separators.
''' </summary>
''' <remarks> (c) 2010 Wallace Kelly . http://WallaceKelly.BlogSpot.com/. </remarks>
Public Class EngineeringFormatProvider
    Implements IFormatProvider, ICustomFormatter

    ''' <summary>
    ''' Returns an object that provides formatting services for the specified type.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="formatType"> Type of the format. </param>
    ''' <returns>
    ''' An instance of the object specified by <paramref name="formatType" />, if the
    ''' <see cref="T:System.IFormatProvider" /> implementation can supply that type of object;
    ''' otherwise, null.
    ''' </returns>
    Public Function GetFormat(ByVal formatType As Type) As Object Implements IFormatProvider.GetFormat
        Return If((formatType Is GetType(ICustomFormatter)), Me, Nothing)
    End Function

    ''' <summary>
    ''' Gets or sets a value indicating whether this object using thousands separator.
    ''' </summary>
    ''' <remarks> Applies between 1000 and only up to 99,999. </remarks>
    ''' <value> <c>True</c> if use thousands separator; otherwise <c>false</c>. </value>
    Public Property UsingThousandsSeparator As Boolean

    ''' <summary> Formats using engineering notation. </summary>
    ''' <remarks>
    ''' Example: String.Format(new EngineeringFormatProvider(), "{0}", number);
    ''' Example: String.Format(new EngineeringFormatProvider(), "{0:G4}", number);
    ''' </remarks>
    ''' <param name="format1">        Specifies the format. This format string is stripped from the
    '''                               original format. For example, the {0} string yield 'nothing'
    '''                               whereas {0:G4} yields G4. </param>
    ''' <param name="arg">            The value to format. </param>
    ''' <param name="formatProvider"> . </param>
    ''' <returns>
    ''' The formatted value as a <see cref="T:String">string</see>
    ''' The empty string is returned if the value is null.
    ''' </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Function Format(ByVal format1 As String, ByVal arg As Object, ByVal formatProvider As IFormatProvider) As String Implements ICustomFormatter.Format

        If String.IsNullOrWhiteSpace(format1) Then
            format1 = "G4"
        End If
        Dim engineeringFormat As String = String.Format(Globalization.CultureInfo.CurrentCulture, "{0}{1}{2}", "{0:", format1, "}e{1}{2:00}")
        Dim defaultFormat As String = String.Format(Globalization.CultureInfo.CurrentCulture, "{0}{1}{2}", "{0:", format1, "}")

        ' for doubles, store the value of the double
        Dim val As Double = Double.NaN
        If arg Is Nothing Then
            Return String.Empty
        ElseIf TypeOf arg Is Double Then
            val = CDbl(arg)
        Else
            ' for other types, try to convert to a double
            Dim typeConverter As System.ComponentModel.TypeConverter = System.ComponentModel.TypeDescriptor.GetConverter(arg)
            If typeConverter.CanConvertTo(GetType(Double)) Then
                Try
                    val = CDbl(typeConverter.ConvertTo(arg, GetType(Double)))
                Catch
                    ' ignore
                End Try
            End If

            ' if cannot convert, return a default value
            If Double.IsNaN(val) Then
                Return If(arg Is Nothing, String.Empty, arg.ToString())
            End If
        End If

        ' for special cases, just write out the string
        If val = 0.0 OrElse Double.IsNaN(val) OrElse Double.IsInfinity(val) Then

            Return val.ToString(Globalization.CultureInfo.CurrentCulture)

        Else
            ' calculate the exponents, as a power of 3
            Dim exp As Double = Math.Log10(Math.Abs(val))
            Dim exp3 As Integer = CInt(3 * Math.Floor(exp / 3.0))

            ' calculate the coefficient
            Dim coef As Double = val / Math.Pow(10, exp3)

            ' special case, for example 0.3142
            If exp3 = -3 AndAlso Math.Abs(coef / 1000.0) < 1 AndAlso Math.Abs(coef / 1000.0) > 0.1 Then
                Return String.Format(Globalization.CultureInfo.CurrentCulture, defaultFormat, val)
            End If

            ' for "small" numbers
            If exp3 <= -3 Then
                If exp3 > 0 Then
                    'Return String.Format(Globalization.CultureInfo.CurrentCulture, format1 & "e{1}{2:00}", coefficient, "+", exp3)
                    Return String.Format(Globalization.CultureInfo.CurrentCulture, engineeringFormat, coef, "+", exp3)
                Else
                    Return String.Format(Globalization.CultureInfo.CurrentCulture, engineeringFormat, coef, "", exp3)
                End If
            End If

            ' for "large" numbers
            If exp < 6 AndAlso exp >= 3 AndAlso Me.UsingThousandsSeparator Then
                ' for numbers needing thousand separators
                Return String.Format(Globalization.CultureInfo.CurrentCulture, "{0:N0}", val)
            ElseIf exp >= 3 Then
                Return If(exp3 > 0,
                    String.Format(Globalization.CultureInfo.CurrentCulture, engineeringFormat, coef, "+", exp3),
                    String.Format(Globalization.CultureInfo.CurrentCulture, engineeringFormat, coef, "", exp3))
            End If

            ' default
            Return String.Format(Globalization.CultureInfo.CurrentCulture, defaultFormat, val)

        End If
    End Function
End Class

