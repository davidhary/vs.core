﻿''' <summary>
''' A format provider for a value representing a digital number consisting from a sequence of
''' bits or bytes.
''' </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 11/21/2019 </para>
''' </remarks>
Public Class DigitalValueFormatProvider
    Implements IFormatProvider, ICustomFormatter

    ''' <summary> The register value default hex prefix. </summary>
    ''' <value> The default hexadecimal prefix. </value>
    Public Shared Property DefaultHexPrefix As String = "0x"

    ''' <summary> Gets or sets the default byte count. </summary>
    ''' <value> The default byte count. </value>
    Public Shared Property DefaultByteCount As Integer = 2

    ''' <summary> Gets or sets the default format string. </summary>
    ''' <value> The default format string. </value>
    Public Shared Property DefaultFormatString As String = $"{DefaultHexPrefix}{{0:X{DefaultByteCount}}}"

    ''' <summary> Gets or sets the default null value caption. </summary>
    ''' <value> The default null value caption. </value>
    Public Shared Property DefaultNullValueCaption As String = $"{DefaultHexPrefix}{StrDup(DefaultByteCount, ".")}"

    ''' <summary> The format string. </summary>
    ''' <value> The format string. </value>
    Public Property FormatString As String = DigitalValueFormatProvider.DefaultFormatString

    ''' <summary> The null value caption. </summary>
    ''' <value> The null value caption. </value>
    Public Property NullValueCaption As String = DigitalValueFormatProvider.DefaultNullValueCaption

    ''' <summary> The caption prefix. </summary>
    ''' <value> The prefix. </value>
    Public Property Prefix As String = DigitalValueFormatProvider.DefaultHexPrefix

    ''' <summary>
    ''' Returns an object that provides formatting services for the specified type.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="formatType"> An object that specifies the type of format object to return. </param>
    ''' <returns>
    ''' An instance of the object specified by <paramref name="formatType" />, if the
    ''' <see cref="T:System.IFormatProvider" /> implementation can supply that type of object;
    ''' otherwise, <see langword="null" />.
    ''' </returns>
    Public Function GetFormat(ByVal formatType As Type) As Object Implements IFormatProvider.GetFormat
        Return If(formatType = GetType(ICustomFormatter), Me, Nothing)
    End Function

    ''' <summary>
    ''' Converts the value of a specified object to an equivalent string representation using
    ''' specified format and culture-specific formatting information.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="formatString">   A format string containing formatting specifications. </param>
    ''' <param name="arg">            An object to format. </param>
    ''' <param name="formatProvider"> An object that supplies format information about the current
    '''                               instance. </param>
    ''' <returns>
    ''' The string representation of the value of <paramref name="arg" />, formatted as specified by
    ''' <paramref name="formatString" /> and <paramref name="formatProvider" />.
    ''' </returns>
    Public Function Format(formatString As String, arg As Object,
                           formatProvider As IFormatProvider) As String Implements ICustomFormatter.Format
        Return DigitalValueFormatProvider.Format(Me.FormatString, Me.NullValueCaption, arg)
    End Function

    ''' <summary>
    ''' Converts the value of a specified object to an equivalent string representation using
    ''' specified format and culture-specific formatting information.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="arg"> An object to format. </param>
    ''' <returns>
    ''' The string representation of the value of <paramref name="arg" />, formatted as specified by.
    ''' </returns>
    Public Function Format(arg As Object) As String
        Return DigitalValueFormatProvider.Format(Me.FormatString, Me.NullValueCaption, arg)
    End Function

    ''' <summary>
    ''' Converts the value of a specified object to an equivalent string representation using
    ''' specified format and culture-specific formatting information.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="formatString">     A format string containing formatting specifications. </param>
    ''' <param name="nullValueCaption"> The null value caption. </param>
    ''' <param name="arg">              An object to format. </param>
    ''' <returns>
    ''' The string representation of the value of <paramref name="arg" />, formatted as specified by.
    ''' </returns>
    Public Shared Function Format(ByVal formatString As String, ByVal nullValueCaption As String, arg As Object) As String
        Return If(arg Is Nothing, nullValueCaption, String.Format(formatString, CInt(arg)))
    End Function

    ''' <summary> Parses the provided string to an enum value. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A Nullable(Of T) </returns>
    Public Function ParseEnumHexValue(Of T As Structure)(ByVal value As String) As Nullable(Of T)
        Return DigitalValueFormatProvider.ParseEnumValue(Of T)(Me.Prefix, value, Globalization.NumberStyles.HexNumber)
    End Function

    ''' <summary> Parses the provided string to an enum value. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="InvalidCastException"> Thrown when an object cannot be cast to a required
    '''                                         type. </exception>
    ''' <param name="prefix">      The caption prefix. </param>
    ''' <param name="value">       The value. </param>
    ''' <param name="numberStyle"> Number of styles. </param>
    ''' <returns> A Nullable(Of T) </returns>
    Public Shared Function ParseEnumValue(Of T As Structure)(ByVal prefix As String, ByVal value As String, ByVal numberStyle As Globalization.NumberStyles) As Nullable(Of T)
        Dim result As New Nullable(Of T)
        If Not String.IsNullOrWhiteSpace(value) Then
            If prefix IsNot Nothing Then value = value.TrimStart(prefix.ToCharArray)
            If value.Length > 0 Then
                Dim enumValue As Integer = 0
                If Integer.TryParse(value, numberStyle, Globalization.CultureInfo.CurrentCulture, enumValue) Then
                    Dim val As T = Nothing
                    If [Enum].TryParse(Of T)(enumValue.ToString, val) Then
                        result = val
                    Else
                        Throw New InvalidCastException($"Can't convert {value}=({enumValue}) to {GetType(T)}")
                    End If
                Else
                    Throw New InvalidCastException($"Can't convert {value} to {GetType(Integer)}")
                End If
            End If
        End If
        Return result
    End Function

    ''' <summary> Hexadecimal value format provider. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="byteCount"> Number of bytes. </param>
    ''' <returns> A RegisterValueFormatProvider. </returns>
    Public Shared Function HexValueFormatProvider(ByVal byteCount As Integer) As DigitalValueFormatProvider
        Return HexValueFormatProvider(DigitalValueFormatProvider.DefaultHexPrefix, byteCount)
    End Function

    ''' <summary> Hexadecimal value format provider. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="prefix">    The caption prefix. </param>
    ''' <param name="byteCount"> Number of bytes. </param>
    ''' <returns> A RegisterValueFormatProvider. </returns>
    Public Shared Function HexValueFormatProvider(ByVal prefix As String, ByVal byteCount As Integer) As DigitalValueFormatProvider
        Dim result As New DigitalValueFormatProvider With {
            .Prefix = prefix,
            .FormatString = $"{prefix}{{0:X{byteCount}}}",
            .NullValueCaption = $"{prefix}{StrDup(byteCount, ".")}"
        }
        Return result
    End Function

End Class

