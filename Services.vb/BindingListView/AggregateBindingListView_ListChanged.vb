Imports System.ComponentModel

Partial Public Class AggregateBindingListView(Of T)

#Region " LIST CHANGED EVENT HANDLER "

    ''' <summary> Removes the List Changed event handlers. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Protected Sub RemoveListChangedEventHandler()
        Me._ListChangedHandlers.RemoveAll()
    End Sub
    ''' <summary> The list changed handlers. </summary>
    Private ReadOnly _ListChangedHandlers As New NotifyListChangedEventContextCollection()

    ''' <summary> Event queue for all listeners interested in List Changed events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event ListChanged As ListChangedEventHandler Implements IBindingList.ListChanged

        AddHandler(ByVal value As ListChangedEventHandler)
            Me._ListChangedHandlers.Add(New NotifyListChangedEventContext(value))
        End AddHandler

        RemoveHandler(ByVal value As ListChangedEventHandler)
            Me._ListChangedHandlers.RemoveValue(value)
        End RemoveHandler

        RaiseEvent(ByVal sender As Object, ByVal e As ListChangedEventArgs)
            Me._ListChangedHandlers.Raise(sender, e)
        End RaiseEvent

    End Event

#End Region

#Region " NOTIFY "

    ''' <summary>
    ''' Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
    ''' fast return of control to the invoking function.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="e"> List Changed event information. </param>
    Protected Overridable Sub NotifyListChanged(ByVal e As ListChangedEventArgs)
        Me._ListChangedHandlers.Post(Me, e)
    End Sub

    ''' <summary>
    ''' Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
    ''' fast return of control to the invoking function.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
    ''' even with slow handler functions.
    ''' </remarks>
    ''' <param name="e"> List Changed event information. </param>
    Protected Overridable Sub AsyncNotifyListChanged(ByVal e As ListChangedEventArgs)
        Me._ListChangedHandlers.Post(Me, e)
    End Sub

    ''' <summary>
    ''' Synchronously notifies (send) list change on the caller thread. Safe for cross threading.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 3 to 4 times larger than naked raise event. This is the best
    ''' approach.
    ''' </remarks>
    ''' <param name="e"> List Changed event information. </param>
    Protected Overridable Sub SyncNotifyListChanged(ByVal e As ListChangedEventArgs)
        Me._ListChangedHandlers.Send(Me, e)
    End Sub

#End Region

#Region " LIST CHANGE NOTIFICATIONS "

    ''' <summary>
    ''' Helper method to build the ListChangedEventArgs needed for the ListChanged event.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="listChangedType"> The type of change that occurred. </param>
    ''' <param name="newIndex">        The index of the changed item. </param>
    Private Sub OnListChanged(ByVal listChangedType As ListChangedType, ByVal newIndex As Integer)
        Me.NotifyListChanged(New ListChangedEventArgs(listChangedType, newIndex))
    End Sub

    ''' <summary>
    ''' Helper method to build the ListChangedEventArgs needed for the ListChanged event.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="listChangedType"> The type of change that  occurred. </param>
    ''' <param name="newIndex">        The index of the item after the change. </param>
    ''' <param name="oldIndex">        The index of the item before the change. </param>
    Private Sub OnListChanged(ByVal listChangedType As ListChangedType, ByVal newIndex As Integer, ByVal oldIndex As Integer)
        Me.NotifyListChanged(New ListChangedEventArgs(listChangedType, newIndex, oldIndex))
    End Sub

    ''' <summary> Raises the ListChanged event with the given event arguments. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="e"> The ListChangedEventArgs to raise the event with. </param>
    Protected Overridable Sub OnListChanged(ByVal e As ListChangedEventArgs)
        Me.NotifyListChanged(e)
    End Sub

#End Region

End Class
