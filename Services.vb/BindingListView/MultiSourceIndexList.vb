''' <summary> Multi source index list. </summary>
''' <remarks>
''' (c) 2006 Andrew Davey. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/15/2018.  </para><para>
''' David, 12/15/2018, 1.2.*. https://blogs.warwick.ac.uk/andrewdavey and
''' https://sourceforge.net/projects/blw/. </para>
''' </remarks>
Friend Class MultiSourceIndexList(Of T)
    Inherits List(Of KeyValuePair(Of ListItemPair(Of T), Integer))

    ''' <summary> Adds sourceList. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="sourceList"> List of sources. </param>
    ''' <param name="item">       The <see cref="ObjectView(Of T)"/> to search for. </param>
    ''' <param name="index">      Zero-based index of the. </param>
    Public Overloads Sub Add(ByVal sourceList As IList, ByVal item As ObjectView(Of T), ByVal index As Integer)
        MyBase.Add(New KeyValuePair(Of ListItemPair(Of T), Integer)(New ListItemPair(Of T)(sourceList, item), index))
    End Sub

    ''' <summary>
    ''' Searches for a given source index value, returning the list index of the value.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="sourceList">  List of sources. </param>
    ''' <param name="sourceIndex"> The source index to find. </param>
    ''' <returns> Returns the index in this list of the source index, or -1 if not found. </returns>
    Public Function IndexOfSourceIndex(ByVal sourceList As IList, ByVal sourceIndex As Integer) As Integer
        For i As Integer = 0 To Me.Count - 1
            If Me(i).Key.List Is sourceList AndAlso Me(i).Value = sourceIndex Then
                Return i
            End If
        Next i
        Return -1
    End Function

    ''' <summary> Searches for a given item, returning the index of the value in this list. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="item"> The <typeparamref name="T"/> item to search for. </param>
    ''' <returns> Returns the index in this list of the item, or -1 if not found. </returns>
    Public Function IndexOfItem(ByVal item As T) As Integer
        For i As Integer = 0 To Me.Count - 1
            If Me(i).Key.Item.Object.Equals(item) AndAlso Me(i).Value > -1 Then
                Return i
            End If
        Next i
        Return -1
    End Function

    ''' <summary>
    ''' Searches for a given item's <see cref="ObjectView(Of T)"/> wrapper, returning the index of
    ''' the value in this list.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="item"> The <see cref="ObjectView(Of T)"/> to search for. </param>
    ''' <returns> Returns the index in this list of the item, or -1 if not found. </returns>
    Public Function IndexOfKey(ByVal item As ObjectView(Of T)) As Integer
        For i As Integer = 0 To Me.Count - 1
            If Me(i).Key.Item.Equals(item) AndAlso Me(i).Value > -1 Then
                Return i
            End If
        Next i
        Return -1
    End Function

    ''' <summary> Checks if the list contains a given item. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="item"> The <typeparamref name="T"/> item to check for. </param>
    ''' <returns> True if the item is contained in the list, otherwise false. </returns>
    Public Function ContainsItem(ByVal item As T) As Boolean
        Return (Me.IndexOfItem(item) <> -1)
    End Function

    ''' <summary> Checks if the list contains a given <see cref="ObjectView(Of T)"/> key. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="key"> The key to search for. </param>
    ''' <returns> True if the key is contained in the list, otherwise false. </returns>
    Public Function ContainsKey(ByVal key As ObjectView(Of T)) As Boolean
        Return (Me.IndexOfKey(key) <> -1)
    End Function

    ''' <summary>
    ''' Returns an array of all the <see cref="ObjectView(Of T)"/> keys in the list.
    ''' </summary>
    ''' <value> The keys. </value>
    Public ReadOnly Property Keys() As ObjectView(Of T)()
        Get
            Return Me.ConvertAll(New Converter(Of KeyValuePair(Of ListItemPair(Of T), Integer), ObjectView(Of T))(Function(kvp As KeyValuePair(Of ListItemPair(Of T), Integer)) kvp.Key.Item)).ToArray()
        End Get
    End Property

    ''' <summary>
    ''' Returns an <see cref="IEnumerator(Of T)"/> to iterate over all the keys in this list.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The <see cref="IEnumerator(Of T)"/> to use. </returns>
    Public Iterator Function GetKeyEnumerator() As IEnumerator(Of ObjectView(Of T))
        For Each kvp As KeyValuePair(Of ListItemPair(Of T), Integer) In Me
            Yield kvp.Key.Item
        Next kvp
    End Function
End Class

''' <summary> A t. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/15/2018 </para>
''' </remarks>
Friend Class ListItemPair(Of T)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="list"> The list. </param>
    ''' <param name="item"> The item. </param>
    Public Sub New(ByVal list As IList, ByVal item As ObjectView(Of T))
        Me._List = list
        Me._Item = item
    End Sub

    ''' <summary> Gets or sets the list. </summary>
    ''' <value> The list. </value>
    Public ReadOnly Property List() As IList

    ''' <summary> Gets or sets the item. </summary>
    ''' <value> The item. </value>
    Public ReadOnly Property Item() As ObjectView(Of T)

End Class
