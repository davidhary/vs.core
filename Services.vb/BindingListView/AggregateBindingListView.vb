Imports System.ComponentModel
Imports System.Reflection
Imports System.Reflection.Emit

''' <summary>
''' A searchable, sortable, filterable, data bindable view of a list of objects.
''' </summary>
''' <remarks>
''' David, 12/15/2018, <para>
''' David, 12/15/2018, 1.2.*, https://blogs.warwick.ac.uk/andrewdavey and
''' https://sourceforge.net/projects/blw/. (c) 2006 Andrew Davey. All rights
''' reserved.</para><para>
''' Licensed under The MIT License.</para><para>
'''  </para>
''' </remarks>
Public Class AggregateBindingListView(Of T)
    Inherits Component
    Implements IBindingListView, IList, IRaiseItemChangedEvents, ICancelAddNew, ITypedList

#Region " CONSTRUCTION "

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub New()
        Me._SourceLists = New BindingList(Of IList)()
        AddHandler TryCast(Me._SourceLists, IBindingList).ListChanged, AddressOf Me.SourceListsChanged
        Me._SavedSourceLists = New List(Of IList)()
        Me._SourceIndices = New MultiSourceIndexList(Of T)()
        ' Start with a filter that includes all items.
        Me._Filter = IncludeAllItemFilter(Of T).Instance
        ' Start with no sorts applied.
        Me._Sorts = New ListSortDescriptionCollection()
        Me._ObjectViewCache = New Dictionary(Of T, ObjectView(Of T))()
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="container"> The container. </param>
    Public Sub New(ByVal container As IContainer)
        Me.New()
        container?.Add(Me)
        If TypeOf MyBase.Site Is ISynchronizeInvoke Then
            Me.SynchronizingObject = TryCast(MyBase.Site, ISynchronizeInvoke)
        End If
    End Sub

#End Region

#Region " PRIVATE MEMBER FIELDS "

    ''' <summary>
    ''' The sorted, filtered list of item indices in _sourceList.
    ''' </summary>
    Private _SourceIndices As MultiSourceIndexList(Of T)

    ''' <summary>
    ''' The current filter applied to the view.
    ''' </summary>
    Private _Filter As IItemFilter(Of T)

    ''' <summary>
    ''' The current sorts applied to the view.
    ''' </summary>
    Private _Sorts As ListSortDescriptionCollection

    ''' <summary>
    ''' The <see cref="System.Collections.Generic.IComparer(Of T)">IComparer</see> used to compare items when sorting.
    ''' </summary>
    Private _Comparer As IComparer(Of KeyValuePair(Of ListItemPair(Of T), Integer))

    ''' <summary>
    ''' The item in the process of being added to the view.
    ''' </summary>
    Private _NewItem As ObjectView(Of T)

    ''' <summary>
    ''' A copy of the source lists so when a list is removed from SourceLists we still have a
    ''' reference to use for unhooking events, etc.
    ''' </summary>
    ''' <exception cref="ArgumentNullException">      Thrown when one or more required arguments are
    '''                                               null. </exception>
    ''' <exception cref="InvalidSourceListException"> Thrown when an Invalid Source List error
    '''                                               condition occurs. </exception>
    ''' <value> The saved source lists. </value>
    Private ReadOnly Property SavedSourceLists As IList(Of IList)

    ''' <summary>
    ''' ObjectView cache used to prevent re-creation of existing object wrappers when in
    ''' FilterAndSort().
    ''' </summary>
    ''' <exception cref="ArgumentNullException">      Thrown when one or more required arguments are
    '''                                               null. </exception>
    ''' <exception cref="InvalidSourceListException"> Thrown when an Invalid Source List error
    '''                                               condition occurs. </exception>
    ''' <value> The object view cache. </value>
    Private ReadOnly Property ObjectViewCache As IDictionary(Of T, ObjectView(Of T))

#End Region

    ''' <summary>
    ''' The list of underlying list of items on which this view is based.
    ''' </summary>
    Private _SourceLists As IList

    ''' <summary> Gets or sets the list of source lists used by this view. </summary>
    ''' <exception cref="ArgumentNullException">      Thrown when one or more required arguments are
    '''                                               null. </exception>
    ''' <exception cref="InvalidSourceListException"> Thrown when an Invalid Source List error
    '''                                               condition occurs. </exception>
    ''' <value> The source lists. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property SourceLists() As IList
        Get
            Return Me._SourceLists
        End Get
        Set(ByVal value As IList)
            If value Is Nothing Then
                Throw New ArgumentNullException(NameOf(value), My.Resources.SourceListsNull)
            End If

            ' Check that every item in each list is of type T.
            For Each obj As Object In value
                If obj Is Nothing Then
                    Throw New InvalidSourceListException()
                End If

                Dim list As IList = Nothing
                If Not String.IsNullOrEmpty(Me.DataMember) Then
                    For Each pd As PropertyDescriptor In TypeDescriptor.GetProperties(obj)
                        If pd.Name = Me.DataMember Then
                            list = TryCast(pd.GetValue(obj), IList)
                            Exit For
                        End If
                    Next pd
                ElseIf TypeOf obj Is IListSource Then
                    Dim src As IListSource = TryCast(obj, IListSource)
                    list = If(src.ContainsListCollection, TryCast(src.GetList()(0), IList), (TryCast(obj, IListSource)).GetList())
                ElseIf Not (TypeOf obj Is ICollection(Of T)) Then
                    list = TryCast(obj, IList)
                Else
                    ' We have a typed collection, so can skip the item-by-item check.
                    Continue For
                End If

                If list Is Nothing Then
                    Throw New InvalidSourceListException()
                End If

                For Each item As Object In list
                    If Not (TypeOf item Is T) Then
                        Throw New InvalidSourceListException(String.Format(My.Resources.InvalidListItemType, GetType(T).FullName))
                    End If
                Next item
            Next obj

            Dim bindingList As IBindingList = TryCast(Me._SourceLists, IBindingList)

            ' Unhook old list changed event.
            If bindingList IsNot Nothing AndAlso bindingList.SupportsChangeNotification Then
                RemoveHandler bindingList.ListChanged, AddressOf Me.SourceListsChanged
            End If

            For Each list As Object In Me._SourceLists
                Dim bl As IBindingList = TryCast(list, IBindingList)
                If bl IsNot Nothing AndAlso bl.SupportsChangeNotification Then
                    RemoveHandler bl.ListChanged, AddressOf Me.SourceListChanged
                End If
            Next list

            Me._SourceLists = value

            bindingList = TryCast(Me._SourceLists, IBindingList)
            ' Hook new list changed event
            If bindingList IsNot Nothing AndAlso bindingList.SupportsChangeNotification Then
                AddHandler bindingList.ListChanged, AddressOf Me.SourceListsChanged
            End If
            For Each list As Object In Me._SourceLists
                Dim bl As IBindingList = TryCast(list, IBindingList)
                If bl IsNot Nothing AndAlso bl.SupportsChangeNotification Then
                    AddHandler bl.ListChanged, AddressOf Me.SourceListChanged
                End If
            Next list

            ' save new lists
            Me.BuildSavedList()

            Me.FilterAndSort()
            Me.OnListChanged(ListChangedType.Reset, -1)
        End Set
    End Property

    ''' <summary> Gets the ObjectView(Of T) of the item at the given index in the view. </summary>
    ''' <value> The ObjectView(Of T) of the item. </value>
    Default Public ReadOnly Property Item(ByVal index As Integer) As ObjectView(Of T)
        Get
            Return Me._SourceIndices(index).Key.Item
        End Get
    End Property

    ''' <summary>
    ''' The property on a source list item that contains the actual list to view.
    ''' If null or empty then the source list item is used instead.
    ''' </summary>
    Private _DataMember As String

    ''' <summary> Gets or sets the data member. </summary>
    ''' <value> The data member. </value>
    <Browsable(False)>
    Public Property DataMember() As String
        Get
            Return Me._DataMember
        End Get
        Set(ByVal value As String)
            Me._DataMember = value
            Me.FilterAndSort()
            Me.OnListChanged(ListChangedType.Reset, -1)
        End Set
    End Property

    ''' <summary> Determine if we should serialize list member. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function ShouldSerializeListMember() As Boolean
        Return Not String.IsNullOrEmpty(Me.DataMember)
    End Function

#Region " Adding New Items "

    ''' <summary>
    ''' Attempts to get a new <typeparamref name="T"/> object to add to the list, first by raising
    ''' the AddingNew event and then (if no new object was assigned) by using the default public
    ''' constructor.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <returns> The new object to add to the list. </returns>
    Protected Overridable Function OnAddingNew() As T
        ' We allow users of this class to provide the object to add
        ' by raising the AddingNew event.
        If Me._AddingNewHandlers.Any Then
            Dim args As New AddingNewEventArgs()
            RaiseEvent AddingNew(Me, args)
            ' Check if we were given an object (and it's the correct type)
            If (args.NewObject IsNot Nothing) AndAlso (TypeOf args.NewObject Is T) Then
                Return CType(args.NewObject, T)
            End If
        End If
        ' Otherwise, try the default public constructor instead.
        ' Use reflection to find it. Note: We're not using the generic new() constraint since
        ' we do not want to force the need for a public default constructor when the user
        ' can simply handle the AddingNew event called above.
        Dim ci As System.Reflection.ConstructorInfo = GetType(T).GetConstructor(System.Type.EmptyTypes)
        If ci IsNot Nothing Then
            ' Invoke the constructor to create the object.
            Return CType(ci.Invoke(Nothing), T)
        Else
            Throw New InvalidOperationException(My.Resources.CannotAddNewItem)
        End If
    End Function

    ''' <summary>
    ''' Adds a new item to the view. Note that EndNew must be called to commit the item to the to the
    ''' source list.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The new item, wrapped in an ObjectView<typeparamref name="T"/>. </returns>
    Public Function AddNew() As ObjectView(Of T)
        ' Are we currently adding another item?
        If Me._NewItem IsNot Nothing Then
            ' Need to commit previous new item before adding another.
            Me.EndNew(Me._SourceIndices.Count - 1)
        End If

        ' Get the new item to add.
        Dim item As T = Me.OnAddingNew()

        ' Create the ObjectView<T> wrapper for the item.
        Dim objectView As New ObjectView(Of T)(item, Me)

        Me._ObjectViewCache(item) = objectView

        Me.HookPropertyChangedEvent(objectView)

        ' Set the _newItem reference so we know what to use when ending/canceling this add operation.
        Me._NewItem = objectView

        ' Add to indexes list, but index of -1 means it's not in the source list yet.
        Me._SourceIndices.Add(Me._NewItemsList, objectView, -1)
        ' Tell any data binders that we've added an item to the view.
        ' Put it at the end of the list.
        Me.OnListChanged(ListChangedType.ItemAdded, Me._SourceIndices.Count - 1)

        Return objectView
    End Function

    ''' <summary>
    ''' Cancels the pending addition of a new item to the source list and remove the item from the
    ''' view.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="itemIndex"> The index of the new item. </param>
    Public Sub CancelNew(ByVal itemIndex As Integer) Implements ICancelAddNew.CancelNew
        ' We must take special care that the item index does refer to the new item.
        If itemIndex > -1 AndAlso itemIndex < Me._SourceIndices.Count AndAlso Me._NewItem IsNot Nothing AndAlso Me._SourceIndices(itemIndex).Key.Item Is Me._NewItem Then
            ' We no longer need to listen to any events from the object.
            Me.UnHookPropertyChangedEvent(Me._NewItem)
            ' Remove the item from the view.
            Me._SourceIndices.RemoveAt(itemIndex)
            ' Data binders need to know the item has gone from the view.
            Me.OnListChanged(ListChangedType.ItemDeleted, itemIndex)
            ' Done with this adding operation, so clear the _newItem reference.
            Me._NewItem = Nothing
        End If
    End Sub

    ''' <summary> Commits the pending addition of a new item to the source list. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="itemIndex"> The index of the new item. </param>
    Public Sub EndNew(ByVal itemIndex As Integer) Implements ICancelAddNew.EndNew
        ' The binding infrastructure tends to call the method
        ' more times than needed and often with itemIndex not even pointing to the 
        ' new object! So we have to take special care to check.
        If itemIndex > -1 AndAlso itemIndex < Me._SourceIndices.Count AndAlso Me._NewItem IsNot Nothing AndAlso Me._SourceIndices(itemIndex).Key.Item Is Me._NewItem Then
            ' In order to reuse the SourceListChanged code for adding a new item
            ' we have to first remove all knowledge of the item, then add it 
            ' to the source list.

            ' We no longer need to listen to any events from the object.
            Me.UnHookPropertyChangedEvent(Me._NewItem)
            ' Remove the item from the view.
            Me._SourceIndices.RemoveAt(itemIndex)

            ' Add the actual data object to the source list.
            ' The SourceListChanged event handler will take care of correctly inserting this
            ' object into the view (if newItemsList is a IBindingList).
            Me._NewItemsList.Add(Me._NewItem.Object)

            ' If it is not an IBindingList (or not SupportsChangeNotification) 
            ' then we must force the update ourselves.
            If Not (TypeOf Me._NewItemsList Is IBindingList) OrElse Not (TryCast(Me._NewItemsList, IBindingList)).SupportsChangeNotification Then
                Me.FilterAndSort()
                Me.OnListChanged(ListChangedType.Reset, -1)
            End If

            ' Done with this adding operation, so clear the _newItem reference.
            Me._NewItem = Nothing
        End If
    End Sub

    ''' <summary>
    ''' The IList we will add new items to.
    ''' </summary>
    Private _NewItemsList As IList

    ''' <summary> Gets or sets the source list to which new items are added. </summary>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <value> A List of new items. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property NewItemsList() As IList
        Get
            Return Me._NewItemsList
        End Get
        Set(ByVal value As IList)
            If value IsNot Nothing AndAlso (Not Me._SourceLists.Contains(value)) Then
                Throw New ArgumentException(My.Resources.SourceListNotFound)
            End If
            Me._NewItemsList = value
        End Set
    End Property

#End Region

    ''' <summary> Re-applies any current filter and sorts to refresh the current view. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub Refresh()
        Me.FilterAndSort()
        ' Get any bound objects to refresh everything as well.
        Me.OnListChanged(ListChangedType.Reset, -1)
    End Sub

    ''' <summary>
    ''' Gets the object used to marshal event-handler calls that are invoked on a non-UI thread.
    ''' </summary>
    ''' <exception cref="InvalidSourceListException"> Thrown when an Invalid Source List error
    '''                                               condition occurs. </exception>
    ''' <exception cref="ArgumentNullException">      Thrown when one or more required arguments are
    '''                                               null. </exception>
    ''' <value> The synchronizing object. </value>
    <Browsable(False), DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)>
    Public Property SynchronizingObject() As ISynchronizeInvoke

    ''' <summary>
    ''' Updates the _sourceIndices list to contain the items that are current viewed according to
    ''' applied filter and sorts.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Protected Sub FilterAndSort()
        ' The view contains items from the source list
        ' and possibly a new items that are not yet committed.
        ' Therefore we can't just clear the list and start over
        ' as we would lose the new items. So we have to to insert
        ' filtered source list items into a new list first.
        ' New items can then be pulled out of the current view
        ' and appended to the new list.
        Dim newList As New MultiSourceIndexList(Of T)()

        ' Get items from the source list that are included by the current filter.
        For Each sourceList As IList In Me.EnumerateSourceLists()
            For i As Integer = 0 To sourceList.Count - 1
                Dim item As T = CType(sourceList(i), T)
                Dim editableObject As ObjectView(Of T)
                If Me._Filter.Include(item) Then
                    If Me._ObjectViewCache.ContainsKey(item) Then
                        editableObject = Me._ObjectViewCache(item)
                    Else
                        editableObject = New ObjectView(Of T)(item, Me)
                        Me._ObjectViewCache.Add(item, editableObject)
                        ' Listen to the editing notification and property changed events.
                        Me.HookEditableObjectEvents(editableObject)
                        Me.HookPropertyChangedEvent(editableObject)
                    End If

                    ' Add the editable object along with the index of the item in the source list.
                    newList.Add(sourceList, editableObject, i)
                Else
                    If Me._ObjectViewCache.ContainsKey(item) Then
                        editableObject = Me._ObjectViewCache(item)
                        Me.UnHookEditableObjectEvents(editableObject)
                        Me.UnHookPropertyChangedEvent(editableObject)
                        Me._ObjectViewCache.Remove(item)
                    End If
                End If
            Next i
        Next sourceList

        ' If we have sorts to apply, do them now
        If Me._Comparer IsNot Nothing Then
            newList.Sort(Me._Comparer)
        End If

        ' Now we can append any new items to the end of the view.
        For Each kvp As KeyValuePair(Of ListItemPair(Of T), Integer) In Me._SourceIndices
            ' New items have a source list index of -1 since they are not
            ' yet in the source list.
            If kvp.Value = -1 Then
                newList.Add(kvp)
            End If
        Next kvp

        ' Set our view now
        Me._SourceIndices = newList

        ' Note: We do not raise the ListChanged event with ListChangeType.Reset
        ' since the view may not have changed that much. It is better to let
        ' the calling code decide what has happened and raise events accordingly.
    End Sub

#Region " Editing Items Event Handlers "

    ''' <summary> Begun item edit. </summary>
    ''' <remarks>
    ''' Currently unused. Here in case we want to perform actions when an item edit begins.
    ''' </remarks>
    ''' <param name="sender"> The <see cref="ObjectView(Of T)"/> that raised the event. </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub OnItemEditBegun(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    ''' <summary> Canceled item edit. </summary>
    ''' <remarks>
    ''' Currently unused. Here in case we want to perform actions when an item edit is canceled.
    ''' </remarks>
    ''' <param name="sender"> The <see cref="ObjectView(Of T)"/> that raised the event. </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub OnItemEditCanceled(ByVal sender As Object, ByVal e As EventArgs)
    End Sub

    ''' <summary> Handles the <see cref="ObjectView(Of T)"/> EndedEdit event. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="sender"> The <see cref="ObjectView(Of T)"/> that raised the event. </param>
    ''' <param name="e">      Event information. </param>
    Protected Overridable Sub OnItemEditEnded(ByVal sender As Object, ByVal e As EventArgs)
        Dim editableObject As ObjectView(Of T) = CType(sender, ObjectView(Of T))

        ' Check if filtering removed the item from view
        ' by getting the index before and after
        Dim oldIndex As Integer = Me._SourceIndices.IndexOfItem(editableObject.Object)
        Me.FilterAndSort()
        Dim newIndex As Integer = Me._SourceIndices.IndexOfItem(editableObject.Object)
        ' if item was filtered out then the newIndex == -1
        If newIndex > -1 Then
            If oldIndex = newIndex Then
                Me.OnListChanged(ListChangedType.ItemChanged, newIndex)
            Else
                Me.OnListChanged(ListChangedType.ItemMoved, newIndex, oldIndex)
            End If
        Else
            Me.OnListChanged(ListChangedType.ItemDeleted, oldIndex)
        End If
    End Sub

#End Region

    ''' <summary> Event handler for when SourceLists is changed. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="InvalidSourceListException"> Thrown when an Invalid Source List error
    '''                                               condition occurs. </exception>
    ''' <param name="sender"> The <see cref="ObjectView(Of T)"/> that raised the event. </param>
    ''' <param name="e">      List changed event information. </param>
    Protected Overridable Sub SourceListsChanged(ByVal sender As Object, ByVal e As ListChangedEventArgs)
        If sender Is Nothing OrElse e Is Nothing Then Return
        If e.ListChangedType = ListChangedType.ItemAdded Then
            Dim list As IList = TryCast(Me.SourceLists(e.NewIndex), IList)
            If list Is Nothing Then
                Me.SourceLists.RemoveAt(e.NewIndex)
                Throw New InvalidSourceListException()
            End If

            If TypeOf list Is IBindingList Then
                ' We need to know when the source list changes
                AddHandler TryCast(list, IBindingList).ListChanged, AddressOf Me.SourceListChanged
            End If
            Me._SavedSourceLists.Add(list)
            Me.FilterAndSort()
            Me.OnListChanged(ListChangedType.Reset, -1)
        ElseIf e.ListChangedType = ListChangedType.ItemDeleted Then
            Dim list As IList = TryCast(Me._SavedSourceLists(e.NewIndex), IList)
            If list IsNot Nothing Then
                If TypeOf list Is IBindingList Then
                    RemoveHandler TryCast(list, IBindingList).ListChanged, AddressOf Me.SourceListChanged
                End If
                Me._SavedSourceLists.RemoveAt(e.NewIndex)
                Me.FilterAndSort()
                Me.OnListChanged(ListChangedType.Reset, -1)
            End If
        ElseIf e.ListChangedType = ListChangedType.Reset Then
            Me.BuildSavedList()
            Me.FilterAndSort()
            Me.OnListChanged(ListChangedType.Reset, -1)
        End If
    End Sub

    ''' <summary> Event handler for when a source list changes. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="sender"> The <see cref="ObjectView(Of T)"/> that raised the event. </param>
    ''' <param name="e">      List changed event information. </param>
    Private Sub SourceListChanged(ByVal sender As Object, ByVal e As ListChangedEventArgs)
        Dim oldIndex As Integer
        Dim newIndex As Integer
        Dim sourceList As IBindingList = TryCast(sender, IBindingList)
        Select Case e.ListChangedType
            Case ListChangedType.ItemAdded
                Me.FilterAndSort()
                ' Get the index of the newly sorted item
                newIndex = Me._SourceIndices.IndexOfSourceIndex(sourceList, e.NewIndex)
                If newIndex > -1 Then
                    Me.OnListChanged(ListChangedType.ItemAdded, newIndex)
                    ' Other items have moved down the list
                    For i As Integer = newIndex + 1 To Me.Count - 1
                        Me.OnListChanged(ListChangedType.ItemMoved, i - 1, i)
                    Next i
                Else
                    ' The item was excluded by the filter,
                    ' so to the viewer the item has been "deleted".
                    ' The new item will have been added at the end of the view
                    Me.OnListChanged(ListChangedType.ItemDeleted, Math.Max(Me.Count - 1, 0))
                End If

            Case ListChangedType.ItemChanged
                ' Check if filtering will remove the item from view
                ' by getting the index before and after
                oldIndex = Me._SourceIndices.IndexOfSourceIndex(sourceList, e.NewIndex)

                ' Is the object in our view?
                If oldIndex < 0 Then
                    Return
                End If

                Me.FilterAndSort()
                newIndex = Me._SourceIndices.IndexOfSourceIndex(sourceList, e.NewIndex)
                ' if item was filtered out then the newIndex == -1
                ' otherwise we can say that the item was changed.
                If newIndex > -1 Then
                    If newIndex = oldIndex Then
                        Me.OnListChanged(ListChangedType.ItemChanged, newIndex)
                    Else
                        ' Two items will have changed places
                        Me.OnListChanged(ListChangedType.ItemMoved, newIndex, oldIndex)
                    End If
                Else
                    Me.OnListChanged(ListChangedType.ItemDeleted, oldIndex)
                End If

            Case ListChangedType.ItemDeleted
                ' Find the deleted index
                newIndex = Me._SourceIndices.IndexOfSourceIndex(sourceList, e.NewIndex)

                ' Did we have the object in our view?
                If newIndex < 0 Then
                    Return
                End If

                ' Stop listening to it's events
                Me.UnHookEditableObjectEvents(Me._SourceIndices(newIndex).Key.Item)
                Me.UnHookPropertyChangedEvent(Me._SourceIndices(newIndex).Key.Item)
                ' Remove its index
                Me._SourceIndices.RemoveAt(newIndex)
                ' Move up indexes after removed item
                For i As Integer = 0 To Me._SourceIndices.Count - 1
                    If Me._SourceIndices(i).Value > e.NewIndex Then
                        Me._SourceIndices(i) = New KeyValuePair(Of ListItemPair(Of T), Integer)(Me._SourceIndices(i).Key, Me._SourceIndices(i).Value - 1)
                    End If
                Next i
                ' Inform listeners that an item has been deleted from this view
                Me.OnListChanged(ListChangedType.ItemDeleted, newIndex)

            Case ListChangedType.ItemMoved
                If (Not Me.IsSorted) AndAlso (TypeOf Me.Filter Is IncludeAllItemFilter(Of T)) Then
                    ' We can move the item in the view
                    ' note indexes match those in _sourceList
                    Me.OnListChanged(ListChangedType.ItemMoved, e.NewIndex, e.OldIndex)
                End If
                    ' Otherwise it makes no sense to move due to sort and/or filter

            Case ListChangedType.Reset
                ' Most of the source list has changed
                ' so re-sort and filter
                Me.FilterAndSort()
                ' The view is most likely to have changed lots as well
                Me.OnListChanged(ListChangedType.Reset, -1)
        End Select
    End Sub

    ''' <summary> Event handler for when an item in the view changes. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="sender"> The item that changed. </param>
    ''' <param name="e">      Property changed event information. </param>
    Private Sub ItemPropertyChanged(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        ' The changed item may not actually be present in the view
        Dim index As Integer = Me._SourceIndices.IndexOfItem(CType(sender, T))
        ' Test the returned index, -1 => not in the view.
        If index > -1 Then
            ' Tell listeners that an item has changed.
            ' This is inline with the IRaiseItemChangedEvents implementation.
            Me.OnListChanged(ListChangedType.ItemChanged, index)
        End If
    End Sub

#Region " Filtering "

    ''' <summary> Applies the filter described by includeItem. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="filter"> The current filter applied to the view. </param>
    Public Sub ApplyFilter(ByVal filter As IItemFilter(Of T))
        Me.Filter = filter
    End Sub

    ''' <summary> Applies the filter described by includeItem. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="includeItem"> The include item. </param>
    Public Sub ApplyFilter(ByVal includeItem As Predicate(Of T))
        If includeItem Is Nothing Then Throw New ArgumentNullException(NameOf(includeItem), My.Resources.IncludeDelegateCannotBeNull)
        Me.Filter = AggregateBindingListView(Of T).CreateItemFilter(includeItem)
    End Sub

    ''' <summary> Gets if this view supports filtering of items. Always returns true. </summary>
    ''' <value> The i binding list view supports filtering. </value>
    Private ReadOnly Property IBindingListView_SupportsFiltering() As Boolean Implements IBindingListView.SupportsFiltering
        Get
            Return True
        End Get
    End Property

    ''' <summary> Gets or sets the binding list view filter. </summary>
    ''' <remarks> Explicitly implemented to expose the stronger Filter property instead. </remarks>
    ''' <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    ''' <value> The i binding list view filter. </value>
    Private Property IBindingListView_Filter() As String Implements IBindingListView.Filter
        Get
            Return CType(Me.Filter, Object).ToString()
        End Get
        Set(ByVal value As String)
            Throw New NotSupportedException("Cannot set filter from string expression.")
            'TODO: Re-instate this line once we have an expression filter
            'Filter = new ExpressionItemFilter<T>(value);
        End Set
    End Property

    ''' <summary> Gets or sets the filter currently applied to the view. </summary>
    ''' <value> The filter. </value>
    Public Property Filter() As IItemFilter(Of T)
        Get
            Return Me._Filter
        End Get
        Set(ByVal value As IItemFilter(Of T))
            ' Do not allow a null filter. Instead, use the "include all items" filter.
            If value Is Nothing Then
                value = IncludeAllItemFilter(Of T).Instance
            End If
            If Me._Filter IsNot value Then
                Me._Filter = value
                Me.FilterAndSort()
                ' The list has probably changed a lot, so get bound controls to reset.
                Me.OnListChanged(ListChangedType.Reset, -1)
            End If
        End Set
    End Property

    ''' <summary> Determine if we should serialize filter. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function ShouldSerializeFilter() As Boolean
        Return (Me.Filter IsNot IncludeAllItemFilter(Of T).Instance)
    End Function

    ''' <summary> Creates item filter. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="predicate"> The predicate. </param>
    ''' <returns> The new item filter. </returns>
    Public Shared Function CreateItemFilter(ByVal predicate As Predicate(Of T)) As IItemFilter(Of T)
        If predicate Is Nothing Then
            Throw New ArgumentNullException(NameOf(predicate))
        End If
        Return New PredicateItemFilter(Of T)(predicate)
    End Function

    ''' <summary>
    ''' Removes any currently applied filter so that all items are displayed by the view.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub RemoveFilter() Implements IBindingListView.RemoveFilter
        ' Set filter back to including all items.
        Me.Filter = IncludeAllItemFilter(Of T).Instance
    End Sub

#End Region

#Region " Sorting "

    ''' <summary>
    ''' Used to signal that a sort on a property is to be descending, not ascending.
    ''' </summary>
    ''' <value> The sort descending modifier. </value>
    Public ReadOnly Property SortDescendingModifier As String = "DESC"

    ''' <summary> The character used to separate sorts by multiple properties. </summary>
    ''' <value> The sort delimiter. </value>
    Public ReadOnly Property SortDelimiter As Char = ","c

    ''' <summary> Gets if this view supports sorting. Always returns true. </summary>
    ''' <value> The i binding list supports sorting. </value>
    Private ReadOnly Property IBindingList_SupportsSorting() As Boolean Implements IBindingList.SupportsSorting
        Get
            Return True
        End Get
    End Property

    ''' <summary> Gets if this view supports advanced sorting. Always returns true. </summary>
    ''' <value> The i binding list view supports advanced sorting. </value>
    Private ReadOnly Property IBindingListView_SupportsAdvancedSorting() As Boolean Implements IBindingListView.SupportsAdvancedSorting
        Get
            Return True
        End Get
    End Property

    ''' <summary>
    ''' Sorts the view by a single property in a given direction. This will remove any existing sort.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="[property]"> A property of <typeparamref name="T"/> to sort by. </param>
    ''' <param name="direction">  The direction to sort in. </param>
    Public Sub ApplySort(ByVal [property] As PropertyDescriptor, ByVal direction As ListSortDirection) Implements System.ComponentModel.IBindingList.ApplySort
        ' Apply sort by setting the current sort descriptions
        ' to be a collection containing just one SortDescription.
        Me.SortDescriptions = New ListSortDescriptionCollection(New ListSortDescription() {New ListSortDescription([property], direction)})
    End Sub

    ''' <summary> Sorts the view by the given collection of sort descriptions. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="sorts"> The sorts to apply. </param>
    Public Sub ApplySort(ByVal sorts As ListSortDescriptionCollection) Implements IBindingListView.ApplySort
        Me.SortDescriptions = sorts
    End Sub

    ''' <summary>
    ''' Sorts the view according to the properties and directions given in the SQL style sort
    ''' parameter.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="sort"> The SQL ORDER BY clause style sort. A comma separated list of properties
    '''                     to sort by. Use "DESC" after a property name to sort descending. The
    '''                     default direction is ascending. </param>
    ''' <example> <code>view.ApplySort("Surname, FirstName, Age DESC");</code> </example>
    Public Sub ApplySort(ByVal sort As String)
        If String.IsNullOrEmpty(sort) Then
            Me.RemoveSort()
            Return
        End If

        ' Parse string for sort descriptions
        Dim sorts() As String = sort.Split(Me.SortDelimiter)
        Dim col(sorts.Length - 1) As ListSortDescription
        For i As Integer = 0 To sorts.Length - 1
            ' Get the sort description.
            ' This will be a name optionally followed by a direction.
            sort = sorts(i).Trim()
            ' A space will separate name from direction.
            Dim pos As Integer = sort.IndexOf(" "c)
            Dim name As String
            Dim direction As ListSortDirection
            If pos = -1 Then
                ' No direction specified, default to ascending.
                name = sort
                direction = ListSortDirection.Ascending
            Else
                ' Name is everything before the space.
                name = sort.Substring(0, pos)
                ' direction is everything after the space.
                Dim dir As String = sort.Substring(pos + 1).Trim()
                ' Check what kind of direction is specified.
                ' (Ignoring case and culture.)
                If String.Compare(dir, Me.SortDescendingModifier, StringComparison.OrdinalIgnoreCase) = 0 Then
                    direction = ListSortDirection.Descending
                Else
                    ' Default to ascending.
                    direction = ListSortDirection.Ascending
                End If
            End If

            ' Put the sort description into the collection.
            col(i) = Me.CreateListSortDescription(name, direction)
        Next i

        Me.ApplySort(New ListSortDescriptionCollection(col))
    End Sub

    ''' <summary> Sorts the view by the given collection of sort descriptions. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="comparer"> The <see cref="System.Collections.Generic.IComparer">IComparer</see>
    '''                         used to compare items when sorting. </param>
    Public Sub ApplySort(ByVal comparer As IComparer(Of T))
        If comparer Is Nothing Then
            Throw New ArgumentNullException(NameOf(comparer))
        End If

        ' Clear any current sorts
        Me._Sorts = New ListSortDescriptionCollection()
        ' Sort with this new comparer
        Me._Comparer = New ExternalSortComparer(Of T)(comparer)
        Me.FilterAndSort()
        Me.OnListChanged(ListChangedType.Reset, -1)
    End Sub

    ''' <summary> Sorts the view by the given collection of sort descriptions. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="comparison"> The comparison. </param>
    Public Sub ApplySort(ByVal comparison As Comparison(Of T))
        If comparison Is Nothing Then Throw New ArgumentNullException(NameOf(comparison))

        ' Clear any current sorts
        Me._Sorts = New ListSortDescriptionCollection()
        ' Sort with this new comparer
        Me._Comparer = New ExternalSortComparison(Of T)(comparison)
        Me.FilterAndSort()
        Me.OnListChanged(ListChangedType.Reset, -1)
    End Sub

    ''' <summary>
    ''' Removes any sort currently applied to the view, restoring it to the order of the source list.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub RemoveSort() Implements System.ComponentModel.IBindingList.RemoveSort
        ' An empty collection of sorts will achieve what we need.
        Me.SortDescriptions = New ListSortDescriptionCollection()
    End Sub

    ''' <summary> Gets if the view is currently sorted. </summary>
    ''' <value> The is sorted. </value>
    <Browsable(False)>
    Public ReadOnly Property IsSorted() As Boolean Implements System.ComponentModel.IBindingList.IsSorted
        Get
            ' To be sorted there must be some sorts applied.
            Return (Me.SortDescriptions.Count > 0)
        End Get
    End Property

    ''' <summary>
    ''' Gets or sets the string representation of the sort currently applied to the view.
    ''' </summary>
    ''' <value> The sort. </value>
    Public Property Sort() As String
        Get
            If Me.IsSorted Then
                ' Build a string of the properties being sorted by
                Dim sb As New System.Text.StringBuilder()
                For Each sortDescription As ListSortDescription In Me.SortDescriptions
                    sb.Append(sortDescription.PropertyDescriptor.Name)
                    ' Need to signal descending sorts
                    If sortDescription.SortDirection = ListSortDirection.Descending Then
                        sb.Append(" "c).Append(Me.SortDescendingModifier)
                    End If
                    ' Separate by SortDelimiter
                    sb.Append(Me.SortDelimiter)
                Next sortDescription
                ' Remove trailing SortDelimiter
                sb.Remove(sb.Length - 1, 1)
                ' Return the string
                Return sb.ToString()
            End If
            Return String.Empty
        End Get
        Set(ByVal value As String)
            Me.ApplySort(value)
        End Set
    End Property

    ''' <summary> Determine if we should serialize sort. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function ShouldSerializeSort() As Boolean
        Return Not String.IsNullOrEmpty(Me.Sort)
    End Function

    ''' <summary>
    ''' Gets the direction in which the view is sorted. If more than one sort is applied, the
    ''' direction of the first is returned.
    ''' </summary>
    ''' <value> The sort direction. </value>
    <Browsable(False)>
    Public ReadOnly Property SortDirection() As ListSortDirection Implements System.ComponentModel.IBindingList.SortDirection
        Get
            If Me.IsSorted Then
                Return Me.SortDescriptions(0).SortDirection
            Else
                ' We don't really want to throw exceptions.
                ' Calling code should have checked IsSorted to know the true situation.
                Return ListSortDirection.Ascending
            End If
        End Get
    End Property

    ''' <summary>
    ''' Gets the property the view is currently sorted by. If more than one sort is applied, the
    ''' property of the first is returned.
    ''' </summary>
    ''' <value> The sort property. </value>
    <Browsable(False)>
    Public ReadOnly Property SortProperty() As PropertyDescriptor Implements System.ComponentModel.IBindingList.SortProperty
        Get
            If Me.IsSorted Then
                Return Me.SortDescriptions(0).PropertyDescriptor
            Else
                ' We don't really want to throw exceptions.
                ' Calling code should have checked IsSorted to know the true situation.
                Return Nothing
            End If
        End Get
    End Property

    ''' <summary> Gets or sets the sorts currently applied to the view. </summary>
    ''' <value> The sort descriptions. </value>
    <Browsable(False)>
    Public Property SortDescriptions() As ListSortDescriptionCollection Implements IBindingListView.SortDescriptions
        Get
            Return Me._Sorts
        End Get
        Private Set(ByVal value As ListSortDescriptionCollection)
            Me._Sorts = value
            Me._Comparer = New SortComparer(value)
            Me.FilterAndSort()
            ' Most of the list will have probably changed, so get bound objects to reset.
            Me.OnListChanged(ListChangedType.Reset, -1)
        End Set
    End Property

    ''' <summary>
    ''' Used to compare items in the view when sorting the _sourceIndices list. It supports multiple
    ''' sorts by different properties and directions.
    ''' </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 12/15/2018 </para>
    ''' </remarks>
    Private Class SortComparer
        Implements IComparer(Of KeyValuePair(Of ListItemPair(Of T), Integer))

        ''' <summary> The comparisons. </summary>
        ''' <value> The comparisons. </value>
        Private ReadOnly Property Comparisons As IDictionary(Of ListSortDescription, Comparison(Of T))

        ''' <summary> Creates a new SortComparer that will use the given sorts. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="sorts"> The sorts to apply to the view. </param>
        Public Sub New(ByVal sorts As ListSortDescriptionCollection)
            Me._Sorts = sorts

            ' Build the delegates used to compare properties of objects
            Me.Comparisons = New Dictionary(Of ListSortDescription, Comparison(Of T))()
            For Each sort As ListSortDescription In sorts
                Me.Comparisons(sort) = BuildComparison(sort.PropertyDescriptor.Name, sort.SortDirection)
            Next sort
        End Sub

        ''' <summary> The sorts. </summary>
        Private ReadOnly _Sorts As ListSortDescriptionCollection

        ''' <summary> Compares two items according to the defined sorts. </summary>
        ''' <remarks>
        ''' Use of light-weight code generation comparison delegates gives ~10x speed up compared to the
        ''' pure reflection based implementation.
        ''' </remarks>
        ''' <param name="x"> The first item to compare. </param>
        ''' <param name="y"> The second item to compare. </param>
        ''' <returns> -1 if x &lt; y, 0 if x = y and 1 if x &gt; y. </returns>
        Public Function Compare(ByVal x As KeyValuePair(Of ListItemPair(Of T), Integer), ByVal y As KeyValuePair(Of ListItemPair(Of T), Integer)) As Integer Implements IComparer(Of KeyValuePair(Of ListItemPair(Of T), Integer)).Compare
            For Each sort As ListSortDescription In Me._Sorts
                Dim result As Integer = Me.Comparisons(sort)(x.Key.Item.Object, y.Key.Item.Object)
                If result <> 0 Then
                    Return result
                End If
            Next sort
            Return 0
        End Function

        ''' <summary> Builds a comparison. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="propertyName"> Name of the property. </param>
        ''' <param name="direction">    The direction. </param>
        ''' <returns> A Comparison(Of T) </returns>
        Private Shared Function BuildComparison(ByVal propertyName As String, ByVal direction As ListSortDirection) As Comparison(Of T)
            Dim pi As PropertyInfo = GetType(T).GetProperty(propertyName)
            Debug.Assert(pi IsNot Nothing, String.Format("Property '{0}' is not a member of type '{1}'", propertyName, GetType(T).FullName))

            If GetType(IComparable).IsAssignableFrom(pi.PropertyType) Then
                If pi.PropertyType.IsValueType Then
                    Return BuildValueTypeComparison(pi, direction)
                Else
                    Dim getProperty As GetPropertyDelegate = BuildGetPropertyMethod(pi)
                    Return Function(x As T, y As T)
                               Dim result As Integer
                               Dim value1 As Object = getProperty(x)
                               Dim value2 As Object = getProperty(y)
                               If value1 IsNot Nothing AndAlso value2 IsNot Nothing Then
                                   result = (TryCast(value1, IComparable)).CompareTo(value2)
                               ElseIf value1 Is Nothing AndAlso value2 IsNot Nothing Then
                                   result = -1
                               ElseIf value1 IsNot Nothing AndAlso value2 Is Nothing Then
                                   result = 1
                               Else
                                   result = 0
                               End If

                               If direction = ListSortDirection.Descending Then
                                   result *= -1
                               End If
                               Return result
                           End Function
                End If
            ElseIf pi.PropertyType.IsGenericType AndAlso pi.PropertyType.GetGenericTypeDefinition().Equals(GetType(Nullable(Of ))) Then
                Return BuildNullableComparison(pi, direction)
            Else
                Return Function(o1 As T, o2 As T)
                           If o1.Equals(o2) Then
                               Return 0
                           Else
                               ' Return o1.ToString().CompareTo(o2.ToString())
                               Return String.Compare(o1.ToString, o2.ToString, StringComparison.Ordinal)
                           End If
                       End Function
            End If
        End Function

        ''' <summary> The delegate. </summary>
        Private Delegate Function GetPropertyDelegate(ByVal obj As T) As Object

        ''' <summary> Builds get property method. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="pi"> The pi. </param>
        ''' <returns> A GetPropertyDelegate. </returns>
        Private Shared Function BuildGetPropertyMethod(ByVal pi As PropertyInfo) As GetPropertyDelegate
            Dim getMethod As MethodInfo = pi.GetGetMethod()
            Debug.Assert(getMethod IsNot Nothing)

            Dim dm As New DynamicMethod("__blw_get_" & pi.Name, GetType(Object), New Type() {GetType(T)}, GetType(T), True)
            Dim il As ILGenerator = dm.GetILGenerator()

            il.Emit(OpCodes.Ldarg_0)
            il.EmitCall(OpCodes.Call, getMethod, Nothing)

            ' Return the result of the comparison.
            il.Emit(OpCodes.Ret)

            ' Create the delegate pointing at the dynamic method.
            Return CType(dm.CreateDelegate(GetType(GetPropertyDelegate)), GetPropertyDelegate)
        End Function

#Disable Warning IDE0051 ' Remove unused private members

        ''' <summary> Builds reference type comparison. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="pi">        The pi. </param>
        ''' <param name="direction"> The direction. </param>
        ''' <returns> A Comparison(Of T) </returns>
        Private Shared Function BuildRefTypeComparison(ByVal pi As PropertyInfo, ByVal direction As ListSortDirection) As Comparison(Of T)
#Enable Warning IDE0051 ' Remove unused private members
            Dim getMethod As MethodInfo = pi.GetGetMethod()
            Debug.Assert(getMethod IsNot Nothing)

            Dim dm As New DynamicMethod("Get" & pi.Name, GetType(Integer), New Type() {GetType(T), GetType(T)}, GetType(T), True)
            Dim il As ILGenerator = dm.GetILGenerator()

            ' Get the value of the first object's property.
            il.Emit(OpCodes.Ldarg_0)
            il.EmitCall(OpCodes.Call, getMethod, Nothing)

            ' Get the value of the second object's property.
            il.Emit(OpCodes.Ldarg_1)
            il.EmitCall(OpCodes.Call, getMethod, Nothing)

            ' Cast the first value to IComparable and call CompareTo,
            ' passing the second value as the argument.
            il.Emit(OpCodes.Castclass, GetType(IComparable))
            il.EmitCall(OpCodes.Call, GetType(IComparable).GetMethod("CompareTo"), Nothing)

            ' If descending then multiply comparison result by -1
            ' to reverse the ordering.
            If direction = ListSortDirection.Descending Then
                il.Emit(OpCodes.Ldc_I4_M1)
                il.Emit(OpCodes.Mul)
            End If

            ' Return the result of the comparison.
            il.Emit(OpCodes.Ret)

            ' Create the delegate pointing at the dynamic method.
            Return CType(dm.CreateDelegate(GetType(Comparison(Of T))), Comparison(Of T))
        End Function

        ''' <summary> Builds value type comparison. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="pi">        The pi. </param>
        ''' <param name="direction"> The direction. </param>
        ''' <returns> A Comparison(Of T) </returns>
        Private Shared Function BuildValueTypeComparison(ByVal pi As PropertyInfo, ByVal direction As ListSortDirection) As Comparison(Of T)
            Dim getMethod As MethodInfo = pi.GetGetMethod()
            Debug.Assert(getMethod IsNot Nothing)

            Dim dm As New DynamicMethod("Get" & pi.Name, GetType(Integer), New Type() {GetType(T), GetType(T)}, GetType(T), True)
            Dim il As ILGenerator = dm.GetILGenerator()

            ' Get the value of the first object's property.
            il.Emit(OpCodes.Ldarg_0)
            il.EmitCall(OpCodes.Call, getMethod, Nothing)
            ' Box the value type
            il.Emit(OpCodes.Box, pi.PropertyType)

            ' Get the value of the second object's property.
            il.Emit(OpCodes.Ldarg_1)
            il.EmitCall(OpCodes.Call, getMethod, Nothing)
            ' Box the value type
            il.Emit(OpCodes.Box, pi.PropertyType)

            ' Cast the first value to IComparable and call CompareTo,
            ' passing the second value as the argument.
            il.Emit(OpCodes.Castclass, GetType(IComparable))
            il.EmitCall(OpCodes.Call, GetType(IComparable).GetMethod("CompareTo"), Nothing)

            ' If descending then multiply comparison result by -1
            ' to reverse the ordering.
            If direction = ListSortDirection.Descending Then
                il.Emit(OpCodes.Ldc_I4_M1)
                il.Emit(OpCodes.Mul)
            End If

            ' Return the result of the comparison.
            il.Emit(OpCodes.Ret)

            ' Create the delegate pointing at the dynamic method.
            Return CType(dm.CreateDelegate(GetType(Comparison(Of T))), Comparison(Of T))
        End Function

        ''' <summary> Builds nullable comparison. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="pi">        The pi. </param>
        ''' <param name="direction"> The direction. </param>
        ''' <returns> A Comparison(Of T) </returns>
        Private Shared Function BuildNullableComparison(ByVal pi As PropertyInfo, ByVal direction As ListSortDirection) As Comparison(Of T)
            Dim getMethod As MethodInfo = pi.GetGetMethod()
            Debug.Assert(getMethod IsNot Nothing)

            'Type nullableType = type of(Nullable<>).MakeGenericType(pi.PropertyType.GetGenericArguments()[0]);

            Dim dm As New DynamicMethod("Get" & pi.Name, GetType(Integer), New Type() {GetType(T), GetType(T)}, GetType(T), True)
            Dim il As ILGenerator = dm.GetILGenerator()

            ' Get the value of the first object's property.
            il.Emit(OpCodes.Ldarg_0)
            il.EmitCall(OpCodes.Call, getMethod, Nothing)

            ' Get the value of the second object's property.
            il.Emit(OpCodes.Ldarg_1)
            il.EmitCall(OpCodes.Call, getMethod, Nothing)

            ' Call Nullable.Compare
            il.EmitCall(OpCodes.Call, GetType(Nullable).GetMethod("Compare", BindingFlags.Static Or BindingFlags.Public).MakeGenericMethod(pi.PropertyType.GetGenericArguments()(0)), Nothing)

            ' If descending then multiply comparison result by -1
            ' to reverse the ordering.
            If direction = ListSortDirection.Descending Then
                il.Emit(OpCodes.Ldc_I4_M1)
                il.Emit(OpCodes.Mul)
            End If

            ' Return the result of the comparison.
            il.Emit(OpCodes.Ret)

            ' Create the delegate pointing at the dynamic method.
            Return CType(dm.CreateDelegate(GetType(Comparison(Of T))), Comparison(Of T))
        End Function
    End Class

    ''' <summary> An u. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 12/15/2018 </para>
    ''' </remarks>
    Private Class ExternalSortComparer(Of TCompared)
        Implements IComparer(Of KeyValuePair(Of ListItemPair(Of TCompared), Integer))

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="comparer"> The comparer. </param>
        Public Sub New(ByVal comparer As IComparer(Of TCompared))
            Me._Comparer = comparer
        End Sub

        ''' <summary> The comparer. </summary>
        Private ReadOnly _Comparer As IComparer(Of TCompared)

        ''' <summary>
        ''' Compares two objects and returns a value indicating whether one is less than, equal to, or
        ''' greater than the other.
        ''' </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="x"> The first object to compare. </param>
        ''' <param name="y"> The second object to compare. </param>
        ''' <returns>
        ''' A signed integer that indicates the relative values of <paramref name="x" /> and
        ''' <paramref name="y" />, as shown in the following table.Value Meaning Less than
        ''' zero<paramref name="x" /> is less than <paramref name="y" />.Zero<paramref name="x" /> equals
        ''' <paramref name="y" />.Greater than zero<paramref name="x" /> is greater than
        ''' <paramref name="y" />.
        ''' </returns>
        Public Function Compare(ByVal x As KeyValuePair(Of ListItemPair(Of TCompared), Integer), ByVal y As KeyValuePair(Of ListItemPair(Of TCompared), Integer)) As Integer Implements IComparer(Of KeyValuePair(Of ListItemPair(Of TCompared), Integer)).Compare
            Return Me._Comparer.Compare(x.Key.Item.Object, y.Key.Item.Object)
        End Function
    End Class

    ''' <summary> An u. </summary>
    ''' <remarks>
    ''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 12/15/2018 </para>
    ''' </remarks>
    Private Class ExternalSortComparison(Of TCompared)
        Implements IComparer(Of KeyValuePair(Of ListItemPair(Of TCompared), Integer))

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="comparison"> The comparison. </param>
        Public Sub New(ByVal comparison As Comparison(Of TCompared))
            Me._Comparison = comparison
        End Sub

        ''' <summary> The comparison. </summary>
        Private ReadOnly _Comparison As Comparison(Of TCompared)

        ''' <summary>
        ''' Compares two objects and returns a value indicating whether one is less than, equal to, or
        ''' greater than the other.
        ''' </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="x"> The first object to compare. </param>
        ''' <param name="y"> The second object to compare. </param>
        ''' <returns>
        ''' A signed integer that indicates the relative values of <paramref name="x" /> and
        ''' <paramref name="y" />, as shown in the following table.Value Meaning Less than
        ''' zero<paramref name="x" /> is less than <paramref name="y" />.Zero<paramref name="x" /> equals
        ''' <paramref name="y" />.Greater than zero<paramref name="x" /> is greater than
        ''' <paramref name="y" />.
        ''' </returns>
        Public Function Compare(ByVal x As KeyValuePair(Of ListItemPair(Of TCompared), Integer), ByVal y As KeyValuePair(Of ListItemPair(Of TCompared), Integer)) As Integer Implements IComparer(Of KeyValuePair(Of ListItemPair(Of TCompared), Integer)).Compare
            Return Me._Comparison(x.Key.Item.Object, y.Key.Item.Object)
        End Function
    End Class

#End Region

#Region "Searching"

    ''' <summary>
    ''' Gets if this view supports searching using the Find method. Always returns true.
    ''' </summary>
    ''' <value> The i binding list supports searching. </value>
    Private ReadOnly Property IBindingList_SupportsSearching() As Boolean Implements IBindingList.SupportsSearching
        Get
            Return True
        End Get
    End Property

    ''' <summary>
    ''' Returns the index of the first item in the view who's property equals the given value.
    ''' -1 is returned if no item is found.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="[property]"> The property of each item to check. </param>
    ''' <param name="key">        The value being sought. </param>
    ''' <returns> The index of the item, or -1 if not found. </returns>
    Public Function Find(ByVal [property] As PropertyDescriptor, ByVal key As Object) As Integer Implements System.ComponentModel.IBindingList.Find
        Dim result As Integer = -1
        If [property] IsNot Nothing Then
            For i As Integer = 0 To Me._SourceIndices.Count - 1
                If [property].GetValue(Me._SourceIndices(i).Key.Item.Object).Equals(key) Then
                    result = i
                    Exit For
                End If
            Next i
        End If
        Return result
    End Function

    ''' <summary>
    ''' Returns the index of the first item in the view who's property equals the given value.
    ''' -1 is returned if no item is found.
    ''' </summary>
    ''' <remarks>
    ''' It is easier for users of this class to enter a property name and get the PropertyDescriptor
    ''' ourselves.
    ''' </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="propertyName"> The name of the property of each item to check. </param>
    ''' <param name="key">          The value being sought. </param>
    ''' <returns> The index of the item, or -1 if not found. </returns>
    Public Function Find(ByVal propertyName As String, ByVal key As Object) As Integer
        Dim pd As PropertyDescriptor = GetPropertyDescriptor(propertyName)
        If pd IsNot Nothing Then
            Return Me.Find(pd, key)
        Else
            Throw New ArgumentException(String.Format(My.Resources.PropertyNotFound, propertyName, GetType(T).FullName), NameOf(propertyName))
        End If
    End Function

#End Region

#Region "IBindingList Members"

    ''' <summary> Gets if this view raises the ListChanged event. Always returns true. </summary>
    ''' <value> The i binding list supports change notification. </value>
    Private ReadOnly Property IBindingList_SupportsChangeNotification() As Boolean Implements IBindingList.SupportsChangeNotification
        Get
            Return True
        End Get
    End Property

    ''' <summary> Binding list add new. </summary>
    ''' <remarks> Explicitly implemented so the type safe AddNew method is exposed instead. </remarks>
    ''' <returns> An Object. </returns>
    Private Function IBindingList_AddNew() As Object Implements IBindingList.AddNew
        Return Me.AddNew()
    End Function

    ''' <summary> Gets if this view allows items to be edited. </summary>
    ''' <remarks> Delegates to the source list. </remarks>
    ''' <value> The i binding list allow edit. </value>
    Private ReadOnly Property IBindingList_AllowEdit() As Boolean Implements IBindingList.AllowEdit
        Get
            For Each list As Object In Me.SourceLists
                If TypeOf list Is IBindingList Then
                    If Not (TryCast(list, IBindingList)).AllowEdit Then
                        Return False
                    End If
                End If
            Next list
            Return True
        End Get
    End Property

    ''' <summary> Gets if this view allows new items to be added using AddNew(). </summary>
    ''' <remarks> Delegates to the source list. </remarks>
    ''' <value> The i binding list allow new. </value>
    Private ReadOnly Property IBindingList_AllowNew() As Boolean Implements IBindingList.AllowNew
        Get
            If Me._NewItemsList IsNot Nothing Then
                If TypeOf Me._NewItemsList Is IBindingList Then
                    ' Respect what the binding list says.
                    Return (TryCast(Me._NewItemsList, IBindingList)).AllowNew
                End If
                ' _newItemsList is a IList, so we can call Add()
                ' it may fail at runtime - but that is the callee's problem
                Return True
            End If
            Return False
        End Get
    End Property

    ''' <summary> Gets if this view allows items to be removed. </summary>
    ''' <remarks> Delegates to the source list. </remarks>
    ''' <value> The i binding list allow remove. </value>
    Private ReadOnly Property IBindingList_AllowRemove() As Boolean Implements IBindingList.AllowRemove
        Get
            For Each list As Object In Me.SourceLists
                If TypeOf list Is IBindingList Then
                    If Not (TryCast(list, IBindingList)).AllowRemove Then
                        Return False
                    End If
                End If
            Next list
            Return True
        End Get
    End Property

    ''' <summary> Not implemented. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="[property]"> The [property]. </param>
    Private Sub IBindingList_AddIndex(ByVal [property] As PropertyDescriptor) Implements IBindingList.AddIndex
        Throw New NotImplementedException()
    End Sub

    ''' <summary> Not implemented. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="NotImplementedException"> Thrown when the requested operation is
    '''                                            unimplemented. </exception>
    ''' <param name="[property]"> The [property]. </param>
    Private Sub IBindingList_RemoveIndex(ByVal [property] As PropertyDescriptor) Implements IBindingList.RemoveIndex
        Throw New NotImplementedException()
    End Sub

#End Region

#Region "IRaiseItemChangedEvents Members"

    ''' <summary>
    ''' Gets if this view raises the ListChanged event when an item changes. Always returns true.
    ''' </summary>
    ''' <value> The raises item changed events. </value>
    <Browsable(False)>
    Public ReadOnly Property RaisesItemChangedEvents() As Boolean Implements IRaiseItemChangedEvents.RaisesItemChangedEvents
        Get
            Return True
        End Get
    End Property

#End Region

#Region " IList Members "

    ''' <summary> List add. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> Either an ObjectView(Of T) or T to remove. </param>
    ''' <returns> An Integer. </returns>
    Private Function IList_Add(ByVal value As Object) As Integer Implements IList.Add
        If value IsNot Nothing Then
            Me.AddNew()
        End If
        Return Me.Count - 1
    End Function

    ''' <summary> Cannot clear this view. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    Private Sub IList_Clear() Implements IList.Clear
        Throw New NotSupportedException(My.Resources.CannotClearView)
    End Sub

    ''' <summary>
    ''' Checks if this view contains the given item. Note that items excluded by current filter are
    ''' not searched.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="item"> The item to search for. </param>
    ''' <returns> True if the item is in the view, else false. </returns>
    Private Function IList_Contains(ByVal item As Object) As Boolean Implements IList.Contains
        ' See if the source indexes contain the item
        Dim value As ObjectView(Of T) = TryCast(item, ObjectView(Of T))
        If value IsNot Nothing Then
            Return Me._SourceIndices.ContainsKey(value)
        ElseIf TypeOf item Is T Then
            Return Me._SourceIndices.ContainsItem(CType(item, T))
        Else
            Return False
        End If
    End Function

    ''' <summary> Gets the index in the view of an item. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="item"> The item to search for. </param>
    ''' <returns> The index of the item, or -1 if not found. </returns>
    Private Function IList_IndexOf(ByVal item As Object) As Integer Implements IList.IndexOf
        If TypeOf item Is ObjectView(Of T) Then
            Return Me._SourceIndices.IndexOfKey(TryCast(item, ObjectView(Of T)))
        ElseIf TypeOf item Is T Then
            Return Me._SourceIndices.IndexOfItem(CType(item, T))
        End If
        Return -1
    End Function

    ''' <summary> Cannot insert an external item into this collection. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    ''' <param name="index"> The index of the item to remove. </param>
    ''' <param name="value"> Either an ObjectView(Of T) or T to remove. </param>
    Private Sub IList_Insert(ByVal index As Integer, ByVal value As Object) Implements IList.Insert
        Throw New NotSupportedException(My.Resources.CannotInsertItem)
    End Sub

    ''' <summary> Gets a value indicating if this view is read-only. </summary>
    ''' <remarks> Delegates to the source list. </remarks>
    ''' <value> The i list is read only. </value>
    Private ReadOnly Property IList_IsReadOnly() As Boolean Implements IList.IsReadOnly
        Get
            For Each list As Object In Me.SourceLists
                If TypeOf list Is IBindingList Then
                    If Not (TryCast(list, IBindingList)).IsReadOnly Then
                        Return False
                    End If
                Else
                    Return False
                End If
            Next list
            Return True
        End Get
    End Property

    ''' <summary>
    ''' Always returns <code>false</code> because the view can change size when source lists are
    ''' added.
    ''' </summary>
    ''' <value> The size of the list is fixed. </value>
    Private ReadOnly Property IList_IsFixedSize() As Boolean Implements IList.IsFixedSize
        Get
            Return False
        End Get
    End Property

    ''' <summary> Removes the given item from the view and underlying source list. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> Either an ObjectView(Of T) or T to remove. </param>
    Private Sub IList_Remove(ByVal value As Object) Implements IList.Remove
        Dim index As Integer = (TryCast(Me, IList)).IndexOf(value)
        TryCast(Me, IList).RemoveAt(index)
    End Sub

    ''' <summary> Removes the item from the view at the given index. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="index"> The index of the item to remove. </param>
    Private Sub IList_RemoveAt(ByVal index As Integer) Implements IList.RemoveAt
        ' Get the index in the source list.
        Dim sourceIndex As Integer = Me._SourceIndices(index).Value
        Dim sourceList As IList = Me._SourceIndices(index).Key.List
        If sourceIndex > -1 Then
            sourceList.RemoveAt(sourceIndex)
            If Not (TypeOf sourceList Is IBindingList) OrElse Not (TryCast(sourceList, IBindingList)).SupportsChangeNotification Then
                Me.FilterAndSort()
                Me.OnListChanged(ListChangedType.Reset, -1)
            End If
        Else
            ' The item is not in the source list yet as it is new
            ' So cancel the new operation instead.
            Me.CancelNew(index)
        End If
    End Sub

    ''' <summary> Gets or sets the <see cref="ObjectView(Of T)"/> at the given index. </summary>
    ''' <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    ''' <value> An <see cref="ObjectView(Of T)"/> object. </value>
    Public Property IListItem(ByVal index As Integer) As Object Implements IList.Item
        Get
            Return Me(index)
        End Get
        Set(ByVal value As Object)
            ' The interface requires we supply a setter
            ' But we don't want external code modifying the view
            ' in this manner.
            Throw New NotSupportedException(My.Resources.CannotSetItem)
        End Set
    End Property

#End Region

#Region "ICollection Members"

    ''' <summary>
    ''' Copies the <see cref="ObjectView(Of T)"/> objects of the view to an
    ''' <see cref="Array"/>, starting at a particular System.Array index.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="array"> The one-dimensional <see cref="Array"/> that is the destination of the
    '''                      elements copied from view. The System.Array must have zero-based
    '''                      indexing. </param>
    ''' <param name="index"> The zero-based index in array at which copying begins. </param>
    Private Sub ICollection_CopyTo(ByVal array As Array, ByVal index As Integer) Implements ICollection.CopyTo
        Me._SourceIndices.Keys.CopyTo(array, index)
    End Sub

    ''' <summary>
    ''' Gets a value indicating whether access to the <see cref="ICollection" />
    ''' is synchronized (thread safe).
    ''' </summary>
    ''' <value> The i collection is synchronized. </value>
    Private ReadOnly Property ICollection_IsSynchronized() As Boolean Implements ICollection.IsSynchronized
        Get
            Return False
        End Get
    End Property

    ''' <summary> Not supported. </summary>
    ''' <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    ''' <value> The i collection synchronization root. </value>
    Private ReadOnly Property ICollection_SyncRoot() As Object Implements ICollection.SyncRoot
        Get
            Throw New NotSupportedException(My.Resources.SyncAccessNotSupported)
        End Get
    End Property

    ''' <summary>
    ''' Gets the number of items currently in the view. This does not include those items excluded by
    ''' the current filter.
    ''' </summary>
    ''' <value> The count. </value>
    <Browsable(False)>
    Public ReadOnly Property Count() As Integer Implements System.Collections.ICollection.Count
        Get
            Return Me._SourceIndices.Count
        End Get
    End Property

#End Region

#Region "IEnumerable Members"

    ''' <summary>
    ''' Returns an enumerator that iterates through all the <see cref="ObjectView(Of T)"/> items in
    ''' the view. This does not include those items excluded by the current filter.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> An IEnumerator to iterate with. </returns>
    Private Function IEnumerable_GetEnumerator() As IEnumerator Implements IEnumerable.GetEnumerator
        Return Me._SourceIndices.GetKeyEnumerator()
    End Function

#End Region

#Region "ITypedList Members"

    ''' <summary>
    ''' Returns the <see cref="PropertyDescriptorCollection"/> that represents the properties on each
    ''' item used to bind data.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="listAccessors"> Array of property descriptors to navigate object hirerachy to
    '''                              actual item object. It can be null. </param>
    ''' <returns>
    ''' The System.ComponentModel.PropertyDescriptorCollection that represents the properties on each
    ''' item used to bind data.
    ''' </returns>
    Private Function ITypedList_GetItemProperties(ByVal listAccessors() As PropertyDescriptor) As PropertyDescriptorCollection Implements ITypedList.GetItemProperties
        Dim originalProps As PropertyDescriptorCollection

        Dim lists As IEnumerator(Of IList) = Me.EnumerateSourceLists().GetEnumerator()

        If lists.MoveNext() AndAlso TypeOf lists.Current Is ITypedList Then
            ' Ask the source list for the properties.
            originalProps = (TryCast(lists.Current, ITypedList)).GetItemProperties(listAccessors)
        Else
            ' Get the properties ourself.
            originalProps = isr.Core.Controls.PropertyDescriptorExtensions.Methods.GetListItemProperties(listAccessors, GetType(T))
        End If

        If listAccessors IsNot Nothing AndAlso listAccessors.Length > 0 Then
            Dim type As Type = originalProps(0).ComponentType
            If type.IsGenericType AndAlso type.GetGenericTypeDefinition() Is GetType(ObjectView(Of )) Then
                originalProps = originalProps(0).GetChildProperties()
            End If
        End If

        Dim newProps As New List(Of PropertyDescriptor)()
        For Each pd As PropertyDescriptor In originalProps
            newProps.Add(pd)
        Next pd
        For Each pd As PropertyDescriptor In Me.GetProvidedViews(originalProps)
            newProps.Add(pd)
        Next pd
        Return New PropertyDescriptorCollection(newProps.ToArray())
    End Function

    ''' <summary> Determine if we should provide view. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="[property]"> The [property]. </param>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Protected Friend Function ShouldProvideView(ByVal [property] As PropertyDescriptor) As Boolean
        Return ProvidedViewPropertyDescriptor(Of T).CanProvideViewOf([property])
    End Function

    ''' <summary> Gets provided view name. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="sourceListProperty"> Source list property. </param>
    ''' <returns> The provided view name. </returns>
    Protected Friend Function GetProvidedViewName(ByVal sourceListProperty As MemberDescriptor) As String
        Return $"{sourceListProperty?.Name}View"
    End Function

    ''' <summary> Creates provided view. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="[object]">           The [object]. </param>
    ''' <param name="sourceListProperty"> Source list property. </param>
    ''' <returns> The new provided view. </returns>
    Protected Friend Function CreateProvidedView(ByVal [object] As ObjectView(Of T), ByVal sourceListProperty As PropertyDescriptor) As Object
        If sourceListProperty Is Nothing Then Throw New ArgumentNullException(NameOf(sourceListProperty))
        Dim list As Object = sourceListProperty.GetValue([object])
        Dim viewType As Type = GetProvidedViewType(sourceListProperty)
        Return Activator.CreateInstance(viewType, list)
    End Function

    ''' <summary> Gets provided view type. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="sourceListProperty"> Source list property. </param>
    ''' <returns> The provided view type. </returns>
    Private Shared Function GetProvidedViewType(ByVal sourceListProperty As PropertyDescriptor) As Type
        Dim viewTypeDef As Type = GetType(BindingListView(Of Object)).GetGenericTypeDefinition()
        Dim typeParam As Type = sourceListProperty.PropertyType.GetGenericArguments()(0)
        Dim viewType As Type = viewTypeDef.MakeGenericType(typeParam)
        Return viewType
    End Function

    ''' <summary> Gets the provided views in this collection. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="properties"> The properties. </param>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process the provided views in this collection.
    ''' </returns>
    Private Iterator Function GetProvidedViews(ByVal properties As PropertyDescriptorCollection) As IEnumerable(Of PropertyDescriptor)
        For i As Integer = 0 To properties.Count - 1
            If Me.ShouldProvideView(properties(i)) Then
                Dim name As String = Me.GetProvidedViewName(properties(i))
                Yield New ProvidedViewPropertyDescriptor(Of T)(name, GetProvidedViewType(properties(i)))
            End If
        Next i
    End Function

    ''' <summary> Gets the name of the view. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="listAccessors"> Unused. Can be null. </param>
    ''' <returns> The name of the view. </returns>
    Private Function ITypedList_GetListName(ByVal listAccessors() As PropertyDescriptor) As String Implements ITypedList.GetListName
        Return Me.GetType().Name
    End Function

#End Region

#Region "Helper Methods"

    ''' <summary>
    ''' Creates a new <see cref="System.ComponentModel.ListSortDescription"/> for given property name
    ''' and sort direction.
    ''' </summary>
    ''' <remarks> Used by external code to simplify sorting the view. </remarks>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <param name="propertyName"> The name of the property to sort by. </param>
    ''' <param name="direction">    The direction in which to sort. </param>
    ''' <returns> A ListSortDescription. </returns>
    Public Function CreateListSortDescription(ByVal propertyName As String, ByVal direction As ListSortDirection) As ListSortDescription
        Dim pd As PropertyDescriptor = AggregateBindingListView(Of T).GetPropertyDescriptor(propertyName)
        If pd Is Nothing Then
            Throw New ArgumentException(String.Format(My.Resources.PropertyNotFound, propertyName, GetType(T).FullName), NameOf(propertyName))
        End If
        Return New ListSortDescription(pd, direction)
    End Function

    ''' <summary> Gets the property descriptor for a given property name. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="propertyName"> The name of a property of <typeparamref name="T"/>. </param>
    ''' <returns> The <see cref="System.ComponentModel.PropertyDescriptor"/>. </returns>
    Private Shared Function GetPropertyDescriptor(ByVal propertyName As String) As PropertyDescriptor
        Return TypeDescriptor.GetProperties(GetType(T)).Find(propertyName, False)
    End Function

    ''' <summary>
    ''' Attaches event handlers to the given <see cref="ObjectView(Of T)"/>'s edit life cycle
    ''' notification events.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="editableObject"> The <see cref="ObjectView(Of T)"/> to listen to. </param>
    Private Sub HookEditableObjectEvents(ByVal editableObject As ObjectView(Of T))
        AddHandler editableObject.EditBegun, AddressOf Me.OnItemEditBegun
        AddHandler editableObject.EditCanceled, AddressOf Me.OnItemEditCanceled
        AddHandler editableObject.EditEnded, AddressOf Me.OnItemEditEnded
    End Sub

    ''' <summary>
    ''' Detaches event handlers from the given <see cref="ObjectView(Of T)"/>'s edit life cycle
    ''' notification events.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="editableObject"> The <see cref="ObjectView(Of T)"/> to stop listening to. </param>
    Private Sub UnHookEditableObjectEvents(ByVal editableObject As ObjectView(Of T))
        RemoveHandler editableObject.EditBegun, AddressOf Me.OnItemEditBegun
        RemoveHandler editableObject.EditCanceled, AddressOf Me.OnItemEditCanceled
        RemoveHandler editableObject.EditEnded, AddressOf Me.OnItemEditEnded
    End Sub

    ''' <summary>
    ''' Attaches an event handler to the <see cref="ObjectView(Of T)"/>'s PropertyChanged event.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="editableObject"> The <see cref="ObjectView(Of T)"/> to listen to. </param>
    Private Sub HookPropertyChangedEvent(ByVal editableObject As ObjectView(Of T))
        AddHandler editableObject.PropertyChanged, AddressOf Me.ItemPropertyChanged
    End Sub

    ''' <summary>
    ''' Detaches the event handler from the <see cref="ObjectView(Of T)"/>'s PropertyChanged event.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="editableObject"> The <see cref="ObjectView(Of T)"/> to stop listening to. </param>
    Private Sub UnHookPropertyChangedEvent(ByVal editableObject As ObjectView(Of T))
        RemoveHandler editableObject.PropertyChanged, AddressOf Me.ItemPropertyChanged
    End Sub

    ''' <summary> Builds saved list. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Private Sub BuildSavedList()
        Me._SavedSourceLists.Clear()
        For Each list As Object In Me.EnumerateSourceLists()
            Me._SavedSourceLists.Add(TryCast(list, IList))
        Next list
    End Sub

    ''' <summary> Gets the source lists in this collection. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process the source lists in this collection.
    ''' </returns>
    Protected Iterator Function EnumerateSourceLists() As IEnumerable(Of IList)
        For Each obj As Object In Me._SourceLists
            If Not String.IsNullOrEmpty(Me.DataMember) Then
                Dim found As Boolean = False
                For Each pd As PropertyDescriptor In TypeDescriptor.GetProperties(obj)
                    If pd.Name = Me.DataMember Then
                        found = True
                        Yield TryCast(pd.GetValue(obj), IList)
                        Exit For
                    End If
                Next pd
                If Not found Then
                    Yield Nothing
                End If
            ElseIf TypeOf obj Is IListSource Then
                Dim src As IListSource = TryCast(obj, IListSource)
                If src.ContainsListCollection Then
                    Dim list As IList = TryCast(src.GetList(), IList)
                    If list IsNot Nothing AndAlso list.Count > 0 Then
                        list = TryCast(list(0), IList)
                        Yield list
                    Else
                        Yield Nothing
                    End If
                Else
                    Yield src.GetList()
                End If
            Else
                Yield TryCast(obj, IList)
            End If
        Next obj
    End Function

#End Region

End Class
