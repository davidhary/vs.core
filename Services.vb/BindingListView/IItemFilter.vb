''' <summary>
''' Defines a general method to test it an item should be included in a
''' <see cref="BindingListView(Of T)"/>.
''' </summary>
''' <remarks>
''' <typeparam name="T"> The type of item to be filtered. </typeparam> <para>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.</para><para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface IItemFilter(Of T)

    ''' <summary> Tests if the item should be included. </summary>
    ''' <param name="item"> The item to test. </param>
    ''' <returns> True if the item should be included, otherwise false. </returns>
    Function Include(ByVal item As T) As Boolean
End Interface

''' <summary>
''' A dummy filter that is used when no filter is needed. It simply includes any and all items
''' tested.
''' </summary>
''' <remarks>
''' (c) 2006 Andrew Davey. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/15/2018, </para><para>
''' David, 12/15/2018, 1.2.*. https://blogs.warwick.ac.uk/andrewdavey and
''' https://sourceforge.net/projects/blw/. </para>
''' </remarks>
Public Class IncludeAllItemFilter(Of T)
    Implements IItemFilter(Of T)

    ''' <summary> Tests if the item should be included. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="item"> The item to test. </param>
    ''' <returns> True if the item should be included, otherwise false. </returns>
    Public Function Include(ByVal item As T) As Boolean Implements IItemFilter(Of T).Include
        ' All items are to be included.
        ' So always return true.
        Return True
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return My.Resources.NoFilter
    End Function

#Region " Singleton Accessor "

    ''' <summary> The instance. </summary>
    Private Shared _Instance As IncludeAllItemFilter(Of T)

    ''' <summary> Gets the singleton instance of <see cref="IncludeAllItemFilter(Of T)"/>. </summary>
    ''' <value> The instance. </value>
    Public Shared ReadOnly Property Instance() As IncludeAllItemFilter(Of T)
        Get
            If _Instance Is Nothing Then
                _Instance = New IncludeAllItemFilter(Of T)()
            End If
            Return _Instance
        End Get
    End Property

#End Region

End Class

''' <summary>
''' A filter that uses a user-defined <see cref="Predicate(Of T)"/> to test items for inclusion
''' in <see cref="BindingListView(Of T)"/>.
''' </summary>
''' <remarks>
''' David, 12/15/2018, 1.2.*. https://blogs.warwick.ac.uk/andrewdavey and <para>
''' https://sourceforge.net/projects/blw/. </para><para>
''' (c) 2006 Andrew Davey. All rights reserved.</para><para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class PredicateItemFilter(Of T)
    Implements IItemFilter(Of T)

    ''' <summary>
    ''' Creates a new <see cref="PredicateItemFilter(Of T)"/> that uses the specified
    ''' <see cref="Predicate(Of T)"/> and default name.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="includeDelegate"> The <see cref="Predicate(Of T)"/> used to test items. </param>
    Public Sub New(ByVal includeDelegate As Predicate(Of T))
        Me.New(includeDelegate, Nothing)
        ' The other constructor is called to do the work.
    End Sub

    ''' <summary>
    ''' Creates a new <see cref="PredicateItemFilter(Of T)"/> that uses the specified
    ''' <see cref="Predicate(Of T)"/>.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="includeDelegate"> The <see cref="PredicateItemFilter(Of T)"/> used to test
    '''                                items. </param>
    ''' <param name="name">            The name used for the ToString() return value. </param>
    Public Sub New(ByVal includeDelegate As Predicate(Of T), ByVal name As String)
        ' We don't allow a null string. Use the default instead.
        Me._Name = If(name, Me._DefaultName)
        If includeDelegate IsNot Nothing Then
            Me._IncludeDelegate = includeDelegate
        Else
            Throw New ArgumentNullException(NameOf(includeDelegate), My.Resources.IncludeDelegateCannotBeNull)
        End If
    End Sub

    ''' <summary> The include delegate. </summary>
    Private ReadOnly _IncludeDelegate As Predicate(Of T)

    ''' <summary> The name. </summary>
    Private ReadOnly _Name As String

    ''' <summary> The default name. </summary>
    Private ReadOnly _DefaultName As String = My.Resources.PredicateFilter

    ''' <summary> Tests if the item should be included. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="item"> The item to test. </param>
    ''' <returns> True if the item should be included, otherwise false. </returns>
    Public Function Include(ByVal item As T) As Boolean Implements IItemFilter(Of T).Include
        Return Me._IncludeDelegate(item)
    End Function

    ''' <summary> Returns a string that represents the current object. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> A string that represents the current object. </returns>
    Public Overrides Function ToString() As String
        Return Me._Name
    End Function
End Class

