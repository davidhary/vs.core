''' <summary> Exception for signaling invalid source list errors. </summary>
''' <remarks>
''' (c) 2006 Andrew Davey. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/15/2018, 1.2. </para><para>
'''  https://blogs.warwick.ac.uk/andrewdavey and </para><para>
''' https://sourceforge.net/projects/blw/. </para>
''' </remarks>
<Serializable>
Public Class InvalidSourceListException
    Inherits Exception

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub New()
        MyBase.New(My.Resources.InvalidSourceList)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="message"> The message. </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="T:System.Exception" /> class with a specified
    ''' error message and a reference to the inner exception that is the cause of this exception.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="message">        The error message that explains the reason for the exception. </param>
    ''' <param name="innerException"> The exception that is the cause of the current exception, or a
    '''                               null reference (<see langword="Nothing" /> in Visual Basic)
    '''                               if no inner exception is specified. </param>
    Public Sub New(ByVal message As String, ByVal innerException As Exception)
        MyBase.New(message, innerException)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="info">    The information. </param>
    ''' <param name="context"> The context. </param>
    Protected Sub New(ByVal info As System.Runtime.Serialization.SerializationInfo, ByVal context As System.Runtime.Serialization.StreamingContext)
        MyBase.New(info, context)
    End Sub
End Class
