Imports System.ComponentModel

Partial Public Class AggregateBindingListView(Of T)

#Region " Adding New event handler "

    ''' <summary> Removes the Adding New event handlers. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Protected Sub RemoveAddingNewEventHandler()
        Me._AddingNewHandlers.RemoveAll()
    End Sub
    ''' <summary> The adding new handlers. </summary>
    Private ReadOnly _AddingNewHandlers As New NotifyAddingNewEventContextCollection()

    ''' <summary> Event queue for all listeners interested in Adding New events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event AddingNew As AddingNewEventHandler

        AddHandler(ByVal value As AddingNewEventHandler)
            Me._AddingNewHandlers.Add(New NotifyAddingNewEventContext(value))
        End AddHandler

        RemoveHandler(ByVal value As AddingNewEventHandler)
            Me._AddingNewHandlers.RemoveValue(value)
        End RemoveHandler

        RaiseEvent(ByVal sender As Object, ByVal e As AddingNewEventArgs)
            Me._AddingNewHandlers.Raise(sender, e)
        End RaiseEvent

    End Event

#End Region

#Region " Notify "

    ''' <summary>
    ''' Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
    ''' fast return of control to the invoking function.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="e"> Adding New event information. </param>
    Protected Overridable Sub NotifyAddingNew(ByVal e As AddingNewEventArgs)
        Me._AddingNewHandlers.Post(Me, e)
    End Sub

    ''' <summary>
    ''' Asynchronously notifies (posts) change on a different thread. Unsafe for cross threading;
    ''' fast return of control to the invoking function.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 7 to 10 times larger than naked raise event. This has no advantage
    ''' even with slow handler functions.
    ''' </remarks>
    ''' <param name="e"> Adding New event information. </param>
    Protected Overridable Sub AsyncNotifyAddingNew(ByVal e As AddingNewEventArgs)
        Me._AddingNewHandlers.Post(Me, e)
    End Sub

    ''' <summary>
    ''' Synchronously notifies (send) property change on a different thread. Safe for cross threading.
    ''' </summary>
    ''' <remarks>
    ''' Overhead of this method is 3 to 4 times larger than naked raise event. This is the best
    ''' approach.
    ''' </remarks>
    ''' <param name="e"> Adding New event information. </param>
    Protected Overridable Sub SyncNotifyAddingNew(ByVal e As AddingNewEventArgs)
        Me._AddingNewHandlers.Send(Me, e)
    End Sub

#End Region

End Class
