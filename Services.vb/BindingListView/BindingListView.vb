Imports System.ComponentModel

''' <summary>
''' A searchable, sortable, filterable, data bindable view of a list of objects.
''' </summary>
''' <remarks>
''' The BindingListView .NET library provides a type-safe, sortable, filterable, data-bindable
''' view of one or more lists of objects. It is the business objects equivalent of using a
''' DataView on a DataTable in ADO.NET. If you have a list of objects to display on a Windows
''' Forms UI (e.g. in a DataGridView) and want to allow your user to sort and filter, then this
''' is the library to use! For more information see
''' https://siderite.blogspot.com/2016/01/bindinglist-vs-observablecollection.html#at3175648015
''' <para>
''' The BindingListView works by creating a wrapper object around each item in a source list.
''' This is just like how a DataView contains DataRowViews, which wrap DataRow objects in
''' ADO.NET. </para>
''' <para>
''' Here is a very simple example of creating a view of a list of objects: </para>
''' <c>
''' Dim customers as IList(Of Customer) = GetCustomers()</c><c>
''' Dim view as New BindingListView(Of Customer)(customers)</c><c>
''' dataGridView1.DataSource = view</c>
''' <para>
''' The new view is passed the customers list as the "source" list. The view is then data bound
''' to a DataGridView control.</para>
''' <para>
''' In the grid you can now sort by clicking on the headers. This was not possible if we had
''' bound directly to the list object instead. </para>
''' <para>
''' You can programatically sort the view Using the <see cref="BindingListView.ApplySort"/>
''' method.</para>
''' <para>
''' There are a number Of Overloads, the simplest Of which takes a String In the form Of an SQL
''' "order by" clause. For example,</para>
''' <c>
''' view.ApplySort("Balance DESC, Surname")</c>
''' <para>
''' would first sort by the Balance Property (putting the highest first) and then sort by Surname
''' (Is normal ascending order). </para>
''' <para>
''' You can specify zero or more properties To sort by. With Each Property you can enter "DESC"
''' To sort In descending order. </para><para>The restrict the items displayed by the view
''' </para>
''' <para>
''' You can Set a filter. A simple filter Is a Function that takes an Object And returns True Or
''' False depending On If the Object should appear In the view. More advanced filters can be
''' created by creating a Class that Implements the <c>IItemFilter(Of T)</c> Interface. </para>
''' <para>
''' This example shows creating a filter Using an anonymous method In C#.</para>
''' <c>
''' view.ApplyFilter(delegate(Customer customer) {return customer.Balance .gt. 1000; });
''' In VB.NET you will have to explicitly create the function.
''' </c>
''' <c>
''' view.ApplyFilter(AddressOf BalanceFilter)
''' ...
''' Function BalanceFilter(ByVal customer As Customer) As Boolean
'''        Return customer.Balance .gt. 1000
''' End Function
''' </c>
''' A filter will Not actually remove items from the source list, they are just Not visible When
''' bound To a grid For example.
''' <para>
''' An Important Detail
''' </para><para>
''' The BindingListView works by creating a wrapper Object around Each item In a source list.
''' This Is just Like how a DataView contains DataRowViews, which wrap DataRow objects, In
''' ADO.NET. The wrapper Object In a BindingListView Is Of type ObjectView(of T). The only time
''' you usually need to interact with an object view is when retrieving items from the view in
''' code:</para>
''' <c>
''' ObjectView(Of Customer)
''' CustomerView = view[0] ' Get first item in view Customer c = customerView.Object;
''' </c>
''' The example above uses the Object Property Of the ObjectView To Return the original Object.
''' This important detail impacts the most often When you are Using a BindingSource component On
''' a Form, As outlined In this following example.
''' <c>
''' Upon a customer being selected (in a grid or listbox for example)
''' we want to do something with that object.
''' <code>
''' Private Sub customersBindingSource_PositionChanged(Object sender, EventArgs e)
'''   ' This cast will fail at runtime. Customer c = (Customer)customersBindingSource.Current '
'''   ' The correct code Is this
'''   Customer c = ((ObjectView(Of customer)(customersBindingSource.Current).Object
''' </code>
''' </c>
''' BindingSource.Current Is typed as just Object, so the invalid cast to Customer cannot be
''' caught at compile time. This causes a runtime cast exception instead. <para>
''' (c) 2006 Andrew Davey. All rights reserved.</para><para>
''' Licensed under The MIT License.</para><para>
''' David, 5/14/2019, </para><para>
''' David, 12/15/2018, https://blogs.warwick.ac.uk/andrewdavey and
''' https://sourceforge.net/projects/blw/. </para>
''' </remarks>
Public Class BindingListView(Of T)
    Inherits AggregateBindingListView(Of T)

    ''' <summary>
    ''' Creates a new <see cref="BindingListView(Of T)"/> of a given IBindingList. All items in the
    ''' list must be of type <typeparamref name="T"/>.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="list"> The list of objects to base the view on. </param>
    Public Sub New(ByVal list As IList)
        MyBase.New()
        Me.DataSource = list
    End Sub

    ''' <summary>
    ''' Creates a new <see cref="BindingListView(Of T)"/> of a given IBindingList. All items in the
    ''' list must be of type <typeparamref name="T"/>.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="container"> The container. </param>
    Public Sub New(ByVal container As IContainer)
        MyBase.New(container)
        Me.DataSource = Nothing
    End Sub

    ''' <summary> Clears the current data. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Private Sub ClearCurrentData()
        Me.SourceLists = New BindingList(Of IList(Of T))()
        Me.NewItemsList = Nothing
        Me.FilterAndSort()
        Me.OnListChanged(New ListChangedEventArgs(ListChangedType.Reset, -1))
    End Sub

    ''' <summary> Gets or sets the data source. </summary>
    ''' <exception cref="ArgumentException"> Thrown when one or more arguments have unsupported or
    '''                                      illegal values. </exception>
    ''' <value> The data source. </value>
    <System.ComponentModel.DefaultValue(CType(Nothing, Object)), AttributeProvider(GetType(IListSource))>
    Public Property DataSource() As IList
        Get
            Dim e As IEnumerator(Of IList) = Me.EnumerateSourceLists().GetEnumerator()
            e.MoveNext()
            Return e.Current
        End Get
        Set(ByVal value As IList)
            If value Is Nothing Then
                ' Clear all current data
                Me.SourceLists = New BindingList(Of IList(Of T))()
                Me.NewItemsList = Nothing
                Me.FilterAndSort()
                Me.OnListChanged(New ListChangedEventArgs(ListChangedType.Reset, -1))
                Return
            End If

            If Not (TypeOf value Is ICollection(Of T)) Then
                ' list is not a strongly-type collection.
                ' Check that items in list are all of type T
                For Each item As Object In value
                    If Not (TypeOf item Is T) Then
                        Throw New ArgumentException(String.Format(My.Resources.InvalidListItemType, GetType(T).FullName), NameOf(value))
                    End If
                Next item
            End If
            Me.SourceLists = New Object() {value}
            Me.NewItemsList = value
        End Set
    End Property

    ''' <summary> Determine if we should serialize data source. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
    Private Function ShouldSerializeDataSource() As Boolean
        Return Me.SourceLists.Count > 0
    End Function

    ''' <summary> Event handler for when SourceLists is changed. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown when the requested operation is invalid. </exception>
    ''' <param name="sender"> The <see cref="T:isr.Core.Forma.ObjectView`1" /> that raised the event. </param>
    ''' <param name="e">      List changed event information. </param>
    Protected Overrides Sub SourceListsChanged(ByVal sender As Object, ByVal e As ListChangedEventArgs)
        If sender Is Nothing OrElse e Is Nothing Then Return
        If (Me.SourceLists.Count > 1 AndAlso e.ListChangedType = ListChangedType.ItemAdded) OrElse
            e.ListChangedType = ListChangedType.ItemDeleted Then
            Throw New InvalidOperationException($"{NameOf(BindingListView(Of T))} allows strictly one source list.")
        Else
            MyBase.SourceListsChanged(sender, e)
        End If
    End Sub
End Class
