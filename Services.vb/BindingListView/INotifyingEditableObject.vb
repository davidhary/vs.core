Imports System.ComponentModel

''' <summary>
''' Extends <see cref="System.ComponentModel.IEditableObject"/> by providing events to raise
''' during edit state changes.
''' </summary>
''' <remarks>
''' (c) 2006 Andrew Davey. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/15/2018, 1.2  </para><para>
''' https://blogs.warwick.ac.uk/andrewdavey and https://sourceforge.net/projects/blw/.
''' </para>
''' </remarks>
Friend Interface INotifyingEditableObject
    Inherits IEditableObject

    ''' <summary>
    ''' An edit has started on the object.
    ''' </summary>
    ''' <remarks>
    ''' This event should be raised from BeginEdit().
    ''' </remarks>
    Event EditBegun As EventHandler

    ''' <summary> Event queue for all listeners interested in EditCanceled events. </summary>
    ''' <remarks>
    ''' This event should be raised from CancelEdit().
    ''' </remarks>
    Event EditCanceled As EventHandler

    ''' <summary> Event queue for all listeners interested in EditEnded events. </summary>
    ''' <remarks>
    ''' This event should be raised from EndEdit().
    ''' </remarks>
    Event EditEnded As EventHandler

End Interface
