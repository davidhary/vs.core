Imports System.ComponentModel

''' <summary> Provided view property descriptor. </summary>
''' <remarks>
''' (c) 2006 Andrew Davey. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/15/2018, 1.2.* </para><para>
''' https://blogs.warwick.ac.uk/andrewdavey and https://sourceforge.net/projects/blw/.
''' </para>
''' </remarks>
Friend Class ProvidedViewPropertyDescriptor(Of T)
    Inherits PropertyDescriptor

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="name">         The name. </param>
    ''' <param name="propertyType"> Type of the property. </param>
    Public Sub New(ByVal name As String, ByVal propertyType As Type)
        MyBase.New(name, Nothing)
        Me._propertyType = propertyType
    End Sub
    ''' <summary> Type of the property. </summary>
    Private ReadOnly _propertyType As Type

    ''' <summary>
    ''' When overridden in a derived class, returns whether resetting an object changes its value.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="component"> The component to test for reset capability. </param>
    ''' <returns> true if resetting the component changes its value; otherwise, false. </returns>
    Public Overrides Function CanResetValue(ByVal component As Object) As Boolean
        Return False
    End Function

    ''' <summary>
    ''' When overridden in a derived class, gets the type of the component this property is bound to.
    ''' </summary>
    ''' <value>
    ''' A <see cref="T:System.Type" /> that represents the type of component this property is bound
    ''' to. When the
    ''' <see cref="M:System.ComponentModel.PropertyDescriptor.GetValue(System.Object)" /> or
    ''' <see cref="M:System.ComponentModel.PropertyDescriptor.SetValue(System.Object,System.Object)" />
    ''' methods are invoked, the object specified might be an instance of this type.
    ''' </value>
    Public Overrides ReadOnly Property ComponentType() As Type
        Get
            Return GetType(ObjectView(Of T))
        End Get
    End Property

    ''' <summary>
    ''' When overridden in a derived class, gets the current value of the property on a component.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <exception cref="ArgumentException">     Thrown when one or more arguments have unsupported or
    '''                                          illegal values. </exception>
    ''' <param name="component"> The component with the property for which to retrieve the value. </param>
    ''' <returns> The value of a property for a given component. </returns>
    Public Overrides Function GetValue(ByVal component As Object) As Object
        If component Is Nothing Then Throw New ArgumentNullException(NameOf(component))
        If Me.ComponentType.IsAssignableFrom(component.GetType()) Then
            Return (TryCast(component, ObjectView(Of T))).GetProvidedView(Me.Name)
        Else
            Throw New ArgumentException($"Type of {NameOf(component)} is not valid.", NameOf(component))
        End If
    End Function

    ''' <summary>
    ''' When overridden in a derived class, gets a value indicating whether this property is read-
    ''' only.
    ''' </summary>
    ''' <value> true if the property is read-only; otherwise, false. </value>
    Public Overrides ReadOnly Property IsReadOnly() As Boolean
        Get
            Return True
        End Get
    End Property

    ''' <summary> When overridden in a derived class, gets the type of the property. </summary>
    ''' <value> A <see cref="T:System.Type" /> that represents the type of the property. </value>
    Public Overrides ReadOnly Property PropertyType() As Type
        Get
            Return Me._propertyType
        End Get
    End Property

    ''' <summary>
    ''' When overridden in a derived class, resets the value for this property of the component to
    ''' the default value.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    ''' <param name="component"> The component with the property value that is to be reset to the
    '''                          default value. </param>
    Public Overrides Sub ResetValue(ByVal component As Object)
        Throw New NotSupportedException()
    End Sub

    ''' <summary>
    ''' When overridden in a derived class, sets the value of the component to a different value.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="NotSupportedException"> Thrown when the requested operation is not supported. </exception>
    ''' <param name="component"> The component with the property value that is to be set. </param>
    ''' <param name="value">     The new value. </param>
    Public Overrides Sub SetValue(ByVal component As Object, ByVal value As Object)
        Throw New NotSupportedException()
    End Sub

    ''' <summary>
    ''' When overridden in a derived class, determines a value indicating whether the value of this
    ''' property needs to be persisted.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="component"> The component with the property to be examined for persistence. </param>
    ''' <returns> true if the property should be persisted; otherwise, false. </returns>
    Public Overrides Function ShouldSerializeValue(ByVal component As Object) As Boolean
        Return False
    End Function

    ''' <summary> Can provide view of the given property. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="prop"> The property. </param>
    ''' <returns> <c>true</c> if we can provide view of; otherwise <c>false</c> </returns>
    Public Shared Function CanProvideViewOf(ByVal prop As PropertyDescriptor) As Boolean
        Dim listTypeDef As Type = GetType(IList(Of Object)).GetGenericTypeDefinition()
        Dim propType As Type = prop.PropertyType
        Dim args() As Type = propType.GetGenericArguments()
        ' Is this a generic type, with only one generic parameter.
        If args.Length = 1 Then
            ' Create type IList<T> where T is args[0]
            Dim listType As Type = listTypeDef.MakeGenericType(args)
            ' Check if the property type implements IList<T>
            ' but is not an IBindingListView (or better).
            If listType.IsAssignableFrom(propType) AndAlso (Not GetType(IBindingListView).IsAssignableFrom(propType)) Then
                Return True
            End If
        End If
        Return False
    End Function
End Class
