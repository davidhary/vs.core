''' <summary> Composite item filter. </summary>
''' <remarks>
''' (c) 2006 Andrew Davey. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 12/15/2018, </para><para>
''' David, 12/15/2018, 1.2.* https://blogs.warwick.ac.uk/andrewdavey and
''' https://sourceforge.net/projects/blw/. </para>
''' </remarks>
Public Class CompositeItemFilter(Of T)
    Implements IItemFilter(Of T)

    ''' <summary> The filters. </summary>
    ''' <value> The filters. </value>
    Private ReadOnly Property Filters As IList(Of IItemFilter(Of T))

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub New()
        Me.Filters = New List(Of IItemFilter(Of T))()
    End Sub

    ''' <summary> Adds a filter. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="filter"> Specifies the filter. </param>
    Public Sub AddFilter(ByVal filter As IItemFilter(Of T))
        Me.Filters.Add(filter)
    End Sub

    ''' <summary> Removes the filter described by filter. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="filter"> Specifies the filter. </param>
    Public Sub RemoveFilter(ByVal filter As IItemFilter(Of T))
        Me.Filters.Remove(filter)
    End Sub

    ''' <summary> Tests if the item should be included. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="item"> The item to test. </param>
    ''' <returns> True if the item should be included, otherwise false. </returns>
    Public Function Include(ByVal item As T) As Boolean Implements IItemFilter(Of T).Include
        For Each filter As IItemFilter(Of T) In Me.Filters
            If Not filter.Include(item) Then
                Return False
            End If
        Next filter
        Return True
    End Function

End Class
