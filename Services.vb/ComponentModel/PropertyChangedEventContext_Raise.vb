Imports System.ComponentModel
Imports System.Threading

Partial Public Class PropertyChangedEventContext

#Region " ACTIVE CONTEXT "

    ''' <summary> Returns the current synchronization context. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="InvalidOperationException"> Thrown if the current synchronization thread is
    '''                                              null. </exception>
    ''' <returns> A Threading.SynchronizationContext. </returns>
    Private Shared Function CurrentSyncContext() As Threading.SynchronizationContext
        If Threading.SynchronizationContext.Current Is Nothing Then
            Threading.SynchronizationContext.SetSynchronizationContext(New Threading.SynchronizationContext)
        End If
        If Threading.SynchronizationContext.Current Is Nothing Then
            Throw New InvalidOperationException("Current Synchronization Context not set;. Must be set before starting the thread.")
        End If
        Return Threading.SynchronizationContext.Current
    End Function

    ''' <summary> Gets a context for the active. </summary>
    ''' <value> The active context. </value>
    Private ReadOnly Property ActiveContext As SynchronizationContext
        Get
            Return If(Me.Context, PropertyChangedEventContext.CurrentSyncContext)
        End Get
    End Property

#End Region

#Region " SEND POST "

    ''' <summary>
    ''' Asynchronously raises (Posts) the <see cref="PropertyChangedEventHandler"/>. It does all the
    ''' checking to see if the SynchronizationContext is nothing or not, and invokes the delegate
    ''' accordingly. Note that, unlike the synchronous Send action, although the event is posted on
    ''' the synchronization context, cross thread exceptions still occurred, ostensibly, due to the
    ''' changing of the sync context between the time the action was elicited and the time it was
    ''' actually invoked.
    ''' </summary>
    ''' <remarks> David, 7/27/2020. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub Post(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then Me.ActiveContext.Post(Sub() evt(sender, e), Nothing)
    End Sub

    ''' <summary>
    ''' Synchronously raises (sends) the <see cref="PropertyChangedEventHandler"/>. It does all the
    ''' checking to see if the SynchronizationContext is nothing or not, and invokes the delegate
    ''' accordingly.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub Send(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then Me.ActiveContext.Send(Sub() evt(sender, e), Nothing)
    End Sub

#End Region

#Region " POST DYNAMIC INVOKE "

    ''' <summary> Posts a dynamic invoke of the event delegate. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="sender"> Source of the event. </param>
    ''' <param name="e">      Property changed event information. </param>
    Public Sub PostDynamicInvoke(ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Dim context As SynchronizationContext = If(Me.Context, PropertyChangedEventContext.CurrentSyncContext)
        Me.PostDynamicInvoke(context, sender, e)
    End Sub

    ''' <summary> Posts a dynamic invoke of the event delegate. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="context"> The context. </param>
    ''' <param name="sender">  Source of the event. </param>
    ''' <param name="e">       Property changed event information. </param>
    Private Sub PostDynamicInvoke(ByVal context As SynchronizationContext, ByVal sender As Object, ByVal e As PropertyChangedEventArgs)
        Dim evt As PropertyChangedEventHandler = Me.Handler
        If evt IsNot Nothing Then
            For Each d As [Delegate] In evt.GetInvocationList
                context.Post(Sub(ee) d.DynamicInvoke(New Object() {sender, e}), e)
            Next
        End If
    End Sub

#End Region

End Class

