Imports System.IO
Imports System.Threading

Namespace Gnu

    ''' <summary> Wrapper around the Gnu Plot process. </summary>
    ''' <remarks>
    ''' (c) 2011 Awoke Knowing. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 6/17/2019. Source: https://github.com/AwokeKnowing/GnuplotCSharp.
    ''' </para>
    ''' </remarks>
    Public NotInheritable Class GnuPlot

#Region " CONSTRUCTION "

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        Private Sub New()
            MyBase.New
            GnuPlot.ProgramFolder = GnuPlot._DefaultProgramFolder
        End Sub

#End Region

#Region " PROCESS "

        ''' <summary> The active process. </summary>
        ''' <value> The active process. </value>
        Public Shared ReadOnly Property ActiveProcess As Process

        ''' <summary> Start a new Gnu Plot process. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="timeout"> The timeout. </param>
        ''' <returns> The Process. </returns>
        Public Shared Function StartProcess(ByVal timeout As TimeSpan) As Process
            GnuPlot.ProgramFolder = If(String.IsNullOrWhiteSpace(GnuPlot.ProgramFolder), GnuPlot._DefaultProgramFolder, GnuPlot.ProgramFolder)
            GnuPlot._ActiveProcess = New Process()
            GnuPlot.ActiveProcess.StartInfo.FileName = $"{ProgramFolder}{_DefaultProgramFile}"
            GnuPlot.ActiveProcess.StartInfo.UseShellExecute = False
            GnuPlot.ActiveProcess.StartInfo.RedirectStandardInput = True
            If GnuPlot.ActiveProcess.Start() Then
                GnuPlot._ActiveStreamWriter = ActiveProcess.StandardInput
                GnuPlot._PlotBuffer = New List(Of StoredPlot)()
                GnuPlot._SPlotBuffer = New List(Of StoredPlot)()
                GnuPlot.Set("terminal wxt size 350,262")
                GnuPlot.Hold = False
                GnuPlot.ActiveProcess.WaitForInputIdle(CInt(timeout.TotalMilliseconds))
            End If
            Return GnuPlot.ActiveProcess
        End Function

        ''' <summary> Ends the process. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        Public Shared Sub EndProcess()
            GnuPlot.Hold = False
            GnuPlot.ActiveProcess?.Dispose()
        End Sub

        ''' <summary> The default program title. </summary>
        Private Const _DefaultProgramTitle As String = "gnuplot"
        ''' <summary> The default program file. </summary>
        Private Const _DefaultProgramFile As String = GnuPlot._DefaultProgramTitle & ".exe"
        ''' <summary> The default program folder. </summary>
        Private Const _DefaultProgramFolder As String = "C:\Program Files\" & GnuPlot._DefaultProgramTitle & "\bin\"

        ''' <summary> Pathname of the program folder. </summary>
        Private Shared _ProgramFolder As String

        ''' <summary> Pathname of the program folder. </summary>
        ''' <value> The pathname of the program folder. </value>
        Public Shared Property ProgramFolder As String
            Get
                Return GnuPlot._ProgramFolder
            End Get
            Set(value As String)
                If String.IsNullOrWhiteSpace(value) Then value = GnuPlot._DefaultProgramFolder
                If Not value.EndsWith("\", StringComparison.OrdinalIgnoreCase) Then value &= "\"
                GnuPlot._ProgramFolder = value
            End Set
        End Property
        ''' <summary> Buffer for plot data. </summary>
        Private Shared _PlotBuffer As List(Of StoredPlot)
        ''' <summary> Buffer for plot data. </summary>
        Private Shared _SPlotBuffer As List(Of StoredPlot)
        ''' <summary> True to replot with splot. </summary>
        Private Shared _ReplotWithSplot As Boolean
        ''' <summary> True to hold. </summary>
        Private Shared _Hold As Boolean

        ''' <summary> True to hold. </summary>
        ''' <value> The hold. </value>
        Public Shared Property Hold() As Boolean
            Get
                Return GnuPlot._Hold
            End Get
            Private Set(ByVal value As Boolean)
                GnuPlot._Hold = value
            End Set
        End Property

        ''' <summary> Hold on. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        Public Shared Sub HoldOn()
            GnuPlot.Hold = True
            GnuPlot._PlotBuffer.Clear()
            GnuPlot._SPlotBuffer.Clear()
        End Sub

        ''' <summary> Hold off. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        Public Shared Sub HoldOff()
            GnuPlot.Hold = False
            GnuPlot._PlotBuffer.Clear()
            GnuPlot._SPlotBuffer.Clear()
        End Sub

        ''' <summary> Closes this object. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        Public Shared Sub Close()
            ActiveProcess.CloseMainWindow()
        End Sub

#End Region

#Region " STREAM "

        ''' <summary> The active stream writer. </summary>
        Private Shared _ActiveStreamWriter As StreamWriter

        ''' <summary> Writes a line. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="commands"> The commands to write. </param>
        Public Shared Sub WriteLine(ByVal commands As String)
            GnuPlot._ActiveStreamWriter.WriteLine(commands)
            GnuPlot._ActiveStreamWriter.Flush()
        End Sub

        ''' <summary> Writes. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="commands"> The commands to write. </param>
        Public Shared Sub Write(ByVal commands As String)
            GnuPlot._ActiveStreamWriter.Write(commands)
            GnuPlot._ActiveStreamWriter.Flush()
        End Sub

        Public Shared Sub [Set](ParamArray ByVal options() As String)
            If options?.Any Then
                For i As Integer = 0 To options.Length - 1
                    GnuPlot._ActiveStreamWriter.WriteLine("set " & options(i))
                Next i
            End If
        End Sub

        ''' <summary> Unsets the given options. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="options"> Options for controlling the operation. </param>
        Public Shared Sub Unset(ParamArray ByVal options() As String)
            If options?.Any Then
                For i As Integer = 0 To options.Length - 1
                    GnuPlot._ActiveStreamWriter.WriteLine("unset " & options(i))
                Next i
            End If
        End Sub

        ''' <summary> Saves a data. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="values">   The values. </param>
        ''' <param name="fileName"> Filename of the file. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Shared Function SaveData(ByVal values As Double(), ByVal fileName As String) As Boolean
            Using dataStream As New StreamWriter(fileName, False)
                GnuPlot.WriteData(values, dataStream)
            End Using
            Return True
        End Function

        ''' <summary> Saves a data. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="abscissa"> The abscissa. </param>
        ''' <param name="ordinate"> The ordinate. </param>
        ''' <param name="fileName"> Filename of the file. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Shared Function SaveData(ByVal abscissa As Double(), ByVal ordinate As Double(), ByVal fileName As String) As Boolean
            If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
            If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
            Using dataStream As New StreamWriter(fileName, False)
                GnuPlot.WriteData(abscissa, ordinate, dataStream)
            End Using
            Return True
        End Function

        ''' <summary> Saves a data. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="abscissa"> The abscissa. </param>
        ''' <param name="ordinate"> The ordinate. </param>
        ''' <param name="depth">    The depth. </param>
        ''' <param name="fileName"> Filename of the file. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Shared Function SaveData(ByVal abscissa As Double(), ByVal ordinate As Double(), ByVal depth As Double(), ByVal fileName As String) As Boolean
            If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
            If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
            Using dataStream As New StreamWriter(fileName, False)
                GnuPlot.WriteData(abscissa, ordinate, depth, dataStream)
            End Using
            Return True
        End Function

        ''' <summary> Saves a data. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="ordinateSize"> Size of the ordinate. </param>
        ''' <param name="depth">        The depth. </param>
        ''' <param name="fileName">     Filename of the file. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        Public Shared Function SaveData(ByVal ordinateSize As Integer, ByVal depth As Double(), ByVal fileName As String) As Boolean
            If depth Is Nothing Then Throw New ArgumentNullException(NameOf(depth))
            Using dataStream As New StreamWriter(fileName, False)
                GnuPlot.WriteData(ordinateSize, depth, dataStream)
            End Using
            Return True
        End Function

        ''' <summary> Saves a data. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="plane">    The plane. </param>
        ''' <param name="fileName"> Filename of the file. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <CLSCompliant(False)>
        Public Shared Function SaveData(ByVal plane As Double(,), ByVal fileName As String) As Boolean
            Using dataStream As New StreamWriter(fileName, False)
                GnuPlot.WriteData(plane, dataStream)
            End Using
            Return True
        End Function

#End Region

#Region " PLOT "

        ''' <summary> Replots this object. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        Public Shared Sub Replot()
            If GnuPlot._ReplotWithSplot Then
                GnuPlot.SPlot(_SPlotBuffer)
            Else
                GnuPlot.Plot(_PlotBuffer)
            End If
        End Sub

        ''' <summary> Plots the given stored plots. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="fileNameOrFunction"> The file name or function. </param>
        ''' <param name="options">            (Optional) Options for controlling the operation. </param>
        Public Shared Sub Plot(ByVal fileNameOrFunction As String, Optional ByVal options As String = "")
            If Not GnuPlot.Hold Then GnuPlot._PlotBuffer.Clear()
            GnuPlot._PlotBuffer.Add(New StoredPlot(fileNameOrFunction, options))
            GnuPlot.Plot(GnuPlot._PlotBuffer)
        End Sub

        ''' <summary> Plots the given stored plots. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="ordinate"> The ordinate. </param>
        ''' <param name="options">  (Optional) Options for controlling the operation. </param>
        Public Shared Sub Plot(ByVal ordinate As Double(), Optional ByVal options As String = "")
            If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
            If Not GnuPlot.Hold Then GnuPlot._PlotBuffer.Clear()
            _PlotBuffer.Add(New StoredPlot(ordinate, options))
            Plot(_PlotBuffer)
        End Sub

        ''' <summary> Plots the given stored plots. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="abscissa"> The abscissa. </param>
        ''' <param name="ordinate"> The ordinate. </param>
        ''' <param name="options">  (Optional) Options for controlling the operation. </param>
        Public Shared Sub Plot(ByVal abscissa As Double(), ByVal ordinate As Double(), Optional ByVal options As String = "")
            If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
            If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
            If Not GnuPlot.Hold Then GnuPlot._PlotBuffer.Clear()
            _PlotBuffer.Add(New StoredPlot(abscissa, ordinate, options))
            Plot(_PlotBuffer)
        End Sub

        ''' <summary> Contours. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="fileNameOrFunction"> The file name or function. </param>
        ''' <param name="options">            (Optional) Options for controlling the operation. </param>
        ''' <param name="labelContours">      (Optional) True to label contours. </param>
        Public Shared Sub Contour(ByVal fileNameOrFunction As String, Optional ByVal options As String = "", Optional ByVal labelContours As Boolean = True)
            If Not GnuPlot.Hold Then GnuPlot._PlotBuffer.Clear()
            Dim p As New StoredPlot(fileNameOrFunction, options, PlotType.ContourFileOrFunction) With {
                .LabelContours = labelContours
            }
            GnuPlot._PlotBuffer.Add(p)
            GnuPlot.Plot(_PlotBuffer)
        End Sub

        ''' <summary> Contours. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="ordinateSize">  Size of the ordinate. </param>
        ''' <param name="depth">         The depth. </param>
        ''' <param name="options">       (Optional) Options for controlling the operation. </param>
        ''' <param name="labelContours"> (Optional) True to label contours. </param>
        Public Shared Sub Contour(ByVal ordinateSize As Integer, ByVal depth As Double(), Optional ByVal options As String = "", Optional ByVal labelContours As Boolean = True)
            If depth Is Nothing Then Throw New ArgumentNullException(NameOf(depth))
            If Not GnuPlot.Hold Then GnuPlot._PlotBuffer.Clear()
            Dim p As New StoredPlot(ordinateSize, depth, options, PlotType.ContourZ) With {
                .LabelContours = labelContours
            }
            GnuPlot._PlotBuffer.Add(p)
            GnuPlot.Plot(_PlotBuffer)
        End Sub

        ''' <summary> Contours. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="abscissa">      The abscissa. </param>
        ''' <param name="ordinate">      The ordinate. </param>
        ''' <param name="depth">         The depth. </param>
        ''' <param name="options">       (Optional) Options for controlling the operation. </param>
        ''' <param name="labelContours"> (Optional) True to label contours. </param>
        Public Shared Sub Contour(ByVal abscissa As Double(), ByVal ordinate As Double(), ByVal depth As Double(), Optional ByVal options As String = "", Optional ByVal labelContours As Boolean = True)
            If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
            If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
            If depth Is Nothing Then Throw New ArgumentNullException(NameOf(depth))
            If Not Hold Then
                GnuPlot._PlotBuffer.Clear()
            End If

            Dim p As New StoredPlot(abscissa, ordinate, depth, options, PlotType.ContourXYZ) With {
                .LabelContours = labelContours
            }
            GnuPlot._PlotBuffer.Add(p)
            GnuPlot.Plot(_PlotBuffer)
        End Sub

        ''' <summary> Contours. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="plane">         The plane. </param>
        ''' <param name="options">       (Optional) Options for controlling the operation. </param>
        ''' <param name="labelContours"> (Optional) True to label contours. </param>
        Public Shared Sub Contour(ByVal plane As Double(,), Optional ByVal options As String = "", Optional ByVal labelContours As Boolean = True)
            If Not Hold Then
                GnuPlot._PlotBuffer.Clear()
            End If

            Dim p As New Gnu.StoredPlot(plane, options, PlotType.ContourZZ) With {
                .LabelContours = labelContours
            }
            GnuPlot._PlotBuffer.Add(p)
            GnuPlot.Plot(_PlotBuffer)
        End Sub

        ''' <summary> Heat map. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="fileNameOrFunction"> The file name or function. </param>
        ''' <param name="options">            (Optional) Options for controlling the operation. </param>
        Public Shared Sub HeatMap(ByVal fileNameOrFunction As String, Optional ByVal options As String = "")
            If Not Hold Then
                _PlotBuffer.Clear()
            End If
            _PlotBuffer.Add(New StoredPlot(fileNameOrFunction, options, PlotType.ColorMapFileOrFunction))
            Plot(_PlotBuffer)
        End Sub

        ''' <summary> Heat map. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="ordinateSize"> Size of the ordinate. </param>
        ''' <param name="intensity">    The intensity. </param>
        ''' <param name="options">      (Optional) Options for controlling the operation. </param>
        Public Shared Sub HeatMap(ByVal ordinateSize As Integer, ByVal intensity As Double(), Optional ByVal options As String = "")
            If intensity Is Nothing Then Throw New ArgumentNullException(NameOf(intensity))
            If Not Hold Then
                _PlotBuffer.Clear()
            End If
            _PlotBuffer.Add(New StoredPlot(ordinateSize, intensity, options, PlotType.ColorMapZ))
            Plot(_PlotBuffer)
        End Sub

        ''' <summary> Heat map. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="abscissa">  The abscissa. </param>
        ''' <param name="ordinate">  The ordinate. </param>
        ''' <param name="intensity"> The intensity. </param>
        ''' <param name="options">   (Optional) Options for controlling the operation. </param>
        Public Shared Sub HeatMap(ByVal abscissa As Double(), ByVal ordinate As Double(), ByVal intensity As Double(), Optional ByVal options As String = "")
            If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
            If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
            If intensity Is Nothing Then Throw New ArgumentNullException(NameOf(intensity))
            If Not Hold Then
                _PlotBuffer.Clear()
            End If
            _PlotBuffer.Add(New StoredPlot(abscissa, ordinate, intensity, options, PlotType.ColorMapXYZ))
            Plot(_PlotBuffer)
        End Sub

        ''' <summary> Heat map. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="intensityGrid"> The intensity grid. </param>
        ''' <param name="options">       (Optional) Options for controlling the operation. </param>
        Public Shared Sub HeatMap(ByVal intensityGrid As Double(,), Optional ByVal options As String = "")
            If Not Hold Then _SPlotBuffer.Clear()
            _PlotBuffer.Add(New StoredPlot(intensityGrid, options, PlotType.ColorMapZZ))
            Plot(_PlotBuffer)
        End Sub

        ''' <summary> Plots the given stored plots. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="storedPlots"> The stored plots. </param>
        Public Shared Sub Plot(ByVal storedPlots As IList(Of StoredPlot))
            If storedPlots Is Nothing Then Throw New ArgumentNullException(NameOf(storedPlots))
            _ReplotWithSplot = False
            Dim plotActionName As String = "plot "
            Dim plotstring As String = String.Empty
            Dim contfile As String
            Dim defcntopts As String
            RemoveContourLabels()
            For i As Integer = 0 To storedPlots.Count - 1
                Dim p As StoredPlot = storedPlots(i)
                defcntopts = If(p.Options.Length > 0 AndAlso (p.Options.Contains(" w") OrElse p.Options.Chars(0) = "w"c), " ", " with lines ")
                Select Case p.PlotType
                    Case PlotType.PlotFileOrFunction
                        If p.FileName IsNot Nothing Then
                            plotstring &= (plotActionName & PlotPath(p.FileName) & " " & p.Options)
                        Else
                            plotstring &= (plotActionName & p.FunctionName & " " & p.Options)
                        End If
                    Case PlotType.PlotXY, PlotType.PlotY
                        plotstring &= (plotActionName & """-"" " & p.Options)
                    Case PlotType.ContourFileOrFunction
                        contfile = Path.GetTempPath() & "_cntrtempdata" & i & ".dat"
                        MakeContourFile((If(p.FileName IsNot Nothing, PlotPath(p.FileName), p.FunctionName)), contfile)
                        If p.LabelContours Then
                            SetContourLabels(contfile)
                        End If
                        plotstring &= (plotActionName & PlotPath(contfile) & defcntopts & p.Options)
                    Case PlotType.ContourXYZ
                        contfile = Path.GetTempPath() & "_cntrtempdata" & i & ".dat"
                        MakeContourFile(p.Abscissa, p.Ordinate, p.Depth, contfile)
                        If p.LabelContours Then
                            SetContourLabels(contfile)
                        End If
                        plotstring &= (plotActionName & PlotPath(contfile) & defcntopts & p.Options)
                    Case PlotType.ContourZZ
                        contfile = Path.GetTempPath() & "_cntrtempdata" & i & ".dat"
                        MakeContourFile(p.Plane, contfile)
                        If p.LabelContours Then
                            SetContourLabels(contfile)
                        End If
                        plotstring &= (plotActionName & PlotPath(contfile) & defcntopts & p.Options)
                    Case PlotType.ContourZ
                        contfile = Path.GetTempPath() & "_cntrtempdata" & i & ".dat"
                        MakeContourFile(p.OrdinateSize, p.Depth, contfile)
                        If p.LabelContours Then
                            SetContourLabels(contfile)
                        End If
                        plotstring &= (plotActionName & PlotPath(contfile) & defcntopts & p.Options)


                    Case PlotType.ColorMapFileOrFunction
                        If p.FileName IsNot Nothing Then
                            plotstring &= (plotActionName & PlotPath(p.FileName) & " with image " & p.Options)
                        Else
                            plotstring &= (plotActionName & p.FunctionName & " with image " & p.Options)
                        End If
                    Case PlotType.ColorMapXYZ, PlotType.ColorMapZ
                        plotstring &= (plotActionName & """-"" " & " with image " & p.Options)
                    Case PlotType.ColorMapZZ
                        plotstring &= (plotActionName & """-"" " & "matrix with image " & p.Options)
                End Select
                If i = 0 Then
                    plotActionName = ", "
                End If
            Next i
            _ActiveStreamWriter.WriteLine(plotstring)

            For i As Integer = 0 To storedPlots.Count - 1
                Dim p As StoredPlot = storedPlots(i)
                Select Case p.PlotType
                    Case PlotType.PlotXY
                        WriteData(p.Abscissa, p.Ordinate, _ActiveStreamWriter, False)
                        _ActiveStreamWriter.WriteLine("e")
                    Case PlotType.PlotY
                        WriteData(p.Ordinate, _ActiveStreamWriter, False)
                        _ActiveStreamWriter.WriteLine("e")
                    Case PlotType.ColorMapXYZ
                        WriteData(p.Abscissa, p.Ordinate, p.Depth, _ActiveStreamWriter, False)
                        _ActiveStreamWriter.WriteLine("e")
                    Case PlotType.ColorMapZ
                        WriteData(p.OrdinateSize, p.Depth, _ActiveStreamWriter, False)
                        _ActiveStreamWriter.WriteLine("e")
                    Case PlotType.ColorMapZZ
                        WriteData(p.Plane, _ActiveStreamWriter, False)
                        _ActiveStreamWriter.WriteLine("e")
                End Select
            Next i
            _ActiveStreamWriter.Flush()
        End Sub

#End Region

#Region " S PLOT "

        ''' <summary> Plots the given stored plots. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="fileNameOrFunction"> The file name or function. </param>
        ''' <param name="options">            (Optional) Options for controlling the operation. </param>
        Public Shared Sub SPlot(ByVal fileNameOrFunction As String, Optional ByVal options As String = "")
            If Not Hold Then _SPlotBuffer.Clear()
            _SPlotBuffer.Add(New StoredPlot(fileNameOrFunction, options, PlotType.SplotFileOrFunction))
            SPlot(_SPlotBuffer)
        End Sub

        ''' <summary> Plots the given stored plots. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="ordinateSize"> Size of the ordinate. </param>
        ''' <param name="depth">        The depth. </param>
        ''' <param name="options">      (Optional) Options for controlling the operation. </param>
        Public Shared Sub SPlot(ByVal ordinateSize As Integer, ByVal depth As Double(), Optional ByVal options As String = "")
            If depth Is Nothing Then Throw New ArgumentNullException(NameOf(depth))
            If Not Hold Then _SPlotBuffer.Clear()
            _SPlotBuffer.Add(New StoredPlot(ordinateSize, depth, options))
            SPlot(_SPlotBuffer)
        End Sub

        ''' <summary> Plots the given stored plots. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="abscissa"> The abscissa. </param>
        ''' <param name="ordinate"> The ordinate. </param>
        ''' <param name="depth">    The depth. </param>
        ''' <param name="options">  (Optional) Options for controlling the operation. </param>
        Public Shared Sub SPlot(ByVal abscissa As Double(), ByVal ordinate As Double(), ByVal depth As Double(), Optional ByVal options As String = "")
            If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
            If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
            If depth Is Nothing Then Throw New ArgumentNullException(NameOf(depth))
            If Not Hold Then _SPlotBuffer.Clear()
            _SPlotBuffer.Add(New StoredPlot(abscissa, ordinate, depth, options))
            SPlot(_SPlotBuffer)
        End Sub

        ''' <summary> Plots the given stored plots. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="plane">   The plane. </param>
        ''' <param name="options"> (Optional) Options for controlling the operation. </param>
        Public Shared Sub SPlot(ByVal plane As Double(,), Optional ByVal options As String = "")
            If Not Hold Then
                _SPlotBuffer.Clear()
            End If
            _SPlotBuffer.Add(New StoredPlot(plane, options))
            SPlot(_SPlotBuffer)
        End Sub

        ''' <summary> Plots the given stored plots. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="storedPlots"> The stored plots. </param>
        Public Shared Sub SPlot(ByVal storedPlots As IList(Of StoredPlot))
            If storedPlots Is Nothing Then Throw New ArgumentNullException(NameOf(storedPlots))
            GnuPlot._ReplotWithSplot = True
            Dim splotActionName As String = "splot "
            Dim plotstring As String = String.Empty
            GnuPlot.RemoveContourLabels()
            For i As Integer = 0 To storedPlots.Count - 1
                Dim p As StoredPlot = storedPlots(i)
                Dim defopts As String = If(p.Options.Length > 0 AndAlso (p.Options.Contains(" w") OrElse p.Options.Chars(0) = "w"c), " ", " with lines ")
                Select Case p.PlotType
                    Case PlotType.SplotFileOrFunction
                        If p.FileName IsNot Nothing Then
                            plotstring &= splotActionName & PlotPath(p.FileName) & defopts & p.Options
                        Else
                            plotstring &= splotActionName & p.FunctionName & defopts & p.Options
                        End If
                    Case PlotType.SplotXYZ, PlotType.SplotZ
                        plotstring &= splotActionName & """-"" " & defopts & p.Options
                    Case PlotType.SplotZZ
                        plotstring &= splotActionName & """-"" matrix " & defopts & p.Options
                End Select
                If i = 0 Then
                    splotActionName = ", "
                End If
            Next i
            _ActiveStreamWriter.WriteLine(plotstring)

            For i As Integer = 0 To storedPlots.Count - 1
                Dim p As StoredPlot = storedPlots(i)
                Select Case p.PlotType
                    Case PlotType.SplotXYZ
                        WriteData(p.Abscissa, p.Ordinate, p.Depth, _ActiveStreamWriter, False)
                        _ActiveStreamWriter.WriteLine("e")
                    Case PlotType.SplotZZ
                        WriteData(p.Plane, _ActiveStreamWriter, False)
                        _ActiveStreamWriter.WriteLine("e")
                        ' GnupStWr.WriteLine("e")
                    Case PlotType.SplotZ
                        WriteData(p.OrdinateSize, p.Depth, _ActiveStreamWriter, False)
                        _ActiveStreamWriter.WriteLine("e")
                End Select
            Next i
            _ActiveStreamWriter.Flush()
        End Sub

#End Region

#Region " WRITE DATA "

        ''' <summary> Writes a data. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="values"> The values. </param>
        ''' <param name="stream"> The stream. </param>
        ''' <param name="flush">  (Optional) True to flush. </param>
        Public Shared Sub WriteData(ByVal values As Double(), ByVal stream As StreamWriter, Optional ByVal flush As Boolean = True)
            If values Is Nothing Then Throw New ArgumentNullException(NameOf(values))
            If stream Is Nothing Then Throw New ArgumentNullException(NameOf(stream))
            For i As Integer = 0 To values.Length - 1
                stream.WriteLine(values(i).ToString())
            Next i
            If flush Then stream.Flush()
        End Sub

        ''' <summary> Writes a data. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="abscissa"> The abscissa. </param>
        ''' <param name="ordinate"> The ordinate. </param>
        ''' <param name="stream">   The stream. </param>
        ''' <param name="flush">    (Optional) True to flush. </param>
        Public Shared Sub WriteData(ByVal abscissa As Double(), ByVal ordinate As Double(), ByVal stream As StreamWriter, Optional ByVal flush As Boolean = True)
            If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
            If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
            If stream Is Nothing Then Throw New ArgumentNullException(NameOf(stream))
            For i As Integer = 0 To Math.Min(abscissa.Length, ordinate.Length) - 1
                stream.WriteLine($"{abscissa(i)} {ordinate(i)}")
            Next i
            If flush Then stream.Flush()
        End Sub

        ''' <summary> Writes a data. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="ordinateSize"> Size of the ordinate. </param>
        ''' <param name="depth">        The depth. </param>
        ''' <param name="stream">       The stream. </param>
        ''' <param name="flush">        (Optional) True to flush. </param>
        Public Shared Sub WriteData(ByVal ordinateSize As Integer, ByVal depth As Double(), ByVal stream As StreamWriter, Optional ByVal flush As Boolean = True)
            If depth Is Nothing Then Throw New ArgumentNullException(NameOf(depth))
            If stream Is Nothing Then Throw New ArgumentNullException(NameOf(stream))
            For i As Integer = 0 To depth.Length - 1
                If i > 0 AndAlso i Mod ordinateSize = 0 Then
                    stream.WriteLine()
                End If
                stream.WriteLine(depth(i).ToString())
            Next i
            If flush Then stream.Flush()
        End Sub

        ''' <summary> Writes a data. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="plane">  The plane. </param>
        ''' <param name="stream"> The stream. </param>
        ''' <param name="flush">  (Optional) True to flush. </param>
        <CLSCompliant(False)>
        Public Shared Sub WriteData(ByVal plane As Double(,), ByVal stream As StreamWriter, Optional ByVal flush As Boolean = True)
            If plane Is Nothing Then Throw New ArgumentNullException(NameOf(plane))
            If stream Is Nothing Then Throw New ArgumentNullException(NameOf(stream))
            Dim m As Integer = plane.GetLength(0)
            Dim n As Integer = plane.GetLength(1)
            Dim line As String
            For i As Integer = 0 To m - 1
                line = String.Empty
                For j As Integer = 0 To n - 1
                    line = $"{line}{plane(i, j)} "
                Next j
                stream.WriteLine(line.TrimEnd())
            Next i
            If flush Then stream.Flush()
        End Sub

        ''' <summary> Writes a data. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="abscissa"> The abscissa. </param>
        ''' <param name="ordinate"> The ordinate. </param>
        ''' <param name="depth">    The depth. </param>
        ''' <param name="stream">   The stream. </param>
        ''' <param name="flush">    (Optional) True to flush. </param>
        Public Shared Sub WriteData(ByVal abscissa As Double(), ByVal ordinate As Double(), ByVal depth As Double(), ByVal stream As StreamWriter, Optional ByVal flush As Boolean = True)
            If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
            If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
            If depth Is Nothing Then Throw New ArgumentNullException(NameOf(depth))
            If stream Is Nothing Then Throw New ArgumentNullException(NameOf(stream))
            For i As Integer = 0 To Math.Min(Math.Min(abscissa.Length, ordinate.Length), depth.Length) - 1
                If i > 0 AndAlso abscissa(i) <> abscissa(i - 1) Then
                    stream.WriteLine("")
                End If
                stream.WriteLine($"{abscissa(i)} {ordinate(i)} {depth(i)}")
            Next i
            If flush Then stream.Flush()
        End Sub

#End Region

#Region " CONTOUR FILE "

        ''' <summary> Plot path. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="path"> Full pathname of the file. </param>
        ''' <returns> A String. </returns>
        Private Shared Function PlotPath(ByVal path As String) As String
            Return """" & path.Replace("\", "\\") & """"
        End Function

        ''' <summary> Saves a set state. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="fileName"> (Optional) Filename of the file. </param>
        Public Shared Sub SaveSetState(Optional ByVal fileName As String = Nothing)
            If fileName Is Nothing Then
                fileName = Path.GetTempPath() & "setstate.tmp"
            End If
            _ActiveStreamWriter.WriteLine($"save set {PlotPath(fileName)}")
            _ActiveStreamWriter.Flush()
            WaitForFile(fileName)
        End Sub

        ''' <summary> Loads set state. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="fileName"> (Optional) Filename of the file. </param>
        Public Shared Sub LoadSetState(Optional ByVal fileName As String = Nothing)
            If fileName Is Nothing Then
                fileName = $"{Path.GetTempPath()}setstate.tmp"
            End If
            _ActiveStreamWriter.WriteLine($"load {PlotPath(fileName)}")
            _ActiveStreamWriter.Flush()
        End Sub

        'these makecontourFile functions should probably be merged into one function and use a StoredPlot parameter

        ''' <summary> Makes contour file. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="fileNameOrFunction"> The file name or function. </param>
        ''' <param name="outputFile">         The output file. </param>
        Private Shared Sub MakeContourFile(ByVal fileNameOrFunction As String, ByVal outputFile As String) 'if it's a file, fileOrFunction needs quotes and escaped backslashes
            SaveSetState()
            GnuPlot.[Set]($"table {PlotPath(outputFile)}")
            GnuPlot.[Set]("contour base")
            GnuPlot.Unset("surface")
            GnuPlot._ActiveStreamWriter.WriteLine($"splot {fileNameOrFunction}")
            GnuPlot.Unset("table")
            GnuPlot._ActiveStreamWriter.Flush()
            GnuPlot.LoadSetState()
            GnuPlot.WaitForFile(outputFile)
        End Sub

        ''' <summary> Makes contour file. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="abscissa">   The abscissa. </param>
        ''' <param name="ordinate">   The ordinate. </param>
        ''' <param name="depth">      The depth. </param>
        ''' <param name="outputFile"> The output file. </param>
        Private Shared Sub MakeContourFile(ByVal abscissa As Double(), ByVal ordinate As Double(), ByVal depth As Double(), ByVal outputFile As String)
            SaveSetState()
            [Set]("table " & PlotPath(outputFile))
            [Set]("contour base")
            Unset("surface")
            _ActiveStreamWriter.WriteLine("splot ""-""")
            WriteData(abscissa, ordinate, depth, _ActiveStreamWriter)
            _ActiveStreamWriter.WriteLine("e")
            Unset("table")
            _ActiveStreamWriter.Flush()
            LoadSetState()
            WaitForFile(outputFile)
        End Sub

        ''' <summary> Makes contour file. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="plane">      The plane. </param>
        ''' <param name="outputFile"> The output file. </param>
        Private Shared Sub MakeContourFile(ByVal plane As Double(,), ByVal outputFile As String)
            SaveSetState()
            [Set]("table " & PlotPath(outputFile))
            [Set]("contour base")
            Unset("surface")
            _ActiveStreamWriter.WriteLine("splot ""-"" matrix")
            WriteData(plane, _ActiveStreamWriter)
            _ActiveStreamWriter.WriteLine("e")
            _ActiveStreamWriter.WriteLine("e")
            Unset("table")
            _ActiveStreamWriter.Flush()
            LoadSetState()
            WaitForFile(outputFile)
        End Sub

        ''' <summary> Makes contour file. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="ordinateSize"> Size of the ordinate. </param>
        ''' <param name="depth">        The depth. </param>
        ''' <param name="outputFile">   The output file. </param>
        Private Shared Sub MakeContourFile(ByVal ordinateSize As Integer, ByVal depth As Double(), ByVal outputFile As String)
            SaveSetState()
            [Set]("table " & PlotPath(outputFile))
            [Set]("contour base")
            Unset("surface")
            _ActiveStreamWriter.WriteLine("splot ""-""")
            WriteData(ordinateSize, depth, _ActiveStreamWriter)
            _ActiveStreamWriter.WriteLine("e")
            Unset("table")
            _ActiveStreamWriter.Flush()
            LoadSetState()
            WaitForFile(outputFile)
        End Sub

        ''' <summary> Number of contour labels. </summary>
        Private Shared _ContourLabelCount As Integer = 50000

        ''' <summary> Sets contour labels. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="contourFile"> The contour file. </param>
        Private Shared Sub SetContourLabels(ByVal contourFile As String)
            Dim file As New System.IO.StreamReader(contourFile)
            Dim line As String
            line = file.ReadLine()
            Do While line IsNot Nothing
                If line.Contains("label:") Then
                    Dim c() As String = file.ReadLine().Trim().Replace("   ", " ").Replace("  ", " ").Split(" "c)
                    GnuPlot._ContourLabelCount += 1
                    _ActiveStreamWriter.WriteLine("set object " & _ContourLabelCount & " rectangle center " & c(0) & "," & c(1) & " size char " & (c(2).ToString().Length + 1) & ",char 1 fs transparent solid .7 noborder fc rgb ""white""  front")
                    _ActiveStreamWriter.WriteLine("set label " & _ContourLabelCount & " """ & c(2) & """ at " & c(0) & "," & c(1) & " front center")
                End If
                line = file.ReadLine()
            Loop
            file.Close()
        End Sub

        ''' <summary> Removes the contour labels. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        Private Shared Sub RemoveContourLabels()
            Do While _ContourLabelCount > 50000
                _ActiveStreamWriter.WriteLine("unset object " & _ContourLabelCount & ";unset label " & _ContourLabelCount)
                _ContourLabelCount -= 1
            Loop
        End Sub

        ''' <summary> Wait for file. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="filename"> Filename of the file. </param>
        ''' <param name="timeout">  (Optional) The timeout. </param>
        ''' <returns> <c>true</c> if it succeeds; otherwise <c>false</c> </returns>
        <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
        Private Shared Function WaitForFile(ByVal filename As String, Optional ByVal timeout As Integer = 10000) As Boolean
            Thread.Sleep(20)
            Dim attempts As Integer = timeout \ 100
            Dim file As System.IO.StreamReader = Nothing
            Do While file Is Nothing
                Try
                    file = New System.IO.StreamReader(filename)
                Catch
                    Dim tempVar As Boolean = attempts > 0
                    attempts -= 1
                    If tempVar Then
                        Thread.Sleep(100)
                    Else
                        Return False
                    End If
                End Try
            Loop
            file.Close()
            Return True
        End Function

#End Region

    End Class

    ''' <summary> Values that represent point styles. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Enum PointStyle
        ''' <summary> An enum constant representing the dot option. </summary>
        Dot = 0
        ''' <summary> An enum constant representing the plus option. </summary>
        Plus = 1
        ''' <summary> An enum constant representing the mark option. </summary>
        XMark = 2
        ''' <summary> An enum constant representing the star option. </summary>
        Star = 3
        ''' <summary> An enum constant representing the dot square option. </summary>
        DotSquare = 4
        ''' <summary> An enum constant representing the solid square option. </summary>
        SolidSquare = 5
        ''' <summary> An enum constant representing the dot circle option. </summary>
        DotCircle = 6
        ''' <summary> An enum constant representing the solid circle option. </summary>
        SolidCircle = 7
        ''' <summary> An enum constant representing the dot triangle up option. </summary>
        DotTriangleUp = 8
        ''' <summary> An enum constant representing the solid triangle up option. </summary>
        SolidTriangleUp = 9
        ''' <summary> An enum constant representing the dot triangle down option. </summary>
        DotTriangleDown = 10
        ''' <summary> An enum constant representing the solid triangle down option. </summary>
        SolidTriangleDown = 11
        ''' <summary> An enum constant representing the dot diamond option. </summary>
        DotDiamond = 12
        ''' <summary> An enum constant representing the solid diamond option. </summary>
        SolidDiamond = 13
    End Enum

    ''' <summary> Values that represent plot types. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Enum PlotType
        ''' <summary> An enum constant representing the plot file or function option. </summary>
        PlotFileOrFunction
        ''' <summary> An enum constant representing the plot Y coordinate option. </summary>
        PlotY
        ''' <summary> An enum constant representing the plot xy option. </summary>
        PlotXY
        ''' <summary> An enum constant representing the contour file or function option. </summary>
        ContourFileOrFunction
        ''' <summary> An enum constant representing the contour xyz option. </summary>
        ContourXYZ
        ''' <summary> An enum constant representing the contour zz option. </summary>
        ContourZZ
        ''' <summary> An enum constant representing the contour Z coordinate option. </summary>
        ContourZ
        ''' <summary> An enum constant representing the color map file or function option. </summary>
        ColorMapFileOrFunction
        ''' <summary> An enum constant representing the color map xyz option. </summary>
        ColorMapXYZ
        ''' <summary> An enum constant representing the color map zz option. </summary>
        ColorMapZZ
        ''' <summary> An enum constant representing the color map Z coordinate option. </summary>
        ColorMapZ
        ''' <summary> An enum constant representing the splot file or function option. </summary>
        SplotFileOrFunction
        ''' <summary> An enum constant representing the splot xyz option. </summary>
        SplotXYZ
        ''' <summary> An enum constant representing the splot zz option. </summary>
        SplotZZ
        ''' <summary> An enum constant representing the splot Z coordinate option. </summary>
        SplotZ
    End Enum

    ''' <summary> Wrapper around the Gnu Plot process. </summary>
    ''' <remarks>
    ''' (c) 2011 Awoke Knowing. All rights reserved.<para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 6/17/2019. Source: https://github.com/AwokeKnowing/GnuplotCSharp.
    ''' </para>
    ''' </remarks>
    Public Class StoredPlot

        ''' <summary> Default constructor. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        Public Sub New()
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="fileNameOrFunction"> The file name or function. </param>
        ''' <param name="options">            (Optional) Options for controlling the operation. </param>
        ''' <param name="plotType">           (Optional) Type of the plot. </param>
        Public Sub New(ByVal fileNameOrFunction As String, Optional ByVal options As String = "", Optional ByVal plotType As PlotType = PlotType.PlotFileOrFunction)
            MyBase.New()
            If IsFile(fileNameOrFunction) Then
                Me.FileName = fileNameOrFunction
            Else
                Me.FunctionName = fileNameOrFunction
            End If
            Me.Options = options
            Me.PlotType = plotType
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="ordinate"> The ordinate. </param>
        ''' <param name="options">  (Optional) Options for controlling the operation. </param>
        Public Sub New(ByVal ordinate As Double(), Optional ByVal options As String = "")
            MyBase.New()
            If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
            Me.Ordinate = ordinate
            Me.Options = options
            Me.PlotType = PlotType.PlotY
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="abscissa"> The abscissa. </param>
        ''' <param name="ordinate"> The ordinate. </param>
        ''' <param name="options">  (Optional) Options for controlling the operation. </param>
        Public Sub New(ByVal abscissa As Double(), ByVal ordinate As Double(), Optional ByVal options As String = "")
            MyBase.New()
            If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
            If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
            Me.Abscissa = abscissa
            Me.Ordinate = ordinate
            Me.Options = options
            Me.PlotType = PlotType.PlotXY
        End Sub

        '3D data

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="ordinateSize"> Size of the ordinate. </param>
        ''' <param name="depth">        The depth. </param>
        ''' <param name="options">      (Optional) Options for controlling the operation. </param>
        ''' <param name="plotType">     (Optional) Type of the plot. </param>
        Public Sub New(ByVal ordinateSize As Integer, ByVal depth As Double(), Optional ByVal options As String = "", Optional ByVal plotType As PlotType = PlotType.SplotZ)
            MyBase.New()
            If depth Is Nothing Then Throw New ArgumentNullException(NameOf(depth))
            Me.OrdinateSize = ordinateSize
            Me.Depth = depth
            Me.Options = options
            Me.PlotType = plotType
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="abscissa"> The abscissa. </param>
        ''' <param name="ordinate"> The ordinate. </param>
        ''' <param name="depth">    The depth. </param>
        ''' <param name="options">  (Optional) Options for controlling the operation. </param>
        ''' <param name="plotType"> (Optional) Type of the plot. </param>
        Public Sub New(ByVal abscissa As Double(), ByVal ordinate As Double(), ByVal depth As Double(), Optional ByVal options As String = "", Optional ByVal plotType As PlotType = PlotType.SplotXYZ)
            MyBase.New()
            If abscissa Is Nothing Then Throw New ArgumentNullException(NameOf(abscissa))
            If ordinate Is Nothing Then Throw New ArgumentNullException(NameOf(ordinate))
            If depth Is Nothing Then Throw New ArgumentNullException(NameOf(depth))
            If abscissa.Length < 2 Then
                Me.OrdinateSize = 1
            Else
                For candidateSize As Integer = 1 To abscissa.Length - 1
                    If abscissa(candidateSize) <> abscissa(candidateSize - 1) Then
                        Me.OrdinateSize = candidateSize
                        Exit For
                    End If
                Next
            End If
            Me.Depth = depth
            Me.Ordinate = ordinate
            Me.Abscissa = abscissa
            Me.Options = options
            Me.PlotType = plotType
        End Sub

        ''' <summary> Constructor. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
        ''' <param name="plane">    The plane. </param>
        ''' <param name="options">  (Optional) Options for controlling the operation. </param>
        ''' <param name="plotType"> (Optional) Type of the plot. </param>
        Public Sub New(ByVal plane As Double(,), Optional ByVal options As String = "", Optional ByVal plotType As PlotType = PlotType.SplotZZ)
            MyBase.New()
            If plane Is Nothing Then Throw New ArgumentNullException(NameOf(plane))
            Me.Plane = plane
            Me.Options = options
            Me.PlotType = plotType
        End Sub

        ''' <summary> Query if 'functionOrFilename' is file. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="functionOrFilename"> Filename of the function or file. </param>
        ''' <returns> <c>true</c> if file; otherwise <c>false</c> </returns>
        Private Shared Function IsFile(ByVal functionOrFilename As String) As Boolean
            Dim dot As Integer = functionOrFilename.LastIndexOf(".", StringComparison.OrdinalIgnoreCase)
            Return dot >= 1 AndAlso (Char.IsLetter(functionOrFilename.Chars(dot - 1)) OrElse Char.IsLetter(functionOrFilename.Chars(dot + 1)))
        End Function

        ''' <summary> Gets or sets the filename of the file. </summary>
        ''' <value> The name of the file. </value>
        Public ReadOnly Property FileName As String = Nothing

        ''' <summary> Gets or sets the name of the function. </summary>
        ''' <value> The name of the function. </value>
        Public ReadOnly Property FunctionName As String = Nothing

        ''' <summary> Gets or sets the abscissa. </summary>
        ''' <value> The abscissa. </value>
        <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
        Public ReadOnly Property Abscissa As Double()

        ''' <summary> Gets or sets the ordinate. </summary>
        ''' <value> The ordinate. </value>
        <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
        Public ReadOnly Property Ordinate As Double()

        ''' <summary> Gets or sets the depth. </summary>
        ''' <value> The depth. </value>
        <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
        Public ReadOnly Property Depth As Double()

        ''' <summary> Gets or sets the plane. </summary>
        ''' <value> The plane. </value>
        <CodeAnalysis.SuppressMessage("Performance", "CA1819:Properties should not return arrays", Justification:="<Pending>")>
        Public ReadOnly Property Plane As Double(,)

        ''' <summary> Gets or sets the size of the ordinate. </summary>
        ''' <value> The size of the ordinate. </value>
        Public ReadOnly Property OrdinateSize As Integer

        ''' <summary> Gets or sets options for controlling the operation. </summary>
        ''' <value> The options. </value>
        Public ReadOnly Property Options As String

        ''' <summary> Gets or sets the type of the plot. </summary>
        ''' <value> The type of the plot. </value>
        Public ReadOnly Property PlotType As PlotType

        ''' <summary> Gets or sets the label contours. </summary>
        ''' <value> The label contours. </value>
        Public Property LabelContours As Boolean

    End Class

End Namespace
