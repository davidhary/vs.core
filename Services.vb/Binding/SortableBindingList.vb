Imports System.ComponentModel

''' <summary> A Sortable Binding List. </summary>
''' <remarks>
''' Sample code for putting data into a data grid view, which is not sortable by columns when
''' bound with Linq:
''' <code>
''' Dim d = From dev In Program.ue.devices Order By dev.device_id Select dev
''' Dim list As New SortableBindingList(Of device)(d.ToList())
''' dataGridViewDeviceList.DataSource = list
''' </code> <para>
''' (c) 2008 Muigai Mwaura and 2013 Physlcu$ All rights reserved.</para><para>
''' Licensed under The MIT License.</para><para>
''' David, 5/13/2019. Source:
''' https://www.codeproject.com/Articles/31418/Implementing-a-Sortable-BindingList-Very-Very-Quic.
''' </para>
''' </remarks>
Public Class SortableBindingList(Of T)
    Inherits BindingList(Of T)

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="items"> Items with which to populate the list. </param>
    Public Sub New(ByVal items As IList(Of T))
        MyBase.New(items)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="items"> Items with which to populate the list. </param>
    Public Sub New(ByVal items As IEnumerable(Of T))
        Me.New(items.ToList)
    End Sub

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub New()
    End Sub

    ''' <summary> Loads the items into the binding list and resets its binding. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="items"> Items with which to populate the list. </param>
    ''' <returns> A sortable binding list. </returns>
    Public Function Load(ByVal items As IList(Of T)) As SortableBindingList(Of T)
        If items IsNot Nothing AndAlso items.Any Then Me.ResetItems(items)
        Return Me
    End Function

    ''' <summary> Loads the items into the binding list and resets its binding. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="items"> Items with which to populate the list. </param>
    ''' <returns> A sortable binding list. </returns>
    Public Function Load(ByVal items As IEnumerable(Of T)) As SortableBindingList(Of T)
        If items IsNot Nothing AndAlso items.Any Then Me.ResetItems(items)
        Return Me
    End Function

    ''' <summary> Loads the items into the binding list and resets its binding. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="items"> The items with which to populate the binding list. </param>
    Private Sub ResetItems(ByVal items As IList(Of T))
        If items IsNot Nothing AndAlso items.Any Then
            Me.RaiseListChangedEvents = False
            Dim tempList As List(Of T) = items.ToList()
            Me.ClearItems()
            For Each item As T In tempList
                Me.Add(item)
            Next item
            Me.RaiseListChangedEvents = True
            Me.ResetBindings()
        End If
    End Sub

    ''' <summary> Loads the items into the binding list and resets its binding. </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="items"> Items with which to populate the list. </param>
    Private Sub ResetItems(ByVal items As IEnumerable(Of T))
        Me.ResetItems(items.ToList)
    End Sub

    ''' <summary>
    ''' Adds a range of items to the list while suspending the list changed events while the items
    ''' are added.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="items"> Items to add to the binding list. </param>
    Public Sub AddRange(ByVal items As IList(Of T))
        If items IsNot Nothing AndAlso items.Any Then
            Me.RaiseListChangedEvents = False
            For Each item As T In items
                Me.Add(item)
            Next item
            Me.RaiseListChangedEvents = True
            Me.ResetBindings()
        End If
    End Sub

    ''' <summary>
    ''' Adds a range of items to the list while suspending the list changed events while the items
    ''' are added.
    ''' </summary>
    ''' <remarks> David, 2020-09-05. </remarks>
    ''' <param name="items"> Items with which to populate the list. </param>
    Public Sub AddRange(ByVal items As IEnumerable(Of T))
        Me.AddRange(items.ToList)
    End Sub

    ''' <summary> Gets a value indicating whether the list supports sorting. </summary>
    ''' <value>
    ''' <see langword="true" /> if the list supports sorting; otherwise, <see langword="false" />.
    ''' The default is <see langword="false" />.
    ''' </value>
    Protected Overrides ReadOnly Property SupportsSortingCore() As Boolean
        Get
            Return True
        End Get
    End Property
    ''' <summary> True if is sorted, false if not. </summary>
    Private _IsSorted As Boolean

    ''' <summary> Gets a value indicating whether the list is sorted. </summary>
    ''' <value>
    ''' <see langword="true" /> if the list is sorted; otherwise, <see langword="false" />. The
    ''' default is <see langword="false" />.
    ''' </value>
    Protected Overrides ReadOnly Property IsSortedCore() As Boolean
        Get
            Return Me._IsSorted
        End Get
    End Property
    ''' <summary> The sort direction. </summary>
    Private _SortDirection As ListSortDirection

    ''' <summary> Gets the direction the list is sorted. </summary>
    ''' <value>
    ''' One of the <see cref="T:System.ComponentModel.ListSortDirection" /> values. The default is
    ''' <see cref="F:System.ComponentModel.ListSortDirection.Ascending" />.
    ''' </value>
    Protected Overrides ReadOnly Property SortDirectionCore() As ListSortDirection
        Get
            Return Me._SortDirection
        End Get
    End Property
    ''' <summary> The sort property. </summary>
    Private _SortProperty As PropertyDescriptor

    ''' <summary>
    ''' Gets the property descriptor that is used for sorting the list if sorting is implemented in a
    ''' derived class; otherwise, returns <see langword="null" />.
    ''' </summary>
    ''' <value>
    ''' The <see cref="T:System.ComponentModel.PropertyDescriptor" /> used for sorting the list.
    ''' </value>
    Protected Overrides ReadOnly Property SortPropertyCore() As PropertyDescriptor
        Get
            Return Me._SortProperty
        End Get
    End Property

#Disable Warning IDE0052 ' Remove unread private members
    ''' <summary> The property descriptors. </summary>
    Private ReadOnly _PropertyDescriptors As PropertyDescriptorCollection = TypeDescriptor.GetProperties(GetType(T))
#Enable Warning IDE0052 ' Remove unread private members

    ''' <summary>
    ''' Sorts the items if overridden in a derived class; otherwise, throws a
    ''' <see cref="T:System.NotSupportedException" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="prop">      A <see cref="T:System.ComponentModel.PropertyDescriptor" /> that
    '''                          specifies the property to sort on. </param>
    ''' <param name="direction"> One of the <see cref="T:System.ComponentModel.ListSortDirection" />
    '''                          values. </param>
    Protected Overrides Sub ApplySortCore(ByVal prop As PropertyDescriptor, ByVal direction As ListSortDirection)
        If prop Is Nothing Then
            MyBase.ApplySortCore(prop, direction)
        Else
            Me._IsSorted = True
            Me._SortDirection = direction
            Me._SortProperty = prop
            Dim predicate As Func(Of T, Object) = Function(n) n.GetType().GetProperty(prop.Name).GetValue(n, Nothing)
            If Me.Items.Count > 10000 Then
                Me.ResetItems(If(Me._SortDirection = ListSortDirection.Ascending, Me.Items.AsParallel().OrderBy(predicate), Me.Items.AsParallel().OrderByDescending(predicate)))
            Else
                Me.ResetItems(If(Me._SortDirection = ListSortDirection.Ascending, Me.Items.OrderBy(predicate), Me.Items.OrderByDescending(predicate)))
            End If
        End If
    End Sub

    ''' <summary>
    ''' Removes any sort applied with
    ''' <see cref="M:System.ComponentModel.BindingList`1.ApplySortCore(System.ComponentModel.PropertyDescriptor,System.ComponentModel.ListSortDirection)" />
    ''' if sorting is implemented in a derived class; otherwise, raises
    ''' <see cref="T:System.NotSupportedException" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Protected Overrides Sub RemoveSortCore()
        Me._IsSorted = False
        Me._SortDirection = MyBase.SortDirectionCore
        Me._SortProperty = MyBase.SortPropertyCore
        Me.ResetBindings()
    End Sub

End Class

''' <summary> A Simple Sortable Binding List. </summary>
''' <remarks>
''' (c) 2011 Kill Me Please. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 5/13/2019, Source:  </para><para>
''' https://www.codeproject.com/Articles/31418/Implementing-a-Sortable-BindingList-Very-Very-Quic. </para><para>
''' Https://www.codeproject.com/script/Membership/View.aspx?mid=4832730. </para>
''' </remarks>
Public Class SimpleSortableBindingList(Of T)
    Inherits BindingList(Of T)

    ''' <summary> Gets a value indicating whether the list supports sorting. </summary>
    ''' <value>
    ''' <see langword="true" /> if the list supports sorting; otherwise, <see langword="false" />.
    ''' The default is <see langword="false" />.
    ''' </value>
    Protected Overrides ReadOnly Property SupportsSortingCore() As Boolean
        Get
            Return True
        End Get
    End Property

    ''' <summary>
    ''' Sorts the items if overridden in a derived class; otherwise, throws a
    ''' <see cref="T:System.NotSupportedException" />.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="prop">      A <see cref="T:System.ComponentModel.PropertyDescriptor" /> that
    '''                          specifies the property to sort on. </param>
    ''' <param name="direction"> One of the <see cref="T:System.ComponentModel.ListSortDirection" />
    '''                          values. </param>
    Protected Overrides Sub ApplySortCore(ByVal prop As PropertyDescriptor, ByVal direction As ListSortDirection)
        If prop Is Nothing Then
            MyBase.ApplySortCore(prop, direction)
        Else
            Dim modifier As Integer = If(direction = ListSortDirection.Ascending, 1, -1)
            If prop.PropertyType.GetInterface("IComparable") IsNot Nothing Then
                Dim items As List(Of T) = Me.Items.ToList()
                items.Sort(New Comparison(Of T)(Function(a, b)
                                                    Dim aVal As IComparable = TryCast(prop.GetValue(a), IComparable)
                                                    Dim bVal As IComparable = TryCast(prop.GetValue(b), IComparable)
                                                    Return aVal.CompareTo(bVal) * modifier
                                                End Function))
                Me.Items.Clear()
                For Each i As T In items
                    Me.Items.Add(i)
                Next i
            End If
        End If
    End Sub

End Class

