Imports System.ComponentModel

''' <summary> A cross-thread safe binding list. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/14/2019.
''' https://stackoverflow.com/questions/10156991/inotifypropertychanged-causes-cross-thread-error
''' </para>
''' </remarks>
Public Class InvokingBindingList(Of T)
    Inherits System.ComponentModel.BindingList(Of T)

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub New()
        MyBase.New
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="list"> The list. </param>
    Public Sub New(ByVal list As IList(Of T))
        MyBase.New(list)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="list">         The list. </param>
    ''' <param name="synchronizer"> Provides a way to synchronously or asynchronously execute a
    '''                             delegate. </param>
    Public Sub New(ByVal list As IList(Of T), synchronizer As ISynchronizeInvoke)
        Me.New(list)
        Me._Synchronizer = synchronizer
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="synchronizer"> Provides a way to synchronously or asynchronously execute a
    '''                             delegate. </param>
    Public Sub New(synchronizer As ISynchronizeInvoke)
        Me.New()
        Me._Synchronizer = synchronizer
    End Sub

    ''' <summary>
    ''' Gets or sets the synchronizer, which provides a way to synchronously or asynchronously
    ''' execute a delegate.
    ''' </summary>
    ''' <remarks>
    ''' This allows assigning the synchronizer to the class after it gets instantiated withing a non-
    ''' UI class, such as a data entity.
    ''' </remarks>
    ''' <value> The synchronizer. </value>
    Public Property Synchronizer As ISynchronizeInvoke

    ''' <summary>
    ''' Raises the <see cref="E:System.ComponentModel.BindingList`1.ListChanged" /> event.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="e"> A <see cref="T:System.ComponentModel.ListChangedEventArgs" /> that contains
    '''                  the event data. </param>
    Protected Overrides Sub OnListChanged(ByVal e As ListChangedEventArgs)
        If Me.Synchronizer?.InvokeRequired Then
            Me.Synchronizer.Invoke(New Action(Of ListChangedEventArgs)(AddressOf Me.OnListChanged), New Object() {e})
        Else
            MyBase.OnListChanged(e)
        End If
    End Sub

    ''' <summary> Adds list. </summary>
    ''' <remarks> David, 2020-07-20. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="list"> The list. </param>
    Public Overloads Sub Add(ByVal list As IList(Of T))
        If list Is Nothing Then Throw New ArgumentNullException(NameOf(list))
        Dim raiseListChangedEventsWasEnabled As Boolean = Me.RaiseListChangedEvents
        Try
            Me.RaiseListChangedEvents = False
            For Each value As T In list
                Me.Add(value)
            Next
            Me.RaiseListChangedEvents = True
            Me.ResetBindings()
        Catch
            Throw
        Finally
            Me.RaiseListChangedEvents = raiseListChangedEventsWasEnabled
        End Try
    End Sub

End Class

