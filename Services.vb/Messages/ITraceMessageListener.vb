''' <summary> Interface for trace message listener. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface ITraceMessageListener
    Inherits IMessageListener

    ''' <summary> Trace event. </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="id">        The identifier. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Function TraceEvent(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object) As String

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <param name="value"> The event message. </param>
    ''' <returns> The trace message details. </returns>
    Function TraceEvent(ByVal value As TraceMessage) As String

    ''' <summary> Trace event overriding the trace level. </summary>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="id">        The identifier. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Function TraceEventOverride(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object) As String

    ''' <summary> Trace event overriding the trace level. </summary>
    ''' <param name="value"> The event message. </param>
    ''' <returns> A String. </returns>
    Function TraceEventOverride(ByVal value As TraceMessage) As String

    ''' <summary>
    ''' Query if talker with identity as specified in the trace message was identified.
    ''' </summary>
    ''' <param name="value"> The event message. </param>
    ''' <returns> <c>true</c> if talker identified; otherwise <c>false</c> </returns>
    Function IsTalkerIdentified(ByVal value As TraceMessage) As Boolean

    ''' <summary> Identify a talker using the talker identity. </summary>
    ''' <param name="value"> The event message. </param>
    Sub IdentifyTalker(ByVal value As TraceMessage)

    ''' <summary> Enumerates talker identity messages in this collection. </summary>
    ''' <returns>
    ''' An enumerator that allows for each to be used to process talker identity messages in this
    ''' collection.
    ''' </returns>
    Function TalkerIdentityMessages() As IList(Of TraceMessage)

    ''' <summary> Query if 'value' is talker date message published. </summary>
    ''' <param name="value"> The event message. </param>
    ''' <returns> <c>true</c> if talker date message published; otherwise <c>false</c> </returns>
    Function IsTalkerDateMessagePublished(ByVal value As TraceMessage) As Boolean

    ''' <summary> Adds a date message. </summary>
    ''' <param name="value"> The event message. </param>
    Sub AddDateMessage(ByVal value As TraceMessage)

    ''' <summary> Enumerates talker date messages in this collection. </summary>
    ''' <returns>
    ''' An enumerator that allows foreach to be used to process talker date messages in this
    ''' collection.
    ''' </returns>
    Function TalkerDateMessages() As IList(Of TraceMessage)

End Interface

