''' <summary>
''' Trace Message Talker -- broadcasts trace messages to trace message listeners.
''' </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 10/10/2014 </para>
''' </remarks>
Public Class TraceMessageTalker
    Implements ITraceMessageTalker

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub New()
        MyBase.New
        ' this prevents issuing new date messages.
        Me.UpdateLastReportDate()
        Me._Listeners = New MessageListenerCollection
        Me._TraceMessage = TraceMessage.Empty
        Me._TraceLogLevel = TraceEventType.Information
        Me._TraceShowLevel = TraceEventType.Information
    End Sub

#End Region

#Region " LISTENERS "

    ''' <summary> Adds a listener. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="listener"> The listener. </param>
    Public Sub AddListener(ByVal listener As IMessageListener) Implements ITraceMessageTalker.AddListener
        If listener Is Nothing Then Throw New ArgumentNullException(NameOf(listener))
        Me.Listeners.Add(listener)
    End Sub

    ''' <summary> Adds the listeners such as the top level trace messages box and log. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="talker"> The talker. </param>
    Public Sub AddListeners(ByVal talker As ITraceMessageTalker) Implements ITraceMessageTalker.AddListeners
        If talker Is Nothing Then Throw New ArgumentNullException(NameOf(talker))
        Me.AddListeners(talker.Listeners)
    End Sub

    ''' <summary> Adds the listeners such as the top level trace messages box and log. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="listeners"> The listeners. </param>
    Public Sub AddListeners(ByVal listeners As IList(Of IMessageListener)) Implements ITraceMessageTalker.AddListeners
        If listeners Is Nothing Then Throw New ArgumentNullException(NameOf(listeners))
        For Each listener As IMessageListener In listeners
            Me.AddListener(listener)
        Next
    End Sub

    ''' <summary> Removes the listeners if the talker was not assigned. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub RemoveListeners() Implements IPublisher.RemoveListeners
        Me.Listeners?.Clear()
    End Sub
    ''' <summary> The private listeners. </summary>
    Private _PrivateListeners As List(Of IMessageListener)

    ''' <summary> Gets the private listeners. </summary>
    ''' <value> The private listeners. </value>
    Public ReadOnly Property PrivateListeners As IList(Of IMessageListener) Implements IPublisher.PrivateListeners
        Get
            If Me._PrivateListeners Is Nothing Then Me._PrivateListeners = New List(Of IMessageListener)
            Return Me._PrivateListeners
        End Get
    End Property

    ''' <summary> Adds a private listener. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="listener"> The listener. </param>
    Public Sub AddPrivateListener(ByVal listener As IMessageListener) Implements IPublisher.AddPrivateListener
        If Me.PrivateListeners IsNot Nothing Then
            Me._PrivateListeners.Add(listener)
        End If
        Me.AddListener(listener)
    End Sub

    ''' <summary> Adds private listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="listeners"> The listeners. </param>
    Public Sub AddPrivateListeners(ByVal listeners As IList(Of IMessageListener)) Implements IPublisher.AddPrivateListeners
        If listeners Is Nothing Then Throw New ArgumentNullException(NameOf(listeners))
        If Me.PrivateListeners IsNot Nothing Then
            Me._PrivateListeners.AddRange(listeners)
        End If
        Me.AddListeners(listeners)
    End Sub

    ''' <summary> Adds private listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="talker"> The talker. </param>
    Public Sub AddPrivateListeners(ByVal talker As ITraceMessageTalker) Implements IPublisher.AddPrivateListeners
        If talker Is Nothing Then Throw New ArgumentNullException(NameOf(talker))
        Me.AddPrivateListeners(talker.Listeners)
    End Sub

    ''' <summary> Removes the private listener described by listener. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="listener"> The listener. </param>
    Public Sub RemovePrivateListener(ByVal listener As IMessageListener) Implements IPublisher.RemovePrivateListener
        Me.RemoveListener(listener)
        If Me.PrivateListeners IsNot Nothing Then
            Me._PrivateListeners.Remove(listener)
        End If
    End Sub

    ''' <summary> Removes the private listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub RemovePrivateListeners() Implements IPublisher.RemovePrivateListeners
        For Each listener As IMessageListener In Me.PrivateListeners
            Me.RemoveListener(listener)
        Next
        Me._PrivateListeners.Clear()
    End Sub

    ''' <summary> Removes the listener described by listener. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="listener"> The listener. </param>
    Public Sub RemoveListener(ByVal listener As IMessageListener) Implements IPublisher.RemoveListener
        Me.Listeners?.Remove(listener)
    End Sub

    ''' <summary> Removes the listeners if the talker was not assigned. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="listeners"> The listeners. </param>
    Public Sub RemoveListeners(ByVal listeners As IList(Of IMessageListener)) Implements IPublisher.RemoveListeners
        If listeners Is Nothing Then Throw New ArgumentNullException(NameOf(listeners))
        For Each listener As IMessageListener In listeners
            Me.RemoveListener(listener)
        Next
    End Sub

    ''' <summary> Gets or sets the listeners. </summary>
    ''' <value> The listeners. </value>
    Public ReadOnly Property Listeners As MessageListenerCollection Implements ITraceMessageTalker.Listeners

    ''' <summary> Gets or sets the trace level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceLogLevel As TraceEventType Implements ITraceMessageTalker.TraceLogLevel

    ''' <summary> Gets or sets the trace level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceShowLevel As TraceEventType Implements ITraceMessageTalker.TraceShowLevel

    ''' <summary> Applies the listener trace level. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="listenerType"> Type of the trace level. </param>
    ''' <param name="value">        The value. </param>
    Public Sub ApplyListenerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType) Implements IPublisher.ApplyListenerTraceLevel
        Me.Listeners.ApplyTraceLevel(listenerType, value)
    End Sub

    ''' <summary> Applies the trace level type to all talkers. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="listenerType"> Type of the trace level. </param>
    ''' <param name="value">        The value. </param>
    Public Sub ApplyTalkerTraceLevel(ByVal listenerType As ListenerType, ByVal value As TraceEventType) Implements IPublisher.ApplyTalkerTraceLevel
        If listenerType = ListenerType.Logger Then
            Me.TraceLogLevel = value
        Else
            Me.TraceShowLevel = value
        End If
    End Sub

    ''' <summary> Applies the talker trace levels described by talker. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="talker"> The talker. </param>
    Public Sub ApplyTalkerTraceLevels(ByVal talker As ITraceMessageTalker) Implements ITraceMessageTalker.ApplyTalkerTraceLevels
        If talker Is Nothing Then Throw New ArgumentNullException(NameOf(talker))
        Me.ApplyTalkerTraceLevel(ListenerType.Logger, talker.TraceLogLevel)
        Me.ApplyTalkerTraceLevel(ListenerType.Display, talker.TraceShowLevel)
    End Sub

    ''' <summary> Applies the listener trace levels described by talker. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="talker"> The talker. </param>
    Public Sub ApplyListenerTraceLevels(ByVal talker As ITraceMessageTalker) Implements ITraceMessageTalker.ApplyListenerTraceLevels
        If talker Is Nothing Then Throw New ArgumentNullException(NameOf(talker))
        Me.ApplyListenerTraceLevel(ListenerType.Logger, talker.TraceLogLevel)
        Me.ApplyListenerTraceLevel(ListenerType.Display, talker.TraceShowLevel)
    End Sub

#End Region

#Region " PUBLISH "

    ''' <summary>
    ''' Determines if the message with the specified level is publishable on the listener type.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="listenerType"> Type of the listener. </param>
    ''' <param name="value">        The value. </param>
    ''' <returns> <c>true</c> if publishable; otherwise <c>false</c> </returns>
    Public Function Publishable(ByVal listenerType As ListenerType, ByVal value As TraceEventType) As Boolean Implements ITraceMessageTalker.Publishable
        Return If(listenerType = ListenerType.Logger, value <= Me.TraceLogLevel, value <= Me.TraceShowLevel)
    End Function

    ''' <summary> Publishes the message. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Public Function Publish(ByVal eventType As TraceEventType, ByVal id As Integer,
                            ByVal format As String, ByVal ParamArray args() As Object) As String Implements ITraceMessageTalker.Publish
        Return Me.Publish(New TraceMessage(eventType, id, format, args))
    End Function

    ''' <summary> Publishes the message. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    ''' <returns> A String. </returns>
    Public Function Publish(ByVal value As TraceMessage) As String Implements ITraceMessageTalker.Publish
        Dim details As String = String.Empty
        If value IsNot Nothing Then
            If Me.IsNewReportDate Then Me.HandleDateChange()
            Me._TraceMessage = New TraceMessage(value)
            details = value.Details
            Me.PublishThis(value)
        End If
        Return details
    End Function

    ''' <summary> Publishes the message (private access). </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Private Sub PublishThis(ByVal value As TraceMessage)
        If Me.Listeners IsNot Nothing Then
            For Each listener As ITraceMessageListener In Me.Listeners
                If listener IsNot Nothing AndAlso Not listener.IsDisposed AndAlso
                    Me.Publishable(listener.ListenerType, value.EventType) Then
                    listener.TraceEvent(value)
                    My.MyLibrary.DoEvents()
                End If
            Next
        End If
    End Sub

    ''' <summary> Publishes overriding the listeners trace level. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Public Function PublishOverride(ByVal eventType As TraceEventType, ByVal id As Integer,
                            ByVal format As String, ByVal ParamArray args() As Object) As String Implements ITraceMessageTalker.PublishOverride
        Return Me.PublishOverride(New TraceMessage(eventType, id, format, args))
    End Function

    ''' <summary> Publishes overriding the listeners trace level. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    ''' <returns> A String. </returns>
    Public Function PublishOverride(ByVal value As TraceMessage) As String Implements ITraceMessageTalker.PublishOverride
        Dim details As String = String.Empty
        If value IsNot Nothing Then
            If Me.IsNewReportDate Then Me.HandleDateChange()
            Me._TraceMessage = New TraceMessage(value)
            details = value.Details
            Me.PublishOverrideThis(value)
        End If
        Return details
    End Function

    ''' <summary> Publish override (private access). </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Private Sub PublishOverrideThis(ByVal value As TraceMessage)
        If Me.Listeners IsNot Nothing Then
            For Each listener As ITraceMessageListener In Me.Listeners
                If listener IsNot Nothing AndAlso Not listener.IsDisposed Then
                    listener.TraceEventOverride(value)
                    My.MyLibrary.DoEvents()
                End If
            Next
        End If
    End Sub

    ''' <summary> Gets or sets a message describing the trace. </summary>
    ''' <value> A message describing the trace. </value>
    Public ReadOnly Property TraceMessage As TraceMessage Implements ITraceMessageTalker.TraceMessage

#End Region

#Region " IDENTIFY "

    ''' <summary> Identify talker. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    Public Sub IdentifyTalker(ByVal value As TraceMessage) Implements ITraceMessageTalker.IdentifyTalker
        If Me.Listeners IsNot Nothing Then
            For Each listener As ITraceMessageListener In Me.Listeners
                If listener IsNot Nothing AndAlso Not listener.IsDisposed Then
                    If Not listener.IsTalkerIdentified(value) Then
                        listener.TraceEventOverride(value)
                        listener.IdentifyTalker(value)
                        My.MyLibrary.DoEvents()
                    End If
                End If
            Next
        End If
    End Sub

    ''' <summary> Identify talkers. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub IdentifyTalkers()
        If Me.Listeners IsNot Nothing Then
            For Each listener As ITraceMessageListener In Me.Listeners
                If listener IsNot Nothing AndAlso Not listener.IsDisposed Then
                    For Each value As TraceMessage In listener.TalkerIdentityMessages
                        listener.TraceEventOverride(value)
                        My.MyLibrary.DoEvents()
                    Next
                End If
            Next
        End If
    End Sub

#End Region

#Region " DATE CHANGED "

    ''' <summary> Publishes date message. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    ''' <returns> A String. </returns>
    Public Function PublishDateMessage(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object) As String Implements ITraceMessageTalker.PublishDateMessage
        Return Me.PublishDateMessage(New TraceMessage(eventType, id, format, args))
    End Function

    ''' <summary> Publishes date message. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The <see cref="TraceMessage">message</see> to display and log. </param>
    ''' <returns> A String. </returns>
    Public Function PublishDateMessage(ByVal value As TraceMessage) As String Implements ITraceMessageTalker.PublishDateMessage
        If value Is Nothing Then
            Return String.Empty
        Else
            If Me.Listeners IsNot Nothing Then
                For Each listener As ITraceMessageListener In Me.Listeners
                    If listener IsNot Nothing AndAlso Not listener.IsDisposed Then
                        If Not listener.IsTalkerDateMessagePublished(value) Then
                            listener.TraceEventOverride(value)
                            listener.AddDateMessage(value)
                            My.MyLibrary.DoEvents()
                        End If
                    End If
                Next
            End If
            Return value.Details
        End If
    End Function

    ''' <summary> Publish date messages. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub PublishDateMessages()
        If Me.Listeners IsNot Nothing Then
            For Each listener As ITraceMessageListener In Me.Listeners
                If listener IsNot Nothing AndAlso Not listener.IsDisposed Then
                    For Each value As TraceMessage In listener.TalkerDateMessages
                        listener.TraceEventOverride(value)
                        My.MyLibrary.DoEvents()
                    Next
                End If
            Next
        End If
    End Sub

    ''' <summary> Updates the last report date. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Private Sub UpdateLastReportDate()
        Me.LastReportDate = DateTimeOffset.Now.Date
    End Sub

    ''' <summary> Query if this object is new report date. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> <c>true</c> if new report date; otherwise <c>false</c> </returns>
    Private Function IsNewReportDate() As Boolean
        Return DateTimeOffset.Now.Date > Me.LastReportDate
    End Function

    ''' <summary> Gets or sets the last report date in local time. </summary>
    ''' <remarks>
    ''' MS Logging using <see cref="Logging.LogFileCreationScheduleOption.Daily">daily</see> file
    ''' creation, creates the new file on a local date change. In congruence, the
    ''' <see cref="LastReportDate">last report date</see> represents local time.
    ''' </remarks>
    ''' <value> The last report date in local time. </value>
    Private Property LastReportDate As DateTimeOffset

    ''' <summary> Handles the date change. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Private Sub HandleDateChange()
        Me.UpdateLastReportDate()
        ' on a new date, publish all date messages and identify all talker
        Me.PublishDateMessages()
        Me.IdentifyTalkers()
    End Sub

#End Region

End Class

