''' <summary> Defines a Trace Message. </summary>
''' <remarks>
''' (c) 2013 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 09/04/2013, 1.2.4955. based on the legacy extended message. </para>
''' </remarks>
<Serializable()>
Public Class TraceMessage

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="format">    Describes the format to use. </param>
    ''' <param name="args">      A variable-length parameters list containing arguments. </param>
    Public Sub New(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(eventType, id, String.Format(format, args))
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="details">   The details. </param>
    Public Sub New(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal details As String)
        MyBase.New()
        Me.Initialize(eventType, id, details)
    End Sub

    ''' <summary> Initializes this object. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <param name="id">        The identifier to use with the trace event. </param>
    ''' <param name="details">   The details. </param>
    Private Sub Initialize(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal details As String)
        Me._Timestamp = DateTimeOffset.Now
        Me.EventType = eventType
        Me.Id = id
        Me.SynopsisDelimiter = TraceMessage.DefaultSynopsisDelimiter
        Me.TraceMessageFormat = TraceMessage.DefaultTraceMessageFormat
        Me.Details = details
    End Sub

    ''' <summary> Makes a deep copy of this object. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The value. </param>
    Private Sub DeepCopy(ByVal value As TraceMessage)
        If value Is Nothing Then
            Me.Clone(TraceMessage.Empty)
        Else
            Me.Initialize(value.EventType, value.Id, value.Details)
            Me._Timestamp = value.Timestamp
        End If
    End Sub

    ''' <summary> Makes a deep copy of this object. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub Clone(ByVal value As TraceMessage)
        Me.DeepCopy(value)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub New(ByVal value As TraceMessage)
        Me.New(TraceEventType.Information, 0, String.Empty)
        Me.Clone(value)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Private Sub New()
        Me.New(TraceEventType.Information, 0, String.Empty)
    End Sub

    ''' <summary> Gets an empty <see cref="TraceMessage">Trace Message</see>. </summary>
    ''' <value> The empty. </value>
    Public Shared ReadOnly Property Empty() As TraceMessage
        Get
            Return New TraceMessage()
        End Get
    End Property

#End Region

#Region " EQUALS "

    ''' <summary> Tests if two TraceMessage objects are considered equal. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <param name="same">  Trace message to be compared. </param>
    ''' <returns> <c>true</c> if the objects are considered equal, false if they are not. </returns>
    Public Overloads Shared Function Equals(ByVal value As TraceMessage, ByVal same As TraceMessage) As Boolean
        Return (value Is Nothing AndAlso same Is Nothing) OrElse
            (value IsNot Nothing AndAlso same IsNot Nothing AndAlso String.Equals(value.Details, same.Details))
    End Function

#End Region

#Region " SYNOPSIS "

    ''' <summary> The default synopsis delimiter. </summary>
    Public Const DefaultSynopsisDelimiter As String = ";. "

    ''' <summary> Gets or sets the synopsis delimiter. </summary>
    ''' <value> The synopsis delimiter. </value>
    Public Property SynopsisDelimiter As String

    ''' <summary> Synopsis index. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value">     The value. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <returns> An Integer. </returns>
    Public Shared Function SynopsisIndex(ByVal value As String, ByVal delimiter As String) As Integer
        Return If(String.IsNullOrWhiteSpace(delimiter) OrElse String.IsNullOrWhiteSpace(value),
            0,
            value.IndexOf(delimiter, StringComparison.OrdinalIgnoreCase))
    End Function

    ''' <summary> Extracts the synopsis. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value">     The value. </param>
    ''' <param name="delimiter"> The delimiter. </param>
    ''' <param name="maxLength"> The maximum length. </param>
    ''' <returns> The extracted synopsis. </returns>
    Public Shared Function ExtractSynopsis(ByVal value As String, ByVal delimiter As String, ByVal maxLength As Integer) As String
        Dim synopsisIndex As Integer = TraceMessage.SynopsisIndex(value, delimiter)
        If synopsisIndex > 0 Then
            If maxLength > 0 AndAlso synopsisIndex > maxLength Then synopsisIndex = maxLength
            Return value.Substring(0, synopsisIndex)
        Else
            Return String.Empty
        End If
    End Function

    ''' <summary> Gets or sets the length of the maximum synopsis. </summary>
    ''' <value> The length of the maximum synopsis. </value>
    Public Property MaxSynopsisLength As Integer

    ''' <summary> Extracts the synopsis. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The extracted synopsis. </returns>
    Public Function ExtractSynopsis() As String
        Return TraceMessage.ExtractSynopsis(Me.Details, Me.SynopsisDelimiter, Me.MaxSynopsisLength)
    End Function

    ''' <summary> Extracts the synopsis. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="maxLength"> The maximum length. </param>
    ''' <returns> The extracted synopsis. </returns>
    Public Function ExtractSynopsis(ByVal maxLength As Integer) As String
        Return TraceMessage.ExtractSynopsis(Me.Details, Me.SynopsisDelimiter, maxLength)
    End Function

#End Region

#Region " DETAILS "

    ''' <summary> Gets or sets the Trace Message. </summary>
    ''' <value> The details. </value>
    Public Property Details() As String

    ''' <summary> Gets or sets the Trace Message time stamp as <see cref="DateTimeOffset"/>. </summary>
    ''' <value> The timestamp. </value>
    Public ReadOnly Property Timestamp() As DateTimeOffset

    ''' <summary> Gets or sets the <see cref="TraceEventType">event type</see>. </summary>
    ''' <value> The <see cref="TraceEventType">event type</see>. </value>
    Public Property EventType() As System.Diagnostics.TraceEventType

    ''' <summary> Gets or sets the identifier to use with the trace event. </summary>
    ''' <value> The identifier to use with the trace event. </value>
    Public Property Id As Integer

#End Region

#Region " TO STRING "

    ''' <summary> The default trace message format. </summary>
    Public Const DefaultTraceMessageFormat As String = "{0},{1,5:X},{2:HH:mm:ss.fff}Z,{3}"

    ''' <summary> Gets or sets the default format for displaying the message. </summary>
    ''' <remarks>
    ''' The format must include 4 elements to display the first two characters of the trace event
    ''' type, the id, timestamp and message details. For example,<code>
    ''' "{0},{1,5:X},{2:HH:mm:ss.fff}Z,{3}"</code>.
    ''' </remarks>
    ''' <value> The trace message format. </value>
    Public Property TraceMessageFormat() As String

    ''' <summary> Returns a message based on the default format. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns>
    ''' A representation of the trace message based on the <see cref="TraceMessageFormat">message
    ''' format</see>.
    ''' </returns>
    Public Overrides Function ToString() As String
        Return If(String.IsNullOrWhiteSpace(Me.TraceMessageFormat),
            Me.ToString(TraceMessage.DefaultTraceMessageFormat),
            Me.ToString(Me.TraceMessageFormat))
    End Function

    ''' <summary> Returns a message based on the default format. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="format"> Describes the format to use. </param>
    ''' <returns>
    ''' A representation of the trace message based on the <see cref="TraceMessageFormat">message
    ''' format</see>.
    ''' </returns>
    Public Overloads Function ToString(ByVal format As String) As String
        Return String.Format(Globalization.CultureInfo.CurrentCulture, format,
                             Me.EventType.ToString.Substring(0, 2), Me.Id, Me.Timestamp.UtcDateTime, Me.Details)
    End Function

#End Region

End Class

