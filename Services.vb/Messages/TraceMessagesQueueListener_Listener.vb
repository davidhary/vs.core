
Partial Public Class TraceMessagesQueueListener
    Implements IMessageListener

    ''' <summary> Gets the is disposed. </summary>
    ''' <value> The is disposed. </value>
    Public ReadOnly Property IsDisposed As Boolean Implements IMessageListener.IsDisposed

    ''' <summary> Writes. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="message"> The message to add. </param>
    Public Sub Write(message As String) Implements IMessageListener.Write
        Me.Enqueue(message)
    End Sub

    ''' <summary> Writes a line. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="message"> The message to add. </param>
    Public Sub WriteLine(message As String) Implements IMessageListener.WriteLine
        Me.Enqueue(message)
    End Sub

    ''' <summary> Registers this event. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="level"> The level. </param>
    Public Overridable Sub Register(level As TraceEventType) Implements IMessageListener.Register
    End Sub

    ''' <summary> Gets the trace level. </summary>
    ''' <value> The trace level. </value>
    Public Overridable Property TraceLevel As TraceEventType Implements IMessageListener.TraceLevel

    ''' <summary> Checks if the log should trace the event type. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> If the log should trace; <c>False</c> otherwise. </returns>
    Public Function ShouldTrace(ByVal value As TraceEventType) As Boolean Implements IMessageListener.ShouldTrace
        Return value <= Me.TraceLevel
    End Function

    ''' <summary> Gets the sentinel indicating this listener as thread safe. </summary>
    ''' <value> True if thread safe. </value>
    Public Overridable ReadOnly Property IsThreadSafe As Boolean Implements IMessageListener.IsThreadSafe
        Get
            Return True
        End Get
    End Property

    ''' <summary> Gets a unique identifier. </summary>
    ''' <value> The identifier of the unique. </value>
    Protected ReadOnly Property UniqueId As Guid = Guid.NewGuid Implements IMessageListener.UniqueId

    ''' <summary> Tests if this ITraceMessageListener is considered equal to another. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="other"> The i trace message listener to compare to this object. </param>
    ''' <returns>
    ''' <c>true</c> if the objects are considered equal, <c>false</c> if they are not.
    ''' </returns>
    Public Overloads Function Equals(other As IMessageListener) As Boolean Implements IEquatable(Of IMessageListener).Equals
        Return other IsNot Nothing AndAlso Guid.Equals(other.UniqueId, Me.UniqueId)
    End Function

    ''' <summary> Applies the trace level this described by value. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
    Private Sub ApplyTraceLevelThis(ByVal value As TraceEventType)
        Me._TraceLevel = value
    End Sub

    ''' <summary> Applies the trace level. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
    Public Overloads Sub ApplyTraceLevel(ByVal value As TraceEventType) Implements IMessageListener.ApplyTraceLevel
        Me.ApplyTraceLevelThis(value)
    End Sub

    ''' <summary> Gets the type of the listener. </summary>
    ''' <value> The type of the listener. </value>
    Public ReadOnly Property ListenerType As ListenerType Implements IMessageListener.ListenerType
        Get
            Return ListenerType.Display
        End Get
    End Property

End Class
