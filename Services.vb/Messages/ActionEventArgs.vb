''' <summary> Event arguments returning action level and details. </summary>
''' <remarks>
''' (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 4/8/2018 </para>
''' </remarks>
Public Class ActionEventArgs
    Inherits System.EventArgs

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Gets the default action failure limen is set to <see cref="TraceEventType.Error"/>.
    ''' </summary>
    ''' <value> The default action failure limen. </value>
    Public Shared ReadOnly Property DefaultActionFailureLimen As TraceEventType = TraceEventType.Error

    ''' <summary>
    ''' Default constructor. Set the <see cref="FailureLimen"/> to
    ''' <see cref="DefaultActionFailureLimen"/>.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub New()
        Me.New(TraceEventType.Error)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="failureLimen"> The failure threshold level. </param>
    Public Sub New(ByVal failureLimen As TraceEventType)
        MyBase.New()
        Me.FailureLimen = failureLimen
        Me.ClearThis()
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Private Sub ClearThis()
        Me._HasOutcomeEvent = False
        Me._Details = String.Empty
        Me._OutcomeEvent = TraceEventType.Information
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub Clear()
        Me.ClearThis()
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets an empty <see cref="ActionEventArgs">event arguments</see>. </summary>
    ''' <value> The empty. </value>
    Public Shared Shadows ReadOnly Property Empty() As ActionEventArgs
        Get
            Return New ActionEventArgs
        End Get
    End Property

    ''' <summary>
    ''' The action is tagged as failed if the <see cref="OutcomeEvent"/> is lower or equals the
    ''' <see cref="FailureLimen"/>.
    ''' </summary>
    ''' <value> The failure limen. </value>
    Public ReadOnly Property FailureLimen As TraceEventType

    ''' <summary> Gets the has outcome event. </summary>
    ''' <value> The has outcome event. </value>
    Public ReadOnly Property HasOutcomeEvent As Boolean

    ''' <summary> Gets the action outcome level. </summary>
    ''' <value> The action outcome level. </value>
    Public ReadOnly Property OutcomeEvent As TraceEventType

    ''' <summary>
    ''' Affirmative if the action failed; that is, the <see cref="OutcomeEvent"/> was at or lower
    ''' than the <see cref="FailureLimen"/>.
    ''' </summary>
    ''' <value> <c>True</c> if action failed; <c>False</c> otherwise. </value>
    Public ReadOnly Property Failed() As Boolean
        Get
            Return Me.OutcomeEvent <= Me.FailureLimen
        End Get
    End Property

    ''' <summary>
    ''' Affirmative if the <see cref="OutcomeEvent"/> was at or lower than the
    ''' <see cref="TraceEventType.Error"/> value.
    ''' </summary>
    ''' <value> <c>True</c> if action failed; <c>False</c> otherwise. </value>
    Public ReadOnly Property Erred() As Boolean
        Get
            Return Me.OutcomeEvent <= TraceEventType.Error
        End Get
    End Property

    ''' <summary> Gets the details. </summary>
    ''' <value> The details. </value>
    Public ReadOnly Property Details As String

    ''' <summary> Gets the has details. </summary>
    ''' <value> The has details. </value>
    Public ReadOnly Property HasDetails As Boolean
        Get
            Return Not String.IsNullOrWhiteSpace(Me.Details)
        End Get
    End Property

    ''' <summary> Registers an outcome event and record the event details. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="outcomeEvent"> The outcome. </param>
    ''' <param name="details">      The details. </param>
    Public Sub RegisterOutcomeEvent(ByVal outcomeEvent As TraceEventType, ByVal details As String)
        Me._HasOutcomeEvent = True
        Me._OutcomeEvent = outcomeEvent
        Me._Details = details
    End Sub

    ''' <summary> Registers an outcome event and record the event details. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="outcome"> The outcome. </param>
    ''' <param name="format">  Specifies the message formatting string. </param>
    ''' <param name="args">    Specifies the arguments. </param>
    Public Sub RegisterOutcomeEvent(ByVal outcome As TraceEventType, format As String, ParamArray args() As Object)
        Me.RegisterOutcomeEvent(outcome, String.Format(format, args))
    End Sub

    ''' <summary> Registers an outcome event as information and record the event details. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="details"> The details. </param>
    Public Sub RegisterOutcomeEvent(ByVal details As String)
        Me.RegisterOutcomeEvent(TraceEventType.Information, details)
    End Sub

    ''' <summary> Registers an outcome event as information and record the event details. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="format"> Specifies the message formatting string. </param>
    ''' <param name="args">   Specifies the arguments. </param>
    Public Sub RegisterOutcomeEvent(format As String, ParamArray args() As Object)
        Me.RegisterOutcomeEvent(String.Format(format, args))
    End Sub

    ''' <summary> Registers a Failure. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="details"> The details. </param>
    Public Sub RegisterFailure(details As String)
        Me.RegisterOutcomeEvent(Me.FailureLimen, details)
    End Sub

    ''' <summary> Registers a Failure. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="format"> Specifies the message formatting string. </param>
    ''' <param name="args">   Specifies the arguments. </param>
    Public Sub RegisterFailure(format As String, ParamArray args() As Object)
        Me.RegisterFailure(String.Format(format, args))
    End Sub

    ''' <summary> Registers an Error. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="details"> The details. </param>
    Public Sub RegisterError(details As String)
        Me.RegisterOutcomeEvent(TraceEventType.Error, details)
    End Sub

    ''' <summary> Registers an Error. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="format"> Specifies the message formatting string. </param>
    ''' <param name="args">   Specifies the arguments. </param>
    Public Sub RegisterError(format As String, ParamArray args() As Object)
        Me.RegisterError(String.Format(format, args))
    End Sub

#End Region

#Region " OBSOLETED "

    ''' <summary> Query if this object is action failed. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> <c>true</c> if action failed; otherwise <c>false</c> </returns>
    <Obsolete("replaced with Failed and Erred")>
    Public Function IsActionFailed() As Boolean
        Return Me.OutcomeEvent <= Me.FailureLimen
    End Function

    ''' <summary> Gets or sets the canceling sentinel. </summary>
    ''' <value> The cancel. </value>
    <Obsolete("replaced with Failed and Erred")>
    Public ReadOnly Property Cancel As Boolean
        Get
            Return Me.Failed
        End Get
    End Property

    ''' <summary> Registers the cancellation. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="details"> The details. </param>
    <Obsolete("replaced with register failed or register error")>
    Public Sub RegisterCancellation(details As String)
        Me.RegisterFailure(details)
    End Sub

    ''' <summary> Registers the cancellation. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="format"> Specifies the message formatting string. </param>
    ''' <param name="args">   Specifies the arguments. </param>
    <Obsolete("replaced with register failed or register error")>
    Public Sub RegisterCancellation(format As String, ParamArray args() As Object)
        Me.RegisterFailure(String.Format(format, args))
    End Sub

#End Region

End Class

