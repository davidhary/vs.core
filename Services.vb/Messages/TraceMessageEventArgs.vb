﻿''' <summary>
''' Defines an event arguments class for <see cref="TraceMessage">trace messages</see>.
''' </summary>
''' <remarks>
''' (c) 2012 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para>
''' </remarks>
Public Class TraceMessageEventArgs

    Inherits System.EventArgs

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TraceMessageEventArgs" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="traceMessage"> A message describing the trace. </param>
    Public Sub New(ByVal traceMessage As TraceMessage)
        MyBase.New()
        Me.TraceMessage = traceMessage
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TraceMessageEventArgs" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="id">      The identifier. </param>
    ''' <param name="details"> The details. </param>
    Public Sub New(ByVal id As Integer, ByVal details As String)
        Me.New(Diagnostics.TraceEventType.Information, id, details)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TraceMessageEventArgs" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="severity"> The severity. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="details">  The details. </param>
    Public Sub New(ByVal severity As Diagnostics.TraceEventType, ByVal id As Integer, ByVal details As String)
        Me.New(New TraceMessage(severity, id, details))
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the <see cref="TraceMessageEventArgs" /> class.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="severity"> The severity. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="format">   The format. </param>
    ''' <param name="args">     The arguments for the format statement. </param>
    Public Sub New(ByVal severity As Diagnostics.TraceEventType, ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        Me.New(New TraceMessage(severity, id, format, args))
    End Sub

#End Region

#Region " PROPERTIES "

    ''' <summary> Gets a message describing the trace. </summary>
    ''' <value> A message describing the trace. </value>
    Public Property TraceMessage As TraceMessage

    ''' <summary> Gets the message severity. </summary>
    ''' <value> The severity. </value>
    Public ReadOnly Property Severity As Diagnostics.TraceEventType
        Get
            Return Me.TraceMessage.EventType
        End Get
    End Property

    ''' <summary> Gets the message details. </summary>
    ''' <value> The details. </value>
    Public ReadOnly Property Details As String
        Get
            Return Me.TraceMessage.Details
        End Get
    End Property

#End Region

End Class
