Imports System.Collections.Concurrent

Imports isr.Core.StackTraceExtensions

''' <summary> Stack of trace messages. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/23/2018 </para>
''' </remarks>
Public Class TraceMessagesStack

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub New()
        MyBase.New()
        Me._ContentsStack = New ConcurrentStack(Of TraceMessage)
        Me.ClearThis()
    End Sub

#End Region

#Region " PEEK "

    ''' <summary> Gets the content. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The content. </returns>
    Public Function TryPeek() As TraceMessage
        Dim result As TraceMessage = Nothing
        Me.GetContentsStack.TryPeek(result)
        Return result
    End Function

#End Region

#Region " PUSH "

    ''' <summary> Gets any. </summary>
    ''' <value> any. </value>
    Public ReadOnly Property Any As Boolean
        Get
            Return Me.GetContentsStack.Any
        End Get
    End Property

    ''' <summary> Gets the trace messages. </summary>
    ''' <value> The trace messages. </value>
    Public ReadOnly Property TraceMessages As IEnumerable(Of TraceMessage)
        Get
            Return Me.GetContentsStack.ToArray
        End Get
    End Property

    ''' <summary> Gets or sets a stack of contents. </summary>
    ''' <value> A stack of contents. </value>
    Protected ReadOnly Property ContentsStack As ConcurrentStack(Of TraceMessage)

    ''' <summary> Gets contents Stack. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The contents Stack. </returns>
    Private Function GetContentsStack() As ConcurrentStack(Of TraceMessage)
        Return Me._ContentsStack
    End Function

    ''' <summary> Gets the content. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The content. </returns>
    Public Function TryPop() As TraceMessage
        Dim result As TraceMessage = Nothing
        Me.GetContentsStack.TryPop(result)
        Return result
    End Function

    ''' <summary> Query if the contents Stack is empty. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> <c>true</c> the contents Stack is empty; otherwise <c>false</c> </returns>
    Public Function IsEmpty() As Boolean
        Return Me.GetContentsStack.IsEmpty
    End Function

    ''' <summary> Fetches the content. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The content. </returns>
    Public Function PopContent() As String
        Dim messageNumber As Integer = 0
        Dim builder As New System.Text.StringBuilder
        Do
            Dim value As TraceMessage = Me.TryPop()

            If value IsNot Nothing Then
                messageNumber += 1
                builder.AppendFormat("{0}:>", messageNumber)
                builder.AppendLine(value.ToString)
            End If
        Loop Until Me.GetContentsStack.IsEmpty
        Return builder.ToString.TrimEnd(Environment.NewLine.ToCharArray)
    End Function

#End Region

#Region " PUSH "

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Private Sub ClearThis()
        Me._ContentsStack.Clear()
        Me._LastMessagePushed = New TraceMessage(TraceEventType.Verbose, 0, "Cleared")
        Me._MessagePushedEventHandlers?.Send(Me, System.EventArgs.Empty)
    End Sub

    ''' <summary> Clears this object to its blank/initial state. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub Clear()
        Me.Clear()
    End Sub

    ''' <summary> Gets or sets the last message pushed. </summary>
    ''' <value> The last message pushed. </value>
    Public ReadOnly Property LastMessagePushed As TraceMessage

    ''' <summary> Adds a message. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    Protected Function Push(ByVal value As String) As String
        If String.IsNullOrWhiteSpace(value) Then value = String.Empty
        Me.Push(New TraceMessage(TraceEventType.Information, My.MyLibrary.TraceEventId, value))
        Return value
    End Function

    ''' <summary> Adds a message. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub Push(ByVal value As TraceMessage)
        If value Is Nothing OrElse String.IsNullOrWhiteSpace(value.Details) Then
            Debug.Assert(Not Debugger.IsAttached, "Empty trace message", New StackTrace(True).UserCallStack(0, 0))
        Else
            Dim lastCount As Long = CType(Me.GetContentsStack?.LongCount, Long?).GetValueOrDefault(0)
            Me.GetContentsStack()?.Push(value)
            If CType(Me.GetContentsStack?.LongCount, Long?).GetValueOrDefault(0) > lastCount Then
                Me._MessagePushedEventHandlers?.Send(Me, System.EventArgs.Empty)
            End If
        End If
    End Sub

#Region " MESSAGE PUSHED "

    ''' <summary> Removes the MessagePushed event handlers. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Protected Sub RemoveMessagePushedEventHandlers()
        Me._MessagePushedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The MessagePushed event handlers. </summary>
    Private ReadOnly _MessagePushedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event Stack for all listeners interested in MessagePushed events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event MessagePushed As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._MessagePushedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._MessagePushedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._MessagePushedEventHandlers.Send(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="MessagePushed">MessagePushed Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyMessagePushed(ByVal e As System.EventArgs)
        Me._MessagePushedEventHandlers.Send(Me, e)
    End Sub

#End Region

#End Region

End Class
