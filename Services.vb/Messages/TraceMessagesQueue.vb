Imports System.Collections.Concurrent

Imports isr.Core.StackTraceExtensions

''' <summary> Queue of trace messages. </summary>
''' <remarks>
''' (c) 2018 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 3/23/2018 </para>
''' </remarks>
Public Class TraceMessagesQueue

#Region " CONSTRUCTION and CLEANUP "

    ''' <summary> Constructor for this class. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Sub New()
        MyBase.New()
        Me._ContentsQueue = New ConcurrentQueue(Of TraceMessage)
    End Sub

#End Region

#Region " DEQUEUE "

    ''' <summary> Gets any. </summary>
    ''' <value> any. </value>
    Public ReadOnly Property Any As Boolean
        Get
            Return Me.ContentsQueue.Any
        End Get
    End Property

    ''' <summary> Gets the number of. </summary>
    ''' <value> The count. </value>
    Public ReadOnly Property Count As Integer
        Get
            Return Me.ContentsQueue.Count
        End Get
    End Property

    ''' <summary> Gets or sets a queue of contents. </summary>
    ''' <value> A queue of contents. </value>
    Protected ReadOnly Property ContentsQueue As ConcurrentQueue(Of TraceMessage)

    ''' <summary> Gets contents queue. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The contents queue. </returns>
    Private Function GetContentsQueue() As ConcurrentQueue(Of TraceMessage)
        Return Me._ContentsQueue
    End Function

    ''' <summary> Gets the content. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The content. </returns>
    Public Function TryDequeue() As TraceMessage
        Dim result As TraceMessage = Nothing
        Me.GetContentsQueue.TryDequeue(result)
        Return result
    End Function

    ''' <summary> Query if the contents queue is empty. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> <c>true</c> the contents queue is empty; otherwise <c>false</c> </returns>
    Public Function IsEmpty() As Boolean
        Return Me.GetContentsQueue.IsEmpty
    End Function

    ''' <summary> Fetches the content. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The content. </returns>
    Public Function DequeueContent() As String
        Dim messageNumber As Integer = 0
        Dim builder As New System.Text.StringBuilder

        Do
            Dim value As TraceMessage = Me.TryDequeue()

            If value IsNot Nothing Then
                messageNumber += 1
                builder.AppendFormat("{0}:>", messageNumber)
                builder.AppendLine(value.ToString)
            End If
        Loop Until Me.GetContentsQueue.IsEmpty
        Return builder.ToString.TrimEnd(Environment.NewLine.ToCharArray)
    End Function

#End Region

#Region " ENQUEUE "

    ''' <summary> Adds a message. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> A String. </returns>
    Protected Function Enqueue(ByVal value As String) As String
        If String.IsNullOrWhiteSpace(value) Then value = String.Empty
        Me.Enqueue(New TraceMessage(TraceEventType.Information, My.MyLibrary.TraceEventId, value))
        Return value
    End Function

    ''' <summary> Adds a message. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The value. </param>
    Public Sub Enqueue(ByVal value As TraceMessage)
        If value Is Nothing OrElse String.IsNullOrWhiteSpace(value.Details) Then
            Debug.Assert(Not Debugger.IsAttached, "Empty trace message", New StackTrace(True).UserCallStack(0, 0))
        Else
            Dim lastCount As Long = CType(Me.GetContentsQueue?.LongCount, Long?).GetValueOrDefault(0)
            Me.GetContentsQueue()?.Enqueue(value)
            If CType(Me.GetContentsQueue?.LongCount, Long?).GetValueOrDefault(0) > lastCount Then
                Me._MessageEnqueuedEventHandlers?.Send(Me, System.EventArgs.Empty)
            End If
        End If
    End Sub

#Region " MESSAGE ENQUEUED "

    ''' <summary> Removes the MessageEnqueued event handlers. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Protected Sub RemoveMessageEnqueuedEventHandlers()
        Me._MessageEnqueuedEventHandlers?.RemoveAll()
    End Sub

    ''' <summary> The MessageEnqueued event handlers. </summary>
    Private ReadOnly _MessageEnqueuedEventHandlers As New EventHandlerContextCollection(Of System.EventArgs)

    ''' <summary> Event queue for all listeners interested in MessageEnqueued events. </summary>
    ''' <remarks> A custom Event is used here to allow us to synchronize with the event listeners. 
    ''' Using a custom Raise method lets you iterate through the delegate list. 
    ''' </remarks>
    Public Custom Event MessageEnqueued As EventHandler(Of System.EventArgs)
        AddHandler(value As EventHandler(Of System.EventArgs))
            Me._MessageEnqueuedEventHandlers.Add(New EventHandlerContext(Of System.EventArgs)(value))
        End AddHandler
        RemoveHandler(value As EventHandler(Of System.EventArgs))
            Me._MessageEnqueuedEventHandlers.RemoveValue(value)
        End RemoveHandler
        RaiseEvent(sender As Object, e As System.EventArgs)
            Me._MessageEnqueuedEventHandlers.Send(sender, e)
        End RaiseEvent
    End Event

    ''' <summary>
    ''' Safely and synchronously <see cref="EventHandlerContext.Send">sends</see> or invokes the
    ''' <see cref="MessageEnqueued">MessageEnqueued Event</see>.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="e"> The <see cref="System.EventArgs" /> instance containing the event data. </param>
    Protected Sub SyncNotifyMessageEnqueued(ByVal e As System.EventArgs)
        Me._MessageEnqueuedEventHandlers.Send(Me, e)
    End Sub

#End Region

#End Region

End Class
