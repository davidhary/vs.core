﻿''' <summary> Interface for talker. </summary>
''' <remarks>
''' (c) 2016 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para>
''' </remarks>
Public Interface ITalker
    Inherits IPublisher

    ''' <summary> Gets or sets the trace message talker. </summary>
    ''' <value> The trace message talker. </value>
    Property Talker As ITraceMessageTalker

    ''' <summary> Assigns a talker. </summary>
    ''' <param name="talker"> The talker. </param>
    Sub AssignTalker(ByVal talker As ITraceMessageTalker)

    ''' <summary> Identifies talkers. </summary>
    Sub IdentifyTalkers()

End Interface

