''' <summary> Encapsulated SQL Server Message Box. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/20/2019 </para>
''' </remarks>
Public NotInheritable Class MyMessageBox

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Private Sub New()
        MyBase.New
    End Sub
#End Region

#Region " MY MESSAGE BOX "

    ''' <summary> Converts an icon to a message box icon. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="icon"> The icon. </param>
    ''' <returns> Icon as an isr.Core.MessageBoxIcon. </returns>
    Private Shared Function ToMessageBoxIcon(ByVal icon As MyMessageBoxIcon) As isr.Core.MyMessageBoxIcon
        Return CType(icon, isr.Core.MyMessageBoxIcon)
    End Function

    ''' <summary> Initializes this object from the given from dialog result. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="dialogResult"> The dialog result. </param>
    ''' <returns> A DialogResult. </returns>
    Private Shared Function FromDialogResult(ByVal dialogResult As isr.Core.MyDialogResult) As MyDialogResult
        Return CType(dialogResult, MyDialogResult)
    End Function

    ''' <summary> Converts a dialogResult to a dialog result. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="dialogResult"> The dialog result. </param>
    ''' <returns> DialogResult as an isr.Core.DialogResult. </returns>
    Private Shared Function ToDialogResult(ByVal dialogResult As MyDialogResult) As isr.Core.MyDialogResult
        Return CType(dialogResult, isr.Core.MyDialogResult)
    End Function

    ''' <summary> Enumerates to dialog results in this collection. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="dialogResults"> The dialog results. </param>
    ''' <returns> DialogResults as an IEnumerable(Of isr.Core.DialogResult) </returns>
    Private Shared Function ToDialogResults(ByVal dialogResults() As MyDialogResult) As IEnumerable(Of isr.Core.MyDialogResult)
        Dim l As New List(Of isr.Core.MyDialogResult)
        For Each dr As MyDialogResult In dialogResults
            l.Add(ToDialogResult(dr))
        Next
        Return l
    End Function

    ''' <summary>
    ''' Synchronously Invokes the exception display on the apartment thread to permit using the
    ''' clipboard.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="exception"> The exception. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal exception As Exception) As MyDialogResult
        Return CType(isr.Core.MyMessageBox.ShowDialog(exception), MyDialogResult)
    End Function

    ''' <summary>
    ''' Synchronously Invokes the exception display on the apartment thread to permit using the
    ''' clipboard.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal text As String, ByVal caption As String) As MyDialogResult
        Return CType(isr.Core.MyMessageBox.ShowDialog(text, caption), MyDialogResult)
    End Function

    ''' <summary>
    ''' Synchronously Invokes the exception display on the apartment thread to permit using the
    ''' clipboard.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="exception">     The exception. </param>
    ''' <param name="icon">          The icon. </param>
    ''' <param name="dialogResults"> The dialog results. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal exception As Exception, ByVal icon As MyMessageBoxIcon, ByVal dialogResults() As MyDialogResult) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialog(exception, ToMessageBoxIcon(icon), ToDialogResults(dialogResults).ToArray))
    End Function

    ''' <summary>
    ''' Synchronously Invokes the exception display on the apartment thread to permit using the
    ''' clipboard.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">          The exception. </param>
    ''' <param name="caption">       The caption. </param>
    ''' <param name="icon">          The icon. </param>
    ''' <param name="dialogResults"> The dialog results. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialog(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon, ByVal dialogResults() As MyDialogResult) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialog(text, caption, ToMessageBoxIcon(icon), ToDialogResults(dialogResults).ToArray))
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="exception"> The exception. </param>
    ''' <param name="icon">      The icon. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialogAbortIgnore(ByVal exception As Exception, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogAbortIgnore(exception, ToMessageBoxIcon(icon)))
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="exception"> The exception. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialogAbortIgnore(ByVal exception As Exception) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialog(exception, ToMessageBoxIcon(MyMessageBoxIcon.Error),
                                                                 New isr.Core.MyDialogResult() {isr.Core.MyDialogResult.Abort, isr.Core.MyDialogResult.Ignore}))
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function ShowDialogAbortIgnore(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialog(text, caption, ToMessageBoxIcon(icon),
                                                                 New isr.Core.MyDialogResult() {isr.Core.MyDialogResult.Abort, isr.Core.MyDialogResult.Ignore}))
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="exception"> The exception. </param>
    ''' <param name="icon">      The icon. </param>
    ''' <returns>
    ''' Either <see cref="MyDialogResult.Ignore">ignore</see> or
    ''' <see cref="MyDialogResult.OK">Okay</see>.
    ''' </returns>
    Public Shared Function ShowDialogIgnoreExit(ByVal exception As Exception, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogIgnoreExit(exception, ToMessageBoxIcon(icon)))
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns>
    ''' Either <see cref="MyDialogResult.Ignore">ignore</see> or
    ''' <see cref="MyDialogResult.OK">Okay</see>.
    ''' </returns>
    Public Shared Function ShowDialogIgnoreExit(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogIgnoreExit(text, caption, ToMessageBoxIcon(icon)))
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="exception"> The exception. </param>
    ''' <param name="icon">      The icon. </param>
    ''' <returns> <see cref="MyDialogResult.OK">Okay</see>. </returns>
    Public Shared Function ShowDialogExit(ByVal exception As Exception, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogExit(exception, ToMessageBoxIcon(icon)))
    End Function

    ''' <summary> Displays the message box. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> <see cref="MyDialogResult.OK">Okay</see>. </returns>
    Public Shared Function ShowDialogExit(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogExit(text, caption, ToMessageBoxIcon(icon)))
    End Function

    ''' <summary> Shows the dialog okay. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogOkay(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogOkay(text, caption, ToMessageBoxIcon(icon)))
    End Function

    ''' <summary> Shows the dialog okay. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogOkay(ByVal text As String, ByVal caption As String) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogOkay(text, caption, ToMessageBoxIcon(MyMessageBoxIcon.Information)))
    End Function

    ''' <summary> Shows the okay/cancel dialog. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogOkayCancel(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogOkayCancel(text, caption, ToMessageBoxIcon(icon)))
    End Function

    ''' <summary> Shows the okay/cancel dialog. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogOkayCancel(ByVal text As String, ByVal caption As String) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogOkayCancel(text, caption))
    End Function

    ''' <summary> Shows the cancel/okay dialog. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogCancelOkay(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogCancelOkay(text, caption, ToMessageBoxIcon(icon)))
    End Function

    ''' <summary> Shows the cancel/okay dialog. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogCancelOkay(ByVal text As String, ByVal caption As String) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogCancelOkay(text, caption))
    End Function

    ''' <summary> Shows the Yes/No dialog. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogYesNo(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogYesNo(text, caption, ToMessageBoxIcon(icon)))
    End Function

    ''' <summary> Shows the Yes/No dialog. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogYesNo(ByVal text As String, ByVal caption As String) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogYesNo(text, caption))
    End Function

    ''' <summary> Shows the No/Yes dialog. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="icon">    The icon. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogNoYes(ByVal text As String, ByVal caption As String, ByVal icon As MyMessageBoxIcon) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogNoYes(text, caption, ToMessageBoxIcon(icon)))
    End Function

    ''' <summary> Shows the No/Yes dialog. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="text">    The text. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A DialogResult. </returns>
    Public Shared Function ShowDialogNoYes(ByVal text As String, ByVal caption As String) As MyDialogResult
        Return FromDialogResult(isr.Core.MyMessageBox.ShowDialogNoYes(text, caption))
    End Function

#End Region

End Class

''' <summary> Values that represent dialog results. </summary>
''' <remarks> David, 2020-09-17. </remarks>
Public Enum MyDialogResult
    ''' <summary> An enum constant representing the none option. </summary>
    None = isr.Core.MyDialogResult.None
    ''' <summary> An enum constant representing the ok option. </summary>
    Ok = isr.Core.MyDialogResult.Ok
    ''' <summary> An enum constant representing the cancel option. </summary>
    Cancel = isr.Core.MyDialogResult.Cancel
    ''' <summary> An enum constant representing the abort option. </summary>
    Abort = isr.Core.MyDialogResult.Abort
    ''' <summary> An enum constant representing the retry option. </summary>
    Retry = isr.Core.MyDialogResult.Retry
    ''' <summary> An enum constant representing the ignore option. </summary>
    Ignore = isr.Core.MyDialogResult.Ignore
    ''' <summary> An enum constant representing the yes option. </summary>
    Yes = isr.Core.MyDialogResult.Yes
    ''' <summary> An enum constant representing the no option. </summary>
    No = isr.Core.MyDialogResult.No
End Enum

''' <summary> Values that represent my message box icons. </summary>
''' <remarks> David, 2020-09-17. </remarks>
<CodeAnalysis.SuppressMessage("Design", "CA1027:Mark enums with FlagsAttribute", Justification:="<Pending>")>
Public Enum MyMessageBoxIcon
    ''' <summary> An enum constant representing the none option. </summary>
    None = isr.Core.MyMessageBoxIcon.None
    ''' <summary> An enum constant representing the asterisk option. </summary>
    Asterisk = isr.Core.MyMessageBoxIcon.Asterisk
    ''' <summary> An enum constant representing the error] option. </summary>
    [Error] = isr.Core.MyMessageBoxIcon.Error
    ''' <summary> An enum constant representing the exclamation option. </summary>
    Exclamation = isr.Core.MyMessageBoxIcon.Exclamation
    ''' <summary> An enum constant representing the hand option. </summary>
    Hand = isr.Core.MyMessageBoxIcon.Hand
    ''' <summary> An enum constant representing the information option. </summary>
    Information = isr.Core.MyMessageBoxIcon.Information
    ''' <summary> An enum constant representing the question option. </summary>
    Question = isr.Core.MyMessageBoxIcon.Question
    ''' <summary> An enum constant representing the stop] option. </summary>
    [Stop] = isr.Core.MyMessageBoxIcon.Stop
    ''' <summary> An enum constant representing the warning option. </summary>
    Warning = isr.Core.MyMessageBoxIcon.Warning
End Enum
