﻿''' <summary> Encapsulated the Text Input Box. </summary>
''' <remarks>
''' (c) 2019 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 1/20/2019 </para>
''' </remarks>
Public NotInheritable Class MyTextInputBox

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Private Sub New()
        MyBase.New
    End Sub
#End Region

#Region " MY TEXT INPUT "

    ''' <summary> Enter text dialog. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function EnterDialog(ByVal caption As String) As String
        Return isr.Core.MyTextInputBox.EnterDialog(caption).DialogValue
    End Function

    ''' <summary> Enter test dialog. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="caption">                    The caption. </param>
    ''' <param name="useSystemPasswordCharacter"> True to use system password character. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function EnterDialog(ByVal caption As String, ByVal useSystemPasswordCharacter As Boolean) As String
        Return isr.Core.MyTextInputBox.EnterDialog(caption, useSystemPasswordCharacter).DialogValue
    End Function

    ''' <summary> Enter test dialog. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="caption">               The caption. </param>
    ''' <param name="prompt">                The prompt. </param>
    ''' <param name="defaultValue">          The default value. </param>
    ''' <param name="useSystemPasswordChar"> True to use system password character. </param>
    ''' <returns> <see cref="MyDialogResult">Dialog result</see>. </returns>
    Public Shared Function EnterDialog(ByVal caption As String, ByVal prompt As String, ByVal defaultValue As String, ByVal useSystemPasswordChar As Boolean) As String
        Return isr.Core.MyTextInputBox.EnterDialog(caption, prompt, defaultValue, useSystemPasswordChar).DialogValue
    End Function

#End Region

End Class

