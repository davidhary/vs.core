﻿''' <summary> The windows forms. </summary>
''' <remarks> David, 2020-09-17. </remarks>
Public NotInheritable Class WindowsForms

#Region " CONSTRUCTION "

    ''' <summary> Initializes a new instance of the <see cref="T:System.Object" /> class. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Private Sub New()
        MyBase.New
    End Sub
#End Region

#Region " ABOUT FORM -- CLASSIC "

    ''' <summary> Displays an about form. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Shared Sub DisplayAbout()
        isr.Core.WindowsForms.DisplayAbout()
    End Sub

#End Region

#Region " CONFIGURATION EDITOR "

    ''' <summary> Opens the settings editor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="caption">  The caption. </param>
    ''' <param name="settings"> Options for controlling the operation. </param>
    Public Shared Sub EditConfiguration(ByVal caption As String, ByVal settings As Configuration.ApplicationSettingsBase)
        isr.Core.ConfigurationEditPad.Open(caption, settings)
    End Sub

#End Region

#Region " MESSAGE FORM "

    ''' <summary> Prompt while waiting for action to complete. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="timeout"> The timeout. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="details"> The details. </param>
    ''' <param name="action">  The action. </param>
    Public Shared Sub ShowMessageForm(ByVal timeout As TimeSpan, ByVal caption As String, ByVal details As String, ByVal action As Action)
        If action Is Nothing Then Throw New ArgumentNullException(NameOf(action))
        isr.Core.WindowsForms.ShowMessageForm(timeout, caption, details, action)
    End Sub

    ''' <summary> Prompt while waiting for a task to complete. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <exception cref="ArgumentNullException"> Thrown when one or more required arguments are null. </exception>
    ''' <param name="timeout"> The timeout. </param>
    ''' <param name="caption"> The caption. </param>
    ''' <param name="details"> The details. </param>
    ''' <param name="task">    The task. </param>
    Public Shared Sub ShowMessageForm(ByVal timeout As TimeSpan, ByVal caption As String, ByVal details As String, ByVal task As System.Threading.Tasks.Task)
        If task Is Nothing Then Throw New ArgumentNullException(NameOf(task))
        isr.Core.WindowsForms.ShowMessageForm(timeout, caption, details, task)
    End Sub

#End Region

#Region " INPUT "

    ''' <summary> Enter input. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="caption"> The caption. </param>
    ''' <returns> A String. </returns>
    Public Shared Function EnterInput(ByVal caption As String) As String
        Return isr.Core.MyTextInputBox.EnterDialog(caption).DialogValue
    End Function

#End Region

#Region " SPLASH "

    ''' <summary> Displays a splash screen. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Shared Sub DisplaySplashScreen()
        isr.Core.WindowsForms.DisplaySplashScreen()
    End Sub

#End Region

#Region " SEND KEYS "

    ''' <summary> Send key strokes to the active application. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="keys"> The keys. </param>
    Public Shared Sub SendKeys(ByVal keys As String)
        isr.Core.WindowsForms.SendKeys(keys)
    End Sub

    ''' <summary>
    ''' Send the given keys to the active application and then wait for the message to be processed.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="keys"> The keys. </param>
    Public Shared Sub SendWait(ByVal keys As String)
        isr.Core.WindowsForms.SendWait(keys)
    End Sub

#End Region

End Class