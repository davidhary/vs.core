#Disable Warning IDE1006 ' Naming Styles
Namespace Global.isr
#Enable Warning IDE1006 ' Naming Styles

    ''' <summary> A trace event constants. </summary>
    ''' <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Friend Module TraceEventConstants

        ''' <summary> The large solution namespace size. Multiply this by the base trace event identity factor, 
        '''           e.g., Hex 7 for the VI library, to get the initial library event id, e.g., 700 hex for the VI library.
        '''           Each such solution can have up to 256 projects. 
        '''           These could be either libraries, test libraries, or test applications. </summary>
        Public Const SolutionNamespaceSize As Integer = &H100

        ''' <summary> The Large solution namespace identity. Multiply this by the large solution namespace value to get
        '''           the first identity of the solution. This allows having 256 project share the namespace of this solution. 
        '''           The range of the large solution identities is between Hex 1000 and FF00. </summary>
        Public Const SolutionNamespaceIdentity As Integer = &H10

        ''' <summary> The small solution namespace size. Multiply this by the <see cref="SmallSolutionNamespaceIdentity"/> to get
        '''           the first identity of the small solution. This allows having 256 solutions each with 16 projects sharing 
        '''           the solution namespace. </summary>
        Public Const SmallSolutionNamespaceSize As Integer = &H10

        ''' <summary> The small solution namespace identity. Multiply this by the <see cref="SmallSolutionNamespaceSize"/> to get 
        '''           the first identity of the small solution. This allows having 256 solutions each with 16 projects sharing 
        '''           the solution namespace. 
        '''           The range of the large solution identities is between 
        '''           <see cref="SmallSolutionNamespaceIdentity"/> * <see cref="SmallSolutionNamespaceSize"/> / <see cref="SolutionNamespaceSize"/> 
        '''           and <see cref="SolutionNamespaceIdentity"/> or hex H100 and hex FF0 </summary>
        Public Const SmallSolutionNamespaceIdentity As Integer = &H10

        ''' <summary> Use this base identity for the custom solution applications event identities.</summary>
        Public Const ApplicationNamespaceIdentity As Integer = &HFFE * TraceEventConstants.SmallSolutionNamespaceSize

    End Module

End Namespace

