Namespace My

    ''' <summary> Provides assembly information for the class library. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Partial Public NotInheritable Class MyLibrary

        ''' <summary>
        ''' Constructor that prevents a default instance of this class from being created.
        ''' </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        Private Sub New()
            MyBase.New()
        End Sub

        ''' <summary> Gets the identifier of the trace source. </summary>
        Public Const TraceEventId As Integer = isr.Core.ProjectTraceEventId.Services
        ''' <summary> The assembly title. </summary>
        Public Const AssemblyTitle As String = "Core Services Library"
        ''' <summary> Information describing the assembly. </summary>
        Public Const AssemblyDescription As String = "Core Services Library"
        ''' <summary> The assembly product. </summary>
        Public Const AssemblyProduct As String = "Core.Services"

    End Class

End Namespace
