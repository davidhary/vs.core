Namespace ExceptionExtensions

    ''' <summary> Adds exception data for building the exception full blown report. </summary>
    ''' <remarks> (c) 2017 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module ProjectMethods

        ''' <summary>
        ''' Adds the <paramref name="exception"/> data to <paramref name="value"/> exception.
        ''' </summary>
        ''' <remarks>
        ''' For more info on the external exceptions see:
        ''' http://msdn.microsoft.com/en-us/library/system.runtime.interopservices.sehexception.aspx.
        ''' </remarks>
        ''' <param name="value">     The value. </param>
        ''' <param name="exception"> The exception. </param>
        ''' <returns>
        ''' <c>true</c> if it <see cref="Exception"/> is not nothing; otherwise <c>false</c>
        ''' </returns>
        Public Function AddExceptionData(ByVal value As System.Exception, ByVal exception As System.Runtime.InteropServices.ExternalException) As Boolean
            If value IsNot Nothing AndAlso exception IsNot Nothing Then
                value.Data.Add($"{value.Data.Count}-External.Error.Code", $"{exception.ErrorCode}")
            End If
            Return exception IsNot Nothing
        End Function

        ''' <summary>
        ''' Adds the <paramref name="exception"/> data to <paramref name="value"/> exception.
        ''' </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="value">     The value. </param>
        ''' <param name="exception"> The exception. </param>
        ''' <returns>
        ''' <c>true</c> if it <see cref="Exception"/> is not nothing; otherwise <c>false</c>
        ''' </returns>
        Public Function AddExceptionData(ByVal value As System.Exception, ByVal exception As ArgumentOutOfRangeException) As Boolean
            If value IsNot Nothing AndAlso exception IsNot Nothing Then
                value.Data.Add($"{value.Data.Count}-Name+Value", $"{exception.ParamName}={exception.ActualValue}")
            End If
            Return exception IsNot Nothing
        End Function

        ''' <summary>
        ''' Adds the <paramref name="exception"/> data to <paramref name="value"/> exception.
        ''' </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="value">     The value. </param>
        ''' <param name="exception"> The exception. </param>
        ''' <returns>
        ''' <c>true</c> if it <see cref="Exception"/> is not nothing; otherwise <c>false</c>
        ''' </returns>
        Public Function AddExceptionData(ByVal value As System.Exception, ByVal exception As ArgumentException) As Boolean
            If value IsNot Nothing AndAlso exception IsNot Nothing Then
                value.Data.Add($"{value.Data.Count}-Name", exception.ParamName)
            End If
            Return exception IsNot Nothing
        End Function

        ''' <summary> Adds exception data from the specified exception. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="exception"> The exception. </param>
        ''' <returns> <c>true</c> if exception was added; otherwise <c>false</c> </returns>
        Public Function AddExceptionData(ByVal exception As System.Exception) As Boolean
            Return ProjectMethods.AddExceptionData(exception, TryCast(exception, ArgumentOutOfRangeException)) OrElse
                   ProjectMethods.AddExceptionData(exception, TryCast(exception, ArgumentException)) OrElse
                   ProjectMethods.AddExceptionData(exception, TryCast(exception, Runtime.InteropServices.ExternalException))
        End Function

        ''' <summary> Converts a value to a full blown string. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <returns> Value as a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Friend Function ToFullBlownString(ByVal value As System.Exception) As String
            Return ProjectMethods.ToFullBlownString(value, Integer.MaxValue)
        End Function

        ''' <summary> Converts this object to a full blown string. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="value"> The value. </param>
        ''' <param name="level"> The level. </param>
        ''' <returns> The given data converted to a String. </returns>
        <System.Runtime.CompilerServices.Extension>
        Friend Function ToFullBlownString(ByVal value As System.Exception, ByVal level As Integer) As String
            Return isr.Core.ExceptionExtensions.Methods.ToFullBlownString(value, level, AddressOf AddExceptionData)
        End Function

    End Module

End Namespace

