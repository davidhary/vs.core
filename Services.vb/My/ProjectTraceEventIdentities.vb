Imports System.ComponentModel

Namespace My

    ''' <summary> Values that represent project trace event identifiers. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Public Enum ProjectTraceEventId
        ''' <summary> An enum constant representing the none option. </summary>
        <Description("Not specified")> None

        <Description("Services")> Services = &H10 * TraceEventConstants.SmallSolutionNamespaceSize

        <Description("Present")> Present = ProjectTraceEventId.Services + &H1

        <Description("Tableaux")> Tableaux = ProjectTraceEventId.Services + &H2

        <Description("Forma")> Forma = ProjectTraceEventId.Services + &H3

        <Description("Controls")> [Controls] = ProjectTraceEventId.Services + &H4

        <Description("Models")> Models = ProjectTraceEventId.Services + &H5

        <Description("Constructs")> Constructs = ProjectTraceEventId.Services + &H6

        <Description("Diagnosis Tester")> DiagnosisTester = ProjectTraceEventId.Services + &H10

        <Description("My Blue Splash Screen")> MyBlueSplashScreen = ProjectTraceEventId.Services + &H11

        <Description("Exception Message Test")> ExceptionMessageTest = ProjectTraceEventId.Services + &H12

        <Description("My Exception Message Box Test")> MyExceptionMessageBoxTest = ProjectTraceEventId.Services + &H13

        <Description("Relic")> Relic = ProjectTraceEventId.Services + &HFF

        ' SMALL SOLUTION IDENTITIES
        <Description("Automata")> Automata = &H20 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Dapper")> Dapper = &H21 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Data")> Data = &H22 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Digital I/O LAN")> DigitalInputOutputLan = &H23 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("High Potential")> HighPotential = &H24 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Initial State Client")> InitialStateClient = &H25 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("IO")> IO = &H26 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Modbus")> Modbus = &H27 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Net")> Net = &H28 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Rich Text Box")> RichTextBox = &H29 * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Open Layers")> OpenLayers = &H2A * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Optima")> Optima = &H2B * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Serial")> Serial = &H2C * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Signals")> Signals = &H2D * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Universal Library")> UniversalLibrary = &H2E * TraceEventConstants.SmallSolutionNamespaceSize
        <Description("Visuals")> Visuals = &H2F * TraceEventConstants.SmallSolutionNamespaceSize
        ' LARGE SOLUTION IDENTITIES
        <Description("Virtual Instruments")> VirtualInstruments = &H10 * TraceEventConstants.SolutionNamespaceSize

        <Description("Application Namespace Identity")> ApplicationNamespace = TraceEventConstants.ApplicationNamespaceIdentity

    End Enum

End Namespace
