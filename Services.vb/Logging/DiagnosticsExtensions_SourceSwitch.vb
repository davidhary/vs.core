﻿Imports System.Runtime.CompilerServices
Namespace DiagnosticsExtensions

    Partial Public Module Methods

#Region " SOURCE SWITCH TRACE LEVEL "

        ''' <summary> Applies the trace level. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="switch"> The <see cref="SourceSwitch">Source switch</see>. </param>
        ''' <param name="value">  The <see cref="TraceEventType">trace level</see> value. </param>
        <Extension()>
        Public Sub ApplyTraceLevel(ByVal switch As SourceSwitch, ByVal value As TraceEventType)
            If switch IsNot Nothing Then
                switch.Level = value.ToSourceLevel
            End If
        End Sub

        ''' <summary> Returns the trace level. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="switch"> The <see cref="SourceSwitch">Source switch</see>. </param>
        ''' <returns>
        ''' The trace level; <see cref="TraceEventType.Information">information</see> if nothing.
        ''' </returns>
        <Extension()>
        Public Function TraceLevel(ByVal switch As SourceSwitch) As TraceEventType
            Return If(switch Is Nothing, TraceEventType.Verbose, switch.Level.ToTraceEventType)
        End Function

#End Region

    End Module

End Namespace
