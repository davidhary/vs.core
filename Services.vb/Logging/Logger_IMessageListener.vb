Imports isr.Core.Services.DiagnosticsExtensions

Partial Public Class MyLog
    Implements IMessageListener

#Region " I MESSAGE LISTENER "

    ''' <summary> Registers this event. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="level"> The level. </param>
    Public Overridable Sub Register(level As TraceEventType) Implements ITraceMessageListener.Register
    End Sub

    ''' <summary> Writes the message at the information level. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="message"> The message to add. </param>
    Public Sub Write(message As String) Implements ITraceMessageListener.Write
        Me.WriteLine(message)
    End Sub

    ''' <summary> Writes a line. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="message"> The message to add. </param>
    Public Sub WriteLine(message As String) Implements ITraceMessageListener.WriteLine
        Me.TraceEvent(New TraceMessage(TraceEventType.Information, My.MyLibrary.TraceEventId, message))
    End Sub
    ''' <summary> The trace level. </summary>
    Private _TraceLevel As TraceEventType

    ''' <summary> Gets or sets the trace level. </summary>
    ''' <value> The trace level. </value>
    Public Property TraceLevel As TraceEventType Implements ITraceMessageListener.TraceLevel
        Get
            Return Me._TraceLevel
        End Get
        Set(value As TraceEventType)
            Me.ApplyTraceLevelThis(value)
        End Set
    End Property

    ''' <summary> Applies the trace level described by value. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
    Private Sub ApplyTraceLevelThis(ByVal value As TraceEventType)
        ' a private internal trace level is now used instead of the trace source to facilitate overridingthe trace source level.
        Me._TraceLevel = value
        Me.TraceSource?.ApplyTraceLevel(TraceEventType.Verbose)
        MyBase.TraceSource?.ApplyTraceLevel(TraceEventType.Verbose)
    End Sub

    ''' <summary> Applies the trace level. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
    Public Sub ApplyTraceLevel(ByVal value As TraceEventType) Implements IMessageListener.ApplyTraceLevel
        Me.ApplyTraceLevelThis(value)
    End Sub

    ''' <summary> Gets the sentinel indicating a thread safe. </summary>
    ''' <value> True if thread safe. </value>
    Public Overridable ReadOnly Property IsThreadSafe As Boolean Implements IMessageListener.IsThreadSafe
        Get
            Return True
        End Get
    End Property

#End Region

End Class
