
Partial Public Class MyLog
    Implements ITraceMessageListener

#Region " I TRACE MESSAGE LISTENER "

    ''' <summary> Gets the thread enabled. </summary>
    ''' <value> The thread enabled. </value>
    Public Property ThreadEnabled As Boolean = True

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The event message. </param>
    ''' <returns> The trace message details. </returns>
    Public Function TraceEvent(ByVal value As TraceMessage) As String Implements ITraceMessageListener.TraceEvent
        If value Is Nothing Then
            Return String.Empty
        Else
            If Me.ThreadEnabled AndAlso Me.ShouldTrace(value.EventType) Then
                Me.GetContentsQueue()?.Enqueue(value)
                Me.FlushMessagesTask()
            Else
                If Me.ShouldTrace(value.EventType) Then Me.TraceSource.TraceEvent(value)
            End If
            Return value.Details
        End If
    End Function

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="id">        The identifier. </param>
    ''' <param name="format">    The message format. </param>
    ''' <param name="args">      Specified the message arguments. </param>
    ''' <returns> The trace message details. </returns>
    Public Function TraceEvent(eventType As TraceEventType, id As Integer, format As String, ParamArray args() As Object) As String Implements ITraceMessageListener.TraceEvent
        Return Me.TraceEvent(New TraceMessage(eventType, id, format, args))
    End Function

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The event message. </param>
    ''' <returns> The trace message details. </returns>
    Public Function TraceEventOverride(ByVal value As TraceMessage) As String Implements ITraceMessageListener.TraceEventOverride
        If value Is Nothing Then
            Return String.Empty
        Else
            If Me.ThreadEnabled Then
                Me.GetContentsQueue()?.Enqueue(value)
                Me.FlushMessagesTask()
            Else
                Me.TraceSource.TraceEvent(value)
            End If
            Return value.Details
        End If
    End Function

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventType"> Type of the event. </param>
    ''' <param name="id">        The identifier. </param>
    ''' <param name="format">    The message format. </param>
    ''' <param name="args">      Specified the message arguments. </param>
    ''' <returns> The trace message details. </returns>
    Public Function TraceEventOverride(eventType As TraceEventType, id As Integer, format As String, ParamArray args() As Object) As String Implements ITraceMessageListener.TraceEventOverride
        Return Me.TraceEventOverride(New TraceMessage(eventType, id, format, args))
    End Function

    ''' <summary> Checks if the log should trace the event type. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns> <c>True</c> If the log should trace; <c>False</c> otherwise. </returns>
    Public Function ShouldTrace(ByVal value As TraceEventType) As Boolean Implements ITraceMessageListener.ShouldTrace
        Return value <= Me.TraceLevel
    End Function

    ''' <summary> Gets a unique identifier. </summary>
    ''' <value> The identifier of the unique. </value>
    Protected ReadOnly Property UniqueId As Guid = Guid.NewGuid Implements ITraceMessageListener.UniqueId

    ''' <summary> Gets the type of the listener. </summary>
    ''' <value> The type of the listener. </value>
    Public ReadOnly Property ListenerType As ListenerType Implements IMessageListener.ListenerType
        Get
            Return ListenerType.Logger
        End Get
    End Property

    ''' <summary> Tests if this ITraceMessageListener is considered equal to another. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="other"> The i trace message listener to compare to this object. </param>
    ''' <returns>
    ''' <c>true</c> if the objects are considered equal, <c>false</c> if they are not.
    ''' </returns>
    Public Overloads Function Equals(other As IMessageListener) As Boolean Implements IEquatable(Of IMessageListener).Equals
        Return other IsNot Nothing AndAlso Guid.Equals(other.UniqueId, Me.UniqueId)
    End Function

#End Region

End Class
