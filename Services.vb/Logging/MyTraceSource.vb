Imports System.Collections.Specialized

Imports isr.Core.Services.DiagnosticsExtensions
Imports isr.Core.Services.ExceptionExtensions

''' <summary> Extends the <see cref="TraceSource">trace source</see>. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/16/2014 </para>
''' </remarks>
Public Class MyTraceSource
    Inherits TraceSource

#Region " CONSTRUCTION "

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="name"> The name. </param>
    Public Sub New(ByVal name As String)
        MyBase.New(name)
    End Sub
    ''' <summary> The listener attributes. </summary>
    Private _ListenerAttributes As StringDictionary

    ''' <summary> Gets the custom attributes supported by the trace source. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns>
    ''' A string array naming the custom attributes supported by the trace source, or null if there
    ''' are no custom attributes.
    ''' </returns>
    Protected Overrides Function GetSupportedAttributes() As String()
        Me._HasBeenInitializedFromConfigFile = True
        Return MyBase.GetSupportedAttributes()
    End Function
    ''' <summary> True if has been initialized from configuration file, false if not. </summary>
    Private _HasBeenInitializedFromConfigFile As Boolean

    ''' <summary> Checks if has been configured. </summary>
    ''' <value> <c>True</c> if configured. </value>
    Public ReadOnly Property HasBeenConfigured As Boolean
        Get
            If Me._ListenerAttributes Is Nothing Then
                Me._ListenerAttributes = Me.Attributes
            End If
            Return Me._HasBeenInitializedFromConfigFile
        End Get
    End Property

#End Region

#Region " TRACE LEVEL "

    ''' <summary> Applies the trace level. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
    Public Sub ApplyTraceLevel(ByVal value As TraceEventType)
        If Me.Switch IsNot Nothing Then
            Me.Switch.ApplyTraceLevel(value)
        End If
    End Sub

    ''' <summary> Returns the trace level. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns>
    ''' The trace level; <see cref="TraceEventType.Information">information</see> if nothing.
    ''' </returns>
    Public Function TraceLevel() As TraceEventType
        Return If(Me.Switch Is Nothing, TraceEventType.Verbose, Me.Switch.TraceLevel)
    End Function

    ''' <summary> Checks if the log should trace the event type. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
    ''' <returns>
    ''' <c>True</c>&gt; If the log should trace the  c&gt;TrueThe trace level;
    ''' <see cref="TraceEventType.Information">information</see> if log is nothing.
    ''' </returns>
    Public Function ShouldTrace(ByVal eventType As TraceEventType) As Boolean
        Return eventType <= Me.TraceLevel
    End Function

#End Region

#Region " TRACE EVENT "

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">        A numeric (integer range) identifier for the event. Specifies as
    '''                          decimal to allow overloading the default method. Using long did not
    '''                          work under VS2010. </param>
    ''' <param name="format">    The trace message format. </param>
    ''' <param name="args">      The trace message arguments. </param>
    ''' <returns> The trace message details or empty. </returns>
    Public Overloads Function TraceEvent(ByVal eventType As TraceEventType, ByVal id As Decimal,
                                         ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
            Me.TraceEvent(eventType, CInt(id), message)
            Return message
        End If
        Return String.Empty
    End Function

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="format">    The trace message format. </param>
    ''' <param name="args">      The trace message arguments. </param>
    ''' <returns> The trace message details or empty. </returns>
    Public Overloads Function TraceEvent(ByVal eventType As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Dim message As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
            Me.TraceEvent(eventType, 0I, message)
            Return message
        End If
        Return String.Empty
    End Function

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">        A numeric (integer range) identifier for the event. </param>
    ''' <param name="messages">  Messages to trace. </param>
    ''' <returns> The trace message details or empty. </returns>
    Public Overloads Function TraceEvent(ByVal eventType As TraceEventType, ByVal id As Integer, ByVal messages As String()) As String
        If messages IsNot Nothing Then
            Dim message As String = String.Join(",", messages)
            Me.TraceEvent(eventType, id, message)
            Return message
        End If
        Return String.Empty
    End Function

#End Region

#Region " EXCEPTION TRACE EVENT "

    ''' <summary> Trace event. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex">             The exception. </param>
    ''' <param name="eventType">      The <see cref="TraceEventType">
    '''                               event type</see>
    '''                               of the trace data. </param>
    ''' <param name="id">             A numeric (integer range) identifier for the event. </param>
    ''' <param name="additionalInfo"> Information describing the additional. </param>
    Public Overloads Sub TraceEvent(ByVal ex As Exception, ByVal eventType As TraceEventType, ByVal id As Integer, ByVal additionalInfo As String)
        If ex Is Nothing Then Return
        Dim builder As New System.Text.StringBuilder(ex.ToFullBlownString)
        builder.Append($"; {additionalInfo}")
        Me.TraceEvent(eventType, id, builder.ToString)
    End Sub

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex">        The exception. </param>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">        A numeric (integer range) identifier for the event. </param>
    ''' <param name="format">    The additional information format. </param>
    ''' <param name="args">      The additional information arguments. </param>
    Public Overloads Sub TraceEvent(ByVal ex As Exception, ByVal eventType As TraceEventType, ByVal id As Integer,
                                    ByVal format As String, ByVal ParamArray args() As Object)
        If ex IsNot Nothing Then Me.TraceEvent(ex, eventType, id, String.Format(format, args))
    End Sub

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex">        The exception. </param>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="format">    The additional information format. </param>
    ''' <param name="args">      The additional information arguments. </param>
    Public Overloads Sub TraceEvent(ByVal ex As Exception, ByVal eventType As TraceEventType,
                                    ByVal format As String, ByVal ParamArray args() As Object)
        Me.TraceEvent(ex, eventType, 0, format, args)
    End Sub

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex">     The exception. </param>
    ''' <param name="id">     A numeric (integer range) identifier for the event. </param>
    ''' <param name="format"> The additional information format. </param>
    ''' <param name="args">   The additional information arguments. </param>
    Public Overloads Sub TraceEvent(ByVal ex As Exception, ByVal id As Integer,
                                    ByVal format As String, ByVal ParamArray args() As Object)
        Me.TraceEvent(ex, TraceEventType.Error, id, format, args)
    End Sub

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex"> The exception. </param>
    ''' <param name="id"> A numeric (integer range) identifier for the event. </param>
    Public Overloads Sub TraceEvent(ByVal ex As Exception, ByVal id As Integer)
        Me.TraceEvent(ex, TraceEventType.Error, id, "")
    End Sub

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex"> The exception. </param>
    Public Overloads Sub TraceEvent(ByVal ex As Exception)
        Me.TraceEvent(ex, TraceEventType.Error, 0, "")
    End Sub

#End Region

#Region " TRACE EVENT -- OVERRIDE TRACE LEVEL "

    ''' <summary>
    ''' Writes a trace event to the trace listeners. Overrides the current trace source level.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">        A numeric (integer range) identifier for the event. </param>
    ''' <param name="format">    The trace message format. </param>
    ''' <param name="args">      The arguments. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function TraceEventOverride(ByVal eventType As TraceEventType, ByVal id As Integer,
                                       ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Dim details As String = String.Format(Globalization.CultureInfo.CurrentCulture, format, args)
            ' save the current trace level.
            Dim lastSourceLevel As Diagnostics.SourceLevels = Me.Switch.Level
            ' set the requested level.
            Me.Switch.Level = eventType.ToSourceLevel
            ' write the entry.
            Me.TraceEvent(eventType, id, details)
            ' restore the level.
            Me.Switch.Level = lastSourceLevel
            Return details
        End If
        Return String.Empty
    End Function

    ''' <summary>
    ''' Writes a trace event to the trace listeners. Overrides the current trace source level.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventType"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="format">    The trace message format. </param>
    ''' <param name="args">      The arguments. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function TraceEventOverride(ByVal eventType As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
        Return If(format IsNot Nothing, Me.TraceEventOverride(eventType, 0, format, args), String.Empty)
    End Function

#End Region

#Region " TRACE EVENT TRACE MESSAGES "

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="message"> The message. </param>
    Public Overloads Sub TraceEvent(ByVal message As TraceMessage)
        If message IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(message.Details) Then
            Me.TraceEvent(message.EventType, message.Id, message.Details)
        End If
    End Sub

    ''' <summary> Writes a trace event to the trace listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="message">        The message. </param>
    ''' <param name="additionalInfo"> Information describing the additional. </param>
    Public Overloads Sub TraceEvent(ByVal message As TraceMessage, ByVal additionalInfo As String)
        If message IsNot Nothing Then
            If String.IsNullOrWhiteSpace(message.Details) Then
                If Not String.IsNullOrWhiteSpace(additionalInfo) Then
                    Me.TraceEvent(message.EventType, message.Id, additionalInfo)
                End If
            ElseIf String.IsNullOrWhiteSpace(additionalInfo) Then
                Me.TraceEvent(message.EventType, message.Id, "{0}; {1}", message.Details, additionalInfo)
            Else
                Me.TraceEvent(message.EventType, message.Id, message.Details)
            End If
        End If
    End Sub

#End Region

#Region " FILE  "

    ''' <summary> Returns the default file log writer. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns>
    ''' The default <see cref="Logging.FileLogTraceListener">file log writer</see>.
    ''' </returns>
    Public Function DefaultFileLogWriter() As Logging.FileLogTraceListener
        Return CType(Me.Listeners(DefaultFileLogTraceListener.DefaultFileLogWriterName), Logging.FileLogTraceListener)
    End Function

    ''' <summary> Returns the filename of the default file log writer. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The filename of the log file. </returns>
    Public Function DefaultFileLogWriterFilePath() As String
        If Me.DefaultFileLogWriter Is Nothing Then
            Return String.Empty
        ElseIf Me.DefaultFileLogWriter IsNot Nothing Then
            Return Me.DefaultFileLogWriter.FullLogFileName
        Else
            Return String.Empty
        End If
    End Function

    ''' <summary> Checks if the default file log writer file exists. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> <c>True</c> if the log file exists. </returns>
    Public Function DefaultFileLogWriterFileExists() As Boolean
        Return DefaultFileLogTraceListener.FileSize(Me.DefaultFileLogWriterFilePath) > 2
    End Function

#End Region

#Region " REPLACE TRACE LISTENER "

    ''' <summary> Replaces the default file log trace listener with a new one. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="logWriter"> The <see cref="Logging.FileLogTraceListener">log writer</see>. </param>
    ''' <returns>
    ''' The <see cref="Logging.FileLogTraceListener">file log trace listener. </see>
    ''' </returns>
    Public Function ReplaceDefaultTraceListener(ByVal logWriter As Logging.FileLogTraceListener) As Logging.FileLogTraceListener
        Me.Listeners.Remove(DefaultFileLogTraceListener.DefaultFileLogWriterName)
        Me.Listeners.Add(logWriter)
        Return logWriter
    End Function

    ''' <summary>
    ''' Replaces the default file log trace listener with a new one for the current user.
    ''' </summary>
    ''' <remarks> The current user application data folder is used. </remarks>
    ''' <returns>
    ''' The <see cref="Logging.FileLogTraceListener">file log trace listener. </see>
    ''' </returns>
    Public Function ReplaceDefaultTraceListener() As Logging.FileLogTraceListener
        Return Me.ReplaceDefaultTraceListener(False)
    End Function

    ''' <summary> Replaces the default file log trace listener with a new one. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="allUsers"> True to select the folder for
    '''                         <see cref="MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                         all users; otherwise, use using current user; otherwise, use the
    '''                         folder for
    '''                         <see cref="MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> 
    ''' </param>
    ''' <returns>
    ''' The <see cref="logging.FileLogTraceListener">file log trace listener. </see>
    ''' </returns>
    Public Function ReplaceDefaultTraceListener(ByVal allUsers As Boolean) As Logging.FileLogTraceListener
        Dim tempListener As Logging.FileLogTraceListener = Nothing
        Dim listener As Logging.FileLogTraceListener = Nothing
        Try
            tempListener = New DefaultFileLogTraceListener(allUsers)
            listener = tempListener
            Me.ReplaceDefaultTraceListener(listener)
        Finally
            If tempListener IsNot Nothing Then tempListener.Dispose()
        End Try
        Return listener
    End Function

#End Region

End Class
