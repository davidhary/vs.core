Imports System.Runtime.CompilerServices
Namespace DiagnosticsExtensions

    Partial Public Module Methods

#Region " TRACE LEVELS AND TRACE EVENT TYPES "

        ''' <summary> Builds trace level trace event type hash. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <returns> A Dictionary for translating trace levels to trace event types. </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function BuildTraceLevelTraceEventTypeHash() As IDictionary(Of TraceLevel, TraceEventType)
            Dim dix2 As New Dictionary(Of TraceLevel, TraceEventType)
            Dim dix3 As Dictionary(Of TraceLevel, TraceEventType) = dix2
            dix3.Add(Diagnostics.TraceLevel.Error, TraceEventType.Error)
            dix3.Add(Diagnostics.TraceLevel.Info, TraceEventType.Information)
            dix3.Add(Diagnostics.TraceLevel.Off, TraceEventType.Stop)
            dix3.Add(Diagnostics.TraceLevel.Verbose, TraceEventType.Verbose)
            dix3.Add(Diagnostics.TraceLevel.Warning, TraceEventType.Warning)
            Return dix2
        End Function
        ''' <summary> The trace level trace event type hash. </summary>
        Private _TraceLevelTraceEventTypeHash As IDictionary(Of TraceLevel, TraceEventType)

        ''' <summary> Trace level trace event type hash. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <returns> An IDictionary(Of TraceLevel, TraceEventType) </returns>
        Private Function TraceLevelTraceEventTypeHash() As IDictionary(Of TraceLevel, TraceEventType)
            If Methods._TraceLevelTraceEventTypeHash Is Nothing Then
                Methods._TraceLevelTraceEventTypeHash = Methods.BuildTraceLevelTraceEventTypeHash()
            End If
            Return Methods._TraceLevelTraceEventTypeHash
        End Function

        ''' <summary> Derives a <see cref="TraceEventType">trace event type</see>. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="value"> A <see cref="TraceLevel">trace level</see> of a trace switch. Specifies
        '''                      what messages to output for the
        '''                      <see cref="System.Diagnostics.Debug">debug</see>
        '''                      and <see cref="System.Diagnostics.Trace">trace</see> and
        '''                      <see cref="System.Diagnostics.TraceSwitch">trace switch</see> classes. 
        ''' </param>
        ''' <returns> The <see cref="TraceEventType">event type</see>. </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        <Extension()>
        Public Function ToTraceEventType(ByVal value As TraceLevel) As TraceEventType
            Return If(Methods.TraceLevelTraceEventTypeHash.ContainsKey(value), Methods.TraceLevelTraceEventTypeHash(value), TraceEventType.Information)
        End Function

#End Region

    End Module

End Namespace

