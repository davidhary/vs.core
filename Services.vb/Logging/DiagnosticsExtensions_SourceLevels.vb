Imports System.Runtime.CompilerServices
Namespace DiagnosticsExtensions

    ''' <summary> A diagnostics extensions. </summary>
    ''' <remarks> (c) 2015 Integrated Scientific Resources, Inc. All rights reserved.<para>
    ''' Licensed under The MIT License.</para> </remarks>
    Public Module Methods

#Region " SOURCE LEVELS AND TRACE EVENT TYPES "

        ''' <summary> Builds source level trace event type hash. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <returns> A Dictionary for translating source level to trace event type. </returns>
        Public Function BuildSourceLevelsTraceEventTypeHash() As IDictionary(Of SourceLevels, TraceEventType)
            Dim dix2 As New Dictionary(Of SourceLevels, TraceEventType)
            Dim dix3 As Dictionary(Of SourceLevels, TraceEventType) = dix2
            dix3.Add(Diagnostics.SourceLevels.Critical, TraceEventType.Critical)
            dix3.Add(Diagnostics.SourceLevels.Error, TraceEventType.Error)
            dix3.Add(Diagnostics.SourceLevels.Information, TraceEventType.Information)
            dix3.Add(Diagnostics.SourceLevels.Off, TraceEventType.Stop)
            dix3.Add(Diagnostics.SourceLevels.Verbose, TraceEventType.Verbose)
            dix3.Add(Diagnostics.SourceLevels.Warning, TraceEventType.Warning)
            Return dix2
        End Function
        ''' <summary> Source levels trace event type hash. </summary>
        Private _SourceLevelsTraceEventTypeHash As IDictionary(Of SourceLevels, TraceEventType)

        ''' <summary> Source levels trace event type hash. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <returns> An IDictionary(Of SourceLevels, TraceEventType) </returns>
        Private Function SourceLevelsTraceEventTypeHash() As IDictionary(Of SourceLevels, TraceEventType)
            If Methods._SourceLevelsTraceEventTypeHash Is Nothing Then
                Methods._SourceLevelsTraceEventTypeHash = Methods.BuildSourceLevelsTraceEventTypeHash
            End If
            Return Methods._SourceLevelsTraceEventTypeHash
        End Function

        ''' <summary>
        ''' Derives a <see cref="TraceEventType">trace event type</see> given the
        ''' <see cref="SourceLevels">source level</see> of a trace switch.
        ''' </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="value"> The <see cref="SourceLevels">levels</see> of the trace message filtered
        '''                      by the source switch and event type filter. </param>
        ''' <returns> The Trace <see cref="TraceEventType">event type</see>. </returns>
        <Extension()>
        Public Function ToTraceEventType(ByVal value As SourceLevels) As TraceEventType
            Return If(Methods.SourceLevelsTraceEventTypeHash.ContainsKey(value), Methods.SourceLevelsTraceEventTypeHash(value), TraceEventType.Information)
        End Function

#End Region

    End Module

End Namespace
