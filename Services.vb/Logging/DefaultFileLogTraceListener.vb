﻿Imports System.Globalization
Imports System.Security.Permissions

''' <summary> Replaces the standard event log. </summary>
''' <remarks>
''' (c) 2011 Integrated Scientific Resources, Inc. All rights reserved. <para>
''' Licensed under The MIT License. </para><para>  
''' David, 8/8/2011 </para>
''' </remarks>
Public Class DefaultFileLogTraceListener
    Inherits Logging.FileLogTraceListener

    ''' <summary> Gets the default file log trace listener name. </summary>
    Public Const DefaultFileLogWriterName As String = "FileLog"

    ''' <summary> The default folder title. </summary>
    Public Const DefaultFolderTitle As String = "_log"

    ''' <summary> The default time format. </summary>
    Public Const DefaultTimeFormat As String = "HH:mm:ss.fff"

    ''' <summary> The default delimiter. </summary>
    Public Const DefaultDelimiter As String = ","

    ''' <summary> The default trace identifier format. </summary>
    Public Const DefaultTraceIdFormat As String = "{0,5:X}"

    ''' <summary> Gets or sets the trace identifier format. </summary>
    ''' <value> The trace identifier format. </value>
    Public Property TraceIdFormat As String


    ''' <summary> The default process identifier format. </summary>
    Public Const DefaultProcessIdFormat As String = "{0:X}"

    ''' <summary> Gets or sets the process identifier format. </summary>
    ''' <value> The process identifier format. </value>
    Public Property ProcessIdIdFormat As String

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:Logging.FileLogTraceListener">file log trace
    ''' listener</see>.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="allUsers"> True to select the folder for
    '''                         <see cref="MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                         all users; otherwise, use using current user; otherwise, use the
    '''                         folder for
    '''                         <see cref="MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> 
    ''' </param>
    Public Sub New(ByVal allUsers As Boolean)
        Me.New(DefaultFileLogTraceListener.DefaultFileLogWriterName, DefaultFileLogTraceListener.DefaultFolderTitle, allUsers)
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:Logging.FileLogTraceListener">file log trace
    ''' listener</see> using the application folder or the specified user level application data
    ''' folder if the application folder is protected.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="name">        The name of the
    '''                            <see cref="T:System.Diagnostics.TraceListener"></see>. </param>
    ''' <param name="folderTitle"> Name of the folder. </param>
    ''' <param name="allUsers">    True to select the folder for
    '''                            <see cref="MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                            all users; otherwise, use using current user; otherwise, use the
    '''                            folder for
    '''                            <see cref="MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> 
    ''' </param>
    Public Sub New(ByVal name As String, ByVal folderTitle As String, ByVal allUsers As Boolean)
        Me.New(name, DefaultFileLogTraceListener.BuildCustomFolder(folderTitle, allUsers))
    End Sub

    ''' <summary>
    ''' Initializes a new instance of the
    ''' <see cref="T:Logging.FileLogTraceListener">file log trace
    ''' listener</see> using the application folder or the specified user level application data
    ''' folder if the application folder is protected.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="name">   The name of the
    '''                       <see cref="T:System.Diagnostics.TraceListener"></see>. </param>
    ''' <param name="folder"> Pathname of the folder. </param>
    Public Sub New(ByVal name As String, ByVal folder As String)
        MyBase.New(name)

        Me.TraceIdFormat = DefaultFileLogTraceListener.DefaultTraceIdFormat
        Me.TimeFormat = DefaultFileLogTraceListener.DefaultTimeFormat
        Me.UsingUniversalTime = True
        Me.TraceIdFormat = DefaultFileLogTraceListener.DefaultTraceIdFormat
        Me.ProcessIdIdFormat = DefaultFileLogTraceListener.DefaultProcessIdFormat

        ' set the default properties of the log file name 
        Me.LogFileCreationSchedule = Logging.LogFileCreationScheduleOption.Daily
        Me.Location = Logging.LogFileLocation.Custom
        Me.Delimiter = DefaultFileLogTraceListener.DefaultDelimiter
        Me.CustomLocation = folder
        Me.IncludeHostName = False
        Me.AutoFlush = True
        Me.TraceOutputOptions = TraceOptions.DateTime Or TraceOptions.LogicalOperationStack

        ' note that the trace event cache time format is not set correctly to -8 hours offset.
        ' Rather, it gives us a -4 hours offset.
    End Sub

    ''' <summary> Creates a listener. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="allUsers"> True to select the folder for
    '''                         <see cref="MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                         all users; otherwise, use using current user; otherwise, use the
    '''                         folder for
    '''                         <see cref="MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> 
    ''' </param>
    ''' <returns> The new listener. </returns>
    Public Shared Function CreateListener(ByVal allUsers As Boolean) As Logging.FileLogTraceListener
        Dim templListener As Logging.FileLogTraceListener = Nothing
        Try
            templListener = New DefaultFileLogTraceListener(allUsers)
            Dim listener As Logging.FileLogTraceListener = templListener
            Return listener
        Catch
            Throw
        Finally
            If templListener IsNot Nothing Then templListener.Dispose()
        End Try
    End Function

#Region " WRITE "

    ''' <summary> The universal time suffix. </summary>
    Private Const _UniversalTimeSuffix As String = "Z"

    ''' <summary> true to using universal time. </summary>
    Private _UsingUniversalTime As Boolean

    ''' <summary>
    ''' Gets or sets the condition for using universal time.  When using universal time, the format
    ''' is automatically modified to add a <see cref="_UniversalTimeSuffix"/> suffix if not set.
    ''' Defaults to True.
    ''' </summary>
    ''' <value> <c>True</c> if [using universal time]; otherwise, <c>False</c>. </value>
    Public Property UsingUniversalTime() As Boolean
        Get
            Return Me._UsingUniversalTime
        End Get
        Set(ByVal value As Boolean)
            Me._UsingUniversalTime = value
            If value AndAlso Not Me._TimeFormat.EndsWith(DefaultFileLogTraceListener._UniversalTimeSuffix, StringComparison.OrdinalIgnoreCase) Then
                Me.TimeFormat &= DefaultFileLogTraceListener._UniversalTimeSuffix
            ElseIf Not value AndAlso Me._TimeFormat.EndsWith(DefaultFileLogTraceListener._UniversalTimeSuffix, StringComparison.OrdinalIgnoreCase) Then
                Me.TimeFormat = Me.TimeFormat.Substring(0, Me.TimeFormat.Length - 1)
            End If
        End Set
    End Property

    ''' <summary> Gets the time format. </summary>
    ''' <value> The time format. </value>
    Public Property TimeFormat() As String

    ''' <summary> Builds trace message. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache"></see> object that
    '''                           contains the current process ID, thread ID, and stack trace
    '''                           information. </param>
    ''' <param name="source">     A name of the trace source that invoked this method. </param>
    ''' <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType"></see>
    '''                           enumeration values. </param>
    ''' <param name="id">         A numeric identifier for the event. </param>
    ''' <param name="message">    A message to write. </param>
    ''' <returns> The trace message record. </returns>
    Private Function BuildTraceMessage(ByVal eventCache As TraceEventCache, ByVal source As String, ByVal eventType As TraceEventType,
                                       ByVal id As Integer, ByVal message As String) As String

        Dim builder As New System.Text.StringBuilder
        builder.AppendFormat("{0}{1}", eventType.ToString.Substring(0, 2), Me.Delimiter)
        If id = 0 Then id = eventType
        builder.AppendFormat(Globalization.CultureInfo.CurrentCulture, Me.TraceIdFormat, id)
        builder.Append(Me.Delimiter)

        If Me.IncludeHostName Then
            builder.AppendFormat("{0}.{1}{2}", Me.HostName, source, Me.Delimiter)
        End If

        If (Me.TraceOutputOptions And TraceOptions.DateTime) = TraceOptions.DateTime Then
            Dim logTime As DateTime = eventCache.DateTime
            ' event cache log time is expressed as UTC. 
            ' Adding the local time offset, converts the UCT time to local time
            If Not Me.UsingUniversalTime Then logTime = logTime.Add(DateTimeOffset.Now.Offset)
            builder.AppendFormat("{0}{1}", logTime.ToString(Me._TimeFormat, CultureInfo.CurrentCulture), Me.Delimiter)
        End If

        If (Me.TraceOutputOptions And TraceOptions.ProcessId) = TraceOptions.ProcessId Then
            builder.AppendFormat(Globalization.CultureInfo.CurrentCulture, Me.ProcessIdIdFormat, eventCache.ProcessId)
            builder.Append(Me.Delimiter)
        End If
        If (Me.TraceOutputOptions And TraceOptions.ThreadId) = TraceOptions.ThreadId Then
            builder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{1}", eventCache.ThreadId, Me.Delimiter)
        End If
        If (Me.TraceOutputOptions And TraceOptions.Timestamp) = TraceOptions.Timestamp Then
            builder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{1}", eventCache.Timestamp, Me.Delimiter)
        End If

        builder.Append(message)

        If (Me.TraceOutputOptions And TraceOptions.LogicalOperationStack) = TraceOptions.LogicalOperationStack Then
            builder.AppendFormat("{0}{1}", Me.Delimiter, DefaultFileLogTraceListener.StackToString(eventCache.LogicalOperationStack))
        End If

        If (Me.TraceOutputOptions And TraceOptions.Callstack) = TraceOptions.Callstack Then
            builder.AppendFormat("{0}{1}", Me.Delimiter, eventCache.Callstack)
        End If
        Return builder.ToString

    End Function

    ''' <summary>
    ''' Writes trace information, a message and event information to the output file or stream.
    ''' </summary>
    ''' <remarks>
    ''' The <see cref="Logging.Log">Visual Basic Log</see> use the
    ''' <see cref="Switch">switch</see> to Determine if the event cache should be traced using the
    ''' switch <see cref="TraceEventType">event type</see>. The
    ''' <see cref="DefaultTraceListener.Filter">filter</see> is Overridable and can be set as
    ''' follows if additional filtering is required; otherwise, it is Nothing:
    ''' <code>
    ''' ' apply the trace level to the trace listener.
    ''' Dim listener As Diagnostics.TraceListener = log.TraceSource.Listeners(DefaultFileLogTraceListener.DefaultTraceListenerName)
    ''' If listener IsNot Nothing Then
    ''' listener.Filter = New EventTypeFilter(log.TraceSource.Switch.Level)
    ''' End If
    ''' </code>
    ''' 20170511: change trim to trim end to preserve the full blown structure.
    ''' </remarks>
    ''' <param name="eventCache"> A <see cref="T:System.Diagnostics.TraceEventCache"></see> object that
    '''                           contains the current process ID, thread ID, and stack trace
    '''                           information. </param>
    ''' <param name="source">     A name of the trace source that invoked this method. </param>
    ''' <param name="eventType">  One of the <see cref="T:System.Diagnostics.TraceEventType"></see>
    '''                           enumeration values. </param>
    ''' <param name="id">         A numeric identifier for the event. </param>
    ''' <param name="message">    A message to write. </param>
    <HostProtection(SecurityAction.LinkDemand, Synchronization:=True)>
    Public Overrides Sub TraceEvent(ByVal eventCache As TraceEventCache, ByVal source As String, ByVal eventType As TraceEventType,
                                    ByVal id As Integer, ByVal message As String)
        If eventCache Is Nothing OrElse String.IsNullOrWhiteSpace(message) Then Return
        If Me.Filter Is Nothing OrElse Me.Filter.ShouldTrace(eventCache, source, eventType, id, message, Nothing, Nothing, Nothing) Then
            If message.Contains(Environment.NewLine) Then
                For Each line As String In message.Split(Environment.NewLine.ToCharArray)
                    Me.TraceEvent(eventCache, source, eventType, id, line.TrimEnd)
                Next
                Return
            End If
            Dim record As String = Me.BuildTraceMessage(eventCache, source, eventType, id, message)
            Me.WriteLine(record)
        End If
    End Sub

    ''' <summary> Name of the host. </summary>
    Private _HostName As String

    ''' <summary> Gets the name of the host. </summary>
    ''' <value> The name of the host. </value>
    Private ReadOnly Property HostName() As String
        Get
            If String.IsNullOrEmpty(Me._HostName) Then
                Me._HostName = Environment.MachineName
            End If
            Return Me._HostName
        End Get
    End Property

    ''' <summary> Stacks to string. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="stack"> The stack. </param>
    ''' <returns> System.String. </returns>
    Private Shared Function StackToString(ByVal stack As Stack) As String
        Dim enumerator As IEnumerator = Nothing
        Dim length As Integer = ", ".Length
        Dim builder As New System.Text.StringBuilder
        Try
            enumerator = stack.GetEnumerator
            Do While enumerator.MoveNext
                builder.AppendFormat("{0}, ", enumerator.Current.ToString)
            Loop
        Finally
            If TypeOf enumerator Is IDisposable Then
                TryCast(enumerator, IDisposable).Dispose()
            End If
        End Try
        builder.Replace("""", """""")
        If (builder.Length >= length) Then
            builder.Remove((builder.Length - length), length)
        End If
        Return ("""" & builder.ToString & """")
    End Function

#End Region

#Region " FOLDER "

    ''' <summary> Determines whether the specified folder path is writable. </summary>
    ''' <remarks>
    ''' Uses a temporary random file name to test if the file can be created. The file is deleted
    ''' thereafter.
    ''' </remarks>
    ''' <param name="path"> The path. </param>
    ''' <returns> <c>True</c> if the specified path is writable; otherwise, <c>False</c>. </returns>
    <CodeAnalysis.SuppressMessage("Design", "CA1031:Do not catch general exception types", Justification:="Exception is published")>
    Public Shared Function IsFolderWritable(ByVal path As String) As Boolean
        Dim filePath As String = String.Empty
        Dim affirmative As Boolean = False
        Try
            filePath = System.IO.Path.Combine(path, System.IO.Path.GetRandomFileName())
            Using s As System.IO.FileStream = System.IO.File.Open(filePath, System.IO.FileMode.OpenOrCreate)
            End Using
            affirmative = True
        Catch
        Finally
            ' SS reported an exception from this test possibly indicating that Windows allowed writing the file 
            ' by failed report deletion. Or else, Windows raised another exception type.
            Try
                If System.IO.File.Exists(filePath) Then
                    System.IO.File.Delete(filePath)
                End If
            Catch
            End Try
        End Try
        Return affirmative
    End Function

    ''' <summary> Returns the default folder name for tracing. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="defaultFolderTitle"> The default folder title. </param>
    ''' <param name="allUsers">           True to select the folder for
    '''                                   <see cref="MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                                   all users; otherwise, use using current user; otherwise,
    '''                                   use the folder for
    '''                                   <see cref="MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> 
    ''' </param>
    ''' <returns> The default path for the log file. </returns>
    Public Shared Function BuildCustomFolder(ByVal defaultFolderTitle As String, ByVal allUsers As Boolean) As String
        Dim candidatePath As String = My.Application.Info.DirectoryPath
        If IsFolderWritable(candidatePath) Then
            Return System.IO.Path.Combine(candidatePath, "..\" & defaultFolderTitle)
        ElseIf allUsers Then
            Return System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.AllUsersApplicationData, defaultFolderTitle)
        Else
            Return System.IO.Path.Combine(My.Computer.FileSystem.SpecialDirectories.CurrentUserApplicationData, defaultFolderTitle)
        End If
    End Function


#End Region

#Region " FILE SIZE "

    ''' <summary> Returns the file size, -2 if path not specified or -1 if file not found. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="path"> The path. </param>
    ''' <returns> System.Int64. </returns>
    Public Shared Function FileSize(ByVal path As String) As Long
        If String.IsNullOrWhiteSpace(path) Then Return 0
        Dim info As System.IO.FileInfo = New System.IO.FileInfo(path)
        Return DefaultFileLogTraceListener.FileSize(info)
    End Function

    ''' <summary> Returns the file size, -2 if path not specified or -1 if file not found. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="value"> The value. </param>
    ''' <returns> System.Int64. </returns>
    Public Shared Function FileSize(ByVal value As System.IO.FileInfo) As Long

        If value Is Nothing Then
            Return -2
        ElseIf value.Exists Then
            Return value.Length
        ElseIf String.IsNullOrWhiteSpace(value.Name) Then
            Return -2
        Else
            Return -1
        End If

    End Function

    ''' <summary> Gets the file log trace listener. </summary>
    ''' <value> The file log trace listener. </value>
    Public Shared ReadOnly Property FileLogTraceListener As Logging.FileLogTraceListener
        Get
            Return TryCast(My.Application.Log.TraceSource.Listeners(DefaultFileLogTraceListener.DefaultFileLogWriterName), Logging.FileLogTraceListener)
        End Get
    End Property

    ''' <summary> Returns true if log file exists; Otherwise, <c>False</c>. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> <c>True</c> if okay, <c>False</c> otherwise. </returns>
    Public Shared Function LogFileExists() As Boolean
        Return DefaultFileLogTraceListener.FileSize(DefaultFileLogTraceListener.FileLogTraceListener?.FullLogFileName) > 2
    End Function

#End Region

End Class

