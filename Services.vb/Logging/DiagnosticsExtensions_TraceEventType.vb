Imports System.Runtime.CompilerServices
Namespace DiagnosticsExtensions

    Partial Public Module Methods

#Region " TRACE EVENT TO AN ID "

        ''' <summary> Builds trace event type identifier hash. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <returns> A Dictionary for translating trace events to Id. </returns>
        Public Function BuildTraceEventTypeIdHash() As IDictionary(Of TraceEventType, Integer)
            Dim dix2 As New Dictionary(Of TraceEventType, Integer)
            Dim dix3 As Dictionary(Of TraceEventType, Integer) = dix2
            dix3.Add(TraceEventType.Information, 0)
            dix3.Add(TraceEventType.Warning, 1)
            dix3.Add(TraceEventType.Error, 2)
            dix3.Add(TraceEventType.Critical, 3)
            dix3.Add(TraceEventType.Start, 4)
            dix3.Add(TraceEventType.Stop, 5)
            dix3.Add(TraceEventType.Suspend, 6)
            dix3.Add(TraceEventType.Resume, 7)
            dix3.Add(TraceEventType.Verbose, 8)
            dix3.Add(TraceEventType.Transfer, 9)
            Return dix2
        End Function
        ''' <summary> The trace event type identifier hash. </summary>
        Private _TraceEventTypeIdHash As IDictionary(Of TraceEventType, Integer)

        ''' <summary> Trace event type identifier hash. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <returns> An IDictionary(Of TraceEventType, Integer) </returns>
        Private Function TraceEventTypeIdHash() As IDictionary(Of TraceEventType, Integer)
            If Methods._TraceEventTypeIdHash Is Nothing Then
                Methods._TraceEventTypeIdHash = Methods.BuildTraceEventTypeIdHash()
            End If
            Return Methods._TraceEventTypeIdHash
        End Function

        ''' <summary> Derives an id given the <see cref="TraceEventType">trace event type</see>. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="value"> A <see cref="TraceEventType">event type</see>. </param>
        ''' <returns> Ids. </returns>
        <Extension()>
        Public Function ToId(ByVal value As TraceEventType) As Integer
            Return If(Methods.TraceEventTypeIdHash.ContainsKey(value), Methods.TraceEventTypeIdHash(value), 0)
        End Function

#End Region

#Region " TRACE LEVELS AND TRACE EVENT TYPES "

        ''' <summary> Builds trace event type trace level hash. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <returns> A Dictionary for translating trace events to trace levels. </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")>
        Public Function BuildTraceEventTypeTraceLevelHash() As IDictionary(Of TraceEventType, TraceLevel)
            Dim dix2 As New Dictionary(Of TraceEventType, TraceLevel)
            Dim dix3 As Dictionary(Of TraceEventType, TraceLevel) = dix2
            dix3.Add(TraceEventType.Critical, Diagnostics.TraceLevel.Error)
            dix3.Add(TraceEventType.Error, Diagnostics.TraceLevel.Error)
            dix3.Add(TraceEventType.Information, Diagnostics.TraceLevel.Info)
            dix3.Add(TraceEventType.Resume, Diagnostics.TraceLevel.Info)
            dix3.Add(TraceEventType.Start, Diagnostics.TraceLevel.Info)
            dix3.Add(TraceEventType.Suspend, Diagnostics.TraceLevel.Off)
            dix3.Add(TraceEventType.Transfer, Diagnostics.TraceLevel.Info)
            dix3.Add(TraceEventType.Verbose, Diagnostics.TraceLevel.Verbose)
            dix3.Add(TraceEventType.Warning, Diagnostics.TraceLevel.Warning)
            Return dix2
        End Function

        ''' <summary> The trace event type trace level hash. </summary>
        Private _TraceEventTypeTraceLevelHash As IDictionary(Of TraceEventType, TraceLevel)

        ''' <summary> Trace event type trace level hash. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <returns> An IDictionary(Of TraceEventType, TraceLevel) </returns>
        Private Function TraceEventTypeTraceLevelHash() As IDictionary(Of TraceEventType, TraceLevel)
            If Methods._TraceEventTypeTraceLevelHash Is Nothing Then
                Methods._TraceEventTypeTraceLevelHash = Methods.BuildTraceEventTypeTraceLevelHash()
            End If
            Return Methods._TraceEventTypeTraceLevelHash
        End Function

        ''' <summary>
        ''' Derives a <see cref="TraceLevel">trace level</see> from the
        ''' <see cref="TraceEventType">trace event type</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="value"> A <see cref="TraceEventType">event type</see>. </param>
        ''' <returns>
        ''' A <see cref="TraceLevel">trace level</see> of a trace switch. Specifies what messages to
        ''' output for the <see cref="System.Diagnostics.Debug">debug</see>
        ''' and <see cref="System.Diagnostics.Trace">trace</see> and
        ''' <see cref="System.Diagnostics.TraceSwitch">trace switch</see> classes.
        ''' </returns>
        <CodeAnalysis.SuppressMessage("Code Quality", "IDE0051:Remove unused private members", Justification:="<Pending>")> <Extension()>
        Public Function ToTraceLevel(ByVal value As TraceEventType) As TraceLevel
            Return If(Methods.TraceEventTypeTraceLevelHash.ContainsKey(value), Methods.TraceEventTypeTraceLevelHash(value), Diagnostics.TraceLevel.Info)
        End Function

#End Region

#Region " SOURCE LEVELS AND TRACE EVENT TYPES "

        ''' <summary> Parse trace event type. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="value"> A <see cref="TraceEventType">event type</see>. </param>
        ''' <returns> The SourceLevels. </returns>
        Public Function ParseTraceEventType(ByVal value As TraceEventType) As SourceLevels
            Dim result As SourceLevels = SourceLevels.Information
            Select Case value
                Case TraceEventType.Critical
                    result = SourceLevels.Critical
                Case TraceEventType.Error
                    result = SourceLevels.Error
                Case TraceEventType.Information
                    result = SourceLevels.Information
                Case TraceEventType.Verbose
                    result = SourceLevels.Verbose
                Case TraceEventType.Warning
                    result = SourceLevels.Warning
            End Select
            Return result
        End Function

        ''' <summary> Builds trace event type source levels hash. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <returns> A Dictionary for translating trace events to source levels. </returns>
        Public Function BuildTraceEventTypeSourceLevelsHash() As IDictionary(Of TraceEventType, SourceLevels)
            Dim dix2 As New Dictionary(Of TraceEventType, SourceLevels)
            Dim dix3 As Dictionary(Of TraceEventType, SourceLevels) = dix2
            dix3.Add(TraceEventType.Critical, ParseTraceEventType(TraceEventType.Critical))
            dix3.Add(TraceEventType.Error, ParseTraceEventType(TraceEventType.Error))
            dix3.Add(TraceEventType.Information, ParseTraceEventType(TraceEventType.Information))
            dix3.Add(TraceEventType.Verbose, ParseTraceEventType(TraceEventType.Verbose))
            dix3.Add(TraceEventType.Warning, ParseTraceEventType(TraceEventType.Warning))
            Return dix2
        End Function
        ''' <summary> Source levels trace event type source levels hash. </summary>
        Private _SourceLevelsTraceEventTypeSourceLevelsHash As IDictionary(Of TraceEventType, SourceLevels)

        ''' <summary> Source levels trace event type source levels hash. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <returns> An IDictionary(Of SourceLevels, TraceEventType) </returns>
        Private Function SourceLevelsTraceEventTypeSourceLevelsHash() As IDictionary(Of TraceEventType, SourceLevels)
            If Methods._SourceLevelsTraceEventTypeSourceLevelsHash Is Nothing Then
                Methods._SourceLevelsTraceEventTypeSourceLevelsHash = Methods.BuildTraceEventTypeSourceLevelsHash
            End If
            Return Methods._SourceLevelsTraceEventTypeSourceLevelsHash
        End Function

        ''' <summary>
        ''' Derives a <see cref="SourceLevels">source level</see> given the
        ''' <see cref="TraceEventType">trace event type</see>.
        ''' </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="value"> A <see cref="TraceEventType">event type</see>. </param>
        ''' <returns> SourceLevels. </returns>
        <Extension()>
        Public Function ToSourceLevel(ByVal value As TraceEventType) As SourceLevels
            Return If(Methods.SourceLevelsTraceEventTypeSourceLevelsHash.ContainsKey(value),
                            Methods.SourceLevelsTraceEventTypeSourceLevelsHash(value), Diagnostics.SourceLevels.Information)
        End Function

#End Region

    End Module

End Namespace
