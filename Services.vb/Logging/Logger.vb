Imports System.Collections.Concurrent
Imports System.Threading
Imports System.Threading.Tasks

Imports isr.Core.Services.DiagnosticsExtensions
Imports isr.Core.Services.ExceptionExtensions

''' <summary> Extends the <see cref="Microsoft.VisualBasic.Logging.Log">log</see>. </summary>
''' <remarks>
''' (c) 2014 Integrated Scientific Resources, Inc. All rights reserved.<para>
''' Licensed under The MIT License.</para><para>
''' David, 2/16/2014 </para>
''' </remarks>
<System.Diagnostics.DebuggerDisplay("id={UniqueId}")>
Public Class MyLog
    Inherits Logging.Log
    Implements IDisposable

#Region " CONSTRUCTION and CLEANUP "
    ''' <summary> The default file log trace listener name. </summary>
    Public Const DefaultFileLogTraceListenerName As String = "FileLog"
    ''' <summary> The default source name. </summary>
    Public Const DefaultSourceName As String = "DefaultSource"

    ''' <summary> Default constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Private Sub New()
        Me.New(My.MyLibrary.AssemblyProduct)
    End Sub

    ''' <summary> Constructor. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="name"> The name. </param>
    Private Sub New(ByVal name As String)
        MyBase.New(name)
        Me.ResetKnownStateThis()
    End Sub

    ''' <summary> Creates a new MyLog. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="name"> The name. </param>
    ''' <returns> A MyLog. </returns>
    Public Shared Function Create(ByVal name As String) As MyLog
        If MyLog._Instance Is Nothing Then
            SyncLock MyLog.SyncLocker
                MyLog._Instance = New MyLog(name)
            End SyncLock
        End If
        Return MyLog._Instance
    End Function

    ''' <summary> Gets the trace source. </summary>
    ''' <value> The trace source. </value>
    Public Overloads Property TraceSource As MyTraceSource

    ''' <summary> Resets to known state. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Private Sub ResetKnownStateThis()
        Me._ContentsQueue = New ConcurrentQueue(Of TraceMessage)
        ' instantiate cancellation token
        Me._CancellationTokenSource = New CancellationTokenSource
        Me._CancellationToken = Me.CancellationTokenSource.Token
        Dim name As String = MyBase.TraceSource.Name
        Me._TraceSource = New MyTraceSource(name)
        ' a private internal trace level is now used instead of the trace source to facilitate overriding the trace source level.
        Me._TraceSource.ApplyTraceLevel(TraceEventType.Verbose)
        If Not Me.TraceSource.HasBeenConfigured Then
            MyBase.InitializeWithDefaultsSinceNoConfigExists()
        End If
        Me.ApplyTraceLevelThis(TraceEventType.Verbose)
    End Sub

#Region " SINGLETON "

    ''' <summary>
    ''' The locking object to enforce thread safety when creating the singleton instance.
    ''' </summary>
    Private Shared ReadOnly SyncLocker As New Object

    ''' <summary>
    ''' The shared instance.
    ''' </summary>
    Private Shared _Instance As MyLog

    ''' <summary> Gets the instance. </summary>
    ''' <value> The instance. </value>
    Public Shared ReadOnly Property Instance() As MyLog
        Get
            Return MyLog._Instance
        End Get
    End Property

    ''' <summary> Returns a new or existing instance of this class. </summary>
    ''' <remarks>
    ''' Use this property to get an instance of this class. Returns the default instance of this form
    ''' allowing to use this form as a singleton for.
    ''' </remarks>
    ''' <value> <c>A</c> new or existing instance of the class. </value>
    Public Shared ReadOnly Property NewInstance(ByVal name As String) As MyLog
        Get
            If MyLog._Instance Is Nothing Then
                SyncLock MyLog.SyncLocker
                    MyLog._Instance = New MyLog(name)
                End SyncLock
            End If
            Return MyLog._Instance
        End Get
    End Property

    ''' <summary> Returns true if the instance is nothing or was disposed. </summary>
    ''' <value> The <c>True</c> if instance or nothing or was disposed. </value>
    Public Shared ReadOnly Property InstanceDisposed As Boolean
        Get
            Return MyLog._Instance Is Nothing OrElse MyLog._Instance.IsDisposed
        End Get
    End Property

#End Region

#Region " Disposable Support "

    ''' <summary> Gets the disposed indicator. </summary>
    ''' <value> The disposed indicator. </value>
    Public ReadOnly Property IsDisposed As Boolean Implements ITraceMessageListener.IsDisposed

    ''' <summary>
    ''' Releases the unmanaged resources used by the isr.Core.Services.MyLog and optionally releases
    ''' the managed resources.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="disposing"> True to release both managed and unmanaged resources; false to
    '''                          release only unmanaged resources. </param>
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        Try
            If Not Me.IsDisposed AndAlso disposing Then
                Me.CancellationTokenSource?.Cancel()
                Me.ActionTask?.Wait(TimeSpan.FromSeconds(2))
                If Me.CancellationTokenSource IsNot Nothing Then Me._CancellationTokenSource.Dispose() : Me._CancellationTokenSource = Nothing
                If Me.ActionTask IsNot Nothing Then Me._ActionTask.Dispose() : Me._ActionTask = Nothing
                If Me.AsyncTask IsNot Nothing Then Me._AsyncTask.Dispose() : Me._AsyncTask = Nothing
                Me.TraceSource = Nothing
            End If
        Finally
            Me._IsDisposed = True
        End Try
    End Sub

    ''' <summary> Calls <see cref="M:Dispose(Boolean Disposing)" /> to cleanup. </summary>
    ''' <remarks>
    ''' Do not make this method Overridable (virtual) because a derived class should not be able to
    ''' override this method.
    ''' </remarks>
    Public Sub Dispose() Implements IDisposable.Dispose
        Me.Dispose(True)
        ' Take this object off the finalization(Queue) and prevent finalization code 
        ' from executing a second time.
        GC.SuppressFinalize(Me)
    End Sub

    ''' <summary>
    ''' This destructor will run only if the Dispose method does not get called. It gives the base
    ''' class the opportunity to finalize. Do not provide destructors in types derived from this
    ''' class.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Protected Overrides Sub Finalize()
        ' Do not re-create Dispose clean-up code here.
        ' Calling Dispose(false) is optimal for readability and maintainability.
        Me.Dispose(False)
    End Sub

#End Region

#End Region

#Region " WRITE LOG ENTRY "

    ''' <summary> Writes an entry. </summary>
    ''' <remarks> Overloads the underlying method. </remarks>
    ''' <param name="message">  The message details. </param>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    Public Overloads Sub WriteEntry(ByVal message As String, ByVal severity As TraceEventType)
        Me.TraceSource.TraceEvent(severity, message)
    End Sub

    ''' <summary> Writes an entry. </summary>
    ''' <remarks> Overloads the underlying method. </remarks>
    ''' <param name="message">  The message details. </param>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    Public Overloads Sub WriteEntry(ByVal message As String, ByVal severity As TraceEventType, ByVal id As Integer)
        Me.TraceSource.TraceEvent(severity, id, message)
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="details">  The message details. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal details As String) As String
        If details IsNot Nothing Then
            Me.WriteEntry(details, severity)
            Return details
        End If
        Return String.Empty
    End Function

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="details">  The message details. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal id As Integer, ByVal details As String) As String
        If details IsNot Nothing Then
            Me.WriteEntry(details, severity, id)
            Return details
        End If
        Return String.Empty
    End Function

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="format">   The message format. </param>
    ''' <param name="args">     Specified the message arguments. </param>
    ''' <returns> The message details or empty. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Me.WriteLogEntry(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End If
        Return String.Empty
    End Function

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="format">   The message format. </param>
    ''' <param name="args">     Specified the message arguments. </param>
    ''' <returns> The message details or empty. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal id As Integer,
                                      ByVal format As String, ByVal ParamArray args() As Object) As String
        If format IsNot Nothing Then
            Me.WriteLogEntry(severity, id, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
        End If
        Return String.Empty
    End Function

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="messages"> Message information to me. </param>
    ''' <returns> The message details or empty. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal messages As String()) As String
        Return If(messages IsNot Nothing, Me.WriteLogEntry(severity, String.Join(",", messages)), String.Empty)
    End Function

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="messages"> Message information to me. </param>
    ''' <returns> The message details or empty. </returns>
    Public Function WriteLogEntry(ByVal severity As TraceEventType, ByVal id As Integer, ByVal messages As String()) As String
        Return If(messages IsNot Nothing, Me.WriteLogEntry(severity, id, String.Join(",", messages)), String.Empty)
    End Function

#End Region

#Region " WRITE EXCEPTION DETAILS "

    ''' <summary> Writes an exception. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex"> The exception. </param>
    Public Overloads Sub WriteException(ByVal ex As Exception)
        Me.WriteException(ex, TraceEventType.Error, "", 0)
    End Sub

    ''' <summary> Writes an exception. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex">             The exception. </param>
    ''' <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
    '''                               data. </param>
    ''' <param name="additionalInfo"> Additional information. </param>
    Public Overloads Sub WriteException(ByVal ex As Exception, ByVal severity As TraceEventType, ByVal additionalInfo As String)
        Me.WriteException(ex, severity, additionalInfo, 0)
    End Sub

    ''' <summary> Writes an exception. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex">             The exception. </param>
    ''' <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
    '''                               data. </param>
    ''' <param name="additionalInfo"> Additional information. </param>
    ''' <param name="id">             The identifier. </param>
    Public Overloads Sub WriteException(ByVal ex As Exception, ByVal severity As TraceEventType, ByVal additionalInfo As String, ByVal id As Integer)
        If ex IsNot Nothing Then
            Dim builder As New System.Text.StringBuilder
            builder.Append(ex.ToFullBlownString)
            If Not String.IsNullOrWhiteSpace(additionalInfo) Then
                builder.Append("; ")
                builder.Append(additionalInfo)
            End If
            Me.WriteEntry(builder.ToString, severity, id)
        End If
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex">             The exception. </param>
    ''' <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
    '''                               data. </param>
    ''' <param name="additionalInfo"> Additional information. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType, ByVal additionalInfo As String)
        If ex IsNot Nothing Then Me.WriteException(ex, severity, additionalInfo)
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex">             The exception. </param>
    ''' <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
    '''                               data. </param>
    ''' <param name="id">             The identifier. </param>
    ''' <param name="additionalInfo"> Additional information. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType, ByVal id As Integer, ByVal additionalInfo As String)
        If ex IsNot Nothing Then Me.WriteException(ex, severity, additionalInfo, id)
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex">       The exception. </param>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="format">   The additional information format. </param>
    ''' <param name="args">     The additional information arguments. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType,
                                     ByVal format As String, ByVal ParamArray args() As Object)
        Me.WriteExceptionDetails(ex, severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex">       The exception. </param>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="format">   The additional information format. </param>
    ''' <param name="args">     The additional information arguments. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType,
                                         ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object)
        Me.WriteExceptionDetails(ex, severity, id, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex"> The exception. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception)
        Me.WriteExceptionDetails(ex, TraceEventType.Error, "")
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="ex"> The exception. </param>
    ''' <param name="id"> The identifier. </param>
    Public Sub WriteExceptionDetails(ByVal ex As Exception, ByVal id As Integer)
        Me.WriteExceptionDetails(ex, TraceEventType.Error, id, "")
    End Sub

#End Region

#Region " WRITE LOG ENTRY -- OVERRIDE TRACE LEVEL "

    ''' <summary>
    ''' Writes a message to the application log listeners. Overrides the current log level.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="details">  The message details. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntryOverride(ByVal severity As TraceEventType, ByVal details As String) As String
        If details IsNot Nothing Then
            ' save the current trace level.
            Dim lastSourceLevel As Diagnostics.SourceLevels = Me.TraceSource.Switch.Level
            ' set the requested level.
            Me.TraceSource.Switch.Level = severity.ToSourceLevel
            ' write the entry.
            Me.WriteEntry(details, severity)
            ' restore the level.
            Me.TraceSource.Switch.Level = lastSourceLevel
            Return details
        End If
        Return String.Empty
    End Function

    ''' <summary>
    ''' Writes a message to the application log listeners. Overrides the current log level.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="details">  The message details. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntryOverride(ByVal severity As TraceEventType,
                                              ByVal id As Integer, ByVal details As String) As String
        If details IsNot Nothing Then
            ' save the current trace level.
            Dim lastSourceLevel As Diagnostics.SourceLevels = Me.TraceSource.Switch.Level
            ' set the requested level.
            Me.TraceSource.Switch.Level = severity.ToSourceLevel
            ' write the entry.
            Me.WriteEntry(details, severity, id)
            ' restore the level.
            Me.TraceSource.Switch.Level = lastSourceLevel
            Return details
        End If
        Return String.Empty
    End Function

    ''' <summary>
    ''' Writes a message to the application log listeners. Overrides the current log level.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="format">   The message details. </param>
    ''' <param name="args">     The arguments. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntryOverride(ByVal severity As TraceEventType,
                                          ByVal format As String, ByVal ParamArray args() As Object) As String
        Return If(format IsNot Nothing,
            Me.WriteLogEntryOverride(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args)),
            String.Empty)
    End Function

    ''' <summary>
    ''' Writes a message to the application log listeners. Overrides the current log level.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
    ''' <param name="id">       The identifier. </param>
    ''' <param name="format">   The message details. </param>
    ''' <param name="args">     The arguments. </param>
    ''' <returns> Message or empty string. </returns>
    Public Function WriteLogEntryOverride(ByVal severity As TraceEventType,
                                              ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object) As String
        Return If(format IsNot Nothing,
            Me.WriteLogEntryOverride(severity, id, String.Format(Globalization.CultureInfo.CurrentCulture, format, args)),
            String.Empty)
    End Function

#End Region

#Region " LOG TRACE MESSAGES "

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="message"> The message. </param>
    Public Sub WriteLogEntry(ByVal message As TraceMessage)
        If message IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(message.Details) Then
            Me.WriteLogEntry(message.EventType, message.Id, message.Details)
        End If
    End Sub

    ''' <summary> Writes a message to the application log listeners. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="message">        The message. </param>
    ''' <param name="additionalInfo"> Information describing the additional. </param>
    Public Sub WriteLogEntry(ByVal message As TraceMessage, ByVal additionalInfo As String)
        If message IsNot Nothing Then
            If String.IsNullOrWhiteSpace(additionalInfo) Then
                Me.WriteLogEntry(message)
            ElseIf Not String.IsNullOrWhiteSpace(message.Details) Then
                Me.WriteLogEntry(message.EventType, message.Id, "{0},{1}", message.Details, additionalInfo)
            End If
        End If
    End Sub

#End Region

#Region " FILE  "

    ''' <summary> Gets the file log trace listener. </summary>
    ''' <value> The file log trace listener. </value>
    Public ReadOnly Property FileLogTraceListener As Logging.FileLogTraceListener
        Get
            Return Me.TraceSource.DefaultFileLogWriter
        End Get
    End Property

    ''' <summary> Gets the filename of the full log file. </summary>
    ''' <value> The filename of the full log file. </value>
    Public ReadOnly Property FullLogFileName As String
        Get
            Return Me.FileLogTraceListener.FullLogFileName
        End Get
    End Property

    ''' <summary> Gets the size of the default file log writer file. </summary>
    ''' <value> The size of the file. </value>
    Public ReadOnly Property FileSize As Long
        Get
            Return DefaultFileLogTraceListener.FileSize(Me.FullLogFileName)
        End Get
    End Property

    ''' <summary> Checks if the default file log writer file exists. </summary>
    ''' <value> <c>True</c> if the log file exists. </value>
    Public ReadOnly Property LogFileExists() As Boolean
        Get
            Return DefaultFileLogTraceListener.FileSize(Me.FullLogFileName) > 2
        End Get
    End Property

    ''' <summary> Opens log file. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The Process. </returns>
    Public Function OpenLogFile() As Process
        Dim proc As String = "explorer.exe"
        Dim args As String = $"{ControlChars.Quote}{Me.FullLogFileName}{ControlChars.Quote}"
        Return Process.Start(proc, args)
    End Function

    ''' <summary> Opens folder location. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The Process. </returns>
    Public Function OpenFolderLocation() As Process
        Dim proc As String = "explorer.exe"
        Dim fi As New System.IO.FileInfo(Me.FullLogFileName)
        Dim args As String = $"{ControlChars.Quote}{fi.DirectoryName}{ControlChars.Quote}"
        Return Process.Start(proc, args)
    End Function

#End Region

#Region " REPLACE TRACE LISTENER "

    ''' <summary> Replaces the default file log trace listener with a new one. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="logWriter"> The
    '''                          <see cref="Logging.FileLogTraceListener">log writer</see>. </param>
    ''' <returns>
    ''' The <see cref="Logging.FileLogTraceListener">file log trace listener. </see>
    ''' </returns>
    Public Function ReplaceDefaultTraceListener(ByVal logWriter As Logging.FileLogTraceListener) As Logging.FileLogTraceListener
        If Me.TraceSource IsNot Nothing Then
            Me.TraceSource.ReplaceDefaultTraceListener(logWriter)
            MyBase.TraceSource.ReplaceDefaultTraceListener(logWriter)
        End If
        Return logWriter
    End Function

    ''' <summary>
    ''' Replaces the default file log trace listener with a new one for the current user.
    ''' </summary>
    ''' <remarks> The current user application data folder is used. </remarks>
    ''' <returns>
    ''' The <see cref="Logging.FileLogTraceListener">file log trace listener. </see>
    ''' </returns>
    Public Function ReplaceDefaultTraceListener() As Logging.FileLogTraceListener
        Return Me.ReplaceDefaultTraceListener(False)
    End Function

    ''' <summary> Replaces the default file log trace listener with a new one. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="allUsers"> True to select the folder for
    '''                         <see cref="MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
    '''                         all users; otherwise, use using current user; otherwise, use the
    '''                         folder for
    '''                         <see cref="MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> 
    ''' </param>
    ''' <returns>
    ''' The <see cref="Logging.FileLogTraceListener">file log trace listener. </see>
    ''' </returns>
    Public Function ReplaceDefaultTraceListener(ByVal allUsers As Boolean) As Logging.FileLogTraceListener
        Dim tempListener As Logging.FileLogTraceListener = Nothing
        Dim listener As Logging.FileLogTraceListener = Nothing
        Try
            tempListener = New DefaultFileLogTraceListener(allUsers)
            listener = tempListener
            Me.ReplaceDefaultTraceListener(listener)
        Finally
            If tempListener IsNot Nothing Then tempListener.Dispose()
        End Try
        Return listener
    End Function

#End Region

#Region " ASYNC QUEUED MESSAGES "

    ''' <summary> The cancellation token source. </summary>
    ''' <value> The cancellation token source. </value>
    Private ReadOnly Property CancellationTokenSource As CancellationTokenSource

    ''' <summary> The cancellation token. </summary>
    ''' <value> The cancellation token. </value>
    Private ReadOnly Property CancellationToken As CancellationToken

    ''' <summary> Query if this object is cancellation requested. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> True if cancellation requested, false if not. </returns>
    Private Function IsCancellationRequested() As Boolean
        Return Me.CancellationToken.IsCancellationRequested
    End Function

    ''' <summary> Gets or sets a queue of contents. </summary>
    ''' <value> A queue of contents. </value>
    Protected ReadOnly Property ContentsQueue As ConcurrentQueue(Of TraceMessage)

    ''' <summary> Gets contents queue. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The contents queue. </returns>
    Private Function GetContentsQueue() As ConcurrentQueue(Of TraceMessage)
        Return Me._ContentsQueue
    End Function

    ''' <summary> Gets the content. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> The content. </returns>
    Private Function GetContent() As TraceMessage
        Dim result As TraceMessage = Nothing
        Me.ContentsQueue.TryDequeue(result)
        Return result
    End Function

    ''' <summary> Uses the message worker to flush any queued messages. </summary>
    ''' <remarks>
    ''' If the calling thread is different from the thread that created the TextBox control, this
    ''' method creates a SetTextCallback and calls itself asynchronously using the Invoke method.
    ''' </remarks>
    Public Sub FlushMessages()
        ' Me.FlushMessagesWorker()
        Me.FlushMessagesTask()
    End Sub

#End Region

#Region " ASYNC QUEUED MESSAGES: TASK "

    ''' <summary> Gets or sets the asynchronous task. </summary>
    ''' <value> The asynchronous task. </value>
    Protected ReadOnly Property AsyncTask As Task

    ''' <summary> Gets or sets the action task. </summary>
    ''' <value> The action task. </value>
    Protected ReadOnly Property ActionTask As Task

    ''' <summary> Queries if a task is active. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <param name="task"> The task. </param>
    ''' <returns> <c>true</c> if a task is active; otherwise <c>false</c> </returns>
    Private Shared Function IsTaskActive(ByVal task As Task) As Boolean
        Return task IsNot Nothing AndAlso Not (task.IsCanceled OrElse task.IsCompleted OrElse task.IsFaulted)
    End Function

    ''' <summary> Query if this object is busy. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> <c>true</c> if busy; otherwise <c>false</c> </returns>
    Private Function IsBusy() As Boolean
        Return Not Me.IsDisposed AndAlso MyLog.IsTaskActive(Me.ActionTask)
    End Function

    ''' <summary> Uses the message worker to flush any queued messages. </summary>
    ''' <remarks>
    ''' If the calling thread is different from the thread that created the TextBox control, this
    ''' method creates a SetTextCallback and calls itself asynchronously using the Invoke method.
    ''' </remarks>
    Private Sub FlushMessagesTask()
        If Not Me.IsBusy Then
            Me._AsyncTask = Me.StartAsyncTask()
        End If
    End Sub

    ''' <summary> Start Asynchronous task. </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    ''' <returns> A Task. </returns>
    Private Async Function StartAsyncTask() As Task
        Me._ActionTask = Task.Run(AddressOf Me.TraceQueue)
        Await Me.ActionTask
    End Function

    ''' <summary>
    ''' This event handler sets the Text property of the TextBox control. It is called on the thread
    ''' that created the TextBox control, so the call is thread-safe.
    ''' </summary>
    ''' <remarks> David, 2020-09-17. </remarks>
    Private Sub TraceQueue()
        Do While Me.GetContentsQueue()?.Any AndAlso Not Me.IsCancellationRequested
            ' the check for tracing is now down when adding the event to the queue.
            ' Dim value As TraceMessage = Me.GetContent: If value IsNot Nothing AndAlso Me.ShouldTrace(value.EventType) Then Me.TraceSource.TraceEvent(value)
            Me.TraceSource.TraceEvent(Me.GetContent)
        Loop
    End Sub

#End Region

End Class

