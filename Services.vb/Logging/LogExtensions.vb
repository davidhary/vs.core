﻿Imports System.Runtime.CompilerServices

Imports isr.Core.Services.DiagnosticsExtensions
Imports isr.Core.Services.ExceptionExtensions
Namespace VisualBasicLoggingExtensions
    ''' <summary> Extends the <see cref="Logging.Log">application log</see> functionality. </summary>
    ''' <remarks> (c) 2010 Integrated Scientific Resources, Inc. All rights reserved. <para>
    ''' Licensed under The MIT License.</para><para>
    ''' David, 11/19/2010, 1.2.3975.x. </para></remarks>
    Public Module Methods

#Region " TRACE LEVEL "

        ''' <summary> Applies the trace level. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">   The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="value"> The <see cref="TraceEventType">trace level</see> value. </param>
        <Extension()>
        Public Sub ApplyTraceLevel(ByVal log As Logging.Log, ByVal value As TraceEventType)
            If log IsNot Nothing AndAlso log.TraceSource IsNot Nothing Then
                log.TraceSource.ApplyTraceLevel(value)
            End If
        End Sub

        ''' <summary> Returns the trace level. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log"> The <see cref="Logging.Log">application log</see>. </param>
        ''' <returns>
        ''' The trace level; <see cref="TraceEventType.Information">information</see> if log is nothing.
        ''' </returns>
        <Extension()>
        Public Function TraceLevel(ByVal log As Logging.Log) As TraceEventType
            Return If(log Is Nothing OrElse log.TraceSource Is Nothing, TraceEventType.Information, log.TraceSource.TraceLevel)
        End Function

        ''' <summary> Checks if the log should trace the event type. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">       The log. </param>
        ''' <param name="eventType"> The <see cref="TraceEventType">event type</see>. </param>
        ''' <returns>
        ''' <c>True</c>&gt; If the log should trace the  c&gt;TrueThe trace level;
        ''' <see cref="TraceEventType.Information">information</see> if log is nothing.
        ''' </returns>
        <Extension()>
        Public Function ShouldTrace(ByVal log As Logging.Log, ByVal eventType As TraceEventType) As Boolean
            Return eventType <= log.TraceLevel
        End Function

#End Region

#Region " WRITE LOG ENTRY "

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="details">  The message details. </param>
        ''' <returns> Message or empty string. </returns>
        <Extension()>
        Public Function WriteLogEntry(ByVal log As Logging.Log, ByVal severity As TraceEventType, ByVal details As String) As String
            If details IsNot Nothing Then
                If log IsNot Nothing Then
                    log.WriteEntry(details, severity)
                End If
                Return details
            End If
            Return String.Empty
        End Function

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="id">       The identifier. </param>
        ''' <param name="details">  The message details. </param>
        ''' <returns> Message or empty string. </returns>
        <Extension()>
        Public Function WriteLogEntry(ByVal log As Logging.Log, ByVal severity As TraceEventType, ByVal id As Integer, ByVal details As String) As String
            If details IsNot Nothing Then
                If log IsNot Nothing Then
                    log.WriteEntry(details, severity, id)
                End If
                Return details
            End If
            Return String.Empty
        End Function

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="format">   The message format. </param>
        ''' <param name="args">     Specified the message arguments. </param>
        ''' <returns> The message details or empty. </returns>
        <Extension()>
        Public Function WriteLogEntry(ByVal log As Logging.Log, ByVal severity As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
            If format IsNot Nothing Then
                Methods.WriteLogEntry(log, severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            End If
            Return String.Empty
        End Function

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="id">       The identifier. </param>
        ''' <param name="format">   The message format. </param>
        ''' <param name="args">     Specified the message arguments. </param>
        ''' <returns> The message details or empty. </returns>
        <Extension()>
        Public Function WriteLogEntry(ByVal log As Logging.Log, ByVal severity As TraceEventType, ByVal id As Integer,
                                      ByVal format As String, ByVal ParamArray args() As Object) As String
            If format IsNot Nothing Then
                Methods.WriteLogEntry(log, severity, id, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            End If
            Return String.Empty
        End Function

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="messages"> Message information to log. </param>
        ''' <returns> The message details or empty. </returns>
        <Extension()>
        Public Function WriteLogEntry(ByVal log As Logging.Log, ByVal severity As TraceEventType, ByVal messages As String()) As String
            Return If(messages IsNot Nothing, Methods.WriteLogEntry(log, severity, String.Join(",", messages)), String.Empty)
        End Function

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="id">       The identifier. </param>
        ''' <param name="messages"> Message information to log. </param>
        ''' <returns> The message details or empty. </returns>
        <Extension()>
        Public Function WriteLogEntry(ByVal log As Logging.Log, ByVal severity As TraceEventType, ByVal id As Integer, ByVal messages As String()) As String
            Return If(messages IsNot Nothing, Methods.WriteLogEntry(log, severity, id, String.Join(",", messages)), String.Empty)
        End Function

#End Region

#Region " WRITE EXCEPTION DETAILS "

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">            The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="ex">             The exception. </param>
        ''' <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
        '''                               data. </param>
        ''' <param name="additionalInfo"> Additional information. </param>
        <Extension()>
        Public Sub WriteExceptionDetails(ByVal log As Logging.Log, ByVal ex As Exception, ByVal severity As TraceEventType,
                                          ByVal additionalInfo As String)
            If ex IsNot Nothing AndAlso log IsNot Nothing Then
                log.WriteExceptionDetails(ex, severity, 0, additionalInfo)
            End If

        End Sub

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">            The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="ex">             The exception. </param>
        ''' <param name="severity">       The <see cref="TraceEventType">event type</see> of the trace
        '''                               data. </param>
        ''' <param name="id">             The identifier. </param>
        ''' <param name="additionalInfo"> Additional information. </param>
        <Extension()>
        Public Sub WriteExceptionDetails(ByVal log As Logging.Log, ByVal ex As Exception, ByVal severity As TraceEventType,
                                         ByVal id As Integer, ByVal additionalInfo As String)
            If ex IsNot Nothing AndAlso log IsNot Nothing Then
                Dim builder As New System.Text.StringBuilder
                builder.Append(ex.ToFullBlownString)
                If Not String.IsNullOrWhiteSpace(additionalInfo) Then
                    builder.Append("; ")
                    builder.Append(additionalInfo)
                End If
                log.WriteEntry(builder.ToString, severity, id)
            End If

        End Sub

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="ex">       The exception. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="format">   The additional information format. </param>
        ''' <param name="args">     The additional information arguments. </param>
        <Extension()>
        Public Sub WriteExceptionDetails(ByVal log As Logging.Log, ByVal ex As Exception, ByVal severity As TraceEventType,
                                     ByVal format As String, ByVal ParamArray args() As Object)
            If log IsNot Nothing Then
                log.WriteExceptionDetails(ex, severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            End If
        End Sub

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="ex">       The exception. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="id">       The identifier. </param>
        ''' <param name="format">   The additional information format. </param>
        ''' <param name="args">     The additional information arguments. </param>
        <Extension()>
        Public Sub WriteExceptionDetails(ByVal log As Logging.Log, ByVal ex As Exception, ByVal severity As TraceEventType,
                                         ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object)
            If log IsNot Nothing Then
                log.WriteExceptionDetails(ex, severity, id, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            End If
        End Sub

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log"> The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="ex">  The exception. </param>
        <Extension()>
        Public Sub WriteExceptionDetails(ByVal log As Logging.Log, ByVal ex As Exception)
            If log IsNot Nothing Then
                log.WriteExceptionDetails(ex, TraceEventType.Error, "")
            End If
        End Sub

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log"> The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="ex">  The exception. </param>
        ''' <param name="id">  The identifier. </param>
        <Extension()>
        Public Sub WriteExceptionDetails(ByVal log As Logging.Log, ByVal ex As Exception, ByVal id As Integer)
            If log IsNot Nothing Then
                log.WriteExceptionDetails(ex, TraceEventType.Error, id, "")
            End If
        End Sub

#End Region

#Region " WRITE LOG ENTRY -- OVERRIDE TRACE LEVEL "

        ''' <summary>
        ''' Writes a message to the application log listeners. Overrides the current log level.
        ''' </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="details">  The message details. </param>
        ''' <returns> Message or empty string. </returns>
        <Extension()>
        Public Function WriteLogEntryOverride(ByVal log As Logging.Log,
                                              ByVal severity As TraceEventType, ByVal details As String) As String
            If details IsNot Nothing Then
                If log IsNot Nothing Then
                    ' save the current trace level.
                    Dim lastSourceLevel As Diagnostics.SourceLevels = log.TraceSource.Switch.Level
                    ' set the requested level.
                    log.TraceSource.Switch.Level = severity.ToSourceLevel
                    ' write the entry.
                    log.WriteEntry(details, severity)
                    ' restore the level.
                    log.TraceSource.Switch.Level = lastSourceLevel
                End If
                Return details
            End If
            Return String.Empty
        End Function

        ''' <summary>
        ''' Writes a message to the application log listeners. Overrides the current log level.
        ''' </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="id">       The identifier. </param>
        ''' <param name="details">  The message details. </param>
        ''' <returns> Message or empty string. </returns>
        <Extension()>
        Public Function WriteLogEntryOverride(ByVal log As Logging.Log, ByVal severity As TraceEventType,
                                              ByVal id As Integer, ByVal details As String) As String
            If details IsNot Nothing Then
                If log IsNot Nothing Then
                    ' save the current trace level.
                    Dim lastSourceLevel As Diagnostics.SourceLevels = log.TraceSource.Switch.Level
                    ' set the requested level.
                    log.TraceSource.Switch.Level = severity.ToSourceLevel
                    ' write the entry.
                    log.WriteEntry(details, severity, id)
                    ' restore the level.
                    log.TraceSource.Switch.Level = lastSourceLevel
                End If
                Return details
            End If
            Return String.Empty
        End Function

        ''' <summary>
        ''' Writes a message to the application log listeners. Overrides the current log level.
        ''' </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="format">   The message details. </param>
        ''' <param name="args">     The arguments. </param>
        ''' <returns> Message or empty string. </returns>
        <Extension()>
        Public Function WriteLogEntryOverride(ByVal log As Logging.Log, ByVal severity As TraceEventType,
                                              ByVal format As String, ByVal ParamArray args() As Object) As String
            Return If(format IsNot Nothing,
                log.WriteLogEntryOverride(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args)),
                String.Empty)
        End Function

        ''' <summary>
        ''' Writes a message to the application log listeners. Overrides the current log level.
        ''' </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="severity"> The <see cref="TraceEventType">event type</see> of the trace data. </param>
        ''' <param name="id">       The identifier. </param>
        ''' <param name="format">   The message details. </param>
        ''' <param name="args">     The arguments. </param>
        ''' <returns> Message or empty string. </returns>
        <Extension()>
        Public Function WriteLogEntryOverride(ByVal log As Logging.Log, ByVal severity As TraceEventType,
                                              ByVal id As Integer, ByVal format As String, ByVal ParamArray args() As Object) As String
            Return If(format IsNot Nothing,
                log.WriteLogEntryOverride(severity, id, String.Format(Globalization.CultureInfo.CurrentCulture, format, args)),
                String.Empty)
        End Function

#End Region

#Region " LOG TRACE MESSAGES "

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">     The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="message"> The message. </param>
        <Extension()>
        Public Sub WriteLogEntry(ByVal log As Logging.Log, ByVal message As TraceMessage)
            If log IsNot Nothing AndAlso message IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(message.Details) Then
                log.WriteLogEntry(message.EventType, message.Id, message.Details)
            End If
        End Sub

        ''' <summary> Writes a message to the application log listeners. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">            The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="message">        The message. </param>
        ''' <param name="additionalInfo"> Information describing the additional. </param>
        <Extension()>
        Public Sub WriteLogEntry(ByVal log As Logging.Log, ByVal message As TraceMessage, ByVal additionalInfo As String)
            If log IsNot Nothing AndAlso message IsNot Nothing Then
                If String.IsNullOrWhiteSpace(additionalInfo) Then
                    log.WriteLogEntry(message)
                ElseIf Not String.IsNullOrWhiteSpace(message.Details) Then
                    log.WriteLogEntry(message.EventType, message.Id, "{0},{1}", message.Details, additionalInfo)
                End If
            End If
        End Sub

#End Region

#Region " FILE  "

        ''' <summary> Returns the filename of the default file log trace writer log file. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log"> The <see cref="Logging.Log">application log</see>. </param>
        ''' <returns> The filename of the log file. </returns>
        <Extension()>
        Public Function DefaultFileLogWriterFilePath(ByVal log As Logging.Log) As String
            If log Is Nothing Then
                Return String.Empty
            Else
                If log.DefaultFileLogWriter Is Nothing Then
                    Return String.Empty
                ElseIf log.DefaultFileLogWriter IsNot Nothing Then
                    Return log.DefaultFileLogWriter.FullLogFileName
                Else
                    Return String.Empty
                End If
            End If
        End Function

        ''' <summary> Checks if the default file log writer file exists. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log"> The log. </param>
        ''' <returns> <c>True</c> if the log file exists. </returns>
        <Extension()>
        Public Function DefaultFileLogWriterFileExists(ByVal log As Logging.Log) As Boolean
            Return DefaultFileLogTraceListener.FileSize(log.DefaultFileLogWriterFilePath) > 2
        End Function

#End Region

#Region " REPLACE TRACE LISTENER "

        ''' <summary> Replaces the default file log trace listener with a new one. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">       The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="logWriter"> The
        '''                          <see cref="Logging.FileLogTraceListener">log writer</see>. </param>
        ''' <returns>
        ''' The <see cref="Logging.FileLogTraceListener">file log trace listener. </see>
        ''' </returns>
        <Extension()>
        Public Function ReplaceDefaultTraceListener(ByVal log As Logging.Log, ByVal logWriter As Logging.FileLogTraceListener) As Logging.FileLogTraceListener
            If log IsNot Nothing AndAlso log.TraceSource IsNot Nothing Then
                log.TraceSource.ReplaceDefaultTraceListener(logWriter)
            End If
            Return logWriter
        End Function

        ''' <summary>
        ''' Replaces the default file log trace listener with a new one for the current user.
        ''' </summary>
        ''' <remarks> The current user application data folder is used. </remarks>
        ''' <param name="log"> The <see cref="Logging.Log">application log</see>. </param>
        ''' <returns>
        ''' The <see cref="Logging.FileLogTraceListener">file log trace listener. </see>
        ''' </returns>
        <Extension()>
        Public Function ReplaceDefaultTraceListener(ByVal log As Logging.Log) As Logging.FileLogTraceListener
            Return log.ReplaceDefaultTraceListener(False)
        End Function

        ''' <summary> Replaces the default file log trace listener with a new one. </summary>
        ''' <remarks> David, 2020-09-17. </remarks>
        ''' <param name="log">      The <see cref="Logging.Log">application log</see>. </param>
        ''' <param name="allUsers"> True to select the folder for
        '''                         <see cref="MyServices.SpecialDirectoriesProxy.AllUsersApplicationData"/>
        '''                         all users; otherwise, use using current user; otherwise, use the
        '''                         folder for
        '''                         <see cref="MyServices.SpecialDirectoriesProxy.CurrentUserApplicationData"/> 
        ''' </param>
        ''' <returns>
        ''' The <see cref="Logging.FileLogTraceListener">file log trace listener. </see>
        ''' </returns>
        <Extension()>
        Public Function ReplaceDefaultTraceListener(ByVal log As Logging.Log, ByVal allUsers As Boolean) As Logging.FileLogTraceListener
            Dim tempListener As Logging.FileLogTraceListener = Nothing
            Dim listener As Logging.FileLogTraceListener = Nothing
            Try
                tempListener = New DefaultFileLogTraceListener(allUsers)
                listener = tempListener
                If log IsNot Nothing Then
                    log.ReplaceDefaultTraceListener(listener)
                Else
                End If
            Finally
                If tempListener IsNot Nothing Then tempListener.Dispose()
            End Try
            Return listener
        End Function

#End Region

    End Module
End Namespace
