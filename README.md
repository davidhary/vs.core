# Core Libraries

Support libraries extending the Visual Studio framework for Windows forms, statistics and engineering.

* [Source Code](#Source-Code)
* [MIT License](LICENSE.md)
* [Change Log](CHANGELOG.md)
* [Facilitated By](#FacilitatedBy)
* [Authors](#Authors)
* [Acknowledgments](#Acknowledgments)
* [Open Source](#Open-Source)
* [Closed Software](#Closed-software)

<a name="Source-Code"></a>
## Source Code
Clone the repository along with its requisite repositories to their respective relative path.

### Repositories
The repositories listed in [external repositories](ExternalReposCommits.csv) are required:
* [Core](https://www.bitbucket.org/davidhary/vs.core) - Core Libraries
```
git clone git@bitbucket.org:davidhary/vs.core.git
```

Clone the repositories into the following folders (parents of the .git folder):
```
.\Libraries\VS\Core\Core
```

#### Global Configuration Files
ISR libraries use a global editor configuration file and a global test run settings file. 
These files can be found in the [IDE Repository]((https://www.bitbucket.org/davidhary/vs.ide)).

Restoring Editor Configuration assuming c:\My is the root folder of the .NET solutions):
```
xcopy /Y c:\My\.editorconfig c:\My\.editorconfig.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.editorconfig c:\My\.editorconfig
```

Restoring Run Settings assuming c:\user\<me> is the root user folder:
```
xcopy /Y c:\user\<me>\.runsettings c:\user\<me>\.runsettings.bak
xcopy /Y c:\My\Libraries\VS\Core\IDE\code\.runsettings c:\user\<me>\.runsettings
```

<a name="FacilitatedBy"></a>
## Facilitated By

* [Visual Studio](https://www.visualstudio.com/) - Visual Studio
* [Jarte](https://www.jarte.com/) - RTF Editor
* [Wix Installer](https://www.wixtoolset.org/) - WiX Toolset
* [Atomineer Code Documentation](https://www.atomineerutils.com/) - Code Documentation
* [EW Software](https://github.com/EWSoftware/VSSpellChecker/wiki/) - Spell Checker
* [Code Converter](https://github.com/icsharpcode/CodeConverter) - Code Converter
* [Search and Replace](http://www.funduc.com/search_replace.htm) - Funduc Search and Replace for Windows

<a name="Authors"></a>
## Authors

* **David Hary** - *Initial Workarounds* - [ATE Coder](https://www.IntegratedScientificResources.com)

<a name="Acknowledgments"></a>
## Acknowledgments

* [Its all a remix](www.everythingisaremix.info) -- we are but a spec on the shoulders of giants
* [John Simmons](https://www.codeproject.com/script/Membership/View.aspx?mid=7741) - outlaw programmer
* [Stack overflow](https://www.stackoveflow.com) - Joel Spolsky

<a name="Open-Source"></a>
### Open source
Open source used by this software is described and licensed at the
following sites:  
[Core Message Box Library](https://bitbucket.org/davidhary/vs.core)  
[Drop Shadow and Fade From](http://www.CodeProject.com/KB/cs/LetYourFormDropAShadow.aspx)  
[Engineering Format](http://WallaceKelly.BlogSpot.com/)  
[Efficient Strong Enumerations](http://www.CodeProject.com/KB/cs/efficient_strong_enum.aspx)  
[Enumeration Extensions](http://www.CodeProject.com/Articles/37921/Enums-Flags-and-CSharp-Oh-my-bad-pun.aspx)  
[Exception Extension](https://www.codeproject.com/Tips/1179564/A-Quick-Dirty-Extension-Method-to-Get-the-Full-Exc)  
[High Resolution Clock](https://www.codeproject.com/articles/792410/high-resolution-clock-in-csharp)  
[Linq Statistics](http://www.codeproject.com/Articles/42492/Using-LINQ-to-Calculate-Basic-Statistics)  
[Notification Window](http://www.codeproject.com/KB/dialog/notificationwindow.aspx)  
[Office Style Splash Screen](http://www.codeproject.com/Articles/804316/Office-Style-Splash-Screen)  
[Safe Copy from Clipboard](http://stackoverflow.com/questions/899350/how-to-copy-the-contents-of-a-string-to-the-clipboard-in-c)  
[Safe Events](http://www.CodeProject.com/KB/cs/EventSafeTrigger.aspx")  
[Safe Events](http://www.DreamInCode.net/forums/user/334492-aeonhack)  
[Safe Events](http://www.DreamInCode.net/code/snippet5016.htm)  
[String Enumerator](http://www.codeproject.com/Articles/17472/StringEnumerator)

<a name="Closed-software"></a>
### Closed software
Closed software used by this software are described and licensed on
the following sites:  
[SQL Exception Message](https://msdn.microsoft.com/en-us/library/ms365274.aspx)
 
